FROM openjdk:8-jdk-alpine
RUN apk add --no-cache ttf-dejavu
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ADD ./reports ./reports
ADD ./db ./db
ADD ./fonts ./fonts
ENTRYPOINT ["java","-jar","/app.jar"]