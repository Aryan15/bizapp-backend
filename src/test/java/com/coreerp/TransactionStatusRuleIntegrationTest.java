package com.coreerp;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.event.rule.AgendaEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.coreerp.dto.TransactionItemQuantityRule;
import com.coreerp.dto.TransactionStatusRule;

//import org.drools.core.event.DebugAgendaEventListener;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = {KieConfiguration.class, TestConfigurtaion.class})
public class TransactionStatusRuleIntegrationTest {

	final static Logger log = LogManager.getLogger(TransactionStatusRuleIntegrationTest.class);
	
    @Autowired
    private KieContainer kieContainer;
    
	
    private KieSession kieSession ;
    
	@Before
	public void setup(){
		
		kieSession = kieContainer.newKieSession("rulesSession");
		//kieSession.addEventListener( new DebugAgendaEventListener() );
		//kieSession.addEventListener( new DebugWorkingMemoryEventListener() );
		
		//KnowledgeRuntimeLogger logger = KnowledgeRuntimeLoggerFactory.newFileLogger(kieSession, "logdir/mylogfile");
	}
	
	
	
	@Test
    public void giveNewQuot_whenFireRule_thenGetStatusNew() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionId("ID01");
		transactionStatusRule.setQuantity(5.0);

		transactionStatusRule.setTransactionType(ApplicationConstants.QUOTATION_TYPE_CUSTOMER);
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo("New");
    }
 
	@Test
    public void giveNewPO_whenFireRule_thenGetQuotationStatusClosed() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.QUOTATION_TYPE_CUSTOMER);
		transactionStatusRule.setTransactionId("ID01");

		transactionStatusRule.setQuantity(5.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.QUOTATION_STATUS_NEW);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.TRANSACTION_CUSTOMER_PURCHASE_ORDER);
		transactionStatusRule.setSourceTransactionId("PO01");
		transactionStatusRule.setSourceQuantity(4.0);
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.QUOTATION_STATUS_COMPLETED);
    }

	
	@Test
    public void giveDeletedPartialDC_whenFireRule_thenGetPOStatusNew() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.PURCHASE_ORDER_TYPE_SUPPLIER);
		transactionStatusRule.setTransactionId("PO01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setCurrentStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_DC);
		transactionStatusRule.setLatestStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.DC_TYPE_SUPPLIER);
		transactionStatusRule.setSourceTransactionId("DC01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.DC_STATUS_DELETED);
		transactionStatusRule.setSourceQuantity(3.0);
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevDCBalanceQuantity(2.0);
        transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(null);
        transactionItemQuantityRule.setQuantity(5.0);
        transactionItemQuantityRule.setSourceQuantity(3.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getdCBalanceQuantity()).isEqualTo(5.0);
        
    }

	@Test
    public void giveDeletedPartialDCWithTwoItems_whenFireRule_thenGetPOStatusNew() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.PURCHASE_ORDER_TYPE_SUPPLIER);
		transactionStatusRule.setTransactionId("PO01");
		transactionStatusRule.setQuantity(15.0);
		transactionStatusRule.setCurrentStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_DC);
		transactionStatusRule.setLatestStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.DC_TYPE_SUPPLIER);
		transactionStatusRule.setSourceTransactionId("DC01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.DC_STATUS_DELETED);
		transactionStatusRule.setSourceQuantity(10.0);
		
		log.info("transactionStatusRule: "+transactionStatusRule);
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(transactionStatusRule.getLatestStatus());
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevDCBalanceQuantity(5.0);
        transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(null);
        transactionItemQuantityRule.setQuantity(5.0);
        transactionItemQuantityRule.setSourceQuantity(5.0);
        
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount); // expected 0
        

        assertThat(ruleCount).isEqualTo(1);
        
        
        transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI02");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevDCBalanceQuantity(4.0);
        transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(null);
        transactionItemQuantityRule.setQuantity(10.0);
        transactionItemQuantityRule.setSourceQuantity(6.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        log.info("transactionItemQuantityRule: "+transactionItemQuantityRule);
        

        assertThat(transactionItemQuantityRule.getdCBalanceQuantity()).isEqualTo(10.0);
        
        
    }
	
	@Test
    public void giveDeletedDC_whenFireRule_thenGetPOStatusPartial() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.PURCHASE_ORDER_TYPE_SUPPLIER);
		transactionStatusRule.setTransactionId("PO01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setLatestStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_DC);
		transactionStatusRule.setCurrentStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_DC_GENERATED);

		transactionStatusRule.setSourceTransactionType(ApplicationConstants.DC_TYPE_SUPPLIER);
		transactionStatusRule.setSourceTransactionId("DC01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.DC_STATUS_DELETED);
		transactionStatusRule.setSourceQuantity(3.0);
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_DC);
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevDCBalanceQuantity(0.0);
        transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(null);
        transactionItemQuantityRule.setQuantity(5.0);
        transactionItemQuantityRule.setSourceQuantity(3.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getdCBalanceQuantity()).isEqualTo(3.0);
        
    }
	
	@Test
    public void giveNewDCWithFullPOQty_whenFireRule_thenGetPOStatusDCGenerated() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.PURCHASE_ORDER_TYPE_SUPPLIER);
		transactionStatusRule.setTransactionId("PO01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setDcBalanceQuantity(5.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.DC_TYPE_SUPPLIER);
		transactionStatusRule.setSourceTransactionId("DC01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.DC_STATUS_NEW);
		transactionStatusRule.setSourceQuantity(5.0);
		
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.PURCHASE_ORDER_STATUS_DC_GENERATED);
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevDCBalanceQuantity(5.0);
        transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(null);
        transactionItemQuantityRule.setQuantity(5.0);
        transactionItemQuantityRule.setSourceQuantity(5.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getdCBalanceQuantity()).isEqualTo(0.0);
    }

	@Test
    public void giveEditedDCWithLesserThanOriginalQuantity_whenFireRule_thenGetPOStatusDCPartial() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.PURCHASE_ORDER_TYPE_SUPPLIER);
		transactionStatusRule.setTransactionId("PO01");
		transactionStatusRule.setQuantity(5.0);
		//transactionStatusRule.setDcBalanceQuantity(2.0);
		transactionStatusRule.setCurrentStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_DC_GENERATED);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.DC_TYPE_SUPPLIER);
		transactionStatusRule.setSourceTransactionId("DC01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.DC_STATUS_NEW);
		transactionStatusRule.setSourceQuantity(3.0);
		
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_DC);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevDCBalanceQuantity(0.0);
        transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(null);
        transactionItemQuantityRule.setQuantity(5.0);
        transactionItemQuantityRule.setSourceQuantity(-2.0); // 3 - 5
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getdCBalanceQuantity()).isEqualTo(2.0);
        
    }
	
	@Test
    public void giveNewDCWithPartialPOQty_whenFireRule_thenGetPOStatusDCPartial() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.PURCHASE_ORDER_TYPE_SUPPLIER);
		transactionStatusRule.setTransactionId("PO01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setDcBalanceQuantity(5.0);
		transactionStatusRule.setCurrentStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.DC_TYPE_SUPPLIER);
		transactionStatusRule.setSourceTransactionId("DC01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.DC_STATUS_NEW);
		transactionStatusRule.setSourceQuantity(3.0);
        transactionStatusRule.setSourcePrevQuantity(0.0);
		
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_DC);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        

        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevDCBalanceQuantity(5.0);
        transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(5.0);
        transactionItemQuantityRule.setQuantity(5.0);
        transactionItemQuantityRule.setSourceQuantity(3.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getdCBalanceQuantity()).isEqualTo(2.0);
        
    }
	
	@Test
    public void giveNewGRN_whenFireRule_thenGetDCStatusGRNGenerated() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.DC_TYPE_SUPPLIER);
		transactionStatusRule.setTransactionId("DC01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setDcBalanceQuantity(null);
		transactionStatusRule.setGrnBalanceQuantity(5.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.DC_STATUS_NEW);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.GRN_TYPE_SUPPLIER);
		transactionStatusRule.setSourceTransactionId("GRN01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.GRN_STATUS_NEW);
		transactionStatusRule.setSourceQuantity(5.0);
		
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.DC_STATUS_GRN_GENEREATED);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        

        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevDCBalanceQuantity(null);
        transactionItemQuantityRule.setPrevGRNBalanceQuantity(5.0);
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(null);
        transactionItemQuantityRule.setQuantity(5.0);
        transactionItemQuantityRule.setSourceQuantity(5.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getgRNBalanceQuantity()).isEqualTo(0.0);
        
    }	
	
	
	@Test
    public void giveNewGRNWithLesserQuantity_whenFireRule_thenGetDCStatusPartialGRN() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.DC_TYPE_SUPPLIER);
		transactionStatusRule.setTransactionId("DC01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setDcBalanceQuantity(null);
		transactionStatusRule.setGrnBalanceQuantity(5.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.DC_STATUS_NEW);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.GRN_TYPE_SUPPLIER);
		transactionStatusRule.setSourceTransactionId("GRN01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.GRN_STATUS_NEW);
		transactionStatusRule.setSourceQuantity(3.0);
        transactionStatusRule.setSourcePrevQuantity(0.0);
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.DC_STATUS_PARTIAL_GRN);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevDCBalanceQuantity(null);
        transactionItemQuantityRule.setPrevGRNBalanceQuantity(5.0);
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(null);
        transactionItemQuantityRule.setQuantity(5.0);
        transactionItemQuantityRule.setSourceQuantity(3.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getgRNBalanceQuantity()).isEqualTo(2.0);
        
    }
	
	@Test
    public void giveEditedGRNWithLesserQuantity_whenFireRule_thenGetDCStatusPartialGRN() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.DC_TYPE_SUPPLIER);
		transactionStatusRule.setTransactionId("DC01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setDcBalanceQuantity(null);
		transactionStatusRule.setGrnBalanceQuantity(0.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.DC_STATUS_GRN_GENEREATED);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.GRN_TYPE_SUPPLIER);
		transactionStatusRule.setSourceTransactionId("GRN01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.GRN_STATUS_NEW);
		transactionStatusRule.setSourceQuantity(3.0);
		transactionStatusRule.setSourcePrevQuantity(5.0);
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.DC_STATUS_PARTIAL_GRN);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevDCBalanceQuantity(null);
        transactionItemQuantityRule.setPrevGRNBalanceQuantity(5.0);
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(null);
        transactionItemQuantityRule.setQuantity(5.0);
        transactionItemQuantityRule.setSourceQuantity(3.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getgRNBalanceQuantity()).isEqualTo(2.0);
        
    }
	
	
	
	@Test
    public void giveNewDC_whenFireRule_thenGetDCStatusNew() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.DC_TYPE_SUPPLIER);
		transactionStatusRule.setTransactionId(null);
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setDcBalanceQuantity(null);
		transactionStatusRule.setGrnBalanceQuantity(null);
		transactionStatusRule.setSourceTransactionType(null);
		transactionStatusRule.setSourceTransactionId(null);
		transactionStatusRule.setSourceTransactionStatus(null);
		transactionStatusRule.setSourceQuantity(null);
		transactionStatusRule.setSourcePrevQuantity(null);
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.DC_STATUS_NEW);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevDCBalanceQuantity(null);
        transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(null);
        transactionItemQuantityRule.setQuantity(5.0);
        transactionItemQuantityRule.setSourceQuantity(null);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getgRNBalanceQuantity()).isEqualTo(5.0);
        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(5.0);
        
    }
	
	
	@Test
    public void giveDeletedGRN_whenFireRule_thenGetDCStatusNew() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.DC_TYPE_SUPPLIER);
		transactionStatusRule.setTransactionId("DC01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setDcBalanceQuantity(null);
		transactionStatusRule.setGrnBalanceQuantity(3.0);
		transactionStatusRule.setLatestStatus(ApplicationConstants.DC_STATUS_NEW);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.DC_STATUS_PARTIAL_GRN);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.GRN_TYPE_SUPPLIER);
		transactionStatusRule.setSourceTransactionId("GRN01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.GRN_STATUS_DELETED);
		transactionStatusRule.setSourceQuantity(2.0);
		transactionStatusRule.setSourcePrevQuantity(2.0);
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.DC_STATUS_NEW);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevDCBalanceQuantity(null);
        transactionItemQuantityRule.setPrevGRNBalanceQuantity(3.0);
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(null);
        transactionItemQuantityRule.setQuantity(5.0);
        transactionItemQuantityRule.setSourceQuantity(2.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getgRNBalanceQuantity()).isEqualTo(5.0);
        
    }
	
	@Test
    public void giveNewInvoiceWithFullQuantity_whenFireRule_thenGetPOStatusInvoiced() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.PURCHASE_ORDER_TYPE_CUSTOMER);
		transactionStatusRule.setTransactionId("PO01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setInvoiceBalanceQuantity(5.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
		transactionStatusRule.setSourceTransactionId("INV01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.INVOICE_STATUS_NEW);
		transactionStatusRule.setSourceQuantity(5.0);
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.PURCHASE_ORDER_STATUS_INVOICED);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(3.0);
        transactionItemQuantityRule.setQuantity(3.0);
        transactionItemQuantityRule.setSourceQuantity(3.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(0.0);
        
        transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI02");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(2.0);
        transactionItemQuantityRule.setQuantity(2.0);
        transactionItemQuantityRule.setSourceQuantity(2.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(0.0);
        
        
    }

	
	@Test
    public void giveNewInvoiceWithPartialQuantity_whenFireRule_thenGetPOStatusPartiallyInvoiced() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.PURCHASE_ORDER_TYPE_CUSTOMER);
		transactionStatusRule.setTransactionId("PO01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setInvoiceBalanceQuantity(5.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
		transactionStatusRule.setSourceTransactionId("INV01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.INVOICE_STATUS_NEW);
		transactionStatusRule.setSourceQuantity(3.0);
		transactionStatusRule.setSourcePrevQuantity(0.0);
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_INVOICED);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(3.0);
        transactionItemQuantityRule.setQuantity(3.0);
        transactionItemQuantityRule.setSourceQuantity(3.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(0.0);
        
        transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI02");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(2.0);
        transactionItemQuantityRule.setQuantity(2.0);
        transactionItemQuantityRule.setSourceQuantity(1.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(1.0);
        
        
    }
	
	
	@Test
    public void giveEditedInvoiceWithLesserQuantity_whenFireRule_thenGetPOStatusPartiallyInvoiced() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.PURCHASE_ORDER_TYPE_CUSTOMER);
		transactionStatusRule.setTransactionId("PO01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setInvoiceBalanceQuantity(0.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_INVOICED);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
		transactionStatusRule.setSourceTransactionId("INV01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.INVOICE_STATUS_NEW);
		transactionStatusRule.setSourceQuantity(3.0);
		transactionStatusRule.setSourcePrevQuantity(5.0);
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_INVOICED);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(0.0);
        transactionItemQuantityRule.setQuantity(3.0);
        transactionItemQuantityRule.setSourceQuantity(-1.0); // 2 - 3
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(1.0);
        
        transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI02");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(0.0);
        transactionItemQuantityRule.setQuantity(2.0);
        transactionItemQuantityRule.setSourceQuantity(-1.0); // 1 - 2
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(1.0);
        
        
    }
	
	@Test
    public void giveDeletedInvoice_whenFireRule_thenGetPOStatusPartiallyInvoiced() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.PURCHASE_ORDER_TYPE_CUSTOMER);
		transactionStatusRule.setTransactionId("PO01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setInvoiceBalanceQuantity(0.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_INVOICED);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
		transactionStatusRule.setSourceTransactionId("INV01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.INVOICE_STATUS_DELETED);
		transactionStatusRule.setSourceQuantity(3.0);
		transactionStatusRule.setLatestStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_INVOICED);
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_INVOICED);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(0.0);
        transactionItemQuantityRule.setQuantity(3.0);
        transactionItemQuantityRule.setSourceQuantity(2.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(2.0);
        
        transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("POI02");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(0.0);
        transactionItemQuantityRule.setQuantity(2.0);
        transactionItemQuantityRule.setSourceQuantity(2.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(2.0);
        
        
    }

	
	@Test
    public void giveNewInvoiceWithFullQuantity_whenFireRule_thenGetDCStatusInvoiced() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.DC_TYPE_CUSTOMER);
		transactionStatusRule.setTransactionId("DC01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setInvoiceBalanceQuantity(5.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.DC_STATUS_NEW);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
		transactionStatusRule.setSourceTransactionId("INV01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.INVOICE_STATUS_NEW);
		transactionStatusRule.setSourceQuantity(5.0);
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.DC_STATUS_INVOICED);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(3.0);
        transactionItemQuantityRule.setQuantity(3.0);
        transactionItemQuantityRule.setSourceQuantity(3.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(0.0);
        
        transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI02");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(2.0);
        transactionItemQuantityRule.setQuantity(2.0);
        transactionItemQuantityRule.setSourceQuantity(2.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(0.0);
        
        
    }
	
	
	@Test
    public void giveNewInvoiceWithPartialQuantity_whenFireRule_thenGetDCStatusPartiallyInvoiced() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.DC_TYPE_CUSTOMER);
		transactionStatusRule.setTransactionId("DC01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setInvoiceBalanceQuantity(5.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.DC_STATUS_NEW);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
		transactionStatusRule.setSourceTransactionId("INV01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.INVOICE_STATUS_NEW);
		transactionStatusRule.setSourceQuantity(3.0);
		transactionStatusRule.setSourcePrevQuantity(0.0);
		
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.DC_STATUS_PARTIALLY_INVOICED);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(3.0);
        transactionItemQuantityRule.setQuantity(3.0);
        transactionItemQuantityRule.setSourceQuantity(3.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(0.0);
        
        transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI02");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(2.0);
        transactionItemQuantityRule.setQuantity(2.0);
        transactionItemQuantityRule.setSourceQuantity(1.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(1.0);
        
        
    }
	
	
	@Test
    public void giveEditedInvoiceWithLesserQuantity_whenFireRule_thenGetDCStatusPartiallyInvoiced() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.DC_TYPE_CUSTOMER);
		transactionStatusRule.setTransactionId("DC01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setInvoiceBalanceQuantity(0.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.DC_STATUS_INVOICED);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
		transactionStatusRule.setSourceTransactionId("INV01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.INVOICE_STATUS_NEW);
		transactionStatusRule.setSourceQuantity(3.0);
		transactionStatusRule.setSourcePrevQuantity(5.0);
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.DC_STATUS_PARTIALLY_INVOICED);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(0.0);
        transactionItemQuantityRule.setQuantity(3.0);
        transactionItemQuantityRule.setSourceQuantity(-1.0); // 2 - 3
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(1.0);
        
        transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI02");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(0.0);
        transactionItemQuantityRule.setQuantity(2.0);
        transactionItemQuantityRule.setSourceQuantity(-1.0); // 1 - 2
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(1.0);
        
        
    }
	
	
	@Test
    public void giveDeletedInvoice_whenFireRule_thenGetDCStatusPartiallyInvoiced() throws Exception {
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setTransactionType(ApplicationConstants.DC_TYPE_CUSTOMER);
		transactionStatusRule.setTransactionId("DC01");
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setInvoiceBalanceQuantity(0.0);

		transactionStatusRule.setCurrentStatus(ApplicationConstants.DC_STATUS_INVOICED);
		transactionStatusRule.setSourceTransactionType(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
		transactionStatusRule.setSourceTransactionId("INV01");
		transactionStatusRule.setSourceTransactionStatus(ApplicationConstants.INVOICE_STATUS_DELETED);
		transactionStatusRule.setSourceQuantity(3.0);
		transactionStatusRule.setLatestStatus(ApplicationConstants.DC_STATUS_PARTIALLY_INVOICED);
		
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionStatusRule.getNewStatus()).isEqualTo(ApplicationConstants.DC_STATUS_PARTIALLY_INVOICED);
        
        
        TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI01");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(0.0);
        transactionItemQuantityRule.setQuantity(3.0);
        transactionItemQuantityRule.setSourceQuantity(2.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(2.0);
        
        transactionItemQuantityRule = new TransactionItemQuantityRule();
        
        transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
        transactionItemQuantityRule.setItemId("DCI02");
        transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
        transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(0.0);
        transactionItemQuantityRule.setQuantity(2.0);
        transactionItemQuantityRule.setSourceQuantity(2.0);
        
        kieSession.insert(transactionItemQuantityRule);

        ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        assertThat(transactionItemQuantityRule.getInvoiceBalanceQuantity()).isEqualTo(2.0);
        
        
    }
}
