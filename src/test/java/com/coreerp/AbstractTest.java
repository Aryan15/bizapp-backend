package com.coreerp;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ErpAPIApp.class, TestConfiguration.class})
@WebAppConfiguration

public abstract class AbstractTest {
	
	/**
     * The Logger instance for all classes in the unit test framework.
     */
    protected Logger logger = LogManager.getLogger(this.getClass());
    
	@Ignore
	public void contextLoads() {
	}

}
