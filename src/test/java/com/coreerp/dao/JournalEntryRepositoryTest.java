package com.coreerp.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.coreerp.model.JournalEntry;

@RunWith(SpringRunner.class)
@DataJpaTest
public class JournalEntryRepositoryTest {

	@Autowired
	TestEntityManager entityManager;
	
	
	@Autowired
	JournalEntryRepository journalEntryRepository;
	
	@Test
	@Ignore
	public void testSave(){
		
		//Given
		
		JournalEntry journalEntry = new JournalEntry();
		
		journalEntry.setAmount(100.00);
		journalEntry.setBusinessDate(new Date());
		journalEntry.setCreditAccount(null);
		journalEntry.setDebitAccount(null);
		journalEntry.setParty(null);
		journalEntry.setRemarks("Test");
		journalEntry.setTransactionDate(new Date());
		journalEntry.setTransactionId(null);
		journalEntry.setTransactionType(null);
		
		
		//When
		
		JournalEntry saved = journalEntryRepository.save(journalEntry);
		
		//Then
		
		assertThat(saved.getRemarks()).isEqualTo(journalEntry.getRemarks());
	}
}
