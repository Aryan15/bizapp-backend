package com.coreerp.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.coreerp.model.PayableReceivableHeader;
import com.coreerp.model.PayableReceivableItem;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PayableReceivableHeaderRepositoryTest {
	
	@Autowired
	TestEntityManager entityManager;
	
	
	@Autowired
	PayableReceivableHeaderRepository payableReceivableHeaderRepository;
	
	
	@Test
	@Ignore
	public void testSave(){
		
		//Given
		
		PayableReceivableItem payableReceivableItem = new PayableReceivableItem();
		List<PayableReceivableItem> items = new ArrayList<PayableReceivableItem>();
		
		PayableReceivableHeader payableReceivableHeader = new PayableReceivableHeader();
		
		
		payableReceivableItem.setPayingAmount(100.00);
		items.add(payableReceivableItem);
		payableReceivableHeader.setBankName("citi");
		payableReceivableHeader.setPayableReceivableItems(items);
		
		//When
		
		PayableReceivableHeader saved = payableReceivableHeaderRepository.save(payableReceivableHeader);
		
		//Then
		
		assertThat(saved.getBankName()).isEqualTo(payableReceivableHeader.getBankName());
		
	}
	

}
