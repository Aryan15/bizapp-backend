package com.coreerp.controller;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsCollectionWithSize.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.coreerp.ApplicationConstants;
import com.coreerp.ErpAPIApp;
import com.coreerp.dto.PurchaseOrderDTO;
import com.coreerp.dto.PurchaseOrderItemDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ErpAPIApp.class, TestConfiguration.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PurchaseOrderControllerTest {
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext wac;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Before
	public void setup(){
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}
	
	@Test
	@Ignore
	public void verifyAllPurchaseOrders() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.get("/api/po").accept(MediaType.APPLICATION_JSON))
		//.andExpect(jsonPath("$.headerId").exists());
		//.andExpect(jsonPath("$",hasSize(1))) //hasSize(1)
		//.andExpect(jsonPath("$.[*].purchaseOrderId", containsInAnyOrder("101")))
		;
	}
	
	@Test
	@Ignore
	public void verifySavePO() throws Exception{
		
		PurchaseOrderDTO poIn = new PurchaseOrderDTO();
		
		poIn.setId("po-test-header-id");
		//poIn.setStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
		poIn.setCompanyId(1l);
		poIn.setFinancialYearId(1l);
		poIn.setTaxId(1l);
		poIn.setPartyId(1l);
		poIn.setTaxAmount(100.00);
		poIn.setTotalTaxableAmount(1500.00);
		poIn.setGrandTotal(1600.00);
		poIn.setPurchaseOrderNumber("test-number");
		poIn.setPurchaseOrderDate(new Date());
		poIn.setPurchaseOrderTypeId(2l);
		
		
		PurchaseOrderItemDTO poItem = new PurchaseOrderItemDTO();
		
		poItem.setId("po-test-item-id1");
		poItem.setAmount(200.00);
		poItem.setPrice(50.00);
		poItem.setQuantity(4.0);
		poItem.setMaterialId(1l);
		poItem.setTaxId(1l);
		poItem.setHeaderId("po-test-header-id");
		poItem.setUnitOfMeasurementId(1l);
		
		List<PurchaseOrderItemDTO> poItems = new ArrayList<PurchaseOrderItemDTO>();
		
		poItems.add(poItem);
		
		poIn.setPurchaseOrderItems(poItems);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/po")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(poIn))
				.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(jsonPath("$.id").exists())
				.andDo(print());
		
		
	}
	
	@Test
	@Ignore
	public void verifyGetPo() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.get("/api/po/101/1").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists())
		//.andExpect(jsonPath("$",hasSize(1)))
		.andExpect(jsonPath("$.id").value("101"));
	}
	
	
	@Test
	@Ignore
	public void verifySaveAndGetPo() throws Exception {
		
		PurchaseOrderDTO poIn = new PurchaseOrderDTO();
		
		poIn.setId("po-test-header-id");
		//poIn.setStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
		poIn.setCompanyId(1l);
		poIn.setFinancialYearId(1l);
		poIn.setTaxId(1l);
		poIn.setPartyId(1l);
		poIn.setTaxAmount(100.00);
		poIn.setTotalTaxableAmount(1500.00);
		poIn.setGrandTotal(1600.00);
		poIn.setPurchaseOrderNumber("test-number");
		poIn.setPurchaseOrderDate(new Date());
		poIn.setPurchaseOrderTypeId(2l);
		
		
		PurchaseOrderItemDTO poItem = new PurchaseOrderItemDTO();
		
		poItem.setId("po-test-item-id");
		poItem.setAmount(200.00);
		poItem.setPrice(50.00);
		poItem.setQuantity(4.0);
		poItem.setMaterialId(1l);
		poItem.setTaxId(1l);
		poItem.setHeaderId("po-test-header-id");
		poItem.setUnitOfMeasurementId(1l);
		
		List<PurchaseOrderItemDTO> poItems = new ArrayList<PurchaseOrderItemDTO>();
		
		poItems.add(poItem);
		
		poIn.setPurchaseOrderItems(poItems);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/po")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(poIn))
				.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(jsonPath("$.id").exists())
				.andDo(print());
		
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/po/po-test-header-id/1").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists())
		//.andExpect(jsonPath("$",hasSize(1)))
		.andExpect(jsonPath("$.id").value("po-test-header-id"))
		.andExpect(jsonPath("$.purchaseOrderItems[0].id").value("po-test-item-id"));
	}
}
