package com.coreerp.controller;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.coreerp.ErpAPIAppTest;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.coreerp.ApplicationConstants;
import com.coreerp.ErpAPIApp;
import com.coreerp.dto.DeliveryChallanDTO;
import com.coreerp.dto.DeliveryChallanItemDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ErpAPIApp.class, TestConfiguration.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DeliveryChallanControllerTest {

	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext wac;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Before
	public void setup(){
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}
	
	@Test
	@Ignore
	public void verifyAllDc() throws Exception{
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/dc").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$",hasSize(1)))
		.andExpect(jsonPath("$[0].id").value("101"));
	}
	
	@Test
	@Ignore
	public void verifySaveDc() throws Exception{
		
		DeliveryChallanDTO dcIn = new DeliveryChallanDTO();
		
		dcIn.setId("dc-test-header-id");
		dcIn.setCompanyId(1l);
		//dcIn.setStatus(ApplicationConstants.DC_STATUS_NEW);
		dcIn.setTaxId(1l);
		dcIn.setFinancialYearId(1l);
		dcIn.setDeliveryChallanDate(new Date());
		dcIn.setPartyId(1l);
		dcIn.setTaxAmount(1400.00);
		dcIn.setDeliveryChallanTypeId(1l);//ApplicationConstants.DC_TYPE_CUSTOMER);
		dcIn.setPrice(100.00);
		dcIn.setAmount(1050.00);
		
		DeliveryChallanItemDTO dcItemIn = new DeliveryChallanItemDTO();
		
		dcItemIn.setId("dc-test-item-id");

		dcItemIn.setHeaderId("dc-test-header-id");

		dcItemIn.setMaterialId(1l);
		dcItemIn.setUnitOfMeasurementId(1l);
		dcItemIn.setTaxId(1l);
		
		
		List<DeliveryChallanItemDTO> dcItemInList = new ArrayList<DeliveryChallanItemDTO>();
		
		dcItemInList.add(dcItemIn);
		
		dcIn.setDeliveryChallanItems(dcItemInList);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/dc")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(dcIn))
				.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(jsonPath("$.id").exists())
				.andDo(print());
		
		
	}
	
	@Test
	@Ignore
	public void verifyGetDc() throws Exception{
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/dc/101/1")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.id").exists())
				.andExpect(jsonPath("$.id").value("101"));
		
		
	}
	
	
	@Test
	@Ignore
	public void verifySaveAndGetDc() throws Exception{

		DeliveryChallanDTO dcIn = new DeliveryChallanDTO();
		
		dcIn.setId("dc-test-header-id");
		dcIn.setCompanyId(1l);
		//dcIn.setStatus(ApplicationConstants.DC_STATUS_NEW);
		dcIn.setTaxId(1l);
		dcIn.setFinancialYearId(1l);
		dcIn.setDeliveryChallanDate(new Date());
		dcIn.setPartyId(1l);
		dcIn.setTaxAmount(1400.00);
		dcIn.setDeliveryChallanTypeId(1l);//(ApplicationConstants.DC_TYPE_CUSTOMER);
		dcIn.setPrice(100.00);
		dcIn.setAmount(1050.00);
		
		DeliveryChallanItemDTO dcItemIn = new DeliveryChallanItemDTO();
		
		dcItemIn.setId("dc-test-item-id");

		dcItemIn.setHeaderId("dc-test-header-id");

		dcItemIn.setMaterialId(1l);
		dcItemIn.setUnitOfMeasurementId(1l);
		dcItemIn.setTaxId(1l);
		
		
		List<DeliveryChallanItemDTO> dcItemInList = new ArrayList<DeliveryChallanItemDTO>();
		
		dcItemInList.add(dcItemIn);
		
		dcIn.setDeliveryChallanItems(dcItemInList);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/dc")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(dcIn))
				.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(jsonPath("$.id").exists())
				.andDo(print());
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/dc/dc-test-header-id/1")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.id").exists())
				.andExpect(jsonPath("$.id").value("dc-test-header-id"))
				.andExpect(jsonPath("$.deliveryChallanItems[0].id").value("dc-test-item-id"));
		
	}
	
}
