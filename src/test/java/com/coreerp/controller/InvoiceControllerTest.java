package com.coreerp.controller;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.coreerp.ApplicationConstants;
import com.coreerp.ErpAPIApp;
import com.coreerp.dto.InvoiceDTO;
import com.coreerp.dto.InvoiceItemDTO;
import com.coreerp.serviceimpl.InvoiceServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ErpAPIApp.class, TestConfiguration.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InvoiceControllerTest {

	final static Logger log = LogManager.getLogger(InvoiceControllerTest.class);
	
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext wac;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Before
	public void setup(){
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}
	
	@Test
	@Ignore
	public void verifyAllInvoices() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/invoice").accept(MediaType.APPLICATION_JSON))
		//.andExpect(jsonPath("$.headerId").exists());
		.andExpect(jsonPath("$",hasSize(2)))
		.andExpect(jsonPath("$.[*].invoiceId", containsInAnyOrder("101","102")));
	}
	
	@Test
	@Ignore
	public void verifySaveInvoice() throws Exception {
		
		
		InvoiceDTO invoiceInput = new InvoiceDTO();
		invoiceInput.setId("input-test-header-id-1");
		invoiceInput.setGrandTotal(2500.00);
		invoiceInput.setCompanyId(1l);
		invoiceInput.setPartyId(1l);
		invoiceInput.setTaxId(1l);
		invoiceInput.setFinancialYearId(1l);
		//invoiceInput.setStatus(ApplicationConstants.INVOICE_STATUS_NEW);
		invoiceInput.setInvoiceTypeId(1l);
		
		InvoiceItemDTO invoiceItemInput = new InvoiceItemDTO();
		invoiceItemInput.setId("input-test-item-id-1");
		invoiceItemInput.setAmount(100.00);
		invoiceItemInput.setMaterialId(1l);
		invoiceItemInput.setTaxId(1l);
		invoiceItemInput.setUnitOfMeasurementId(1l);
		
		List<InvoiceItemDTO> invoiceItemInputList = new ArrayList<InvoiceItemDTO>();
		
		invoiceItemInputList.add(invoiceItemInput);
		
		invoiceInput.setInvoiceItems(invoiceItemInputList);

		mockMvc.perform(MockMvcRequestBuilders.post("/api/invoice")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(invoiceInput))
				.accept(MediaType.APPLICATION_JSON)				
				)
				.andExpect(jsonPath("$.invoiceId").exists())
				.andExpect(jsonPath("$.invoiceId").value("input-test-header-id-1"))
				.andExpect(jsonPath("$.invoiceItems[0].id").value("input-test-item-id-1"))
				.andDo(print());
	}

	@Test
	@Ignore
	public void verifyUpdateInvoice() throws Exception {
		
		
		InvoiceDTO invoiceInput = new InvoiceDTO();
		//invoiceInput.setInvoiceId("input-test-header-id-1");
		invoiceInput.setGrandTotal(2500.00);
		invoiceInput.setCompanyId(1l);
		invoiceInput.setPartyId(1l);
		invoiceInput.setTaxId(1l);
		invoiceInput.setFinancialYearId(1l);
		//invoiceInput.setStatus(ApplicationConstants.INVOICE_STATUS_NEW);
		invoiceInput.setInvoiceTypeId(1l);
		
		InvoiceItemDTO invoiceItemInput = new InvoiceItemDTO();
		invoiceItemInput.setId("input-test-item-id-1");
		invoiceItemInput.setAmount(100.00);
		invoiceItemInput.setMaterialId(1l);
		invoiceItemInput.setTaxId(1l);
		invoiceItemInput.setUnitOfMeasurementId(1l);
		
		List<InvoiceItemDTO> invoiceItemInputList = new ArrayList<InvoiceItemDTO>();
		
		invoiceItemInputList.add(invoiceItemInput);
		
		invoiceInput.setInvoiceItems(invoiceItemInputList);

		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/invoice")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(invoiceInput))
				.accept(MediaType.APPLICATION_JSON)				
				)
				.andExpect(jsonPath("$.invoiceId").exists())
				//.andExpect(jsonPath("$.invoiceId").value("input-test-header-id-1"))
				//.andExpect(jsonPath("$.invoiceItems[0].id").value("input-test-item-id-1"))
				//.andDo(print());
				.andReturn();
		
		log.info("result: "+result);
		

		
//		InvoiceDTO invoiceInputU = new InvoiceDTO();
//		invoiceInputU.setInvoiceId("input-test-header-id-1");
//		invoiceInputU.setInvoiceVersion(1);
//		invoiceInputU.setLatest("Y");
//		invoiceInputU.setGrandTotal(2500.00);
//		invoiceInputU.setCompanyId(1l);
//		invoiceInputU.setPartyId(1l);
//		invoiceInputU.setTaxId(1l);
//		invoiceInputU.setFinancialYearId(1l);
//		invoiceInputU.setStatus(ApplicationConstants.INVOICE_STATUS_NEW);
//		invoiceInputU.setInvoiceType(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
//		
//		InvoiceItemDTO invoiceItemInputU = new InvoiceItemDTO();
//		invoiceItemInputU.setId("input-test-item-id-1");
//		invoiceItemInputU.setIdVersion(1);
//		invoiceItemInputU.setLatest("Y");
//		invoiceItemInputU.setAmount(100.00);
//		invoiceItemInputU.setMaterialId(1l);
//		invoiceItemInputU.setTaxId(1l);
//		invoiceItemInputU.setUnitOfMeasurementId(1l);
//		
//		List<InvoiceItemDTO> invoiceItemInputListU = new ArrayList<InvoiceItemDTO>();
//		
//		invoiceItemInputListU.add(invoiceItemInputU);
//		
//		invoiceInputU.setInvoiceItems(invoiceItemInputListU);
//
//		mockMvc.perform(MockMvcRequestBuilders.post("/api/invoice")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(objectMapper.writeValueAsString(invoiceInputU))
//				.accept(MediaType.APPLICATION_JSON)				
//				)
//				.andExpect(jsonPath("$.invoiceId").exists())
//				.andExpect(jsonPath("$.invoiceId").value("input-test-header-id-1"))
//				.andExpect(jsonPath("$.invoiceVersion").value("2"))
//				.andExpect(jsonPath("$.invoiceItems[0].id").value("input-test-item-id-1"))				
//				.andDo(print());
	}

	@Test
	@Ignore
	public void verifyGetInvoice() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/invoice/101/1").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.invoiceId").exists())
		//.andExpect(jsonPath("$",hasSize(1)))
		.andExpect(jsonPath("$.invoiceId").value("101"));
	}


	@Test
	@Ignore
	public void verifySaveAndGetInvoice() throws Exception {
		

		InvoiceDTO invoiceInput = new InvoiceDTO();
		//invoiceInput.setInvoiceId("input-test-header-id-1");
		invoiceInput.setGrandTotal(2500.00);
		invoiceInput.setCompanyId(1l);
		invoiceInput.setPartyId(1l);
		invoiceInput.setTaxId(1l);
		invoiceInput.setFinancialYearId(1l);
		//invoiceInput.setStatus(ApplicationConstants.INVOICE_STATUS_NEW);
		invoiceInput.setInvoiceTypeId(1l);
		
		InvoiceItemDTO invoiceItemInput = new InvoiceItemDTO();
		invoiceItemInput.setId("input-test-item-id-1");
		invoiceItemInput.setAmount(100.00);
		invoiceItemInput.setMaterialId(1l);
		invoiceItemInput.setTaxId(1l);
		invoiceItemInput.setUnitOfMeasurementId(1l);
		
		List<InvoiceItemDTO> invoiceItemInputList = new ArrayList<InvoiceItemDTO>();
		
		invoiceItemInputList.add(invoiceItemInput);
		
		invoiceInput.setInvoiceItems(invoiceItemInputList);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/invoice")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(invoiceInput))
				.accept(MediaType.APPLICATION_JSON)				
				)
		.andExpect(jsonPath("$.invoiceId").exists());		
		
//		mockMvc.perform(MockMvcRequestBuilders.get("/api/invoice/input-test-header-id-1/1").accept(MediaType.APPLICATION_JSON))
//		.andExpect(jsonPath("$.invoiceId").exists())
//		//.andExpect(jsonPath("$",hasSize(1)))
//		.andExpect(jsonPath("$.invoiceId").value("input-test-header-id-1"))
//		.andExpect(jsonPath("$.invoiceItems[0].id").value("input-test-item-id-1"))
//		;
	}
}
