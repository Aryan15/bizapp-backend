package com.coreerp.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.coreerp.ApplicationConstants;
import com.coreerp.ErpAPIApp;
import com.coreerp.dto.PurchaseOrderDTO;
import com.coreerp.dto.PurchaseOrderItemDTO;
import com.coreerp.dto.QuotationDTO;
import com.coreerp.dto.QuotationItemDTO;
import com.coreerp.mapper.QuotationHeaderMapper;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ErpAPIApp.class, TestConfiguration.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PurchaseOrderBackTrackingTest {

	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext wac;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	QuotationHeaderMapper quotationHeaderMapper;
	
	@Before
	public void setup(){
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}
	
	@Test
	@Ignore
	public void verifySavePOLinkedtoQuotationStatusCompleted() throws Exception{
		QuotationDTO quotIn = new QuotationDTO();
		
		quotIn.setId("quotation-test-header-id");
		//quotIn.setStatus(ApplicationConstants.QUOTATION_STATUS_NEW);
		quotIn.setCompanyId(1l);
		quotIn.setFinancialYearId(1l);
		quotIn.setTaxId(1l);
		quotIn.setPartyId(1l);
		quotIn.setTaxAmount(100.00);
		quotIn.setAmount(1500.00);
		quotIn.setTotalTaxableAmount(1500.00);
		quotIn.setGrandTotal(1600.00);
		quotIn.setQuotationNumber("test-number");
		quotIn.setQuotationDate(new Date());
		
		
		QuotationItemDTO quotationItem = new QuotationItemDTO();
		
		quotationItem.setId("quotation-test-item-id");
		quotationItem.setAmount(200.00);
		quotationItem.setPrice(50.00);
		quotationItem.setQuantity(4.00);
		quotationItem.setMaterialId(1l);
		quotationItem.setTaxId(1l);
		quotationItem.setQuotationHeaderId("quotation-test-header-id");
		quotationItem.setUnitOfMeasurementId(1l);
		
		List<QuotationItemDTO> quotationItems = new ArrayList<QuotationItemDTO>();
		
		quotationItems.add(quotationItem);
		
		quotIn.setQuotationItems(quotationItems);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/quotation")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(quotIn))
				.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(jsonPath("$.id").exists())
				.andDo(print());
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/quotation/quotation-test-header-id/1").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists());
		
		PurchaseOrderDTO poIn = new PurchaseOrderDTO();
		
		poIn.setId("po-test-header-id");
		//poIn.setStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
		poIn.setCompanyId(1l);
		poIn.setFinancialYearId(1l);
		poIn.setTaxId(1l);
		poIn.setPartyId(1l);
		poIn.setTaxAmount(100.00);
		poIn.setTotalTaxableAmount(1500.00);
		poIn.setGrandTotal(1600.00);
		poIn.setPurchaseOrderNumber("test-number");
		poIn.setPurchaseOrderDate(new Date());
		poIn.setPurchaseOrderTypeId(2l);
		
//		QuotationHeader qtnHeader = quotationHeaderMapper.dtoToModelMap(quotIn);
//		
//		List<QuotationHeader> qtnHeaderList = new ArrayList<QuotationHeader>();
//		
//		qtnHeaderList.add(qtnHeader);
		
		List<QuotationDTO> qtnHeaderList = new ArrayList<QuotationDTO>();
		qtnHeaderList.add(quotIn);
		
		
		PurchaseOrderItemDTO poItem = new PurchaseOrderItemDTO();
		
		poItem.setId("po-test-item-id");
		poItem.setAmount(200.00);
		poItem.setPrice(50.00);
		poItem.setQuantity(4.0);
		poItem.setMaterialId(1l);
		poItem.setTaxId(1l);
		poItem.setHeaderId("po-test-header-id");
		poItem.setUnitOfMeasurementId(1l);
		poItem.setQuotationHeaderId(quotIn.getId());
		
		List<PurchaseOrderItemDTO> poItems = new ArrayList<PurchaseOrderItemDTO>();
		
		poItems.add(poItem);
		
		poIn.setPurchaseOrderItems(poItems);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/po")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(poIn))
				.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(jsonPath("$.id").exists())
				.andExpect(jsonPath("$.quotationHeaders[0].id").exists())
				.andDo(print());
		
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/po/po-test-header-id/1").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists())
		//.andExpect(jsonPath("$",hasSize(1)))
		.andExpect(jsonPath("$.id").value("po-test-header-id"))
		.andExpect(jsonPath("$.purchaseOrderItems[0].id").value("po-test-item-id"))
		.andExpect(jsonPath("$.status").value(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW))
		.andExpect(jsonPath("$.quotationHeaders[0].id").value("quotation-test-header-id"))
		.andExpect(jsonPath("$.quotationHeaders[0].status").value(ApplicationConstants.QUOTATION_STATUS_COMPLETED))
		.andDo(print());
		
		
		
	}

	
	@Test
	@Ignore
	public void verifySavePOLinkedtoQuotationStatusPartial() throws Exception{
		QuotationDTO quotIn = new QuotationDTO();
		
		quotIn.setId("quotation-test-header-id1");
		//quotIn.setStatus(ApplicationConstants.QUOTATION_STATUS_NEW);
		quotIn.setCompanyId(1l);
		quotIn.setFinancialYearId(1l);
		quotIn.setTaxId(1l);
		quotIn.setPartyId(1l);
		quotIn.setTaxAmount(100.00);
		quotIn.setAmount(1500.00);
		quotIn.setTotalTaxableAmount(1500.00);
		quotIn.setGrandTotal(1600.00);
		quotIn.setQuotationNumber("test-number");
		quotIn.setQuotationDate(new Date());
		
		
		QuotationItemDTO quotationItem = new QuotationItemDTO();
		
		quotationItem.setId("quotation-test-item-id1");
		quotationItem.setAmount(200.00);
		quotationItem.setPrice(50.00);
		quotationItem.setQuantity(4.00); // QUOTATION QUANTITY 4
		quotationItem.setMaterialId(1l);
		quotationItem.setTaxId(1l);
		quotationItem.setQuotationHeaderId("quotation-test-header-id1");
		quotationItem.setUnitOfMeasurementId(1l);
		
		List<QuotationItemDTO> quotationItems = new ArrayList<QuotationItemDTO>();
		
		quotationItems.add(quotationItem);
		
		quotIn.setQuotationItems(quotationItems);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/quotation")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(quotIn))
				.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(jsonPath("$.id").exists())
				.andDo(print());
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/quotation/quotation-test-header-id1/1").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists());
		
		PurchaseOrderDTO poIn = new PurchaseOrderDTO();
		
		poIn.setId("po-test-header-id1");
		//poIn.setStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
		poIn.setCompanyId(1l);
		poIn.setFinancialYearId(1l);
		poIn.setTaxId(1l);
		poIn.setPartyId(1l);
		poIn.setTaxAmount(100.00);
		poIn.setTotalTaxableAmount(1500.00);
		poIn.setGrandTotal(1600.00);
		poIn.setPurchaseOrderNumber("test-number");
		poIn.setPurchaseOrderDate(new Date());
		poIn.setPurchaseOrderTypeId(2l);
		
//		QuotationHeader qtnHeader = quotationHeaderMapper.dtoToModelMap(quotIn);
//		
//		List<QuotationHeader> qtnHeaderList = new ArrayList<QuotationHeader>();
//		
//		qtnHeaderList.add(qtnHeader);
		
		List<QuotationDTO> qtnHeaderList = new ArrayList<QuotationDTO>();
		qtnHeaderList.add(quotIn);
		
		
		PurchaseOrderItemDTO poItem = new PurchaseOrderItemDTO();
		
		poItem.setId("po-test-item-id1");
		poItem.setAmount(200.00);
		poItem.setPrice(50.00);
		poItem.setQuantity(2.0); // PO ITEM QUANTITY IS LESS THAN QUOTATION QUANTITY
		poItem.setMaterialId(1l);
		poItem.setTaxId(1l);
		poItem.setHeaderId("po-test-header-id1");
		poItem.setUnitOfMeasurementId(1l);
		poItem.setQuotationHeaderId(quotIn.getId());
		
		List<PurchaseOrderItemDTO> poItems = new ArrayList<PurchaseOrderItemDTO>();
		
		poItems.add(poItem);
		
		
		poIn.setPurchaseOrderItems(poItems);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/po")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(poIn))
				.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(jsonPath("$.id").exists())
				.andExpect(jsonPath("$.quotationHeaders[0].id").exists())
				.andDo(print());
		
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/po/po-test-header-id1/1").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists())
		//.andExpect(jsonPath("$",hasSize(1)))
		.andExpect(jsonPath("$.id").value("po-test-header-id1"))
		.andExpect(jsonPath("$.purchaseOrderItems[0].id").value("po-test-item-id1"))
		.andExpect(jsonPath("$.status").value(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW))
		.andExpect(jsonPath("$.quotationHeaders[0].id").value("quotation-test-header-id1"))
		.andExpect(jsonPath("$.quotationHeaders[0].status").value(ApplicationConstants.QUOTATION_STATUS_PARTIAL))
		.andDo(print());
		
		
		
	}
	
	//@Test
	public void whenCancelPOThenQuotationWillBeNew() throws Exception {
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/po/cancel/101/1")
		.accept(MediaType.APPLICATION_JSON)
		)
		.andExpect(jsonPath("$.quotationHeaders[0].id").value("101"))
		.andExpect(jsonPath("$.quotationHeaders[0].idVersion").value("2"))
		.andExpect(jsonPath("$.quotationHeaders[0].status").value(ApplicationConstants.QUOTATION_STATUS_NEW))
		.andDo(print())
		;

	}

	//@Test
	public void whenDeletePOThenQuotationWillBeNew() throws Exception {
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/po/delete/101/1")
		.accept(MediaType.APPLICATION_JSON)
		)
		.andExpect(jsonPath("$.quotationHeaders[0].id").value("101"))
		.andExpect(jsonPath("$.quotationHeaders[0].idVersion").value("2"))
		.andExpect(jsonPath("$.quotationHeaders[0].status").value(ApplicationConstants.QUOTATION_STATUS_NEW))
		.andDo(print())
		;

	}
	
	@Test
	@Ignore
	public void verifyCancellingPOLinkedtoQuotationStatusNew() throws Exception{
		QuotationDTO quotIn = new QuotationDTO();
		
		quotIn.setId("quotation-test-header-id2");
		//quotIn.setStatus(ApplicationConstants.QUOTATION_STATUS_NEW);
		quotIn.setCompanyId(1l);
		quotIn.setFinancialYearId(1l);
		quotIn.setTaxId(1l);
		quotIn.setPartyId(1l);
		quotIn.setTaxAmount(100.00);
		quotIn.setAmount(1500.00);
		quotIn.setTotalTaxableAmount(1500.00);
		quotIn.setGrandTotal(1600.00);
		quotIn.setQuotationNumber("test-number");
		quotIn.setQuotationDate(new Date());
		
		
		QuotationItemDTO quotationItem = new QuotationItemDTO();
		
		quotationItem.setId("quotation-test-item-id2");
		quotationItem.setAmount(200.00);
		quotationItem.setPrice(50.00);
		quotationItem.setQuantity(4.00); // QUOTATION QUANTITY 4
		quotationItem.setMaterialId(1l);
		quotationItem.setTaxId(1l);
		quotationItem.setQuotationHeaderId("quotation-test-header-id2");
		quotationItem.setUnitOfMeasurementId(1l);
		
		List<QuotationItemDTO> quotationItems = new ArrayList<QuotationItemDTO>();
		
		quotationItems.add(quotationItem);
		
		quotIn.setQuotationItems(quotationItems);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/quotation")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(quotIn))
				.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(jsonPath("$.id").exists())
				.andDo(print());
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/quotation/quotation-test-header-id2/1").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists());
		
		PurchaseOrderDTO poIn = new PurchaseOrderDTO();
		
		poIn.setId("po-test-header-id2");
		//poIn.setStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
		poIn.setCompanyId(1l);
		poIn.setFinancialYearId(1l);
		poIn.setTaxId(1l);
		poIn.setPartyId(1l);
		poIn.setTaxAmount(100.00);
		poIn.setTotalTaxableAmount(1500.00);
		poIn.setGrandTotal(1600.00);
		poIn.setPurchaseOrderNumber("test-number");
		poIn.setPurchaseOrderDate(new Date());
		poIn.setPurchaseOrderTypeId(2l);
		
//		QuotationHeader qtnHeader = quotationHeaderMapper.dtoToModelMap(quotIn);
//		
//		List<QuotationHeader> qtnHeaderList = new ArrayList<QuotationHeader>();
//		
//		qtnHeaderList.add(qtnHeader);
		
		List<QuotationDTO> qtnHeaderList = new ArrayList<QuotationDTO>();
		qtnHeaderList.add(quotIn);
				
		
		PurchaseOrderItemDTO poItem = new PurchaseOrderItemDTO();
		
		poItem.setId("po-test-item-id2");
		poItem.setAmount(200.00);
		poItem.setPrice(50.00);
		poItem.setQuantity(4.0); // SET PO QUANTITY TO MATCH QUOTATION
		poItem.setMaterialId(1l);
		poItem.setTaxId(1l);
		poItem.setHeaderId("po-test-header-id2");
		poItem.setUnitOfMeasurementId(1l);
		poItem.setQuotationHeaderId(quotIn.getId());
		
		List<PurchaseOrderItemDTO> poItems = new ArrayList<PurchaseOrderItemDTO>();
		
		poItems.add(poItem);
		
		
		poIn.setPurchaseOrderItems(poItems);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/po")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(poIn))
				.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(jsonPath("$.id").exists())
				.andExpect(jsonPath("$.quotationHeaders[0].id").exists())
				.andDo(print());
		
		//Now cancel the PO
		
//		mockMvc.perform(MockMvcRequestBuilders.post("/api/po/cancel/po-test-header-id2/1")
//		.accept(MediaType.APPLICATION_JSON)
//		).andExpect(jsonPath("$.quotationHeaders[0].status").value(ApplicationConstants.QUOTATION_STATUS_NEW));
		
//		mockMvc.perform(MockMvcRequestBuilders.get("/api/po/po-test-header-id2/1").accept(MediaType.APPLICATION_JSON))
//		.andExpect(jsonPath("$.id").exists())
//		//.andExpect(jsonPath("$",hasSize(1)))
//		.andExpect(jsonPath("$.id").value("po-test-header-id2"))
//		.andExpect(jsonPath("$.purchaseOrderItems[0].id").value("po-test-item-id2"))
//		.andExpect(jsonPath("$.status").value(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW))
//		.andExpect(jsonPath("$.quotationHeaders[0].id").value("quotation-test-header-id2"))
//		.andExpect(jsonPath("$.quotationHeaders[0].status").value(ApplicationConstants.QUOTATION_STATUS_COMPLETED))
//		.andDo(print());
		
		//Now cancel the PO
		
//		PurchaseOrderDTO poInCancel = new PurchaseOrderDTO();
//		
//		poInCancel.setId("po-test-header-id2");
//		poInCancel.setVersion(1);
//		poInCancel.setStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_CANCELLED); // SEND CANCEL STATUS
//		poInCancel.setCompanyId(1l);
//		poInCancel.setFinancialYearId(1l);
//		poInCancel.setTaxId(1l);
//		poInCancel.setPartyId(1l);
//		poInCancel.setTaxAmount(100.00);
//		poInCancel.setTotalTaxableAmount(1500.00);
//		poInCancel.setGrandTotal(1600.00);
//		poInCancel.setPurchaseOrderNumber("test-number");
//		poInCancel.setPurchaseOrderDate(new Date());
//		poInCancel.setPurchaseOrderType(ApplicationConstants.PURCHASE_ORDER_TYPE_CUSTOMER);
		
//		
//		List<QuotationDTO> qtnHeaderListc = new ArrayList<QuotationDTO>();
//		qtnHeaderListc.add(quotIn);
//		
//		poInCancel.setQuotationHeaders(qtnHeaderListc);
//		
		
//		PurchaseOrderItemDTO poItemCancel = new PurchaseOrderItemDTO();
//		
//		poItemCancel.setId("po-test-item-id3");
//		poItemCancel.setIdVersion(1);
//		poItemCancel.setAmount(200.00);
//		poItemCancel.setPrice(50.00);
//		poItemCancel.setQuantity(4); 
//		poItemCancel.setMaterialId(1l);
//		poItemCancel.setTaxId(1l);
//		poItemCancel.setHeaderId("po-test-header-id2");
//		poItemCancel.setHeaderIdVersion(1);
//		poItemCancel.setUnitOfMeasurementId(1l);
//		poItemCancel.setQuotationHeaderId(quotIn.getId());
//		poItemCancel.setQuotationHeaderIdVersion(quotIn.getIdVersion());
//		
//		List<PurchaseOrderItemDTO> poItemsC = new ArrayList<PurchaseOrderItemDTO>();
//		
//		poItemsC.add(poItemCancel);
//		
//		
//		poInCancel.setPurchaseOrderItems(poItemsC);
		
//		mockMvc.perform(MockMvcRequestBuilders.post("/api/po/cancel/po-test-header-id2/1")
//				.accept(MediaType.APPLICATION_JSON)
//				)
//				.andExpect(jsonPath("$.id").exists())
//				.andExpect(jsonPath("$.quotationHeaders[0].id").exists())
//				.andExpect(jsonPath("$.quotationHeaders[0].status").value(ApplicationConstants.QUOTATION_STATUS_NEW))
//				.andDo(print());
		
		
//		mockMvc.perform(MockMvcRequestBuilders.get("/api/po/po-test-header-id2/1").accept(MediaType.APPLICATION_JSON))
//		.andExpect(jsonPath("$.id").exists())
//		//.andExpect(jsonPath("$",hasSize(1)))
//		.andExpect(jsonPath("$.id").value("po-test-header-id2"))
//		.andExpect(jsonPath("$.purchaseOrderItems[0].id").value("po-test-item-id2"))
//		.andExpect(jsonPath("$.status").value(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW))
//		.andExpect(jsonPath("$.quotationHeaders[0].id").value("quotation-test-header-id2"))
//		.andExpect(jsonPath("$.quotationHeaders[0].status").value(ApplicationConstants.QUOTATION_STATUS_NEW))
//		.andDo(print());
	}
}
