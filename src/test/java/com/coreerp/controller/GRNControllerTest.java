package com.coreerp.controller;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.coreerp.ErpAPIApp;
import com.coreerp.dto.GRNHeaderDTO;
import com.coreerp.dto.GRNItemDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={ErpAPIApp.class,TestConfiguration.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestPropertySource(
		  locations = "classpath:application-integrationtest.properties")
public class GRNControllerTest {

	final static Logger log=LogManager.getLogger(GRNControllerTest.class);
	
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext wac;
	
	@Autowired
	ObjectMapper  objectMapper;
	
	@Before
	public void setup(){
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}
	
	@Test
	@Ignore
	public void verifyAllGRNs() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/grn").accept(MediaType.APPLICATION_JSON))
		//.andExpect(jsonPath("$.headerId").exists());
		.andExpect(jsonPath("$",hasSize(2)))
		.andExpect(jsonPath("$.[*].grnId", containsInAnyOrder("101","102")));
	}
	
	@Test
	@Ignore
	public void verifySaveGRN() throws Exception {
		
		
		GRNHeaderDTO grnInput = new GRNHeaderDTO();
		grnInput.setId("input-test-header-id-1");
		grnInput.setGrnId(1l);
		grnInput.setGrnNumber("GRN-1");
		
		grnInput.setPurchaseOrderHeaderId("1");
		grnInput.setFinancialYearId(1l);
		//grnInput.setStatus("NEW");
		
		GRNItemDTO grnItemInput = new GRNItemDTO();
		grnItemInput.setId("input-test-item-id-1");
		grnItemInput.setAmount(100.00);
		grnItemInput.setMaterialId(1l);
		grnItemInput.setDeliveryChallanQuantity(2.00);
		grnItemInput.setMaterialSpecification("Good");
		
		List<GRNItemDTO> grnItemInputList = new ArrayList<GRNItemDTO>();
		
		grnItemInputList.add(grnItemInput);
		
		grnInput.setGrnItems(grnItemInputList);

		mockMvc.perform(MockMvcRequestBuilders.post("/api/grn")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(grnItemInput))
				.accept(MediaType.APPLICATION_JSON)				
				)
				.andExpect(jsonPath("$.invoiceId").exists())
				.andExpect(jsonPath("$.invoiceId").value("input-test-header-id-1"))
				.andExpect(jsonPath("$.invoiceItems[0].id").value("input-test-item-id-1"))
				.andDo(print());
	}

	
}
