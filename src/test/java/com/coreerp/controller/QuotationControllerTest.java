package com.coreerp.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.coreerp.ApplicationConstants;
import com.coreerp.ErpAPIApp;
import com.coreerp.dto.QuotationDTO;
import com.coreerp.dto.QuotationItemDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ErpAPIApp.class, TestConfiguration.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class QuotationControllerTest {

	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext wac;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Before
	public void setup(){
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}
	
	@Test
	@Ignore
	public void verifyGetQuotation() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.get("/api/quotation/101/1").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists())
		//.andExpect(jsonPath("$",hasSize(1)))
		.andExpect(jsonPath("$.id").value("101"));
	}
	
	@Test
	@Ignore
	public void verifySaveAndGetQuotation() throws Exception {
		QuotationDTO quotIn = new QuotationDTO();
		
		quotIn.setId("quotation-test-header-id");
		//quotIn.setStatus(ApplicationConstants.QUOTATION_STATUS_NEW);
		quotIn.setCompanyId(1l);
		quotIn.setFinancialYearId(1l);
		quotIn.setTaxId(1l);
		quotIn.setPartyId(1l);
		quotIn.setTaxAmount(100.00);
		quotIn.setTotalTaxableAmount(1500.00);
		quotIn.setGrandTotal(1600.00);
		quotIn.setQuotationNumber("test-number");
		quotIn.setQuotationDate(new Date());
		
		
		QuotationItemDTO quotationItem = new QuotationItemDTO();
		
		quotationItem.setId("quotation-test-item-id");
		quotationItem.setAmount(200.00);
		quotationItem.setPrice(50.00);
		quotationItem.setQuantity(4.00);
		quotationItem.setMaterialId(1l);
		quotationItem.setTaxId(1l);
		quotationItem.setQuotationHeaderId("quotation-test-header-id");
		quotationItem.setUnitOfMeasurementId(1l);
		
		List<QuotationItemDTO> quotationItems = new ArrayList<QuotationItemDTO>();
		
		quotationItems.add(quotationItem);
		
		quotIn.setQuotationItems(quotationItems);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/quotation")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(quotIn))
				.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(jsonPath("$.id").exists())
				.andDo(print());
		
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/quotation/quotation-test-header-id/1").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists())
		//.andExpect(jsonPath("$",hasSize(1)))
		.andExpect(jsonPath("$.id").value("quotation-test-header-id"))
		.andExpect(jsonPath("$.quotationItems[0].id").value("quotation-test-item-id"))
		;
	}
}
