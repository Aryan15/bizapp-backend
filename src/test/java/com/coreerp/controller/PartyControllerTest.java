package com.coreerp.controller;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.coreerp.ErpAPIApp;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ErpAPIApp.class, TestConfiguration.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PartyControllerTest {

	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext wac;
	
	@Before
	public void setup(){
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}
	
	
	@Test
	@Ignore
	public void verifyAllCustomers() throws Exception{
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/customer").accept(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$",hasSize(2)))
				//.andExpect(jsonPath("$[0].name").value("Test Customer"))
				.andExpect(jsonPath("$[*].name", containsInAnyOrder("Test Customer","Test Customer2")))
				;
		
	}
	
	@Test
	@Ignore
	public void verifySaveCustomer() throws Exception{
		
		String customerJson = "{ "
				+ "                  \"id\": null,"
				+ "                  \"name\": \"Test Customer\","
				+ "                  \"address\": \"Test Address\","
				+ "                  \"countryId\": 1,"
        	    + "                  \"stateId\": 1,"
				+ "                  \"cityId\": null,"
				+ "                  \"areaId\": null,"
				+ "                  \"pinCode\": null,"
				+ "                  \"primaryTelephone\": null,"
				+ "                  \"secondaryTelephone\": null,"
				+ "                  \"primaryMobile\": null,"
				+ "                  \"secondaryMobile\": null,"
				+ "                  \"email\": \"test@test.com\","
				+ "                  \"contactPersonName\": null,"
				+ "                  \"contactPersonNumber\": null,"
				+ "                  \"webSite\": null,"
				+ "                  \"partyTypeId\": 1,"
				+ "                  \"deleted\": \"N\","
				+ "                  \"billAddress\": null,"
				+ "                  \"panNumber\": null,"
				+ "                  \"gstNumber\": null"
				+ " }";
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/customer")
						.contentType(MediaType.APPLICATION_JSON)
						.content(customerJson)
						.accept(MediaType.APPLICATION_JSON)
						)
						.andExpect(jsonPath("$.id").exists())
						.andExpect(jsonPath("$.name").value("Test Customer"))
						.andExpect(jsonPath("$.countryId").value("1"));
	}
	
	@Test
	@Ignore
	public void verifyGetParty() throws Exception{
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/customer/1").accept(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.id").exists())
			.andExpect(jsonPath("$.id").value("1"))
			.andExpect(jsonPath("$.name").value("Test Customer"));
		
		
	}
	
}
