package com.coreerp.serviceimpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.CompanyRepository;
import com.coreerp.dao.CountryRepository;
import com.coreerp.dao.DeliveryChallanHeaderRepository;
import com.coreerp.dao.FinancialYearRepository;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.dao.MaterialTypeRepository;
import com.coreerp.dao.PartyRepository;
import com.coreerp.dao.PartyTypeRepository;
import com.coreerp.dao.StateRepository;
import com.coreerp.dao.TaxRepository;
import com.coreerp.dao.TaxTypeRepository;
import com.coreerp.dao.UnitOfMeasurementRepository;
import com.coreerp.model.Company;
import com.coreerp.model.Country;
import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.DeliveryChallanItem;
import com.coreerp.model.FinancialYear;
import com.coreerp.model.Material;
import com.coreerp.model.MaterialType;
import com.coreerp.model.Party;
import com.coreerp.model.PartyType;
import com.coreerp.model.State;
import com.coreerp.model.Tax;
import com.coreerp.model.TaxType;
import com.coreerp.model.TransactionPK;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DeliveryChallanServiceImplTest {

	@Autowired
    private TestEntityManager entityManager;
	
	@Autowired
	DeliveryChallanHeaderRepository deliveryChallanHeaderRepository;
	
	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	TaxTypeRepository taxTypeRepository;
	
	@Autowired
	TaxRepository taxRepository;
	
	@Autowired
	FinancialYearRepository financialYearRepository;
	
	@Autowired
	PartyTypeRepository partyTypeRepository;
	
	@Autowired
	PartyRepository partyRepository;
	
	@Autowired 
	CountryRepository countryRepository;
	
	@Autowired
	StateRepository stateRepository;
	
	@Autowired
	MaterialRepository materialRepository;
	
	@Autowired
	UnitOfMeasurementRepository unitOfMeasurementRepository;
	
	@Autowired
	MaterialTypeRepository materialTypeRepository;
	
	@Test
	@Ignore
	public void saveDeliveryChallan(){
		Country country = new Country();
		country.setName("India");
		country.setDeleted("N");
		
		entityManager.persist(country);
		entityManager.flush();
		
		State state = new State();
		state.setName("Karnataka");
		state.setCountry(countryRepository.getOne(1l));
		
		entityManager.persist(state);
		entityManager.flush();
		
		
		Company company = new Company();
		company.setName("test company");
		company.setDeleted("N");
		company.setCountry(countryRepository.getOne(1l));
		company.setState(stateRepository.getOne(1l));
		
		entityManager.persist(company);
		entityManager.flush();
		
		TaxType taxType = new TaxType();
		taxType.setName("GST");
		taxType.setDeleted("N");
		
		
		entityManager.persist(taxType);
		entityManager.flush();
		
		Tax tax = new Tax();
		tax.setName("test tax");
		tax.setTaxType(taxTypeRepository.getOne(1l));
		tax.setDeleted("N");
		tax.setRate(18.00);
		
		entityManager.persist(tax);
		entityManager.flush();
		
		FinancialYear financialYear = new FinancialYear();
		financialYear.setFinancialYear("2017-2018");	
		
		entityManager.persist(financialYear);
		entityManager.flush();
		
		PartyType partyType = new PartyType();
		partyType.setName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
		
		entityManager.persist(partyType);
		entityManager.flush();
		
		Party party = new Party();
		party.setName("test party");
		party.setPartyType(partyTypeRepository.getOne(1l));
		party.setState(stateRepository.getOne(1l));
		party.setDeleted("N");
		party.setEmail("test@test.com");
		party.setCountry(countryRepository.getOne(1l));
		party.setAddress("Test address");
		
		entityManager.persist(party);
		entityManager.flush();
		
		MaterialType materialType = new MaterialType();
		materialType.setDeleted("N");
		materialType.setName("Raw Material");
		
		entityManager.persist(materialType);
		entityManager.flush();
		
		Material material = new Material();
		material.setName("Test Material Name");
		material.setPrice(100.00);
		material.setDeleted("N");
		material.setHsnCode("TEST-HSN");
		material.setCompany(companyRepository.getOne(1l));
		material.setUnitOfMeasurement(unitOfMeasurementRepository.getOne(1l));
		material.setMaterialType(materialTypeRepository.getOne(1l));
		material.setTax(taxRepository.getOne(1l));
		
		entityManager.persist(material);
		entityManager.flush();
		
		DeliveryChallanHeader deliveryChallanHeader = new DeliveryChallanHeader();
		
		
		deliveryChallanHeader.setId("dc-test-id");
		deliveryChallanHeader.setCompany(companyRepository.getOne(1l));
		deliveryChallanHeader.setDeliveryChallanDate(new Date());
		deliveryChallanHeader.setParty(partyRepository.getOne(1l));
		//deliveryChallanHeader.setStatus(ApplicationConstants.DC_STATUS_NEW);
		deliveryChallanHeader.setFinancialYear(financialYearRepository.getOne(1l));
		
		
		DeliveryChallanItem deliveryChallanItem = new DeliveryChallanItem();
		
		deliveryChallanItem.setId("dc-test-item-id");
		deliveryChallanItem.setMaterial(materialRepository.getOne(1l));
		deliveryChallanItem.setUnitOfMeasurement(unitOfMeasurementRepository.getOne(1l));
		deliveryChallanItem.setTax(taxRepository.getOne(1l));
		deliveryChallanItem.setDeliveryChallanHeader(deliveryChallanHeader);
		List<DeliveryChallanItem> deliveryChallanItems = new ArrayList<DeliveryChallanItem>();
		deliveryChallanItems.add(deliveryChallanItem);

		
		deliveryChallanHeader.setDeliveryChallanItems(deliveryChallanItems);
		
		entityManager.merge(deliveryChallanHeader);
		entityManager.flush();
		
		DeliveryChallanHeader deliveryChallanHeaderAssert = deliveryChallanHeaderRepository.getOne("dc-test-id");
		
		assertEquals(ApplicationConstants.DC_STATUS_NEW, deliveryChallanHeaderAssert.getStatus());
		
		//entityManager.remove(deliveryChallanHeader);
	}
	

	
}
