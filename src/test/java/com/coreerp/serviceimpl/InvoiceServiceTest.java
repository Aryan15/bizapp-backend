package com.coreerp.serviceimpl;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import com.coreerp.dao.InvoiceHeaderRepository;
import com.coreerp.dao.InvoiceItemRepository;
import com.coreerp.dto.InvoiceDTO;
import com.coreerp.dto.InvoiceItemDTO;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.InvoiceItem;

@RunWith(SpringRunner.class)
//@DataJpaTest
//@SpringBootTest
//@ContextConfiguration
public class InvoiceServiceTest  {
	
	final static Logger log = LogManager.getLogger(InvoiceServiceTest.class);

//	@TestConfiguration
//	static class AccountServiceTestContextConfiguration {
//		@Bean
//		public InvoiceService invoiceService() {
//			return new InvoiceServiceImpl();
//		}
//
//
//
//	}
	


	
	@Mock
	private InvoiceHeaderRepository invoiceHeaderRepository;

	@Mock
	private InvoiceItemRepository invoiceItemRepository;

	@InjectMocks
	private InvoiceServiceImpl invoiceService;
	
	@Before
	public void setup(){

		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	@Ignore
	public void saveInvoice(){
		
		InvoiceDTO invoiceDTOIn = new InvoiceDTO();
		invoiceDTOIn.setAddress("test address");
		
		List<InvoiceItemDTO> invoiceItemDTOlist = new ArrayList<InvoiceItemDTO>(); 
		
		InvoiceItemDTO invoiceItemDTO = new InvoiceItemDTO();
		invoiceItemDTO.setId("test-item");

		invoiceItemDTOlist.add(invoiceItemDTO);
		
		invoiceDTOIn.setInvoiceItems(invoiceItemDTOlist);
		
		InvoiceHeader invoiceHeader = new InvoiceHeader();
		
		invoiceHeader.setPartyAddress(invoiceDTOIn.getAddress());
		
		InvoiceItem invoiceItem = new InvoiceItem();
		invoiceItem.setId(invoiceItemDTO.getId());

		
		List<InvoiceItem> invoiceItems = new ArrayList<InvoiceItem>();
		invoiceItems.add(invoiceItem);
		
		invoiceHeader.setInvoiceItems(invoiceItems);
				
		
		when(invoiceHeaderRepository.save(invoiceHeader)).thenReturn(invoiceHeader);
		//when(invoiceItemRepository.save(invoiceItem)).thenReturn(invoiceItem);
		
		
		//InvoiceDTO result = invoiceService.saveInvoice(invoiceDTOIn);
//		TransactionPK invoiceHeaderPK = new TransactionPK();
//		invoiceHeaderPK.setId("test");
//		invoiceHeaderPK.setIdVersion(1);
		
//		InvoiceHeader invoiceHeaderAssert = invoiceService.getInvoiceHeaderByHeaderId(invoiceHeaderPK);
		
		//assertEquals("test", result.getHeaderId());
		//log.info("invoiceHeaderAssert: "+invoiceHeaderAssert);
	}

}
