package com.coreerp.serviceimpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.coreerp.AbstractTest;
import com.coreerp.ApplicationConstants;
import com.coreerp.dao.CompanyRepository;
import com.coreerp.dao.CountryRepository;
import com.coreerp.dao.FinancialYearRepository;
import com.coreerp.dao.InvoiceHeaderRepository;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.dao.MaterialTypeRepository;
import com.coreerp.dao.PartyRepository;
import com.coreerp.dao.PartyTypeRepository;
import com.coreerp.dao.StateRepository;
import com.coreerp.dao.TaxRepository;
import com.coreerp.dao.TaxTypeRepository;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dao.UnitOfMeasurementRepository;
import com.coreerp.model.Company;
import com.coreerp.model.Country;
import com.coreerp.model.FinancialYear;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.InvoiceItem;
import com.coreerp.model.Material;
import com.coreerp.model.MaterialType;
import com.coreerp.model.Party;
import com.coreerp.model.PartyType;
import com.coreerp.model.State;
import com.coreerp.model.Tax;
import com.coreerp.model.TaxType;
import com.coreerp.model.TransactionType;

//@RunWith(SpringRunner.class)
//@DataJpaTest
public class InvoiceServiceImplTest extends AbstractTest {
	

	
	@Autowired
    private TestEntityManager entityManager;
	
	@Autowired
	private InvoiceHeaderRepository invoiceHeaderRepository;
	
	@Autowired
	TaxTypeRepository taxTypeRepository;
	
	@Autowired
	TaxRepository taxRepository;

	@Autowired 
	CountryRepository countryRepository;
	
	@Autowired
	StateRepository stateRepository;
	
	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	FinancialYearRepository financialYearRepository;
	
	@Autowired
	PartyTypeRepository partyTypeRepository;
	
	@Autowired
	PartyRepository partyRepository;
	
	
	@Autowired
	MaterialTypeRepository materialTypeRepository ;
	
	@Autowired
	UnitOfMeasurementRepository unitOfMeasurementRepository;
	
	@Autowired
	MaterialRepository materialRepository;
	
	@Autowired
	TransactionTypeRepository transactionTypeRepository;
	
	@Test
	@Ignore
	public void saveInvoiceHeader(){
		
		Country country = new Country();
		country.setName("India");
		country.setDeleted("N");
		
		entityManager.persist(country);
		entityManager.flush();
		
		State state = new State();
		state.setName("Karnataka");
		state.setCountry(countryRepository.getOne(1l));
		
		entityManager.persist(state);
		entityManager.flush();
		
		Company company = new Company();
		company.setName("test company");
		company.setDeleted("N");
		company.setCountry(countryRepository.getOne(1l));
		company.setState(stateRepository.getOne(1l));
		
		entityManager.persist(company);
		entityManager.flush();

		TaxType taxType = new TaxType();
		taxType.setName("GST");
		taxType.setDeleted("N");
		
		
		entityManager.persist(taxType);
		entityManager.flush();
		
		Tax tax = new Tax();
		tax.setName("test tax");
		tax.setTaxType(taxTypeRepository.getOne(1l));
		tax.setDeleted("N");
		tax.setRate(18.00);
		
		entityManager.persist(tax);
		entityManager.flush();
		
		FinancialYear financialYear = new FinancialYear();
		financialYear.setFinancialYear("2017-2018");	
		
		entityManager.persist(financialYear);
		entityManager.flush();
		
		
		PartyType partyType = new PartyType();
		partyType.setName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
		
		entityManager.persist(partyType);
		entityManager.flush();
		
		Party party = new Party();
		party.setName("test party");
		party.setPartyType(partyTypeRepository.getOne(1l));
		party.setState(stateRepository.getOne(1l));
		party.setDeleted("N");
		party.setEmail("test@test.com");
		party.setCountry(countryRepository.getOne(1l));
		party.setAddress("Test address");
		
		entityManager.persist(party);
		entityManager.flush();
		
		
		MaterialType materialType = new  MaterialType();
		
		materialType.setDeleted("N");
		materialType.setName("test material type");
		
		entityManager.persist(materialType);
		entityManager.flush();
		
		Material material = new Material();
		material.setDeleted("N");
		material.setName("test material");
		material.setPrice(100.50);
		material.setMaterialType(materialTypeRepository.getOne(1l));
		material.setTax(taxRepository.getOne(1l));
		material.setUnitOfMeasurement(unitOfMeasurementRepository.getOne(1l));
		material.setCompany(companyRepository.getOne(1l));
		
		entityManager.persist(material);
		entityManager.flush();
		
		TransactionType transactionType = new TransactionType();
		transactionType.setName("Customer Invoice");
		
		entityManager.persist(transactionType);
		
		InvoiceHeader invoice = new InvoiceHeader();
		invoice.setId("test");
		invoice.setInvId(1l);
		invoice.setPartyAddress("Test Address");
		//invoice.setStatus(ApplicationConstants.INVOICE_STATUS_NEW);
		invoice.setCompany(companyRepository.getOne(1l));
		invoice.setFinancialYear(financialYearRepository.getOne(1l));
		invoice.setInvoiceType(transactionTypeRepository.findByName(ApplicationConstants.INVOICE_TYPE_CUSTOMER));
		invoice.setTax(taxRepository.getOne(1l));
		invoice.setParty(partyRepository.getOne(1l));
		
		InvoiceItem invoiceItem = new InvoiceItem();
		
		invoiceItem.setId("test-item");
		invoiceItem.setMaterial(materialRepository.getOne(1l));
		invoiceItem.setTax(taxRepository.getOne(1l));
		invoiceItem.setUnitOfMeasurement(unitOfMeasurementRepository.getOne(1l));
		invoiceItem.setPrice(100.50);
		invoiceItem.setQuantity(2.0);
		invoiceItem.setInvoiceHeader(invoice);
		
		List<InvoiceItem> invoiceItems = new ArrayList<InvoiceItem>();
		
		invoiceItems.add(invoiceItem);
		
		invoice.setInvoiceItems(invoiceItems);
		
		
		entityManager.merge(invoice);
		entityManager.flush();
		
		InvoiceHeader invoiceHeaderAssert = invoiceHeaderRepository.findByHeaderId("test");
		
		assertEquals("Test Address", invoiceHeaderAssert.getPartyAddress());
		
		//entityManager.remove(invoice);
		
	}
	
	@Test
	@Ignore
	public void saveAndFindByInvoiceNumber(){
		
		Country country = new Country();
		country.setName("India");
		country.setDeleted("N");
		
		entityManager.persist(country);
		entityManager.flush();
		
		State state = new State();
		state.setName("Karnataka");
		state.setCountry(countryRepository.getOne(1l));
		
		entityManager.persist(state);
		entityManager.flush();
		
		Company company = new Company();
		company.setName("test company");
		company.setDeleted("N");
		company.setCountry(countryRepository.getOne(1l));
		company.setState(stateRepository.getOne(1l));
		
		entityManager.persist(company);
		entityManager.flush();

		TaxType taxType = new TaxType();
		taxType.setName("GST");
		taxType.setDeleted("N");
		
		
		entityManager.persist(taxType);
		entityManager.flush();
		
		Tax tax = new Tax();
		tax.setName("test tax");
		tax.setTaxType(taxTypeRepository.getOne(1l));
		tax.setDeleted("N");
		tax.setRate(18.00);
		
		entityManager.persist(tax);
		entityManager.flush();
		
		FinancialYear financialYear = new FinancialYear();
		financialYear.setFinancialYear("2017-2018");	
		
		entityManager.persist(financialYear);
		entityManager.flush();
		
		
		PartyType partyType = new PartyType();
		partyType.setName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
		
		entityManager.persist(partyType);
		entityManager.flush();
		
		Party party = new Party();
		party.setName("test party");
		party.setPartyType(partyTypeRepository.getOne(1l));
		party.setState(stateRepository.getOne(1l));
		party.setDeleted("N");
		party.setEmail("test@test.com");
		party.setCountry(countryRepository.getOne(1l));
		party.setAddress("Test address");
		
		entityManager.persist(party);
		entityManager.flush();

		MaterialType materialType = new  MaterialType();
		
		materialType.setDeleted("N");
		materialType.setName("test material type");
		
		entityManager.persist(materialType);
		entityManager.flush();
		
		Material material = new Material();
		material.setDeleted("N");
		material.setName("test material");
		material.setPrice(100.50);
		material.setMaterialType(materialTypeRepository.getOne(1l));
		material.setTax(taxRepository.getOne(1l));
		material.setUnitOfMeasurement(unitOfMeasurementRepository.getOne(1l));
		material.setCompany(companyRepository.getOne(1l));
		
		entityManager.persist(material);
		entityManager.flush();

		TransactionType transactionType = new TransactionType();
		transactionType.setName("Customer Invoice");
		
		entityManager.persist(transactionType);
		
		InvoiceHeader invoice = new InvoiceHeader();
		invoice.setInvId(1l);
		invoice.setId("test2");
		invoice.setPartyAddress("Test Address2");
		invoice.setInvoiceNumber("test2");
		//invoice.setStatus(ApplicationConstants.INVOICE_STATUS_NEW);
		invoice.setCompany(companyRepository.getOne(1l));
		invoice.setFinancialYear(financialYearRepository.getOne(1l));
		invoice.setInvoiceType(transactionTypeRepository.findByName(ApplicationConstants.INVOICE_TYPE_CUSTOMER));
		invoice.setTax(taxRepository.getOne(1l));
		invoice.setParty(partyRepository.getOne(1l));

		InvoiceItem invoiceItem = new InvoiceItem();
		
		invoiceItem.setId("test-item");
		invoiceItem.setMaterial(materialRepository.getOne(1l));
		invoiceItem.setTax(taxRepository.getOne(1l));
		invoiceItem.setUnitOfMeasurement(unitOfMeasurementRepository.getOne(1l));
		invoiceItem.setPrice(100.50);
		invoiceItem.setQuantity(2.0);
		invoiceItem.setInvoiceHeader(invoice);
		
		List<InvoiceItem> invoiceItems = new ArrayList<InvoiceItem>();
		
		invoiceItems.add(invoiceItem);
		
		invoice.setInvoiceItems(invoiceItems);
		
		
		entityManager.merge(invoice);
		entityManager.flush();
		
		InvoiceHeader invoiceHeaderAssert = invoiceHeaderRepository.findByInvoiceNumber("test2");
		
		assertEquals("Test Address2", invoiceHeaderAssert.getPartyAddress());
		
		//entityManager.remove(invoice);
	}
}
