package com.coreerp.serviceimpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.CompanyRepository;
import com.coreerp.dao.CountryRepository;
import com.coreerp.dao.FinancialYearRepository;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.dao.MaterialTypeRepository;
import com.coreerp.dao.PartyRepository;
import com.coreerp.dao.PartyTypeRepository;
import com.coreerp.dao.PurchaseOrderHeaderRepository;
import com.coreerp.dao.StateRepository;
import com.coreerp.dao.TaxRepository;
import com.coreerp.dao.TaxTypeRepository;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dao.UnitOfMeasurementRepository;
import com.coreerp.model.Company;
import com.coreerp.model.Country;
import com.coreerp.model.FinancialYear;
import com.coreerp.model.Material;
import com.coreerp.model.MaterialType;
import com.coreerp.model.Party;
import com.coreerp.model.PartyType;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.model.PurchaseOrderItem;
import com.coreerp.model.State;
import com.coreerp.model.Tax;
import com.coreerp.model.TaxType;
import com.coreerp.model.TransactionType;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PurchaseOrderServiceImplTest {

	@Autowired
    private TestEntityManager entityManager;
	
	@Autowired
	PurchaseOrderHeaderRepository purchaseOrderHeaderRepository; 
	
	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	TaxTypeRepository taxTypeRepository;
	
	@Autowired
	TaxRepository taxRepository;
	
	@Autowired
	FinancialYearRepository financialYearRepository;
	
	@Autowired
	PartyTypeRepository partyTypeRepository;
	
	@Autowired
	PartyRepository partyRepository;
	
	@Autowired 
	CountryRepository countryRepository;
	
	@Autowired
	StateRepository stateRepository;
	
	@Autowired
	MaterialTypeRepository materialTypeRepository ;
	
	@Autowired
	UnitOfMeasurementRepository unitOfMeasurementRepository;
	
	@Autowired
	MaterialRepository materialRepository;
	
	@Autowired
	TransactionTypeRepository transactionTypeRepository;
	
	@Test
	@Ignore
	public void savePurchaseHeader(){
		
		Country country = new Country();
		country.setName("India");
		country.setDeleted("N");
		
		entityManager.persist(country);
		entityManager.flush();
		
		State state = new State();
		state.setName("Karnataka");
		state.setCountry(countryRepository.getOne(1l));
		
		entityManager.persist(state);
		entityManager.flush();
		
		
		Company company = new Company();
		company.setName("test company");
		company.setDeleted("N");
		company.setCountry(countryRepository.getOne(1l));
		company.setState(stateRepository.getOne(1l));
		
		entityManager.persist(company);
		entityManager.flush();
		
		TaxType taxType = new TaxType();
		taxType.setName("GST");
		taxType.setDeleted("N");
		
		
		entityManager.persist(taxType);
		entityManager.flush();
		
		Tax tax = new Tax();
		tax.setName("test tax");
		tax.setTaxType(taxTypeRepository.getOne(1l));
		tax.setDeleted("N");
		tax.setRate(18.00);
		
		entityManager.persist(tax);
		entityManager.flush();
		
		FinancialYear financialYear = new FinancialYear();
		financialYear.setFinancialYear("2017-2018");	
		
		entityManager.persist(financialYear);
		entityManager.flush();
		
		PartyType partyType = new PartyType();
		partyType.setName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
		
		entityManager.persist(partyType);
		entityManager.flush();
		
		Party party = new Party();
		party.setName("test party");
		party.setPartyType(partyTypeRepository.getOne(1l));
		party.setState(stateRepository.getOne(1l));
		party.setDeleted("N");
		party.setEmail("test@test.com");
		party.setCountry(countryRepository.getOne(1l));
		party.setAddress("Test address");
		
		entityManager.persist(party);
		entityManager.flush();
		
		MaterialType materialType = new  MaterialType();
		
		materialType.setDeleted("N");
		materialType.setName("test material type");
		
		entityManager.persist(materialType);
		entityManager.flush();
		
		Material material = new Material();
		material.setDeleted("N");
		material.setName("test material");
		material.setPrice(100.50);
		material.setMaterialType(materialTypeRepository.getOne(1l));
		material.setTax(taxRepository.getOne(1l));
		material.setUnitOfMeasurement(unitOfMeasurementRepository.getOne(1l));
		material.setCompany(companyRepository.getOne(1l));
		
		entityManager.persist(material);
		entityManager.flush();
		
		TransactionType tranType = new TransactionType();
		tranType.setName(ApplicationConstants.PURCHASE_ORDER_TYPE_CUSTOMER);
		
		entityManager.persist(tranType);
		entityManager.flush();
		
		PurchaseOrderHeader purchaseOrderHeader = new PurchaseOrderHeader();
		purchaseOrderHeader.setId("po-test-id");
		purchaseOrderHeader.setCgstTaxAmount(100.00);
		purchaseOrderHeader.setCompany(companyRepository.getOne(1l));
		purchaseOrderHeader.setTax(taxRepository.getOne(1l));
		purchaseOrderHeader.setFinancialYear(financialYearRepository.getOne(1l));
		purchaseOrderHeader.setParty(partyRepository.getOne(1l));
		purchaseOrderHeader.setGrandTotal(100.00);
		purchaseOrderHeader.setTaxAmount(50.00);
		purchaseOrderHeader.setNetAmount(50.00);
		purchaseOrderHeader.setPurchaseOrderType(transactionTypeRepository.findByName(ApplicationConstants.PURCHASE_ORDER_TYPE_CUSTOMER));
		purchaseOrderHeader.setPurchaseOrderNumber("po-test-101");
		//purchaseOrderHeader.setStatus(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW);
		purchaseOrderHeader.setPurchaseOrderDate(new Date());
		//purchaseOrderHeader.setPurchaseOrderType(purchaseOrderType);
		
		PurchaseOrderItem purchaseOrderItem = new PurchaseOrderItem();
		
		
		
		purchaseOrderItem.setId("test-item");
		//purchaseOrderItem.setMaterial(materialRepository.getOne(1011l));
		purchaseOrderItem.setQuantity(100.0);
		purchaseOrderItem.setPrice(100.50);
		purchaseOrderItem.setPurchaseOrderHeader(purchaseOrderHeader);
		purchaseOrderItem.setUnitOfMeasurement(unitOfMeasurementRepository.getOne(1l));
		purchaseOrderItem.setMaterial(materialRepository.getOne(1l));
		purchaseOrderItem.setTax(taxRepository.getOne(1l));
		
		List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<PurchaseOrderItem>();
		
		purchaseOrderItems.add(purchaseOrderItem);
		
		purchaseOrderHeader.setPurchaseOrderItems(purchaseOrderItems);
		
		
		entityManager.merge(purchaseOrderHeader);
		entityManager.flush();
		
		
		
		PurchaseOrderHeader purchaseOrderHeaderAssert = purchaseOrderHeaderRepository.getOne(purchaseOrderHeader.getId());
		
		assertEquals("po-test-id", purchaseOrderHeaderAssert.getId());
		
		
		
	}
}
