package com.coreerp.serviceimpl;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.coreerp.dao.PayableReceivableHeaderRepository;
import com.coreerp.service.PayableReceivableService;

@RunWith(SpringRunner.class)
public class PayableReceivableServiceImplTest {

	
	@TestConfiguration
	static class PayableReceivableServiceImplTestContextConfig{
		@Bean
		public PayableReceivableService payableReceivableService(){
			return new PayableReceivableServiceImpl();
		}
	}
	
	
	@MockBean
	private PayableReceivableService payableReceivableService;
	
	
	@MockBean
	private PayableReceivableHeaderRepository payableReceivableHeaderRepository;
	
	
	
	@Before
	public void setup(){
		
	}
	
	@Test
	public void test(){
		
	}
}
