package com.coreerp.serviceimpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.coreerp.AbstractTest;
import com.coreerp.ApplicationConstants;
import com.coreerp.dao.CompanyRepository;
import com.coreerp.dao.CountryRepository;
import com.coreerp.dao.FinancialYearRepository;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.dao.MaterialTypeRepository;
import com.coreerp.dao.PartyRepository;
import com.coreerp.dao.PartyTypeRepository;
import com.coreerp.dao.QuotationHeaderRepository;
import com.coreerp.dao.StateRepository;
import com.coreerp.dao.TaxRepository;
import com.coreerp.dao.TaxTypeRepository;
import com.coreerp.dao.UnitOfMeasurementRepository;
import com.coreerp.model.Company;
import com.coreerp.model.Country;
import com.coreerp.model.FinancialYear;
import com.coreerp.model.Material;
import com.coreerp.model.MaterialType;
import com.coreerp.model.Party;
import com.coreerp.model.PartyType;
import com.coreerp.model.QuotationHeader;
import com.coreerp.model.QuotationItem;
import com.coreerp.model.State;
import com.coreerp.model.Tax;
import com.coreerp.model.TaxType;

//@RunWith(SpringRunner.class)
//@DataJpaTest
public class QuotationHeaderServiceImplTest extends AbstractTest{
	
	@Autowired
    private TestEntityManager entityManager;
	
	//@Autowired
	//private QuotationHeaderService quotationHeaderService;
	
	@Autowired
	private QuotationHeaderRepository quotaionHeaderRepository;
	
	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	TaxTypeRepository taxTypeRepository;
	
	@Autowired
	TaxRepository taxRepository;
	
	@Autowired
	FinancialYearRepository financialYearRepository;
	
	@Autowired
	PartyTypeRepository partyTypeRepository;
	
	@Autowired
	PartyRepository partyRepository;
	
	@Autowired 
	CountryRepository countryRepository;
	
	@Autowired
	StateRepository stateRepository;
	
	@Autowired
	MaterialTypeRepository materialTypeRepository ;
	
	@Autowired
	UnitOfMeasurementRepository unitOfMeasurementRepository;
	
	@Autowired
	MaterialRepository materialRepository;

	@Test
	@Ignore
	public void getAllQuotationHeader(){
		
		Country country = new Country();
		country.setName("India");
		country.setDeleted("N");
		
		entityManager.persist(country);
		entityManager.flush();
		
		State state = new State();
		state.setName("Karnataka");
		state.setCountry(countryRepository.getOne(1l));
		
		entityManager.persist(state);
		entityManager.flush();
		
		Company company = new Company();
		company.setName("test company");
		company.setState(stateRepository.getOne(1l));
		company.setCountry(countryRepository.getOne(1l));
		company.setDeleted("N");
		
		entityManager.persist(company);
		entityManager.flush();
		
		TaxType taxType = new TaxType();
		taxType.setName("GST");
		taxType.setDeleted("N");
		
		
		entityManager.persist(taxType);
		entityManager.flush();
		
		Tax tax = new Tax();
		tax.setName("test tax");
		tax.setRate(18.00);
		tax.setDeleted("N");
		tax.setTaxType(taxTypeRepository.getOne(1l));
		
		entityManager.persist(tax);
		entityManager.flush();
		
		FinancialYear financialYear = new FinancialYear();
		financialYear.setFinancialYear("2017-2018");	
		
		entityManager.persist(financialYear);
		entityManager.flush();
		
		PartyType partyType = new PartyType();
		partyType.setName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
		
		entityManager.persist(partyType);
		entityManager.flush();
		
		Party party = new Party();
		party.setName("test party");
		party.setPartyType(partyTypeRepository.getOne(1l));
		party.setEmail("test@test.com");
		party.setCountry(countryRepository.getOne(1l));
		party.setState(stateRepository.getOne(1l));
		party.setAddress("Test address");
		party.setDeleted("N");
		
		entityManager.persist(party);
		entityManager.flush();
		
		MaterialType materialType = new  MaterialType();
		
		materialType.setDeleted("N");
		materialType.setName("test material type");
		
		entityManager.persist(materialType);
		entityManager.flush();
		
		Material material = new Material();
		material.setDeleted("N");
		material.setName("test material");
		material.setPrice(100.50);
		material.setMaterialType(materialTypeRepository.getOne(1l));
		material.setTax(taxRepository.getOne(1l));
		material.setUnitOfMeasurement(unitOfMeasurementRepository.getOne(1l));
		material.setCompany(companyRepository.getOne(1l));
		
		entityManager.persist(material);
		entityManager.flush();
		
		QuotationHeader quotationHeader = new QuotationHeader();
		quotationHeader.setCreatedBy("Unit-Test-User");

		quotationHeader.setCompany(companyRepository.getOne(1l));
		quotationHeader.setTax(taxRepository.getOne(1l));
		quotationHeader.setParty(partyRepository.getOne(1l));
		quotationHeader.setFinancialYear(financialYearRepository.getOne(1l));
		//quotationHeader.setStatus(ApplicationConstants.QUOTATION_STATUS_NEW);
		
		QuotationItem quotationItem = new QuotationItem();
		
		quotationItem.setId("test-item");

		quotationItem.setMaterial(materialRepository.getOne(1011l));
		quotationItem.setQuantity(100.00);
		quotationItem.setPrice(100.50);
		quotationItem.setQuotationHeader(quotationHeader);
		
		List<QuotationItem> quotationItems = new ArrayList<QuotationItem>();
		
		quotationItems.add(quotationItem);
		
		quotationHeader.setQuotationItems(quotationItems);
		
		
		entityManager.merge(quotationHeader);
		entityManager.flush();
		
		
		//QuotationHeader quotationHeaderAssertService = quotationHeaderService.findByCreatedBy("Unit-Test-User");
		
		QuotationHeader quotationHeaderAssert = quotaionHeaderRepository.findByCreatedBy(quotationHeader.getCreatedBy());
		
		//logger.log(null, "Prior to assert");
		
		//assertEquals("Unit-Test-User", quotationHeaderAssertService.getCreatedBy());
		
		assertEquals("Unit-Test-User", quotationHeaderAssert.getCreatedBy());
		
		
		//logger.log(null, "After the assert");
	}
}
