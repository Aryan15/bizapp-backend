package com.coreerp.serviceimpl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.coreerp.AbstractTest;
import com.coreerp.dao.CountryRepository;
import com.coreerp.dao.PartyRepository;
import com.coreerp.dao.PartyTypeRepository;
import com.coreerp.dao.StateRepository;
import com.coreerp.model.Country;
import com.coreerp.model.Party;
import com.coreerp.model.PartyType;
import com.coreerp.model.State;

//@RunWith(SpringRunner.class)
//@DataJpaTest
public class PartyServiceImplTest extends AbstractTest{

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private PartyRepository partyRepository;
	
	@Autowired
	private PartyTypeRepository partyTypeRepository;
	
	@Autowired 
	CountryRepository countryRepository;
	
	@Autowired
	StateRepository stateRepository;
	
	@Test
	@Ignore
	public void saveParty(){
		
//		PartyType partyType = new PartyType();
//		partyType.setId(10l);
//		partyType.setName("Customer");
//		
//		partyTypeRepository.save(partyType);
		
		Country country = new Country();
		country.setName("India");
		country.setDeleted("N");
		
		entityManager.persist(country);
		entityManager.flush();
		
		State state = new State();
		state.setName("Karnataka");
		state.setCountry(countryRepository.getOne(1l));
		
		entityManager.persist(state);
		entityManager.flush();
		
		Party party = new Party();
		
		//party.setId(1l);
		party.setName("Test Party");
		party.setDeleted("N");
		party.setPartyType(partyTypeRepository.findByName("Customer"));
		party.setState(stateRepository.getOne(1l));
		party.setCountry(countryRepository.getOne(1l));
		party.setAddress("Test Address");
		party.setEmail("test@test.com");
		
		entityManager.persist(party);
		entityManager.flush();
		
		Party partyAssert = partyRepository.getOne(party.getId());
		
		assertEquals("Test Party", partyAssert.getName());
		
		entityManager.remove(party);
		
		
	}
	
	
	@Test
	@Ignore
	public void testDeletedParty(){
		
//		PartyType partyType = new PartyType();
//		partyType.setId(10l);
//		partyType.setDeleted("N");
//		partyType.setName("Customer");
//		
//		partyTypeRepository.save(partyType);
		
		
		Country country = new Country();
		country.setName("India");
		country.setDeleted("N");
		
		entityManager.persist(country);
		entityManager.flush();
		
		State state = new State();
		state.setName("Karnataka");
		state.setCountry(countryRepository.getOne(1l));
		
		entityManager.persist(state);
		entityManager.flush();
		
		
		Party party = new Party();
		
		//party.setId(1l);
		party.setName("Test Party");
		party.setDeleted("Y");
		party.setPartyType(partyTypeRepository.findByName("Customer"));
		party.setState(stateRepository.getOne(1l));
		party.setCountry(countryRepository.getOne(1l));
		party.setAddress("Test Address");
		party.setEmail("test@test.com");
		
		entityManager.persist(party);
		entityManager.flush();
		
		Party partyAssert = partyRepository.getOne(party.getId());
				
		//assertNull(partyAssert);
		assertEquals("Y", partyAssert.getDeleted());
		
		entityManager.remove(party);
		
		
	}
	
	@Test
	@Ignore
	public void saveAndFindByPartyType(){
		PartyType partyType = new PartyType();
		partyType.setId(1l);
		partyType.setDeleted("N");
		partyType.setName("Customer");
		
		partyTypeRepository.save(partyType);
		
		Country country = new Country();
		country.setName("India");
		country.setDeleted("N");
		
		entityManager.persist(country);
		entityManager.flush();
		
		State state = new State();
		state.setName("Karnataka");
		state.setCountry(countryRepository.getOne(1l));
		
		entityManager.persist(state);
		entityManager.flush();
		
		
		Party party = new Party();
		
		//party.setId(1l);
		party.setName("Test Party");
		party.setDeleted("N");
		party.setPartyType(partyTypeRepository.getOne(1l));
		party.setState(stateRepository.getOne(1l));
		party.setCountry(countryRepository.getOne(1l));
		party.setAddress("Test Address");
		party.setEmail("test@test.com");
		
		
		entityManager.persist(party);
		entityManager.flush();
		
		List<Party> partyAssertList = partyRepository.findByPartyType(partyType);
		
		for(Party partyAssert : partyAssertList){
			//assertEquals("Test Party", partyAssert.getName());
			if(partyAssert.getName() == "Test Partty"){
				assertEquals("Test Party", partyAssert.getName());
			}
			
		}
		
		entityManager.remove(party);
		
	}
}

