/*
insert into ma_role(id, role) values (1, 'ADMIN');
insert into ma_role(id, role) values (2, 'USER');

insert into ma_user(id, active, email, name, last_name, username, password) values (1, 1, 'ravi@gmail.com', 'Ravi', 'H', 'ravi', '$2a$10$qZs2LgkRBo5NwLGiHOd.9u767i2dJdXVvXqqKA1vZ2GGCvyxW8KZe');

insert into ma_user_role(user_id, role_id) values (1, 1);
insert into ma_user_role(user_id, role_id) values (1, 2);

insert into ma_country(id, name, deleted) values(1,'India','N');
insert into ma_country(id, name, deleted) values(2,'Australia','N');

insert into ma_state(id, name, deleted, country_id, state_code) values(1,'Karnataka','N',1,29);
insert into ma_city(id,name,deleted,state_id) values(1,'Bangalore','N',1);

insert into ma_company(id, name, deleted, city_id, state_id, country_id, gst_number ) values (1, 'Uthkrushta', 'N',1,1,1,'29ASDFG12345A1AA');

insert into ma_party_type(id, deleted, name) values(1, 'N', 'Customer');
insert into ma_party(id, deleted, name, country_id, party_type_id, state_id, address, email, gst_number) values(1,'N', 'Test Customer', 1, 1,1,'Test address', 'email@email.com','29ABCDE2J2JQ');
insert into ma_party(id, deleted, name, country_id, party_type_id, state_id, address, email, gst_number) values(2,'N', 'Test Customer2', 1, 1,1,'Test address', 'email@email2.com','29AFEDE2B2JN');

insert into cd_tax_type(id, name, deleted) values(1, 'GST', 'N');
insert into ma_tax(id, name, rate, deleted, tax_type_id) values (1,'GST', 18.0, 'N', 1);
insert into ma_tax(id, name, rate, deleted, tax_type_id) values (2,'GST', 24.0, 'N', 1);

insert into ma_financial_year(id, deleted, start_date, end_date, financial_year, is_active) values(1,'N','2017-05-01 00:00:00', '2018-04-30 00:00:00','2017-2018','Y');

insert into cd_material_type(id, deleted, name) values(1, 'N', 'Raw Material');
insert into cd_material_type(id, deleted, name) values(2, 'N', 'Semi Finished');
insert into cd_material_type(id, deleted, name) values(3, 'N', 'Finished');
insert into cd_material_type(id, deleted, name) values(4, 'N', 'Service');

insert  into cd_transaction_type(id,created_by,created_date_time,updated_by,updated_date_time,descr,name) values (1,NULL,NULL,NULL,NULL,NULL,'Customer Invoice');

insert into conf_number_range(id, deleted, trans_type_id, prefix) values(1,'N',1, 'CIN');

insert into ma_uom(id, deleted, name) values(1,'N','kg');

insert into ma_material(id, deleted, name, material_type_id, uom_id, price, company_id, tax_id, number, hsn_code) values(1,'N','Test Material',1 ,1, 1500.00,1,1,'M-101','HSN-101');

*/