package com.coreerp.dao;

import com.coreerp.model.VoucherHeaderAudit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VoucherHeaderAuditRepository extends JpaRepository<VoucherHeaderAudit, String> {
}
