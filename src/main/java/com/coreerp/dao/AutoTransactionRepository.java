package com.coreerp.dao;

import com.coreerp.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AutoTransactionRepository extends JpaRepository<AutoTransactionGeneration, String> {


    @Query(value="select * "
    + " from conf_auto_transaction  c"
    + " where find_in_set(:currentDate, c.scheduled_dates) > 0"
    + " AND IFNULL(c.stop_transaction, 0) != 1" , nativeQuery = true)
    public List<AutoTransactionGeneration> getAutoGenerationDetailsByDates(String currentDate);



    @Query("select i from AutoTransactionGeneration i ")
    public List<AutoTransactionGeneration> getAutoGenerationDetails();




    @Query("select i from AutoTransactionGeneration i where i.id = ?1")
   public AutoTransactionGeneration findById(Long id);


    Long countByInvoiceHeaderId(String invNumber);


    @Query("select i from AutoTransactionGeneration i where i.invoiceHeaderId = ?1")
    public AutoTransactionGeneration  getAutoTransactionByInvoice(String id);




    @Query("select i "
            + " from AutoTransactionGeneration i "
            + " where i.partyId = :partyId "
            + " and i.periodToCreateTransaction = :period "
            + "and i.stopTranaction != :end")
    public List<AutoTransactionGeneration> getByPartyType(@Param("partyId") Long partyId,@Param("period") Integer period,@Param("end") Integer end);


    @Query("select i from AutoTransactionGeneration i where i.invoiceHeaderId = :invId and (i.stopTranaction !=:stopTransaction OR i.stopTranaction IS NULL)")
    public AutoTransactionGeneration getAutoGeneration(String invId,Integer stopTransaction);


}
