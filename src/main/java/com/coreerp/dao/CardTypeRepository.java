package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.CardType;

public interface CardTypeRepository extends JpaRepository<CardType, Long> {

}
