package com.coreerp.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.coreerp.reports.model.WeeklyPurhaseOrderReport;
import com.coreerp.reports.model.WeeklyTransactionReportModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.model.FinancialYear;
import com.coreerp.model.Party;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;
import org.springframework.data.repository.query.Param;

public interface PurchaseOrderHeaderRepository extends JpaRepository<PurchaseOrderHeader, String> {

	public List<PurchaseOrderHeader> findByParty(Party party);
	
	public List<PurchaseOrderHeader> findByPartyAndStatusNotIn(Party party, Collection<Status> status);
	
	public List<PurchaseOrderHeader> findByPartyAndPurchaseOrderTypeAndClosePoAndStatusNotIn(Party party,TransactionType purchaseOrderType,Long closePo, Collection<Status> status);

	public List<PurchaseOrderHeader> findByPurchaseOrderNumber(String poNumber);

	@Query("select max(i.purchaseOrderId) from PurchaseOrderHeader i where i.purchaseOrderNumber like CONCAT(?1, '%')")
	public Long getMaxPONumber(String prefix);
	
	//public Long countByPurchaseOrderIdGreaterThan(Long  purchaseOrderId);
	
	public Long countByPurchaseOrderIdGreaterThanAndPurchaseOrderTypeAndFinancialYear(Long purchaseOrderId, TransactionType poType, FinancialYear financialYear);
	
//	@Query("select max(i.purchaseOrderId) from PurchaseOrderHeader i join i.purchaseOrderType t where t.id = ?1")
	@Query(value=" SELECT MAX(ph.po_id) " + 
			" FROM tr_purchase_order_header ph " + 
			" INNER JOIN cd_transaction_type tt " + 
			" ON(tt.id = ph.po_type) " + 
			" INNER JOIN conf_number_range nr " + 
			" ON(nr.trans_type_id = tt.id) " + 
			" WHERE tt.id = ?1 " + 
			" AND ((nr.auto_number_reset = 1 AND ph.financial_year_id = ( " + 
			" SELECT fy.id FROM ma_financial_year fy " + 
			" WHERE fy.is_active = 1 " + 
			" )) OR nr.auto_number_reset = 0) ", nativeQuery = true)
	Long getMaxPONumberByTransactionType(Long transactionTypeId);

	public PurchaseOrderHeader findByPurchaseOrderNumberAndPurchaseOrderType(String poNumber,
			TransactionType poType);
	
	public Long countByPurchaseOrderNumberAndPurchaseOrderTypeAndFinancialYear(String poNumber, TransactionType poType, FinancialYear financialYear);
	public Long countByPurchaseOrderNumberAndPurchaseOrderTypeAndPartyAndFinancialYear(String poNumber, TransactionType poType, Party party, FinancialYear financialYear);




	@Query(
			"SELECT NEW com.coreerp.reports.model.WeeklyPurhaseOrderReport("
					+ " count(ph.id) AS poCount  "
					+ " ,sum(ph.grandTotal) AS poAmount "
					+ " ) "
					+ " FROM PurchaseOrderHeader ph "
					+ " join ph.purchaseOrderType phType "
					+ "  where 1 = 1 "
					+ " and phType.id = :transactionType "
					+ " and (date(ph.purchaseOrderDate) >= :fromDate or :fromDate is null) "
					+ " and (date(ph.purchaseOrderDate) <= :toDate or :toDate is null) "
	)
	public WeeklyPurhaseOrderReport getPoWeeklyReport(
			@Param("fromDate") Date fromDate
			,@Param("toDate") Date toDate
			,@Param("transactionType") Long transactionType

	);



	@Query("select  count(ph.id) AS poCount "
			+ " FROM PurchaseOrderHeader ph "
			+ " join ph.purchaseOrderType phType "
			+  " join ph.status phstatuse"
			+ "  where 1 = 1 "
			+ " and phType.id = :transactionType "
			+  " and phstatuse.id= :statusId "
			+ " and (date(ph.purchaseOrderDate) >= :fromDate or :fromDate is null) "
			+ " and (date(ph.purchaseOrderDate) <= :toDate or :toDate is null) "
	)
	public long getCompletedPoWeeklyReport(
			@Param("fromDate") Date fromDate
			,@Param("toDate") Date toDate
			,@Param("transactionType") Long transactionType
			,@Param("statusId") Long statusId
	);

	@Query("select  count(ph.id) AS poCount "
			+ " FROM PurchaseOrderHeader ph "
			+ " join ph.purchaseOrderType phType "
			+  " join ph.status phstatuse"
			+ "  where 1 = 1 "
			+ " and phType.id = :transactionType "
			+  " and phstatuse.id != :statusId "
			+ " and (date(ph.purchaseOrderDate) >= :fromDate or :fromDate is null) "
			+ " and (date(ph.purchaseOrderDate) <= :toDate or :toDate is null) "
	)
	public long getBalancedPoWeeklyReport(
			@Param("fromDate") Date fromDate
			,@Param("toDate") Date toDate
			,@Param("transactionType") Long transactionType
			,@Param("statusId") Long statusId
	);








}
