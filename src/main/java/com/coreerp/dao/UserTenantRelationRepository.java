package com.coreerp.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.dto.UserTenantRelationDTO;
import com.coreerp.model.UserTenantRelation;
import org.springframework.transaction.annotation.Transactional;


public interface UserTenantRelationRepository extends JpaRepository<UserTenantRelation, Integer> {

    UserTenantRelation findByUsername(String name);
    
    UserTenantRelation findByEmail(String email);
    
    UserTenantRelation findFirstByTenant(String tenantName);
    
    UserTenantRelation findByTenantAndIsActiveAndIsPrimary(String tenant, Integer isActive, Integer isPrimary);
    
    UserTenantRelation findByUsernameAndIsActive(String username, Integer isActive);
    
    List<UserTenantRelation> findByIsActive(Integer isActive);
    
    @Query(value="select to_date from tr_customer_plan_subs where company_id = ?1", nativeQuery=true)
    Date getExpiryDateOfCompany(Integer companyId);
    
    
    @Query(value="select to_date from tr_customer_plan_subs where company_id = ?1", nativeQuery=true)
    UserTenantRelationDTO findByUsername(Integer companyId);

    @Query("select max(companyId) from UserTenantRelation")
    Integer getMaxCompanyId();

    @Transactional
    @Modifying
    @Query(value="insert into tr_customer_plan_subs(from_date, to_date, is_current_plan, plan_id, company_id, party_id)" +
            " values(?1, ?2, ?3, ?4, ?5, ?6)", nativeQuery=true)
    void insertPlanSubs(Date fromDate, Date toDate, Integer isCurrentPlan, Integer planId, Integer companyId, Long party_id);

    @Query("select distinct u.username from UserTenantRelation u" +
            " where u.isActive = 1" +
            " and u.isPrimary = 1 ")
    List<String> findActiveUsernames();



    @Query("select distinct u from UserTenantRelation u" +
            " where u.isActive = 1" +
            " and u.isPrimary = 1 ")
    List<UserTenantRelation> findAllUser();

}
