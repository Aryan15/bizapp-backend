package com.coreerp.dao;

import com.coreerp.model.CompanyEmailModel;
import com.coreerp.model.GlobalSetting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyEmailRepository extends JpaRepository<CompanyEmailModel, Long> {
}
