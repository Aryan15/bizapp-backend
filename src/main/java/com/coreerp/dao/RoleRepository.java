package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Role;
import com.coreerp.model.RoleType;

public interface RoleRepository extends JpaRepository<Role, Long>{
	
	Role findByRole(String role);

	List<Role> findByRoleType(RoleType roleType);
	List<Role> findByRoleTypeNotIn(RoleType roleType);
}
