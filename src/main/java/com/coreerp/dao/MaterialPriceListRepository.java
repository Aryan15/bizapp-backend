package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Material;
import com.coreerp.model.MaterialPriceList;
import com.coreerp.model.Party;

public interface MaterialPriceListRepository extends JpaRepository<MaterialPriceList, Long> {

	List<MaterialPriceList> findByParty(Party party);


	MaterialPriceList findByPartyAndMaterial(Party party, Material material);



}
