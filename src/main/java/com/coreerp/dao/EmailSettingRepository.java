package com.coreerp.dao;

import com.coreerp.model.UserTenantRelation;
import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.EmailSetting;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmailSettingRepository extends JpaRepository<EmailSetting, Integer> {




    @Query("select  u from EmailSetting u")
   EmailSetting findAllUser();


    @Query("select i from EmailSetting i where i.id = ?1")
  EmailSetting getEmail(Long id);


}
