package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Activity;
import com.coreerp.model.Menu;

public interface MenuRepository extends JpaRepository<Menu, Long> {
	
	public List<Menu> findByActivities(List<Activity> activities);

}
