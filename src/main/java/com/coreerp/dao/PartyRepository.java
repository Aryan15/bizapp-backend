package com.coreerp.dao;

import java.util.List;

import com.coreerp.model.FinancialYear;
import com.coreerp.model.TransactionType;
import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Party;
import com.coreerp.model.PartyType;
import com.coreerp.model.UserTenantRelation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PartyRepository extends JpaRepository<Party, Long> {

	List<Party> findByPartyType(PartyType partyType);
	
	List<Party> findByPartyTypeInAndNameIgnoreCaseContainsOrPartyCodeIgnoreCaseContains(List<PartyType> partyType, String searchString,String searchStringCode);

	List<Party> findByNameIgnoreCaseContains(String searchString);

	List<Party> findByPartyCodeIgnoreCaseContains(String searchString);
	
	Party findByEmail(String email);
	
	Party findByPrimaryMobile(String number);
	
	List<Party> findByGstNumber(String gstNumber);


	@Query(value= "select MAX(ph.party_code_id)"+
			" from ma_party ph" +
			" where ph.party_type_id=:partyId ",nativeQuery = true)
	Long getMaxPartyNumber(@Param("partyId") Long partyId);



	@Query("select p "
			+ " from Party p "
			+ " where (p.partyType IN (:partyTypes) "
		    + " AND (p.name like %:name% or name is null "
			+ " or p.partyCode like %:name% or name is null))")
	List<Party> getPartyList(List<PartyType> partyTypes, String name);

	Long countByPartyCodeAndPartyType(String partyCode, PartyType partyType);


	@Query("select p "
			+ " from Party p "
			+ " where p.name like %:name% or name is null ")
	Party getPartyByName(String name);

}
