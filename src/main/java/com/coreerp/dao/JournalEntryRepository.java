package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.model.JournalEntry;
import com.coreerp.model.TransactionType;

public interface JournalEntryRepository extends JpaRepository<JournalEntry, String> {

	@Query("select j from JournalEntry j "
			+ "where j.transactionType = ?1 "
			+ "and j.isReversal is null "
			+ "and j.transactionId = ?2 "
			+ "and j.transactionIdSequence = (select max(transactionIdSequence) "
			+ "									from JournalEntry je "
			+ "									where je.transactionType = j.transactionType "
			+ "									and   je.transactionId = j.transactionId )")
	public JournalEntry findByTransactionTypeAndTransactionId(TransactionType transactionType, String transactionId); 
}
