package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.DeliveryChallanItem;
import com.coreerp.model.PurchaseOrderHeader;
import org.springframework.data.jpa.repository.Query;

public interface DeliveryChallanItemRepository extends JpaRepository<DeliveryChallanItem, String> {

	public List<DeliveryChallanItem> findByDeliveryChallanHeader(DeliveryChallanHeader deliveryChallanHeader);

	@Query("select i from DeliveryChallanItem i " +
			" join i.purchaseOrderItem pi " +
			" join pi.purchaseOrderHeader ph " +
			" where ph.id = ?1 ")
	public List<DeliveryChallanItem> findByPurchaseOrderHeader(String phId);

	public DeliveryChallanItem findByJwNoteItemId(String noteItemId);
}
