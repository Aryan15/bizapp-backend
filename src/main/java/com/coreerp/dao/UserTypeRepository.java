package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.UserType;

public interface UserTypeRepository extends JpaRepository<UserType, Long> {

}
