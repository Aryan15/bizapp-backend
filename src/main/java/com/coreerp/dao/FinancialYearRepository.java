package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.FinancialYear;

public interface FinancialYearRepository extends JpaRepository<FinancialYear, Long> {

	public FinancialYear findByIsActive(Number isActive);
}
