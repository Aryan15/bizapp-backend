package com.coreerp.dao;

import java.util.List;


import com.coreerp.model.AutoTransactionGeneration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.dto.PartyDTO;
import com.coreerp.model.Material;
import com.coreerp.model.MaterialType;
import com.coreerp.model.Party;
import com.coreerp.model.SupplyType;

public interface MaterialRepository extends JpaRepository<Material, Long> {

	@Query("select m "
			+ " from Material m "
			+ " join m.materialType mt "
			+ " where (m.name like %:name% "
			+ " or m.partNumber like %:partNumber%) "
			+ " and mt.id = :materialTypeId ")
	public List<Material> materialSearchForJobwork(Long materialTypeId, String name, String partNumber);

	@Query("select m "
			+ " from Material m "
			+ " join m.materialType mt "
			+ " where (m.name like %:name% "
			+ " or m.partNumber like %:partNumber%) ")
	public List<Material> findByNameIgnoreCaseContaining(String name,String partNumber);
	
	public List<Material> findByParty(Party party);

	public Material findByPartNumber(String partNumber);

	@Query("select m "
			+ " from Material m "
			+ " join m.supplyType st "
			+ " left join m.materialType mt "
			+ " where (m.name like %:name% "
			+ " or m.partNumber like %:partNumber%) "
			+ " and st.id = :supplyTypeId "
			+ " and (mt.name != :materialTypeName or mt.name is null )")
	public List<Material> materialSearch(String name,String partNumber, Long supplyTypeId, String materialTypeName);
	
	
	@Query("select m "
			+ " from Material m "
			+ " join m.supplyType st "
			+ " join m.party p "
			+ " join m.materialType mt "
			+ " where (m.name like %:name% "
			+ " or m.partNumber like %:partNumber%) "
			+ " and st.id = :supplyTypeId "
			+ " and p.id = :partyId "
			+ " and mt.name != :materialTypeName ")
	public List<Material> materialSearchWithParty(Long partyId, String name, String partNumber, Long supplyTypeId, String materialTypeName);


	@Query("select m " +
			" from Material m " +
			" where (upper(replace(m.name,' ', '')) like %:name% )"+
			" and (m.id != :id or :id is null )")
	public List<Material> materialSearchByName(String name,Long id);
	
	public List<Material> findByIdIn(List<Long> materialIds);

	public List<Material> findByMaterialType(MaterialType materialType);

	@Query(value = "SELECT COUNT(1)" +
			" FROM ma_material m" +
			" WHERE EXISTS " +
			" (" +
			" (SELECT 1" +
			" FROM tr_quotation_item qi" +
			" WHERE qi.material_id = m.id" +
			" ) UNION" +
			" (SELECT 1" +
			" FROM tr_purchase_order_item poi" +
			" WHERE poi.material_id = m.id" +
			" ) UNION" +
			" (SELECT 1" +
			" FROM tr_delivery_challan_item di" +
			" WHERE di.material_id = m.id" +
			" ) UNION" +
			" (SELECT 1" +
			" FROM tr_grn_item gi" +
			" WHERE gi.material_id = m.id" +
			" ) UNION" +
			" (SELECT 1" +
			" FROM tr_invoice_item ini" +
			" WHERE ini.material_id = m.id" +
			" )" +
			" )" +
			" AND m.id = :materialId ",nativeQuery = true)
	public Integer materialExistsinTransactions(Long materialId);
	
	public Material getById(Long id);

	@Query("select i from Material i  where i.id = ?1")
	public List<Material> getAllMaterials(Long materialTypeId);

}


