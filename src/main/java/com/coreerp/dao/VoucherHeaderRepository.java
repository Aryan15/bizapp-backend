package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.model.VoucherHeader;
import com.coreerp.model.Party;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;

public interface VoucherHeaderRepository extends JpaRepository<VoucherHeader, String>{

	@Query("select i from VoucherHeader i where i.id = ?1")
	VoucherHeader findByHeaderId(String headerId);
	
	List<VoucherHeader> findByVoucherNumber(String voucherNumber);
	
	@Query("select max(i.voucherId) from VoucherHeader i where i.voucherNumber like CONCAT(?1,'%')")
    Long getMaxVoucherNumber(String prefix);
	
	
	Long countByVoucherIdGreaterThan(Long voucherId);
	
	Long countByVoucherNumberAndVoucherType(String voucherNumber, TransactionType id );
	
//	@Query("select max(i.voucherId) from VoucherHeader i join i.voucherType t where t.id = ?1")
	@Query(value=" SELECT MAX(vh.voucher_id) " + 
			" FROM tr_voucher_header vh " + 
			" INNER JOIN cd_transaction_type tt " + 
			" ON(tt.id = vh.voucher_type) " + 
			" INNER JOIN conf_number_range nr " + 
			" ON(nr.trans_type_id = tt.id) " + 
			" WHERE tt.id = ?1 " + 
			" AND ((nr.auto_number_reset = 1 AND vh.financial_year_id = ( " + 
			" SELECT fy.id FROM ma_financial_year fy " + 
			" WHERE fy.is_active = 1 " + 
			" )) OR nr.auto_number_reset = 0) ", nativeQuery = true)
	Long getMaxVoucherNumberByVoucherType(Long id);
	

	
}