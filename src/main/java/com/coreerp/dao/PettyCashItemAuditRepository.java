package com.coreerp.dao;

import com.coreerp.model.PettyCashItemAudit;
import com.coreerp.model.VoucherHeaderAudit;
import com.coreerp.model.VoucherItemAudit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PettyCashItemAuditRepository extends JpaRepository<PettyCashItemAudit, String> {

    List<PettyCashItemAudit> findByPettyCashHeader(String id);
}
