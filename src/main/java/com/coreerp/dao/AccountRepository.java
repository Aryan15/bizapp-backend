package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

}
