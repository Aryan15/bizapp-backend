package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.FinancialYear;
import com.coreerp.model.Party;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionPK;
import com.coreerp.model.TransactionType;

public interface DeliveryChallanHeaderRepository extends JpaRepository<DeliveryChallanHeader, String> {

	public List<DeliveryChallanHeader> findByParty(Party party);

	@Query("select max(i.deliveryChallanId) from DeliveryChallanHeader i where i.deliveryChallanNumber like CONCAT(?1, '%')")
	public Long getMaxDCNumber(String prefix);

	@Query("select d from DeliveryChallanHeader d where d.party = ?1 and d.deliveryChallanType = ?2 and d.balanceQuantity > 0")
	public List<DeliveryChallanHeader> findByPartyAndTransactionTypeWithNonZeroBalanceQuantity(Party partyObjectById,
			TransactionType modelById);

	public List<DeliveryChallanHeader> findByDeliveryChallanNumber(String dcNumber);

	public List<DeliveryChallanHeader> findByPartyAndStatusNotIn(Party party, List<Status> statuses);

	public List<DeliveryChallanHeader> findByPartyAndDeliveryChallanTypeAndStatusNotInAndBalanceQuantityIsGreaterThan(
			Party partyObject, TransactionType transactionType, List<Status> statuses, Double quantity);
	
	public List<DeliveryChallanHeader> findByPartyAndDeliveryChallanTypeAndStatusNotIn(
			Party partyObject, TransactionType transactionType, List<Status> statuses);
	
	
	//Long countByDeliveryChallanIdGreaterThanAndDeliveryChallanTypeAndFinancialYear(Long deliveryChallanId, TransactionType dcType, FinancialYear financialYear);
	
//	@Query("select max(i.deliveryChallanId) from DeliveryChallanHeader i join i.deliveryChallanType t where t.id = ?1")
	@Query(value=" SELECT MAX(dh.dc_id) " + 
			" FROM tr_delivery_challan_header dh " + 
			" INNER JOIN cd_transaction_type tt " + 
			" ON(tt.id = dh.dc_type) " + 
			" INNER JOIN conf_number_range nr " + 
			" ON(nr.trans_type_id = tt.id) " +
			" WHERE (FIND_IN_SET (tt.id, (SELECT transaction_type_ids "+
			" FROM conf_common_sequence "+
			" WHERE FIND_IN_SET(?1, transaction_type_ids) > 0 "+
			")) > 0 OR tt.id = ?1) "+
			" AND ((nr.auto_number_reset = 1 AND dh.financial_year_id = ( "+
			" SELECT fy.id FROM ma_financial_year fy "+
			" WHERE fy.is_active = 1 "+
			" )) OR nr.auto_number_reset = 0) ", nativeQuery = true)
	public Long getMaxDCNumberByTransactionType(Long transactionTypeId);

	public Long countByDeliveryChallanNumberAndDeliveryChallanTypeAndFinancialYear(String dcNumber, TransactionType dcType,FinancialYear financialYear);
	
	boolean existsByFinancialYearAndDeliveryChallanTypeIn(FinancialYear financialYear, List<TransactionType> transactionTypes);

	public Long countByDeliveryChallanNumberAndDeliveryChallanTypeAndPartyAndFinancialYear(String dcNumber, TransactionType dcType, Party party,FinancialYear financialYear);


	@Query(value= "select count(dh.dc_id) "+
			" from tr_delivery_challan_header dh " +
			" where dh.dc_id > :dcId " +
			" and  dh.dc_type IN(:list) " +
			" and dh.financial_year_id = :financialYearId ",nativeQuery = true)
	Long countMaxDcCount(Long dcId, List<Long> list,  Long financialYearId);


	@Query(value = "select transaction_type_ids from conf_common_sequence where find_in_set(:dcTypeId, transaction_type_ids) > 0", nativeQuery = true)
	String getCommonSequence(Long dcTypeId);

	public DeliveryChallanHeader findByJwNoteId(String noteId);

}
