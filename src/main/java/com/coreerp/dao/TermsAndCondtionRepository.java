package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.coreerp.model.TermsAndCondition;
import com.coreerp.model.TransactionType;

public interface TermsAndCondtionRepository extends JpaRepository<TermsAndCondition, Long>{

	@Query("select i "
			+ " from TermsAndCondition i "
			+ " where i.transactionType = :transactionType "
			+ " and (i.defaultTermsAndCondition = :defaultTermsAndCondition or :defaultTermsAndCondition is null ) ")
	public List<TermsAndCondition> findByTransactionTypeAndDefaultTermsAndCondition(@Param("transactionType") TransactionType transactionType,
			@Param("defaultTermsAndCondition") Integer defaultTermsAndCondition);

}
