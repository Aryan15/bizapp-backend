package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.PersonGender;

public interface PersonGenderRepository extends JpaRepository<PersonGender, Long> {

}
