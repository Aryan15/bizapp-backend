package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.PartyType;

public interface PartyTypeRepository extends JpaRepository<PartyType, Long> {

	PartyType findByName(String name);

}
