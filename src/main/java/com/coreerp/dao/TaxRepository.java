package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Tax;

public interface TaxRepository extends JpaRepository<Tax, Long> {

}
