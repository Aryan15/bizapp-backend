package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.RoleType;

public interface RoleTypeRepository extends JpaRepository<RoleType, Long> {

	RoleType findByName(String name);
	List<RoleType> findByNameNotIn(List<String> names);
}
