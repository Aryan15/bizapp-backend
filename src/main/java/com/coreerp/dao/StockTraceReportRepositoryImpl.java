package com.coreerp.dao;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.stereotype.Repository;

import com.coreerp.model.StockTraceReport;

@Repository
public class StockTraceReportRepositoryImpl implements StockTraceReportRepository{

final static Logger log = LogManager.getLogger(StockTraceReportRepositoryImpl.class);
	
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public List<StockTraceReport> getReport(Date fromDate, Date toDate) {
		
		TypedQuery<StockTraceReport> query = manager.createNamedQuery("qryStockTraceReport", StockTraceReport.class);
		query.setParameter("from_date", fromDate);
		query.setParameter("to_date", toDate);
				
		List<StockTraceReport> stockTraceReports =  query.getResultList();
		log.info("Returning "+stockTraceReports.size()+" Rows");
		
		return stockTraceReports;
	}

	@Override
	public List<StockTraceReport> getReportForMaterial(Date fromDate, Date toDate, Long materialId) {
		TypedQuery<StockTraceReport> query = manager.createNamedQuery("qryStockTraceReport", StockTraceReport.class);
		query.setParameter("from_date", fromDate);
		query.setParameter("to_date", toDate);
		query.setParameter("material_id", materialId);
				
		List<StockTraceReport> stockTraceReports =  query.getResultList()
				.stream()
				.filter(res -> 
					(res.getReverse().equals("N") ? true : false)
				)
				.collect(Collectors.toList());
		log.info("Returning "+stockTraceReports.size()+" Rows");
		
		return stockTraceReports;
	}
	

}
