package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {

}
