package com.coreerp.dao;

import com.coreerp.model.VoucherItemAudit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VoucherItemAuditRepository extends JpaRepository<VoucherItemAudit, String> {

    List<VoucherItemAudit> findByVoucherHeaderId(String id);
}
