package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.UnitOfMeasurement;

public interface UnitOfMeasurementRepository extends JpaRepository<UnitOfMeasurement, Long> {

}
