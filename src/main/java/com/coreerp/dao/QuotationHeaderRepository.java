package com.coreerp.dao;

import java.util.Collection;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface QuotationHeaderRepository extends JpaRepository<QuotationHeader, String>{

	QuotationHeader findByCreatedBy(String createdBy);
	
	List<QuotationHeader> findByParty(Party party);
	
	public List<QuotationHeader> findByPartyAndStatusNotIn(Party party, Collection<Status> status);

	@Query("select max(i.quotationId) from QuotationHeader i where i.quotationNumber like CONCAT(?1,'%')")
	public Long getMaxQuotationNumber(String prefix);

	List<QuotationHeader> findByQuotationNumber(String quotationNumber);

	List<QuotationHeader> findByQuotationNumberIgnoreCaseContains(String searchString);

	List<QuotationHeader> findByQuotationDate(String searchString);
	
	Long countByQuotationNumberAndQuotationTypeAndFinancialYear(String quotationNumber, TransactionType id , FinancialYear financialYear );
	
//	@Query("select max(i.quotationId) from QuotationHeader i join i.quotationType t where t.id = ?1")
	@Query(value=" SELECT MAX(qh.quotation_id) " + 
			" FROM tr_quotation_header qh " + 
			" INNER JOIN cd_transaction_type tt " + 
			" ON(tt.id = qh.quotation_type_id) " + 
			" INNER JOIN conf_number_range nr " + 
			" ON(nr.trans_type_id = tt.id) " + 
			" WHERE tt.id = ?1 " + 
			" AND ((nr.auto_number_reset = 1 AND qh.financial_year_id = ( " + 
			" SELECT fy.id FROM ma_financial_year fy " + 
			" WHERE fy.is_active = 1 " + 
			" )) OR nr.auto_number_reset = 0) ", nativeQuery = true)
	Long getMaxQuotationNumberByTransactionType(Long transactionTypeId);
}
