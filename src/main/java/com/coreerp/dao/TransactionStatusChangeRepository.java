package com.coreerp.dao;

import java.util.List;

import com.coreerp.model.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TransactionStatusChangeRepository extends JpaRepository<TransactionStatusChange, Long> {

	@Query("select t from TransactionStatusChange t "
			+ " where t.transactionId = ?1 "
			+ " and t.createdDateTime = ( "
			+ " select max(createdDateTime) from TransactionStatusChange tt "
			+ " where tt.transactionId = t.transactionId "
			+ " and (UPPER(tt.sourceOperation) not like '%DELETE%' or tt.sourceOperation is null ) ) ")
	TransactionStatusChange findByLatestTransactionId(String transactionId);
	

	
	@Query("select t from TransactionStatusChange t "
			+ " where t.sourceTransactionId = ?1 "
			+ " and t.createdDateTime = ( "
			+ " select max(createdDateTime) from TransactionStatusChange tt "
			+ " where tt.sourceTransactionId = t.sourceTransactionId "
			+ " and tt.transactionId = t.transactionId ) "
			+ " and (UPPER(t.sourceOperation) not like '%DELETE%' or t.sourceOperation is null) ")
	List<TransactionStatusChange> findByLatestSourceTransactionId(String sourceTransactionId);


	@Query("select t from TransactionStatusChange t "
			+ " where t.sourceTransactionId = ?1 "
			+ " and t.createdDateTime = ( "
			+ " select max(createdDateTime) from TransactionStatusChange tt "
			+ " where tt.sourceTransactionId = t.sourceTransactionId "
			+ " and tt.transactionId = t.transactionId ) "
			+ " and (UPPER(t.sourceOperation) not like '%DELETE%' or t.sourceOperation is null) ")
	TransactionStatusChange findByLatestSourceTransactionsId(String sourceTransactionId);



	
	List<TransactionStatusChange> findByTransactionId(String transactionId);

	@Query("select i from TransactionStatusChange i where i.transactionId = ?1")
	List<TransactionStatusChange> findByTranId(String transactionId);

	@Query("select i from TransactionStatusChange i where i.sourceTransactionId = ?1")
	List<TransactionStatusChange> findByStranId(String stransactionId);

	@Modifying(flushAutomatically = true)
	@Query("delete from TransactionStatusChange i where i=:transactionStatusChange")
	void deleteTransaction(@Param("transactionStatusChange") TransactionStatusChange transactionStatusChange);



	@Query("select t from TransactionStatusChange t "
			+ " where t.transactionId = ?1 "
			+ " and t.sourceTransactionId is not null "
			+ " and t.createdDateTime = ( "
			+ " select max(createdDateTime) from TransactionStatusChange tt "
			+ " where tt.transactionId = t.transactionId ) "
			+ " and (UPPER(t.sourceOperation) not like '%DELETE%' or t.sourceOperation is null) ")
	List<TransactionStatusChange> findByTransactionIdAndSourceTransactionIdIsNotNull(String transactionId);


//	@Query("select t from TransactionStatusChange t "
//			+ " where t.transactionId = ?1 "
//			+ " and t.sourceTransactionId is not null "
//			+ " and (UPPER(t.sourceOperation) not like '%DELETE%' or t.sourceOperation is null) "
//			+ " group by t.sourceTransactionId" )
	//List<TransactionStatusChange> findByTransactionIdAndSourceTransactionIdIsNotNulls(String transactionId);




    @Query(
            "SELECT NEW com.coreerp.model.TransactionReffernceModel("
					+ " t.sourceTransactionId as  sourceTransactionId "
                    + "  ,max(tType.id) AS transactionTypeId "
                    + " ,max(tType.name) AS transactionTypeName "
                    + " ,max(t.transactionId) AS transactionId "
                    + " ,max(t.transactionNumber) AS transactionNumber "
			        + " ,max(status.id) as statusId "
					+ " ,max(status.name) as statusId "
			        + " ,max(t.latest) AS latest "
			        + " ,max(stType.id) as sourceTransactionTypeId "
					+ " ,max(stType.name) AS transactionTypeName "
					+ " ,max(t.sourceTransactionNumber)  as sourceTransactionNumber "
			        +"  ,max(t.sourceOperation) AS sourceOperation "
	                + " )"
					+ " FROM TransactionStatusChange t "
                    + " join t.transactionType tType"
					+ " join t.sourceTransactionType stType"
                    + " join t.status status "
					+ " where  1 = 1 "
					+  " and t.transactionId = ?1 "
					+ " and t.sourceTransactionId is not null "
					+ " and (UPPER(t.sourceOperation) not like '%DELETE%' or t.sourceOperation is null) "
					+ " group by t.sourceTransactionId"  )
	public List<TransactionReffernceModel> findByTransactionIdAndSourceTransactionIdIsNotNulls(String transactionId);













    @Query("select t from TransactionStatusChange t "
			+ " where t.transactionId = ?1 "
			+ " and t.sourceTransactionId is not null "
			+ " and t.createdDateTime = ( "
			+ " select max(createdDateTime) from TransactionStatusChange tt "
			+ " where tt.transactionId = t.transactionId ) "
			+ " and (UPPER(t.sourceOperation) not like '%DELETE%' or t.sourceOperation is null) ")
	TransactionStatusChange findByTransactionsIdAndSourceTransactionIdIsNotNull(String transactionId);
	
	List<TransactionStatusChange> findBySourceTransactionId(String transactionId);
	
	boolean existsByParty(Party party);
	
	boolean existsByTransactionTypeIn(List<TransactionType> transactionTypes);

	@Query(value = "SELECT COUNT(1) " +
			"FROM tr_status_change t1 " +
			"INNER JOIN tr_status_change t2 " +
			"ON(t1.tran_id = t2.tran_id) " +
			"WHERE t1.tran_id IN " +
			"(SELECT DISTINCT tran_id FROM tr_status_change " +
			"WHERE src_tran_id in (:tranNumbers)) " +
			"AND t1.src_tran_id in (:tranNumbers) " +
			"AND t1.src_tran_id != t2.src_tran_id " +
			"AND t2.id > t1.id " +
			"AND t1.src_oper NOT LIKE '%Deleted%' " +
			"AND t2.src_oper NOT LIKE '%Deleted%' " +
			"AND  t1.src_oper NOT LIKE '%DC Cancelled%' " +
			"AND  t2.src_oper NOT LIKE '%DC Cancelled%'" , nativeQuery = true)
	Integer countByTransactionNumber(List<String> tranNumbers);

}
