package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.*;

import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.PayableReceivableItem;


public interface PayableReceivableItemRepository extends JpaRepository<PayableReceivableItem, String> {
	
	List<PayableReceivableItem> findByInvoiceHeader(InvoiceHeader invoiceHeader);

	@Query("select i from PayableReceivableItem i where i.invoiceHeader.id = ?1")
	List<PayableReceivableItem> getItem(String id);
}
