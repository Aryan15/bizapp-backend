package com.coreerp.dao;

import com.coreerp.model.PettyCashHeaderAudit;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PettyCashHeaderAuditRepository extends JpaRepository<PettyCashHeaderAudit, String> {
}
