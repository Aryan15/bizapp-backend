package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.TransactionType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TransactionTypeRepository extends JpaRepository<TransactionType, Long> {

	TransactionType findByName(String name);
	
	List<TransactionType> findByIdIn(List<Long> ids);



	@Query(value= "select count(ct.id) "+
			" from cd_transaction_type ct " +
			" where  ct.name IN(:tTypes) "+
			" and ct.deleted = :N ",nativeQuery = true)
	Integer findTransactionType(@Param("tTypes") List<String> tTypes,@Param("N") String N);

}
