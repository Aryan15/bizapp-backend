package com.coreerp.dao;

import com.coreerp.model.PrintTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrintTemplateRepository extends JpaRepository<PrintTemplate, Long> {
}
