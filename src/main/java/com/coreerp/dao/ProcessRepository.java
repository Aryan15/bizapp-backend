package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.coreerp.model.Process;

public interface ProcessRepository extends JpaRepository<Process, Long> {

}
