package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Country;

public interface CountryRepository extends JpaRepository<Country, Long> {

}
