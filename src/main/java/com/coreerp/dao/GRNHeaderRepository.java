package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.model.FinancialYear;
import com.coreerp.model.GRNHeader;
import com.coreerp.model.Party;
import com.coreerp.model.Status;

public interface GRNHeaderRepository extends JpaRepository<GRNHeader, String>{

	@Query("select i from GRNHeader i where i.id = ?1")
	GRNHeader findByHeaderId(String headerId);
	
	List<GRNHeader> findByGrnNumber(String grnNumber);
	
	@Query("select max(i.grnId) from GRNHeader i where i.grnNumber like CONCAT(?1,'%')")
    Long getMaxGrnNumber(String prefix);
	
	List<GRNHeader> findBySupplierId(Long partyId);
	
	Long countByGrnIdGreaterThan(Long grnId);
	
//	@Query("select max(i.grnId) from GRNHeader i join i.grnType t where t.id = ?1")
	@Query(value=" SELECT MAX(gh.grn_id) " + 
			" FROM tr_grn_header gh " + 
			" INNER JOIN cd_transaction_type tt " + 
			" ON(tt.id = gh.grn_type_id) " + 
			" INNER JOIN conf_number_range nr " + 
			" ON(nr.trans_type_id = tt.id) " + 
			" WHERE tt.id = ?1 " + 
			" AND ((nr.auto_number_reset = 1 AND gh.financial_year_id = ( " + 
			" SELECT fy.id FROM ma_financial_year fy " + 
			" WHERE fy.is_active = 1 " + 
			" )) OR nr.auto_number_reset = 0) ", nativeQuery = true)
	Long getMaxGrnNumberByTransactionType(Long id);
	

	public List<GRNHeader> findBySupplierAndGrnStatusNotIn(Party party, List<Status> statuses);
	
//	Long countByGrnNumber(String grnNumber);
	
	public Long countByGrnNumberAndSupplierAndIdNotInAndFinancialYear(String grnNumber, Party party,String id,FinancialYear financialYear);
	
	Boolean existsByFinancialYear(FinancialYear financialYear);
}
