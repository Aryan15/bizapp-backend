package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.GstRegistrationType;

public interface GstRegistrationTypeRepository extends JpaRepository<GstRegistrationType, Long> {

}
