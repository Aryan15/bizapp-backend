package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.model.Party;
import com.coreerp.model.PayableReceivableHeader;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;

public interface PayableReceivableHeaderRepository extends JpaRepository<PayableReceivableHeader, String> {

	@Query("select max(i.payReferenceId) from PayableReceivableHeader i where i.payReferenceNumber like CONCAT(?1,'%')")
	Long getMaxPRNumber(String prefix);

//	@Query("select max(i.payReferenceId) from PayableReceivableHeader i join i.transactionType t where t.id = ?1")
	@Query(value=" SELECT MAX(prh.pay_ref_id) " + 
			" FROM tr_payable_receivable_header prh " + 
			" INNER JOIN cd_transaction_type tt " + 
			" ON(tt.id = prh.transaction_type) " + 
			" INNER JOIN conf_number_range nr " + 
			" ON(nr.trans_type_id = tt.id) " + 
			" WHERE tt.id = ?1 " + 
			" AND ((nr.auto_number_reset = 1 AND prh.financial_year_id = ( " + 
			" SELECT fy.id FROM ma_financial_year fy " + 
			" WHERE fy.is_active = 1 " + 
			" )) OR nr.auto_number_reset = 0) ", nativeQuery = true)
	Long getMaxPRNumberByTransactionType(Long id);
	
	
	Long countByPayReferenceIdGreaterThanAndTransactionType(Long payReferenceId, TransactionType tTye);
	
	Long countByPartyAndStatusIn(Party party, List<Status> statuses);

}
