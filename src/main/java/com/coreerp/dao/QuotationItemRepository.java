package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.QuotationHeader;
import com.coreerp.model.QuotationItem;
import com.coreerp.model.TransactionPK;

public interface QuotationItemRepository extends JpaRepository<QuotationItem, String> {

	public List<QuotationItem> findByQuotationHeader(QuotationHeader quotationHeader);
}
