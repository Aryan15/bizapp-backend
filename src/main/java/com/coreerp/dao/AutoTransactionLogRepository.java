package com.coreerp.dao;

import com.coreerp.model.AutoTransactionLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AutoTransactionLogRepository extends JpaRepository<AutoTransactionLog, String> {
    @Query("select max(i) from AutoTransactionLog i where i.invoiceHeaderId= ?1")
    public AutoTransactionLog getAutoTransLogDetails(String invoiceHeaderId);



    @Query("select count(i) from AutoTransactionLog i where i.invoiceHeaderId= ?1")
    public Integer getCountOfInvoice(String invoiceHeaderId);

    @Query("select i.transactionCount from AutoTransactionLog i where i.invoiceHeaderId= ?1")
    public Integer getTransactionCount(String invoiceHeaderId);
}
