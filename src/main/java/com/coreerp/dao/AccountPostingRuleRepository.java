package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.AccountPostingRule;
import com.coreerp.model.TransactionType;

public interface AccountPostingRuleRepository extends JpaRepository<AccountPostingRule, Long> {

	public AccountPostingRule findByTransactionType(TransactionType transactionType);
}
