package com.coreerp.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.coreerp.reports.model.WeeklyTransactionReportModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.model.FinancialYear;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.Party;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;
import org.springframework.data.repository.query.Param;

public interface InvoiceHeaderRepository extends JpaRepository<InvoiceHeader, String> {

	@Query("select i from InvoiceHeader i where i.id = ?1")
	InvoiceHeader findByHeaderId(String headerId);

	InvoiceHeader findByInvoiceNumber(String invoiceNumber);

	@Query("select max(i.invId) from InvoiceHeader i where i.invoiceNumber like CONCAT(?1, '%')")
	Long getMaxInvoiceNumber(String prefix);
	
//	@Query("select max(i.invId) from InvoiceHeader i join i.invoiceType t where t.id = ?1")
//	@Query("select max(ih.invId) "
//			+ " from InvoiceHeader ih "
//			+ " join TransactionType tt on tt.id = ih.invoiceType.id "
//			+ " join NumberRangeConfiguration nr on nr.transactionType.id = ih.invoiceType.id "
//			+ " where tt.id = ?1 "
//			+ " and ((nr.autoNumberReset = 1 and ih.financialYear.id = ("
//			+ " select id from FinancialYear fy "
//			+ " where fy.isActive = 1 "
//			+ ")) or nr.autoNumberReset = 0) ")
	@Query(value=" SELECT MAX(ih.inv_id) " + 
			" FROM tr_invoice_header ih " + 
			" INNER JOIN cd_transaction_type tt " + 
			" ON(tt.id = ih.invoice_type) " + 
			" INNER JOIN conf_number_range nr " + 
			" ON(nr.trans_type_id = tt.id) " +
			" WHERE (FIND_IN_SET (tt.id, (SELECT transaction_type_ids " +
			" FROM conf_common_sequence "+
			" WHERE FIND_IN_SET(?1, transaction_type_ids) > 0 "+
			")) > 0 OR tt.id = ?1) "+
			" AND ((nr.auto_number_reset = 1 AND ih.financial_year_id = ( " +
			" SELECT fy.id FROM ma_financial_year fy " + 
			" WHERE fy.is_active = 1 " + 
			" )) OR nr.auto_number_reset = 0) ", nativeQuery = true)
	Long getMaxInvoiceNumberByTransactionType(Long transactionTypeId);

	List<InvoiceHeader> findByInvoiceTypeAndInvoiceDateBetween(TransactionType transactionType, Date invoiceFromDate, Date invoiceToDate);

	@Query("select i from InvoiceHeader i join i.invoiceType t join i.party p where " +
			" t.id = :ttypeId" +
			" and  (p.id = :PartyId  or :PartyId is null) " +
			" and (date(i.invoiceDate) >= :invoiceFromDate or :invoiceFromDate is null) " +
			" and (date(i.invoiceDate) <= :invoiceToDate or :invoiceToDate is null) " +
			"")
	List<InvoiceHeader> findByInvoiceTypeAndAndPartyIdAndInvoiceDateGreaterThanEqualOrInvoiceDateLessThanEqual(@Param("ttypeId") Long ttypeId,@Param("PartyId") Long PartyId, @Param("invoiceFromDate") Date invoiceFromDate, @Param("invoiceToDate") Date invoiceToDate );

	List<InvoiceHeader> findByPartyAndInvoiceType(Party party, TransactionType transactionType);
	
	@Query("select i from InvoiceHeader i where i.party in (?1)")
	List<InvoiceHeader> findByPartyList(List<Party> party);

	//@Query("select i from InvoiceHeader i where i.party = ?1 and i.invoiceType = ?2 and dueAmount > 0")
	//List<InvoiceHeader> findByPartyAndInvoiceTypeWithNonZeroDueAmount(Party party, TransactionType transactionType);
	
	
	List<InvoiceHeader> findByPartyAndInvoiceTypeAndDueAmountGreaterThanAndStatusNotInOrderByInvoiceDateAscInvIdAsc(Party party, TransactionType transactionType, Double dueAmount, Collection<Status> status);
	
	
	public List<InvoiceHeader> findByPartyAndInvoiceTypeAndStatusNotIn(Party party, TransactionType transactionType, Collection<Status> status);
	
	Long countByInvIdGreaterThanAndInvoiceTypeAndFinancialYear(Long invId, TransactionType invType,FinancialYear financialYear);
	
	InvoiceHeader findByInvoiceNumberAndInvoiceType(String invNumber, TransactionType invType);
	
	Long countByInvoiceNumberAndInvoiceTypeAndFinancialYear(String invNumber, TransactionType invType,FinancialYear financialYear);
	
	boolean existsByFinancialYearAndInvoiceTypeIn(FinancialYear financialYear, List<TransactionType> transactionTypes);

    Long countByInvoiceNumberAndInvoiceTypeAndPartyAndFinancialYear(String invNumber, TransactionType invType, Party party,FinancialYear financialYear);

	@Query(value= "select count(ih.inv_id) "+
			" from tr_invoice_header ih " +
			" where ih.inv_id > :invId " +
			" and  ih.invoice_type IN(:list) " +
			" and ih.financial_year_id = :financialYearId ",nativeQuery = true)
	Long countMaxInvCount(@Param("invId") Long invId, @Param("list") List<Long> list, @Param("financialYearId") Long financialYearId);

    @Query(value = "select transaction_type_ids from conf_common_sequence where find_in_set(:invoiceTypeId, transaction_type_ids) > 0", nativeQuery = true)
    String getCommonSequence(Long invoiceTypeId);






    public List<InvoiceHeader> findByInvoiceTypeAndFinancialYearAndParty( TransactionType transactionType,FinancialYear financialYear,Party party);

	InvoiceHeader findByInvoiceNumberAndFinancialYear(String invoiceNumber,FinancialYear financialYear);





	@Query(
			"SELECT NEW com.coreerp.reports.model.WeeklyTransactionReportModel("
					+ " count(inh.id) AS invoiceCount  "
					+ " ,sum(inh.grandTotal) AS invoiceAmount "
					+ " ,sum(inh.dueAmount) AS BalanecAmount "
					+ " ,sum(inh.dueAmount) AS paidAmount "
					+ " ) "
					+ " FROM InvoiceHeader inh "
					+ " join inh.invoiceType inType "
					+ " where 1 = 1 "
					+ " and inType.id = :transactionType "
					+ " and (date(inh.invoiceDate) >= :fromDate or :fromDate is null) "
					+ " and (date(inh.invoiceDate) <= :toDate or :toDate is null) "
	)
	public WeeklyTransactionReportModel getWeeklyReport(
			@Param("fromDate") Date fromDate
			,@Param("toDate") Date toDate
			,@Param("transactionType") Long transactionType

	);








	@Query("select sum(i.dueAmount) from InvoiceHeader i join i.invoiceType t join i.party p where " +
			" t.id = :ttypeId" +
			 " AND (p.name like %:partyName%) "+
			"")
	Double getDueAmountOfParty(@Param("partyName") String partyName,@Param("ttypeId") Long ttypeId);





}
