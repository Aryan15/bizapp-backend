package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.ExpenseHeader;


public interface ExpenseRepository extends JpaRepository<ExpenseHeader, Long> {

	List<ExpenseHeader> findByIdIn(List<Long> ids);
}
