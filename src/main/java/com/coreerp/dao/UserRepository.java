package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	
	User findByEmail(String email);

	User findByUsername(String username);

}
