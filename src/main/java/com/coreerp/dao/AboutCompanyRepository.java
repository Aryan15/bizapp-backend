package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.AboutCompany;

public interface AboutCompanyRepository extends JpaRepository<AboutCompany, Long>{

}
