package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.model.Country;
import com.coreerp.model.State;

public interface StateRepository extends JpaRepository<State, Long> {

	public List<State> findByCountry(Country country);
	
	@Modifying
	@Query("delete from State t where t.id = ?1")
	void deleteForce(Long id);
}
