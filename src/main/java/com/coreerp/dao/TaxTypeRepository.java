package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.TaxType;

public interface TaxTypeRepository extends JpaRepository<TaxType, Long> {

}
