package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.NamePrefix;

public interface NamePrefixRepository extends JpaRepository<NamePrefix, Long> {

}
