package com.coreerp.dao;

import com.coreerp.model.InvoiceHeaderAudit;
import com.coreerp.model.InvoiceItemAudit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvoiceItemAuditRepository extends JpaRepository<InvoiceItemAudit, String>  {

    List<InvoiceItemAudit> findByInvoiceHeaderId(String headerId);
}
