package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.model.ExpenseHeader;
import com.coreerp.model.Party;
import com.coreerp.model.VoucherHeader;
import com.coreerp.model.VoucherItem;

public interface VoucherItemRepository extends JpaRepository<VoucherItem, String> {

	@Query("select i from VoucherItem i join i.voucherHeader h where h.id = ?1")
	List<VoucherItem> findByHeaderId(String headerId);
	
	List<VoucherItem> findByVoucherHeader(VoucherHeader voucherHeader);
	
	@Modifying
	@Query("delete from VoucherItem g where g.id = ?1")
	void deleteForce(String id);
	
	boolean existsByExpenseHeaderIn(List<ExpenseHeader> expenseHeaders);
	
}