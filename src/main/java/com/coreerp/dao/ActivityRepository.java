package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Activity;

public interface ActivityRepository extends JpaRepository<Activity, Long> {

}
