package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Activity;
import com.coreerp.model.ActivityRoleBinding;
import com.coreerp.model.Role;
import com.coreerp.model.User;

public interface ActivityRoleBindingRepository extends JpaRepository<ActivityRoleBinding, Long> {

	public List<ActivityRoleBinding> findByUser(User user);
	public List<ActivityRoleBinding> findByUserAndActivity(User user, Activity activity);
	public List<ActivityRoleBinding> findByRole(Role role);
	public List<ActivityRoleBinding> findByActivity(Activity activity);
	public ActivityRoleBinding findByRoleAndActivity(Role role, Activity activity);
	
	
}
