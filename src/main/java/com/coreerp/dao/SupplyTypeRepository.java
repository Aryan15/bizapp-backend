package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.SupplyType;

public interface SupplyTypeRepository extends JpaRepository<SupplyType, Long> {

	
}
