package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.PurchaseOrderItem;
import com.coreerp.model.TransactionPK;

public interface PurchaseOrderItemRepository extends JpaRepository<PurchaseOrderItem, String> {

}
