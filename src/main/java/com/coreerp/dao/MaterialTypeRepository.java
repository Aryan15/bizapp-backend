package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.MaterialType;

public interface MaterialTypeRepository extends JpaRepository<MaterialType, Long> {
	
	MaterialType findByName(String name);

}
