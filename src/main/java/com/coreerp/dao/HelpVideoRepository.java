package com.coreerp.dao;

import com.coreerp.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface HelpVideoRepository extends JpaRepository<HelpVideoModel, Long> {



    @Query("select h from HelpVideoModel h where h.activityId =:activityId or :activityId is null")
    public Page<HelpVideoModel> getHelpVideoByHeader(Long activityId,Pageable pageable);

    @Query("select h from HelpVideoModel h where h.activityId =:activityId or :activityId is null")
    public List<HelpVideoModel> getHelpVideoByHeaderList(Long activityId);


}
