package com.coreerp.dao;

import com.coreerp.model.AutoTransactionGeneration;
import com.coreerp.model.PettyCashHeader;
import com.coreerp.model.PettyCashItem;
import com.coreerp.model.TransactionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PettyCashRepository extends JpaRepository<PettyCashHeader, String> {

   Long countByPettyCashNumberAndPettyCashTypeId(String PettyCashNumber, TransactionType id );



   @Query(value=" SELECT MAX(ih.petty_cash_id) " +
           " FROM tr_pettycash_header ih " +
           " INNER JOIN cd_transaction_type tt " +
           " ON(tt.id = ih.petty_cash_type_id) " +
           " INNER JOIN conf_number_range nr " +
           " ON(nr.trans_type_id = tt.id) " +
           " WHERE (FIND_IN_SET (tt.id, (SELECT transaction_type_ids " +
           " FROM conf_common_sequence "+
           " WHERE FIND_IN_SET(?1, transaction_type_ids) > 0 "+
           ")) > 0 OR tt.id = ?1) "+
           " AND ((nr.auto_number_reset = 1 AND ih.financial_year_id = ( " +
           " SELECT fy.id FROM ma_financial_year fy " +
           " WHERE fy.is_active = 1 " +
           " )) OR nr.auto_number_reset = 0) ", nativeQuery = true)
   Long getMaxPettyCashNumberByTransactionType(Long transactionTypeId);



   @Query("select i from PettyCashHeader i where i.isLatest =?1")
   public PettyCashHeader getPettyCashHeader(Long value);



   @Query("select i from PettyCashHeader i ")
   public List<PettyCashHeader> getAllTransaction();

   @Query("select h from PettyCashHeader h where concat(year(h.pettyCashDate), '-', month(h.pettyCashDate)) = :monthYear and h.isLatest = :isLatest ")
   public PettyCashHeader findByMonthAndIsLatest(@Param("monthYear") String monthYear,  @Param("isLatest") Long isLatest);
}
