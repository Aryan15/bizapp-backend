package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Area;
import com.coreerp.model.City;
import org.springframework.data.jpa.repository.Query;

public interface AreaRepository extends JpaRepository<Area, Long> {
	
	public List<Area> findByCity(City city);


	@Query("select i.name from Area i where i.id = ?1")
	public  String getAreaName(Long id);

}
