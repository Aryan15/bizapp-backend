package com.coreerp.dao;

import java.util.Date;
import java.util.List;

import com.coreerp.model.StockTraceReport;

public interface StockTraceReportRepository {

	public List<StockTraceReport> getReport(Date fromDate, Date toDate);
	
	public List<StockTraceReport> getReportForMaterial(Date fromDate, Date toDate, Long materialId);


}
