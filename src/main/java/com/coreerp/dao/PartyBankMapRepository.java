package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.model.Party;
import com.coreerp.model.PartyBankMap;

public interface PartyBankMapRepository extends JpaRepository<PartyBankMap, Long> {

	List<PartyBankMap> findByParty(Party party);
	
	@Query("select p from PartyBankMap p join p.party pt where pt.id = ?1")
	List<PartyBankMap> findByPartyId(Long partyId);
	
	@Query("select p from PartyBankMap p join p.company c where c.id = ?1")
	List<PartyBankMap> findByCompanyId(Long companyId);
	
	
}
