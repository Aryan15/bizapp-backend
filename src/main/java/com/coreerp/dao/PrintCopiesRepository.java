package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.coreerp.model.PrintCopy;

public interface PrintCopiesRepository extends JpaRepository<PrintCopy, Long> {

}
