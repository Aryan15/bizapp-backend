package com.coreerp.dao;

import com.coreerp.model.PettyCashItem;
import com.coreerp.model.VoucherItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface PettyCashItemRepository  extends JpaRepository<PettyCashItem, String> {


    @Modifying
    @Query("delete from PettyCashItem g where g.id = ?1")
    void deleteForce(String id);
}
