package com.coreerp.dao;

import com.coreerp.model.InvoiceHeaderAudit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceHeaderAuditRepository extends JpaRepository<InvoiceHeaderAudit, String> {
}
