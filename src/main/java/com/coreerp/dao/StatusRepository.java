package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;

public interface StatusRepository extends JpaRepository<Status, Long> {

	Status findByName(String name);

	Status findByTransactionTypeAndName(TransactionType transactionType, String name);

}
