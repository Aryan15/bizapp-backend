package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.GRNHeader;
import com.coreerp.model.GRNItem;
import com.coreerp.model.InvoiceItem;

public interface GRNItemRepository extends JpaRepository<GRNItem, String> {

	@Query("select i from GRNItem i join i.grnHeader h where h.id = ?1")
	List<GRNItem> findByHeaderId(String headerId);
	
	List<GRNItem> findByGrnHeader(GRNHeader grnHeader);
	
	@Modifying
	@Query("delete from GRNItem g where g.id = ?1")
	void deleteForce(String id);
	
}
