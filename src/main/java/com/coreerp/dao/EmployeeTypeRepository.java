package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.EmployeeType;

public interface EmployeeTypeRepository extends JpaRepository<EmployeeType, Long> {

}
