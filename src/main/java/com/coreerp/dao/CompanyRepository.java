package com.coreerp.dao;

import com.coreerp.model.EmailSetting;
import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Company;
import org.springframework.data.jpa.repository.Query;

public interface CompanyRepository extends JpaRepository<Company, Long> {


    @Query("select  u from Company u")
    Company getCompany();
}
