package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.GlobalSetting;

public interface GlobalSettingRepository extends JpaRepository<GlobalSetting, Long> {

}
