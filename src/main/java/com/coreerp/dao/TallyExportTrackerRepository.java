package com.coreerp.dao;

import com.coreerp.model.TallyExportTracker;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TallyExportTrackerRepository extends PagingAndSortingRepository<TallyExportTracker, Long> {

}
