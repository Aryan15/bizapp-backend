package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


import com.coreerp.model.Material;
import com.coreerp.model.StockTrace;

public interface StockTraceRepository extends JpaRepository<StockTrace, String> {

	List<StockTrace> findByTransactionHeaderId(String transactionHeaderId);

	@Query("select s from StockTrace s "
			+ "where s.transactionId = ?1 "
			+ "and s.transactionIdSequence = (select max(transactionIdSequence) "
			+ "									from StockTrace st "
			+ "									where st.transactionId = s.transactionId ) ")
	StockTrace findByTransactionId(String transactionId);

	@Query(value="select  COUNT( * ) from tr_stock_trace st  where st.material_id=?", nativeQuery = true)
	public Long getMatreialCountByMatreialId(Long id);
	
	 public  void  deleteByMaterial(Material material);

	@Modifying
	@Query("delete from StockTrace t where t.transactionHeaderId = ?1")
	public  void  deleteByTransactionType(String id);
	
}
