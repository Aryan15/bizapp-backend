package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.NumberRangeConfiguration;
import com.coreerp.model.TransactionType;

public interface NumberRangeConfigurationRepository extends JpaRepository<NumberRangeConfiguration, Long> {


	NumberRangeConfiguration findByTransactionType(TransactionType transactionType);

}
