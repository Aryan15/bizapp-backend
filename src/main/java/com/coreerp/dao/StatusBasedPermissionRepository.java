package com.coreerp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Status;
import com.coreerp.model.StatusBasedPermission;

public interface StatusBasedPermissionRepository extends JpaRepository<StatusBasedPermission, Long> {

	
	public StatusBasedPermission findByStatus(Status status);
}
