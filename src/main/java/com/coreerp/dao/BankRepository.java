package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.Bank;

public interface BankRepository extends JpaRepository<Bank, Long> {
	
	public List<Bank> findByBankName(String bankName);

}
