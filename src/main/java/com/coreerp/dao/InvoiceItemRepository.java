package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.InvoiceItem;
import com.coreerp.model.PurchaseOrderHeader;

public interface InvoiceItemRepository extends JpaRepository<InvoiceItem, String> {

	@Query("select i from InvoiceItem i join i.invoiceHeader h where h.id = ?1")
	List<InvoiceItem> findByHeaderId(String headerId);

	@Query("select i from InvoiceItem i " +
			" join i.deliveryChallanItem di " +
			" join di.deliveryChallanHeader dh " +
			" where dh.id = ?1 ")
	List<InvoiceItem> findByDeliveryChallanHeader(String deliveryChallanHeaderId);

	@Query("select i from InvoiceItem i " +
			" join i.purchaseOrderItem pi " +
			" join pi.purchaseOrderHeader ph " +
			" where ph.id = ?1 ")
	List<InvoiceItem> findByPurchaseOrderHeader(String purchaseOrderHeaderId);

	@Query("select count(i) from InvoiceItem i join i.invoiceHeader h where h.id = ?1")
	        Long findCountByHeaderId(String headerId);


}
