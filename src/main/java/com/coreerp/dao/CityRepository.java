package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.City;
import com.coreerp.model.State;

public interface CityRepository extends JpaRepository<City, Long> {

	public List<City> findByState(State state);
}
