package com.coreerp.dao;

import com.coreerp.model.EwayBillTracking;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.Material;
import com.coreerp.model.SupplyType;
import com.coreerp.reports.model.BalanceReport;
import com.coreerp.reports.model.EWayBillReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public interface EwayBillTrackingRepository extends JpaRepository<EwayBillTracking, Long> {

    List<EwayBillTracking> findByInvoiceHeader(InvoiceHeader invoiceHeader);

    List<EwayBillTracking> findByInvoiceHeaderAndStatusNotIn(InvoiceHeader invoiceHeader, ArrayList<Integer> statusId);

    EwayBillTracking findByInvoiceHeaderAndIsCurrentAndStatusIn(InvoiceHeader invoiceHeader, Integer isCurrent, ArrayList<Integer> statusId);

    EwayBillTracking findByInvoiceHeaderAndIsCurrent(InvoiceHeader invoiceHeader, Integer isCurrent);

    EwayBillTracking findByEWayBillNo(String eWayNum);

    List<EwayBillTracking> findByStatus(Integer status);

}
