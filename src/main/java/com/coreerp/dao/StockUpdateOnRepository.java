package com.coreerp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coreerp.model.PartyType;
import com.coreerp.model.StockUpdateOn;

public interface StockUpdateOnRepository extends JpaRepository<StockUpdateOn, Long>  {

	public List<StockUpdateOn> findByPartyTypeIn(List<PartyType> partyTypes);
}
