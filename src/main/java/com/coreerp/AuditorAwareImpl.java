package com.coreerp;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class AuditorAwareImpl implements AuditorAware<String> {

	final static Logger log = LogManager.getLogger(AuditorAwareImpl.class);
	
	@Override
	public Optional<String> getCurrentAuditor() {
		
		//log.info("in AuditorAwareImpl: "+SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		
//		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//		String currentPrincipalName = authentication.getName();
//		
//		log.info("in AuditorAwareImpl: currentPrincipalName: "+currentPrincipalName);
//		
		
		//return "test";
		try{
			//String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
//			Optional<String> username = (Optional<String>) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			Optional<String> username = Optional.of(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
			
//			Optional<String> username = Optional.of(SecurityContextHolder.getContext().getAuthentication().getPrincipal().);
			
			log.info("got username: "+username.isPresent());
			return username;
		}catch(Exception e){
			log.info("Exception: "+e);
			return null;
		}
		
	}

}
