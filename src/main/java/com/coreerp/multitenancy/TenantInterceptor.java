package com.coreerp.multitenancy;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.UserTenantRelationRepository;
import com.coreerp.jwt.JwtTokenUtil;
import com.coreerp.model.UserTenantRelation;
import com.coreerp.serviceimpl.DeactivatedUserService;

@Component
public class TenantInterceptor extends HandlerInterceptorAdapter {

	private static Logger logger = LogManager.getLogger(TenantInterceptor.class.getName());
    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    JwtTokenUtil jwtTokenUtil;
    
    @Autowired
    UserTenantRelationRepository userTenantRelationRepository;
    
    @Autowired
    DeactivatedUserService deactivatedUserService;
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        String authToken = request.getHeader(this.tokenHeader);
        //logger.info("authToken: "+authToken);
        String tenantId = "";
        
        

		//		+ ":: Start Time=" + System.currentTimeMillis());
        
        
        if(authToken != null && !authToken.isEmpty()){
        	tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);
        	String username = jwtTokenUtil.getUsernameFromToken(authToken);
			MDC.put("client", tenantId);
			MDC.put("user", username);
			logger.info("Request URL::" + request.getRequestURL());
        	Date subscriptionEndDate = jwtTokenUtil.getSubscriptionEndDateFromToken(authToken);
        	
        	if(subscriptionEndDate != null && subscriptionEndDate.before(new Date())) {
        		logger.info("Checking if user active");
        		TenantContext.setCurrentTenant(ApplicationConstants.DEFAULT_TENANT_ID);
        		UserTenantRelation userTenantRelation = userTenantRelationRepository.findByTenantAndIsActiveAndIsPrimary(tenantId, 1, 1);
        		
        		if(userTenantRelation == null) {
        			logger.info("User is not subscribed. cannot access");
        			throw new InvalidClientException();
        		}
        		
        	}
        	//Check if user is deactivated
        	
        	if(deactivatedUserService.findByTenant(tenantId)) {
        		logger.info("User deactivated. cannot access");
    			throw new InvalidClientException();
        	}
        	
        	if(request.getRequestURL().toString().contains("/new-employee")){
        		//When a new Employee gets created who needs login
        		//An entry in user_tenant_relation of central schema/DEFAULT_TENANT_ID (mybos_coreerp)
        		//needs to be created. otherwise Employee cannot login because 
        		//no schema mapping will be found in the user_tenant_relation
        		tenantId = ApplicationConstants.DEFAULT_TENANT_ID;
        		//logger.info("/new-employee tenantId: "+tenantId);
        		
        	}
        	if(request.getRequestURL().toString().contains("/check-username")){
        		//Need to lookup user_tenant_relation in schema/DEFAULT_TENANT_ID (mybos_coreerp)
        		//This is required to check uniqueness of the username
        		tenantId = ApplicationConstants.DEFAULT_TENANT_ID;
        		//logger.info("/check-username tenantId: "+tenantId);
        		
        	}
        }else{
        	
        	if(request.getRequestURL().toString().contains("/auth") ||
					request.getRequestURL().toString().contains("/tally")){
        		tenantId = TenantContext.getCurrentTenant();
        	}
        	else{
        		//logger.info("Incorrect credentials");
        		logger.error("Incorrect credentials");
				logger.info("Request URL::" + request.getRequestURL());

        		throw new InvalidClientException();
        	}
        }
        
//        logger.info("tenantId: "+tenantId);
        
        TenantContext.setCurrentTenant(tenantId);

        
        //Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        //String currentPrincipalName = authentication.getName();
        
        //logger.info("preHandle currentPrincipalName: "+currentPrincipalName);
        
        return true;
    }

    @Override
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
        TenantContext.clear();
    }
}
