package com.coreerp.multitenancy;


import java.util.concurrent.Callable;

import com.coreerp.ApplicationConstants;

public abstract class UnboundTenantTask<T> implements Callable<T> {

    protected String username;

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public T call() throws Exception {
        TenantContext.setCurrentTenant(ApplicationConstants.DEFAULT_TENANT_ID);
        return callInternal();
    }

    protected abstract T callInternal();
}