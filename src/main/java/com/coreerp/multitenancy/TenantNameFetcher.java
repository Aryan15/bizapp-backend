package com.coreerp.multitenancy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dao.UserTenantRelationRepository;
import com.coreerp.dto.UserTenantRelationDTO;
import com.coreerp.reports.dao.UserTenantRelationDTORepository;


@Component
public class TenantNameFetcher extends UnboundTenantTask<UserTenantRelationDTO> {

//    @Autowired
//    private UserTenantRelationRepository userTenantRelationRepository;

	@Autowired
	UserTenantRelationDTORepository userTenantRelationDTORepository;
	
//    @Override
//    protected UserTenantRelation callInternal() {
//        UserTenantRelation utr = userTenantRelationRepository.findByUsername(this.username);
//        return utr;
//    }
    
    @Override
    protected UserTenantRelationDTO callInternal() {
    	UserTenantRelationDTO utr = userTenantRelationDTORepository.getUserTenantRelationDTO(this.username);
        return utr;
    }
}
