package com.coreerp.multitenancy;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNAUTHORIZED, reason="Invalid Client Token")
public class InvalidClientException extends RuntimeException  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6877824151028505912L;

}

