package com.coreerp.tallyexternal.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class BasicBuyerAddressExList {

    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type ="String";

    @JsonProperty("BASICBUYERADDRESS")
    private String basicBuyerAddressBNo;

    @JsonProperty("BASICBUYERADDRESS  ")
    private String basicBuyerAddressRod;

    @JsonProperty("BASICBUYERADDRESS   ")
    private String basicBuyerArea;

    @JsonProperty("BASICBUYERADDRESS    ")
    private String basicBuyerCity;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBasicBuyerAddressBNo() {
        return basicBuyerAddressBNo;
    }

    public void setBasicBuyerAddressBNo(String basicBuyerAddressBNo) {
        this.basicBuyerAddressBNo = basicBuyerAddressBNo;
    }

    public String getBasicBuyerAddressRod() {
        return basicBuyerAddressRod;
    }

    public void setBasicBuyerAddressRod(String basicBuyerAddressRod) {
        this.basicBuyerAddressRod = basicBuyerAddressRod;
    }

    public String getBasicBuyerArea() {
        return basicBuyerArea;
    }

    public void setBasicBuyerArea(String basicBuyerArea) {
        this.basicBuyerArea = basicBuyerArea;
    }

    public String getBasicBuyerCity() {
        return basicBuyerCity;
    }

    public void setBasicBuyerCity(String basicBuyerCity) {
        this.basicBuyerCity = basicBuyerCity;
    }
}
