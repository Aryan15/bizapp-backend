package com.coreerp.tallyexternal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GstLeadgerExList {

    @JsonProperty("LEDGERNAME")
    private String ledgerName;

    @JsonProperty("ISDEEMEDPOSITIVE")
    private String isDeemedPositive;

    @JsonProperty("ISPARTYLEDGER")
    private String isPartyLeadger;

    @JsonProperty("AMOUNT")
    private Double amount;


    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getIsDeemedPositive() {
        return isDeemedPositive;
    }

    public void setIsDeemedPositive(String isDeemedPositive) {
        this.isDeemedPositive = isDeemedPositive;
    }

    public String getIsPartyLeadger() {
        return isPartyLeadger;
    }

    public void setIsPartyLeadger(String isPartyLeadger) {
        this.isPartyLeadger = isPartyLeadger;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
