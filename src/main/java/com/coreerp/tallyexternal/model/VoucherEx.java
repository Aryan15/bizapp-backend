package com.coreerp.tallyexternal.model;

import com.coreerp.tally.model.voucher.RequestDesc;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class VoucherEx {
    @JsonProperty("ADDRESS.LIST")
    private AddressListEx addressListEx;

    @JsonProperty("DATE")
    private  String date;

    @JsonProperty("STATENAME")
    private String stateName;

    @JsonProperty("COUNTRYOFRESIDENCE")
    private String countryName;

    @JsonProperty("PARTYNAME")
    private String partyName;

    @JsonProperty("PARTYGSTIN")
    private String partyGstin;


    @JsonProperty("VOUCHERTYPENAME")
    private String voucherTypeName;

    @JsonProperty("VOUCHERNUMBER")
    private String voucherNumber;

    @JsonProperty("PARTYLEDGERNAME")
    private  String partyLeadgerName;

    @JsonProperty("BASICBASEPARTYNAME")
    private String basicPartyName;

    @JsonProperty("PERSISTEDVIEW")
    private String persistedView;

    @JsonProperty("PLACEOFSUPPLY")
    private String placeOfSupply;

    @JsonProperty("BASICBUYERNAME")
    private String basicBuyerName;

    @JsonProperty("CONSIGNEEGSTIN")
    private String consigneeGstin;

    @JsonProperty("BASICDUEDATEOFPYMT")
    private String basicDueDateOfPymt;

    @JsonProperty("LEDGERENTRIES.LIST")
    private LeadgerEntriesExList leadgerEntriesExList;

    @JsonProperty("LEDGERENTRIES.LIST ")
    private GstLeadgerExList cgstLeadgerExList;

    @JsonProperty("LEDGERENTRIES.LIST  ")
    private GstLeadgerExList sgstLeadgerExList;

//    @JsonProperty("LEDGERENTRIES.LIST   ")
//    private GstLeadgerExList igstLeadgerExList;

    @JsonProperty("ALLINVENTORYENTRIES.LIST")
    private List<AllInventryExList> allInventryExList;
    @JsonProperty("BASICBUYERADDRESS.LIST")
    private BasicBuyerAddressExList basicBuyerAddressExList;


    public String getConsigneeGstin() {
        return consigneeGstin;
    }

    public void setConsigneeGstin(String consigneeGstin) {
        this.consigneeGstin = consigneeGstin;
    }

    public String getBasicDueDateOfPymt() {
        return basicDueDateOfPymt;
    }

    public void setBasicDueDateOfPymt(String basicDueDateOfPymt) {
        this.basicDueDateOfPymt = basicDueDateOfPymt;
    }

    public LeadgerEntriesExList getLeadgerEntriesExList() {
        return leadgerEntriesExList;
    }

    public void setLeadgerEntriesExList(LeadgerEntriesExList leadgerEntriesExList) {
        this.leadgerEntriesExList = leadgerEntriesExList;
    }

    public GstLeadgerExList getCgstLeadgerExList() {
        return cgstLeadgerExList;
    }

    public void setCgstLeadgerExList(GstLeadgerExList cgstLeadgerExList) {
        this.cgstLeadgerExList = cgstLeadgerExList;
    }

    public GstLeadgerExList getSgstLeadgerExList() {
        return sgstLeadgerExList;
    }

    public void setSgstLeadgerExList(GstLeadgerExList sgstLeadgerExList) {
        this.sgstLeadgerExList = sgstLeadgerExList;
    }

//    public GstLeadgerExList getIgstLeadgerExList() {
//        return igstLeadgerExList;
//    }
//
//    public void setIgstLeadgerExList(GstLeadgerExList igstLeadgerExList) {
//        this.igstLeadgerExList = igstLeadgerExList;
//    }

    public List<AllInventryExList> getAllInventryExList() {
        return allInventryExList;
    }

    public void setAllInventryExList(List<AllInventryExList> allInventryExList) {
        this.allInventryExList = allInventryExList;
    }

    public BasicBuyerAddressExList getBasicBuyerAddressExList() {
        return basicBuyerAddressExList;
    }

    public void setBasicBuyerAddressExList(BasicBuyerAddressExList basicBuyerAddressExList) {
        this.basicBuyerAddressExList = basicBuyerAddressExList;
    }

    public AddressListEx getAddressListEx() {
        return addressListEx;
    }

    public void setAddressListEx(AddressListEx addressListEx) {
        this.addressListEx = addressListEx;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getVoucherTypeName() {
        return voucherTypeName;
    }

    public void setVoucherTypeName(String voucherTypeName) {
        this.voucherTypeName = voucherTypeName;
    }

    public String getVoucherNumber() {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public String getPartyLeadgerName() {
        return partyLeadgerName;
    }

    public void setPartyLeadgerName(String partyLeadgerName) {
        this.partyLeadgerName = partyLeadgerName;
    }

    public String getBasicPartyName() {
        return basicPartyName;
    }

    public void setBasicPartyName(String basicPartyName) {
        this.basicPartyName = basicPartyName;
    }

    public String getPersistedView() {
        return persistedView;
    }

    public void setPersistedView(String persistedView) {
        this.persistedView = persistedView;
    }

    public String getPlaceOfSupply() {
        return placeOfSupply;
    }

    public void setPlaceOfSupply(String placeOfSupply) {
        this.placeOfSupply = placeOfSupply;
    }

    public String getBasicBuyerName() {
        return basicBuyerName;
    }

    public void setBasicBuyerName(String basicBuyerName) {
        this.basicBuyerName = basicBuyerName;
    }

    public String getPartyGstin() {
        return partyGstin;
    }

    public void setPartyGstin(String partyGstin) {
        this.partyGstin = partyGstin;
    }
}
