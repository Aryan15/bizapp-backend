package com.coreerp.tallyexternal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BillAllocationExList {
    @JsonProperty("NAME")
    private  String name;
    @JsonProperty("BILLCREDITPERIOD")
    private String billCreditPeriod;
    @JsonProperty("BILLTYPE")
    private  String billType;
    @JsonProperty("AMOUNT")
    private Double amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBillCreditPeriod() {
        return billCreditPeriod;
    }

    public void setBillCreditPeriod(String billCreditPeriod) {
        this.billCreditPeriod = billCreditPeriod;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
