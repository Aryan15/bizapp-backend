package com.coreerp.tallyexternal.model;

import com.coreerp.tally.model.voucher.OldAuditEntryList;
import com.coreerp.tally.model.voucher.TallyMessageVoucher;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class BodyEx {
    @JacksonXmlProperty(namespace = "TallyUDF", localName = "TALLYMESSAGE")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<TallyMessageEx> tallyMessageExes;

    public List<TallyMessageEx> getTallyMessageExes() {
        return tallyMessageExes;
    }

    public void setTallyMessageExes(List<TallyMessageEx> tallyMessageExes) {
        this.tallyMessageExes = tallyMessageExes;
    }
}
