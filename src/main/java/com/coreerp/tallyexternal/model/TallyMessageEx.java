package com.coreerp.tallyexternal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TallyMessageEx {

    @JsonProperty("VOUCHER")
 private VoucherEx voucherEx;

    public VoucherEx getVoucherEx() {
        return voucherEx;
    }

    public void setVoucherEx(VoucherEx voucherEx) {
        this.voucherEx = voucherEx;
    }
}
