package com.coreerp.tallyexternal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BatchAllocationExList {

    @JsonProperty("GODOWNNAME")
    private String godownName;

    @JsonProperty("BATCHNAME")
    private String batchName;

    @JsonProperty("DESTINATIONGODOWNNAME")
    private String destinationGodownName;

    @JsonProperty("INDENTNO")
    private String indentNo;

    @JsonProperty("ORDERNO")
    private String orderNo;

    @JsonProperty("TRACKINGNUMBER")
    private String trackingNumber;

    @JsonProperty("AMOUNT")
    private Double amount;

    @JsonProperty("ACTUALQTY")
    private String actulQty;

    @JsonProperty("BILLEDQTY")
    private String billedQty;

    @JsonProperty("MFDON")
    private String mfDon;

    @JsonProperty("EXPIRYPERIOD")
    private String experiaPeriod;

    public String getMfDon() {
        return mfDon;
    }

    public void setMfDon(String mfDon) {
        this.mfDon = mfDon;
    }

    public String getExperiaPeriod() {
        return experiaPeriod;
    }

    public void setExperiaPeriod(String experiaPeriod) {
        this.experiaPeriod = experiaPeriod;
    }

    public String getGodownName() {
        return godownName;
    }

    public void setGodownName(String godownName) {
        this.godownName = godownName;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getDestinationGodownName() {
        return destinationGodownName;
    }

    public void setDestinationGodownName(String destinationGodownName) {
        this.destinationGodownName = destinationGodownName;
    }

    public String getIndentNo() {
        return indentNo;
    }

    public void setIndentNo(String indentNo) {
        this.indentNo = indentNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getActulQty() {
        return actulQty;
    }

    public void setActulQty(String actulQty) {
        this.actulQty = actulQty;
    }

    public String getBilledQty() {
        return billedQty;
    }

    public void setBilledQty(String billedQty) {
        this.billedQty = billedQty;
    }
}