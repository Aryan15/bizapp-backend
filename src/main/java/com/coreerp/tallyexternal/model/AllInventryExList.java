package com.coreerp.tallyexternal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AllInventryExList {

    @JsonProperty("STOCKITEMNAME")
    private String stockItemName;

    @JsonProperty("ISDEEMEDPOSITIVE")
    private String isDeemedPositive;

    @JsonProperty("RATE")
    private String rate;

    @JsonProperty("DISCOUNT")
    private String discount;

    @JsonProperty("AMOUNT")
    private Double amount;

    @JsonProperty("ACTUALQTY")
    private String actualQty;

    @JsonProperty("BILLEDQTY")
    private String billedQty;

    @JsonProperty("BASICUSERDESCRIPTION.LIST")
    private BasicUserDescriptionExList basicUserDescriptionExList;

    @JsonProperty("BATCHALLOCATIONS.LIST")
    private BatchAllocationExList batchAllocationExList;

    public BasicUserDescriptionExList getBasicUserDescriptionExList() {
        return basicUserDescriptionExList;
    }

    public void setBasicUserDescriptionExList(BasicUserDescriptionExList basicUserDescriptionExList) {
        this.basicUserDescriptionExList = basicUserDescriptionExList;
    }

    public BatchAllocationExList getBatchAllocationExList() {
        return batchAllocationExList;
    }

    public void setBatchAllocationExList(BatchAllocationExList batchAllocationExList) {
        this.batchAllocationExList = batchAllocationExList;
    }

    public AccountingAllocationExList getAccountingAllocationExList() {
        return accountingAllocationExList;
    }

    public void setAccountingAllocationExList(AccountingAllocationExList accountingAllocationExList) {
        this.accountingAllocationExList = accountingAllocationExList;
    }

    @JsonProperty("ACCOUNTINGALLOCATIONS.LIST")
    private  AccountingAllocationExList accountingAllocationExList;

    public String getStockItemName() {
        return stockItemName;
    }

    public void setStockItemName(String stockItemName) {
        this.stockItemName = stockItemName;
    }

    public String getIsDeemedPositive() {
        return isDeemedPositive;
    }

    public void setIsDeemedPositive(String isDeemedPositive) {
        this.isDeemedPositive = isDeemedPositive;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }


    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getActualQty() {
        return actualQty;
    }

    public void setActualQty(String actualQty) {
        this.actualQty = actualQty;
    }

    public String getBilledQty() {
        return billedQty;
    }

    public void setBilledQty(String billedQty) {
        this.billedQty = billedQty;
    }
}
