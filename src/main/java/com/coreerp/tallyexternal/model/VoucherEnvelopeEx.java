package com.coreerp.tallyexternal.model;

import com.coreerp.tally.model.voucher.Body;
import com.coreerp.tally.model.voucher.Header;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@JacksonXmlRootElement(localName = "ENVELOPE")
@XmlRootElement
public class VoucherEnvelopeEx {
    @JsonProperty("HEADER")
    @XmlElement
    private HeaderEx headerEx;

    @JsonProperty("ISLASTSYNC")
    private String isLastSync;

    @JsonProperty("BODY")
    @XmlElement
    private BodyEx bodyEx;

    public HeaderEx getHeaderEx() {
        return headerEx;
    }

    public void setHeaderEx(HeaderEx headerEx) {
        this.headerEx = headerEx;
    }

    public BodyEx getBodyEx() {
        return bodyEx;
    }

    public void setBodyEx(BodyEx bodyEx) {
        this.bodyEx = bodyEx;
    }

    public String getIsLastSync() {
        return isLastSync;
    }

    public void setIsLastSync(String isLastSync) {
        this.isLastSync = isLastSync;
    }

//    @Override
//    public String toString() {
//        return "VoucherEnvelope{" +
//                "header='" + header + '\'' +
//                ", bodyEx='" + bodyEx + '\'' +
//                ", isLastSync='" + isLastSync + '\'' +
//                '}';
//    }
}
