package com.coreerp.tallyexternal.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class BasicUserDescriptionExList {

    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type ="String";

    @JsonProperty("BASICUSERDESCRIPTION")
    private String basicUserDescription;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBasicUserDescription() {
        return basicUserDescription;
    }

    public void setBasicUserDescription(String basicUserDescription) {
        this.basicUserDescription = basicUserDescription;
    }
}
