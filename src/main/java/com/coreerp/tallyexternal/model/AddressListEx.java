package com.coreerp.tallyexternal.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class AddressListEx {

    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type ="String";



    @JsonProperty("ADDRESS")
    private String addressBnum ;

    @JsonProperty("ADDRESS ")
    private String addressRoad;

    @JsonProperty("ADDRESS  ")
    private String addressArea;

    @JsonProperty("ADDRESS   ")
    private String addressCity;


















    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddressBnum() {
        return addressBnum;
    }

    public void setAddressBnum(String addressBnum) {
        this.addressBnum = addressBnum;
    }

    public String getAddressRoad() {
        return addressRoad;
    }

    public void setAddressRoad(String addressRoad) {
        this.addressRoad = addressRoad;
    }

    public String getAddressArea() {
        return addressArea;
    }

    public void setAddressArea(String addressArea) {
        this.addressArea = addressArea;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }
}
