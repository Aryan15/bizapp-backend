package com.coreerp.tallyexternal.service;


import com.coreerp.ApplicationConstants;
import com.coreerp.dao.InvoiceHeaderRepository;
import com.coreerp.dao.StateRepository;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.InvoiceItem;
import com.coreerp.model.TransactionType;
import com.coreerp.service.GlobalSettingService;
import com.coreerp.tally.model.voucher.Inventory;
import com.coreerp.tally.model.voucher.TallyMessageVoucher;
import com.coreerp.tally.model.voucher.Voucher;
import com.coreerp.tally.service.VoucherService;
import com.coreerp.tallyexternal.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class VoucherServiceEx {
    final static Logger log = LogManager.getLogger(VoucherServiceEx.class);

    @Autowired
    InvoiceHeaderRepository invoiceHeaderRepository;

    @Autowired
    StateRepository stateRepository;

    @Autowired
    GlobalSettingService globalSettingService;

    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    double totalGrandTotal = 0.0;


    public List<TallyMessageEx> getTallyMessageVoucherList(TransactionType invoiceType, Long partyId, Date invoiceFromDate, Date invoiceToDate) {

        List<InvoiceHeader> invoiceHeaderList = invoiceHeaderRepository.findByInvoiceTypeAndAndPartyIdAndInvoiceDateGreaterThanEqualOrInvoiceDateLessThanEqual(invoiceType.getId(), partyId, invoiceFromDate, invoiceToDate);

        List<TallyMessageEx> outTallyMessageVouchers = new ArrayList<>();
        invoiceHeaderList.forEach(inv -> {
            TallyMessageEx tallyMessageVoucher = getTallyMessageVoucher(inv);
            outTallyMessageVouchers.add(tallyMessageVoucher);

        });

        return outTallyMessageVouchers;

    }


    private TallyMessageEx getTallyMessageVoucher(InvoiceHeader invoiceHeader) {
        TallyMessageEx tallyMessageVoucher = new TallyMessageEx();
        VoucherEx voucher = getVoucher(invoiceHeader);
        tallyMessageVoucher.setVoucherEx(voucher);
        return tallyMessageVoucher;

    }


    private VoucherEx getVoucher(InvoiceHeader invoiceHeader) {
        List<String> salesTypes = new ArrayList<>();
        salesTypes.add(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
        salesTypes.add(ApplicationConstants.INVOICE_TYPE_INCOMING_JW_INVOICE);


        List<String> purchaseTypes = new ArrayList<>();
        purchaseTypes.add(ApplicationConstants.INVOICE_TYPE_SUPPLIER);
        purchaseTypes.add(ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE);

        VoucherEx voucherEx = new VoucherEx();
        voucherEx.setAddressListEx(getAddressListEx(invoiceHeader));
        voucherEx.setBasicBuyerAddressExList(getBasicBuyerAddressList(invoiceHeader));
        voucherEx.setDate(dateFormat.format(invoiceHeader.getInvoiceDate()));
        voucherEx.setStateName(invoiceHeader.getParty().getState().getName());
        voucherEx.setCountryName(invoiceHeader.getParty().getCountry().getName());
        voucherEx.setPartyName(invoiceHeader.getPartyName());
        voucherEx.setVoucherNumber(invoiceHeader.getInvoiceNumber());

        if (salesTypes.contains(invoiceHeader.getInvoiceType().getName())) {
            voucherEx.setVoucherTypeName(ApplicationConstants.TALLY_SALES_VOUCHER);
        } else if (purchaseTypes.contains(invoiceHeader.getInvoiceType().getName())) {
            voucherEx.setVoucherTypeName(ApplicationConstants.TALLY_PURCHASE_VOUCHER);
        }
        voucherEx.setPartyLeadgerName(invoiceHeader.getPartyName());
        voucherEx.setBasicPartyName(invoiceHeader.getPartyName());
        voucherEx.setPersistedView("Invoice Voucher View");
        voucherEx.setPlaceOfSupply(invoiceHeader.getParty().getState().getName());
        voucherEx.setBasicBuyerName(invoiceHeader.getPartyName());
        voucherEx.setPartyGstin(invoiceHeader.getParty().getGstNumber());
        voucherEx.setConsigneeGstin(invoiceHeader.getParty().getGstNumber());
        voucherEx.setBasicDueDateOfPymt(invoiceHeader.getPartyDueDaysLimit());
        voucherEx.setLeadgerEntriesExList(getLeadgerEntriesExList(invoiceHeader));
        voucherEx.setSgstLeadgerExList(getSgstLeadgerExList(invoiceHeader));
        voucherEx.setCgstLeadgerExList(getCgstLeadgerExList(invoiceHeader));
        //voucherEx.setIgstLeadgerExList(getIgstLeadgerExList(invoiceHeader));
        List<AllInventryExList> inventoryList = getAllInventryList(invoiceHeader);
        voucherEx.setAllInventryExList(inventoryList);
        return voucherEx;
    }


    private AddressListEx getAddressListEx(InvoiceHeader invoiceHeader) {

        AddressListEx addressListEx = new AddressListEx();
        if (invoiceHeader.getParty().getArea() != null) {
            addressListEx.setAddressArea(invoiceHeader.getParty().getArea().getName());
        }
        addressListEx.setAddressBnum(invoiceHeader.getParty().getAddress());
        addressListEx.setAddressRoad(invoiceHeader.getParty().getAddress());
        if (invoiceHeader.getParty().getArea() != null) {
            addressListEx.setAddressCity(invoiceHeader.getParty().getCity().getName());
        }

        return addressListEx;


    }

    private BasicBuyerAddressExList getBasicBuyerAddressList(InvoiceHeader invoiceHeader) {

        BasicBuyerAddressExList basicBuyerAddressExList = new BasicBuyerAddressExList();
        if (invoiceHeader.getParty().getArea() != null) {
            basicBuyerAddressExList.setBasicBuyerArea(invoiceHeader.getParty().getArea().getName());
        }
        // basicBuyerAddressExList.setBasicBuyerAddressBNo(invoiceHeader.getParty().getAddress());
        basicBuyerAddressExList.setBasicBuyerAddressRod(invoiceHeader.getParty().getAddress());
        if (invoiceHeader.getParty().getArea() != null) {
            basicBuyerAddressExList.setBasicBuyerCity(invoiceHeader.getParty().getCity().getName());
        }
        return basicBuyerAddressExList;


    }

    private LeadgerEntriesExList getLeadgerEntriesExList(InvoiceHeader invoiceHeader) {
        LeadgerEntriesExList leadgerEntriesExList = new LeadgerEntriesExList();
        leadgerEntriesExList.setLedgerName(invoiceHeader.getPartyName());
        leadgerEntriesExList.setIsDeemedPositive(ApplicationConstants.TALLY_YES_CONSTANT);
        leadgerEntriesExList.setIsPartyLeadger(ApplicationConstants.TALLY_YES_CONSTANT);
        leadgerEntriesExList.setAmount(-1 * invoiceHeader.getGrandTotal());
        leadgerEntriesExList.setBillAllocationExList(getBillAllocationExList(invoiceHeader));
        return leadgerEntriesExList;
    }


    private GstLeadgerExList getSgstLeadgerExList(InvoiceHeader invoiceHeader) {
        GstLeadgerExList gstLeadgerExList = new GstLeadgerExList();
        gstLeadgerExList.setLedgerName(ApplicationConstants.TALLY_TAX_SGST);
        gstLeadgerExList.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLeadgerExList.setIsPartyLeadger(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLeadgerExList.setAmount(invoiceHeader.getSgstTaxAmount());

        return gstLeadgerExList;
    }

    private GstLeadgerExList getCgstLeadgerExList(InvoiceHeader invoiceHeader) {
        GstLeadgerExList gstLeadgerExList = new GstLeadgerExList();
        gstLeadgerExList.setLedgerName(ApplicationConstants.TALLY_TAX_CGST);
        gstLeadgerExList.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLeadgerExList.setIsPartyLeadger(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLeadgerExList.setAmount(invoiceHeader.getCgstTaxAmount());

        return gstLeadgerExList;
    }

    private GstLeadgerExList getIgstLeadgerExList(InvoiceHeader invoiceHeader) {
        GstLeadgerExList gstLeadgerExList = new GstLeadgerExList();
        gstLeadgerExList.setLedgerName(invoiceHeader.getPartyName());
        gstLeadgerExList.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLeadgerExList.setIsPartyLeadger(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLeadgerExList.setAmount(invoiceHeader.getIgstTaxAmount());

        return gstLeadgerExList;
    }


    private BillAllocationExList getBillAllocationExList(InvoiceHeader invoiceHeader) {
        BillAllocationExList billAllocationExList = new BillAllocationExList();
        billAllocationExList.setName(invoiceHeader.getInvoiceNumber());
        billAllocationExList.setBillCreditPeriod(invoiceHeader.getPartyDueDaysLimit());

        billAllocationExList.setBillType("Tax Invoice");
        billAllocationExList.setAmount(-1 * invoiceHeader.getGrandTotal());

        return billAllocationExList;
    }


    private List<AllInventryExList> getAllInventryList(InvoiceHeader invoiceHeader) {
        List<AllInventryExList> outInventoryList = new ArrayList<>();

        invoiceHeader.getInvoiceItems().forEach(item -> {

            AllInventryExList inventory = getInventry(item);
            outInventoryList.add(inventory);


        });


        return outInventoryList;
    }

    private AllInventryExList getInventry(InvoiceItem invoiceItem) {
        AllInventryExList allInventryExList = new AllInventryExList();

        allInventryExList.setBasicUserDescriptionExList(getBasicUserDes(invoiceItem));
        allInventryExList.setStockItemName(invoiceItem.getMaterial().getName());
        allInventryExList.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        if (invoiceItem.getPrice() != null && invoiceItem.getUnitOfMeasurement().getName() != null) {
            allInventryExList.setRate(invoiceItem.getPrice().toString() + "/" + invoiceItem.getUnitOfMeasurement().getName());
        }
        allInventryExList.setAmount(invoiceItem.getAmount());
        allInventryExList.setActualQty(invoiceItem.getQuantity().toString() + "EA");
        allInventryExList.setBilledQty(invoiceItem.getQuantity().toString() + "EA");
        allInventryExList.setDiscount(invoiceItem.getDiscountAmount().toString());
        allInventryExList.setBatchAllocationExList(batchAllocationExList(invoiceItem));
        allInventryExList.setAccountingAllocationExList(getAccountList(invoiceItem));
        return allInventryExList;
    }

    private BasicUserDescriptionExList getBasicUserDes(InvoiceItem invoiceItem) {
        BasicUserDescriptionExList basicUserDescriptionExList = new BasicUserDescriptionExList();
        basicUserDescriptionExList.setBasicUserDescription(invoiceItem.getMaterial().getSpecification());


        return basicUserDescriptionExList;

    }

    private BatchAllocationExList batchAllocationExList(InvoiceItem invoiceItem) {
        BatchAllocationExList batchAllocationExList = new BatchAllocationExList();

        batchAllocationExList.setGodownName(invoiceItem.getInvoiceHeader().getShipToAddress());
        batchAllocationExList.setDestinationGodownName(invoiceItem.getInvoiceHeader().getShipToAddress());
        batchAllocationExList.setMfDon(dateFormat.format(invoiceItem.getInvoiceHeader().getInvoiceDate()));
        batchAllocationExList.setBatchName(invoiceItem.getBatchCode());
        batchAllocationExList.setAmount(invoiceItem.getAmount());
        batchAllocationExList.setActulQty(invoiceItem.getQuantity().toString() + "EA");
        batchAllocationExList.setBilledQty(invoiceItem.getQuantity().toString() + "EA");
        return batchAllocationExList;
    }

    private AccountingAllocationExList getAccountList(InvoiceItem invoiceItem) {
        AccountingAllocationExList accountingAllocationExList = new AccountingAllocationExList();
        accountingAllocationExList.setLeadgerName(ApplicationConstants.TALLY_SALES_VOUCHER);
        accountingAllocationExList.setAmount(invoiceItem.getAmount());
        accountingAllocationExList.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        accountingAllocationExList.setIsPartyLeadger(ApplicationConstants.TALLY_NO_CONSTANT);

        return accountingAllocationExList;
    }

}
