package com.coreerp.tallyexternal.service;

import com.coreerp.ApplicationConstants;
import com.coreerp.model.TransactionType;
import com.coreerp.tally.model.voucher.Body;
import com.coreerp.tally.model.voucher.Envelope;
import com.coreerp.tally.model.voucher.Header;
import com.coreerp.tally.model.voucher.TallyMessageVoucher;
import com.coreerp.tally.service.TallyVoucherService;
import com.coreerp.tallyexternal.model.BodyEx;
import com.coreerp.tallyexternal.model.HeaderEx;
import com.coreerp.tallyexternal.model.TallyMessageEx;
import com.coreerp.tallyexternal.model.VoucherEnvelopeEx;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
public class TallyVoucherServiceEx {
    final static Logger log = LogManager.getLogger(TallyVoucherServiceEx.class);
    @Autowired
    VoucherServiceEx voucherServiceEx;

    public String getVoucherEnvelope(TransactionType invoiceType, Long PartyId, Date invoiceFromDate, Date invoiceToDate) {
        log.info("in getVoucherEnvelope");
        VoucherEnvelopeEx envelope = new VoucherEnvelopeEx();
        envelope.setIsLastSync(ApplicationConstants.TALLY_NO_CONSTANT);
        envelope.setHeaderEx(getHeader());

        envelope.setBodyEx(getBody(invoiceType, PartyId, invoiceFromDate, invoiceToDate));

        XmlMapper xmlMapper = new XmlMapper();
        String outValue = "";
        try {
            outValue = xmlMapper.writeValueAsString(envelope);
            outValue = outValue.replaceAll("\\s+\\>","\\>");
            outValue = outValue.replaceAll("\\s+/\\>","/\\>");
            outValue = outValue.replaceAll("xmlns:wstxns\\d+=","xmlns:UDF=");
            outValue = outValue.replaceAll("wstxns\\d+:", "");

        } catch (JsonProcessingException e) {
            log.info("error: " + e);
        } catch (IOException ex){
            log.info("IO error: " + ex);
        }
        return outValue;
    }


    private HeaderEx getHeader() {
        return new HeaderEx();
    }

    private BodyEx getBody(TransactionType invoiceType, Long PartyId, Date invoiceFromDate, Date invoiceToDate) {
        BodyEx body = new BodyEx();
        body.setTallyMessageExes(getTallyMessageVoucher(invoiceType, PartyId, invoiceFromDate, invoiceToDate));
        return body;
    }


    private List<TallyMessageEx> getTallyMessageVoucher(TransactionType invoiceType, Long PartyId, Date invoiceFromDate, Date invoiceToDate) {
        return voucherServiceEx.getTallyMessageVoucherList(invoiceType, PartyId, invoiceFromDate, invoiceToDate);
    }

}
