package com.coreerp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
//@PropertySource("classpath:application.properties")
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	//@Autowired
	//private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	//@Autowired
	//private DataSource dataSource;
	
	//@Value("${spring.queries.users-query}")
	//private String usersQuery;
	
	//@Value("${spring.queries.roles-query}")
	//private String rolesQuery;

	
//    @Bean
//    public DaoAuthenticationProvider daoAuthenticationProvider(BCryptPasswordEncoder passwordEncoder,
//                                                               UserDetailsService userDetailsService){
// 
//        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
//        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder);
//        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
//        return daoAuthenticationProvider;
//    }
    
//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.jdbcAuthentication()
//			.usersByUsernameQuery(usersQuery)
//			.authoritiesByUsernameQuery(rolesQuery)
//			.dataSource(dataSource)
//			.passwordEncoder(bCryptPasswordEncoder)
//			;
//	}
	
//	@Override
//	protected void configure(HttpSecurity http) throws Exception{
//		
//		http
//		.httpBasic().and()
//		.csrf().disable()
//		.cors().and()
//		.authorizeRequests()
//		.antMatchers("/").permitAll()
//		.antMatchers("/login").permitAll()
//		.antMatchers("/api/**").permitAll()
//		.antMatchers("/registration").permitAll()
//		.antMatchers("/admin/**").hasAuthority("ADMIN").anyRequest()
//		.authenticated().and().csrf().disable().formLogin()
//		.loginPage("/login").failureUrl("/login?error=true")
//		.defaultSuccessUrl("/admin/home")
//		.usernameParameter("username")
//		.passwordParameter("password")
//		.and().logout()
//		.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//		.logoutSuccessUrl("/").and().exceptionHandling()
//		.accessDeniedPage("/access-denied");
//	}
	

	@Override
	protected void configure(HttpSecurity http) throws Exception{
	
		http
		.httpBasic().and()
		.csrf().disable()
		.cors().and()
		.authorizeRequests()		
		.antMatchers("/","/auth/**","/tally/**").permitAll()
		.anyRequest().authenticated()
		.and()
		.addFilter(new JWTAuthorizationFilter(authenticationManager()))
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		;
	}
	
	
	@Override
	public void configure(WebSecurity web) throws Exception {
	    web
	       .ignoring()
	       .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
	}
	
	
	   @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
	   @Override
	   public AuthenticationManager authenticationManagerBean() throws Exception {
	       return super.authenticationManagerBean();
	   }
}
