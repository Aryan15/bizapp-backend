package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.model.*;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.DeliveryChallanItemRepository;
import com.coreerp.dao.InvoiceItemRepository;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.dao.PurchaseOrderHeaderRepository;
import com.coreerp.dao.PurchaseOrderItemRepository;
import com.coreerp.dao.TransactionStatusChangeRepository;
import com.coreerp.dto.PurchaseOrderDTO;
import com.coreerp.dto.PurchaseOrderItemDTO;
import com.coreerp.dto.PurchaseOrderWrapperDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.PurchaseOrderHeaderMapper;
import com.coreerp.mapper.PurchaseOrderItemMapper;
import com.coreerp.service.CompanyService;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.GlobalSettingService;
import com.coreerp.service.MaterialPriceListService;
import com.coreerp.service.MaterialService;
import com.coreerp.service.PartyService;
import com.coreerp.service.PurchaseOrderService;
import com.coreerp.service.QuotationHeaderService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TaxService;
import com.coreerp.service.TransactionTypeService;

@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

	final static Logger log = LogManager.getLogger(PurchaseOrderServiceImpl.class);
	
	@Autowired
	PurchaseOrderHeaderRepository purchaseOrderHeaderRepository;
	
	@Autowired
	DeliveryChallanItemRepository deliveryChallanItemRepository;
	
	@Autowired
	InvoiceItemRepository invoiceItemRepository;
	
	@Autowired
	PurchaseOrderItemRepository purchaseOrderItemRepository;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	FinancialYearService financialYearService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	QuotationHeaderService quotationHeaderService;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	PurchaseOrderHeaderMapper purchaseOrderHeaderMapper;
	
	@Autowired
	//MaterialService materialService;
	MaterialRepository materialRepository;
	
	@Autowired
	PurchaseOrderItemMapper purchaseOrderItemMapper;
	
//	@Autowired
//	PurchaseOrderBackTrackingService purchaseOrderBackTrackingService;

	@Autowired
	PurchaseOrderBackTrackingServiceNew purchaseOrderBackTrackingServiceNew;

	
	@Autowired
	PurchaseOrderStatusService purchaseOrderStatusService;
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	GlobalSettingService globalSettingService;
	
	@Autowired
	TransactionStatusChangeRepository transactionStatusChangeRepository;
	
	@Autowired
	MaterialPriceListService materialPriceListService;
	
	@Autowired
	MaterialService materialService;
	//boolean setStatus = true;
	
//	private PurchaseOrderDTO submit(PurchaseOrderDTO purchaseOrderDTOIn){
//		//update prev version to null if applicable
//		
//		PurchaseOrderHeader poHeader = purchaseOrderHeaderMapper.dtoToModelMap(purchaseOrderDTOIn);
//		
//		if(poHeader.getId() != null){
//			
//			log.info("poHeader.getId(), poHeader.getIdVersion()"+poHeader.getId()+", "+poHeader.getIdVersion());
//			
//			PurchaseOrderHeader poHeaderPre = purchaseOrderHeaderRepository.getOne(new TransactionPK(poHeader.getId(), poHeader.getIdVersion()));
//			
//			log.info("poHeaderPre: "+poHeaderPre);
//			if(poHeaderPre != null){
//				poHeaderPre.setLatest(null);
//	
//				List<PurchaseOrderItem> poItemsPre = poHeaderPre.getPurchaseOrderItems();
//				
//				poItemsPre.forEach(poItem -> {
//										poItem.setLatest(null);
//										poItem.setPurchaseOrderHeader(poHeaderPre);
//										QuotationHeader poItemQHeader = poItem.getQuotationHeader();
//										List<QuotationItem> poItemQItems =  poItemQHeader.getQuotationItems();
//										poItemQItems.forEach(qItem -> {
//											qItem.setLatest(null);
//										});
//										poItemQHeader.setLatest(null);
//										poItemQHeader.setQuotationItems(poItemQItems);
//										poItem.setQuotationHeader(poItemQHeader);
//				
//										});
//				
//				poHeaderPre.setPurchaseOrderItems(poItemsPre);
//				
//				purchaseOrderHeaderRepository.save(poHeaderPre);
//				
//				//increment actual po version
//				
//				poHeader.setIdVersion(poHeader.getIdVersion()+1);
//				poHeader.setLatest("Y");
//				List<PurchaseOrderItem> poItemList = poHeader.getPurchaseOrderItems();
//				poItemList.forEach(poItem -> {
//					poItem.setIdVersion(poItem.getIdVersion() + 1);	
//					
//					QuotationHeader poItemQHeader = poItem.getQuotationHeader();
//					List<QuotationItem> poItemQItems =  poItemQHeader.getQuotationItems();
//					poItemQItems.forEach(qItem -> {
//						qItem.setIdVersion(qItem.getIdVersion() + 1);
//					});
//					poItemQHeader.setIdVersion(poItemQHeader.getIdVersion() + 1);
//					//Back track Quotation linked to PO
//					poItemQHeader.setStatus(ApplicationConstants.QUOTATION_STATUS_COMPLETED); // Change this to get proper status
//					poItemQHeader.setQuotationItems(poItemQItems);
//					poItem.setQuotationHeader(poItemQHeader);
//					
//				});
//				poHeader.setPurchaseOrderItems(poItemList);
//			}
//		}				
//		//Derive status
//		poHeader.setStatus(getPOStatus(poHeader));
//		
//		
//		//Back track Quotation linked to PO
//		
////		List<PurchaseOrderItem> poPreItems = poHeader.getPurchaseOrderItems();
//		List<PurchaseOrderItem> poNewItems = poHeader.getPurchaseOrderItems();
////		
////		List<PurchaseOrderItem> oldAndNewPoItems = new ArrayList<PurchaseOrderItem>();
//		
////		poPreItems.forEach(poPreItem -> {
////			QuotationHeader qPreHeader = poPreItem.getQuotationHeader();
////			qPreHeader.setLatest(null);
////			List<QuotationItem> qPreItems = qPreHeader.getQuotationItems();
////			qPreItems.forEach(qPreItem -> {				
////				qPreItem.setLatest(null);
////				
////			});
////			qPreHeader.setQuotationItems(qPreItems);
////		});
////		
//		
//
//		poNewItems.forEach(poNewItem -> {
//			QuotationHeader qPreHeader = poNewItem.getQuotationHeader();
//			QuotationHeader qNewHeader = poNewItem.getQuotationHeader();
//			qNewHeader.setLatest(null);
//			List<QuotationItem> qNewItems = qNewHeader.getQuotationItems();
//			qNewItems.forEach(qPreItem -> {				
//				qPreItem.setLatest(null);
//				
//			});
//			qNewHeader.setQuotationItems(qNewItems);
//		});
////		
////		oldAndNewPoItems.addAll(poPreItems);
////		oldAndNewPoItems.addAll(poNewItems);
////		
////		poHeader.setPurchaseOrderItems(oldAndNewPoItems);
//		
//		log.info("poHeader before save: "+poHeader);
//		
//		poHeader.getPurchaseOrderItems().forEach( poi -> {
//			log.info("poItem before save:"+poi);
//			log.info("poi.getQuotationHeader: "+poi.getQuotationHeader());
//			poi.getQuotationHeader().getQuotationItems().forEach(qi -> {
//				log.info("qi: "+qi);
//				});
//			});
//		
//		
//		
//		//Save final PO
//		
//		PurchaseOrderHeader poHeaderOut = purchaseOrderHeaderRepository.save(poHeader);
//		
//		return purchaseOrderHeaderMapper.modelToDTOMap(poHeaderOut); 
//		
//	}
	
	@Override
	public PurchaseOrderDTO save(PurchaseOrderWrapperDTO purchaseOrderWrapperDTO) {
		
		PurchaseOrderDTO purchaseOrderDTOIn = purchaseOrderWrapperDTO.getPurchaseOrderHeader();
		
		
		PurchaseOrderHeader poPre = new PurchaseOrderHeader();
		List<PurchaseOrderItem> poItemsPre = new ArrayList<PurchaseOrderItem>();
		
		if(purchaseOrderDTOIn.getId() != null){
			purchaseOrderHeaderRepository.getOne(purchaseOrderDTOIn.getId()).getPurchaseOrderItems().forEach(item -> {
				PurchaseOrderItem poItemPre = new PurchaseOrderItem();
				poItemPre.setId(item.getId());
				poItemPre.setQuantity(item.getQuantity());
				poItemPre.setMaterial(item.getMaterial());
				poItemPre.setQuotationItem(item.getQuotationItem());
				poItemsPre.add(poItemPre);
			});
		}
		
		//invoiceHeaderPre.setInvoiceItems(invoiceDTOIn.getId() != null ? invoiceHeaderRepository.getOne(invoiceDTOIn.getId()).getInvoiceItems() : null);
		
		poPre.setPurchaseOrderItems(poItemsPre);
		
		
		
		if(purchaseOrderWrapperDTO.getItemToBeRemove().size() > 0){
			purchaseOrderWrapperDTO.getItemToBeRemove().forEach(id -> {
				 deleteItem(id);
			});
		}
		
		//return submit(purchaseOrderDTOIn);
		
		log.info("PO save: "+purchaseOrderDTOIn);
		
		PurchaseOrderHeader poHeader = purchaseOrderHeaderMapper.dtoToModelMap(purchaseOrderDTOIn);
		
		log.info("PO save model:poHeader: "+poHeader);

		//Derive status
		poHeader.setStatus(getPOStatus(poHeader));
		
		List<PurchaseOrderItem> poItems = purchaseOrderItemMapper.dtoToModelList(purchaseOrderDTOIn.getPurchaseOrderItems());
		
		poItems.stream().forEach(poItem -> poItem.setPurchaseOrderHeader(poHeader));
		
		poHeader.setPurchaseOrderItems(poItems);
		
		
		log.info("poHeader before save: "+poHeader);
		
		PurchaseOrderHeader poHeaderOut = purchaseOrderHeaderRepository.save(poHeader);
//		log.info("poHeaderOut :............>>>>>"+ poHeaderOut +"  ship to adress :"+ poHeaderOut.getShipToAddress());
		
		//update material -- NOT REQUIRED AS PER ISSUE #895. Consider adding a setting to make price update
		//materialPriceUpdate(purchaseOrderDTOIn, poHeaderOut.getPurchaseOrderType().getName());
		
		//Create status change
		
		statusChangeEntry(poHeaderOut);
		
		//Back track Quotation linked to PO
		PurchaseOrderHeader poHeaderBackTrackOut = purchaseOrderBackTrackingServiceNew.updateTransactionStatus(poHeaderOut, poPre);
		
		//Update material spec and hsn
		purchaseOrderDTOIn.getPurchaseOrderItems()
		.forEach(PurchaseOrderItem -> {
		 	materialService.updateMaterialHsnAndSpec(PurchaseOrderItem.getMaterialId(), PurchaseOrderItem.getHsnOrSac(), PurchaseOrderItem.getSpecification() );
		});
		
		log.info("poHeaderBackTrackOut: "+poHeaderBackTrackOut);
		
		//poHeaderOut = purchaseOrderHeaderRepository.save(poHeaderBackTrackOut);
	
				
		log.info("before return from save() : "+poHeaderOut);
		return purchaseOrderHeaderMapper.modelToDTOMap(poHeaderOut); 
	}
	private void materialPriceUpdate(PurchaseOrderDTO poHeader, String poType){
		
		GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);
		
		//String invoiceType = invoiceHeader.getInvoiceType().getName();
		
		log.info("po type: "+poType);
		log.info("ApplicationConstants.PURCHASE_ORDER_TYPE_SUPPLIER: "+ApplicationConstants.PURCHASE_ORDER_TYPE_SUPPLIER);
		
		
		poHeader.getPurchaseOrderItems().forEach(item -> {
			
			Material material = materialRepository.getOne(item.getMaterialId()); // materialService.getMaterialById(item.getMaterial().getId());
			
			Boolean updateRequired = false;
			
			log.info("globalSetting.getPurchasePrice(): "+globalSetting.getPurchasePrice());
			log.info("material.getBuyingPrice(): "+material.getBuyingPrice());
			log.info("item.getPrice(): "+item.getPrice());
			if(!materialPriceListService.checkForPartyAndMaterial(poHeader.getPartyId(), item.getMaterialId()))
			{
			if(poType.equals(ApplicationConstants.PURCHASE_ORDER_TYPE_SUPPLIER)){
				
				log.info("inside supplier po check");
				
				if(globalSetting.getPurchasePrice() == ApplicationConstants.MATERIAL_PRICING_TYPE_BUYING_PRICE){
					log.info("purchase price is buying price");
					if(material.getBuyingPrice() != item.getPrice()){
						material.setBuyingPrice(item.getPrice());
						updateRequired = true;
					}
					
				}else if(globalSetting.getPurchasePrice() == ApplicationConstants.MATERIAL_PRICING_TYPE_MRP){
					log.info("purchase price is MRP");
					if(material.getMrp() != item.getPrice()){
						material.setMrp(item.getPrice());
						updateRequired = true;
						
					}
					
				}
				
			}else if(poType.equals(ApplicationConstants.PURCHASE_ORDER_TYPE_CUSTOMER)){
				
				log.info("inside customer po check");
				
				if(globalSetting.getSalePrice() == ApplicationConstants.MATERIAL_PRICING_TYPE_SELLING_PRICE){
					
					log.info("selling price is selling price");
					
					if(material.getPrice() != item.getPrice()){
						
						material.setPrice(item.getPrice());
						updateRequired = true;
					}
					
				}else if(globalSetting.getSalePrice() == ApplicationConstants.MATERIAL_PRICING_TYPE_MRP){
					
					log.info("selling price is MRP");
					
					if(material.getMrp() != item.getPrice()){
						material.setMrp(item.getPrice());
						updateRequired = true;
					}
					
				}
				
			}
			

			}
			
			if(item.getHsnOrSac()!= null && (!item.getHsnOrSac().equals(material.getHsnCode()))){
				material.setHsnCode(item.getHsnOrSac());
				updateRequired = true;
			}
			
			log.info("updateRequired: "+updateRequired);
			
			if(updateRequired){
				
				materialRepository.save(material);
			}
			
			
		});
		
	}
	private Status getPOStatus(PurchaseOrderHeader poHeader) {
		if(poHeader.getStatus() == null )
			return purchaseOrderStatusService.getStatus(poHeader);
		else
			return poHeader.getStatus();
	}

	@Override
	public List<PurchaseOrderDTO> getByParty(Party partyIn) {
		return purchaseOrderHeaderMapper.modelToDTOList(purchaseOrderHeaderRepository.findByParty(partyIn));
	}

	
	@Override
	public List<PurchaseOrderDTO> getAll() {
		return  purchaseOrderHeaderMapper.modelToDTOList(purchaseOrderHeaderRepository.findAll());
	}
	
	@Override
	public PurchaseOrderItemDTO saveItem(PurchaseOrderItemDTO purchaseOrderItemDTOIn) {
		return null;
	}

	@Override
	public PurchaseOrderDTO getById(String id) {
		return purchaseOrderHeaderMapper.modelToDTOMap(purchaseOrderHeaderRepository.getOne(id));
	}



//	@Override
//	public PurchaseOrderDTO cancel(String id) {
//		
//		log.info("in PO Cancel");
//		
//		PurchaseOrderHeader poToCancel = purchaseOrderHeaderRepository.getOne(id);
//		
//		log.info("po items: "+poToCancel.getPurchaseOrderItems());
//		
//		TransactionType transactionType = transactionTypeService.getModelById(poToCancel.getPurchaseOrderType().getId());
//		
//		poToCancel.setStatus(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_CANCELLED));
//		
//		PurchaseOrderHeader poOut = purchaseOrderHeaderMapper.dtoToModelMap(save(purchaseOrderHeaderMapper.modelToDTOMap(poToCancel)));
//		
//		log.info("poOut: "+poOut);
//		
//		log.info("po out items: "+poOut.getPurchaseOrderItems());
//		
//		return purchaseOrderHeaderMapper.modelToDTOMap(poOut);
//		
//		
//	}

	@Override
	public TransactionResponseDTO delete(String id) {
//		log.info("in PO Delete");
//		
//		PurchaseOrderHeader poToDelete = purchaseOrderHeaderRepository.getOne(id);
//		
//		log.info("po items: "+poToDelete.getPurchaseOrderItems());
//		
//		TransactionType transactionType = transactionTypeService.getModelById(poToDelete.getPurchaseOrderType().getId());
//		
//		poToDelete.setStatus(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED));
//		
//		PurchaseOrderHeader poOut = purchaseOrderHeaderMapper.dtoToModelMap(save(purchaseOrderHeaderMapper.modelToDTOMap(poToDelete)));
//		
//		
//		log.info("poOut: "+poOut);
//		
//		log.info("po out items: "+poOut.getPurchaseOrderItems());
//		
//		return purchaseOrderHeaderMapper.modelToDTOMap(poOut);
		
		/*
		TransactionResponseDTO res = new TransactionResponseDTO();
		
		String deleteStatus = "Quotation Deleted Successfully";
		try {
		QuotationHeader qhDelete = quotationHeaderRepository.getOne(id);
		qhDelete.getQuotationItems().forEach(item -> {
			quotationItemRepository.delete(item);
		});
		
		quotationHeaderRepository.delete(qhDelete);
		}catch(Exception e){
			deleteStatus = "Cannot Delete quotation";
		}
		
		res.setResponseString(deleteStatus);
		return res;
		 */
		
		TransactionResponseDTO res = new TransactionResponseDTO();
		String deleteStatus = "Purchase Order Deleted Successfully";		
		Integer deleteStatusNum = 1;
		
		PurchaseOrderHeader poToDelete = purchaseOrderHeaderRepository.getOne(id);
		
		Long greaterCount = purchaseOrderHeaderRepository.countByPurchaseOrderIdGreaterThanAndPurchaseOrderTypeAndFinancialYear(poToDelete.getPurchaseOrderId(), poToDelete.getPurchaseOrderType(),poToDelete.getFinancialYear());
		
		log.info("greaterCount"+greaterCount);
		
		if(greaterCount > 0){
			
			deleteStatus = "Cannot delete older Purchase Order";
			deleteStatusNum = 0;
			log.info("3");
			
			}
		else{
	
			try{
				if(greaterCount == 0){
	
					if(saveAllowed(poToDelete)){
			

						poToDelete.setStatus(statusService.getStatusByTransactionTypeAndName(poToDelete.getPurchaseOrderType(), ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED));
						log.info("po delete status: "+poToDelete.getStatus());
						deleteStatusChangeEntry(id);
						purchaseOrderBackTrackingServiceNew.updateTransactionStatus(poToDelete, null);
						poToDelete.getPurchaseOrderItems().forEach(item -> {
							
							purchaseOrderItemRepository.delete(item);
		
						});
													
						purchaseOrderHeaderRepository.delete(poToDelete);
						

					}
					
					else{
							deleteStatus = "Cannot Delete Purchase Order";
							deleteStatusNum = 0;
						}

				}else{
				
					deleteStatus = "Cannot Delete Purchase Order";
					deleteStatusNum = 0;
				}
			}
		
			catch(Exception e){
				deleteStatus = "Cannot Delete Purchase Order";
	//			res.setResponseString(deleteStatus);
	//			res.setResponseStatus(0);
				deleteStatusNum = 0;
			}
		}
		res.setResponseString(deleteStatus);
		res.setResponseStatus(deleteStatusNum);
		
		return res;
	}
	
	private boolean saveAllowed(PurchaseOrderHeader poToDelete){
		
		boolean returnValue = false;
		
		if(poToDelete.getStatus() != null){
			String poStatus = poToDelete.getStatus().getName();
			if(poStatus.equals(ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED)){
				//If there are invoices tracked from PO delete not possible
				
				List<InvoiceItem> invItems = invoiceItemRepository.findByPurchaseOrderHeader(poToDelete.getId());
				if(invItems.isEmpty()){
				
					//If there are DC tracked from PO delete not possible
					List<DeliveryChallanItem> dcItems = deliveryChallanItemRepository.findByPurchaseOrderHeader(poToDelete.getId());
					
					if(dcItems.isEmpty()){
						returnValue = true;
					}else{
						returnValue = false;
					}
					
				}else{
					returnValue = false;
				}
			}else{
				returnValue = true;
			}
		}else{
			returnValue = true;
		}
		return returnValue;
	}

	@Override
	public List<PurchaseOrderDTO> getByPartyAndStatusNotIn(Party party, TransactionType poType, List<Status> statuses, TransactionType requestingTransactionType) {
//		return

		log.info("party: "+party.getId());
		log.info("po type: "+poType.getId());
		statuses.forEach(st -> {
			log.info("status: "+st);
		});
		List<PurchaseOrderDTO> returnList =
				purchaseOrderHeaderMapper.modelToDTOList(purchaseOrderHeaderRepository.findByPartyAndPurchaseOrderTypeAndClosePoAndStatusNotIn(party, poType,0l, statuses));

		log.info("returnlist: "+returnList.size());

		List<PurchaseOrderDTO> poDTOList = new ArrayList<PurchaseOrderDTO>();
		
		Date currentDate = new Date();
		
		returnList
				.stream()
//				.peek(po -> {
////					if (po.getIsOpenEnded() > 0){
//						log.info(po.getPurchaseOrderNumber()+";"+po.getIsOpenEnded()+";"+po.getPurchaseOrderEndDate());
////					}
//				})
				.filter(po -> po.getIsOpenEnded() != null && po.getIsOpenEnded() > 0 ? ( currentDate.compareTo(po.getPurchaseOrderEndDate()) <= 0 || DateUtils.isSameDay(currentDate, po.getPurchaseOrderEndDate()) ) : true)
				.forEach(po -> {
//			log.info("po 11:"+po.getPurchaseOrderNumber());
			PurchaseOrderDTO poDTO = po; //new PurchaseOrderDTO();
			//poDTO.setPurchaseOrderItems(null);
			List<PurchaseOrderItemDTO> poItem = new ArrayList<PurchaseOrderItemDTO>();
			if(requestingTransactionType.getName().equals(ApplicationConstants.INVOICE_TYPE_CUSTOMER)
					||requestingTransactionType.getName().equals(ApplicationConstants.INVOICE_TYPE_SUPPLIER)
					||requestingTransactionType.getName().equals(ApplicationConstants.INVOICE_TYPE_PROFORMA)
					||requestingTransactionType.getName().equals(ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE)
					||requestingTransactionType.getName().equals(ApplicationConstants.INVOICE_TYPE_INCOMING_JW_INVOICE)
			)
			{
			poItem.addAll(po.getPurchaseOrderItems().stream().filter(poi -> poi.getInvoiceBalanceQuantity()> 0).collect(Collectors.toList()));
			poDTO.setPurchaseOrderItems(poItem);
			
			}
			else if(requestingTransactionType.getName().equals(ApplicationConstants.DC_TYPE_CUSTOMER)
					||requestingTransactionType.getName().equals(ApplicationConstants.DC_TYPE_SUPPLIER)
					|| requestingTransactionType.getName().equals(ApplicationConstants.INCOMING_JOBWORK_IN_DC)
					|| requestingTransactionType.getName().equals(ApplicationConstants.OUTGOING_JOBWORK_OUT_DC)
			)
			{
				poItem.addAll(po.getPurchaseOrderItems().stream().filter(poi -> poi.getDcBalanceQuantity()> 0 || ( poDTO.getIsOpenEnded() != null && poDTO.getIsOpenEnded() == 1)).collect(Collectors.toList()));
				poDTO.setPurchaseOrderItems(poItem);
			}
			
			poDTOList.add(poDTO);
		});
		return poDTOList;
	}
	
	@Override
	public List<PurchaseOrderDTO> getByPurchaseOrderNumber(String poNumber){
		return purchaseOrderHeaderMapper.modelToDTOList(purchaseOrderHeaderRepository.findByPurchaseOrderNumber(poNumber));
	}

	@Override
	public PurchaseOrderHeader getModelById(String id) {

		return purchaseOrderHeaderRepository.getOne(id);
	}

	@Override
	public PurchaseOrderItem getItemModelById(String id) {
		return purchaseOrderItemRepository.getOne(id);
	}

	@Override
	public Long getMaxPONumber(String prefix) {
		return purchaseOrderHeaderRepository.getMaxPONumber(prefix);
	}



	@Override
	public TransactionResponseDTO deleteItem(String id){
		TransactionResponseDTO res = new TransactionResponseDTO();
		res.setResponseStatus(1);
		String deleteStatus = "PO Item Deleted Successfully";
		try {
		PurchaseOrderItem poDelete = purchaseOrderItemRepository.getOne(id);
		
		purchaseOrderItemRepository.delete(poDelete);
		}catch(Exception e){
			deleteStatus = "Cannot Delete PO Item";
			res.setResponseStatus(0);
		}
		
		res.setResponseString(deleteStatus);
		return res;
	}
	@Override
	public Long getMaxPONumberByTransactionType(Long id) {
		// TODO Auto-generated method stub
		return purchaseOrderHeaderRepository.getMaxPONumberByTransactionType(id);
	}
	@Override
	public PurchaseOrderHeader getpoNumberByPurchaseOrderNumberAndTransactionType(String poNumber, TransactionType poType) {
		// TODO Auto-generated method stub
		return purchaseOrderHeaderRepository.findByPurchaseOrderNumberAndPurchaseOrderType(poNumber,poType);
	}
	
	private void statusChangeEntry(PurchaseOrderHeader poHeader){
		
		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();
		
		transactionStatusChange.setSourceOperation(null);
		transactionStatusChange.setSourceTransactionId(null);
		transactionStatusChange.setSourceTransactionType(null);
		transactionStatusChange.setStatus(poHeader.getStatus());
		transactionStatusChange.setTransactionId(poHeader.getId());
		transactionStatusChange.setTransactionType(poHeader.getPurchaseOrderType());
		transactionStatusChange.setParty(poHeader.getParty());

		transactionStatusChangeRepository.save(transactionStatusChange);
		
	}
	
	private void deleteStatusChangeEntry(String id){
		
		List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository.findByTransactionId(id);
		log.info("transactionStatusChangeList: "+transactionStatusChangeList.size());
		transactionStatusChangeList.forEach(tr -> {
			log.info("tr: "+tr);
			transactionStatusChangeRepository.delete(tr);
		});
		
		transactionStatusChangeList = transactionStatusChangeRepository.findBySourceTransactionId(id);
		
		log.info("findBySourceTransactionId: "+transactionStatusChangeList.size());
		
		transactionStatusChangeList.forEach(tr -> {
			log.info("tr 2: "+tr);
			transactionStatusChangeRepository.delete(tr);
		});
		
		
	}
	@Override
	public Long countByPurchaseOrderNumberAndTransactionType(String poNumber, TransactionType poType, Long partyId) {

		Party party = partyService.getPartyObjectById(partyId);
		FinancialYear financialYear = financialYearService.findByIsActive(1);


		List<String> buyPOTypes = Arrays.asList(ApplicationConstants.PURCHASE_ORDER_TYPE_SUPPLIER,ApplicationConstants.PURCHASE_ORDER_INCOMING_JOBWORK,ApplicationConstants.PURCHASE_ORDER_TYPE_CUSTOMER);

		if(buyPOTypes.contains(poType.getName())) {
			return purchaseOrderHeaderRepository.countByPurchaseOrderNumberAndPurchaseOrderTypeAndPartyAndFinancialYear(poNumber, poType, party,financialYear);
		}
		else {
			return purchaseOrderHeaderRepository.countByPurchaseOrderNumberAndPurchaseOrderTypeAndFinancialYear(poNumber,poType,financialYear);

		}		
	}

	@Override
	public TransactionResponseDTO closePo(String poId,Long statusId){
		TransactionResponseDTO res = new TransactionResponseDTO();
		res.setResponseStatus(1);
		String deleteStatus = "PO Status Updted Successfully";
		try {
			PurchaseOrderHeader purchaseOrderHeader = purchaseOrderHeaderRepository.getOne(poId);
			purchaseOrderHeader.setClosePo(statusId);
			purchaseOrderHeaderRepository.save(purchaseOrderHeader);
		}catch(Exception e){
			deleteStatus = "Can not Update PO Status";
			res.setResponseStatus(0);
		}

		res.setResponseString(deleteStatus);
		return res;


	}
	

}
