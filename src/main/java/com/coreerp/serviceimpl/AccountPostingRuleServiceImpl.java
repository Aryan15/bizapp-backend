package com.coreerp.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.AccountPostingRuleRepository;
import com.coreerp.model.AccountPostingRule;
import com.coreerp.model.TransactionType;
import com.coreerp.service.AccountPostingRuleService;

@Service
public class AccountPostingRuleServiceImpl implements AccountPostingRuleService {

	@Autowired
	AccountPostingRuleRepository accountPostingRuleRepository;
	
	@Override
	public AccountPostingRule getByTransactionType(TransactionType transactionType) {
		return accountPostingRuleRepository.findByTransactionType(transactionType);
	}

}
