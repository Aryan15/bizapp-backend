package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.coreerp.dao.*;
import com.coreerp.dto.StateDTO;
import com.coreerp.model.HelpVideoModel;
import com.coreerp.model.State;
import com.coreerp.reports.model.InvoiceItemReportModel;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.dto.UserDTO;
import com.coreerp.dto.UserPasswordDTO;
import com.coreerp.mapper.UserMapper;
import com.coreerp.model.Role;
import com.coreerp.model.User;
import com.coreerp.service.AreaService;
import com.coreerp.service.CityService;
import com.coreerp.service.CompanyService;
import com.coreerp.service.CountryService;
import com.coreerp.service.StateService;
import com.coreerp.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	final static Logger log = LogManager.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;
	
//	@Autowired
//	private RoleRepository roleRepository;
	
	@Autowired
	private BCryptPasswordEncoder bCryprPasswordEncoder;
	
	@Autowired
	UserMapper userMapper;
	
	@Autowired
	RoleRepository roleRepository;
	
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	CityService cityService;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	CountryService countryService;
	
	
	@Autowired
	EmployeeTypeRepository employeeTypeRepository;
	
	@Autowired
	PersonGenderRepository personGenderRepository;
	
	@Autowired
	NamePrefixRepository NamePrefixRepository;
	
	@Autowired
	SecurityQuestionRepository securityQuestionRepository;
	
	@Autowired
	StateService stateService;

	@Autowired
	HelpVideoRepository helpVideoRepository;
	
	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public UserDTO saveUser(UserDTO user) {
		if(user.getId() == null){
		user.setPassword((user.getPassword() != null && !user.getPassword().isEmpty()) ? bCryprPasswordEncoder.encode(user.getPassword()) : null);
		}
		user.setActive(1);
		//Role userRole = roleRepository.findByRole("ADMIN");
		//user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		return userMapper.modelToDTOMap(userRepository.save(userMapper.dtoToModelMap(user)));
	}
	
	@Override
	public UserDTO saveEmployee(UserDTO user) {
		if(user.getId() == null){
		user.setPassword((user.getPassword() != null && !user.getPassword().isEmpty()) ? bCryprPasswordEncoder.encode(user.getPassword()) : null);
		}
		user.setActive(1);
		User usermodel = new User();
		
		if(user.getId() != null) {
			usermodel = userRepository.getOne(user.getId());
		}
		
		usermodel.setArea(user.getAreaId() != null ? areaService.getAreaById(user.getAreaId()) : null);
		usermodel.setCity(user.getCityId() != null ? cityService.getCityById(user.getCityId()) : null);
		usermodel.setComments(user.getComments());
		usermodel.setCompany(companyService.getModelById(user.getCompanyId()));
		usermodel.setCountry(user.getCountryId() != null ? countryService.getCountryById(user.getCountryId()) : null);
		usermodel.setCurrentAddress(user.getCurrentAddress());
		usermodel.setDateOfBirth(user.getDateOfBirth());
		usermodel.setDateOfJoin(user.getDateOfJoin());
		usermodel.setDateOfLeaving(user.getDateOfLeaving());
		usermodel.setDeleted(user.getDeleted());
		usermodel.setDisplayName(user.getDisplayName());
		usermodel.setEmployeeId(user.getEmployeeId());
		usermodel.setEmployeeNumber(user.getEmployeeNumber());
		usermodel.setEmployeeType(user.getEmployeeTypeId() != null ? employeeTypeRepository.getOne(user.getEmployeeTypeId()) : null);
		usermodel.setGender(user.getGenderId() != null ? personGenderRepository.getOne(user.getGenderId()) : null);
		usermodel.setId(user.getId());
		usermodel.setLastName(usermodel.getLastName());
		usermodel.setMobileNumber(user.getMobileNumber());
		usermodel.setName(user.getName());
		usermodel.setNamePrefix(user.getPrefixId() != null ? NamePrefixRepository.getOne(user.getPrefixId()) : null);
		usermodel.setPanNumber(user.getPanNumber());
		usermodel.setPermanentAddress(user.getPermanentAddress());
		usermodel.setPinCode(user.getPinCode());
		usermodel.setRoles(new HashSet<Role>(roleRepository.findAllById(user.getRoleIds()))); //new HashSet<Role>(roleRepository.findAll(user.getRoleIds())));
		usermodel.setState(user.getStateId() != null ? stateService.getStateById(user.getStateId()) : null);
		usermodel.setTelephoneNumber(user.getTelephoneNumber());
		usermodel.setUserLanguage(user.getUserLanguage());
		return userMapper.modelToDTOMap(userRepository.save(usermodel));
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public User getUserById(Long id) {
		return userRepository.getOne(id);
	}

	@Override
	public List<UserDTO> getAllUsers() {
		return userMapper.modelToDTOList(userRepository.findAll());
	}

	@Override
	public TransactionResponseDTO deleteUser(Long id) {
		
		TransactionResponseDTO response = new TransactionResponseDTO();
		
		response.setResponseStatus(1);
		response.setResponseString("Deleted successfully");
		
		try{
			
			User user = userRepository.getOne(id);
			user.getRoles().forEach(role -> {
//				try{
//					roleRepository.delete(role.getId());
//				}catch(Exception e){
//					log.info("Can't delete role: "+e.getMessage());
//					response.setResponseStatus(0);
//					response.setResponseString("Error deleting user: "+e.getMessage());
//				}
				user.getRoles().remove(role);
				
			});
			
			userRepository.deleteById(id);
		}catch(Exception e){
			response.setResponseStatus(0);
			response.setResponseString("Error deleting user: "+e.getMessage());
			log.info("Error deleting: "+e);
		}
		
		return response;
	}
	
	public User save(User userModel){
		return userRepository.save(userModel);
	}
	
	@Override
	public Boolean resetPasswordEmployee(UserDTO userDTO) {
		if(userDTO.getPassword()=="") {
			log.info("if condition true userPassword: "+userDTO.getPassword());
			return false;
		}
		else {
			User user = userRepository.getOne(userDTO.getId());
			user.setEmail(userDTO.getEmail());
			user.setUsername(userDTO.getUsername());
			user.setPassword( bCryprPasswordEncoder.encode(userDTO.getPassword()));
			user.setIsLogInRequired((long)1);
			user.setActive(1);
			userRepository.save(user);
			return true;
		}

	}

	@Override
	public Boolean resetPassword(UserDTO userDTO) {
		if(userDTO.getPassword().isEmpty()) {
			return false;
		}
		else {
			User user = userRepository.findByUsername(userDTO.getUsername());
			user.setPassword(bCryprPasswordEncoder.encode(userDTO.getPassword()));
			user.setActive(1);
			user.setIsLogInRequired((long)1);
			try {
				userRepository.save(user);
			}catch(Exception e) {
				log.error("Error: "+e);
				return false;
			}
			return true;
		}
		
	}
	
	@Override
	public Boolean disableLogin(UserDTO userDTO) {
		if(userDTO.getId() == null) {
			return false;
		}else {
			User user = userRepository.getOne(userDTO.getId());
			user.setIsLogInRequired((long)0);
			user.setActive(0);
			try {
				userRepository.save(user);
			}catch(Exception e) {
				log.error("Error: "+e);
				return false;
			}
		}
		return true;
	}
	@Override
	public List<HelpVideoModel> getHelpVideos(Long activityId) {
		return helpVideoRepository.getHelpVideoByHeaderList(activityId);
	}
}
