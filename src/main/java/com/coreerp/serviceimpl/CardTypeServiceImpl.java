package com.coreerp.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.CardTypeRepository;
import com.coreerp.model.CardType;
import com.coreerp.service.CardTypeService;

@Service
public class CardTypeServiceImpl implements CardTypeService {

	@Autowired
	CardTypeRepository cardTypeRepository;
	
	@Override
	public CardType getById(Long id) {
		return cardTypeRepository.getOne(id);
	}

}
