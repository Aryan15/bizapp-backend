package com.coreerp.serviceimpl;

import java.util.List;

import com.coreerp.dao.CompanyEmailRepository;
import com.coreerp.dto.*;
import com.coreerp.mapper.CompanyEmailMapper;
import com.coreerp.model.PrintCopy;
import com.coreerp.model.UnitOfMeasurement;
import com.coreerp.service.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.GlobalSettingRepository;
import com.coreerp.mapper.GlobalSettingMapper;
import com.coreerp.model.GlobalSetting;

@Service
public class GlobalSettingServiceImpl implements GlobalSettingService {

	final static Logger log= LogManager.getLogger(GlobalSettingServiceImpl.class);
	
	@Autowired
	GlobalSettingRepository globalSettingReppository;
	
	@Autowired
	GlobalSettingMapper globalSettingMapper;


	@Autowired
	NumberRangeConfigurationService numberRangeConfigurationService;

	@Autowired
	TaxService taxService;

	@Autowired
	UnitOfMeasurementService unitOfMeasurementService;


	@Autowired
	TermsAndConditionService termsAndConditionService;

	@Autowired
	PrintCopiesService printCopiesService;


	@Autowired
	RoleService roleService;

	@Autowired
	CompanyEmailMapper companyEmailMapper;

	@Autowired
	CompanyEmailRepository companyEmailRepository;

	@Override
	public GlobalSettingDTO save(GlobalSettingDTO globalSettingDTO) {
     		return globalSettingMapper.modelToDTOMap(globalSettingReppository.save(globalSettingMapper.dtoToModelMap(globalSettingDTO)));
	}


	@Override
	public CompanyEmailDTO saveCompanyEmail(CompanyEmailDTO companyEmailDTO) {
		return companyEmailMapper.modelToDTOMap(companyEmailRepository.save(companyEmailMapper.dtoToModelMap(companyEmailDTO)));
	}

	@Override
	public GlobalSetting getGlobalSettingById(Long id) {
		return globalSettingReppository.getOne(id);
	}

	@Override
	public GlobalSettingDTO getGlobalSettingDTOById(Long id) {
		return globalSettingMapper.modelToDTOMap(globalSettingReppository.getOne(id));
	}

	@Override
	public List<GlobalSettingDTO> getAllGlobalSettings() {
		
		globalSettingReppository.findAll().forEach(gs -> {
			log.info("gs"+ gs);
		});
		return globalSettingMapper.modelToDTOList(globalSettingReppository.findAll());
	}



	@Override
	public SettingsWrapperDTO getAllGlobalSettingsList(){


		SettingsWrapperDTO settingsWrapperDTO = new SettingsWrapperDTO();
		List<GlobalSettingDTO>globalSettingDTOS =globalSettingMapper.modelToDTOList(globalSettingReppository.findAll());
		GlobalSettingDTO globalSettingDTO=globalSettingDTOS.get(0);
		settingsWrapperDTO.setGlobalSetting(globalSettingDTO);

		 List<NumberRangeConfigurationDTO> numberRangeConfigurationDTOS = numberRangeConfigurationService.getAll();
		settingsWrapperDTO.setNumberRangeConfigurations(numberRangeConfigurationDTOS);
	    List<TaxDTO> taxDTOS =	taxService.getAll();
		settingsWrapperDTO.setTaxes(taxDTOS);
		List<UnitOfMeasurement>  unitOfMeasurements = unitOfMeasurementService.getAll();
		settingsWrapperDTO.setUnitOfMeasurements(unitOfMeasurements);
		List<TermsAndConditionDTO> termsAndConditionDTOS =termsAndConditionService.getAll();
		settingsWrapperDTO.setTermsAndConditions(termsAndConditionDTOS);
	    List<PrintCopy> printCopies=printCopiesService.getAll();
		settingsWrapperDTO.setPrintCopies(printCopies);
		return  settingsWrapperDTO;
	}


}
