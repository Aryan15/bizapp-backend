package com.coreerp.serviceimpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("storage")
@Component
public class StorageProperties {
	
//	 @Autowired
//	 private Environment environment;
	 
    /**
     * Folder location for storing files
     */
	@Value("${image.path}")
    private String location;// = environment.getProperty("image.path");//"upload-dir";

    public String getLocation() {
    	//String tenantIdentifier = TenantContext.getCurrentTenant();
        return location; //+tenantIdentifier+"/";
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
