package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import com.coreerp.dao.PartyRepository;
import com.coreerp.dto.PartyMaterialPriceListDTO;
import com.coreerp.reports.dao.MaterialPriceListReportRepository;
import com.coreerp.reports.model.PartyPriceListReport;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.controller.PartyController;
import com.coreerp.dao.MaterialPriceListRepository;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.dto.MaterialPriceListDTO;
import com.coreerp.mapper.MaterialPriceListMapper;
import com.coreerp.model.GlobalSetting;
import com.coreerp.model.Material;
import com.coreerp.model.MaterialPriceList;
import com.coreerp.model.Party;
import com.coreerp.service.GlobalSettingService;
import com.coreerp.service.MaterialPriceListService;
import com.coreerp.service.PartyService;

@Service
public class MaterialPriceListServiceImpl implements MaterialPriceListService {

	final static Logger log = LogManager.getLogger(PartyController.class);
	
	@Autowired
	MaterialPriceListRepository materialPriceListRepository;
	
	@Autowired
	MaterialPriceListMapper materialPriceListMapper;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	MaterialRepository materialRepository;
	
	@Autowired
	GlobalSettingService globalSettingService;

	@Autowired
	MaterialPriceListReportRepository materialPriceListReportRepository;

	@Autowired
	PartyRepository partyRepository;
	
	@Override
	public MaterialPriceListDTO save(MaterialPriceListDTO materialPriceListDTO) {
		
		return materialPriceListMapper.modelToDTOMap(materialPriceListRepository.save(materialPriceListMapper.dtoToModelMap(materialPriceListDTO)));
	}

	@Override
	public MaterialPriceListDTO getById(Long materialPriceListId) {
		return materialPriceListMapper.modelToDTOMap(materialPriceListRepository.getOne(materialPriceListId));
	}

	@Override
	public MaterialPriceList getModelById(Long materialPriceListId) {
		return materialPriceListRepository.getOne(materialPriceListId);
	}

	@Override
	public List<MaterialPriceListDTO> saveList(List<MaterialPriceListDTO> materialPriceListDTOList) {
		
		List<MaterialPriceListDTO> dtoOut = new ArrayList<MaterialPriceListDTO>();
		
		materialPriceListDTOList.forEach(materialPriceListDTO -> {
			dtoOut.add(materialPriceListMapper.modelToDTOMap(materialPriceListRepository.save(materialPriceListMapper.dtoToModelMap(materialPriceListDTO))));
		});
		
		return dtoOut;
	}

	@Override
	public List<MaterialPriceListDTO> getForParty(Long partyId) {
		
		GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);
		List<MaterialPriceListDTO> materialPriceListDTO = new ArrayList<MaterialPriceListDTO>();
		Party party = partyService.getPartyObjectById(partyId);
		
		log.info("globalSetting.getSpecialPriceCalculation(): " + globalSetting.getSpecialPriceCalculation());
				
		
		if (globalSetting.getSpecialPriceCalculation() == 1 && party != null) {
			log.info("getForParty: party: "+party.getName());
			materialPriceListDTO =materialPriceListMapper.modelToDTOList( materialPriceListRepository.findByParty(party));
		
		
		log.info("getForParty: materialPriceLists size: "+materialPriceListDTO.size());
		}
		return materialPriceListDTO;
		
	}
	
	@Override
	public void deleteById(Long id){
		materialPriceListRepository.deleteById(id);
	}



	@Override
	public Boolean checkForPartyAndMaterial(Long partyId, Long materialId) {
		Party party = partyService.getPartyObjectById(partyId);
		
		Material material = materialRepository.getOne(materialId);
		
		MaterialPriceList mlist = materialPriceListRepository.findByPartyAndMaterial(party, material);
		
		if(mlist != null){
			return true;
		}
		return false;
	}

	@Override
	public PartyMaterialPriceListDTO getPagePriceListReport(Long partyTypeId, Long partyId, Integer pageNumber, Integer pageSize, String sortDirection, String sortColumn,Long allowAllMaterial) {
		Page<PartyPriceListReport> partyPriceListReports = null;
		List<PartyPriceListReport> partyPriceListReportss = new ArrayList<PartyPriceListReport>();
		;

		PageRequest pageRequest = null;
		if (pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0) {
			pageRequest =
					new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "partyName");
		}
		if (pageSize == -99) {
			pageRequest =
					new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "partyName");
		}
		PartyMaterialPriceListDTO partyMaterialPriceListDTO = new PartyMaterialPriceListDTO();

		if (allowAllMaterial==1) {
			if (partyId == null) {
				List<Material> materials = materialRepository.findAll();
				materials.forEach(mat -> {
					String materialType=null;
					if(mat.getMaterialType()!=null){
						materialType=mat.getMaterialType().getName();
					}
					PartyPriceListReport partyPriceListReport = new PartyPriceListReport(mat.getId(), mat.getPrice(), null, null, mat.getId(), mat.getName(), mat.getPrice(), mat.getPartNumber(), materialType);
					partyPriceListReportss.add(partyPriceListReport);
					partyMaterialPriceListDTO.setPartyPriceListReport(partyPriceListReportss);
					partyMaterialPriceListDTO.setTotalCount(partyPriceListReportss.size());

				});
			} else {

				List<Material> materials = materialRepository.findAll();
				Party party = partyRepository.getOne(partyId);
				List<MaterialPriceList> materialPriceLists = materialPriceListReportRepository.findByParty(party);

				materials.forEach(mat -> {
//                    String materialType=null;
//                    if(mat.getMaterialType()!=null){
//                        materialType=mat.getMaterialType().getName();
//                    }

					AtomicReference<Long> id = new AtomicReference<>(0l);
					if (materialPriceLists != null) {

						materialPriceLists.forEach(price -> {
							if (mat.getId() == price.getMaterial().getId()) {
							    String materialName=null;
							    if(mat.getMaterialType()!=null){
                                    materialName =mat.getMaterialType().getName();
                                }
								id.set(1l);
								PartyPriceListReport partyPriceListReport = new PartyPriceListReport(price.getId(), price.getSellingPrice(), price.getComments(), price.getParty().getName(), mat.getId(), mat.getName(), mat.getPrice(), mat.getPartNumber(), materialName);
								partyPriceListReportss.add(partyPriceListReport);


							}


						});
					}
					if (id.get() != 1) {
                        String materialName=null;
                        if(mat.getMaterialType()!=null){
                            materialName =mat.getMaterialType().getName();
                        }
						PartyPriceListReport partyPriceListReport = new PartyPriceListReport(mat.getId(), mat.getPrice(), null, null, mat.getId(), mat.getName(), mat.getPrice(), mat.getPartNumber(), materialName);
						partyPriceListReportss.add(partyPriceListReport);
						id.set(0l);
					}

				});

			}
			partyMaterialPriceListDTO.setPartyPriceListReport(partyPriceListReportss);
			partyMaterialPriceListDTO.setTotalCount(partyPriceListReportss.size());

		} else {
			partyPriceListReports = materialPriceListReportRepository.getPriceListReport(partyTypeId, partyId, pageRequest);

			partyMaterialPriceListDTO.setPartyPriceListReport(partyPriceListReports.getContent());
			partyMaterialPriceListDTO.setTotalCount((int) partyPriceListReports.getTotalElements());
		}


		return partyMaterialPriceListDTO;

	}



}
