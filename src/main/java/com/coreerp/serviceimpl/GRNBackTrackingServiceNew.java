package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.kie.api.event.rule.AgendaEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.TrackingAgendaEventListener;
import com.coreerp.dao.DeliveryChallanHeaderRepository;
import com.coreerp.dao.DeliveryChallanItemRepository;
import com.coreerp.dao.TransactionStatusChangeRepository;
import com.coreerp.dto.TransactionItemQuantityRule;
import com.coreerp.dto.TransactionStatusRule;
import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.DeliveryChallanItem;
import com.coreerp.model.GRNHeader;
import com.coreerp.model.GRNItem;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionStatusChange;
import com.coreerp.service.BackTrackingService;
import com.coreerp.service.StatusService;

@Component
public class GRNBackTrackingServiceNew implements BackTrackingService<GRNHeader> {

	final static Logger log = LogManager.getLogger(GRNBackTrackingServiceNew.class);
	
	String preDCId = "";
	
	Double prevDcQuantity = 0.0;
	Double prevGrnBalanceQuantity = 0.0;
	Double grnQuantity = 0.0;
	Double sourceQuantity = 0.0;
	Double sourcePrevQuantity = 0.0;
	
	@Autowired
    private KieContainer kieContainer;
    
	
    private KieSession kieSession ;
    
    @Autowired
   	StatusService statusService;
       
   	@Autowired
   	TransactionStatusChangeRepository transactionStatusChangeRepository;
   	
   	@Autowired
   	DeliveryChallanHeaderRepository deliveryChallanHeaderRepository; 
   	
   	@Autowired
   	DeliveryChallanItemRepository deliveryChallanItemRepository;
   	
	@Override
	public GRNHeader updateTransactionStatus(GRNHeader in, GRNHeader pre) {
		
		log.info("in updateTransactionStatus ");
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession = kieContainer.newKieSession("rulesSession");
		
		kieSession.addEventListener(agendaEventListener);
		//Loop through POs attached to the DC
		
		List<DeliveryChallanHeader> grnDCHeaders = new ArrayList<DeliveryChallanHeader>();
		
		//Loop through DCs attached to the GRN
		
		in.getGrnItems().forEach(gri -> {
			if(gri.getDeliveryChallanHeader() != null & gri.getDeliveryChallanHeader().getId() != preDCId){
				grnDCHeaders.add(gri.getDeliveryChallanHeader());
				preDCId = gri.getDeliveryChallanHeader().getId();
			}
		});
		
		if(grnDCHeaders.size() > 0){
			
			grnDCHeaders.forEach(dcHeader -> {
				
				//Build TransactionStatusRule
				
				TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
				
				prevDcQuantity = 0.0;
				//dcBalanceQuantity = 0.0;
				prevGrnBalanceQuantity = 0.0;
				
				dcHeader.getDeliveryChallanItems().forEach(dci -> {
					prevGrnBalanceQuantity += dci.getGrnBalanceQuantity();
					prevDcQuantity += dci.getQuantity();
					
					
				});
				
				sourcePrevQuantity = 0.0;
				
				if (pre != null ){
					pre.getGrnItems().forEach(ini -> {
						sourcePrevQuantity += ini.getAcceptedQuantity();
					});
					
					
				}
				
				log.info("prevGrnBalanceQuantity: "+prevGrnBalanceQuantity);
				log.info("prevDcQuantity: "+prevDcQuantity);
				
				String latestStatus = null;
				if(in.getGrnStatus().getName().equals(ApplicationConstants.GRN_STATUS_DELETED)){
//					latestStatus = transactionStatusChangeRepository.findByLatestTransactionId(dcHeader.getId()).getStatus().getName();
//					log.info("latestStatus: "+latestStatus);
					
					TransactionStatusChange transactionStatusChange = transactionStatusChangeRepository.findByLatestTransactionId(dcHeader.getId());
					
					if(transactionStatusChange != null){
						latestStatus = transactionStatusChange.getStatus().getName();
					}
					
					log.info("latestStatus: "+latestStatus);
				}
				transactionStatusRule.setLatestStatus(latestStatus);
				transactionStatusRule.setQuantity(prevDcQuantity);
				
				grnQuantity = 0.0;
				
				in.getGrnItems()
				.stream()
				.filter(gri -> gri.getDeliveryChallanHeader().getId().equals(dcHeader.getId()))
				.forEach(gri -> {
					grnQuantity += gri.getAcceptedQuantity();
				});
				transactionStatusRule.setGrnBalanceQuantity(prevGrnBalanceQuantity);
				transactionStatusRule.setCurrentStatus(dcHeader.getStatus().getName());
				transactionStatusRule.setSourceQuantity(grnQuantity);
				transactionStatusRule.setSourcePrevQuantity(sourcePrevQuantity);
				transactionStatusRule.setSourceTransactionId(in.getId());
				transactionStatusRule.setSourceTransactionStatus(in.getGrnStatus().getName());
				transactionStatusRule.setSourceTransactionType(in.getGrnType().getName());
				//transactionStatusRule.setStatus(poHeader.getStatus().getName());
				transactionStatusRule.setTransactionId(dcHeader.getId());
				transactionStatusRule.setTransactionType(dcHeader.getDeliveryChallanType().getName());
				
				log.info("transactionStatusRule: "+transactionStatusRule);
				
				
				
				
				//Get status from Drools
				
				kieSession.insert(transactionStatusRule);

		        int ruleCount = kieSession.fireAllRules();
		        
		        
		        
		        log.info("rules count: "+ruleCount);
		        
		        if(ruleCount > 0 && transactionStatusRule.getNewStatus() != null)
		        {
		        	Status status  = statusService.getStatusByTransactionTypeAndName(dcHeader.getDeliveryChallanType(), transactionStatusRule.getNewStatus());
			        dcHeader.setStatus(status);
			        
			        //Meke dc Item updates
			        
			        if(transactionStatusRule.getItemUpdateRequired()){
			        	
			        	dcHeader.getDeliveryChallanItems()
			        	.stream()
			        	//.filter(dci -> in.getGrnItems().stream().anyMatch(gri -> gri.getDeliveryChallanItem().getId().equals(dci.getId())))
			        	.forEach(dcItem -> {

							Double preQuantity =  0.0;

							if (pre != null  && pre.getGrnItems() != null && pre.getGrnItems().size() > 0)
							{
								OptionalDouble	tempQuantity =
										pre.getGrnItems()
										.stream()
										.filter(ii -> ii.getDeliveryChallanItem().getId() == dcItem.getId())
										.mapToDouble(di -> di.getAcceptedQuantity())
										.findFirst();


								if(tempQuantity.isPresent()) {
									preQuantity  = tempQuantity.getAsDouble();
								}

							}
			        		
			        		TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
				            
				            transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
				            transactionItemQuantityRule.setItemId(dcItem.getId());
				            transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
				            transactionItemQuantityRule.setPrevGRNBalanceQuantity(dcItem.getGrnBalanceQuantity());
				            transactionItemQuantityRule.setQuantity(dcItem.getQuantity());
				            
				            sourceQuantity = 0.0;
							
//				            sourceQuantity = in.getGrnItems()
//							.stream()
//							.filter(i -> i.getDeliveryChallanItem().getId().equals(dcItem.getId()))
//							.findFirst().get().getAcceptedQuantity();
							
				            GRNItem gItem = in.getGrnItems()
									.stream()
									.filter(i -> i.getDeliveryChallanItem().getId().equals(dcItem.getId()))
									.findFirst()
									.orElse(null);
				            
				            //In case of delete of an item in GRN, above item will be null due to filter 
				            if(gItem != null){
								sourceQuantity = preQuantity != null ? (gItem.getAcceptedQuantity() - preQuantity) : gItem.getAcceptedQuantity();
				            }else{
								sourceQuantity = -preQuantity;
							}
				            //If Item is getting deleted in GRN, respective DC Grn balance quantity is faked to dc item quantity

							if(sourceQuantity == 0&& Double.compare(gItem != null ? gItem.getAcceptedQuantity() : 0.0, preQuantity) != 0){
								transactionItemQuantityRule.setPrevGRNBalanceQuantity(dcItem.getQuantity());
								transactionItemQuantityRule.setSourceQuantity(0.0);
							}else{
								transactionItemQuantityRule.setPrevGRNBalanceQuantity(dcItem.getGrnBalanceQuantity());
								transactionItemQuantityRule.setSourceQuantity(sourceQuantity);
							}

//				            transactionItemQuantityRule.setSourceQuantity(sourceQuantity);
				            
				            log.info("item rule before: "+transactionItemQuantityRule);
				            
				            kieSession.insert(transactionItemQuantityRule);

				            int ruleCounti = kieSession.fireAllRules();
				            
				            log.info("rules count: "+ruleCounti);
				            log.info("item rule after: "+transactionItemQuantityRule);
				            
				            if(ruleCounti > 0 && transactionItemQuantityRule.getgRNBalanceQuantity() != null)
				            	dcItem.setGrnBalanceQuantity(transactionItemQuantityRule.getgRNBalanceQuantity());
			        	});
			        	
			        	
			        }
			        
			        kieSession.dispose();
			        
			        deliveryChallanHeaderRepository.save(dcHeader);	
			        
			        //Status change entry
			        
			        statusChangeEntry(dcHeader, in);
		        }
			});
			
		}
		
		
		
		return null;
	}
	
	
	private void statusChangeEntry(DeliveryChallanHeader dcHeader , GRNHeader grnHeader ){
		
		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();
		
		transactionStatusChange.setSourceOperation("GRN "+grnHeader.getGrnStatus().getName());
		transactionStatusChange.setSourceTransactionId(grnHeader.getId());
		transactionStatusChange.setSourceTransactionType(grnHeader.getGrnType());
		transactionStatusChange.setSourceTransactionNumber(grnHeader.getGrnNumber());
		transactionStatusChange.setStatus(dcHeader.getStatus());
		transactionStatusChange.setTransactionId(dcHeader.getId());
		transactionStatusChange.setTransactionType(dcHeader.getDeliveryChallanType());
		transactionStatusChange.setTransactionNumber(dcHeader.getDeliveryChallanNumber());
		transactionStatusChange.setParty(dcHeader.getParty());

		transactionStatusChangeRepository.save(transactionStatusChange);
		
	}
	
	
	public void itemDelete(GRNItem item){
		
		log.info("in itemDelete ");
		
		
		
		if(item.getDeliveryChallanItem() != null){
			
			//DeliveryChallanHeader dcHeader = item.getDeliveryChallanHeader();
//			if(dcHeader.getStatus().getName().equals(ApplicationConstants.DC_STATUS_GRN_GENEREATED)){
//				Status status  = statusService.getStatusByTransactionTypeAndName(dcHeader.getDeliveryChallanType(), ApplicationConstants.DC_STATUS_PARTIAL_GRN);
//		        dcHeader.setStatus(status);
//		        deliveryChallanHeaderRepository.save(dcHeader);	
//			}
			
			DeliveryChallanItem dcItem = item.getDeliveryChallanItem();
			
			dcItem.setGrnBalanceQuantity(dcItem.getGrnBalanceQuantity()+item.getAcceptedQuantity());
			
			deliveryChallanItemRepository.save(dcItem);	
			
			//statusChangeEntry(dcHeader, item.getGrnHeader());
			
		}
		
	}

}
