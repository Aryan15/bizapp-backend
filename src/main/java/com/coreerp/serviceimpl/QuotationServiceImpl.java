package com.coreerp.serviceimpl;

import java.util.List;

import com.coreerp.model.*;
import com.coreerp.service.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.dao.QuotationHeaderRepository;
import com.coreerp.dao.QuotationItemRepository;
import com.coreerp.dao.TransactionStatusChangeRepository;
import com.coreerp.dto.QuotationDTO;
import com.coreerp.dto.QuotationItemDTO;
import com.coreerp.dto.QuotationWrapperDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.QuotationHeaderMapper;
import com.coreerp.mapper.QuotationItemMapper;

@Service
public class QuotationServiceImpl implements QuotationService {
	
	final static Logger log = LogManager.getLogger(QuotationServiceImpl.class);
	
	@Autowired
	QuotationHeaderRepository quotationHeaderRepository;
	
	@Autowired
	QuotationItemRepository quotationItemRepository;
	
	@Autowired
	QuotationHeaderMapper quotationHeaderMapper;
	
	@Autowired
	QuotationItemMapper quotationItemMapper;
	
	@Autowired
	QuotationStatusService quotationStatusService;
	
	@Autowired
	//MaterialService materialService;
	MaterialRepository materialRepository;
	
	@Autowired
	GlobalSettingService globalSettingService;
	
	@Autowired
	TransactionStatusChangeRepository transactionStatusChangeRepository;
	
	@Autowired
	MaterialPriceListService materialPriceListService;
	
	@Autowired
	MaterialService materialService;
	//boolean setStatus = true;

	@Autowired
	FinancialYearService financialYearService;
	
	@Override
	public QuotationDTO save(QuotationWrapperDTO quotationWrapperDTO) {
		
		
		QuotationDTO quotationDTO = quotationWrapperDTO.getQuotationHeader();
		
		if(quotationWrapperDTO.getItemToBeRemove().size() > 0){
			quotationWrapperDTO.getItemToBeRemove().forEach(id -> {
				deleteItem(id);
			});
		}
		
		log.info("incoming dto: "+quotationDTO);
		
		log.info("quotationDTO in: "+quotationDTO);
		
		QuotationHeader quotationHeader = quotationHeaderMapper.dtoToModelMap(quotationDTO);
		
		log.info("quotationHeader in: "+quotationHeader);


		//Derive status
		quotationHeader.setStatus(getStatus(quotationHeader));

		List<QuotationItem> quotationItems = quotationHeader.getQuotationItems(); // quotationItemMapper.dtoToModelList(quotationDTO.getQuotationItems());
		
		log.info("items before save:..."+quotationItems);
		//set header to items		
		quotationItems.forEach(qItem -> {
											qItem.setQuotationHeader(quotationHeader);
											
										});

		//set items to header 
		quotationHeader.setQuotationItems(quotationItems);

		//Update material spec and hsn
		quotationDTO.getQuotationItems()
		.forEach(QuotationItem -> {
		 	materialService.updateMaterialHsnAndSpec(QuotationItem.getMaterialId(), QuotationItem.getHsnOrSac(), QuotationItem.getSpecification() );
		});
		
		//save
		QuotationHeader quotationHeaderOut = quotationHeaderRepository.save(quotationHeader);
		
		//update material -- NOT REQUIRED AS PER ISSUE #895. Consider adding a setting to make price update
		//materialPriceUpdate(quotationDTO, quotationHeaderOut.getQuotationType().getName());
				
		log.info("items after save:..."+quotationHeaderOut.getQuotationItems());
		
		statusChangeEntry(quotationHeaderOut);
		
		return quotationHeaderMapper.modelToDTOMap(quotationHeaderOut);
	}
	
	private void materialPriceUpdate(QuotationDTO quotationHeader, String quotationType){
		
		GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);
		
		//String invoiceType = invoiceHeader.getInvoiceType().getName();
		
		log.info("quotationType: "+quotationType);
		log.info("ApplicationConstants.QUOTATION_TYPE_SUPPLIER: "+ApplicationConstants.QUOTATION_TYPE_SUPPLIER);
		
		
		quotationHeader.getQuotationItems().forEach(item -> {
			
			Material material = materialRepository.getOne(item.getMaterialId()); // materialService.getMaterialById(item.getMaterial().getId());
			
			Boolean updateRequired = false;
			
			log.info("globalSetting.getPurchasePrice(): "+globalSetting.getPurchasePrice());
			log.info("material.getBuyingPrice(): "+material.getBuyingPrice());
			log.info("item.getPrice(): "+item.getPrice());
			
			if(!materialPriceListService.checkForPartyAndMaterial(quotationHeader.getPartyId(), item.getMaterialId()))
			{
			if(quotationType.equals(ApplicationConstants.QUOTATION_TYPE_SUPPLIER)){
				
				log.info("inside supplier qt check");
				
				if(globalSetting.getPurchasePrice() == ApplicationConstants.MATERIAL_PRICING_TYPE_BUYING_PRICE){
					log.info("purchase price is buying price");
					if(material.getBuyingPrice() != item.getPrice()){
						material.setBuyingPrice(item.getPrice());
						updateRequired = true;
					}
					
				}else if(globalSetting.getPurchasePrice() == ApplicationConstants.MATERIAL_PRICING_TYPE_MRP){
					log.info("purchase price is MRP");
					if(material.getMrp() != item.getPrice()){
						material.setMrp(item.getPrice());
						updateRequired = true;
						
					}
					
				}
				
			}else if(quotationType.equals(ApplicationConstants.QUOTATION_TYPE_CUSTOMER)){
				
				log.info("inside customer qt check");
				
				if(globalSetting.getSalePrice() == ApplicationConstants.MATERIAL_PRICING_TYPE_SELLING_PRICE){
					
					log.info("selling price is selling price");
					
					if(material.getPrice() != item.getPrice()){
						
						material.setPrice(item.getPrice());
						updateRequired = true;
					}
					
				}else if(globalSetting.getSalePrice() == ApplicationConstants.MATERIAL_PRICING_TYPE_MRP){
					
					log.info("selling price is MRP");
					
					if(material.getMrp() != item.getPrice()){
						material.setMrp(item.getPrice());
						updateRequired = true;
					}
					
				}
				
			}
			}

					
			
			if(item.getHsnOrSac()!= null && (!item.getHsnOrSac().equals(material.getHsnCode()))){
				material.setHsnCode(item.getHsnOrSac());
				updateRequired = true;
			}
			
			log.info("updateRequired: "+updateRequired);
			
			if(updateRequired){
				
				materialRepository.save(material);
			}
			
			
		});
		
	}

	private Status getStatus(QuotationHeader quotationHeader) {
		if(quotationHeader.getStatus() == null )
			return quotationStatusService.getStatus(quotationHeader);
		else
			return quotationHeader.getStatus();
	}

	@Override
	public QuotationItemDTO saveItem(QuotationItemDTO quotationItemDTO) {
		return quotationItemMapper.modelToDTOMap(quotationItemRepository.save(quotationItemMapper.dtoToModelMap(quotationItemDTO)));
	}

	@Override
	public List<QuotationDTO> getAll() {
		return quotationHeaderMapper.modelToDTOList(quotationHeaderRepository.findAll());
	}

	@Override
	public List<QuotationDTO> getByParty(Party party) {
		return quotationHeaderMapper.modelToDTOList(quotationHeaderRepository.findByParty(party));
	}



	@Override
	public QuotationDTO getById(String id) {
		
		log.info("in getById...id: "+id);
		
		QuotationHeader quotationHeader = quotationHeaderRepository.getOne(id);
		
		log.info("in getById...quotationHeader: "+quotationHeader);
		
		if(quotationHeader != null){
		if(quotationHeader.getQuotationItems().size() == 0 ){
			List<QuotationItem> quotationItems = quotationItemRepository.findByQuotationHeader(quotationHeader);
			quotationHeader.setQuotationItems(quotationItems);
		}
		}
		
		return quotationHeaderMapper.modelToDTOMap(quotationHeader);
	}

	@Override
	public List<QuotationDTO> getByPartyAndStatusNotIn(Party party, List<Status> statuses) {
		return quotationHeaderMapper.modelToDTOList(quotationHeaderRepository.findByPartyAndStatusNotIn(party, statuses));
	}
	
	@Override
	public List<QuotationDTO> getByQuotationNumber(String quotationNumber){
		return quotationHeaderMapper.modelToDTOList(quotationHeaderRepository.findByQuotationNumber(quotationNumber));
	}

	@Override
	public QuotationHeader getModelById(String id) {
		return quotationHeaderRepository.getOne(id);
	}

	@Override
	public QuotationItem getItemModelById(String id) {
		return quotationItemRepository.getOne(id);
	}

	@Override
	public Long getMaxQuotationNumber(String prefix) {
		return quotationHeaderRepository.getMaxQuotationNumber(prefix);
	}

	@Override
	public TransactionResponseDTO delete(String id) {
		
		TransactionResponseDTO res = new TransactionResponseDTO();
		
		String deleteStatus = "Quotation Deleted Successfully";
		try {
		QuotationHeader qhDelete = quotationHeaderRepository.getOne(id);
		deleteStatusChangeEntry(id);
		qhDelete.getQuotationItems().forEach(item -> {
			quotationItemRepository.delete(item);
		});
		
		quotationHeaderRepository.delete(qhDelete);
		}catch(Exception e){
			deleteStatus = "Cannot Delete quotation";
		}
		
		res.setResponseString(deleteStatus);
		return res;
	}

	@Override
	public List<QuotationDTO> getByQuotationNumberLike(String searchString) {
		return quotationHeaderMapper.modelToDTOList(quotationHeaderRepository.findByQuotationNumberIgnoreCaseContains(searchString));
	}
	
	@Override
	public List<QuotationDTO> getByQuotationDate(String searchString){
		return quotationHeaderMapper.modelToDTOList(quotationHeaderRepository.findByQuotationDate(searchString));
		
	}
	
	@Override
	public TransactionResponseDTO deleteItem(String id){
		TransactionResponseDTO res = new TransactionResponseDTO();
		res.setResponseStatus(1);
		String deleteStatus = "Quotation Item Deleted Successfully";
		try {
		QuotationItem qiDelete = quotationItemRepository.getOne(id);
		
		quotationItemRepository.delete(qiDelete);
		}catch(Exception e){
			deleteStatus = "Cannot Delete quotation Item";
			res.setResponseStatus(0);
		}
		
		res.setResponseString(deleteStatus);
		return res;
	}

	@Override
	public Long countByQuotationNumberAndTransactionType(String quotationNumber, TransactionType id) {
		// TODO Auto-generated method stub
		FinancialYear financialYear = financialYearService.findByIsActive(1);
		return quotationHeaderRepository.countByQuotationNumberAndQuotationTypeAndFinancialYear(quotationNumber, id,financialYear);
	}

	@Override
	public Long getMaxQuotationNumberByTransactionType(Long transactionTypeId) {
		// TODO Auto-generated method stub
		return quotationHeaderRepository.getMaxQuotationNumberByTransactionType(transactionTypeId);
	}

	private void statusChangeEntry(QuotationHeader qotHeader){
		
		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();
		
		transactionStatusChange.setSourceOperation(null);
		transactionStatusChange.setSourceTransactionId(null);
		transactionStatusChange.setSourceTransactionType(null);
		transactionStatusChange.setStatus(qotHeader.getStatus());
		transactionStatusChange.setTransactionId(qotHeader.getId());
		transactionStatusChange.setTransactionType(qotHeader.getQuotationType());
		transactionStatusChange.setParty(qotHeader.getParty());

		transactionStatusChangeRepository.save(transactionStatusChange);
		
	}

	private void deleteStatusChangeEntry(String id) {

		List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository
				.findByTransactionId(id);

		transactionStatusChangeList.forEach(tr -> {
			transactionStatusChangeRepository.delete(tr);
		});

		transactionStatusChangeList = transactionStatusChangeRepository.findBySourceTransactionId(id);

		transactionStatusChangeList.forEach(tr -> {
			transactionStatusChangeRepository.delete(tr);
		});

	}

}
