package com.coreerp.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.CardTypeRepository;
import com.coreerp.dao.PaymentMethodRepository;
import com.coreerp.dto.CardTypeDTO;
import com.coreerp.dto.PaymentMethodDTO;
import com.coreerp.mapper.CardTypeMapper;
import com.coreerp.mapper.PaymentMethodMapper;
import com.coreerp.model.PaymentMethod;
import com.coreerp.service.PaymentMethodService;

@Service
public class PaymentMethodServiceImpl implements PaymentMethodService {

	@Autowired
	PaymentMethodRepository paymentMethodRepository;
	
	@Autowired
	PaymentMethodMapper paymentMethodMapper;
	
	@Autowired
	CardTypeMapper cardTypeMapper;
	
	@Autowired
	CardTypeRepository cardTypeRepository;
	
	
	@Override
	public PaymentMethod getById(Long id) {
		return paymentMethodRepository.getOne(id);
	}

	@Override
	public List<PaymentMethodDTO> getAll() {
		// TODO Auto-generated method stub
		return paymentMethodMapper.modelToDTOList(paymentMethodRepository.findAll());
	}

	@Override
	public List<CardTypeDTO> getAllCardType(){
		return cardTypeMapper.modelToDTOList(cardTypeRepository.findAll());
	}
}
