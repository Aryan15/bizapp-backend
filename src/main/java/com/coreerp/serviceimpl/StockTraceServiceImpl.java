package com.coreerp.serviceimpl;

import java.util.Date;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.StockTraceRepository;
import com.coreerp.dto.StockTraceDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.StockTraceMapper;
import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.DeliveryChallanItem;
import com.coreerp.model.GRNHeader;
import com.coreerp.model.GRNItem;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.InvoiceItem;
import com.coreerp.model.Material;
import com.coreerp.model.StockTrace;
import com.coreerp.service.StockTraceBuilder;
import com.coreerp.service.StockTraceService;
import com.coreerp.service.TransactionTypeService;

@Service
public class StockTraceServiceImpl implements StockTraceService {

	final static Logger log = LogManager.getLogger(StockTraceServiceImpl.class);
	@Autowired
	StockTraceRepository stockTraceRepository;

	@Autowired
	StockTraceMapper stockTraceMapper;

	@Autowired
	StockTraceBuilder stockTraceBuilder;

	@Autowired
	TransactionTypeService transactionTypeService;

	Integer transactionIdSequence = 1;

	@Override
	public StockTraceDTO save(StockTraceDTO stockTraceDTO) {

		StockTrace stockTrace = stockTraceMapper.dtoToModelMap(stockTraceDTO);

		return stockTraceMapper.modelToDTOMap(stockTraceRepository.save(stockTrace));
	}

	@Override
	public StockTrace getModelById(String id) {
		return stockTraceRepository.getOne(id);
	}

	@Override
	public StockTraceDTO getById(String id) {
		return stockTraceMapper.modelToDTOMap(stockTraceRepository.getOne(id));
	}

	@Override
	public List<StockTrace> getByTransactionHeaderId(String transactionHeaderId) {
		return stockTraceRepository.findByTransactionHeaderId(transactionHeaderId);
	}

	@Override
	public StockTrace getByTransactionId(String transactionId) {
		return stockTraceRepository.findByTransactionId(transactionId);
	}

	@Override
	public StockTrace saveMaterialCreationStockTrace(Material material) {
		// Check if reversal required

		postReversalMaterialStockTrace(material);

		StockTrace stockTraceSequence = stockTraceRepository.findByTransactionId(material.getId().toString());

		// log.info("stockTraceSequence sequence:
		// "+stockTraceSequence.getTransactionIdSequence());

		transactionIdSequence = stockTraceSequence != null ? stockTraceSequence.getTransactionIdSequence() + 1 : 1;

		log.info("transactionIdSequence: " + transactionIdSequence);

		// Build Stock Trace for Material

		log.info("material created date time..." + material.getCreatedDateTime());

		StockTrace stMaterial = stockTraceBuilder.makeMaterialCreationStockTrace(material);
		stMaterial.setTransactionIdSequence(transactionIdSequence);

		// Post Stock Trace for Material

		log.info("beforre saving..." + stMaterial);

		stockTraceRepository.save(stMaterial);

		return null;
	}

	@Override
	public StockTrace saveGrnCreationStockTrace(GRNHeader grnHeader, GRNHeader grnHeaderPre) {
		// log.info("passed grn id: "+grnHeader.getId());
		// //Check if reversal required
		//
		// postReversalGrnStockTrace(grnHeader);
		//
		// StockTrace stockTraceSequence =
		// stockTraceRepository.findByTransactionId(grnHeader.getId());
		//
		// transactionIdSequence = stockTraceSequence!= null ?
		// stockTraceSequence.getTransactionIdSequence() : 1;
		//
		// //Build Stock Trace for GRN
		// List<StockTrace> stList =
		// stockTraceBuilder.makeGrnCreationStockTrace(grnHeader);
		//
		// //Update with sequence
		//
		// stList.forEach(st ->
		// st.setTransactionIdSequence(transactionIdSequence++));
		//
		// //Post Stock Trace for GRN
		//
		// stList.forEach(st -> stockTraceRepository.save(st));
		//
		//
		// return null;
		log.info("1. id: " + grnHeader.getId());
		List<StockTrace> stReverseRequired = getByTransactionHeaderId(grnHeader.getId());

		// transactionIdSequence = stReverseRequired != null ?
		// stReverseRequired.getTransactionIdSequence() : 1;
		log.info("2");
		grnHeader.getGrnItems().forEach(grnItem -> {

			// post if reversal required
			if (stReverseRequired != null) {
				log.info("3");

				List<GRNItem> preItems = grnHeaderPre.getGrnItems().stream().filter(i -> i.getId().equals(grnItem.getId()))
						.collect(Collectors.toList());
				GRNItem preItem = null;
				log.info("preItems.size(): " + preItems.size());
				if (preItems.size() != 0) {
					preItem = preItems.get(0);
				}
				log.info("preItem: " + preItem);
				log.info("invoiceItem.getQuantity(): " + grnItem.getAcceptedQuantity());
				// log.info("preItem.getQuantity(): "+preItem.getQuantity());
				// post reverse if new quantity != previous quantity
				Double preQuantity = preItem != null ? preItem.getAcceptedQuantity() : null;
				log.info("preQuantity: " + preQuantity);
				if (preItem != null && Double.compare(grnItem.getAcceptedQuantity(), preQuantity) != 0) {
					log.info("preItem.getQuantity(): " + preItem.getAcceptedQuantity());
					OptionalInt maxTransIdSeq = stReverseRequired
											.stream()
											.mapToInt(st -> st.getTransactionIdSequence())
											.max();
					log.info("maxTransIdSeq: " + maxTransIdSeq);
					
					List<StockTrace> stItems = stReverseRequired.stream()
							.filter(st -> st.getTransactionId().equals(grnItem.getId()))
							.filter(st -> st.getReverse() == null)
							.filter(st -> maxTransIdSeq.isPresent() ?  st.getTransactionIdSequence() == maxTransIdSeq.getAsInt() : true)
							.collect(Collectors.toList());

					log.info("stItems.size(): " + stItems.size());
					
					if (stItems.size() != 0) {
						StockTrace stItem = stItems.get(0);
						StockTrace stReverse = new StockTrace();

						stReverse.setBusinessDate(stItem.getBusinessDate());
						stReverse.setMaterial(stItem.getMaterial());
						stReverse.setParty(stItem.getParty());
						stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
						stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
						stReverse.setRemarks(stItem.getRemarks());
						stReverse.setReverse("Y");
						stReverse.setTransactionDate(stItem.getTransactionDate());
						stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
						stReverse.setTransactionNumber(stItem.getTransactionNumber());
						stReverse.setTransactionId(stItem.getTransactionId());
						stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
						stReverse.setTransactionNote(stItem.getTransactionNote());
						stReverse.setTransactionType(stItem.getTransactionType());

						stockTraceRepository.save(stReverse);
					}
				}
			}

			// Build stock trace for the invoice item if prev item quantity is
			// not equal to current quantity

			List<GRNItem> preItems = grnHeaderPre.getGrnItems().stream().filter(i -> i.getId().equals(grnItem.getId()))
					.collect(Collectors.toList());
			GRNItem preItem = null;
			if (preItems.size() != 0) {
				preItem = preItems.get(0);
			}

			if (grnItem.getAcceptedQuantity() != (preItem != null ? preItem.getAcceptedQuantity() : null)) {
				StockTrace st = new StockTrace();

				if (stReverseRequired != null) {
					List<StockTrace> stRc = stReverseRequired.stream()
							.filter(stR -> stR.getTransactionId().equals(grnItem.getId())).collect(Collectors.toList());

					StockTrace stockTraceFirst = stRc.size() != 0 ? stRc.get(0) : null;
					transactionIdSequence = stockTraceFirst != null ? stockTraceFirst.getTransactionIdSequence() : 1;
				}
				st.setBusinessDate(grnHeader.getGrnDate());
				st.setMaterial(grnItem.getMaterial());
				st.setParty(grnHeader.getSupplier());
				st.setQuantityIn(grnItem.getAcceptedQuantity());
				st.setQuantityOut(0.0);
				st.setRemarks("Purchase");
				st.setReverse(null);
				st.setTransactionDate(new Date());
				st.setTransactionHeaderId(grnHeader.getId());
				st.setTransactionNumber(grnHeader.getGrnNumber());
				st.setTransactionId(grnItem.getId());
				// TODO configure
				st.setTransactionNote("Purchase");
				st.setTransactionType(transactionTypeService.getByName(ApplicationConstants.GRN_TYPE_SUPPLIER));
				st.setTransactionIdSequence(++transactionIdSequence);

				stockTraceRepository.save(st);
			}

		});

		return null;
	}

	@Override
	public StockTrace saveCustomerInvoiceCreationStockTrace(InvoiceHeader invoiceHeader,
			InvoiceHeader invoiceHeaderPre) {

		/*
		 * log.info("passed invoice id: "+invoiceHeader.getId()); //Check if
		 * reversal required
		 * 
		 * postReversalCustomerInvoiceStockTrace(invoiceHeader);
		 * 
		 * StockTrace stockTraceSequence =
		 * stockTraceRepository.findByTransactionId(invoiceHeader.getId());
		 * 
		 * transactionIdSequence = stockTraceSequence != null ?
		 * stockTraceSequence.getTransactionIdSequence() : 1;
		 * 
		 * //Build Stock Trace for Invoice List<StockTrace> stList =
		 * stockTraceBuilder.makeCustomerInvoiceCreationStockTrace(invoiceHeader
		 * );
		 * 
		 * //Update with sequence
		 * 
		 * stList.forEach(st ->
		 * st.setTransactionIdSequence(transactionIdSequence++));
		 * 
		 * //Post Stock Trace for Invoice
		 * 
		 * stList.forEach(st -> stockTraceRepository.save(st));
		 */
		log.info("1");
		List<StockTrace> stReverseRequired = getByTransactionHeaderId(invoiceHeader.getId());

		// transactionIdSequence = stReverseRequired != null ?
		// stReverseRequired.getTransactionIdSequence() : 1;
		log.info("2");
		invoiceHeader.getInvoiceItems().forEach(invoiceItem -> {

			// post if reversal required
			if (stReverseRequired != null) {

				log.info("3");
				List<InvoiceItem> preItems = invoiceHeaderPre.getInvoiceItems().stream()
						.filter(i -> i.getId().equals(invoiceItem.getId())).collect(Collectors.toList());
				InvoiceItem preItem = null;
				log.info("preItems.size(): " + preItems.size());
				if (preItems.size() != 0) {
					preItem = preItems.get(0);
				}
				log.info("preItem: " + preItem);
				log.info("invoiceItem.getQuantity(): " + invoiceItem.getQuantity());
				// log.info("preItem.getQuantity(): "+preItem.getQuantity());
				// post reverse if new quantity != previous quantity
				Double preQuantity = preItem != null ? preItem.getQuantity() : null;
				log.info("preQuantity: " + preQuantity);
				if (preItem != null && Double.compare(invoiceItem.getQuantity(), preQuantity) != 0) {
					log.info("preItem.getQuantity(): " + preItem.getQuantity());
					List<StockTrace> stItems = stReverseRequired.stream()
							.filter(st -> st.getTransactionId().equals(invoiceItem.getId())).collect(Collectors.toList());
					log.info("stItems.size(): "+ stItems.size());
					if (stItems.size() != 0) {
						StockTrace stItem = stItems.get(0);
						StockTrace stReverse = new StockTrace();

						stReverse.setBusinessDate(stItem.getBusinessDate());
						stReverse.setMaterial(stItem.getMaterial());
						stReverse.setParty(stItem.getParty());
						stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
						stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
						stReverse.setRemarks(stItem.getRemarks());
						stReverse.setReverse("Y");
						stReverse.setTransactionDate(stItem.getTransactionDate());
						stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
						stReverse.setTransactionNumber(stItem.getTransactionNumber());
						stReverse.setTransactionId(stItem.getTransactionId());
						stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
						stReverse.setTransactionNote(stItem.getTransactionNote());
						stReverse.setTransactionType(stItem.getTransactionType());

						stockTraceRepository.save(stReverse);
					}
				}
			}

			// Build stock trace for the invoice item if prev item quantity is
			// not equal to current quantity

			List<InvoiceItem> preItems = invoiceHeaderPre.getInvoiceItems().stream()
					.filter(i -> i.getId().equals(invoiceItem.getId())).collect(Collectors.toList());
			InvoiceItem preItem = null;
			log.info("preItems.size(): " + preItems.size());
			if (preItems.size() != 0) {
				preItem = preItems.get(0);
			}

			// log.info("preItem.getQuantity(): "+preItem.getQuantity());
			log.info("invoiceItem.getQuantity(): " + invoiceItem.getQuantity());

			// log.info("compare null...", Double.compare(1.0, (Double) null));

			int qtyCompare = Double.compare(invoiceItem.getQuantity(), (preItem != null ? preItem.getQuantity() : 0));

			log.info("qtyCompare: " + qtyCompare);

			if (Double.compare(invoiceItem.getQuantity(), (preItem != null ? preItem.getQuantity() : 0)) != 0) {
				StockTrace st = new StockTrace();
				log.info("inside stock trace");
				if (stReverseRequired != null) {
					
					OptionalInt maxTransIdSeq = stReverseRequired
							.stream()
							.mapToInt(stss -> stss.getTransactionIdSequence())
							.max();
					log.info("maxTransIdSeq: " + maxTransIdSeq);
					
					List<StockTrace> stRc = stReverseRequired.stream()
							.filter(sts -> sts.getTransactionId().equals(invoiceItem.getId()))
							.filter(sts -> sts.getReverse() == null)
							.filter(sts -> maxTransIdSeq.isPresent() ? sts.getTransactionIdSequence() == maxTransIdSeq.getAsInt() : true)
							.collect(Collectors.toList());
	
	
					//List<StockTrace> stRc = stReverseRequired.stream()
					//		.filter(stR -> stR.getTransactionId().equals(invoiceItem.getId())).collect(Collectors.toList());

					StockTrace stockTraceFirst = stRc.size() != 0 ? stRc.get(0) : null;
					transactionIdSequence = stockTraceFirst != null ? stockTraceFirst.getTransactionIdSequence() : 1;
				}

				st.setBusinessDate(invoiceHeader.getInvoiceDate());
				st.setMaterial(invoiceItem.getMaterial());
				st.setParty(invoiceHeader.getParty());
				st.setQuantityIn(0.0);
				st.setQuantityOut(invoiceItem.getQuantity());
				st.setRemarks("Sales");
				st.setReverse(null);
				st.setTransactionDate(new Date());
				st.setTransactionHeaderId(invoiceHeader.getId());
				st.setTransactionId(invoiceItem.getId());
				st.setTransactionNumber(invoiceHeader.getInvoiceNumber());
				// TODO configure
				st.setTransactionNote("Sales");
				st.setTransactionType(transactionTypeService.getByName(ApplicationConstants.INVOICE_TYPE_CUSTOMER));
				st.setTransactionIdSequence(++transactionIdSequence);

				log.info("before strace insert: " + st);
				stockTraceRepository.save(st);
			}

		});

		return null;
	}

	
	@Override
	public StockTrace saveSupplierInvoiceCreationStockTrace(InvoiceHeader invoiceHeader,
			InvoiceHeader invoiceHeaderPre) {

		
		log.info("1");
		List<StockTrace> stReverseRequired = getByTransactionHeaderId(invoiceHeader.getId());

		// transactionIdSequence = stReverseRequired != null ?
		// stReverseRequired.getTransactionIdSequence() : 1;
		log.info("2");
		invoiceHeader.getInvoiceItems().forEach(invoiceItem -> {

			// post if reversal required
			if (stReverseRequired != null) {

				log.info("3");
				List<InvoiceItem> preItems = invoiceHeaderPre.getInvoiceItems().stream()
						.filter(i -> i.getId().equals(invoiceItem.getId())).collect(Collectors.toList());
				InvoiceItem preItem = null;
				log.info("preItems.size(): " + preItems.size());
				if (preItems.size() != 0) {
					preItem = preItems.get(0);
				}
				log.info("preItem: " + preItem);
				log.info("invoiceItem.getQuantity(): " + invoiceItem.getQuantity());
				// log.info("preItem.getQuantity(): "+preItem.getQuantity());
				// post reverse if new quantity != previous quantity
				Double preQuantity = preItem != null ? preItem.getQuantity() : null;
				log.info("preQuantity: " + preQuantity);
				if (preItem != null && Double.compare(invoiceItem.getQuantity(), preQuantity) != 0) {
					log.info("preItem.getQuantity(): " + preItem.getQuantity());
					List<StockTrace> stItems = stReverseRequired.stream()
							.filter(st -> st.getTransactionId().equals(invoiceItem.getId())).collect(Collectors.toList());
					log.info("stItems.size(): "+ stItems.size());
					if (stItems.size() != 0) {
						StockTrace stItem = stItems.get(0);
						StockTrace stReverse = new StockTrace();

						stReverse.setBusinessDate(stItem.getBusinessDate());
						stReverse.setMaterial(stItem.getMaterial());
						stReverse.setParty(stItem.getParty());
						stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
						stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
						stReverse.setRemarks(stItem.getRemarks());
						stReverse.setReverse("Y");
						stReverse.setTransactionDate(stItem.getTransactionDate());
						stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
						stReverse.setTransactionNumber(stItem.getTransactionNumber());
						stReverse.setTransactionId(stItem.getTransactionId());
						stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
						stReverse.setTransactionNote(stItem.getTransactionNote());
						stReverse.setTransactionType(stItem.getTransactionType());

						stockTraceRepository.save(stReverse);
					}
				}
			}

			// Build stock trace for the invoice item if prev item quantity is
			// not equal to current quantity

			List<InvoiceItem> preItems = invoiceHeaderPre.getInvoiceItems().stream()
					.filter(i -> i.getId().equals(invoiceItem.getId())).collect(Collectors.toList());
			InvoiceItem preItem = null;
			log.info("preItems.size(): " + preItems.size());
			if (preItems.size() != 0) {
				preItem = preItems.get(0);
			}

			// log.info("preItem.getQuantity(): "+preItem.getQuantity());
			log.info("invoiceItem.getQuantity(): " + invoiceItem.getQuantity());

			// log.info("compare null...", Double.compare(1.0, (Double) null));

			int qtyCompare = Double.compare(invoiceItem.getQuantity(), (preItem != null ? preItem.getQuantity() : 0));

			log.info("qtyCompare: " + qtyCompare);

			if (Double.compare(invoiceItem.getQuantity(), (preItem != null ? preItem.getQuantity() : 0)) != 0) {
				StockTrace st = new StockTrace();
				log.info("inside stock trace");
				if (stReverseRequired != null) {
					
					OptionalInt maxTransIdSeq = stReverseRequired
							.stream()
							.mapToInt(stss -> stss.getTransactionIdSequence())
							.max();
					log.info("maxTransIdSeq: " + maxTransIdSeq);
					
					List<StockTrace> stRc = stReverseRequired.stream()
							.filter(sts -> sts.getTransactionId().equals(invoiceItem.getId()))
							.filter(sts -> sts.getReverse() == null)
							.filter(sts -> maxTransIdSeq.isPresent() ? sts.getTransactionIdSequence() == maxTransIdSeq.getAsInt() : true)
							.collect(Collectors.toList());
	
	
					//List<StockTrace> stRc = stReverseRequired.stream()
					//		.filter(stR -> stR.getTransactionId().equals(invoiceItem.getId())).collect(Collectors.toList());

					StockTrace stockTraceFirst = stRc.size() != 0 ? stRc.get(0) : null;
					transactionIdSequence = stockTraceFirst != null ? stockTraceFirst.getTransactionIdSequence() : 1;
				}

				st.setBusinessDate(invoiceHeader.getInvoiceDate());
				st.setMaterial(invoiceItem.getMaterial());
				st.setParty(invoiceHeader.getParty());
				st.setQuantityIn(invoiceItem.getQuantity());
				st.setQuantityOut(0.0);
				st.setRemarks("Purchase");
				st.setReverse(null);
				st.setTransactionDate(new Date());
				st.setTransactionHeaderId(invoiceHeader.getId());
				st.setTransactionId(invoiceItem.getId());
				st.setTransactionNumber(invoiceHeader.getInvoiceNumber());
				// TODO configure
				st.setTransactionNote("Purchase");
				st.setTransactionType(transactionTypeService.getByName(ApplicationConstants.INVOICE_TYPE_SUPPLIER));
				st.setTransactionIdSequence(++transactionIdSequence);

				log.info("before strace insert: " + st);
				stockTraceRepository.save(st);
			}

		});

		return null;
	}
	
	
	public StockTrace postReversalMaterialStockTrace(Material material) {

		StockTrace stReverseRequired = getByTransactionId(material.getId().toString());

		// Integer transactionIdSequence =1;
		// Post reversal if entry already exists and quantityIn is different
		// from material's stock
		if (stReverseRequired != null && material.getStock() != stReverseRequired.getQuantityIn()) {

			StockTrace stReverse = new StockTrace();

			stReverse.setBusinessDate(stReverseRequired.getBusinessDate());
			stReverse.setMaterial(stReverseRequired.getMaterial());
			stReverse.setParty(stReverseRequired.getParty());
			stReverse.setQuantityIn(stReverseRequired.getQuantityIn() * -1);
			stReverse.setQuantityOut(stReverseRequired.getQuantityOut() * -1);
			stReverse.setRemarks(stReverseRequired.getRemarks());
			stReverse.setReverse("Y");
			stReverse.setTransactionDate(stReverseRequired.getTransactionDate());
			stReverse.setTransactionHeaderId(stReverseRequired.getTransactionHeaderId());
			stReverse.setTransactionNumber(stReverseRequired.getTransactionNumber());
			stReverse.setTransactionId(stReverseRequired.getTransactionId());
			stReverse.setTransactionIdSequence(stReverseRequired.getTransactionIdSequence() + 1);
			stReverse.setTransactionNote(stReverseRequired.getTransactionNote());
			stReverse.setTransactionType(stReverseRequired.getTransactionType());

			stockTraceRepository.save(stReverse);
		}

		return null;

	}

	public StockTrace postReversalGrnStockTrace(GRNHeader grnHeader) {

		log.info("grnHeader.getId()" + grnHeader.getId());
		// Check if reversal required

		List<StockTrace> stReverseRequired = getByTransactionHeaderId(grnHeader.getId());

		log.info("stReverseRequired" + stReverseRequired);

		// Post reversal if required

		if (stReverseRequired != null) {
			stReverseRequired.forEach(stItem -> {

				// post reversal only if GRN item quantity is different from the
				// existing positing

				if (stItem.getQuantityIn() != grnHeader.getGrnItems().stream()
						.filter(gi -> gi.getId().equals(stItem.getTransactionId()))
						.mapToDouble(gi -> gi.getAcceptedQuantity()).sum()) {
					StockTrace stReverse = new StockTrace();

					stReverse.setBusinessDate(stItem.getBusinessDate());
					stReverse.setMaterial(stItem.getMaterial());
					stReverse.setParty(stItem.getParty());
					stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
					stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
					stReverse.setRemarks(stItem.getRemarks());
					stReverse.setReverse("Y");
					stReverse.setTransactionDate(stItem.getTransactionDate());
					stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
					stReverse.setTransactionNumber(stItem.getTransactionNumber());
					stReverse.setTransactionId(stItem.getTransactionId());
					stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
					stReverse.setTransactionNote(stItem.getTransactionNote());
					stReverse.setTransactionType(stItem.getTransactionType());

					stockTraceRepository.save(stReverse);
				}
			});
		}
		return null;

	}
	
	public StockTrace postReversalGrnItemStockTrace(GRNItem grnItem) {
		
		StockTrace stReverse = new StockTrace();
		
		StockTrace stItem = getByTransactionId(grnItem.getId());

		stReverse.setBusinessDate(stItem.getBusinessDate());
		stReverse.setMaterial(stItem.getMaterial());
		stReverse.setParty(stItem.getParty());
		stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
		stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
		stReverse.setRemarks(stItem.getRemarks());
		stReverse.setReverse("Y");
		stReverse.setTransactionDate(stItem.getTransactionDate());
		stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
		stReverse.setTransactionNumber(stItem.getTransactionNumber());
		stReverse.setTransactionId(stItem.getTransactionId());
		stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
		stReverse.setTransactionNote(stItem.getTransactionNote());
		stReverse.setTransactionType(stItem.getTransactionType());

		stockTraceRepository.save(stReverse);
		
		return null;
	}

	public StockTrace postReversalCustomerInvoiceStockTrace(InvoiceHeader invoiceHeader) {

		// Check if reversal required

		List<StockTrace> stReverseRequired = getByTransactionHeaderId(invoiceHeader.getId());

		// Post reversal if required

		if (stReverseRequired != null) {
			stReverseRequired.forEach(stItem -> {

				// post reversal only if invoice item quantity is different from
				// the existing positing
				if (stItem.getQuantityOut() != invoiceHeader.getInvoiceItems().stream()
						.filter(inv -> inv.getId().equals(stItem.getTransactionId())).mapToDouble(inv -> inv.getQuantity())
						.sum()) {
					StockTrace stReverse = new StockTrace();

					stReverse.setBusinessDate(stItem.getBusinessDate());
					stReverse.setMaterial(stItem.getMaterial());
					stReverse.setParty(stItem.getParty());
					stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
					stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
					stReverse.setRemarks(stItem.getRemarks());
					stReverse.setReverse("Y");
					stReverse.setTransactionDate(stItem.getTransactionDate());
					stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
					stReverse.setTransactionNumber(stItem.getTransactionNumber());
					stReverse.setTransactionId(stItem.getTransactionId());
					stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
					stReverse.setTransactionNote(stItem.getTransactionNote());
					stReverse.setTransactionType(stItem.getTransactionType());

					stockTraceRepository.save(stReverse);
				}
			});
		}
		return null;

	}
	
	public StockTrace postReversalSupplierInvoiceStockTrace(InvoiceHeader invoiceHeader) {

		// Check if reversal required

		List<StockTrace> stReverseRequired = getByTransactionHeaderId(invoiceHeader.getId());

		// Post reversal if required

		if (stReverseRequired != null) {
			stReverseRequired.forEach(stItem -> {

				// post reversal only if invoice item quantity is different from
				// the existing positing
				if (stItem.getQuantityOut() != invoiceHeader.getInvoiceItems().stream()
						.filter(inv -> inv.getId().equals(stItem.getTransactionId())).mapToDouble(inv -> inv.getQuantity())
						.sum()) {
					StockTrace stReverse = new StockTrace();

					stReverse.setBusinessDate(stItem.getBusinessDate());
					stReverse.setMaterial(stItem.getMaterial());
					stReverse.setParty(stItem.getParty());
					stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
					stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
					stReverse.setRemarks(stItem.getRemarks());
					stReverse.setReverse("Y");
					stReverse.setTransactionDate(stItem.getTransactionDate());
					stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
					stReverse.setTransactionNumber(stItem.getTransactionNumber());
					stReverse.setTransactionId(stItem.getTransactionId());
					stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
					stReverse.setTransactionNote(stItem.getTransactionNote());
					stReverse.setTransactionType(stItem.getTransactionType());

					stockTraceRepository.save(stReverse);
				}
			});
		}
		return null;

	}

	@Override
	public StockTrace saveJWDCCreationStockTrace(DeliveryChallanHeader dcHeader, DeliveryChallanHeader dcHeaderPre) {
		log.info("1. id: " + dcHeader.getId());
		List<StockTrace> stReverseRequired = getByTransactionHeaderId(dcHeader.getId());

		// transactionIdSequence = stReverseRequired != null ?
		// stReverseRequired.getTransactionIdSequence() : 1;
		log.info("2");
		dcHeader.getDeliveryChallanItems().forEach(dcItem -> {

			// post if reversal required
			if (stReverseRequired != null) {
				log.info("3");

				List<DeliveryChallanItem> preItems = dcHeaderPre.getDeliveryChallanItems().stream()
						.filter(i -> i.getId().equals(dcItem.getId())).collect(Collectors.toList());
				DeliveryChallanItem preItem = null;
				log.info("preItems.size(): " + preItems.size());
				if (preItems.size() != 0) {
					preItem = preItems.get(0);
				}
				log.info("preItem: " + preItem);
				log.info("invoiceItem.getQuantity(): " + dcItem.getQuantity());
				// log.info("preItem.getQuantity(): "+preItem.getQuantity());
				// post reverse if new quantity != previous quantity
				Double preQuantity = preItem != null ? preItem.getQuantity() : null;
				log.info("preQuantity: " + preQuantity);
				if (preItem != null && Double.compare(dcItem.getQuantity(), preQuantity) != 0) {
					log.info("preItem.getQuantity(): " + preItem.getQuantity());
					List<StockTrace> stItems = stReverseRequired.stream()
							.filter(st -> st.getTransactionId().equals(dcItem.getId())).collect(Collectors.toList());

					if (stItems.size() != 0) {
						StockTrace stItem = stItems.get(0);
						StockTrace stReverse = new StockTrace();

						stReverse.setBusinessDate(stItem.getBusinessDate());
						stReverse.setMaterial(stItem.getMaterial());
						stReverse.setParty(stItem.getParty());
						stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
						stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
						stReverse.setRemarks(stItem.getRemarks());
						stReverse.setReverse("Y");
						stReverse.setTransactionDate(stItem.getTransactionDate());
						stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
						stReverse.setTransactionId(stItem.getTransactionId());
						stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
						stReverse.setTransactionNote(stItem.getTransactionNote());
						stReverse.setTransactionType(stItem.getTransactionType());

						stockTraceRepository.save(stReverse);
					}
				}
			}

			// Build stock trace for the DC item if prev item quantity is not
			// equal to current quantity

			List<DeliveryChallanItem> preItems = dcHeaderPre.getDeliveryChallanItems().stream()
					.filter(i -> i.getId().equals(dcItem.getId())).collect(Collectors.toList());
			DeliveryChallanItem preItem = null;
			if (preItems.size() != 0) {
				preItem = preItems.get(0);
			}

			if (dcItem.getQuantity() != (preItem != null ? preItem.getQuantity() : null)) {
				StockTrace st = new StockTrace();

				if (stReverseRequired != null) {
					List<StockTrace> stRc = stReverseRequired.stream()
							.filter(stR -> stR.getTransactionId().equals(dcItem.getId())).collect(Collectors.toList());

					StockTrace stockTraceFirst = stRc.size() != 0 ? stRc.get(0) : null;
					transactionIdSequence = stockTraceFirst != null ? stockTraceFirst.getTransactionIdSequence() : 1;
				}
				st.setBusinessDate(dcHeader.getDeliveryChallanDate());
				st.setMaterial(dcItem.getMaterial());
				st.setParty(dcHeader.getParty());
				st.setQuantityIn(0.0);
				st.setQuantityOut(dcItem.getQuantity());
				st.setRemarks("Jobwork Material");
				st.setReverse(null);
				st.setTransactionDate(new Date());
				st.setTransactionHeaderId(dcHeader.getId());
				st.setTransactionId(dcItem.getId());
				// TODO configure
				st.setTransactionNote("Incoming Jobwork");
				st.setTransactionType(dcHeader.getDeliveryChallanType());
				st.setTransactionIdSequence(++transactionIdSequence);

				stockTraceRepository.save(st);
			}

		});

		return null;
	}

	@Override
	public StockTrace postReversalJWDCStockTrace(DeliveryChallanHeader dcHeader) {
		// Check if reversal required

		List<StockTrace> stReverseRequired = getByTransactionHeaderId(dcHeader.getId());

		// Post reversal if required

		if (stReverseRequired != null) {
			stReverseRequired.forEach(stItem -> {

				// post reversal only if invoice item quantity is different from
				// the existing positing
				if (stItem.getQuantityOut() != dcHeader.getDeliveryChallanItems().stream()
						.filter(dc -> dc.getId().equals(stItem.getTransactionId())).mapToDouble(inv -> inv.getQuantity())
						.sum()) {
					StockTrace stReverse = new StockTrace();

					stReverse.setBusinessDate(stItem.getBusinessDate());
					stReverse.setMaterial(stItem.getMaterial());
					stReverse.setParty(stItem.getParty());
					stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
					stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
					stReverse.setRemarks(stItem.getRemarks());
					stReverse.setReverse("Y");
					stReverse.setTransactionDate(stItem.getTransactionDate());
					stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
					stReverse.setTransactionId(stItem.getTransactionId());
					stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
					stReverse.setTransactionNote(stItem.getTransactionNote());
					stReverse.setTransactionType(stItem.getTransactionType());

					stockTraceRepository.save(stReverse);
				}
			});
		}
		return null;
	}

	@Override
	public StockTrace saveDcCreationStockTrace(DeliveryChallanHeader dcHeader, DeliveryChallanHeader dcHeaderPre) {

		log.info("1. id: " + dcHeader.getId());
		List<StockTrace> stReverseRequired = getByTransactionHeaderId(dcHeader.getId());

		// transactionIdSequence = stReverseRequired != null ?
		// stReverseRequired.getTransactionIdSequence() : 1;
		log.info("2");
		dcHeader.getDeliveryChallanItems().forEach(dcItem -> {

			// post if reversal required
			if (stReverseRequired != null) {
				log.info("3");

				List<DeliveryChallanItem> preItems = dcHeaderPre.getDeliveryChallanItems().stream()
						.filter(i -> i.getId().equals(dcItem.getId())).collect(Collectors.toList());
				DeliveryChallanItem preItem = null;
				log.info("preItems.size(): " + preItems.size());
				if (preItems.size() != 0) {
					preItem = preItems.get(0);
				}
				log.info("preItem: " + preItem);
				log.info("dcItem.getQuantity(): " + dcItem.getQuantity());
				// log.info("preItem.getQuantity(): "+preItem.getQuantity());
				// post reverse if new quantity != previous quantity
				Double preQuantity = preItem != null ? preItem.getQuantity() : null;
				log.info("preQuantity: " + preQuantity);
				if (preItem != null && Double.compare(dcItem.getQuantity(), preQuantity) != 0) {
					log.info("preItem.getQuantity(): " + preItem.getQuantity());
					List<StockTrace> stItems = stReverseRequired.stream()
							.filter(st -> st.getTransactionId().equals(dcItem.getId())).collect(Collectors.toList());

					if (stItems.size() != 0) {
						StockTrace stItem = stItems.get(0);
						StockTrace stReverse = new StockTrace();

						stReverse.setBusinessDate(stItem.getBusinessDate());
						stReverse.setMaterial(stItem.getMaterial());
						stReverse.setParty(stItem.getParty());
						stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
						stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
						stReverse.setRemarks(stItem.getRemarks());
						stReverse.setReverse("Y");
						stReverse.setTransactionDate(stItem.getTransactionDate());
						stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
						stReverse.setTransactionNumber(stItem.getTransactionNumber());
						stReverse.setTransactionId(stItem.getTransactionId());
						stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
						stReverse.setTransactionNote(stItem.getTransactionNote());
						stReverse.setTransactionType(stItem.getTransactionType());

						stockTraceRepository.save(stReverse);
					}
				}
			}

			// Build stock trace for the invoice item if prev item quantity is
			// not equal to current quantity

			List<DeliveryChallanItem> preItems = dcHeaderPre.getDeliveryChallanItems().stream()
					.filter(i -> i.getId().equals(dcItem.getId())).collect(Collectors.toList());
			DeliveryChallanItem preItem = null;
			if (preItems.size() != 0) {
				preItem = preItems.get(0);
			}

			if (dcItem.getQuantity() != (preItem != null ? preItem.getQuantity() : null)) {
				StockTrace st = new StockTrace();

				if (stReverseRequired != null) {
					
					
					OptionalInt maxTransIdSeq = stReverseRequired
							.stream()
							.mapToInt(stss -> stss.getTransactionIdSequence())
							.max();
					log.info("maxTransIdSeq: " + maxTransIdSeq);
					
					List<StockTrace> stRc = stReverseRequired.stream()
							.filter(sts -> sts.getTransactionId().equals(dcItem.getId()))
							.filter(sts -> sts.getReverse() == null)
							.filter(sts -> maxTransIdSeq.isPresent() ? sts.getTransactionIdSequence() == maxTransIdSeq.getAsInt() : true)
							.collect(Collectors.toList());
					
					
//					List<StockTrace> stRc = stReverseRequired.stream()
//							.filter(stR -> stR.getTransactionId().equals(dcItem.getId())).collect(Collectors.toList());

					StockTrace stockTraceFirst = stRc.size() != 0 ? stRc.get(0) : null;
					transactionIdSequence = stockTraceFirst != null ? stockTraceFirst.getTransactionIdSequence() : 1;
				}
				st.setBusinessDate(dcHeader.getDeliveryChallanDate());
				st.setMaterial(dcItem.getMaterial());
				st.setParty(dcHeader.getParty());
				st.setQuantityIn(dcHeader.getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_CUSTOMER) ? 0.0 : dcItem.getQuantity());
				st.setQuantityOut(dcHeader.getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_CUSTOMER) ? dcItem.getQuantity() : 0.0);
				st.setRemarks(dcHeader.getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_CUSTOMER) ? "Sales" : "Purchase");
				st.setReverse(null);
				st.setTransactionDate(new Date());
				st.setTransactionHeaderId(dcHeader.getId());
				st.setTransactionNumber(dcHeader.getDeliveryChallanNumber());
				st.setTransactionId(dcItem.getId());
				// TODO configure
				st.setTransactionNote(dcHeader.getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_CUSTOMER) ? "Sales" : "Purchase");
				st.setTransactionType(dcHeader.getDeliveryChallanType());
				st.setTransactionIdSequence(++transactionIdSequence);

				stockTraceRepository.save(st);
			}

		});

		return null;
	}

	public StockTrace postReversalDcStockTrace(DeliveryChallanHeader dcHeader) {

		log.info("dcHeader.getId()" + dcHeader.getId());
		// Check if reversal required

		List<StockTrace> stReverseRequired = getByTransactionHeaderId(dcHeader.getId());

		log.info("stReverseRequired" + stReverseRequired);

		// Post reversal if required

		if (stReverseRequired != null) {
			stReverseRequired.forEach(stItem -> {

				// post reversal only if GRN item quantity is different from the
				// existing positing

				if (stItem.getQuantityIn() != dcHeader.getDeliveryChallanItems().stream()
						.filter(gi -> gi.getId().equals(stItem.getTransactionId())).mapToDouble(gi -> gi.getQuantity())
						.sum()) {
					StockTrace stReverse = new StockTrace();

					stReverse.setBusinessDate(stItem.getBusinessDate());
					stReverse.setMaterial(stItem.getMaterial());
					stReverse.setParty(stItem.getParty());
					stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
					stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
					stReverse.setRemarks(stItem.getRemarks());
					stReverse.setReverse("Y");
					stReverse.setTransactionDate(stItem.getTransactionDate());
					stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
					stReverse.setTransactionNumber(stItem.getTransactionNumber());
					stReverse.setTransactionId(stItem.getTransactionId());
					stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
					stReverse.setTransactionNote(stItem.getTransactionNote());
					stReverse.setTransactionType(stItem.getTransactionType());

					stockTraceRepository.save(stReverse);
				}
			});
		}
		return null;

	}

	@Override
	public StockTrace saveCreditNoteCreationStockTrace(InvoiceHeader invoiceHeader, InvoiceHeader invoiceHeaderPre) {
		log.info("1");
		List<StockTrace> stReverseRequired = getByTransactionHeaderId(invoiceHeader.getId());

		// transactionIdSequence = stReverseRequired != null ?
		// stReverseRequired.getTransactionIdSequence() : 1;
		log.info("2");
		invoiceHeader.getInvoiceItems().forEach(invoiceItem -> {

			// post if reversal required
			if (stReverseRequired != null) {

				log.info("3");
				List<InvoiceItem> preItems = invoiceHeaderPre.getInvoiceItems().stream()
						.filter(i -> i.getId().equals(invoiceItem.getId())).collect(Collectors.toList());
				InvoiceItem preItem = null;
				log.info("preItems.size(): " + preItems.size());
				if (preItems.size() != 0) {
					preItem = preItems.get(0);
				}
				log.info("preItem: " + preItem);
				log.info("invoiceItem.getQuantity(): " + invoiceItem.getQuantity());
				// log.info("preItem.getQuantity(): "+preItem.getQuantity());
				// post reverse if new quantity != previous quantity
				Double preQuantity = preItem != null ? preItem.getQuantity() : null;
				log.info("preQuantity: " + preQuantity);
				if (preItem != null && Double.compare(invoiceItem.getQuantity(), preQuantity) != 0) {
					log.info("preItem.getQuantity(): " + preItem.getQuantity());
					List<StockTrace> stItems = stReverseRequired.stream()
							.filter(st -> st.getTransactionId().equals(invoiceItem.getId())).collect(Collectors.toList());

					if (stItems.size() != 0) {
						StockTrace stItem = stItems.get(0);
						StockTrace stReverse = new StockTrace();

						stReverse.setBusinessDate(stItem.getBusinessDate());
						stReverse.setMaterial(stItem.getMaterial());
						stReverse.setParty(stItem.getParty());
						stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
						stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
						stReverse.setRemarks(stItem.getRemarks());
						stReverse.setReverse("Y");
						stReverse.setTransactionDate(stItem.getTransactionDate());
						stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
						stReverse.setTransactionNumber(stItem.getTransactionNumber());
						stReverse.setTransactionId(stItem.getTransactionId());
						stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
						stReverse.setTransactionNote(stItem.getTransactionNote());
						stReverse.setTransactionType(stItem.getTransactionType());

						stockTraceRepository.save(stReverse);
					}
				}
			}

			// Build stock trace for the invoice item if prev item quantity is
			// not equal to current quantity

			List<InvoiceItem> preItems = invoiceHeaderPre.getInvoiceItems().stream()
					.filter(i -> i.getId().equals(invoiceItem.getId())).collect(Collectors.toList());
			InvoiceItem preItem = null;
			log.info("preItems.size(): " + preItems.size());
			if (preItems.size() != 0) {
				preItem = preItems.get(0);
			}

			// log.info("preItem.getQuantity(): "+preItem.getQuantity());
			log.info("invoiceItem.getQuantity(): " + invoiceItem.getQuantity());

			// log.info("compare null...", Double.compare(1.0, (Double) null));

			int qtyCompare = Double.compare(invoiceItem.getQuantity(), (preItem != null ? preItem.getQuantity() : 0));

			log.info("qtyCompare: " + qtyCompare);

			if (Double.compare(invoiceItem.getQuantity(), (preItem != null ? preItem.getQuantity() : 0)) != 0) {
				StockTrace st = new StockTrace();
				log.info("inside stock trace");
				if (stReverseRequired != null) {
					
					OptionalInt maxTransIdSeq = stReverseRequired
							.stream()
							.mapToInt(stss -> stss.getTransactionIdSequence())
							.max();
					log.info("maxTransIdSeq: " + maxTransIdSeq);
					
					List<StockTrace> stRc = stReverseRequired.stream()
							.filter(sts -> sts.getTransactionId().equals(invoiceItem.getId()))
							.filter(sts -> sts.getReverse() == null)
							.filter(sts -> maxTransIdSeq.isPresent() ? sts.getTransactionIdSequence() == maxTransIdSeq.getAsInt() : true)
							.collect(Collectors.toList());
					
//					List<StockTrace> stRc = stReverseRequired.stream()
//							.filter(stR -> stR.getTransactionId().equals(invoiceItem.getId())).collect(Collectors.toList());

					StockTrace stockTraceFirst = stRc.size() != 0 ? stRc.get(0) : null;
					transactionIdSequence = stockTraceFirst != null ? stockTraceFirst.getTransactionIdSequence() : 1;
				}

				st.setBusinessDate(invoiceHeader.getInvoiceDate());
				st.setMaterial(invoiceItem.getMaterial());
				st.setParty(invoiceHeader.getParty());
				st.setQuantityIn(invoiceItem.getQuantity());
				st.setQuantityOut(0.0);
				st.setRemarks("Credit Note");
				st.setReverse(null);
				st.setTransactionDate(new Date());
				st.setTransactionHeaderId(invoiceHeader.getId());
				st.setTransactionNumber(invoiceHeader.getInvoiceNumber());
				st.setTransactionId(invoiceItem.getId());
				// TODO configure
				st.setTransactionNote("Credit Note");
				st.setTransactionType(transactionTypeService.getByName(ApplicationConstants.NOTE_TYPE_CREDIT));
				st.setTransactionIdSequence(++transactionIdSequence);

				log.info("before strace insert: " + st);
				stockTraceRepository.save(st);
			}

		});

		return null;
	}

	@Override
	public StockTrace postReversalCreditNoteStockTrace(InvoiceHeader invoiceHeader) {
		// Check if reversal required

		List<StockTrace> stReverseRequired = getByTransactionHeaderId(invoiceHeader.getId());

		// Post reversal if required

		if (stReverseRequired != null) {
			stReverseRequired.forEach(stItem -> {

				// post reversal only if invoice item quantity is different from
				// the existing positing
				if (stItem.getQuantityOut() != invoiceHeader.getInvoiceItems().stream()
						.filter(inv -> inv.getId().equals(stItem.getTransactionId())).mapToDouble(inv -> inv.getQuantity())
						.sum()) {
					StockTrace stReverse = new StockTrace();

					stReverse.setBusinessDate(stItem.getBusinessDate());
					stReverse.setMaterial(stItem.getMaterial());
					stReverse.setParty(stItem.getParty());
					stReverse.setQuantityIn(stItem.getQuantityIn() * -1);
					stReverse.setQuantityOut(stItem.getQuantityOut() * -1);
					stReverse.setRemarks(stItem.getRemarks());
					stReverse.setReverse("Y");
					stReverse.setTransactionDate(stItem.getTransactionDate());
					stReverse.setTransactionHeaderId(stItem.getTransactionHeaderId());
					stReverse.setTransactionNumber(stItem.getTransactionNumber());
					stReverse.setTransactionId(stItem.getTransactionId());
					stReverse.setTransactionIdSequence(stItem.getTransactionIdSequence() + 1);
					stReverse.setTransactionNote(stItem.getTransactionNote());
					stReverse.setTransactionType(stItem.getTransactionType());

					stockTraceRepository.save(stReverse);
				}
			});
		}
		return null;
	}
	
	@Override
	public Boolean deleteFirstMaterialInStockTrace(Material material,Long id) {
		Boolean response = false;
		
		try {
			
			Long materialCount=stockTraceRepository.getMatreialCountByMatreialId(id);
			
			if(materialCount==1) {
				stockTraceRepository.deleteByMaterial(material);
				response = true;
			}else {
				response = true;
			}
			
		}
		catch(Exception e){
			
			response = false;
			
		}
		return response;
}
}