package com.coreerp.serviceimpl;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.controller.InvoiceController;
import com.coreerp.dao.DeliveryChallanHeaderRepository;
import com.coreerp.dao.GRNHeaderRepository;
import com.coreerp.dao.InvoiceHeaderRepository;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dto.TransactionTypeDTO;
import com.coreerp.mapper.TransactionTypeMapper;
import com.coreerp.model.FinancialYear;
import com.coreerp.model.GlobalSetting;
import com.coreerp.model.TransactionType;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.GlobalSettingService;
import com.coreerp.service.TransactionTypeService;

@Service
public class TransactionTypeServiceImpl implements TransactionTypeService {
	
	final static Logger log = LogManager.getLogger(TransactionTypeServiceImpl.class);
	
	@Autowired
	private  TransactionTypeRepository transactionTypeRepository;
	
	@Autowired
	private TransactionTypeMapper transactionTypeMapper;
	
	@Autowired
	private GlobalSettingService globalSettingService;
	
	@Autowired
	InvoiceHeaderRepository invoiceHeaderRepository;
	
	@Autowired
	GRNHeaderRepository gRNHeaderRepository;
	
	@Autowired
	DeliveryChallanHeaderRepository deliveryChallanHeaderRepository;
	
	@Autowired
	FinancialYearService financialYearService;

	@Override
	public TransactionTypeDTO save(TransactionTypeDTO transactionTypeDTO) {
		return transactionTypeMapper.modelToDTOMap(transactionTypeRepository.save(transactionTypeMapper.dtoToModelMap(transactionTypeDTO)));
	}

	@Override
	public TransactionTypeDTO getById(Long id) {
		return transactionTypeMapper.modelToDTOMap(transactionTypeRepository.getOne(id));
	}

	@Override
	public TransactionType getModelById(Long id) {
		return transactionTypeRepository.getOne(id);
	}

	@Override
	public TransactionType getByName(String name) {
		return transactionTypeRepository.findByName(name);
	}

	@Override
	public List<TransactionTypeDTO> gtAll() {
		return transactionTypeMapper.modelToDTOList(transactionTypeRepository.findAll());
	}

	@Override
	public Boolean allowStockIncreaseOption() {
		
		//Get stock increase tran type from global setting
		GlobalSetting gSetting= globalSettingService.getGlobalSettingById(1l);
		
		Boolean retValue = false;
		TransactionType tType = null;
		List<TransactionType> tTypes = new ArrayList<TransactionType>();
		log.info("gSetting.getStockUpdateIncrease().getTransactionType().getName(): "+gSetting.getStockUpdateIncrease().getTransactionType().getName());
		FinancialYear financialYear =  financialYearService.findByIsActive(1);
		//Check if count exists in that transaction
		switch(gSetting.getStockUpdateIncrease().getTransactionType().getName()) {
			
			case ApplicationConstants.GRN_TYPE_SUPPLIER: 
				if(gRNHeaderRepository.existsByFinancialYear(financialYear)) {
					retValue = false;
				}else {
					retValue = true;
				}
				break;
			case ApplicationConstants.DC_TYPE_SUPPLIER:
				tType = transactionTypeRepository.findByName(ApplicationConstants.DC_TYPE_SUPPLIER);				
				tTypes.add(tType);
				
				if(deliveryChallanHeaderRepository.existsByFinancialYearAndDeliveryChallanTypeIn(financialYear, tTypes))
				{
					retValue = false;
				}else {
					retValue = true;
				}
				break;
			case ApplicationConstants.INVOICE_TYPE_SUPPLIER:
				tType = transactionTypeRepository.findByName(ApplicationConstants.INVOICE_TYPE_SUPPLIER);
				
				tTypes.add(tType);
				
				if(invoiceHeaderRepository.existsByFinancialYearAndInvoiceTypeIn(financialYear, tTypes))
				{
					retValue = false;
				}else {
					retValue = true;
				}
				log.info("returning: "+retValue);
				break;
		}
		
		
		
		// if exists return false
		// else return true
		
		return retValue;
	}

	@Override
	public Boolean allowStockDecreaseOption() {
		//Get stock increase tran type from global setting
		GlobalSetting gSetting= globalSettingService.getGlobalSettingById(1l);
		Boolean retValue = false;
		TransactionType tType = null;
		List<TransactionType> tTypes = new ArrayList<TransactionType>();
		FinancialYear financialYear =  financialYearService.findByIsActive(1);
		//Check if count exists in that transaction
		switch(gSetting.getStockUpdateDecrease().getTransactionType().getName()) {
			
			case ApplicationConstants.DC_TYPE_CUSTOMER:
				tType = transactionTypeRepository.findByName(ApplicationConstants.DC_TYPE_CUSTOMER);				
				tTypes.add(tType);
				
				if(deliveryChallanHeaderRepository.existsByFinancialYearAndDeliveryChallanTypeIn(financialYear, tTypes))
				{
					retValue = false;
				}else {
					retValue = true;
				}
				break;
			case ApplicationConstants.INVOICE_TYPE_CUSTOMER:
				tType = transactionTypeRepository.findByName(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
				
				tTypes.add(tType);
				
				if(invoiceHeaderRepository.existsByFinancialYearAndInvoiceTypeIn(financialYear,tTypes))
				{
					retValue = false;
				}else {
					retValue = true;
				}
				break;
		}
		
		
		
		// if exists return false
		// else return true
		
		return retValue;
	}


	@Override
	public Integer getJwTransactionType(){
		List<String> tTypes = new ArrayList<String>();

		String jI=ApplicationConstants.INVOICE_TYPE_INCOMING_JW_INVOICE;
		tTypes.add(jI);
	String jID=	ApplicationConstants.INCOMING_JOBWORK_IN_DC;
		tTypes.add(jID);
		String jOD=	ApplicationConstants.INCOMING_JOBWORK_OUT_DC;
		tTypes.add(jOD);
		String jP	=ApplicationConstants.PURCHASE_ORDER_INCOMING_JOBWORK;
		tTypes.add(jP);
		String jC	=ApplicationConstants.JOBWORK_CREDIT_NOTE;
		tTypes.add(jC);
		Integer transactionCount =transactionTypeRepository.findTransactionType(tTypes,"N");

    System.out.println(transactionCount+"---------------------------------");
		return transactionCount;

	}









//	public String getBuyOrSell(Long id){
//		String retValue = "";
//
//		List<String> buyTransactions = Arrays.asList(ApplicationConstants.INVOICE_TYPE_SUPPLIER, ApplicationConstants.PURCHASE_ORDER_TYPE_SUPPLIER, ApplicationConstants.DC_TYPE_SUPPLIER, ApplicationConstants.NOTE_TYPE_DEBIT);
//
//		List<String> sellTransactions = Arrays.asList(ApplicationConstants.INVOICE_TYPE_PROFORMA, ApplicationConstants.INVOICE_TYPE_CUSTOMER, ApplicationConstants.PURCHASE_ORDER_TYPE_CUSTOMER, ApplicationConstants.DC_TYPE_CUSTOMER, ApplicationConstants.QUOTATION_TYPE_CUSTOMER, ApplicationConstants.NOTE_TYPE_CREDIT);
//
//		return retValue;
//	}
}
