package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.AreaRepository;
import com.coreerp.dao.CityRepository;
import com.coreerp.dto.AreaDTO;
import com.coreerp.model.Area;
import com.coreerp.model.City;
import com.coreerp.service.AreaService;

@Service
public class AreaServiceImpl implements AreaService {

	@Autowired
	AreaRepository areaRepository;
	
	@Autowired
	CityRepository cityRepository;

	@Override
	public AreaDTO saveArea(AreaDTO areaDTO) {
		Area area=DTOToModelMap(areaDTO);
		Area areaOut =  areaRepository.save(area);
		
		AreaDTO areaDTOOut = new AreaDTO();
		
		areaDTOOut.setCityId(areaOut.getCity() != null ? areaOut.getCity().getId() : null);
		areaDTOOut.setDeleted(areaOut.getDeleted());
		areaDTOOut.setId(areaOut.getId());
		areaDTOOut.setName(areaOut.getName());
		
		return areaDTOOut;
	}

	private Area DTOToModelMap(AreaDTO areaDTO){
		Area area=new Area();
		area.setId(areaDTO.getId());
		area.setName(areaDTO.getName());
		area.setCity(areaDTO.getCityId() != null ? cityRepository.getOne(areaDTO.getCityId()) : null);
		area.setDeleted(areaDTO.getDeleted());
		return area;
	}
	/*@Override
	public List<Area> getAllAreas() {
		// TODO Auto-generated method stub
		return areaRepository.findAll();
	}*/
	
	@Override
	public List<AreaDTO> getAllAreas() {
		List<AreaDTO> areaDTOList= new ArrayList<AreaDTO>();
		List<Area> areaList=areaRepository.findAll();
		
		for(Area area:areaList){
			AreaDTO areaDTO=new AreaDTO();
			areaDTO.setId(area.getId());
			areaDTO.setName(area.getName());
			areaDTO.setDeleted(area.getDeleted());
			areaDTO.setCityId(area.getCity() != null ? area.getCity().getId() : null);
			areaDTOList.add(areaDTO);
		}
		return areaDTOList;
	}

	@Override
	public Area getAreaById(Long id) {
		return areaRepository.getOne(id);
	}

	@Override
	public List<AreaDTO> getAreasByCity(Long cityId) {
		
		City city = cityRepository.getOne(cityId);
		
		List<AreaDTO> areaDTOList= new ArrayList<AreaDTO>();
		List<Area> areaList=areaRepository.findByCity(city);
		
		for(Area area:areaList){
			AreaDTO areaDTO=new AreaDTO();
			areaDTO.setId(area.getId());
			areaDTO.setName(area.getName());
			areaDTO.setDeleted(area.getDeleted());
			areaDTO.setCityId(area.getCity() != null ? area.getCity().getId() : null);
			areaDTOList.add(areaDTO);
		}
		return areaDTOList;
		
	}

	@Override
	public Area deleteAreaById(Long id) {
		areaRepository.deleteById(id);
		return null;
	}
	
	
}
