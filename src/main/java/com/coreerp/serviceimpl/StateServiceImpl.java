package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.CountryRepository;
import com.coreerp.dao.StateRepository;
import com.coreerp.dto.StateDTO;
import com.coreerp.model.Country;
import com.coreerp.model.State;
import com.coreerp.service.StateService;

@Service
public class StateServiceImpl implements StateService {

	final static Logger log = LogManager.getLogger(StateServiceImpl.class);

	@Autowired
	StateRepository stateRepository;
	
	@Autowired
	CountryRepository countryRepository;
	
	
	@Override
	public StateDTO saveState(StateDTO stateDTO) {
		State state = mapDTOtoModel(stateDTO);
		
		return mapModelToDTO(stateRepository.save(state));
		
	}
	
	private StateDTO mapModelToDTO(State state){
		
		StateDTO dto = new StateDTO();
		dto.setCountryId(state.getCountry().getId());
		dto.setDeleted(state.getDeleted());
		dto.setId(state.getId());
		dto.setName(state.getName());
		dto.setStateCode(state.getStateCode());
		
		return dto;
	}

	private State mapDTOtoModel(StateDTO stateDTO) {
		State state = new State();
		state.setId(stateDTO.getId());
		state.setName(stateDTO.getName());
		state.setCountry(countryRepository.getOne(stateDTO.getCountryId()));
		//state.setCities(stateDTO.getCities());
		state.setDeleted(stateDTO.getDeleted());
		state.setStateCode(stateDTO.getStateCode());
		
		return state;
	}

	/*@Override
	public List<StateDTO> getAllStates() {
		return stateRepository.findAll();
	}*/

	@Override
	public List<StateDTO> getAllStates(){
		List<StateDTO> stateDTOList = new ArrayList<StateDTO>();
		List<State> stateList = stateRepository.findAll();
		
		for(State state : stateList){
			StateDTO stateDTO = new StateDTO();
			stateDTO.setId(state.getId());
			stateDTO.setName(state.getName());
			stateDTO.setDeleted(state.getDeleted());
			stateDTO.setCountryId(state.getCountry().getId());
			stateDTO.setStateCode(state.getStateCode());
			stateDTOList.add(stateDTO);
			
			//log.info("returning state: "+stateDTO);
		}
	
	return stateDTOList;
	
	}
	
	public List<StateDTO> getAllStatesForCountry(Long countryId){
		
		Country country = countryRepository.getOne(countryId);
		
		List<StateDTO> stateDTOList = new ArrayList<StateDTO>();
		List<State> stateList = stateRepository.findByCountry(country);
		
		for(State state : stateList){
			StateDTO stateDTO = new StateDTO();
			stateDTO.setId(state.getId());
			stateDTO.setName(state.getName());
			stateDTO.setDeleted(state.getDeleted());
			stateDTO.setCountryId(state.getCountry().getId());
			stateDTO.setStateCode(state.getStateCode());
			stateDTOList.add(stateDTO);
			
			//log.info("returning state: "+stateDTO);
		}
	
	return stateDTOList;
	
	}
	
	
	
	@Override
	public State getStateById(Long id) {
		return stateRepository.getOne(id);
	}

	@Override
	public State deleteStateById(Long id) {
		
		State state = stateRepository.getOne(id);
		log.info("deleting state: "+state);
		stateRepository.deleteForce(id);
		
		return state;
	}

}
