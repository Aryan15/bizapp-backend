package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import com.coreerp.dto.NumberRangeConfigurationDTO;
import com.coreerp.service.NumberRangeConfigurationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.FinancialYearRepository;
import com.coreerp.dto.FinancialYearDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.FinancialYearMapper;
import com.coreerp.model.FinancialYear;
import com.coreerp.service.FinancialYearService;

@Service
public class FinancialYearServiceImpl implements FinancialYearService {
    final static Logger log = LogManager.getLogger(FinancialYearServiceImpl.class);
	@Autowired
	FinancialYearRepository financialYearRepository;
	
	@Autowired
	FinancialYearMapper financialYearMapper;

	@Autowired
	NumberRangeConfigurationService numberRangeConfigurationService;
	@Override
	public FinancialYear getById(Long id) {
		return financialYearRepository.getOne(id);
	}

	@Override
	public FinancialYear save(FinancialYear financialYear) {
		return financialYearRepository.save(financialYear);
	}

	@Override
	public FinancialYear findByIsActive(Number isActive){
		return financialYearRepository.findByIsActive(isActive);
	}
	
	@Override
	public List<FinancialYearDTO> getAllFinancialYears(){
			return financialYearMapper.modelToDTOList(financialYearRepository.findAll());	
	}
	
	@Override
	public List<FinancialYearDTO> saveAll(List<FinancialYearDTO> dtoList) {
		
		List<FinancialYearDTO> outList = new  ArrayList<FinancialYearDTO>();
		
		dtoList.forEach(dto -> {
			FinancialYear fout = financialYearRepository.save(financialYearMapper.dtoToModelMap(dto));
			outList.add(financialYearMapper.modelToDTOMap(fout));
		});

		List<NumberRangeConfigurationDTO> numberRangeConfigurations = numberRangeConfigurationService.getAll();
		List<NumberRangeConfigurationDTO> numberRangeConfigurationsUpdate = new ArrayList<>();
		numberRangeConfigurations.forEach(numberRangeConfiguration ->
				{
					if (numberRangeConfiguration.getAutoNumberReset() == 1 && numberRangeConfiguration.getStartNumber() != null) {
						numberRangeConfiguration.setStartNumber(1);
						numberRangeConfigurationsUpdate.add(numberRangeConfiguration);
					}

				}

		);

		numberRangeConfigurationService.saveAll(numberRangeConfigurationsUpdate);

		return outList;
	
	}
	
	@Override
	public TransactionResponseDTO delete(Long id) {
		
		TransactionResponseDTO res = new TransactionResponseDTO();
		
		String deleteStatus = "Transaction setting deleted successfully";
		Integer responseStatus = 1;
		
		try {
			financialYearRepository.deleteById(id);
		}catch(Exception e){
			deleteStatus = "Cannot Delete quotation";
			responseStatus = 0;
			
		}
		
		res.setResponseString(deleteStatus);
		res.setResponseStatus(responseStatus);
		return res;
	}
	
}

