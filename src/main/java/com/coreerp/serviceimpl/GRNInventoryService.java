package com.coreerp.serviceimpl;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.mapper.MaterialMapper;
import com.coreerp.model.GRNHeader;
import com.coreerp.model.GlobalSetting;
import com.coreerp.model.InvoiceItem;
import com.coreerp.model.GRNItem;
import com.coreerp.model.Material;
import com.coreerp.service.GlobalSettingService;
import com.coreerp.service.InventoryService;
import com.coreerp.service.StockTraceService;

@Component
public class GRNInventoryService implements InventoryService<GRNHeader> {

	final static Logger log = LogManager.getLogger(GRNInventoryService.class);
	
	@Autowired
	MaterialRepository materialRepository;
	
	@Autowired
	GlobalSettingService globalSettingService;
	
	@Autowired
	MaterialMapper materialMapper;
	
	
	@Autowired
	StockTraceService stockTraceService;
	
	/* GRN
	 * ** Update Material stock if decrease/increase stock is on GRN 
	 * ** if GRN status is 'New' or 'Open'
	 * 
	 * 		** update stock = stock + GRN item accepted quantity
	 * 
 	 *  ** if GRN status is 'Cancelled'
	 * 
	 * 		** update stock = stock - GRN item accepted quantity
	 * 
	 * ** if GRN status is 'Deleted'
	 * 
	 * 		** update stock = stock - GRN  item accepted quantity	
	 */
	@Override
	public String updateInventory(GRNHeader transactionComponent, GRNHeader grnHeaderPre) {
		String updateInventoryStatus = "Failed";
		
		if(stockUpdateApplicable(transactionComponent)){
			if(transactionComponent.getGrnStatus().getName().equals(ApplicationConstants.GRN_STATUS_NEW) || transactionComponent.getGrnStatus().getName().equals(ApplicationConstants.GRN_STATUS_OPEN)){
				updateInventoryStatus = increaseStock(transactionComponent, grnHeaderPre);
			}else if(transactionComponent.getGrnStatus().getName().equals(ApplicationConstants.GRN_STATUS_CANCELLED)){
				updateInventoryStatus = decreaseStock(transactionComponent, grnHeaderPre);
			}else if(transactionComponent.getGrnStatus().getName().equals(ApplicationConstants.GRN_STATUS_DELETED)){
				updateInventoryStatus = decreaseStock(transactionComponent, grnHeaderPre);
			}
			
			//Stock Trace
			
			if(transactionComponent.getGrnStatus().getName().equals(ApplicationConstants.GRN_STATUS_CANCELLED) || transactionComponent.getGrnStatus().getName().equals(ApplicationConstants.GRN_STATUS_DELETED)){
				stockTraceService.postReversalGrnStockTrace(transactionComponent);
			}else{
				stockTraceService.saveGrnCreationStockTrace(transactionComponent, grnHeaderPre);
				
				log.info("Handle materials got deleted recently");
				grnHeaderPre.getGrnItems()
				.forEach(preItem -> {
					
					Optional<GRNItem> grnItem =
							transactionComponent.getGrnItems()
							.stream()
							.filter(ini -> ini.getId().equals(preItem.getId()))
							.findFirst()
							;
							
							//If item doesn't exist, reverse stock trace
							if(!grnItem.isPresent()){
					
								log.info("calling reverse item for grn with material: "+ preItem.getMaterial().getName());
								stockTraceService.postReversalGrnItemStockTrace(preItem);
							}
				});
				;
			}
		}
		
		
		return updateInventoryStatus;
	}

//	private String increaseStock(GRNHeader transactionComponent, GRNHeader grnHeaderPre ) {
//		String status = "Failed";
//		
//		transactionComponent.getGrnItems().stream()
//							.forEach(grnItem -> {
//								Material material = materialRepository.getOne(grnItem.getMaterial().getId());
//								log.info("material: "+material);
//								if(material.getSupplyType().getName().equals(ApplicationConstants.SUPPLY_TYPE_GOODS)){
//								material.setStock(material.getStock()!= null ? (material.getStock() + grnItem.getAcceptedQuantity()) : grnItem.getAcceptedQuantity());								
//								
//								//MaterialDTO materialDTO = materialMapper.modelToDTOMap(material);
//								materialRepository.save(material);
//								}
//							});
//		status = "Success";
//		
//		return status;
//	}
	private String increaseStock(GRNHeader transactionComponent, GRNHeader grnHeaderPre ) {
		String status = "Failed";
		log.info("in increaseStock"+ grnHeaderPre);
		transactionComponent.getGrnItems().stream()
		.forEach(grnItem -> {
			Material material = materialRepository.getOne(grnItem.getMaterial().getId());
//			
			if(material.getSupplyType().getName().equals(ApplicationConstants.SUPPLY_TYPE_GOODS)){
				Double stockValue = grnItem.getAcceptedQuantity();
				log.info("got material: "+material);
				log.info("got stockValue: "+stockValue);
				//get previous invoice item
				List<GRNItem> tempGRNItems = grnHeaderPre.getGrnItems().stream().filter(item -> item.getId() == grnItem.getId()).collect(Collectors.toList());
				GRNItem GRNItemPre = null;
				log.info("got tempGRNItems: "+tempGRNItems);
				if(tempGRNItems.size() > 0){
					GRNItemPre = tempGRNItems.get(0) ;
				}
					

				if(GRNItemPre != null){
					log.info("got prev invoice item quantity: "+GRNItemPre.getAcceptedQuantity());
					log.info("got current invoice item quantity: "+grnItem.getAcceptedQuantity());
					//if previous quantity is more than recent one, 
					//earlier stock update should get decreased to the difference value
					if(GRNItemPre.getAcceptedQuantity() > grnItem.getAcceptedQuantity()){
						stockValue = material.getStock() -  (GRNItemPre.getAcceptedQuantity() - grnItem.getAcceptedQuantity()) ;
						log.info("1 "+stockValue);
					}else
					//if recent quantity is more than previous quantity,
					//stock is just increased to the difference amount
					if(GRNItemPre.getAcceptedQuantity() < grnItem.getAcceptedQuantity()){
						stockValue = material.getStock() + (grnItem.getAcceptedQuantity() - GRNItemPre.getAcceptedQuantity());
						log.info("2 "+stockValue);
					}
					//if no change in quantity, stock should is unchanged
					else{
						stockValue = material.getStock();
						log.info("3 "+stockValue);
					}
				}
				else
				{
					log.info("4.."+stockValue);
					//stockValue = material.getStock() + grnItem.getAcceptedQuantity();
					stockValue = material.getStock() != null ?  material.getStock() + grnItem.getAcceptedQuantity() : grnItem.getAcceptedQuantity();
					log.info("4"+stockValue);
				}
				
				material.setStock(material.getStock() != null ? stockValue : (0 + stockValue));
				log.info("5"+stockValue);
			}
		});
		
		
		//Handle deleted items
				if(grnHeaderPre != null){
					grnHeaderPre.getGrnItems().forEach(preItem -> {
						Material material = materialRepository.getOne(preItem.getMaterial().getId());
						Optional<GRNItem> grnItem =
						transactionComponent.getGrnItems()
						.stream()
						.filter(ini -> ini.getId().equals(preItem.getId()))
						.findFirst()
						;
						
						//If item doesn't exist, decrease material stock
						if(!grnItem.isPresent()){
							Double newStock = preItem.getMaterial().getStock() - preItem.getAcceptedQuantity();
							material.setStock(newStock);
							
						}
						
					});
				}
		
		status = "Success";
		
		return status;
	}
	private String decreaseStock(GRNHeader transactionComponent, GRNHeader grnHeaderPre) {
		String status = "Failed";
		
		transactionComponent.getGrnItems().stream()
							.forEach(grnItem -> {
								Material material = materialRepository.getOne(grnItem.getMaterial().getId());

								Double quantity = grnItem.getAcceptedQuantity();
								
								OptionalDouble preQuantity = grnHeaderPre != null ? grnHeaderPre.getGrnItems()
										.stream()
										.filter(iitem -> iitem.getId().equals(grnItem.getId()))
										.mapToDouble(iitem -> iitem.getAcceptedQuantity())
										.findFirst() : null;
										
								Double updateQuantity = (preQuantity != null && preQuantity.isPresent() && quantity != preQuantity.getAsDouble() )? (quantity - preQuantity.getAsDouble()) : quantity;
								
								if(material.getSupplyType().getName().equals(ApplicationConstants.SUPPLY_TYPE_GOODS)){
								material.setStock(material.getStock() != null ? material.getStock() - updateQuantity : (0 - updateQuantity));								
								
								//MaterialDTO materialDTO = materialMapper.modelToDTOMap(material);
								materialRepository.save(material);
								}
							});
		status = "Success";
		
		return status;
	}
	
//	private String decreaseStock(GRNHeader transactionComponent, GRNHeader grnHeaderPre) {
//		String status = "Failed";
//		log.info("in decreaseStock"+ grnHeaderPre);
//		transactionComponent.getGrnItems().stream()
//		.forEach(grnItem -> {
//			Material material = materialRepository.getOne(grnItem.getMaterial().getId());
////			
//			if(material.getSupplyType().getName().equals(ApplicationConstants.SUPPLY_TYPE_GOODS)){
//				Double stockValue = grnItem.getAcceptedQuantity();
//				log.info("got material: "+material);
//				log.info("got stockValue: "+stockValue);
//				//get previous invoice item
//				List<GRNItem> tempGRNItems = grnHeaderPre.getGrnItems().stream().filter(item -> item.getId() == grnItem.getId()).collect(Collectors.toList());
//				GRNItem GRNItemPre = null;
//				log.info("got tempGRNItems: "+tempGRNItems);
//				if(tempGRNItems.size() > 0){
//					GRNItemPre = tempGRNItems.get(0) ;
//				}
//					
//
//				if(GRNItemPre != null){
//					log.info("got prev invoice item quantity: "+GRNItemPre.getAcceptedQuantity());
//					log.info("got current invoice item quantity: "+grnItem.getAcceptedQuantity());
//					//if previous quantity is more than recent one, 
//					//earlier stock update should get increased to the difference value
//					if(GRNItemPre.getAcceptedQuantity() > grnItem.getAcceptedQuantity()){
//						stockValue = material.getStock() +  (GRNItemPre.getAcceptedQuantity() - grnItem.getAcceptedQuantity()) ;
//						log.info("1 "+stockValue);
//					}else
//					//if recent quantity is more than previous quantity,
//					//stock is just decreased to the difference amount
//					if(GRNItemPre.getAcceptedQuantity() < grnItem.getAcceptedQuantity()){
//						stockValue = material.getStock() - (grnItem.getAcceptedQuantity() - grnItem.getAcceptedQuantity());
//						log.info("2 "+stockValue);
//					}
//					//if no change in quantity, stock should is unchanged
//					else{
//						stockValue = material.getStock();
//						log.info("3 "+stockValue);
//					}
//				}
//				else
//				{
//					log.info("4.."+stockValue);
//					stockValue = material.getStock() - grnItem.getAcceptedQuantity();
//					log.info("4"+stockValue);
//				}
//				
//				material.setStock(material.getStock() != null ? stockValue : (0 - stockValue));
//				log.info("5"+stockValue);
//			}
//		});
//		status = "Success";		
//		
//		return status;
//		
//	}
	
	private boolean stockUpdateApplicable(GRNHeader grnHeader) {
		boolean returnValue = false;
		
		GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);
		log.info("grnHeader.getGrnType().getName(): "+grnHeader.getGrnType().getName());
		log.info("ApplicationConstants.GRN_TYPE_SUPPLIER: "+ApplicationConstants.GRN_TYPE_SUPPLIER);
		log.info("globalSetting: "+globalSetting.getStockUpdateIncrease().getTransactionType().getName());
		if(grnHeader.getGrnType().getName().equals(ApplicationConstants.GRN_TYPE_SUPPLIER)){
			log.info("Supplier GRN");
			if(globalSetting.getStockUpdateIncrease().getTransactionType().getName().equals(grnHeader.getGrnType().getName())){
				log.info("GRN mapped for stock increase");
				returnValue = true;
			}
		}
		
		return returnValue;
	}

}
