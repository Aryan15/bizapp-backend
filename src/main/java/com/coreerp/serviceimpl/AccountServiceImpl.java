package com.coreerp.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.AccountRepository;
import com.coreerp.dto.AccountDTO;
import com.coreerp.mapper.AccountMapper;
import com.coreerp.model.Account;
import com.coreerp.service.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	AccountMapper accountMapper;
	
	
	@Override
	public AccountDTO save(AccountDTO accountDTO) {

		
		return accountMapper.modelToDTOMap(accountRepository.save(accountMapper.dtoToModelMap(accountDTO)));
	}

	@Override
	public AccountDTO getById(Long id) {
		return accountMapper.modelToDTOMap(accountRepository.getOne(id));
	}

	@Override
	public Account getModelById(Long id) {
		return accountRepository.getOne(id);
	}

}
