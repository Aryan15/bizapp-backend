package com.coreerp.serviceimpl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import com.coreerp.dao.*;
import com.coreerp.dto.*;
import com.coreerp.jwt.JwtAuthenticationRequest;
import com.coreerp.jwt.JwtAuthenticationResponse;
import com.coreerp.mapper.AutoTransactionMapper;
import com.coreerp.model.*;
import com.coreerp.multitenancy.TenantContext;
import com.coreerp.reports.model.WeeklyPurhaseOrderReport;
import com.coreerp.reports.model.WeeklyTransactionReportModel;
import com.coreerp.service.*;

import com.coreerp.utils.DateUtils;
import com.coreerp.utils.EmailService;
import com.google.common.io.ByteStreams;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.core.env.Environment;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.coreerp.ApplicationConstants;
import com.coreerp.mapper.InvoiceHeaderMapper;
import com.coreerp.mapper.InvoiceItemMapper;

import javax.mail.util.ByteArrayDataSource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service

public class InvoiceServiceImpl implements InvoiceService {

    final static Logger log = LogManager.getLogger(InvoiceServiceImpl.class);

    @Autowired
    StatusService statusService;

    @Autowired
    InvoiceHeaderRepository invoiceHeaderRepository;

    @Autowired
    InvoiceItemRepository invoiceItemRepository;

    @Autowired
    InvoiceHeaderMapper invoiceHeaderMapper;

    @Autowired
    InvoiceItemMapper invoiceItemMapper;

    @Autowired
    InvoiceStatusService invoiceStatusService;

    @Autowired
    InvoiceBackTrackingServiceNew invoiceBacktrackingService;

    @Autowired
    InvoiceInventoryService invoiceInventoryService;

    @Autowired
    JournalEntryService JournalEntryService;

    @Autowired
    PartyService partyService;

    @Autowired
    PartyTypeService partyTypeService;

    @Autowired
    TransactionTypeService transactionTypeService;

    @Autowired
    StockTraceService stockTraceService;

    @Autowired
    // MaterialService materialService;
            MaterialRepository materialRepository;

    @Autowired
    MaterialService materialService;

    @Autowired
    GlobalSettingService globalSettingService;

    @Autowired
    TransactionTypeRepository transactionTypeRepository;

    @Autowired
    PayableReceivableItemRepository payableReceivableItemRepository;

    @Autowired
    TransactionStatusChangeRepository transactionStatusChangeRepository;

    @Autowired
    MaterialPriceListService materialPriceListService;

    @Autowired
    InvoiceHeaderAuditRepository invoiceHeaderAuditRepository;

    @Autowired
    InvoiceItemAuditRepository invoiceItemAuditRepository;


    @Autowired
    AutoTransactionLogRepository transactionLogRepository;

    @Autowired
    AutoTransactionRepository autoTransactionRepository;


    @Autowired
    FinancialYearService financialYearService;

    @Autowired
    AutoTransactionMapper autoTransactionMapper;

    @Autowired
    PartyRepository partyRepository;

    @Autowired
    private BCryptPasswordEncoder bCryprPasswordEncoder;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    JasperReportServiceImpl jasperReportService;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private EmailSettingRepository emailSettingRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    UserTenantRelationRepository userTenantRelationRepository;

    @Autowired
    private Environment environment;

    @Autowired
    StockTraceRepository stockTraceRepository;

    @Autowired
    NumberRangeConfigurationRepository numberRangeConfigurationRepository;

    @Autowired
    PrintTemplateRepository printTemplateRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PurchaseOrderHeaderRepository purchaseOrderHeaderRepository;

    @Autowired
    DeliveryChallanService deliveryChallanService;

    @Autowired
    DeliveryChallanItemRepository deliveryChallanItemRepository;

    @Autowired
    EwayBillTrackingRepository ewayBillTrackingRepository;
    public InvoiceServiceImpl() {
    }


//	@Autowired
//	MaterialService materialService;

    @Override
    public Page<InvoiceHeader> getAllInvoiceHeader(Pageable pageable) {
        return invoiceHeaderRepository.findAll(pageable);
    }

    @Override
    public void saveInvoiceHeader(InvoiceHeader invoiceHeader) {
        invoiceHeaderRepository.save(invoiceHeader);

    }

    @Override
    public Iterable<InvoiceHeader> findAllInvoiceHeaders() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void deleteInvoiceHeader(String invoiceHeaderId) {
        invoiceHeaderRepository.deleteById(invoiceHeaderId);
    }

    @Override
    public Page<InvoiceItem> getAllInvoiceItems(Pageable pageable) {
        return invoiceItemRepository.findAll(pageable);
    }

    @Override
    public void saveInvoiceItem(InvoiceItem invoiceItem) {
        invoiceItemRepository.save(invoiceItem);
    }

    @Override
    public Iterable<InvoiceItem> findAllInvoiceItems() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void deleteInvoiceItem(String invoiceItemId) {
        invoiceItemRepository.deleteById(invoiceItemId);
    }

    @Override
    public List<InvoiceDTO> getByPartyAndTransactionTypeAndStatusNotIn(Party party, String transactionType,
                                                                       List<Status> statuses) {

        log.info(party.getName()+"---"+transactionType+"----"+statuses);

      TransactionType transactionType1=  transactionTypeService.getByName(transactionType);
        log.info(transactionType1+"----------");

        List<InvoiceDTO> returnList = invoiceHeaderMapper.modelToDTOList(invoiceHeaderRepository.findByPartyAndInvoiceTypeAndStatusNotIn(party,
                transactionTypeService.getByName(transactionType), statuses));


        log.info(returnList.size()+"---------");
        List<InvoiceDTO> invList = new ArrayList<InvoiceDTO>();

        returnList.forEach(inv -> {
            InvoiceDTO invDTO = inv;
            List<InvoiceItemDTO> invItems = new ArrayList<InvoiceItemDTO>();

            invItems.addAll(inv.getInvoiceItems().stream().filter(ini -> ini.getNoteBalanceQuantity() > 0).collect(Collectors.toList()));
            invDTO.setInvoiceItems(invItems);

            invList.add(invDTO);
        });


        return invList;
    }

    @Override
    public List<InvoiceItem> getAllInvoiceItemsByHeaderId(String invoiceHeaderId, Pageable pageable) {

        return invoiceItemRepository.findByHeaderId(invoiceHeaderId);
    }

    @Override
    public InvoiceHeader getInvoiceHeaderByHeaderId(String invoiceHeaderId) {

        return invoiceHeaderRepository.findByHeaderId(invoiceHeaderId);
    }

    @Override
    public List<InvoiceDTO> findAllInvoices() {

        List<InvoiceHeader> invoiceHeaders = invoiceHeaderRepository.findAll();
        List<InvoiceDTO> invoiceDTOList = new ArrayList<InvoiceDTO>();
        for (InvoiceHeader invoiceHeader : invoiceHeaders) {
            invoiceDTOList.add(invoiceHeaderMapper.modelToDTOMap(invoiceHeader));
        }
        log.info("invoiceDTOList.invoiceItems: "
                + (invoiceDTOList.size() > 0 ? invoiceDTOList.get(0).getInvoiceItems().size() : 0));
        return invoiceDTOList;
    }

    @Override
    public List<InvoiceDTO> findAllCustomerInvoices() {

        PartyType partyType = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);

        return findAllInvoicesForPartyType(partyType);
    }

    @Override
    public InvoiceDTO getInvoicesByHeaderId(String invoiceHeaderId) {

        // InvoiceHeader invoiceHeader =
        // invoiceHeaderRepository.findByHeaderId(invoiceHeaderPK.getId(),
        // invoiceHeaderPK.getIdVersion());
        InvoiceHeader invoiceHeader = invoiceHeaderRepository.getOne(invoiceHeaderId);
        //log.info("in getInvoicesByHeaderId invoice header id: " + invoiceHeader.getId());
        //log.info("in getInvoicesByHeaderId invoice item : " + invoiceHeader.getInvoiceItems());

        if (invoiceHeader.getInvoiceItems().size() == 0) {
            List<InvoiceItem> invoiceItems = invoiceItemRepository.findByHeaderId(invoiceHeader.getId());
            //log.info("2 in getInvoicesByHeaderId invoice item : " + invoiceItems);
            invoiceHeader.setInvoiceItems(invoiceItems);
            //log.info("3 in getInvoicesByHeaderId invoice item : " + invoiceHeader.getInvoiceItems());
        }
        return invoiceHeaderMapper.modelToDTOMap(invoiceHeader);
    }

    /*
     * CUSTOMER INVOICE save the invoice... ** derive status as New if incoming
     * id is null and/or status is null/ 'New' ** derive status as Open if
     * incoming id is not null and status != 'Cancelled'
     *
     * update DC (create new version and update prev version latest to null) ..
     * ** update linked DC status from Active to Partial if invoice item level
     * quantity < DC quantity ** update linked DC status from Active/Partial to
     * Completed if invoice item level quantity = DC quantity
     *
     * update PO (create new version and update prev version latest to null)..
     * ** update linked PO status from Active to Partial if invoice item level
     * quantity < PO quantity ** update linked PO status from Active/Partial to
     * Completed if invoice item level quantity = PO quantity
     *
     * Update Material stock if decrease stock is on Customer Invoice ** update
     * stock = stock - invoice item quantity
     *
     * save the invoice(create new version and update prev version latest to
     * null) ** derive status as Cancelled if incoming id is not null/ 'New' and
     * status = 'Cancelled'
     *
     * update DC (create new version and update prev version latest to null) ..
     * ** update linked DC status from Completed to Active if invoice item level
     * quantity = DC quantity ** update linked DC status from Completed to
     * Partial if invoice item level quantity < DC quantity ** update linked DC
     * status from Partial to Active if invoice item level quantity < DC
     * quantity
     *
     * update PO (create new version and update prev version latest to null)..
     * ** update linked PO status from Completed to Active if invoice item level
     * quantity = DC quantity ** update linked PO status from Completed to
     * Partial if invoice item level quantity < DC quantity ** update linked PO
     * status from Partial to Active if invoice item level quantity < DC
     * quantity
     *
     * Update Material stock if decrease stock is on Customer Invoice ** update
     * stock = stock + invoice item quantity
     *
     * save the invoice as Deleted ** validate if the deleting invoice is the
     * latest in the series ** derive status as Deleted if incoming id is not
     * null and status = 'Deleted'
     *
     * update DC (create new version and update prev version latest to null) ..
     * ** update linked DC status from Completed to Active if invoice item level
     * quantity = DC quantity ** update linked DC status from Completed to
     * Partial if invoice item level quantity < DC quantity ** update linked DC
     * status from Partial to Active if invoice item level quantity < DC
     * quantity
     *
     * update PO (create new version and update prev version latest to null)..
     * ** update linked PO status from Completed to Active if invoice item level
     * quantity = DC quantity ** update linked PO status from Completed to
     * Partial if invoice item level quantity < DC quantity ** update linked PO
     * status from Partial to Active if invoice item level quantity < DC
     * quantity
     *
     * Update Material stock if decrease stock is on Customer Invoice ** update
     * stock = stock + invoice item quantity
     */
    @Override
    @Transactional
    public InvoiceDTO saveInvoice(InvoiceWrapperDTO invoiceWrapperIn) {

        InvoiceDTO invoiceDTOIn = invoiceWrapperIn.getInvoiceHeader();

        // Get older invoice if exists

        InvoiceHeader invoiceHeaderPre = new InvoiceHeader();
        List<InvoiceItem> invoiceItemsPre = new ArrayList<InvoiceItem>();

        if (invoiceDTOIn.getId() != null) {
            invoiceHeaderRepository.getOne(invoiceDTOIn.getId()).getInvoiceItems().forEach(item -> {
                InvoiceItem invoiceItemPre = new InvoiceItem();
                invoiceItemPre.setId(item.getId());
                if(invoiceDTOIn.getTransactionStatus()!=null && invoiceDTOIn.getTransactionStatus().equals(ApplicationConstants.TRANSACTION_CANCELLED)) {

                    invoiceItemPre.setQuantity(0.0);
              }
                else{
                    invoiceItemPre.setQuantity(item.getQuantity());
                }
                invoiceItemPre.setMaterial(item.getMaterial());
                invoiceItemPre.setDeliveryChallanItem(item.getDeliveryChallanItem());
                invoiceItemPre.setPurchaseOrderItem(item.getPurchaseOrderItem());
                invoiceItemPre.setSourceInvoiceItem(item.getSourceInvoiceItem());
                invoiceItemPre.setProformaInvoiceItem(item.getProformaInvoiceItem());
                invoiceItemPre.setGrnItem(item.getGrnItem());
                invoiceItemsPre.add(invoiceItemPre);
            });
        }

        // invoiceHeaderPre.setInvoiceItems(invoiceDTOIn.getId() != null ?
        // invoiceHeaderRepository.getOne(invoiceDTOIn.getId()).getInvoiceItems()
        // : null);

        invoiceHeaderPre.setInvoiceItems(invoiceItemsPre);


        //InvoiceDTO invoiceDTOIn = invoiceWrapperIn.getInvoiceHeader();

        log.info("before item delete: " + invoiceWrapperIn.getItemToBeRemove());

        if (invoiceWrapperIn.getItemToBeRemove().size() > 0) {
            invoiceWrapperIn.getItemToBeRemove().forEach(id -> {
                log.info("delete id: " + id);
                deleteItem(id);
            });
        }


//		invoiceHeaderPre.getInvoiceItems().forEach(item -> {
//			log.info("prev:--- " + item.getMaterial().getName() + ": " + item.getQuantity());
//		});

        // InvoiceHeader invoiceHeaderPre = invoiceDTOIn.getId() != null ?
        // invoiceHeaderRepository.getOne(invoiceDTOIn.getId()) : null;



        InvoiceHeader invoiceHeader = invoiceHeaderMapper.dtoToModelMap(invoiceDTOIn);

//		log.info("invoiceHeader............>>>>" + invoiceHeader.getBillToAddress() + "name :" +invoiceHeader.getParty().getName());
//		log.info("invoiceHeader 2............>>>>" + invoiceHeader + invoiceHeader.getGstNumber());

        // Derive status
        invoiceHeader.setStatus(getInvoiceStatus(invoiceHeader));

        log.info(getInvoiceStatus(invoiceHeader)+"getInvoiceStatus(invoiceHeader)");


        List<InvoiceItem> invoiceItems = invoiceItemMapper.dtoToModelList(invoiceDTOIn.getInvoiceItems());// invoiceDTOItemToInvoiceItemMap(invoiceDTOIn.getInvoiceItems(),
        // invoiceHeader);

        // set header to items
        invoiceItems.stream().forEach(invoiceItem -> invoiceItem.setInvoiceHeader(invoiceHeader));

        // set items to header
        invoiceHeader.setInvoiceItems(invoiceItems);

        // save
        InvoiceHeader invoiceHeaderOut = invoiceHeaderRepository.save(invoiceHeader);
        InvoiceHeader invoiceHeaderUpdate=invoiceHeaderOut;
        log.info("invoiceHeader............>>>>" + invoiceHeaderOut.getId() + "name :" +invoiceHeaderOut.getParty().getName());
        invoiceHeaderRepository.flush();


        if((invoiceHeaderOut.getInvoiceType().getName().equals(ApplicationConstants.JOBWORK_CREDIT_NOTE)|| invoiceHeaderOut.getInvoiceType().getName().equals(ApplicationConstants.SUBCONTRACT_DEBIT_NOTE)) && invoiceHeaderOut.getMaterialNoteType()==ApplicationConstants.MATERIAL_NOTE_TYPE_REWORK) {
            DeliveryChallanDTO deliveryChallanDTO =  createInDC(invoiceHeaderOut);
            DeliveryChallanHeaderWrapperDTO deliveryChallanHeaderWrapperDTO =new DeliveryChallanHeaderWrapperDTO();
            deliveryChallanHeaderWrapperDTO.setItemToBeRemove(null);
            deliveryChallanHeaderWrapperDTO.setDeliveryChallanHeader(deliveryChallanDTO);

            deliveryChallanService.save(deliveryChallanHeaderWrapperDTO);
        }


//		invoiceHeaderOut.getInvoiceItems().forEach(item -> {
//			log.info("after save:--- " + item.getId());
//		});

        // Status change entry

        statusChangeEntry(invoiceHeaderOut);


        // Manage inventory for Debit/Credit Note
        if (invoiceHeaderOut.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_DEBIT)
                || invoiceHeaderOut.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_CREDIT) || invoiceHeaderOut.getInvoiceType().getName().equals(ApplicationConstants.JOBWORK_CREDIT_NOTE)|| invoiceHeaderOut.getInvoiceType().getName().equals(ApplicationConstants.SUBCONTRACT_DEBIT_NOTE)) {
            String updateInventoryStatus = invoiceInventoryService.updateInventory(invoiceHeaderOut, invoiceHeaderPre);
            log.info("updateInventoryStatus: " + updateInventoryStatus);
          }

        // back track DC/PO/Quot/Proforma/Invoice(from Note)
        if (!invoiceHeaderOut.getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_PROFORMA) && invoiceHeaderOut.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)&& !invoiceHeaderOut.getInvoiceType().getName().equals(ApplicationConstants.JOBWORK_PROFORMA_INVOICE)) {
            deleteStatusChangeEntry(invoiceHeaderOut.getId());

        }
        invoiceBacktrackingService.updateTransactionStatus(invoiceHeaderOut, invoiceHeaderPre);

        //Update source invoice due amount if saving CREDIT NOTE or DEBIT NOTE
//		if (invoiceHeaderOut.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_CREDIT)
//				|| invoiceHeaderOut.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_DEBIT))
//		{
//			//String sourceInvoiceId =
//			Optional<InvoiceHeader> sourceInvoice = invoiceHeaderOut.getInvoiceItems().stream()
//					.findFirst()
//					.map(ini -> ini.getSourceInvoiceItem() != null ? ini.getSourceInvoiceItem().getInvoiceHeader() : null);
//			log.info("source Invoice: "+sourceInvoice);
//			try {
//				if(sourceInvoice.isPresent()) {
//					InvoiceHeader sourceInvoiceObject = sourceInvoice.get();
//					sourceInvoiceObject.setDueAmount(sourceInvoiceObject.getDueAmount() - invoiceHeaderOut.getGrandTotal());
//					invoiceHeaderRepository.save(sourceInvoiceObject);
//					invoiceHeaderRepository.flush();
//				}
//
//			} catch (TransactionDataException e) {
//				log.error("Somer error settig due amount: "+e);
//			}
//
//
//		}


        //Update material spec and hsn

        invoiceDTOIn.getInvoiceItems()
                .forEach(invItem -> {
                    materialService.updateMaterialHsnAndSpec(invItem.getMaterialId(), invItem.getHsnOrSac(), invItem.getSpecification());
                });


        if (invoiceHeaderOut.getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_CUSTOMER)
                || invoiceHeaderOut.getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_SUPPLIER)) {
            // Update material price -- NOT REQUIRED AS PER ISSUE #895. Consider adding a setting to make price update
//			if (!invoiceHeaderOut.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED))
//				materialPriceUpdate(invoiceDTOIn, invoiceHeaderOut.getInvoiceType().getName());


//			invoiceHeaderPre.getInvoiceItems().forEach(item -> {
//				log.info("2 prev:--- " + item.getMaterial().getName() + ": " + item.getQuantity());
//			});

            // Manage inventory
            String updateInventoryStatus = invoiceInventoryService.updateInventory(invoiceHeaderOut, invoiceHeaderPre);

            log.info("updateInventoryStatus: " + updateInventoryStatus);


            // Post a journal entry
            // JournalEntryService.saveInvoiceJournalEntry(invoiceHeaderOut);
        }

//		invoiceHeaderOut.getInvoiceItems().forEach(item -> {
//			log.info("before return:--- " + item.getId());
//		});

        log.info("before item delete: " + invoiceWrapperIn.getItemToBeRemove());

        if (invoiceWrapperIn.getItemToBeRemove().size() > 0) {
            invoiceWrapperIn.getItemToBeRemove().forEach(id -> {
                log.info("delete id: " + id);
                deleteItem(id);
            });
        }
        log.info("invoiceHeaderOut............>>>>" + invoiceHeaderOut.getId() + "name :" +invoiceHeaderOut.getParty().getName());

        List<InvoiceItem> invoiceItemsUpdate = new ArrayList<InvoiceItem>();

 if(invoiceHeaderOut.getStatus().getName().equals(ApplicationConstants.TRANSACTION_CANCELLED)){

          invoiceHeaderOut.getInvoiceItems().forEach(item -> {
             if(item.getDeliveryChallanItem()!=null){
               item.setDeliveryChallanItem(null);
                 invoiceItemsUpdate.add(item);
             }
            if(item.getPurchaseOrderItem()!=null){
                item.setPurchaseOrderItem(null);
                invoiceItemsUpdate.add(item);
            }
          if(item.getGrnItem()!=null){
                 item.setGrnItem(null);
             invoiceItemsUpdate.add(item);
            }

         if(item.getProformaInvoiceItem()!=null){
               item.setProformaInvoiceItem(null);
                invoiceItemsUpdate.add(item);
             }


      });
        invoiceItemsUpdate.forEach(item ->{
           invoiceItemRepository.save(item);

         });


    }


        return invoiceHeaderMapper.modelToDTOMap(invoiceHeaderOut);
    }

    private void materialPriceUpdate(InvoiceDTO invoiceHeader, String invoiceType) {

        GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);

        // String invoiceType = invoiceHeader.getInvoiceType().getName();
        log.info("invoice type:------- " + invoiceType);
        log.info("invoice type: " + invoiceType);
        log.info("ApplicationConstants.INVOICE_TYPE_SUPPLIER: " + ApplicationConstants.INVOICE_TYPE_SUPPLIER);

        invoiceHeader.getInvoiceItems().forEach(item -> {

            Material material = materialRepository.getOne(item.getMaterialId()); // materialService.getMaterialById(item.getMaterial().getId());

            Boolean updateRequired = false;

            log.info("globalSetting.getPurchasePrice(): " + globalSetting.getPurchasePrice());
            log.info("material.getBuyingPrice(): " + material.getBuyingPrice());
            log.info("item.getPrice(): " + item.getPrice());

            if (!materialPriceListService.checkForPartyAndMaterial(invoiceHeader.getPartyId(), item.getMaterialId())) {
                if (invoiceType.equals(ApplicationConstants.INVOICE_TYPE_SUPPLIER)) {

                    log.info("inside supplier invoice check");

                    if (globalSetting.getPurchasePrice() == ApplicationConstants.MATERIAL_PRICING_TYPE_BUYING_PRICE) {
                        log.info("purchase price is buying price");
                        if (material.getBuyingPrice() != item.getPrice()) {
                            material.setBuyingPrice(item.getPrice());
                            updateRequired = true;
                        }

                    } else if (globalSetting.getPurchasePrice() == ApplicationConstants.MATERIAL_PRICING_TYPE_MRP) {
                        log.info("purchase price is MRP");
                        if (material.getMrp() != item.getPrice()) {
                            material.setMrp(item.getPrice());
                            updateRequired = true;

                        }

                    }

                } else if (invoiceType.equals(ApplicationConstants.INVOICE_TYPE_CUSTOMER)
                        || invoiceType.equals(ApplicationConstants.INVOICE_TYPE_PROFORMA)) {

                    log.info("inside customer invoice check");

                    if (globalSetting.getSalePrice() == ApplicationConstants.MATERIAL_PRICING_TYPE_SELLING_PRICE) {

                        log.info("selling price is selling price");

                        if (material.getPrice() != item.getPrice()) {

                            material.setPrice(item.getPrice());
                            updateRequired = true;
                        }

                    } else if (globalSetting.getSalePrice() == ApplicationConstants.MATERIAL_PRICING_TYPE_MRP) {

                        log.info("selling price is MRP");

                        if (material.getMrp() != item.getPrice()) {
                            material.setMrp(item.getPrice());
                            updateRequired = true;
                        }

                    }

                }
            }

//			if (item.getHsnOrSac() != null && (!item.getHsnOrSac().equals(material.getHsnCode()))) {
//				material.setHsnCode(item.getHsnOrSac());
//				updateRequired = true;
//			}
//			log.info("specification======>" + item.getSpecification());
//			log.info("specification======>" + material.get);


            log.info("updateRequired: " + updateRequired);

            if (updateRequired) {


                materialRepository.save(material);

            }

        });

    }

    private Status getInvoiceStatus(InvoiceHeader invoiceHeader) {
        return invoiceStatusService.getStatus(invoiceHeader);
    }

    @Override
    public InvoiceDTO findInvoiceByInvoiceNumber(String invoiceNumber) {
        return invoiceHeaderMapper.modelToDTOMap(invoiceHeaderRepository.findByInvoiceNumber(invoiceNumber));
    }

    public Long getMaxInvoiceNumber(String prefix) {
        return invoiceHeaderRepository.getMaxInvoiceNumber(prefix);

    }

    public Long getMaxInvoiceNumberByTransactionType(Long transactionTypeId) {
        return invoiceHeaderRepository.getMaxInvoiceNumberByTransactionType(transactionTypeId);

    }


    @Override
    public InvoiceHeader save(InvoiceHeader invoiceHeaderIn) {
        return invoiceHeaderRepository.save(invoiceHeaderIn);
    }

    @Override
    public List<InvoiceDTO> getInvoicesForPartyAndTransactionType(Long partyId, Long transactionTypeId) {

        return invoiceHeaderMapper.modelToDTOList(invoiceHeaderRepository.findByPartyAndInvoiceType(
                partyService.getPartyObjectById(partyId), transactionTypeService.getModelById(transactionTypeId)));
    }

    @Override
    public List<InvoiceDTO> findAllSupplierInvoices() {

        PartyType partyType = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);

        return findAllInvoicesForPartyType(partyType);
    }

    private List<InvoiceDTO> findAllInvoicesForPartyType(PartyType partyType) {

        List<Party> customerList = partyService.getPartyModelsByPartyType(partyType);

        List<InvoiceHeader> invoiceHeaders = invoiceHeaderRepository.findByPartyList(customerList);
        List<InvoiceDTO> invoiceDTOList = new ArrayList<InvoiceDTO>();
        for (InvoiceHeader invoiceHeader : invoiceHeaders) {
            invoiceDTOList.add(invoiceHeaderMapper.modelToDTOMap(invoiceHeader));
        }
        log.info("invoiceDTOList.invoiceItems: "
                + (invoiceDTOList.size() > 0 ? invoiceDTOList.get(0).getInvoiceItems().size() : 0));
        return invoiceDTOList;

    }

    @Override
    public InvoiceItem getItemById(String itemId) {
        return invoiceItemRepository.getOne(itemId);
    }

    @Override
    public InvoiceHeader getModelById(String id) {
        return invoiceHeaderRepository.getOne(id);
    }

    @Override
    public List<InvoiceDTO> getInvoicesForPartyAndTransactionTypeWithNonZeroDueAmount(Long partyId,
                                                                                      Long transactionTypeId) {

        TransactionType transactionType = transactionTypeService.getModelById(transactionTypeId);

//		List<Status> statuses = Arrays.asList(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DC_GENERATED), statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DC_GENERATED), statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED));
        List<Status> statuses = new ArrayList<Status>();

        //statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED));
        statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.INVOICE_STATUS_CANCELLED));


        return invoiceHeaderMapper.modelToDTOList(invoiceHeaderRepository
                .findByPartyAndInvoiceTypeAndDueAmountGreaterThanAndStatusNotInOrderByInvoiceDateAscInvIdAsc(partyService.getPartyObjectById(partyId),
                        transactionTypeService.getModelById(transactionTypeId), 0.0, statuses));
    }

    private boolean saveAllowed(InvoiceHeader invDelete) {

        boolean returnValue = false;

        if (invDelete.getStatus() != null) {

            String invStatus = invDelete.getStatus().getName();

            if ((invStatus.equals(ApplicationConstants.INVOICE_STATUS_DELETED))) {
                // deliveryChallanHeaderOut = cancelledStatus(in);

                List<PayableReceivableItem> prItems = payableReceivableItemRepository.findByInvoiceHeader(invDelete);

                if (prItems.isEmpty()) {

                    returnValue = true;

                }
            } else {
                returnValue = true;
            }

        } else {
            returnValue = true;
        }

        return returnValue;

    }

    @Override
    public TransactionResponseDTO delete(String id,Integer reuseId) {
        Long invCount=0l;
        TransactionResponseDTO res = new TransactionResponseDTO();

        String deleteStatus = "Invoice Deleted Successfully";
        Integer deleteStatusNum = 1;


        log.info("1");
        // InvoiceHeader invoiceHeader =
        // invoiceHeaderMapper.dtoToModelMap(invoiceDTOIn);

        //check invoice used in auto transaction


        Long count = checkInvoiceInAutoTranaction(id);

        if(count>0){
            deleteStatus = "Cannot delete this invoice,Since it has been used in recurring invoice,If you want to delete please stop the recurring invoice option";
            deleteStatusNum = 0;
        }
        else {

            // If not latest invoice, do not delete
            InvoiceHeader inhDelete = invoiceHeaderRepository.getOne(id);
            InvoiceHeader inhPre = new InvoiceHeader();
            inhPre.setStatus(inhDelete.getStatus());
            String commonSequence = invoiceHeaderRepository.getCommonSequence(inhDelete.getInvoiceType().getId());
            log.info("inhDelete.getInvoiceType(): " + inhDelete.getInvoiceType());
            if(inhDelete.getInvoiceType().getName().equals(ApplicationConstants.JOBWORK_CREDIT_NOTE)|| inhDelete.getInvoiceType().getName().equals(ApplicationConstants.SUBCONTRACT_DEBIT_NOTE)||inhDelete.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_CREDIT) || inhDelete.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_DEBIT))
            {
                deleteStatus=" Note Deleted Successfully";
            }
            log.info("commonSequence: " + commonSequence);
            List<Long> commonSequenceList = new ArrayList<>();

            if (commonSequence != null) {
                commonSequenceList = Arrays.stream(commonSequence.split(",")).map(Long::parseLong).collect(Collectors.toList());
            } else {
                commonSequenceList.add(inhDelete.getInvoiceType().getId());
            }
            if(reuseId==null ||reuseId==0 ) {
              invCount = invoiceHeaderRepository.countMaxInvCount(inhDelete.getInvId(), commonSequenceList, inhDelete.getFinancialYear().getId());
            }
            log.info("invCount: " + invCount);
            if (invCount > 0) {
                if (inhDelete.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_CREDIT) || inhDelete.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_DEBIT) || inhDelete.getInvoiceType().getName().equals(ApplicationConstants.JOBWORK_CREDIT_NOTE)|| inhDelete.getInvoiceType().getName().equals(ApplicationConstants.SUBCONTRACT_DEBIT_NOTE)) {
                    deleteStatus = "Cannot delete older notes";
                } else {
                    deleteStatus = "Cannot delete older invoices";
                }
                deleteStatusNum = 0;
                log.info("3");

            } else {

                try {

                    Boolean result=getEwaybillbyInvId(inhDelete);
                    log.info(result+"-------result");

                    if( result==false)
                    {
                        deleteStatus="Cannot Delete,Eway Bill generated for this invoice";
                        deleteStatusNum = 0;

                    }
                    else {


                        if (saveAllowed(inhDelete)) {
                            // Derive status
                            inhDelete.setStatus(statusService.getStatusByTransactionTypeAndName(inhDelete.getInvoiceType(),
                                    ApplicationConstants.INVOICE_STATUS_DELETED));

                            log.info("4");
                            // back track DC/PO/Quot
                            if (!inhDelete.getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_PROFORMA) && !inhDelete.getInvoiceType().getName().equals(ApplicationConstants.JOBWORK_PROFORMA_INVOICE)) {
                                deleteStatusChangeEntry(id);

                                invoiceBacktrackingService.updateTransactionStatus(inhDelete, null);
                            }
                            log.info("5");
                            // Manage inventory
                            String updateInventoryStatus = invoiceInventoryService.updateInventory(inhDelete, inhPre);

                            log.info("updateInventoryStatus: " + updateInventoryStatus);
                            // Stock Trace for customer Invoice

//					List<String> reverseStockStatuses = new ArrayList<String>();
//					reverseStockStatuses.add(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
//					reverseStockStatuses.add(ApplicationConstants.NOTE_TYPE_CREDIT);
//					reverseStockStatuses.add(ApplicationConstants.NOTE_TYPE_DEBIT);
//					
//					reverseStockStatuses.contains(inhDelete.getInvoiceType().getName());
//					
//					if (reverseStockStatuses.contains(inhDelete.getInvoiceType().getName()) ) {
//						stockTraceService.postReversalCustomerInvoiceStockTrace(inhDelete);
//						log.info("6");
//					}

                            // Post a journal entry
//					JournalEntryService.saveInvoiceJournalEntry(inhDelete);

                            log.info("7");

                            //Make Audit entry
                            auditEntry(inhDelete);
                            // Do the actual physical delete... no need to keep record
                            // in deleted status
                            inhDelete.getInvoiceItems().forEach(item -> {
                                invoiceItemRepository.delete(item);
                            });

                            log.info("8");
                            invoiceHeaderRepository.delete(inhDelete);
                            log.info("9");

                            DeliveryChallanHeader deliveryChallanHeader = deliveryChallanService.findByNoteId(inhDelete.getId());
                            if (deliveryChallanHeader != null) {

                                deliveryChallanService.delete(deliveryChallanHeader.getId());
                            }
                        } else {
                            deleteStatus = "Cannot Delete Invoice!! Its Used in Payable-receivable";
                            deleteStatusNum = 0;
                            deleteAuditEntry(id);
                        }
                    }
                } catch (Exception e) {
                    deleteStatus = "Cannot Delete Invoice";
                    deleteStatusNum = 0;
                    log.error("10 " + e);
                    throw e;
                }
            }
        }

        res.setResponseString(deleteStatus);
        res.setResponseStatus(deleteStatusNum);

        log.info("11");
        return res;
    }

    private boolean getEwaybillbyInvId(InvoiceHeader invoiceHeader)
    {


        List<EwayBillTracking> ewayBillObj =ewayBillTrackingRepository.findByInvoiceHeader(invoiceHeader);

        if(ewayBillObj.size()>0)
            return false;
        else
            return true;

    }
    private void auditEntry(InvoiceHeader inhDelete) {

        InvoiceHeaderAudit header = invoiceHeaderMapper.modelToAuditMap(inhDelete);

        invoiceHeaderAuditRepository.save(header);

        List<InvoiceItemAudit> items = invoiceItemMapper.modelToAuditList(inhDelete.getInvoiceItems());

        items.forEach(item -> {
            invoiceItemAuditRepository.save(item);
        });

    }


    private void deleteAuditEntry(String headerId) {

        InvoiceHeaderAudit header = invoiceHeaderAuditRepository.getOne(headerId);

        if (header != null) {
            List<InvoiceItemAudit> items = invoiceItemAuditRepository.findByInvoiceHeaderId(headerId);
            if (items != null && items.size() > 0) {
                items.forEach(item -> {
                    invoiceItemAuditRepository.delete(item);
                });
            }

            invoiceHeaderAuditRepository.delete(header);
        }

    }

    @Override
    public TransactionResponseDTO deleteItem(String id) {
        TransactionResponseDTO res = new TransactionResponseDTO();
        log.info("in deleteItem: " + id);
        res.setResponseStatus(1);
        String deleteStatus = "Invoice Item Deleted Successfully";
        try {
            InvoiceItem invoiceDelete = invoiceItemRepository.getOne(id);
            // update stock
//			Material material = materialRepository.getOne(invoiceDelete.getMaterial().getId());
//			if (material.getSupplyType().getName().equals(ApplicationConstants.SUPPLY_TYPE_GOODS) && invoiceDelete.getInvoiceHeader().getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_CUSTOMER)) {
//				material.setStock(
//						material.getStock() != null ? material.getStock() + invoiceDelete.getQuantity() : null);
//				materialRepository.save(material);
//			}
            //invoiceBacktrackingService.itemDelete(invoiceDelete);
            invoiceItemRepository.delete(invoiceDelete);
            invoiceItemRepository.flush();
            log.info("After delete");

        } catch (Exception e) {
            deleteStatus = "Cannot Delete Invoice Item";
            log.info("Error: " + e);
            res.setResponseStatus(0);
        }

        res.setResponseString(deleteStatus);
        return res;
    }

    @Override
    public InvoiceHeader getByInvoiceNumberAndTransactionType(String invNumber, TransactionType invType) {
        // TODO Auto-generated method stub
        return invoiceHeaderRepository.findByInvoiceNumberAndInvoiceType(invNumber, invType);
    }

    @Override
    public Long countByInvoiceNumberAndTransactionType(String invNumber, TransactionType invType, Long partyId) {
        Party party = partyService.getPartyObjectById(partyId);
        FinancialYear financialYear = financialYearService.findByIsActive(1);
        List<String> buyInvTypes = Arrays.asList(
                ApplicationConstants.INVOICE_TYPE_SUPPLIER
                , ApplicationConstants.NOTE_TYPE_DEBIT
                , ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE);

        if (buyInvTypes.contains(invType.getName())) {
            return invoiceHeaderRepository.countByInvoiceNumberAndInvoiceTypeAndPartyAndFinancialYear(invNumber, invType, party, financialYear);
        } else {
            return invoiceHeaderRepository.countByInvoiceNumberAndInvoiceTypeAndFinancialYear(invNumber, invType, financialYear);
        }
    }


    private void statusChangeEntry(InvoiceHeader invHeader) {

        TransactionStatusChange transactionStatusChange = new TransactionStatusChange();

        transactionStatusChange.setSourceOperation(null);
        transactionStatusChange.setSourceTransactionId(null);
        transactionStatusChange.setSourceTransactionType(null);
        transactionStatusChange.setSourceTransactionNumber(null);
        transactionStatusChange.setStatus(invHeader.getStatus());
        transactionStatusChange.setTransactionId(invHeader.getId());
        transactionStatusChange.setTransactionType(invHeader.getInvoiceType());
        transactionStatusChange.setTransactionNumber(invHeader.getInvoiceNumber());
        transactionStatusChange.setParty(invHeader.getParty());

        transactionStatusChangeRepository.save(transactionStatusChange);

    }

    private void deleteStatusChangeEntry(String id) {

        List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository.findByTranId(id);

        transactionStatusChangeList.forEach(tr -> {

            transactionStatusChangeRepository.deleteTransaction(tr);
        });

        List<TransactionStatusChange>  transactionStatusChangeLists = transactionStatusChangeRepository.findByStranId(id);

        transactionStatusChangeLists.forEach(tr -> {

            transactionStatusChangeRepository.deleteTransaction(tr);
        });


    }




    @Override
    public List<InvoiceDTO> getByPartyAndTransactionTypeAndStatusNotIn(Party party, TransactionType transactionType,
                                                                       List<Status> statuses) {
        return invoiceHeaderMapper.modelToDTOList(invoiceHeaderRepository.findByPartyAndInvoiceTypeAndStatusNotIn(party,
                transactionType, statuses));
    }

    @Override
    public Long getTotalCountByInvoiceNumber() {
        return invoiceHeaderRepository.count();
    }


    @Override
    public AutoTransactionGenerationDTO saveAutoTransactionGeneration(AutoTransactionGenerationDTO autoTransactionGenerationDTO) {
        InvoiceHeader invoice = new InvoiceHeader();
        FinancialYear financialYear = financialYearService.findByIsActive(1);
        AutoTransactionGeneration autoTransactionGeneration = autoTransactionMapper.dtoToModelMap(autoTransactionGenerationDTO);
        AutoTransactionGeneration autoTransactionGenerationOut = new AutoTransactionGeneration();


        log.info(autoTransactionGeneration.getId() + "date" + autoTransactionGeneration.getEndDate());
        Integer day = Calendar.getInstance().get(Calendar.DATE);


        if (autoTransactionGeneration.getInvoiceNumber() != null) {
            invoice = invoiceHeaderRepository.findByInvoiceNumberAndFinancialYear(autoTransactionGeneration.getInvoiceNumber(), financialYear);
            autoTransactionGeneration.setInvoiceHeaderId(invoice.getId());


        } else if (autoTransactionGeneration.getInvoiceNumber() == null) {

            invoice = invoiceHeaderRepository.findByHeaderId(autoTransactionGeneration.getInvoiceHeaderId());
            autoTransactionGeneration.setInvoiceNumber(invoice.getInvoiceNumber());

        }



    if(autoTransactionGeneration.getId() == null) {
       TransactionResponseDTO  transactionResponseDTO = checkForValidAutoTransaction(autoTransactionGeneration);
         if(transactionResponseDTO.getResponseStatus()==1) {
             autoTransactionGenerationOut.setId(0l);
          }
           else{
               autoTransactionGenerationOut = autoTransactionRepository.save(autoTransactionGeneration);
          }
      }
      else {
         autoTransactionGenerationOut = autoTransactionRepository.save(autoTransactionGeneration);
     }

        return autoTransactionMapper.modelToDTOMap(autoTransactionGenerationOut);
    }


    @Override
    @Transactional
    public void generateAutoTransaction(EmailSetting emailSetting, String email, String userName, String name,String password) {

        log.info("inside generateAutoTransaction");
        log.info("setting tenant: " + TenantContext.getCurrentTenant());
        List<String> deleteItem = new ArrayList<>();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date todayDate = new Date();
        String todaydates = dateFormat.format(todayDate);

        Integer stopTransaction = 1;
        List<AutoTransactionGeneration> autoGenerationDetails = autoTransactionRepository.getAutoGenerationDetailsByDates(todaydates);

        log.info("setting tenant2 : " + TenantContext.getCurrentTenant());
        log.info("autoGenerationDetails size" + autoGenerationDetails.size());

        autoGenerationDetails.forEach(autoT -> {
            log.info(TenantContext.getCurrentTenant() + ": need to process: " + autoT);
        });
        if (autoGenerationDetails != null && autoGenerationDetails.size() > 0) {
            autoGenerationDetails.forEach(autoTran -> {
                log.info("autoTransaction object---------------------" + autoTran);


                Integer invoiceCount = 1;

                Calendar calendar = Calendar.getInstance();
                log.info(calendar + "calendar");
                Date today = new Date();
                log.info(today + "calendar");
                Integer lastDayOfMonth = calendar.getInstance().getActualMaximum(Calendar.DATE);
                log.info(lastDayOfMonth + "lastDayOfMonth");
                Integer dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

                log.info(dayOfWeek + "dayOfWeek");

                Date currentDate = new Date();

                InvoiceHeader invoiceHeader = invoiceHeaderRepository.findByHeaderId(autoTran.getInvoiceHeaderId());


                List<InvoiceItem> invoiceItems = invoiceItemRepository.findByHeaderId(invoiceHeader.getId());

                invoiceHeader.setInvoiceItems(invoiceItems);

                entityManager.detach(invoiceHeader);


                List<InvoiceItemDTO> outInvoiceItemDto = new ArrayList<>();

                InvoiceDTO invoiceDTO = invoiceHeaderMapper.modelToDTOMap(invoiceHeader);

                invoiceDTO.setId(null);
                invoiceDTO.setInvoiceNumber(null);
                invoiceDTO.setInvId(null);
                invoiceDTO.setInvoiceDate(currentDate);
                List<InvoiceItemDTO> invoiceItemDTOS = invoiceDTO.getInvoiceItems();
                invoiceItemDTOS.forEach(invoiceItemDTO -> {
                    invoiceItemDTO.setId(null);
                    invoiceItemDTO.setHeaderId(null);
                    outInvoiceItemDto.add(invoiceItemDTO);
                });
                invoiceDTO.setInvoiceItems(outInvoiceItemDto);

                InvoiceWrapperDTO invoiceWrapperDTO = new InvoiceWrapperDTO();
                invoiceWrapperDTO.setInvoiceHeader(invoiceDTO);
                invoiceWrapperDTO.setItemToBeRemove(deleteItem);

                InvoiceDTO invoiceDTOOut = saveInvoice(invoiceWrapperDTO);
                Integer amount = invoiceDTOOut.getGrandTotal().intValue();
                String amountInWords = DateUtils.amountInWord(amount);
                TransactionType transactionType = transactionTypeRepository.getOne(1l);
             NumberRangeConfiguration numberRangeConfiguration =    numberRangeConfigurationRepository.findByTransactionType(transactionType);
              PrintTemplate printTemplate =  printTemplateRepository.getOne(numberRangeConfiguration.getPrintTemplate().getId());

              Party party=  partyRepository.getOne(autoTran.getPartyId());



          if(printTemplate.getIsJasperPrint()==1 && autoTran.getEmail()==1) {

              try {

               Boolean genratedPrint = getReport(printTemplate.getJasperFileName(), autoTran.getInvoiceHeaderId(), ApplicationConstants.COPY_TEXT, amountInWords, numberRangeConfiguration.getPrintHeaderText(), emailSetting, email, userName, name, password,numberRangeConfiguration.getAllowShipingAddress(),party.getEmail(),invoiceDTOOut.getInvoiceNumber(),invoiceDTOOut.getPartyName(), request);


              } catch (JRException e) {
                  e.printStackTrace();
              } catch (SQLException | IOException e) {
                  e.printStackTrace();
              }
          }


                invoiceCount = invoiceCount + 1;

                log.info(autoTran.getInvoiceHeaderId() + "autoTran.getInvoiceHeaderId()");
                log.info(currentDate + "currentDate");
                log.info(invoiceDTOOut.getId() + "invoiceDTOOut.getId()");
                log.info(invoiceCount + "invoiceCount");
                saveAutoTransactionLog(autoTran.getInvoiceHeaderId(), currentDate, invoiceDTOOut.getId(), invoiceCount);


            });
        }


    }


    public boolean saveAutoTransactionLog(String invoiceHeaderId, Date currentDate, String generatedInvoiceId, Integer transactionCount) {

        AutoTransactionLog autoTransactionLog = new AutoTransactionLog();
        autoTransactionLog.setInvoiceHeaderId(invoiceHeaderId);
        autoTransactionLog.setTransactionGenratedDate(currentDate);
        autoTransactionLog.setGeneratedInvoiceId(generatedInvoiceId);
        autoTransactionLog.setTransactionCount(transactionCount);
        transactionLogRepository.save(autoTransactionLog);

        return true;


    }


    @Override
    public List<InvoiceDTO> getAllInvoiceForCustomer(Long partyId,Long transactionTypes) {
        TransactionType transactionType = transactionTypeService.getModelById(1l);
        FinancialYear financialYear = financialYearService.findByIsActive(1);
        Party party =partyRepository.getOne(partyId);
        List<InvoiceHeader> invoiceHeaders = invoiceHeaderRepository.findByInvoiceTypeAndFinancialYearAndParty(transactionType, financialYear,party);

        List<InvoiceDTO> invoiceDTOList = new ArrayList<InvoiceDTO>();
        List<AutoTransactionGeneration> autoTransactionGenerations= autoTransactionRepository.getAutoGenerationDetails();
        invoiceHeaders.forEach(inv->{
        AutoTransactionGeneration autoTransactionGeneration = autoTransactionRepository.getAutoTransactionByInvoice(inv.getId());
               if(autoTransactionGeneration==null) {

                       invoiceDTOList.add(invoiceHeaderMapper.modelToDTOMap(inv));



               }


        });


        return invoiceDTOList;
    }

    @Override
    public TransactionResponseDTO deleteTransaction(Long id) {

        TransactionResponseDTO res = new TransactionResponseDTO();

        String deleteStatus = "Transaction Deleted Successfully";
        Integer deleteStatusNum = 1;

        AutoTransactionGeneration autoTransactionGeneration = autoTransactionRepository.findById(id);
        try {
            autoTransactionRepository.delete(autoTransactionGeneration);


        } catch (Exception e) {
            deleteStatus = "Cannot Delete Invoice";
            deleteStatusNum = 0;
            log.error("10 " + e);
            throw e;
        }
        res.setResponseString(deleteStatus);
        res.setResponseStatus(deleteStatusNum);
        return res;
    }


    @Override
    public AutoTransactionGenerationDTO stopTransaction(AutoTransactionGenerationDTO autoTransactionGenerationDTO) {
        AutoTransactionGeneration autoTransactionGeneration = autoTransactionMapper.dtoToModelMap(autoTransactionGenerationDTO);
        autoTransactionGeneration.setStopTranaction(1);
        AutoTransactionGeneration autoTransactionGenerationOut = autoTransactionRepository.save(autoTransactionGeneration);
        AutoTransactionGenerationDTO autoTransactionGenerationDTOOut = autoTransactionMapper.modelToDTOMap(autoTransactionGenerationOut);
        return autoTransactionGenerationDTOOut;


    }


    public Boolean getReport(String reportName, String id, String copyText, String amountInWords, String printHeaderText, EmailSetting emailSetting, String email, String userName, String name, String password, Integer allowShippingAddess, String userEmail, String invoiceNumber, String partyName, HttpServletRequest request) throws SQLException, JRException, IOException {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=" + "Invoice.pdf");

        User user = userRepository.getOne(1000l);

        JwtAuthenticationRequest jwtAuthenticationRequest = new JwtAuthenticationRequest();

        jwtAuthenticationRequest.setUsername("flipkart");
        jwtAuthenticationRequest.setPassword("flipkart123");


        String apiUrl = "http://localhost:8082/core-erp";
        WebClient webClient = WebClient.builder().baseUrl(apiUrl).build();
        Mono<JwtAuthenticationResponse> token = webClient.post()
                .uri("/auth")
                .body(BodyInserters.fromObject(jwtAuthenticationRequest))
                .retrieve()
                .bodyToMono(JwtAuthenticationResponse.class)
                .onErrorMap(e -> new Exception("Error While getting Token", e));

        String tokenString = token.block().getToken();


        log.info(tokenString + "tokenString");
        Integer allowShippingAddress = allowShippingAddess;
        String ship = "0";
        if (allowShippingAddress != null && allowShippingAddress != 0) {
            ship = "1";
        }


        String companySignature = "0";
        Company company = companyRepository.getOne(1l);
        if (company.getSignatureImagePath() != null) {

            companySignature = "1";

        }

        InputStreamResource response = null;
        log.info(reportName + "reportName" + copyText + "copyText" + amountInWords + "printHeaderText" + printHeaderText);
        String strurl = "/api/jasper-print/" + ship + "/" + companySignature + "?id=" + id + "&reportName=" + reportName + "&copyText=" + copyText + "&amountInWords=" + amountInWords + "&printHeaderText=" + printHeaderText;


        WebClient client = WebClient
                .builder()
                .baseUrl("http://localhost:8082/core-erp")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();

        response = client.get()
                .uri(strurl, request)
                .headers(headerss -> headerss.setBearerAuth(tokenString))
                .retrieve()
                .bodyToMono(InputStreamResource.class)
                .block();

        log.info("response.getInputStream() : " + response);
        ByteArrayDataSource attachment = new ByteArrayDataSource(IOUtils.toByteArray(response.getInputStream()), "application/pdf");
        Boolean sentEmail = sendEmail(emailSetting, email, userName, name, userEmail, company.getName(), invoiceNumber, partyName, attachment);

        return sentEmail;
    }

    ;


    public Boolean sendEmail(EmailSetting emailSetting, String email, String userName, String name, String userEmail, String companyName, String invoiceNumber, String partyName, ByteArrayDataSource byteArrayDataSource) {


        Boolean sentEmail = true;

        EmailObject emailObject = new EmailObject();


        emailObject.setName(name != null ? name : userName);
        emailObject.setOtp("0");
        // String templateName = environment.getProperty("email.Invoice.template.name");
        String subjecta = "Invoice from" + " " + companyName;

        String templateName = "Dear Sir/Madam,<br /><br />Greetings from " + " " + companyName + "<br /><br />We have sent an invoice bearing Invoice number : " + invoiceNumber + " for" + partyName + "<br /><br /> Kindly check the same and do the needful. If any clarification is required, kindly contact" + companyName + "<br /><br /> This is a automatically generated email, please do not reply to this mail." + "" + "<br /><br /> Powered by suktha solutions";

       log.info("----");
        if (emailSetting != null && emailSetting.getIsEmailEnabled() != 0 && userEmail != null) {


            emailService.sendEmailSingleFileAttach(emailSetting, emailObject, emailSetting.getEmailFrom(), templateName, userEmail, subjecta, byteArrayDataSource, "Invoice.pdf",0);
        } else {
            sentEmail = false;
        }

        return sentEmail;
    }


   @Override
    public Long checkInvoiceInAutoTranaction(String id){
       AutoTransactionGeneration autoTransactionGeneration= autoTransactionRepository.getAutoGeneration(id,1);
      Long transactionCount=0l;
       if(autoTransactionGeneration!=null && autoTransactionGeneration.getId()!=null){
           transactionCount =1l;

       }




        return  transactionCount;
    }


    @Override
    public AutoTransactionGenerationDTO getAutoTransction(String id){
      AutoTransactionGeneration autoTransactionGeneration=autoTransactionRepository.getAutoTransactionByInvoice(id);
      AutoTransactionGenerationDTO autoTransactionGenerationDTO = new AutoTransactionGenerationDTO();
      if(autoTransactionGeneration!=null) {
          log.info(autoTransactionGeneration.getInvoiceHeaderId() + "autoTransactionGeneration.getInvoiceHeaderId()");
           autoTransactionGenerationDTO = autoTransactionMapper.modelToDTOMap(autoTransactionGeneration);

      }
        return autoTransactionGenerationDTO;
    }


    @Override
    public TransactionResponseDTO getAutoTransctionLog(String id){
      Integer countOfInvoice=  transactionLogRepository.getCountOfInvoice(id);
      TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
      if(countOfInvoice>=1){
          transactionResponseDTO.setResponseStatus(1);


      }
      else{
          transactionResponseDTO.setResponseStatus(0);
      }


      return transactionResponseDTO;
    }


    public TransactionResponseDTO checkForValidAutoTransaction(AutoTransactionGeneration autoTransactionGeneration) {
            Integer end=1;
        InvoiceHeader invoiceHeader = invoiceHeaderRepository.findByHeaderId(autoTransactionGeneration.getInvoiceHeaderId());
        AtomicReference<Integer> checkCount = new AtomicReference<>(0);
        Long countOfInvoice = invoiceItemRepository.findCountByHeaderId(invoiceHeader.getId());

        TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
        transactionResponseDTO.setResponseStatus(0);
        List<AutoTransactionGeneration> listAutoTransaction = autoTransactionRepository.getByPartyType(autoTransactionGeneration.getPartyId(), autoTransactionGeneration.getPeriodToCreateTransaction(),end);
        if (listAutoTransaction != null && listAutoTransaction.size() > 0) {
            listAutoTransaction.forEach(auto -> {

                Integer checkOut = checkCount.get();

                if (checkOut.longValue() == countOfInvoice) {
                    transactionResponseDTO.setResponseStatus(1);
                } else {
                    checkCount.set(0);
                    Integer transactionCount = transactionLogRepository.getTransactionCount(auto.getInvoiceHeaderId());
                    if (transactionCount == null) {
                        transactionCount = 0;
                    }
                    if (transactionCount != auto.getCountOfInvoice() && transactionResponseDTO.getResponseStatus() == 0) {
                        InvoiceHeader invoiceHeaderCheck = invoiceHeaderRepository.findByHeaderId(auto.getInvoiceHeaderId());
                        Long countOfNewInvoice = invoiceItemRepository.findCountByHeaderId(invoiceHeaderCheck.getId());

                        invoiceHeaderCheck.getInvoiceItems().forEach(items -> {
                            String matrialName = items.getMaterial().getName();
                            Double quantity = items.getQuantity();


                            invoiceHeader.getInvoiceItems().forEach(newItems -> {


                                if (matrialName.equals(newItems.getMaterial().getName()) && quantity.longValue() == newItems.getQuantity().longValue()) {

                                    checkCount.updateAndGet(v -> v + 1);


                                }


                            });

                        });

                    }

                    Integer check = checkCount.get();

                    Boolean value = check.longValue() == countOfInvoice;

                    if (value == true) {

                        transactionResponseDTO.setResponseStatus(1);


                    }
                }
            });

        }

        return transactionResponseDTO;

    }

    @Override
    @Transactional
    public InvoiceDTO reuseInvoiec(InvoiceWrapperDTO invoiceWrapperIn) {
        InvoiceDTO invoiceDTO = new InvoiceDTO();

        TransactionResponseDTO deleteStatus = delete(invoiceWrapperIn.getInvoiceHeader().getReusedInvoiceId(),invoiceWrapperIn.getInvoiceHeader().getIsReused());
        stockTraceRepository.deleteByTransactionType(invoiceWrapperIn.getInvoiceHeader().getReusedInvoiceId());
        if (deleteStatus.getResponseStatus() == 1) {
            invoiceDTO = saveInvoice(invoiceWrapperIn);
            if (invoiceDTO.getId() != null) {
                InvoiceHeaderAudit invoiceHeaderAudit = invoiceHeaderAuditRepository.getOne(invoiceWrapperIn.getInvoiceHeader().getReusedInvoiceId());
                invoiceHeaderAudit.setIsReused(1);
                invoiceHeaderAudit.setReusedHeaderId(invoiceDTO.getId());
                auditEntryReuse(invoiceHeaderAudit);
            }

        }

        return invoiceDTO;
    }


    private void auditEntryReuse(InvoiceHeaderAudit inhDelete) {

        invoiceHeaderAuditRepository.save(inhDelete);
    }

    @Override
    @Transactional
    public void weeklyTransactionReport(EmailSetting emailSetting,String email,String userName,String name,String password){
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String date= dateFormat.format(new Date());
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        log.info(dateFormat.format(cal.getTime()));
        Date dateTo=cal.getTime();
        cal.add( Calendar.DAY_OF_MONTH, - 7);
        Date dateFrom=cal.getTime();
        log.info(dateFormat.format(cal.getTime()));
       Company company =companyRepository.getOne(1l);


        log.info(dateFrom+"------------------------------");
        log.info(dateTo+"--------------------------------");

            WeeklyTransactionReportModel weeklyTransactionReportModel = invoiceHeaderRepository.getWeeklyReport(dateFrom,dateTo,1L);


        WeeklyPurhaseOrderReport weeklyPurhaseOrderReport =purchaseOrderHeaderRepository.getPoWeeklyReport(dateFrom,dateTo,11L);
     Long completedPo =   purchaseOrderHeaderRepository.getCompletedPoWeeklyReport(dateFrom,dateTo,11L,66l);

       Long balancePo =   purchaseOrderHeaderRepository.getBalancedPoWeeklyReport(dateFrom,dateTo,11L,66l);



          String text = "<div>"+ "Dear"+""+company.getName()+"</div>";


         text =  text +
                 "<tr align='center'>"
                 + "<div>"+"Here are the summary of your weekly activities dated from"+dateFormat.format(dateFrom) +" "+"To" +dateFormat.format(dateTo) +"</div>"
                 + "</tr>";


        text =  text + "Sales";
        text = text + "<table width='100%' border='1' style=\"border-collapse: collapse\" align='center'  bordercolor=\"#6699FF\" >"

                + "<tr align='center'>"
                + "<td>" +"Count Of Invoice" + "</td>"
                + "<td>" +"Invoice Amount" + "</td>"
                + "<td>" +" Paid Amount" + "</td>"
                + "<td>" +" Balance Amount" + "</td>"
                + "</tr>";

       text = text + "<tr align='center'>"
                + "<td>" + weeklyTransactionReportModel.getInvoiceCount() + "</td>"
                + "<td>" + weeklyTransactionReportModel.getInvoiceAmount() + "</td>"
                + "<td>" +weeklyTransactionReportModel.getPaidAmount() + "</td>"
                + "<td>" + weeklyTransactionReportModel.getBalanecAmount() + "</td>"

                + "</tr>"
              +  "</table>";


        text = text + "<tr align='center'>"

                + "</tr>";

        text =  text + "Purchase";

        text = text + "<table width='100%' border='1' style=\"border-collapse: collapse\" align='center'  bordercolor=\"#6699FF\" >"

                + "<tr align='center'>"
                + "<td>" +"Count Of Invoice" + "</td>"
                + "<td>" +"Invoice Amount" + "</td>"
                + "<td>" +" Recived Amount" + "</td>"
                + "<td>" +" Balance Amount" + "</td>"
                + "</tr>";

        weeklyTransactionReportModel = invoiceHeaderRepository.getWeeklyReport(dateFrom,dateTo,5L);

        text = text + "<tr align='center'>"
                + "<td>" + weeklyTransactionReportModel.getInvoiceCount() + "</td>"
                + "<td>" +weeklyTransactionReportModel.getInvoiceAmount() + "</td>"
                + "<td>" +weeklyTransactionReportModel.getPaidAmount() + "</td>"
                + "<td>" + weeklyTransactionReportModel.getBalanecAmount() + "</td>"

                + "</tr>"
        +  "</table>";



        text = text + "<tr align='center'>"

                + "</tr>";

        text =  text + "Customer Purchase Orders";

        text = text + "<table width='100%' border='1' style=\"border-collapse: collapse\" align='center'  bordercolor=\"#6699FF\" >"

                + "<tr align='center'>"
                + "<td>" +"New Po Received" + "</td>"
                + "<td>" +"Total Value Of Po" + "</td>"
                + "<td>" +"Completed Po" + "</td>"
                + "<td>" +"In Process Po" + "</td>"
                + "</tr>";

        text = text + "<tr align='center'>"
                + "<td>" + weeklyPurhaseOrderReport.getPoCount() + "</td>"
                + "<td>" +weeklyPurhaseOrderReport.getPoAmount() + "</td>"
                + "<td>" +completedPo + "</td>"
                + "<td>" + balancePo + "</td>"

                + "</tr>"
        +  "</table>";

        text = text + "<tr align='center'>"

                + "</tr>";

        text =  text + "Supllier Purchase Orders";

        text = text + "<table width='100%' border='1' style=\"border-collapse: collapse\" align='center'  bordercolor=\"#6699FF\" >"

                + "<tr align='center'>"
                + "<td>" +"New Po Raised" + "</td>"
                + "<td>" +"Total Value Of Po Raised" + "</td>"
                + "<td>" +"Completed Po" + "</td>"
                + "<td>" +"In Process Po" + "</td>"
                + "</tr>";


         weeklyPurhaseOrderReport =purchaseOrderHeaderRepository.getPoWeeklyReport(dateFrom,dateTo,2L);
         completedPo =   purchaseOrderHeaderRepository.getCompletedPoWeeklyReport(dateFrom,dateTo,2L,10l);

         balancePo =   purchaseOrderHeaderRepository.getBalancedPoWeeklyReport(dateFrom,dateTo,2L,10l);


        text = text + "<tr align='center'>"
                + "<td>" + weeklyPurhaseOrderReport.getPoCount()+ "</td>"
                + "<td>" +weeklyPurhaseOrderReport.getPoAmount() + "</td>"
                + "<td>" +completedPo+ "</td>"
                + "<td>" + balancePo + "</td>"

                + "</tr>"
                + "</table>";

        String subject ="Weekly Transaction Report";





        EmailObject emailObject = new EmailObject();


        emailObject.setName(name != null ? name : userName);
        emailObject.setOtp("0");

        if (emailSetting != null && emailSetting.getIsEmailEnabled() != 0) {
            emailService.sendEmailSingleFileAttach(emailSetting, emailObject, emailSetting.getEmailFrom(), text, email, subject, null, "Invoice.pdf",1);
        }
    }









    @Override
    public PartyDueAmountDTO dueAmount(String partyName) {
        List<PartyDTO> partyDTOList = partyService.getPartyDTOByName(partyName);
        PartyDueAmountDTO partyDueAmountDTO = new PartyDueAmountDTO();
        if (partyDTOList.size() > 0) {
            if (partyDTOList.size() > 1) {
                partyDTOList.forEach(party -> {
                    partyDueAmountDTO.setPartyName(partyDueAmountDTO.getPartyName() + "," + party.getName());
                });

                partyDueAmountDTO.setPartyCount("There Are Multiple Customer With Same Name Which Customer You like To Select");

            } else {
                partyDTOList.forEach(party -> {

                    Double transactionDueAmount = invoiceHeaderRepository.getDueAmountOfParty(partyName, 1l);
                    partyDueAmountDTO.setPartyName(party.getName());
                    partyDueAmountDTO.setPartyId(party.getId());
                    partyDueAmountDTO.setPartyDueAmount(transactionDueAmount);
                });


            }

        }
        partyDueAmountDTO.setPartyCount("There is no customer with this name");
        return partyDueAmountDTO;
    }
    @Override
    public String sendDueAmountEmail(String partyName) {
        String sentEmail = "Email Cant Sent";


        Double dueAmount = invoiceHeaderRepository.getDueAmountOfParty(partyName, 1l);
        EmailObject emailObject = new EmailObject();
        EmailSetting emailSetting = emailSettingRepository.findAllUser();
        Company company = companyRepository.getCompany();
       String companyName =company.getName();
        if (company != null) {
            Party party = partyRepository.getPartyByName(partyName);
            emailObject.setName(party.getName());
            emailObject.setOtp("0");
            // String templateName = environment.getProperty("email.Invoice.template.name");
            String subjecta = "Remainder  from  "+companyName;
            String templateName = "Dear Sir/Madam,<br /><br />Greetings from " +companyName+ "<br /><br /> We have Sent Due Amount that you need to pay  Please check and contat us";
            templateName = templateName + "<tr>  </tr>";
            templateName = templateName + dueAmount + "Rupess need to pay";
            if (emailSetting != null && emailSetting.getIsEmailEnabled() != 0 && party.getEmail() != null) {
                emailService.sendEmailSingleFileAttach(emailSetting, emailObject, emailSetting.getEmailFrom(), templateName, party.getEmail(), subjecta, null, "Invoice.pdf",1);
                sentEmail = "Email Sent to Customer "+party.getName()+"Succesfully";
            } else {
                sentEmail = "Email Cant Sent";
            }
        }
            return sentEmail;


    }
    @Transactional
   DeliveryChallanDTO createInDC(InvoiceHeader invoiceHeader){
        DeliveryChallanDTO deliveryChallanDTOOut= new DeliveryChallanDTO();

      DeliveryChallanHeader deliveryChallanHeader= deliveryChallanService.findByNoteId(invoiceHeader.getId());


       deliveryChallanDTOOut.setAmount(0.0);
       if(invoiceHeader.getInvoiceItems()!=null)
       {

           Double totalQuantity = invoiceHeader.getInvoiceItems()
                   .stream()
                   .mapToDouble(item -> item.getQuantity())
                   .sum();

           deliveryChallanDTOOut.setBalanceQuantity(totalQuantity);
           deliveryChallanDTOOut.setTotalQuantity(totalQuantity);

       }
       if(deliveryChallanHeader!=null){
           deliveryChallanDTOOut.setId(deliveryChallanHeader.getId());
           deliveryChallanDTOOut.setCreatedBy(deliveryChallanHeader.getCreatedBy());
           deliveryChallanDTOOut.setCreatedDate(deliveryChallanHeader.getCreatedDateTime());
           deliveryChallanDTOOut.setDeliveryChallanId(deliveryChallanHeader.getDeliveryChallanId());
          deliveryChallanDTOOut.setDeliveryChallanNumber(deliveryChallanHeader.getDeliveryChallanNumber());
          deliveryChallanDTOOut.setDeliveryChallanDate(deliveryChallanHeader.getDeliveryChallanDate());

       }
       else {

           deliveryChallanDTOOut.setId(null);
           deliveryChallanDTOOut.setCreatedBy(invoiceHeader.getCreatedBy());
           deliveryChallanDTOOut.setCreatedDate(invoiceHeader.getCreatedDateTime());
           deliveryChallanDTOOut.setDeliveryChallanId(null);
           deliveryChallanDTOOut.setDeliveryChallanNumber(null);
           deliveryChallanDTOOut.setDeliveryChallanDate(new Date());
       }


       deliveryChallanDTOOut.setCgstTaxAmount(null);
       deliveryChallanDTOOut.setCgstTaxRate(null);
       deliveryChallanDTOOut.setCompanyId(invoiceHeader.getCompany().getId());
       deliveryChallanDTOOut.setDeliveryAddress(invoiceHeader.getPartyAddress());
       //deliveryChallanDTOOut.setDeliveryChallanDate(new Date());
       deliveryChallanDTOOut.setDeliveryChallanItems(deliveryItemMapper(invoiceHeader));
       deliveryChallanDTOOut.seteWayBill(null);
       if(invoiceHeader.getInvoiceType().getId()==ApplicationConstants.SUBCONTRACT_DEBIT_NOTE_ID){
           deliveryChallanDTOOut.setDeliveryChallanTypeId(16l);

       }
       else {
           deliveryChallanDTOOut.setDeliveryChallanTypeId(13l);
       }
       deliveryChallanDTOOut.setDeliveryNote(null);
       deliveryChallanDTOOut.setDeliveryTerms(null);
       deliveryChallanDTOOut.setDispatchDate(null);
       deliveryChallanDTOOut.setDispatchTime(null);
       deliveryChallanDTOOut.setFinancialYearId(invoiceHeader.getFinancialYear().getId());
       deliveryChallanDTOOut.setForSale(null);

       deliveryChallanDTOOut.setIgstTaxAmount(null);
       deliveryChallanDTOOut.setIgstTaxRate(null);
       deliveryChallanDTOOut.setInDeliveryChallanDate(null);
       deliveryChallanDTOOut.setInDeliveryChallanNumber(null);
       deliveryChallanDTOOut.setInspectedBy(invoiceHeader.getCreatedBy());
       deliveryChallanDTOOut.setInspectionDate(new Date());
       deliveryChallanDTOOut.setInternalReferenceDate(null);
       deliveryChallanDTOOut.setInternalReferenceNumber(null);
       deliveryChallanDTOOut.setLabourChargesOnly(null);
       deliveryChallanDTOOut.setModeOfDispatch(null);
       deliveryChallanDTOOut.setModeOfDispatchStatus(null);
       deliveryChallanDTOOut.setNonReturnableDeliveryChallan(null);
       deliveryChallanDTOOut.setNotForSale(null);
       deliveryChallanDTOOut.setNumberOfPackages(null);
       deliveryChallanDTOOut.setNumberOfPackagesStatus(null);
       deliveryChallanDTOOut.setOfficeAddress(invoiceHeader.getPartyAddress());
       deliveryChallanDTOOut.setPartyId(invoiceHeader.getParty().getId());
       deliveryChallanDTOOut.setPaymentTerms(null);
       deliveryChallanDTOOut.setPersonName(null);
       deliveryChallanDTOOut.setPurchaseOrderDate(null);
       deliveryChallanDTOOut.setPurchaseOrderNumber(null);
       deliveryChallanDTOOut.setPrice(null);
       deliveryChallanDTOOut.setReturnableDeliveryChallan(null);
       deliveryChallanDTOOut.setReturnForJobWork(null);
       deliveryChallanDTOOut.setSgstTaxAmount(null);
       deliveryChallanDTOOut.setSgstTaxRate(null);
       deliveryChallanDTOOut.setStatusId(85l);
       deliveryChallanDTOOut.setTaxAmount(null);
       deliveryChallanDTOOut.setTaxId(null);
       deliveryChallanDTOOut.setTermsAndConditions(null);
       deliveryChallanDTOOut.setAddress(invoiceHeader.getParty().getAddress());
       deliveryChallanDTOOut.setVehicleNumber(null);
       deliveryChallanDTOOut.setVehicleNumberStatus(null);
       deliveryChallanDTOOut.setDiscountAmount(null);
       deliveryChallanDTOOut.setDiscountPercentage(null);
       deliveryChallanDTOOut.setAmountAfterDiscount(null);
       deliveryChallanDTOOut.setStatusName("New");
       deliveryChallanDTOOut.setPartyName(invoiceHeader.getParty().getName());
       deliveryChallanDTOOut.setGstNumber(invoiceHeader.getGstNumber());
       deliveryChallanDTOOut.setRemarks(null);
       deliveryChallanDTOOut.setInclusiveTax(null);
       deliveryChallanDTOOut.setBillToAddress((invoiceHeader.getBillToAddress()));
       deliveryChallanDTOOut.setShipToAddress(invoiceHeader.getShipToAddress());

       deliveryChallanDTOOut.setCompanyName(invoiceHeader.getCompanyName());
       deliveryChallanDTOOut.setCompanygGstRegistrationTypeId(invoiceHeader.getCompanygGstRegistrationTypeId());
       deliveryChallanDTOOut.setCompanyPinCode(invoiceHeader.getCompanyPinCode());
       deliveryChallanDTOOut.setCompanyStateId(invoiceHeader.getCompanyStateId());
       deliveryChallanDTOOut.setCompanyStateName(invoiceHeader.getCompanyStateName());
       deliveryChallanDTOOut.setCompanyCountryId(invoiceHeader.getCompanyCountryId());
       deliveryChallanDTOOut.setCompanyPrimaryMobile(invoiceHeader.getCompanyPrimaryMobile());
       deliveryChallanDTOOut.setCompanySecondaryMobile(invoiceHeader.getCompanySecondaryMobile());
       deliveryChallanDTOOut.setCompanyContactPersonNumber(invoiceHeader.getCompanyContactPersonNumber());
       deliveryChallanDTOOut.setCompanyContactPersonName(invoiceHeader.getCompanyContactPersonName());
       deliveryChallanDTOOut.setCompanyPrimaryTelephone(invoiceHeader.getCompanyPrimaryTelephone());
       deliveryChallanDTOOut.setCompanySecondaryTelephone(invoiceHeader.getCompanySecondaryTelephone());
       deliveryChallanDTOOut.setCompanyWebsite(invoiceHeader.getCompanyWebsite());
       deliveryChallanDTOOut.setCompanyEmail(invoiceHeader.getCompanyEmail());
       deliveryChallanDTOOut.setCompanyFaxNumber(invoiceHeader.getCompanyFaxNumber());
       deliveryChallanDTOOut.setCompanyAddress(invoiceHeader.getCompanyAddress());
       deliveryChallanDTOOut.setCompanyTagLine(invoiceHeader.getCompanyTagLine());
       deliveryChallanDTOOut.setCompanyGstNumber(invoiceHeader.getCompanyGstNumber());
       deliveryChallanDTOOut.setCompanyPanNumber(invoiceHeader.getCompanyPanNumber());
       deliveryChallanDTOOut.setCompanyPanDate(invoiceHeader.getCompanyPanDate());
       deliveryChallanDTOOut.setCompanyCeritificateImagePath(invoiceHeader.getCompanyCeritificateImagePath());
       deliveryChallanDTOOut.setCompanyLogoPath(invoiceHeader.getCompanyLogoPath());

       deliveryChallanDTOOut.setPartyName(invoiceHeader.getPartyName());
       deliveryChallanDTOOut.setPartyContactPersonNumber(invoiceHeader.getPartyContactPersonNumber());
       deliveryChallanDTOOut.setPartyPinCode(invoiceHeader.getPartyPinCode());
       deliveryChallanDTOOut.setPartyAreaId(invoiceHeader.getPartyAreaId());
       deliveryChallanDTOOut.setPartyCityId(invoiceHeader.getPartyCityId());
       deliveryChallanDTOOut.setPartyStateId(invoiceHeader.getPartyStateId());
       deliveryChallanDTOOut.setPartyCountryId(invoiceHeader.getPartyCountryId());
       deliveryChallanDTOOut.setPartyPrimaryTelephone(invoiceHeader.getPartyPrimaryTelephone());
       deliveryChallanDTOOut.setPartySecondaryTelephone(invoiceHeader.getPartySecondaryTelephone());
       deliveryChallanDTOOut.setPartyPrimaryMobile(invoiceHeader.getPartyPrimaryMobile());
       deliveryChallanDTOOut.setPartySecondaryMobile(invoiceHeader.getPartySecondaryMobile());
       deliveryChallanDTOOut.setPartyEmail(invoiceHeader.getPartyEmail());
       deliveryChallanDTOOut.setPartyWebsite(invoiceHeader.getPartyWebsite());
       deliveryChallanDTOOut.setPartyContactPersonName(invoiceHeader.getPartyContactPersonName());
//		deliveryChallanDTOOut.setPartyBillToAddress(model.getPartyBillToAddress());
//		deliveryChallanDTOOut.setPartyShipAddress(model.getPartyBillToAddress());
       deliveryChallanDTOOut.setPartyDueDaysLimit(invoiceHeader.getPartyDueDaysLimit());
       deliveryChallanDTOOut.setPartyGstRegistrationTypeId(invoiceHeader.getPartyGstRegistrationTypeId());
//		deliveryChallanDTOOut.setPartyGstNumber(model.getPartyGstNumber());
       deliveryChallanDTOOut.setPartyPanNumber(invoiceHeader.getPartyPanNumber());
       deliveryChallanDTOOut.setIsIgst(invoiceHeader.getIsIgst());
       deliveryChallanDTOOut.setPartyCode(invoiceHeader.getParty().getPartyCode());
       deliveryChallanDTOOut.setUpdateBy(invoiceHeader.getUpdatedBy());
       deliveryChallanDTOOut.setUpdatedDate(invoiceHeader.getUpdatedDateTime());
        deliveryChallanDTOOut.setJwNoteId(invoiceHeader.getId());
















           return deliveryChallanDTOOut;
    }


    @Transactional
   List<DeliveryChallanItemDTO> deliveryItemMapper(InvoiceHeader invoiceHeader){
        List<DeliveryChallanItemDTO> deliveryChallanItemDTOS=new ArrayList<>();
//        DeliveryChallanHeader deliveryChallanHeader =deliveryChallanService.findByNoteId(invoiceHeader.getId());
//        if(deliveryChallanHeader!=null){
//           // List<DeliveryChallanItem> deliveryChallanItems =deliveryChallanItemRepository.findByDeliveryChallanHeader(deliveryChallanHeader);
//
//
//        }


        List<InvoiceItem> invoiceItems = invoiceItemRepository.findByHeaderId(invoiceHeader.getId());



        if(invoiceItems!=null){
            invoiceItems.forEach(invoiceItem -> {
              DeliveryChallanItem deliveryChallanItem =deliveryChallanItemRepository.findByJwNoteItemId(invoiceItem.getId());

                double materialPrice=0;
                DeliveryChallanItemDTO deliveryChallanItemDTO =new DeliveryChallanItemDTO();

                if(deliveryChallanItem!=null){
                    deliveryChallanItemDTO.setId(deliveryChallanItem.getId());
                    deliveryChallanItemDTO.setHeaderId(deliveryChallanItem.getDeliveryChallanHeader().getId());

                }
                else{
                    deliveryChallanItemDTO.setId(null);
                    deliveryChallanItemDTO.setHeaderId(null);
                }


                deliveryChallanItemDTO.setAmount(0.0);
                deliveryChallanItemDTO.setBatchCode(invoiceItem.getBatchCode());
                deliveryChallanItemDTO.setBatchCodeId(invoiceItem.getBatchCode());
                deliveryChallanItemDTO.setCgstTaxAmount(invoiceItem.getCgstTaxAmount());
                deliveryChallanItemDTO.setCgstTaxPercentage(invoiceItem.getCgstTaxPercentage());
                deliveryChallanItemDTO.setDeliveryChallanItemStatus(null);
                deliveryChallanItemDTO.setDeliveryChallanType(null);

                deliveryChallanItemDTO.setIgstTaxAmount(null);
                deliveryChallanItemDTO.setIgstTaxPercentage(null);
                deliveryChallanItemDTO.setInDeliveryChallanNumber(null);
                deliveryChallanItemDTO.setInDeliveryChallanDate(null);
                deliveryChallanItemDTO.setMaterialId(invoiceItem.getMaterial().getId());
                deliveryChallanItemDTO.setMaterialName(materialService.getMaterialById(invoiceItem.getMaterial().getId()).getName());



                deliveryChallanItemDTO.setPrice(invoiceItem.getPrice());

                deliveryChallanItemDTO.setProcessId(null);
                deliveryChallanItemDTO.setProcessName(null);
                deliveryChallanItemDTO.setPurchaseOrderHeaderId(null);
                deliveryChallanItemDTO.setPurchaseOrderNumber(null);
                deliveryChallanItemDTO.setPurchaseOrderItemId(null);
                deliveryChallanItemDTO.setQuantity(invoiceItem.getQuantity());
                deliveryChallanItemDTO.setRemarks(null);

                deliveryChallanItemDTO.setSgstTaxAmount(invoiceItem.getSgstTaxAmount());
                deliveryChallanItemDTO.setSgstTaxPercentage(invoiceItem.getSgstTaxPercentage());
                deliveryChallanItemDTO.setSlNo(invoiceItem.getSlNo());
                deliveryChallanItemDTO.setStatus(null);
                deliveryChallanItemDTO.setTaxAmount(null);
                deliveryChallanItemDTO.setTaxId(null);
                deliveryChallanItemDTO.setTaxRate(null);
//		deliveryChallanItemDTO.setUnitOfMeasurementId(model.getUnitOfMeasurement().getId());
                deliveryChallanItemDTO.setUnitOfMeasurementId(invoiceItem.getUnitOfMeasurement() != null ? invoiceItem.getUnitOfMeasurement().getId() : null);
                deliveryChallanItemDTO.setPartNumber(invoiceItem.getMaterial().getPartNumber());
                deliveryChallanItemDTO.setPartName(invoiceItem.getMaterial().getName());
                deliveryChallanItemDTO.setHsnOrSac(invoiceItem.getMaterial().getHsnCode());
                deliveryChallanItemDTO.setUom(invoiceItem.getUnitOfMeasurement() != null ? invoiceItem.getUnitOfMeasurement().getName() : null);
                deliveryChallanItemDTO.setInclusiveTax(null);
                deliveryChallanItemDTO.setDiscountAmount(null);
                deliveryChallanItemDTO.setDiscountPercentage(null);
                deliveryChallanItemDTO.setAmountAfterDiscount(null);
                deliveryChallanItemDTO.setInvoiceBalanceQuantity(null);
                deliveryChallanItemDTO.setGrnBalanceQuantity(null);
                deliveryChallanItemDTO.setSourceDeliveryChallanHeaderId(null);
                deliveryChallanItemDTO.setSourceDeliveryChallanItemId(null);
                deliveryChallanItemDTO.setDcBalanceQuantity(invoiceItem.getQuantity());
                deliveryChallanItemDTO.setIncomingQuantity(invoiceItem.getQuantity());
                deliveryChallanItemDTO.setSpecification(null);
                deliveryChallanItemDTO.setSourceDeliveryChallanNumber(null);
                deliveryChallanItemDTO.setSourceDeliveryChallanDate(null);
                deliveryChallanItemDTO.setIsContainer(null);
                deliveryChallanItemDTO.setOutName(invoiceItem.getMaterial().getOutName());
                deliveryChallanItemDTO.setOutPartNumber(invoiceItem.getMaterial().getOutPartNumber());
                deliveryChallanItemDTO.setJwNoteItemId(invoiceItem.getId());
                deliveryChallanItemDTOS.add(deliveryChallanItemDTO);
            });







        }

       return deliveryChallanItemDTOS;

    }


}
