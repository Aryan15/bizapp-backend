package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.kie.api.event.rule.AgendaEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.TrackingAgendaEventListener;
import com.coreerp.dao.DeliveryChallanHeaderRepository;
import com.coreerp.dao.DeliveryChallanItemRepository;
import com.coreerp.dao.GRNHeaderRepository;
import com.coreerp.dao.InvoiceHeaderRepository;
import com.coreerp.dao.PurchaseOrderHeaderRepository;
import com.coreerp.dao.PurchaseOrderItemRepository;
import com.coreerp.dao.TransactionStatusChangeRepository;
import com.coreerp.dto.TransactionItemQuantityRule;
import com.coreerp.dto.TransactionStatusRule;
import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.DeliveryChallanItem;
import com.coreerp.model.GRNHeader;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.InvoiceItem;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.model.PurchaseOrderItem;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionStatusChange;
import com.coreerp.service.BackTrackingService;
import com.coreerp.service.StatusService;

@Component
public class InvoiceBackTrackingServiceNew implements BackTrackingService<InvoiceHeader> {

	final static Logger log = LogManager.getLogger(InvoiceBackTrackingServiceNew.class);
	
	String prePOId = "";
	String preDCId = "";
	String preProformaInvoiceId = "";
	String preSourceInvoiceId = "";
	Double dcBalanceQuantity = 0.0;
	Double invBalanceQuantity = 0.0;
	Double invQuantity = 0.0;
	Double poQuantity = 0.0;
	Double dcQuantity = 0.0;
	Double noteQuantity = 0.0;
	Double sourceQuantity = 0.0;
	Double prInvBalanceQuantity = 0.0;
	Double prInvQuantity = 0.0;
	String preGRNId ="";
	Double GRNQuantity = 0.0;
	Double sourcePrevQuantity = 0.0;
	Double preQuantity = 0.0;
//	List<String> deletedItemIds = new ArrayList<String>();
	
	@Autowired
    private KieContainer kieContainer;
    
	
    private KieSession kieSession ;
    
    @Autowired
   	StatusService statusService;
       
   	@Autowired
   	TransactionStatusChangeRepository transactionStatusChangeRepository;
    
    
    @Autowired
    private PurchaseOrderHeaderRepository purchaseOrderRepository;
    
    
    @Autowired
    private DeliveryChallanHeaderRepository deliveryChallanHeaderRepository;
    
    @Autowired
    private InvoiceHeaderRepository invoiceHeaderRepository;
	
    @Autowired
    private PurchaseOrderItemRepository purchaseOrderItemRepository ;
    
   	@Autowired
   	DeliveryChallanItemRepository deliveryChallanItemRepository;
   	
   	@Autowired
   	GRNHeaderRepository grnHeaderRepository;
    
	@Override
	public InvoiceHeader updateTransactionStatus(InvoiceHeader in,
			InvoiceHeader pre) {
		
		log.info("in updateTransactionStatus ");
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession = kieContainer.newKieSession("rulesSession");
		
		kieSession.addEventListener(agendaEventListener);
		
		//Back Track PO
		poBackTrack(in, pre);
		
		
		//Back Track DC
		dcBackTrack(in, pre);

		//Back Track Proforma Invoice
		proformaBackTrack(in, pre);
		
		//Back Track Invoice for Debit/Credit Note
		
		invoiceBackTrack(in, pre);

		//Back track of grn
		GRNBackTrack(in,pre);
		
		kieSession.dispose();
		
		return null;
	}
	
	private void poBackTrack(InvoiceHeader in, InvoiceHeader pre){

		log.info("poBackTrack");
		//Loop through POs attached to the Invoice
		List<PurchaseOrderHeader> invPOHeaders = new ArrayList<PurchaseOrderHeader>();
		
		in.getInvoiceItems().forEach(ini -> {
			PurchaseOrderHeader itemPoHeader = ini.getPurchaseOrderItem() != null ? ini.getPurchaseOrderItem().getPurchaseOrderHeader() : null;
			if(itemPoHeader != null && prePOId != itemPoHeader.getId()){
				invPOHeaders.add(itemPoHeader);
				prePOId = itemPoHeader.getId();
			}
		});
		
		log.info("invPOHeaders.size(): "+invPOHeaders.size());
		
		if(invPOHeaders.size() > 0 ){
			
			invPOHeaders.forEach(poHeader -> {


				//For open ended PO, change status to Open (from New)
				if(poHeader.getIsOpenEnded() != null && poHeader.getIsOpenEnded() == 1)
				{
					if(poHeader.getStatus() != null && !poHeader.getStatus().getName().equals(ApplicationConstants.PURCHASE_ORDER_STATUS_OPEN)) {
						Status openStatus = statusService.getStatusByTransactionTypeAndName(poHeader.getPurchaseOrderType(), ApplicationConstants.PURCHASE_ORDER_STATUS_OPEN);
						poHeader.setStatus(openStatus);
						purchaseOrderRepository.save(poHeader);
					}
					statusChangeEntryPO(poHeader, in);
					return;
				}

				TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
				//Build TransactionStatusRule
				invBalanceQuantity = 0.0;
				//dcBalanceQuantity = 0.0;
				poQuantity = 0.0;
				
				poHeader.getPurchaseOrderItems().forEach(poi -> {
					invBalanceQuantity += poi.getInvoiceBalanceQuantity();
					poQuantity += poi.getQuantity();
					
				});
				
				sourcePrevQuantity = 0.0;
				
				if (pre != null ){
					pre.getInvoiceItems()
							.stream()
							.filter(ini -> ini.getPurchaseOrderItem() != null)
							.forEach(ini -> {
						sourcePrevQuantity += ini.getQuantity();
					});
					
					
				}
				

				String latestStatus = null;
				if(in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_DELETED) || in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED) ){
//					latestStatus = transactionStatusChangeRepository.findByLatestTransactionId(poHeader.getId()).getStatus().getName();
//					log.info("latestStatus: "+latestStatus);
					
					TransactionStatusChange transactionStatusChange = transactionStatusChangeRepository.findByLatestTransactionId(poHeader.getId());
					log.info("transactionStatusChange: "+transactionStatusChange);
					if(transactionStatusChange!=null) {
                        if (transactionStatusChange.getStatus().getName().equalsIgnoreCase(ApplicationConstants.PURCHASE_ORDER_STATUS_INVOICED)) {

                            latestStatus = ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_INVOICED;
                        } else {
                            latestStatus = transactionStatusChange.getStatus().getName();
                        }

                        log.info("latestStatus: " + latestStatus);
                    }
				}

				transactionStatusRule.setInvoiceBalanceQuantity(invBalanceQuantity);
				transactionStatusRule.setCurrentStatus(poHeader.getStatus().getName());
				transactionStatusRule.setTransactionId(poHeader.getId());
				transactionStatusRule.setLatestStatus(latestStatus);
				transactionStatusRule.setQuantity(poQuantity);

				invQuantity = 0.0;
				
				in.getInvoiceItems()
						.stream()
						.filter(ini -> ini.getPurchaseOrderItem() != null)
						.forEach(ini -> {
					invQuantity += ini.getQuantity();
				});
				
				transactionStatusRule.setSourceQuantity(invQuantity);
				transactionStatusRule.setSourcePrevQuantity(sourcePrevQuantity);
				transactionStatusRule.setSourceTransactionId(in.getId());
				transactionStatusRule.setSourceTransactionStatus(in.getStatus().getName());
				transactionStatusRule.setSourceTransactionType(in.getInvoiceType().getName());
				transactionStatusRule.setCurrentStatus(poHeader.getStatus().getName());
				transactionStatusRule.setTransactionId(poHeader.getId());
				transactionStatusRule.setTransactionType(poHeader.getPurchaseOrderType().getName());
				
				log.info("transactionStatusRule: "+transactionStatusRule);

				
				//Get status from Drools
				
				kieSession.insert(transactionStatusRule);

		        int ruleCount = kieSession.fireAllRules();
		        
		        
		        
		        log.info("rules count: "+ruleCount +transactionStatusRule);

				
				//Update PO with derived status
		        if(ruleCount > 0 && transactionStatusRule.getNewStatus() != null)
		        {
		        	Status status  = statusService.getStatusByTransactionTypeAndName(poHeader.getPurchaseOrderType(), transactionStatusRule.getNewStatus());
		        	log.info("status"+status);
			        poHeader.setStatus(status);
			        
			      //Meke po Item updates
			        if(transactionStatusRule.getItemUpdateRequired()){
			        	poHeader.getPurchaseOrderItems()
			        	.stream()
			        	//.filter(poi -> in.getInvoiceItems().stream().anyMatch(ini -> ini.getPurchaseOrderItem().getId().equals(poi.getId())))
			        	.filter(
			        			poi -> {
					        		Long preItemExistCount = 0l;
					        		if(pre != null){
						        		preItemExistCount= pre.getInvoiceItems()
										            .stream()
										            .filter(ini -> ini.getPurchaseOrderItem() != null && ini.getPurchaseOrderItem().getId().equals(poi.getId())).count();
					        		}
					        		if(preItemExistCount >0)
					        			return true;
					        		else
					        		//	return false;
					        			return in.getInvoiceItems().stream().anyMatch(ii -> ii.getPurchaseOrderItem() != null ? (ii.getPurchaseOrderItem().getId().equals(poi.getId())) : false );
					        	})
			        	.forEach(poItem -> {
			        		
			        		Double preQuantity = null;
			        		
			        		if (pre != null  && pre.getInvoiceItems() != null && pre.getInvoiceItems().size() > 0)
			        		{
			        			preQuantity = pre.getInvoiceItems()
			        				.stream()
			        				.filter(ii -> ii.getPurchaseOrderItem().getId() == poItem.getId())
			        				.mapToDouble(di -> di.getQuantity())
			        				.findFirst()
			        				.getAsDouble();
			        		}
			        		
			        		
			        		log.info("1");
			        		TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
				            
				            transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
				            transactionItemQuantityRule.setItemId(poItem.getId());
				            transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
				            transactionItemQuantityRule.setPrevDCBalanceQuantity(poItem.getDcBalanceQuantity());
				            log.info("2");
				            transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
				            //transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(poItem.getInvoiceBalanceQuantity());
				            //transactionItemQuantityRule.setPrevQuantity(poItem.getQuantity());
				            //transactionItemQuantityRule.setPrevStatus(transactionStatusRule.getCurrentStatus());
				            transactionItemQuantityRule.setQuantity(preQuantity != null ? preQuantity : poItem.getQuantity());
				            
				            sourceQuantity = 0.0;

				            InvoiceItem iItem = in.getInvoiceItems()
							.stream()
									.filter(i-> i.getPurchaseOrderItem() !=null ? (i.getPurchaseOrderItem().getId().equals(poItem.getId())):false)
							.findFirst()
							.orElse(null);
				            
				            log.info("3 "+iItem);
				            
				            if(iItem != null ){
				            	//sourceQuantity = iItem.getQuantity();
								sourceQuantity = preQuantity != null ? (iItem.getQuantity() - preQuantity) : iItem.getQuantity();
				            }else{
								sourceQuantity = preQuantity != null ? -preQuantity : 0.0;
							}
				            
				            //If Item is getting deleted in Invoice, respective PO Invoice balance quantity is faked to po item quantity
				            if(sourceQuantity == 0 && Double.compare(iItem != null ? iItem.getQuantity() : 0.0, preQuantity) != 0){
				            	transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(poItem.getQuantity());
								transactionItemQuantityRule.setSourceQuantity(0.0);
				            }else {
				            	transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(poItem.getInvoiceBalanceQuantity());
								transactionItemQuantityRule.setSourceQuantity(sourceQuantity);
				            }
				            
				            log.info("4");
				           // transactionItemQuantityRule.setSourceQuantity(sourceQuantity);
				            //transactionItemQuantityRule.setSourceTransactionType(transactionStatusRule.getSourceTransactionType());
				            //transactionItemQuantityRule.setStatus(transactionStatusRule.getNewStatus());
				            //transactionItemQuantityRule.setTransactionType(transactionStatusRule.getTransactionType());
				            
				            log.info("item rule before: "+transactionItemQuantityRule);
				            
				            kieSession.insert(transactionItemQuantityRule);

				            int ruleCounti = kieSession.fireAllRules();
				            
				            log.info("rules count: "+ruleCounti);
				            log.info("item rule after: "+transactionItemQuantityRule);
				            
				            if(ruleCounti > 0 && transactionItemQuantityRule.getInvoiceBalanceQuantity() != null)
				            	poItem.setInvoiceBalanceQuantity(transactionItemQuantityRule.getInvoiceBalanceQuantity());
				            
				            
			        	});
			        }
			     
			        purchaseOrderRepository.save(poHeader);	
			        
			        //Status change entry
					if(!in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {
						statusChangeEntryPO(poHeader, in);
					}
					//Status change entry
		        }
				
				
		        
		        
		        				
			});

		}
	}

	
	private void dcBackTrack(InvoiceHeader in, InvoiceHeader pre){
		
		log.info("dcBackTrack");
		
		//Loop through DCs attached to the Invoice
		List<DeliveryChallanHeader> invDCHeaders = new ArrayList<DeliveryChallanHeader>();

		in.getInvoiceItems().forEach(ini -> {
			DeliveryChallanHeader itemDcHeader = ini.getDeliveryChallanItem() != null ? ini.getDeliveryChallanItem().getDeliveryChallanHeader() : null;

			if(itemDcHeader != null && preDCId != itemDcHeader.getId()){
				invDCHeaders.add(itemDcHeader);
				preDCId = itemDcHeader.getId();
			}
		});
		
//		deletedItemIds = new ArrayList<String>();
//		
//		if(pre != null) {
//			pre.getInvoiceItems()
//			.forEach(preItem -> {
//				//if(in.getInvoiceItems().stream().findFirst())
//				Boolean found = in.getInvoiceItems()
//				.stream()
//				.anyMatch(inItem -> inItem.getId().equals(preItem.getId()));
//				
//				if(!found && preItem.getDeliveryChallanItem() != null) {
//					deletedItemIds.add(preItem.getDeliveryChallanItem().getId());
//					log.info("Item deleted in invoice, dcId: "+preItem.getDeliveryChallanItem().getId());
//				}
//				
//			});
//		}
//		
//		log.info("deletedItemIds: "+deletedItemIds.size());
		
		log.info("invDCHeaders.size(): "+invDCHeaders.size());
		if(invDCHeaders.size() > 0 ){
			
			invDCHeaders.forEach(dcHeader -> {
				
				TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
				//Build TransactionStatusRule
				invBalanceQuantity = 0.0;
				//dcBalanceQuantity = 0.0;
				dcQuantity = 0.0;
				
				dcHeader.getDeliveryChallanItems().forEach(dci -> {
					invBalanceQuantity += dci.getInvoiceBalanceQuantity();
					dcQuantity += dci.getQuantity();
					
				});
				
				sourcePrevQuantity = 0.0;
				
//				if(pre != null )
//				{
//					pre.getInvoiceItems()
//					.forEach(ini -> {
//					log.info("preDCHead: "+ini.getDeliveryChallanHeader());
//					});
//				}
			
				
				if(pre != null )
				{
					pre.getInvoiceItems()
					.stream()
					.filter(prein -> prein.getDeliveryChallanItem() == null ? false : (prein.getDeliveryChallanItem().getDeliveryChallanHeader().getId().equals((dcHeader != null ? dcHeader.getId() : null))))
					.forEach(ini -> {
					sourcePrevQuantity += ini.getQuantity();
					});
				}

				String latestStatus = null;
				if(in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_DELETED)|| in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)){
//					latestStatus = transactionStatusChangeRepository.findByLatestTransactionId(dcHeader.getId()).getStatus().getName();
//					log.info("latestStatus: "+latestStatus);

					TransactionStatusChange transactionStatusChange = transactionStatusChangeRepository.findByLatestTransactionId(dcHeader.getId());

					if(transactionStatusChange != null){
					    if(transactionStatusChange.getStatus().getName().equalsIgnoreCase(ApplicationConstants.DC_STATUS_INVOICED)){
					   
                            latestStatus = ApplicationConstants.DC_STATUS_PARTIALLY_INVOICED;
                        }
					    else {
                            latestStatus = transactionStatusChange.getStatus().getName();
                        }
					}

					log.info("latestStatus: "+latestStatus);
				}

				transactionStatusRule.setInvoiceBalanceQuantity(invBalanceQuantity);
				transactionStatusRule.setCurrentStatus(dcHeader.getStatus().getName());
				transactionStatusRule.setTransactionId(dcHeader.getId());
				transactionStatusRule.setLatestStatus(latestStatus);
				transactionStatusRule.setQuantity(dcQuantity);

				invQuantity = 0.0;
				
				in.getInvoiceItems()
				.stream()
				.filter(inii -> inii.getDeliveryChallanItem() == null ? false : (inii.getDeliveryChallanItem().getDeliveryChallanHeader() != null && inii.getDeliveryChallanItem().getDeliveryChallanHeader().getId().equals(dcHeader.getId())))
				.forEach(ini -> {
					invQuantity += ini.getQuantity();
				});
				
				transactionStatusRule.setSourceQuantity(invQuantity);
				transactionStatusRule.setSourcePrevQuantity(sourcePrevQuantity);
				transactionStatusRule.setSourceTransactionId(in.getId());
				transactionStatusRule.setSourceTransactionStatus(in.getStatus().getName());
				transactionStatusRule.setSourceTransactionType(in.getInvoiceType().getName());
				transactionStatusRule.setTransactionType(dcHeader.getDeliveryChallanType().getName());
				
				log.info("transactionStatusRule: "+transactionStatusRule);

				
				//Get status from Drools
				
				kieSession.insert(transactionStatusRule);

		        int ruleCount = kieSession.fireAllRules();
		        
		        
		        
		        log.info("rules count: "+ruleCount);

				
				//Update DC with derived status
		        if(ruleCount > 0 && transactionStatusRule.getNewStatus() != null)
		        {
		        	log.info("1");
		        	Status status  = statusService.getStatusByTransactionTypeAndName(dcHeader.getDeliveryChallanType(), transactionStatusRule.getNewStatus());
			        dcHeader.setStatus(status);
			        
			        log.info("2");
			      //Meke dc Item updates
			        if(transactionStatusRule.getItemUpdateRequired()){
			        	log.info("3");
			        	dcHeader.getDeliveryChallanItems()
			        	.stream()
			        	//.filter(dci -> in.getInvoiceItems().stream().anyMatch(ini -> ini.getDeliveryChallanItem().getId().equals(dci.getId())))
			        	.filter(
			        			dci -> {
					        		Long preItemExistCount = 0l;
					        		if(pre != null){
						        		preItemExistCount= pre.getInvoiceItems()
										            .stream()
										            .filter(ini -> ini.getDeliveryChallanItem() != null && ini.getDeliveryChallanItem().getId().equals(dci.getId())).count();
					        		}
					        		if(preItemExistCount >0)
					        			return true;
					        		else
					        		//	return false;
					        			return in.getInvoiceItems().stream().anyMatch(di -> di.getDeliveryChallanItem() != null ?  di.getDeliveryChallanItem().getId().equals(dci.getId()) : false);
					        	})
			        	.forEach(dcItem -> {
			        		
			        		
			        		preQuantity = 0.0;
			        		
			        		if (pre != null  && pre.getInvoiceItems() != null && pre.getInvoiceItems().size() > 0)
			        		{
			        			OptionalDouble tempQuantity = 
			        			 pre.getInvoiceItems()
			        				.stream()
			        				.filter(ii -> ii.getDeliveryChallanItem() != null ? ii.getDeliveryChallanItem().getId() == dcItem.getId() : false)
			        				.mapToDouble(di -> di.getQuantity())
			        				.findFirst()
			        				//.getAsDouble();
			        				;
			        			
			        			
			        			if(tempQuantity.isPresent()) {
			        				preQuantity  = tempQuantity.getAsDouble();
								}
			        		}
			        		
			        		
			        		TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
			        		log.info("4: "+dcItem.getInvoiceBalanceQuantity());    
				            transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
				            transactionItemQuantityRule.setItemId(dcItem.getId());
				            transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
				            transactionItemQuantityRule.setPrevDCBalanceQuantity(null);
				            transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
				            //transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(dcItem.getInvoiceBalanceQuantity());
				            //transactionItemQuantityRule.setPrevQuantity(dcItem.getQuantity());
				            //transactionItemQuantityRule.setPrevStatus(transactionStatusRule.getCurrentStatus());
				            transactionItemQuantityRule.setQuantity(dcItem.getQuantity());
				            
				            sourceQuantity = 0.0;
							
				            log.info("5");
				            InvoiceItem iItem =  in.getInvoiceItems()
							.stream()

									.filter(i ->i.getDeliveryChallanItem()!=null? (i.getDeliveryChallanItem().getId().equals(dcItem.getId())):false)
							.findFirst()
							.orElse(null);
				            
				            
				            //In case of delete of an item in Invoice, above item will be null due to filter 
				            if(iItem != null){
				            	sourceQuantity = preQuantity != null ? (iItem.getQuantity() - preQuantity) : iItem.getQuantity();
				            }else{
								sourceQuantity = -preQuantity;
							}
				            //If Item is getting deleted in Invoice, respective DC Invoice balance quantity is faked to dc item quantity
				            if(sourceQuantity == 0 && Double.compare(iItem != null ? iItem.getQuantity() : 0.0, preQuantity) != 0){
				            	transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(dcItem.getQuantity());
								transactionItemQuantityRule.setSourceQuantity(0.0);
				            	log.info("5.5: "+transactionItemQuantityRule.getPrevInvoiceBalanceQuantity());
				            }else {
				            	transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(dcItem.getInvoiceBalanceQuantity());
								transactionItemQuantityRule.setSourceQuantity(sourceQuantity);
				            	log.info("5.6: "+transactionItemQuantityRule.getPrevInvoiceBalanceQuantity());
				            }
				            
				            log.info("6: "+sourceQuantity);
				            
//				            transactionItemQuantityRule.setSourceQuantity(sourceQuantity);
				            //transactionItemQuantityRule.setSourceTransactionType(transactionStatusRule.getSourceTransactionType());
				            //transactionItemQuantityRule.setStatus(transactionStatusRule.getNewStatus());
				            //transactionItemQuantityRule.setTransactionType(transactionStatusRule.getTransactionType());
				            
				            log.info("item rule before: "+transactionItemQuantityRule);
				            
				            kieSession.insert(transactionItemQuantityRule);

				            int ruleCounti = kieSession.fireAllRules();
				            
				            log.info("rules count: "+ruleCounti);
				            log.info("item rule after: "+transactionItemQuantityRule);
				            
				            if(ruleCounti > 0 && transactionItemQuantityRule.getInvoiceBalanceQuantity() != null)
				            	dcItem.setInvoiceBalanceQuantity(transactionItemQuantityRule.getInvoiceBalanceQuantity());
				            
				            log.info("dcitem invoice balance quantity completed");
				            
			        	});
			        }
			        
			        deliveryChallanHeaderRepository.save(dcHeader);	
			        
			        //Status change entry

                 if(!in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED))
                 {
                        statusChangeEntryDC(dcHeader, in);
                    }

			        
					//Status change entry				
		        }
				
				

		        

			});

		}
		
		//In invoice multiple DC can be selected
		//In case DC removed from that multiple list, removed DC should be back to New status
		
		List<DeliveryChallanHeader> removedDCList = new ArrayList<DeliveryChallanHeader>();
		
		if(pre != null)
		{
			pre.getInvoiceItems()
				.stream()
				.filter(inii -> inii.getDeliveryChallanItem() != null)
				.map(inii -> inii.getDeliveryChallanItem().getDeliveryChallanHeader())
				.distinct()
				.forEach(preDC -> {
					if(preDC != null) {
						log.info("preDC 111: "+preDC.getId());
						if(!invDCHeaders.contains(preDC)) {
							removedDCList.add(preDC);
						}
					}
				});
			
			log.info("removed dc count: "+removedDCList.size());
			
			removedDCList.forEach(rmDC -> {
				
				TransactionStatusChange transactionStatusChange = transactionStatusChangeRepository.findByLatestTransactionId(rmDC.getId());
				
	//			String latestStatus;
	//			if(transactionStatusChange != null){
	//				latestStatus = transactionStatusChange.getStatus().getName();
	//				log.info("rmDC latestStatus: "+latestStatus);
	//				
	//				Status status  = statusService.getStatusByTransactionTypeAndName(rmDC.getDeliveryChallanType(), latestStatus);
	//				
	//				rmDC.setStatus(status);
	//			}
				
				Status status  = statusService.getStatusByTransactionTypeAndName(rmDC.getDeliveryChallanType(), ApplicationConstants.DC_STATUS_NEW);
				rmDC.setStatus(status);
				
				rmDC.getDeliveryChallanItems()
				.forEach(rmdci -> {
					log.info("resetting balance quantity "+rmdci.getId() + " : "+(rmdci.getQuantity() - rmdci.getInvoiceBalanceQuantity()));
					rmdci.setInvoiceBalanceQuantity(rmdci.getQuantity() - rmdci.getInvoiceBalanceQuantity());
				});
				
				deliveryChallanHeaderRepository.save(rmDC);
				
			});
		}

	}
	
	
	private void GRNBackTrack(InvoiceHeader in, InvoiceHeader pre){
		
		log.info("GRNBackTrack");
		
		//Loop through grns attached to the Invoice
		List<GRNHeader> invGRNHeaders = new ArrayList<GRNHeader>();
		
		in.getInvoiceItems().forEach(ini -> {
			//log.info("ini"+ini.getGrnHeader());
			GRNHeader itemGrnHeader = ini.getGrnItem() != null ? ini.getGrnItem().getGrnHeader() : null;
			if(itemGrnHeader != null && preGRNId != itemGrnHeader.getId()){
				invGRNHeaders.add(itemGrnHeader);
				preGRNId = itemGrnHeader.getId();
			}
		});
		
		log.info("invGRNHeaders.size(): "+invGRNHeaders.size());
		if(invGRNHeaders.size() > 0 ){
			
			invGRNHeaders.forEach(grnHeader -> {
				
				TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
				//Build TransactionStatusRule
				invBalanceQuantity = 0.0;
				//dcBalanceQuantity = 0.0;
				GRNQuantity = 0.0;
				
				grnHeader.getGrnItems().forEach(grni -> {
					invBalanceQuantity += grni.getBalanceQuantity();
					GRNQuantity += grni.getAcceptedQuantity();
					
				});
				
				sourcePrevQuantity = 0.0;
				
				if(pre != null )
				{
					pre.getInvoiceItems()
							.stream()
							.filter(ini -> ini.getGrnItem() != null)
							.forEach(ini -> {
					sourcePrevQuantity += ini.getQuantity();
					});
				}

				String latestStatus = null;
				if(in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_DELETED)|| in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)){
//					latestStatus = transactionStatusChangeRepository.findByLatestTransactionId(dcHeader.getId()).getStatus().getName();
//					log.info("latestStatus: "+latestStatus);
					
					TransactionStatusChange transactionStatusChange = transactionStatusChangeRepository.findByLatestTransactionId(grnHeader.getId());

                    if (transactionStatusChange.getStatus().getName().equalsIgnoreCase(ApplicationConstants.GRN_STATUS_INVOICED)) {

                        latestStatus = ApplicationConstants.GRN_STATUS_PARTIALLY_INVOICED;
                    } else {
                        latestStatus = transactionStatusChange.getStatus().getName();
                    }
					
					log.info("latestStatus: "+latestStatus);
				}

				transactionStatusRule.setInvoiceBalanceQuantity(invBalanceQuantity);
				transactionStatusRule.setCurrentStatus(grnHeader.getGrnStatus().getName());
				transactionStatusRule.setTransactionId(grnHeader.getId());
				transactionStatusRule.setLatestStatus(latestStatus);
				transactionStatusRule.setQuantity(GRNQuantity);

				invQuantity = 0.0;
				
				in.getInvoiceItems()
						.stream()
						.filter(ini -> ini.getGrnItem() != null)
						.forEach(ini -> {
					invQuantity += ini.getQuantity();
				});
				
				transactionStatusRule.setSourceQuantity(invQuantity);
				transactionStatusRule.setSourcePrevQuantity(sourcePrevQuantity);
				transactionStatusRule.setSourceTransactionId(in.getId());
				transactionStatusRule.setSourceTransactionStatus(in.getStatus().getName());
				transactionStatusRule.setSourceTransactionType(in.getInvoiceType().getName());
				transactionStatusRule.setTransactionType(grnHeader.getGrnType().getName());
				
				log.info("transactionStatusRule: "+transactionStatusRule);

				
				//Get status from Drools
				
				kieSession.insert(transactionStatusRule);

		        int ruleCount = kieSession.fireAllRules();
		        
		        
		        
		        log.info("rules count: "+ruleCount);

				
				//Update DC with derived status
		        if(ruleCount > 0 && transactionStatusRule.getNewStatus() != null)
		        {
		        	log.info("1");
		        	Status status  = statusService.getStatusByTransactionTypeAndName(grnHeader.getGrnType(), transactionStatusRule.getNewStatus());
		        	grnHeader.setGrnStatus(status);
			        
			        log.info("2");
			      //Meke dc Item updates
			        if(transactionStatusRule.getItemUpdateRequired()){
			        	log.info("3");
			        	grnHeader.getGrnItems()
			        	.stream()
			        	//.filter(dci -> in.getInvoiceItems().stream().anyMatch(ini -> ini.getDeliveryChallanItem().getId().equals(dci.getId())))
			        	.filter(
			        			gri -> {
					        		Long preItemExistCount = 0l;
					        		if(pre != null){
						        		preItemExistCount= pre.getInvoiceItems()
										            .stream()
										            .filter(ini -> ini.getGrnItem() != null && ini.getGrnItem().getId().equals(gri.getId())).count();
					        		}
					        		if(preItemExistCount >0)
					        			return true;
					        		else
					        		//	return false;
					        			return in.getInvoiceItems().stream().anyMatch(ii -> ii.getGrnItem() != null ? ii.getGrnItem().getId().equals(gri.getId()) : false);
					        	})
			        	.forEach(grnItem -> {
			        		
			        		Double preQuantity = null;
			        		
			        		if (pre != null  && pre.getInvoiceItems() != null && pre.getInvoiceItems().size() > 0)
			        		{
			        			preQuantity = pre.getInvoiceItems()
			        				.stream()
			        				.filter(ii -> ii.getGrnItem().getId() == grnItem.getId())
			        				.mapToDouble(di -> di.getQuantity())
			        				.findFirst()
			        				.getAsDouble();
			        		}
			        		
			        		TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
			        		log.info("4");    
				            transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
				            transactionItemQuantityRule.setItemId(grnItem.getId());
				            transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
				            transactionItemQuantityRule.setPrevDCBalanceQuantity(null);
				            transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
				            transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(grnItem.getBalanceQuantity());
				            //transactionItemQuantityRule.setPrevQuantity(dcItem.getQuantity());
				            //transactionItemQuantityRule.setPrevStatus(transactionStatusRule.getCurrentStatus());
				            transactionItemQuantityRule.setQuantity(grnItem.getAcceptedQuantity());
				            
				            sourceQuantity = 0.0;

				            log.info("5");
				            InvoiceItem iItem =  in.getInvoiceItems()
							.stream()
							.filter(i -> i.getGrnItem()!=null? (i.getGrnItem().getId().equals(grnItem.getId())):false)
							.findFirst()
							.orElse(null);
				            
				            
				            //In case of delete of an item in Invoice, above item will be null due to filter 
				            if(iItem != null){
								sourceQuantity = preQuantity != null ? (iItem.getQuantity() - preQuantity) : iItem.getQuantity();
				            }else{
								sourceQuantity = -preQuantity;
							}
				            //If Item is getting deleted in Invoice, respective DC Invoice balance quantity is faked to dc item quantity
				            if(sourceQuantity == 0&& Double.compare(iItem != null ? iItem.getQuantity() : 0.0, preQuantity) != 0){
				            	transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(grnItem.getAcceptedQuantity());
								transactionItemQuantityRule.setSourceQuantity(0.0);
				            }else{
								transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(grnItem.getBalanceQuantity());
								transactionItemQuantityRule.setSourceQuantity(sourceQuantity);
							}
				            
				            log.info("6: "+sourceQuantity);
				            
//				            transactionItemQuantityRule.setSourceQuantity(sourceQuantity);
				            //transactionItemQuantityRule.setSourceTransactionType(transactionStatusRule.getSourceTransactionType());
				            //transactionItemQuantityRule.setStatus(transactionStatusRule.getNewStatus());
				            //transactionItemQuantityRule.setTransactionType(transactionStatusRule.getTransactionType());
				            
				            log.info("item rule before: "+transactionItemQuantityRule);
				            
				            kieSession.insert(transactionItemQuantityRule);

				            int ruleCounti = kieSession.fireAllRules();
				            
				            log.info("rules count: "+ruleCounti);
				            log.info("item rule after: "+transactionItemQuantityRule);
				            
				            if(ruleCounti > 0 && transactionItemQuantityRule.getInvoiceBalanceQuantity() != null)
				            	grnItem.setBalanceQuantity(transactionItemQuantityRule.getInvoiceBalanceQuantity());
				            
				            log.info("grnitem invoice balance quantity completed");
				            
			        	});
			        }
			        
			        grnHeaderRepository.save(grnHeader);	
			        
			        //Status change entry
					if(!in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {
						statusChangeEntryGRN(grnHeader, in);
					}
					//Status change entry				
		        }
				
				

		        

			});

		}
		

	}
	
	
	private void proformaBackTrack(InvoiceHeader in, InvoiceHeader pre){
		
		log.info("in proformaBackTrack");
		//Loop through Proforma Invoices attached to the Invoice
		List<InvoiceHeader> proformaInvHeaders = new ArrayList<InvoiceHeader>();
		
		in.getInvoiceItems().forEach(ini -> {
			InvoiceHeader itemPInvoiceHeader = ini.getProformaInvoiceItem() != null ? ini.getProformaInvoiceItem().getInvoiceHeader() : null;
			if(itemPInvoiceHeader != null && preProformaInvoiceId != itemPInvoiceHeader.getId()){
				proformaInvHeaders.add(itemPInvoiceHeader);
				preProformaInvoiceId = itemPInvoiceHeader.getId();
			}
		});
		
		log.info("proformaInvHeaders.size(): "+proformaInvHeaders.size());
		
		if(proformaInvHeaders.size() > 0 ){
			
			proformaInvHeaders.forEach(prInvHeader -> {
				
				TransactionStatusRule transactionStatusRule = new TransactionStatusRule();

				String latestStatus = null;
				log.info("in.getStatus().getName(): "+in.getStatus().getName());
				if(in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_DELETED)|| in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)){
					
					TransactionStatusChange transactionStatusChange = transactionStatusChangeRepository.findByLatestTransactionId(prInvHeader.getId());

					if(transactionStatusChange != null){
						latestStatus = transactionStatusChange.getStatus().getName();
					}
					log.info("latestStatus: "+latestStatus);
				}
				
				log.info("1: "+prInvHeader.getStatus());

				transactionStatusRule.setInvoiceBalanceQuantity(null);
				transactionStatusRule.setCurrentStatus(prInvHeader.getStatus().getName());
				transactionStatusRule.setTransactionId(prInvHeader.getId());
				transactionStatusRule.setLatestStatus(latestStatus);
				transactionStatusRule.setQuantity(null);

				invQuantity = 0.0;
				
				in.getInvoiceItems().forEach(ini -> {
					invQuantity += ini.getQuantity();
				});
				
				log.info("invQuantity: "+invQuantity);
				
				transactionStatusRule.setSourceQuantity(invQuantity);
				transactionStatusRule.setSourceTransactionId(in.getId());
				transactionStatusRule.setSourceTransactionStatus(in.getStatus().getName());
				transactionStatusRule.setSourceTransactionType(in.getInvoiceType().getName());
				transactionStatusRule.setTransactionType(prInvHeader.getInvoiceType().getName());
				
				log.info("transactionStatusRule: "+transactionStatusRule);

				
				//Get status from Drools
				
				kieSession.insert(transactionStatusRule);

		        int ruleCount = kieSession.fireAllRules();
		        
		        
		        
		        log.info("rules count: "+ruleCount);


				
				//Update Invoice with derived status
		        if(ruleCount > 0 && transactionStatusRule.getNewStatus() != null)
		        {
		        	Status status  = statusService.getStatusByTransactionTypeAndName(prInvHeader.getInvoiceType(), transactionStatusRule.getNewStatus());
			        prInvHeader.setStatus(status);

			        invoiceHeaderRepository.save(prInvHeader);	
			        
			        //Status change entry
					if(!in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {
						statusChangeEntryProInvoice(prInvHeader, in);
					}
		        }
				
		        
		        

		        
				//Status change entry				
			});

		}
	}
	
	
	private void invoiceBackTrack(InvoiceHeader in, InvoiceHeader pre){
		
		log.info("in invoiceBackTrack");
		//Loop through Invoices attached to the Debit/Credit Notes
		List<InvoiceHeader> sourceInvoiceHeaders = new ArrayList<InvoiceHeader>();
		
		
		in.getInvoiceItems().forEach(ini -> {
			InvoiceHeader itemSourceInvoiceHeader = ini.getSourceInvoiceItem() != null ? ini.getSourceInvoiceItem().getInvoiceHeader() : null;
			if(itemSourceInvoiceHeader != null && preSourceInvoiceId != itemSourceInvoiceHeader.getId()){
				sourceInvoiceHeaders.add(itemSourceInvoiceHeader);
				preSourceInvoiceId = itemSourceInvoiceHeader.getId();
			}
		});
		
		log.info("sourceInvoiceHeaders.size(): "+sourceInvoiceHeaders.size());
		
		if(sourceInvoiceHeaders.size() > 0 ){
			
			sourceInvoiceHeaders.forEach(srcInvHeader -> {
				
				TransactionStatusRule transactionStatusRule = new TransactionStatusRule();

				//Build TransactionStatusRule
				invBalanceQuantity = 0.0;
				//dcBalanceQuantity = 0.0;
				noteQuantity = 0.0;
				
				srcInvHeader.getInvoiceItems().forEach(ini -> {
					invBalanceQuantity += ini.getNoteBalanceQuantity();
					noteQuantity += ini.getQuantity();
					
				});
				
				sourcePrevQuantity = 0.0;
				
				if(pre != null )
				{
					pre.getInvoiceItems().forEach(ini -> {
					sourcePrevQuantity += ini.getQuantity();
					});
				}

				String latestStatus = null;
				if(in.getStatus().getName().equals(ApplicationConstants.NOTE_STATUS_DELETED)){
//					latestStatus = transactionStatusChangeRepository.findByLatestTransactionId(dcHeader.getId()).getStatus().getName();
//					log.info("latestStatus: "+latestStatus);
					
					TransactionStatusChange transactionStatusChange = transactionStatusChangeRepository.findByLatestTransactionId(srcInvHeader.getId());
					
					if(transactionStatusChange != null){
						latestStatus = transactionStatusChange.getStatus().getName();
					}
					
					log.info("latestStatus: "+latestStatus);
				}

				transactionStatusRule.setInvoiceBalanceQuantity(invBalanceQuantity);
				transactionStatusRule.setCurrentStatus(srcInvHeader.getStatus().getName());
				transactionStatusRule.setTransactionId(srcInvHeader.getId());
				transactionStatusRule.setLatestStatus(latestStatus);
				transactionStatusRule.setQuantity(noteQuantity);

				invQuantity = 0.0;
				
				in.getInvoiceItems().forEach(ini -> {
					invQuantity += ini.getQuantity();
				});
				
				transactionStatusRule.setSourceQuantity(invQuantity);
				transactionStatusRule.setSourcePrevQuantity(sourcePrevQuantity);
				transactionStatusRule.setSourceTransactionId(in.getId());
				transactionStatusRule.setSourceTransactionStatus(in.getStatus().getName());
				transactionStatusRule.setSourceTransactionType(in.getInvoiceType().getName());
				transactionStatusRule.setTransactionType(srcInvHeader.getInvoiceType().getName());
				
				log.info("transactionStatusRule: "+transactionStatusRule);

				
				//Get status from Drools
				
				kieSession.insert(transactionStatusRule);

		        int ruleCount = kieSession.fireAllRules();
		        
		        
		        
		        log.info("rules count: "+ruleCount);
		        log.info("transactionStatusRule.getNewStatus(): "+transactionStatusRule.getNewStatus());


				
				//Update Invoice with derived status
		        if(ruleCount > 0 && transactionStatusRule.getNewStatus() != null)
		        {
		        	log.info("1 before status get");
		        	Status status  = statusService.getStatusByTransactionTypeAndName(srcInvHeader.getInvoiceType(), transactionStatusRule.getNewStatus());
		        	log.info("got status: "+status.getName());
			        srcInvHeader.setStatus(status);
			        
			      //Meke dc Item updates
			        if(transactionStatusRule.getItemUpdateRequired()){
			        	log.info("3");
			        	srcInvHeader.getInvoiceItems()
			        	.stream()
			        	//.filter(dci -> in.getInvoiceItems().stream().anyMatch(ini -> ini.getDeliveryChallanItem().getId().equals(dci.getId())))
			        	.filter(
			        			pni -> {
					        		Long preItemExistCount = 0l;
					        		if(pre != null){
						        		preItemExistCount= pre.getInvoiceItems()
										            .stream()
										            .filter(ini -> ini.getSourceInvoiceItem() != null && ini.getSourceInvoiceItem().getId().equals(pni.getId())).count();
					        		}
					        		if(preItemExistCount >0)
					        			return true;
					        		else
					        		//	return false;
					        			return in.getInvoiceItems().stream().anyMatch(ii -> ii.getSourceInvoiceItem() != null && ii.getSourceInvoiceItem().getId().equals(pni.getId()));
					        	})
			        	.forEach(iItem -> {
			        		
			        		Double preQuantity = null;
			        		
			        		if (pre != null  && pre.getInvoiceItems() != null && pre.getInvoiceItems().size() > 0)
			        		{
			        			preQuantity = pre.getInvoiceItems()
			        				.stream()
			        				.filter(ii -> ii.getSourceInvoiceItem().getId() == iItem.getId())
			        				.mapToDouble(di -> di.getQuantity())
			        				.findFirst()
			        				.getAsDouble();
			        		}
			        		
			        		TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();
			        		log.info("4");    
				            transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
				            transactionItemQuantityRule.setItemId(iItem.getId());
				            transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
				            transactionItemQuantityRule.setPrevDCBalanceQuantity(null);
				            transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
				            transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(iItem.getNoteBalanceQuantity());
				            //transactionItemQuantityRule.setPrevQuantity(dcItem.getQuantity());
				            //transactionItemQuantityRule.setPrevStatus(transactionStatusRule.getCurrentStatus());
				            transactionItemQuantityRule.setQuantity(iItem.getQuantity());
				            
				            sourceQuantity = 0.0;

				            log.info("5");
				            InvoiceItem iiItem =  in.getInvoiceItems()
							.stream()
							.filter(i -> i.getSourceInvoiceItem()!=null?(i.getSourceInvoiceItem().getId().equals(iItem.getId())):false)
							.findFirst()
							.orElse(null);
				            
				            
				            //In case of delete of an item in Invoice, above item will be null due to filter 
				            if(iiItem != null){
								sourceQuantity = preQuantity != null ? (iiItem.getQuantity() - preQuantity) : iiItem.getQuantity();
				            }else{
								sourceQuantity = -preQuantity;
							}
				            //If Item is getting deleted in Invoice, respective Note Invoice balance quantity is faked to dc item quantity
				            if(sourceQuantity == 0 && Double.compare(iiItem != null ? iiItem.getQuantity() : 0.0, preQuantity) != 0){
				            	transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(iItem.getQuantity());
								transactionItemQuantityRule.setSourceQuantity(0.0);
				            }
				            else {
				            	transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(iItem.getNoteBalanceQuantity());
								transactionItemQuantityRule.setSourceQuantity(sourceQuantity);
				            }
				            
				            log.info("6: "+sourceQuantity);
				            
//				            transactionItemQuantityRule.setSourceQuantity(sourceQuantity);
				            //transactionItemQuantityRule.setSourceTransactionType(transactionStatusRule.getSourceTransactionType());
				            //transactionItemQuantityRule.setStatus(transactionStatusRule.getNewStatus());
				            //transactionItemQuantityRule.setTransactionType(transactionStatusRule.getTransactionType());
				            
				            log.info("item rule before: "+transactionItemQuantityRule);
				            
				            kieSession.insert(transactionItemQuantityRule);

				            int ruleCounti = kieSession.fireAllRules();
				            
				            log.info("rules count: "+ruleCounti);
				            log.info("item rule after: "+transactionItemQuantityRule);
				            
				            if(ruleCounti > 0 && transactionItemQuantityRule.getInvoiceBalanceQuantity() != null)
				            	iItem.setNoteBalanceQuantity(transactionItemQuantityRule.getInvoiceBalanceQuantity());
				            
				            log.info("dcitem invoice balance quantity completed");
				            
			        	});
			        }

			        invoiceHeaderRepository.save(srcInvHeader);	
			        
			        //Status change entry
					if(!in.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {

						statusChangeEntrySourceInvoice(srcInvHeader, in);
					}
		        }
				
		        
		        

		        
				//Status change entry				
			});

		}
	}
	
	private void statusChangeEntryPO(PurchaseOrderHeader poHeader , InvoiceHeader invHeader ){
		
		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();
		
		transactionStatusChange.setSourceOperation("Invoice "+invHeader.getStatus().getName());
		transactionStatusChange.setSourceTransactionId(invHeader.getId());
		transactionStatusChange.setSourceTransactionType(invHeader.getInvoiceType());
		transactionStatusChange.setSourceTransactionNumber(invHeader.getInvoiceNumber());
		transactionStatusChange.setStatus(poHeader.getStatus());
		transactionStatusChange.setTransactionId(poHeader.getId());
		transactionStatusChange.setTransactionType(poHeader.getPurchaseOrderType());
		transactionStatusChange.setTransactionNumber(poHeader.getPurchaseOrderNumber());
		transactionStatusChange.setParty(poHeader.getParty());

		transactionStatusChangeRepository.save(transactionStatusChange);
		
	}
	
	private void statusChangeEntryDC(DeliveryChallanHeader dcHeader , InvoiceHeader invHeader ){
		
		log.info("statusChangeEntryDC: "+invHeader.getStatus()+" : "+invHeader.getInvoiceType() );
		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();
		
		transactionStatusChange.setSourceOperation("Invoice "+invHeader.getStatus().getName());
		transactionStatusChange.setSourceTransactionId(invHeader.getId());
		transactionStatusChange.setSourceTransactionType(invHeader.getInvoiceType());
		transactionStatusChange.setSourceTransactionNumber(invHeader.getInvoiceNumber());
		transactionStatusChange.setStatus(dcHeader.getStatus());
		transactionStatusChange.setTransactionId(dcHeader.getId());
		transactionStatusChange.setTransactionType(dcHeader.getDeliveryChallanType());
		transactionStatusChange.setTransactionNumber(dcHeader.getDeliveryChallanNumber());
		transactionStatusChange.setParty(dcHeader.getParty());

		transactionStatusChangeRepository.save(transactionStatusChange);
		log.info("statusChangeEntryDC complete");
		
	}
	
	private void statusChangeEntryGRN(GRNHeader grnHeader , InvoiceHeader invHeader ){
		
		log.info("statusChangeEntryGRN: "+invHeader.getStatus()+" : "+invHeader.getInvoiceType() );
		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();
		
		transactionStatusChange.setSourceOperation("Invoice "+invHeader.getStatus().getName());
		transactionStatusChange.setSourceTransactionId(invHeader.getId());
		transactionStatusChange.setSourceTransactionType(invHeader.getInvoiceType());
		transactionStatusChange.setSourceTransactionNumber(invHeader.getInvoiceNumber());
		transactionStatusChange.setStatus(grnHeader.getGrnStatus());
		transactionStatusChange.setTransactionId(grnHeader.getId());
		transactionStatusChange.setTransactionType(grnHeader.getGrnType());
		transactionStatusChange.setTransactionNumber(grnHeader.getGrnNumber());
		transactionStatusChange.setParty(grnHeader.getSupplier());

		transactionStatusChangeRepository.save(transactionStatusChange);
		log.info("statusChangeEntryGRN complete");
		
	}
	
	private void statusChangeEntryProInvoice(InvoiceHeader prInvHeader, InvoiceHeader invHeader){

		log.info("statusChangeEntryProInvoice: "+invHeader.getStatus()+" : "+invHeader.getInvoiceType());
		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();
		
		transactionStatusChange.setSourceOperation("Invoice "+invHeader.getStatus().getName());
		transactionStatusChange.setSourceTransactionId(invHeader.getId());
		transactionStatusChange.setSourceTransactionType(invHeader.getInvoiceType());
		transactionStatusChange.setSourceTransactionNumber(invHeader.getInvoiceNumber());
		transactionStatusChange.setStatus(prInvHeader.getStatus());
		transactionStatusChange.setTransactionId(prInvHeader.getId());
		transactionStatusChange.setTransactionType(prInvHeader.getInvoiceType());
		transactionStatusChange.setTransactionNumber(prInvHeader.getInvoiceNumber());
		transactionStatusChange.setParty(prInvHeader.getParty());

		transactionStatusChangeRepository.save(transactionStatusChange);
		log.info("statusChangeEntryProInvoice complete");
	}
	
	private void statusChangeEntrySourceInvoice(InvoiceHeader prInvHeader, InvoiceHeader invHeader){

		log.info("statusChangeEntrySourceInvoice: "+invHeader.getStatus()+" : "+invHeader.getInvoiceType());
		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();
		
		transactionStatusChange.setSourceOperation("Note "+invHeader.getStatus().getName());
		transactionStatusChange.setSourceTransactionId(invHeader.getId());
		transactionStatusChange.setSourceTransactionType(invHeader.getInvoiceType());
		transactionStatusChange.setSourceTransactionNumber(invHeader.getInvoiceNumber());
		transactionStatusChange.setStatus(prInvHeader.getStatus());
		transactionStatusChange.setTransactionId(prInvHeader.getId());
		transactionStatusChange.setTransactionType(prInvHeader.getInvoiceType());
		transactionStatusChange.setTransactionNumber(prInvHeader.getInvoiceNumber());
		transactionStatusChange.setParty(prInvHeader.getParty());
		

		transactionStatusChangeRepository.save(transactionStatusChange);
		log.info("statusChangeEntryProInvoice complete");
	}
	
	
	
	
	public void itemDelete(InvoiceItem item){
		
		log.info("in itemDelete--------");
		
		
		//PO
		if(item.getPurchaseOrderItem() != null){
			
			PurchaseOrderHeader poHeader = item.getPurchaseOrderItem().getPurchaseOrderHeader();
			if(poHeader.getStatus().getName().equals(ApplicationConstants.PURCHASE_ORDER_STATUS_INVOICED)){
				Status status  = statusService.getStatusByTransactionTypeAndName(poHeader.getPurchaseOrderType(), ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_INVOICED);
		        poHeader.setStatus(status);
		        purchaseOrderRepository.save(poHeader);	
			}
			
			PurchaseOrderItem poItem = item.getPurchaseOrderItem();
			
			poItem.setInvoiceBalanceQuantity(poItem.getInvoiceBalanceQuantity()+item.getQuantity());
			
			purchaseOrderItemRepository.save(poItem);	
			
			statusChangeEntryPO(poHeader, item.getInvoiceHeader());
			
		}
		
		//DC
		if(item.getDeliveryChallanItem() != null){
			
			DeliveryChallanHeader dcHeader = item.getDeliveryChallanItem().getDeliveryChallanHeader();
			if(dcHeader.getStatus().getName().equals(ApplicationConstants.DC_STATUS_INVOICED)){
				Status status  = statusService.getStatusByTransactionTypeAndName(dcHeader.getDeliveryChallanType(), ApplicationConstants.DC_STATUS_PARTIALLY_INVOICED);
		        dcHeader.setStatus(status);
		        deliveryChallanHeaderRepository.save(dcHeader);	
			}
			
			DeliveryChallanItem dcItem = item.getDeliveryChallanItem();
			
			dcItem.setInvoiceBalanceQuantity(dcItem.getInvoiceBalanceQuantity()+item.getQuantity());
			
			deliveryChallanItemRepository.save(dcItem);	
			
			statusChangeEntryDC(dcHeader, item.getInvoiceHeader());
			
		}
		
	}
}
