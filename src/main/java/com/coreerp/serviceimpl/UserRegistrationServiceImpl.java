package com.coreerp.serviceimpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;

import com.coreerp.dto.PartyDTO;
import com.coreerp.mapper.PartyMapper;
import com.coreerp.model.*;
import com.coreerp.service.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.EmployeeTypeRepository;
import com.coreerp.dao.GstRegistrationTypeRepository;
import com.coreerp.dao.RoleRepository;
import com.coreerp.dao.UserTenantRelationRepository;
import com.coreerp.dto.CompanyDTO;
import com.coreerp.dto.UserDTO;
import com.coreerp.dto.UserTenantRelationDTO;
import com.coreerp.mapper.CompanyMapper;
import com.coreerp.mapper.UserMapper;
import com.coreerp.multitenancy.TenantContext;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.FileSystemResourceAccessor;

@Service
public class UserRegistrationServiceImpl implements UserRegistrationService {

	private static Logger logger = LogManager.getLogger(UserRegistrationServiceImpl.class.getName());
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	@Autowired
	UserTenantRelationRepository userTenantRelationRepository;

	@Autowired
	EmployeeTypeRepository employeeTypeRepository;

	@Autowired
	CompanyMapper companyMapper;

	@Autowired
	UserMapper userMapper;

	@Autowired
	CompanyService companyService;

	@Autowired
	UserService userService;

	@Autowired
	CityService cityService;

	@Autowired
	AreaService areaService;

	@Autowired
	StateService stateService;

	@Autowired
	CountryService countryService;

	@Autowired
	GstRegistrationTypeRepository gstRegistrationTypeRepository;

//	@Value("${db.url}")
	@Value("${spring.datasource.url}")
	private String dbUrl;

	@Value("${spring.datasource.username}")
	private String dbUsername;

	@Value("${spring.datasource.password}")
	private String dbPassword;

	@Value("${tenant.schema.prefix}")
	private String tenantSchemaPrefix;

	@Value("${db.url.jasper}")
	private String databaseUrl;

	@Autowired
	private BCryptPasswordEncoder bCryprPasswordEncoder;

	@Autowired
	RoleRepository roleRepository;


	@Override
	public UserTenantRelation  createUserTenantRelationEntry(UserTenantRelation userTenantRelation) {
		/*
		 * --set context to default --create userTenantRelation object
		 * userTenantRelationService.save
		 */

		//Boolean returnValue = true;

		UserTenantRelation savedUserTenantRelation = null;

		ExecutorService es = Executors.newSingleThreadExecutor();



		logger.info("incoming userTenantRelation: "+ userTenantRelation.getTenant());
		logger.info("userTenantRelation: "+userTenantRelation.getUsername());

		Future<UserTenantRelation> utrFuture =  es.submit(new Callable<UserTenantRelation>(){

			@Override
			public UserTenantRelation call() throws Exception {
				try {
					TenantContext.setCurrentTenant(ApplicationConstants.DEFAULT_TENANT_ID);

					//Get max company and set + 1 to new company
					Integer maxCompanyId = userTenantRelationRepository.getMaxCompanyId() + 1;
					userTenantRelation.setCompanyId(maxCompanyId);
					//TO DO Check if username exists

					//Replace spaces with underscore and make it loweer case
					String tenantName = userTenantRelation.getTenant().replaceAll("[^\\p{Alpha}\\p{Digit}]+","_").toLowerCase();


					tenantName = tenantSchemaPrefix + tenantName;

					//Limit tenant name to 64 characters
					//int tenantNameLength = tenantName.length();
					tenantName = tenantName.length() > 64 ? tenantName.substring(0,63) : tenantName;

					userTenantRelation.setTenant(tenantName);

					logger.info("Check if tenant exists");
					//Check if tenant exists
					if(userTenantRelationRepository.findFirstByTenant(tenantName) != null){
						logger.info("Company name exists! appending timestamp");
						tenantName = tenantName.length() > 64 ? tenantName.substring(0,50) : tenantName;
						tenantName = tenantName+ "_" + new Date().getTime() ;
						userTenantRelation.setTenant(tenantName ); // Append milliseconds for uniqueness
					}



					UserTenantRelation systemUser = new UserTenantRelation();


					systemUser.setTenant(tenantName);
					systemUser.setUsername("SYSTEM@"+tenantName);
					systemUser.setName("SYSTEM_"+tenantName);
					systemUser.setEmail(null);
					systemUser.setCreatedBy("system");
					systemUser.setCreatedDateTime(new Date());
					systemUser.setRegistrationDate(new Date());
					systemUser.setUpdatedBy("system");
					systemUser.setUpdatedDateTime(new Date());
					systemUser.setIsActive(1);
					systemUser.setCompanyId(maxCompanyId);
					userTenantRelationRepository.save(systemUser);

                   // Party party1= partyService.getPartyObjectById()
					//logger.info("Saving party   "+party1);
					//userTenantRelation.setParty(party);

					logger.info("Saving tenant");
					UserTenantRelation returnUtr = userTenantRelationRepository.save(userTenantRelation);


					return  returnUtr;






				} catch (Exception e) {

					logger.error("Exception while saving user tenant relation " + e);
					//returnValue = false;

					return null;
				}

			}

		});

		try {
			savedUserTenantRelation = utrFuture.get();

		} catch (InterruptedException | ExecutionException e) {

			e.printStackTrace();
		}

		es.shutdown();



		logger.info("returning "+ savedUserTenantRelation.getTenant() +","+ savedUserTenantRelation.getUsername());
		return savedUserTenantRelation;
	}


	@Override
	public void  deleteUserTenantRelationEntry(UserTenantRelation userTenantRelation) {
		/*
		 * --set context to default --create userTenantRelation object
		 * userTenantRelationService.save
		 */

		//Boolean returnValue = true;

		//UserTenantRelation savedUserTenantRelation = null;

		ExecutorService es = Executors.newSingleThreadExecutor();



		logger.info("incoming userTenantRelation: "+ userTenantRelation.getTenant());
		logger.info("userTenantRelation: "+userTenantRelation.getUsername());

		Future<UserTenantRelation> utrFuture =  es.submit(new Callable<UserTenantRelation>(){

			@Override
			public UserTenantRelation call() throws Exception {
				try {
					TenantContext.setCurrentTenant(ApplicationConstants.DEFAULT_TENANT_ID);


					userTenantRelationRepository.delete(userTenantRelation);

					return null;

				} catch (Exception e) {

					logger.error("Exception while deleting user tenant relation " + e);
					//returnValue = false;

					return null;
				}

			}

		});


		try {
			utrFuture.get();

		} catch (InterruptedException | ExecutionException e) {

			e.printStackTrace();
		}


		es.shutdown();



	}

	@Override
	public Boolean createNewDataBase(UserTenantRelation userTenantRelation) {

		/*
		 * --create new database
		 *
		 * mysqladmin -uroot -psecret create mybos_coreerp_tenant3
		 */

		Boolean returnValue = true;

		String dbName = userTenantRelation.getTenant();
//
//		dbName = dbName.replaceAll(" ", "_").toLowerCase();
//		String dbPasswordCreateDB = dbPassword.replaceAll("\\$","\\\\\\$");
//		logger.info("pwd: "+dbPassword);
//		String command = "mysqladmin -u" + dbUsername + " -p" + dbPasswordCreateDB + " create " + dbName;
//
//		if (!executeCommand(command, new File(System.getProperty("user.home")))){
//			returnValue = false;
//		}

		String sql = "CREATE DATABASE IF NOT EXISTS "+ dbName;
		PreparedStatement stmt = null;
		Connection conn = null;
		logger.info("running: "+sql);
		try  {
//			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
//			String url = dbUrl+"?user="+dbUsername+"&password="+dbPassword;
//			conn = DriverManager.getConnection
//					(url);
			stmt = conn.prepareStatement(sql);
			stmt.execute();
			logger.info("done: "+sql);
		} catch (Exception e) {
			logger.error("Error creating DB: "+ e.getMessage());
			logger.error("Error creating DB: "+e.getStackTrace());
			returnValue = false;
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
				logger.error(se2.getStackTrace());
			}
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				logger.error(se.getStackTrace());
			}
		}
		return returnValue;
	}

	@Override
	public Boolean createTenantObjects(UserTenantRelation userTenantRelation) {

		/*
		 * --Create objects with seed data. liquibase changelist will take care of both

			liquibase --defaultsFile=.\liquibase-tenant.properties --username=<value> --password=<value> --url=<value> update

		 */
		Boolean returnValue = true;


		String changeLogFile = "db/liquibase/db-changelog-tenant-1.0.xml";
		String url = databaseUrl+userTenantRelation.getTenant()+"?useSSL=false";

		try{
			updateDb(url, dbUsername, dbPassword, changeLogFile);

			logger.info("liquibase update finished OK");
		}catch(Exception e){
			logger.error("Could not run liquibase: "+e);
			returnValue = false;
		}

		return returnValue;
	}

	@Override
	public Boolean createJobworkEntries(UserTenantRelation userTenantRelation) {

		/*
		 * --Create Jobwork specific entries in in menu, transaction type etc

		 */
		Boolean returnValue = true;
		String changeLogFile = "db/liquibase/db-changelog-jw-1.0.xml";
		String url = databaseUrl+userTenantRelation.getTenant()+"?useSSL=false";

		try{
			updateDb(url, dbUsername, dbPassword, changeLogFile);

			logger.info("liquibase update finished OK");
		}catch(Exception e){
			logger.error("Could not run liquibase: "+e);
			returnValue = false;
		}

		return returnValue;
	}

//	@Override
//	public Boolean createUserInTenant(Company company, UserDTO user, UserTenantRelation userTenantRelation) {
//	/*
//	 * --Create an entry in ma_user
//
//		--Set user object
//		--Set context to new schema
//
//		TenantContext.setCurrentTenant(tenantId);
//
//		--userservice.save
//	 */
//
//		Boolean returnValue = true;
//
//		String tenant = userTenantRelation.getTenant();
//
//		TenantContext.setCurrentTenant(tenant);
//
//		user.setCompanyId(company.getId());
//		user.setEmployeeTypeId(1l);//TO DO get it from somewhere
//		user.setDeleted("N");
//		try{
//			//Delete existing entry
//
//			userService.deleteUser(1l);
//
//			//Add passed one
//
//			userService.saveUser(user);
//
//			logger.info("User created");
//		}catch(Exception e){
//			logger.error("Erro creating user: "+e);
//			returnValue = false;
//		}
//
//
//
//		return returnValue;
//	}

	@Override
	public Boolean sendConformationEmail(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean activateUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Company createCompanyAndUser(CompanyDTO companyDTO, UserDTO user, UserTenantRelationDTO userTenantRelationCreated) {
	/*--Create entry in ma_company


	--Set company object
	--Set context to new schema

	TenantContext.setCurrentTenant(tenantId);

	--companyservice.save
	 *
	 */

		//Boolean returnValue = true;

		logger.info("in createCompanyAndUser: "+companyDTO);
		Company company = null;

		//String tenant = userTenantRelation.getTenant();

		//logger.info("setting tenant to "+tenant);
		//TenantContext.setCurrentTenant(tenant);

		companyDTO.setDeleted("N");
		companyDTO.setCountryId(companyDTO.getCountryId() != null ? companyDTO.getCountryId() : 1l); //TO DO get proper country
		companyDTO.setStateId(companyDTO.getStateId() != null ? companyDTO.getStateId() : 1l); //TO DO get proper state

		//User userModel = new User();
		try{

			//Get existing company (default record)
			logger.info("TenantContext: "+TenantContext.getCurrentTenant());

			Company companyPre = companyService.getModelById(1l);


//			if(companyPre != null){
//				logger.info("Deleting company ");
//				companyService.delete(companyPre);
//			}


			//copy dto details to companypre

			logger.info("1 "+companyDTO);
			logger.info("1.2 "+companyPre);
			logger.info("TenantContext: "+TenantContext.getCurrentTenant());

			companyPre.setName(companyDTO.getName());
			logger.info("2");
			companyPre.setAddress(companyDTO.getAddress());
			logger.info("3");
			companyPre.setCity(companyDTO.getCityId() != null ? cityService.getCityById(companyDTO.getCityId()) : null);
			logger.info("4");
			companyPre.setCompanyLogoPath(companyDTO.getCompanyLogoPath());
			logger.info("5");
			companyPre.setCountry(countryService.getCountryById(companyDTO.getCountryId()));
			logger.info("6");
			companyPre.setGstNumber(companyDTO.getGstNumber());
			logger.info("7");
			companyPre.setEmail(companyDTO.getEmail());
			logger.info("8");
			companyPre.setState(companyDTO.getStateId() != null ? stateService.getStateById(companyDTO.getStateId()) : null);
			logger.info("9: "+companyPre.getState().getName());
			companyPre.setGstRegistrationType(companyDTO.getGstRegistrationTypeId() != null ? gstRegistrationTypeRepository.getOne(companyDTO.getGstRegistrationTypeId()) : null);
			logger.info("10");
			//Create the one passed

			///Company companySave = companyMapper.dtoToModelMap(companyDTO);



			//Company companyNew = companyMapper.dtoToModelMap(companyDTO);

			logger.info("before company save"+companyPre);

			company = companyService.saveModel(companyPre);
			logger.info("company saved"+company);


//			user.setCompanyId(company.getId());
//			user.setEmployeeTypeId(1l);//TO DO get it from somewhere
//			user.setDeleted("N");
//
//			userService.deleteUser(1l);
//
//			//Add passed one
//
//			userService.saveUser(user);


			//Get pre exisitng user

			User userModel = userService.getUserById(1000l);


			userModel.setCompany(company);
			userModel.setUsername(user.getUsername());
			//userModel.setPassword(user.getPassword());
			userModel.setPassword(user.getPassword() != null ? bCryprPasswordEncoder.encode(user.getPassword()) : null);
			userModel.setEmail(user.getEmail());
			userModel.setName(user.getName());
			userModel.setIsLogInRequired(user.getIsLogInRequired());
			userModel.setCurrentAddress(user.getCurrentAddress());
			userModel.setMobileNumber(user.getMobileNumber());
			//userModel.setRoles(user.getRoleIds());
			//userModel.setRoles(new HashSet<Role>(roleRepository.findAll(user.getRoleIds())));
			userModel.setEmployeeType(employeeTypeRepository.getOne(1l));
			userModel.setUserLanguage(user.getUserLanguage());

			userService.save(userModel);

			logger.info("User created");


			User systemUser = userService.getUserById(9999l);


			systemUser.setCompany(company);
			systemUser.setUsername("SYSTEM@"+userTenantRelationCreated.getTenant());
			//userModel.setPassword(user.getPassword());
			systemUser.setPassword(systemUser.getPassword() != null ? bCryprPasswordEncoder.encode(systemUser.getPassword()) : null);
//			systemUser.setEmail(user.getEmail());
			systemUser.setName("SYSTEM_"+userTenantRelationCreated.getTenant());
			systemUser.setIsLogInRequired(1l);
//			systemUser.setCurrentAddress(user.getCurrentAddress());
//			systemUser.setMobileNumber(user.getMobileNumber());
			//userModel.setRoles(user.getRoleIds());
			//userModel.setRoles(new HashSet<Role>(roleRepository.findAll(user.getRoleIds())));
			systemUser.setEmployeeType(employeeTypeRepository.getOne(4l));


			userService.save(systemUser);


		}catch(Exception e){
			logger.error("Error saving company: "+e);
			//returnValue = false;
		}



		return company;
	}


//	private Boolean executeCommand(String command, File file){
//
//		Boolean returnValue = true;
//
//		boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
//
//		ProcessBuilder builder = new ProcessBuilder();
//
////		String commmand = "mysqladmin -u" + dbUsername + " -p" + dbPassword + " create "
////				+ dbName;
//
//		if (isWindows) {
//			builder.command("cmd.exe", "/c", command);
//		} else {
//			builder.command("sh", "-c", command);
//		}
//
//		try{
//			builder.directory(file);
//
//			Process process = builder.start();
//
//			StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), System.out::println);
//			Executors.newSingleThreadExecutor().submit(streamGobbler);
//
//			int exitCode = process.waitFor();
//			assert exitCode == 0;
//			logger.info("command executed successfully: "+command);
//		}catch(Exception e){
//			logger.error("Exception runnning command: "+e);
//			returnValue = false;
//		}
//
//		return returnValue;
//	}


	private void updateDb(String url, String login, String password, String changeLogFile) throws Exception{
		Connection c = DriverManager.getConnection(url,login,password);
		logger.info("got connection");

		Liquibase liquibase = null;
		try {
			Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(c));
			liquibase = new Liquibase(changeLogFile, new FileSystemResourceAccessor(), database);
			String context = null;
			liquibase.update(context);
			logger.info("liquibase update done");
		} catch (Exception e) {
			throw new DatabaseException(e);
		} finally {
			if (liquibase != null) {
				liquibase.forceReleaseLocks();
				logger.info("liquibase rollback");
			}
			if (c != null) {
				try {
					c.rollback();
					c.close();
					logger.info("connection rollback");
				} catch (SQLException e) {
					//nothing to do
					logger.info("Sql exception "+e);
				}
			}
		}
	}


	private static class StreamGobbler implements Runnable {


		private InputStream inputStream;
		private Consumer<String> consumer;

		public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
			this.inputStream = inputStream;
			this.consumer = consumer;
		}

		@Override
		public void run() {
			new BufferedReader(new InputStreamReader(inputStream)).lines()
					.forEach(consumer);
		}

	}


}
