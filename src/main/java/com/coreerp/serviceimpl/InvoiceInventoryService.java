package com.coreerp.serviceimpl;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.mapper.MaterialMapper;
import com.coreerp.model.GRNItem;
import com.coreerp.model.GlobalSetting;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.InvoiceItem;
import com.coreerp.model.Material;
import com.coreerp.service.GlobalSettingService;
import com.coreerp.service.InventoryService;
import com.coreerp.service.StockTraceService;


@Component
public class InvoiceInventoryService implements InventoryService<InvoiceHeader> {

	final static Logger log = LogManager.getLogger(InvoiceInventoryService.class);
	
//	@Autowired
//	MaterialService materialService;
	
	@Autowired
	MaterialRepository materialRepository;
	
	@Autowired
	GlobalSettingService globalSettingService;
	
	@Autowired
	MaterialMapper materialMapper;
	
	@Autowired
	StockTraceService stockTraceService;
	
	/* CUSTOMER INVOICE
	 * ** Update Material stock if decrease/increase stock is on Customer Invoice 
	 * ** if invoice status is 'New' or 'Open'
	 * 
	 * 		** update stock = stock - invoice item quantity
	 * 
 	 *  ** if invoice status is 'Cancelled'
	 * 
	 * 		** update stock = stock + invoice item quantity
	 * 
	 * ** if invoice status is 'Deleted'
	 * 
	 * 		** update stock = stock + invoice item quantity	
	 */
	@Override
	public String updateInventory(InvoiceHeader transactionComponent, InvoiceHeader invoiceHeaderPre) {
		String updateInventoryStatus = "Failed";
		String preStatus = invoiceHeaderPre != null ? (invoiceHeaderPre.getStatus() != null ? invoiceHeaderPre.getStatus().getName() : "") : "";
		log.info("preStatus: "+preStatus);
		//log.info("transactionComponent.getStatus(): "+transactionComponent.getStatus());
		
		if(stockDecreaseApplicable(transactionComponent)){
			if(transactionComponent.getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_CUSTOMER)){
				
			
				if(transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_NEW) || transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_OPEN)){
					updateInventoryStatus = dicreaseStock(transactionComponent, invoiceHeaderPre);
					stockTraceService.saveCustomerInvoiceCreationStockTrace(transactionComponent, invoiceHeaderPre);
				}else if(transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)){
					updateInventoryStatus = increaseStock(transactionComponent, invoiceHeaderPre);
					stockTraceService.postReversalCustomerInvoiceStockTrace(transactionComponent);
				}else if(transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_DELETED) && !preStatus.equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)){
					updateInventoryStatus = increaseStock(transactionComponent, invoiceHeaderPre);
					stockTraceService.postReversalCustomerInvoiceStockTrace(transactionComponent);
				}
			}else if(transactionComponent.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_CREDIT) || transactionComponent.getInvoiceType().getName().equals(ApplicationConstants.JOBWORK_CREDIT_NOTE)){
				if(transactionComponent.getStatus().getName().equals(ApplicationConstants.NOTE_STATUS_NEW) || transactionComponent.getStatus().getName().equals(ApplicationConstants.NOTE_STATUS_OPEN)){
					updateInventoryStatus = increaseStock(transactionComponent, invoiceHeaderPre);
					stockTraceService.saveCreditNoteCreationStockTrace(transactionComponent, invoiceHeaderPre);
				}else if(transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)){
					updateInventoryStatus = dicreaseStock(transactionComponent, invoiceHeaderPre);
					stockTraceService.postReversalCreditNoteStockTrace(transactionComponent);
				}else if(transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_DELETED)){
					updateInventoryStatus = dicreaseStock(transactionComponent, invoiceHeaderPre);
					stockTraceService.postReversalCreditNoteStockTrace(transactionComponent);
				}
			}else if(transactionComponent.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_DEBIT) || transactionComponent.getInvoiceType().getName().equals(ApplicationConstants.SUBCONTRACT_DEBIT_NOTE)){
				if(transactionComponent.getStatus().getName().equals(ApplicationConstants.NOTE_STATUS_NEW) || transactionComponent.getStatus().getName().equals(ApplicationConstants.NOTE_STATUS_OPEN)){
					updateInventoryStatus = dicreaseStock(transactionComponent, invoiceHeaderPre);
					stockTraceService.saveCustomerInvoiceCreationStockTrace(transactionComponent, invoiceHeaderPre);
				}else if(transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)){
					updateInventoryStatus = increaseStock(transactionComponent, invoiceHeaderPre);
					stockTraceService.postReversalCustomerInvoiceStockTrace(transactionComponent);
				}else if(transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_DELETED)){
					updateInventoryStatus = increaseStock(transactionComponent, invoiceHeaderPre);
					stockTraceService.postReversalCustomerInvoiceStockTrace(transactionComponent);
				}
			}
			
			
			// Stock Trace for customer Invoice

			

//			if (transactionComponent.getStatus().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)
//					|| transactionComponent.getStatus().equals(ApplicationConstants.INVOICE_STATUS_DELETED)) {
//				stockTraceService.postReversalCustomerInvoiceStockTrace(transactionComponent);
//			} else {
//				stockTraceService.saveCustomerInvoiceCreationStockTrace(transactionComponent, invoiceHeaderPre);
//			}
			
		}else if(stockIncreaseApplicable(transactionComponent)){
			if(transactionComponent.getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_SUPPLIER)){
				
			
				if(transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_NEW) || transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_OPEN)){
					updateInventoryStatus = increaseStock(transactionComponent, invoiceHeaderPre);
					stockTraceService.saveSupplierInvoiceCreationStockTrace(transactionComponent, invoiceHeaderPre);
				}else if(transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)){
					updateInventoryStatus = dicreaseStock(transactionComponent, invoiceHeaderPre);
					stockTraceService.postReversalSupplierInvoiceStockTrace(transactionComponent);
				}else if(transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_DELETED)){
					updateInventoryStatus = dicreaseStock(transactionComponent, invoiceHeaderPre);
					stockTraceService.postReversalSupplierInvoiceStockTrace(transactionComponent);
				}
			}
		}
		
		return updateInventoryStatus;
	}

	private String increaseStock(InvoiceHeader transactionComponent, InvoiceHeader invoiceHeaderPre) {
		String status = "Failed";
		
		transactionComponent.getInvoiceItems().stream()
							.forEach(invoiceItem -> {
								Material material = materialRepository.getOne(invoiceItem.getMaterial().getId());
								log.info("material: "+material);
								if(material.getSupplyType().getName().equals(ApplicationConstants.SUPPLY_TYPE_GOODS)){
									
									Double quantity = invoiceItem.getQuantity();
									
									OptionalDouble preQuantity = (invoiceHeaderPre != null  && invoiceHeaderPre.getInvoiceItems() != null)? invoiceHeaderPre.getInvoiceItems()
														.stream()
														.filter(iitem -> iitem.getId().equals(invoiceItem.getId()))
														.mapToDouble(iitem -> iitem.getQuantity())
														.findFirst()
														: null;

									Double updateQuantity = (preQuantity != null && preQuantity.isPresent() && preQuantity.getAsDouble() != quantity) ? (quantity - preQuantity.getAsDouble()) : quantity;
									
									log.info("updatedQuantity: "+updateQuantity);
									
									if((preQuantity != null && preQuantity.isPresent() && preQuantity.getAsDouble() != quantity) || ( preQuantity == null || !preQuantity.isPresent()) || transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED) || transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_DELETED))
									{
										log.info("Material stock update reqired: "+quantity+" : "+preQuantity);
										material.setStock(material.getStock() != null ? material.getStock() + updateQuantity : updateQuantity);								
										
										//MaterialDTO materialDTO = materialMapper.modelToDTOMap(material);
										materialRepository.save(material);
									}else {
										log.info("Material stock update NOT reqired: "+quantity+" : "+preQuantity);
									}
								}

							});
		
		
		//Handle deleted items for supplier invoice or credit note edit
		if(invoiceHeaderPre != null 
				&& (transactionComponent.getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_SUPPLIER)
						|| transactionComponent.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_CREDIT))){
			invoiceHeaderPre.getInvoiceItems().forEach(preItem -> {
				Material material = materialRepository.getOne(preItem.getMaterial().getId());
				Optional<InvoiceItem> invoiceItem =
				transactionComponent.getInvoiceItems()
				.stream()
				.filter(ini -> ini.getId().equals(preItem.getId()))
				.findFirst()
				;
				
				//If item doesn't exist, decrease material stock
				if(!invoiceItem.isPresent()){
					Double newStock = preItem.getMaterial().getStock() - preItem.getQuantity();
					material.setStock(newStock);
					
				}
				
			});
		}
		
		status = "Success";
		
		return status;
	}

	private String dicreaseStock(InvoiceHeader transactionComponent, InvoiceHeader invoiceHeaderPre) {
		String status = "Failed";
		log.info("in dicreaseStock");
		transactionComponent.getInvoiceItems().stream()
		.forEach(invoiceItem -> {
			Material material = materialRepository.getOne(invoiceItem.getMaterial().getId());
			if(material.getSupplyType().getName().equals(ApplicationConstants.SUPPLY_TYPE_GOODS)){
				Double stockValue = invoiceItem.getQuantity();
				log.info("got material: "+material);
				//get previous invoice item
				List<InvoiceItem> tempInvoiceItems = (invoiceHeaderPre != null && invoiceHeaderPre.getInvoiceItems() != null) ? invoiceHeaderPre.getInvoiceItems().stream().filter(item -> item.getId() == invoiceItem.getId()).collect(Collectors.toList()) : null;
				InvoiceItem invoiceItemPre = null;
				if(tempInvoiceItems != null && tempInvoiceItems.size() > 0){
					invoiceItemPre = tempInvoiceItems.get(0) ;
				}
					

				if(invoiceItemPre != null){
					log.info("got prev invoice item quantity: "+invoiceItemPre.getQuantity());
					log.info("got current invoice item quantity: "+invoiceItem.getQuantity());
					//if previous quantity is more than recent one, 
					//earlier stock update should get increased to the difference value
					if(invoiceItemPre.getQuantity() > invoiceItem.getQuantity()){
						stockValue = material.getStock() +  (invoiceItemPre.getQuantity() - invoiceItem.getQuantity()) ;
						log.info("1 "+stockValue);
					}else
					//if recent quantity is more than previous quantity,
					//stock is just decreased to the difference amount
					if(invoiceItemPre.getQuantity() < invoiceItem.getQuantity()){
						stockValue = material.getStock() - (invoiceItem.getQuantity() - invoiceItemPre.getQuantity());
						log.info("2 "+stockValue);
					}
					//if no change in quantity, stock should is unchanged
					else
					if((transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) || (transactionComponent.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_DELETED))){
						stockValue = material.getStock() - invoiceItem.getQuantity() ;
						log.info("2.5 "+stockValue);
					}
					else
					{
						stockValue = material.getStock();
						log.info("3 "+stockValue);
					}
				}else{
					stockValue = (material.getStock() != null ? material.getStock() : 0) - invoiceItem.getQuantity();
				}
				
				material.setStock(stockValue);

			}
		});
		
		//Handle deleted items
		if(invoiceHeaderPre != null && invoiceHeaderPre.getInvoiceItems() != null){
			invoiceHeaderPre.getInvoiceItems().forEach(preItem -> {
				Material material = materialRepository.getOne(preItem.getMaterial().getId());
				Optional<InvoiceItem> invItem =
				transactionComponent.getInvoiceItems()
				.stream()
				.filter(ini -> ini.getId().equals(preItem.getId()))
				.findFirst()
				;
				
				//If item doesn't exist, increase material stock
				if(!invItem.isPresent()){
					Double newStock = preItem.getMaterial().getStock() + preItem.getQuantity();
					material.setStock(newStock);
					
				}
				
			});
		}
		
		status = "Success";		
		
		return status;
		
	}

	private boolean stockDecreaseApplicable(InvoiceHeader invoiceHeader) {
		//Get from confing
		//return true; // for time being
		boolean returnValue = false;
		
		GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);
		log.info("invoiceHeader.getInvoiceType().getName(): "+invoiceHeader.getInvoiceType().getName());
		log.info("ApplicationConstants.INVOICE_TYPE_CUSTOMER: "+ApplicationConstants.INVOICE_TYPE_CUSTOMER);
		log.info("globalSetting: "+globalSetting.getStockUpdateDecrease().getTransactionType().getName());
		if(invoiceHeader.getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_CUSTOMER)){
			log.info("customer invoice");
			if(globalSetting.getStockUpdateDecrease().getTransactionType().getName().equals(invoiceHeader.getInvoiceType().getName())){
				log.info("customer invoice mapped for stock decrease");
				returnValue = true;
			}
		}else if(invoiceHeader.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_CREDIT) || invoiceHeader.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_DEBIT)){
				returnValue = true;
		}
		
		return returnValue;
		
	}

	
	private boolean stockIncreaseApplicable(InvoiceHeader invoiceHeader) {
		//Get from confing
		//return true; // for time being
		boolean returnValue = false;
		
		GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);
		log.info("invoiceHeader.getInvoiceType().getName(): "+invoiceHeader.getInvoiceType().getName());
//		log.info("ApplicationConstants.INVOICE_TYPE_CUSTOMER: "+ApplicationConstants.INVOICE_TYPE_CUSTOMER);
		log.info("globalSetting: "+globalSetting.getStockUpdateIncrease().getTransactionType().getName());
		if(invoiceHeader.getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_SUPPLIER)){
			log.info("supplier invoice");
			if(globalSetting.getStockUpdateIncrease().getTransactionType().getName().equals(invoiceHeader.getInvoiceType().getName())){
				log.info("supplier invoice mapped for stock increase");
				returnValue = true;
			}
		}
		
		return returnValue;
		
	}
	
}
