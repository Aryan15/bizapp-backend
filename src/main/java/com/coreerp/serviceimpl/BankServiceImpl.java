package com.coreerp.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.BankRepository;
import com.coreerp.dto.BankDTO;
import com.coreerp.mapper.BankMapper;
import com.coreerp.model.Bank;
import com.coreerp.service.BankService;

@Service
public class BankServiceImpl implements BankService {

	@Autowired
	BankRepository bankRepository;
	
	@Autowired
	BankMapper bankMapper;
	
	@Override
	public BankDTO save(BankDTO bankDTO) {
		return bankMapper.modelToDTOMap(bankRepository.save(bankMapper.dtoToModelMap(bankDTO)));
	}

	@Override
	public BankDTO getById(Long id) {
		return bankMapper.modelToDTOMap(bankRepository.getOne(id));
	}

	@Override
	public Bank getModelById(Long id) {
		return bankRepository.getOne(id);
	}

	@Override
	public List<BankDTO> getAll() {
		return bankMapper.modelToDTOList(bankRepository.findAll());
	}

	@Override
	public Bank deleteBankById(Long id)  {
		Bank bank = bankRepository.getOne(id);
		bankRepository.delete(bank);
		return bank;
	}

}
