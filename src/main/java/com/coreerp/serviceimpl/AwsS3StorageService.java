package com.coreerp.serviceimpl;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.internal.Mimetypes;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import com.coreerp.service.StorageService;
import com.coreerp.utils.AWSUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Base64;
import java.util.stream.Stream;

@Service
@Qualifier("AwsS3StorageService")
public class AwsS3StorageService implements StorageService {
    private static Logger log = LogManager.getLogger(AwsS3StorageService.class.getName());

    private AmazonS3 s3client;

    private static String companyPrefix = "company";

    @Value("${amazonProperties.bucketName}")
    private String bucketName;

    @Autowired
    AWSUtil awsUtil;

    @Override
    public void init() {
        //
    }

    @Override
    public void store(MultipartFile multipartFile, String tenantId) {

        awsUtil.store(bucketName, companyPrefix, multipartFile, tenantId);

    }



    @Override
    public Stream<Path> loadAll() {
        return null;
    }

    @Override
    public Path load(String filename, String tenantId) {
        return null;
    }

    @Override
    public Resource loadAsResource(String filename, String tenantId) {

        return awsUtil.loadAsResource(bucketName, companyPrefix, filename, tenantId);
    }


    @Override
    public String loadAsBase64(String filename, String tenantId) {
        return awsUtil.loadAsBase64(bucketName, companyPrefix, filename, tenantId);
    }


    @Override
    public Boolean deleteFile(String fileName) {
        return awsUtil.deleteFile(bucketName, fileName);
    }



    @Override
    public void deleteAll() {

    }

    @Override
    public String getPresignedUrl(String bucketName, String fileName){
        return awsUtil.getPresignedUrl(bucketName, fileName);
    }
}
