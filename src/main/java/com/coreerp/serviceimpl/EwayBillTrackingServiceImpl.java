package com.coreerp.serviceimpl;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.EwayBillTrackingRepository;
import com.coreerp.dao.InvoiceItemRepository;
import com.coreerp.dto.EwayBillInvoiceResponseDTO;
import com.coreerp.model.EwayBillTracking;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.InvoiceItem;
import com.coreerp.service.EwayBillTrackingService;
import com.coreerp.service.InvoiceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EwayBillTrackingServiceImpl implements EwayBillTrackingService {

    @Autowired
    EwayBillTrackingRepository ewayBillTrackingRepository;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    InvoiceItemRepository invoiceItemRepository;


    final static Logger log = LogManager.getLogger(EwayBillTrackingServiceImpl.class);

    @Override
    public EwayBillTracking saveEwayBillTracking(EwayBillTracking ewayBillTracking, EwayBillInvoiceResponseDTO ewayBillInvoiceResponseDTO){

        try {
            if(ewayBillTracking.getId()==null){
                ewayBillTracking.setStatus(1);
                ewayBillTracking.setIsCurrent(1);
            }
            if(ewayBillInvoiceResponseDTO.getValidUpto()!=null){
                Date validUpto=new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(ewayBillInvoiceResponseDTO.getValidUpto());
                if(ewayBillTracking.getId()!=null){
                    ewayBillTracking.setValidUpto(validUpto);
                }

            }
            ewayBillTracking.setHsnCode("");

         ewayBillTrackingRepository.save(ewayBillTracking);
        } catch (ParseException e) {
            log.info("incorrect date format");
        }
        return ewayBillTracking;
    }

    @Override
    public EwayBillTracking updateEwayBillTracking(EwayBillTracking ewayBillTracking, EwayBillInvoiceResponseDTO ewayBillInvoiceResponseDTO){
        String strHsnCode = "";
        Integer noOfItem = 0;
        log.info("updateEwayBillTracking ***************** :  ");
        try {
            if(ewayBillInvoiceResponseDTO.getEwayBillNo() != null && ewayBillInvoiceResponseDTO.isFlag()==true){
                Date docdate=new SimpleDateFormat("dd/MM/yyyy").parse(ewayBillInvoiceResponseDTO.getDocDate());
                Date eWayBillDate= new SimpleDateFormat("dd/MM/yyyy").parse(ewayBillInvoiceResponseDTO.getEwayBillDate());
                if(ewayBillInvoiceResponseDTO.getValidUpto()!=null){
                    Date validUpto=new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(ewayBillInvoiceResponseDTO.getValidUpto());
                    ewayBillTracking.setValidUpto(validUpto);

                }

                ewayBillTracking.setDocDate(docdate);
                ewayBillTracking.seteWayBillNo(ewayBillInvoiceResponseDTO.getEwayBillNo());
                ewayBillTracking.setDocNo(ewayBillInvoiceResponseDTO.getDocNo());
                ewayBillTracking.seteWayBillDate(eWayBillDate);
                ewayBillTracking.setPdfUrl(ewayBillInvoiceResponseDTO.getPdfUrl());
                ewayBillTracking.setDetailedpdfUrl(ewayBillInvoiceResponseDTO.getDetailedpdfUrl());
                ewayBillTracking.setJsonData(ewayBillInvoiceResponseDTO.getJsonData()==null ? "" : ewayBillInvoiceResponseDTO.getJsonData().toString());
                ewayBillTracking.setStatus(ApplicationConstants.EWAY_BILL_ACTIVE);

            }else{
                ewayBillTracking.setStatus(ApplicationConstants.EWAY_BILL_FAILED);
                ewayBillTracking.setError(ewayBillInvoiceResponseDTO.getError_log_ids());
            }
            ewayBillTracking.setResponse(ewayBillInvoiceResponseDTO.getMessage());
            ewayBillTracking.setIsCurrent(1);
            noOfItem = Math.toIntExact(invoiceItemRepository.findCountByHeaderId(ewayBillTracking.getInvoiceHeader().getId()));
            List<InvoiceItem> invoiceItems = invoiceItemRepository.findByHeaderId(ewayBillTracking.getInvoiceHeader().getId());
            if(invoiceItems != null ){
                log.info("invoiceItems ***************** :  "+invoiceItems);
                for (InvoiceItem invoiceItem : invoiceItems) {
                    if(invoiceItem.getMaterial().getHsnCode() != null){
                        strHsnCode = strHsnCode + ( invoiceItem.getMaterial().getHsnCode() == null ? "" : "," +invoiceItem.getMaterial().getHsnCode());
                    }
                }
            }
            log.info("strHsnCode ***************** :  "+strHsnCode);
            if(strHsnCode !=null && strHsnCode != ""){
                strHsnCode= strHsnCode.substring(1, strHsnCode.length()-1);
            }
            ewayBillTracking.setNoOfItems(noOfItem);
            ewayBillTracking.setHsnCode(strHsnCode);
            ewayBillTrackingRepository.save(ewayBillTracking);

        } catch (ParseException e) {
            log.info("incorrect date format");
        }


        return ewayBillTracking;
    }

    @Override
    public EwayBillTracking updateEwayBillTrackingAfterCancel(EwayBillTracking ewayBillTracking){

             ewayBillTracking.setStatus(3);
             ewayBillTracking.setCancelledDate(new Date());
             ewayBillTrackingRepository.save(ewayBillTracking);

        return ewayBillTracking;
    }

    @Override
    public List<EwayBillTracking> getByInvoiceHeader(InvoiceHeader invoiceHeader) {
        List<EwayBillTracking>  ewayBillTrackingList = ewayBillTrackingRepository.findByInvoiceHeader(invoiceHeader);
        return ewayBillTrackingList;
    }

    @Override
    public List<EwayBillTracking> getEwayBillDetailsByInvoiceHeaderAndStatus(String invoiceHeaderId) {
        InvoiceHeader invoiceHeader = invoiceService.getModelById(invoiceHeaderId);
        ArrayList<Integer> statusId = new ArrayList<Integer>();
        statusId.add(ApplicationConstants.EWAY_BILL_INITIAL_RECORD);
        statusId.add(ApplicationConstants.EWAY_BILL_FAILED);
        return ewayBillTrackingRepository.findByInvoiceHeaderAndStatusNotIn(invoiceHeader, statusId);
    }


    @Override
    public EwayBillTracking getByEWayBillNo(String eWayBillNum) {
        log.info("EwayBillTracking eway :  "+ewayBillTrackingRepository.findByEWayBillNo(eWayBillNum));

        return ewayBillTrackingRepository.findByEWayBillNo(eWayBillNum);
    }

    @Override
    public EwayBillTracking getActiveEwayBillByInvoiceHeaderAndStatus(String invoiceHeaderId, ArrayList<Integer> statusId) {
        InvoiceHeader invoiceHeader = invoiceService.getModelById(invoiceHeaderId);

        return ewayBillTrackingRepository.findByInvoiceHeaderAndIsCurrentAndStatusIn(invoiceHeader,1,statusId);
    }

    @Override
    public EwayBillTracking getActiveEwayBillByInvoiceHeader(String invoiceHeaderId) {
        InvoiceHeader invoiceHeader = invoiceService.getModelById(invoiceHeaderId);

        return ewayBillTrackingRepository.findByInvoiceHeaderAndIsCurrent(invoiceHeader,1);

    }


}
