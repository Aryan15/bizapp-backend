package com.coreerp.serviceimpl;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;
import com.coreerp.service.StatusService;
import com.coreerp.service.TransactionStatusService;
import com.coreerp.service.TransactionTypeService;

@Component
public class InvoiceStatusService implements TransactionStatusService<InvoiceHeader> {

	final static Logger log = LogManager.getLogger(InvoiceStatusService.class);

	@Autowired
	StatusService statusService;

	@Autowired
	TransactionTypeService transactionTypeService;

	@Override
	public Status getStatus(InvoiceHeader in) {
		/*
		 * 
		 * ** derive status as New if incoming id is null and/or status is null/
		 * 'New' ** derive status as Open if incoming id is not null and status
		 * != 'Cancelled'
		 * 
		 * ** derive status as Cancelled if incoming id is not null/ 'New' and
		 * status = 'Cancelled'
		 * 
		 * ** validate if the deleting invoice is the latest in the series **
		 * derive status as Deleted if incoming id is not null and status =
		 * 'Deleted'
		 */
		Status status = new Status();

		log.info("Status derivation parameters....");
		log.info("in.getId: " + in.getId());
		log.info("in.getInvoiceStatus(): " + in.getStatus());

		String inStatus = in.getStatus() != null ? in.getStatus().getName() : null;

		TransactionType transactionType = transactionTypeService.getModelById(in.getInvoiceType().getId());

		if (transactionType.getName().equals(ApplicationConstants.INVOICE_TYPE_CUSTOMER)
				|| transactionType.getName().equals(ApplicationConstants.INVOICE_TYPE_SUPPLIER)) {

			if ((in.getId() == null || in.getId().isEmpty())) { // ||
																// inStatus.equals(ApplicationConstants.INVOICE_STATUS_NEW)
				log.info("1");
				status = statusService.getStatusByTransactionTypeAndName(transactionType,
						ApplicationConstants.INVOICE_STATUS_NEW);
			} else if ((in.getId() != null && !in.getId().isEmpty())
					&& !inStatus.equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {
				log.info("2");
				// status =
				// statusService.getStatusByTransactionTypeAndName(transactionType,
				// ApplicationConstants.INVOICE_STATUS_OPEN);
				status = statusService.getStatusByTransactionTypeAndName(transactionType,
						ApplicationConstants.INVOICE_STATUS_NEW);
			} else if ((in.getId() != null && !in.getId().isEmpty())
					&& inStatus.equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {
				log.info("3");
				status = statusService.getStatusByTransactionTypeAndName(transactionType,
						ApplicationConstants.INVOICE_STATUS_CANCELLED);
			} else if ((in.getId() != null && !in.getId().isEmpty())
					&& inStatus.equals(ApplicationConstants.INVOICE_STATUS_DELETED)) {
				log.info("4");
				status = statusService.getStatusByTransactionTypeAndName(transactionType,
						ApplicationConstants.INVOICE_STATUS_DELETED);
			} else if ((in.getId() != null && !in.getId().isEmpty())
					&& (inStatus.equals(ApplicationConstants.INVOICE_STATUS_NEW)
							|| inStatus.equals(ApplicationConstants.INVOICE_STATUS_OPEN)
							|| inStatus.equals(ApplicationConstants.INVOICE_STATUS_PARTIAL) && (in.getDueAmount() != 0)
									&& (in.getDueAmount() != in.getGrandTotal()))) {
				log.info("5");
				status = statusService.getStatusByTransactionTypeAndName(transactionType,
						ApplicationConstants.INVOICE_STATUS_PARTIAL);
			} else if ((in.getId() != null && !in.getId().isEmpty()) && (inStatus
					.equals(ApplicationConstants.INVOICE_STATUS_NEW)
					|| inStatus.equals(ApplicationConstants.INVOICE_STATUS_OPEN)
					|| inStatus.equals(ApplicationConstants.INVOICE_STATUS_PARTIAL) && (in.getDueAmount() == 0))) {
				log.info("5");
				status = statusService.getStatusByTransactionTypeAndName(transactionType,
						ApplicationConstants.INVOICE_STATUS_COMPLETED);
			} else {
				log.info("6");
				// status =
				// statusService.getStatusByTransactionTypeAndName(transactionType,
				// ApplicationConstants.INVOICE_STATUS_OPEN);
				status = statusService.getStatusByTransactionTypeAndName(transactionType,
						ApplicationConstants.INVOICE_STATUS_NEW);
			}
			log.info("out status: " + status);
		} else {
			if (in.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_CREDIT)
					|| in.getInvoiceType().getName().equals(ApplicationConstants.NOTE_TYPE_DEBIT)) {
				
				log.info("1");
				// status =
				// statusService.getStatusByTransactionTypeAndName(transactionType,
				// ApplicationConstants.NOTE_STATUS_NEW);

				if ((in.getId() == null || in.getId().isEmpty())) { // ||
					// inStatus.equals(ApplicationConstants.INVOICE_STATUS_NEW)
					log.info("1");
					status = statusService.getStatusByTransactionTypeAndName(transactionType,
							ApplicationConstants.NOTE_STATUS_NEW);
				} else if ((in.getId() != null && !in.getId().isEmpty())
						&& !inStatus.equals(ApplicationConstants.NOTE_STATUS_CANCELLED)) {
					log.info("2");
					// status =
					// statusService.getStatusByTransactionTypeAndName(transactionType,
					// ApplicationConstants.INVOICE_STATUS_OPEN);
					status = statusService.getStatusByTransactionTypeAndName(transactionType,
							ApplicationConstants.NOTE_STATUS_NEW);
				} else if ((in.getId() != null && !in.getId().isEmpty())
						&& inStatus.equals(ApplicationConstants.NOTE_STATUS_CANCELLED)) {
					log.info("3");
					status = statusService.getStatusByTransactionTypeAndName(transactionType,
							ApplicationConstants.NOTE_STATUS_CANCELLED);
				} else if ((in.getId() != null && !in.getId().isEmpty())
						&& inStatus.equals(ApplicationConstants.NOTE_STATUS_DELETED)) {
					log.info("4");
					status = statusService.getStatusByTransactionTypeAndName(transactionType,
							ApplicationConstants.NOTE_STATUS_DELETED);
				} else if ((in.getId() != null && !in.getId().isEmpty())
						&& (inStatus.equals(ApplicationConstants.NOTE_STATUS_NEW)
								|| inStatus.equals(ApplicationConstants.NOTE_STATUS_OPEN)
								|| inStatus.equals(ApplicationConstants.NOTE_STATUS_PARTIAL)
										&& (in.getDueAmount() != 0) && (in.getDueAmount() != in.getGrandTotal()))) {
					log.info("5");
					status = statusService.getStatusByTransactionTypeAndName(transactionType,
							ApplicationConstants.NOTE_STATUS_PARTIAL);
				} else if ((in.getId() != null && !in.getId().isEmpty()) && (inStatus
						.equals(ApplicationConstants.NOTE_STATUS_NEW)
						|| inStatus.equals(ApplicationConstants.NOTE_STATUS_OPEN)
						|| inStatus.equals(ApplicationConstants.NOTE_STATUS_PARTIAL) && (in.getDueAmount() == 0))) {
					log.info("5");
					status = statusService.getStatusByTransactionTypeAndName(transactionType,
							ApplicationConstants.NOTE_STATUS_COMPLETED);
				} else {
					log.info("6");
					// status =
					// statusService.getStatusByTransactionTypeAndName(transactionType,
					// ApplicationConstants.INVOICE_STATUS_OPEN);
					status = statusService.getStatusByTransactionTypeAndName(transactionType,
							ApplicationConstants.NOTE_STATUS_NEW);
				}
				log.info("out status: " + status);

				
				
			} else {
				if ((in.getId() == null || in.getId().isEmpty())
						|| inStatus.equals(ApplicationConstants.PROFORMA_INVOICE_STATUS_NEW)) { // ||
																								// inStatus.equals(ApplicationConstants.INVOICE_STATUS_NEW)
					log.info("1");
					status = statusService.getStatusByTransactionTypeAndName(transactionType,
							ApplicationConstants.PROFORMA_INVOICE_STATUS_NEW);
				} else {
					log.info("2");
					status = statusService.getStatusByTransactionTypeAndName(transactionType,
							ApplicationConstants.INVOICE_STATUS_CANCELLED);
				}
			}

		}
		return status;
	}

}
