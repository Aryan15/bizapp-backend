package com.coreerp.serviceimpl;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.*;
import com.coreerp.dto.PettyCashHeaderDTO;
import com.coreerp.dto.PettyCashWrapperDTO;


import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.PettyCashHeaderMapper;
import com.coreerp.mapper.PettyCashItemMapper;
import com.coreerp.model.*;
import com.coreerp.service.PettyCashService;
import com.coreerp.service.StatusService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PettyCashServiceImpl implements PettyCashService {

    final static Logger log = LogManager.getLogger(PettyCashServiceImpl.class);

    @Autowired
    CategoryRepository categoryRepository;



    @Autowired
    PettyCashService pettyCashService;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    PettyCashRepository pettyCashRepository;

    @Autowired
    PettyCashHeaderMapper pettyCashHeaderMapper;

    @Autowired
    PettyCashItemRepository pettyCashItemRepository;

    @Autowired
    PettyCashItemMapper pettyCashItemMapper;

    @Autowired
    PettyCashHeaderAuditRepository pettyCashHeaderAuditRepository;

    @Autowired
    PettyCashItemAuditRepository pettyCashItemAuditRepository;

    @Autowired
    StatusService statusService;


    @Override
    public Category saveCategory(Category categoryIn) {

        return categoryRepository.save(categoryIn);
    }

    @Override
    public Tag saveTag(Tag tagIn) {

        return tagRepository.save(tagIn);
    }

   @Override
 public  Long countByPettyCashAndTransactionType(String pettyCashNumber, TransactionType id){
    return pettyCashRepository.countByPettyCashNumberAndPettyCashTypeId(pettyCashNumber, id);
 }

    @Override
    public PettyCashHeaderDTO savePettyCash(PettyCashWrapperDTO pettyCashWrapperDTO) {


        PettyCashHeaderDTO PettyCashHeaderDTOIn = pettyCashWrapperDTO.getPettyCashHeader();

        PettyCashHeader pettyCashHeader = pettyCashHeaderMapper.dtoToModelMap(PettyCashHeaderDTOIn);




        if(pettyCashHeader.getId()==null){
              pettyCashHeader.setIsLatest(1l);
       PettyCashHeader pettyCashHeaderOut = pettyCashRepository.getPettyCashHeader(1l);

              if (pettyCashHeaderOut != null) {
                 pettyCashHeaderOut.setIsLatest(0l);
                 PettyCashHeader pettyCashHeaderOutSave = pettyCashRepository.save(pettyCashHeaderOut);
           }
             }

        //Get older Voucher if exists

        PettyCashHeader pettyCashHeaderPre = new PettyCashHeader();
        List<PettyCashItem> pettyCashItemsPre = new ArrayList<PettyCashItem>();

        if(PettyCashHeaderDTOIn.getId() != null){
            pettyCashRepository.getOne(PettyCashHeaderDTOIn.getId()).getPettyCashItems().forEach(item -> {
                PettyCashItem pettyCashItemPre = new PettyCashItem();
                pettyCashItemPre.setId(item.getId());

                pettyCashItemsPre.add(pettyCashItemPre);
            });
        }



        pettyCashHeaderPre.setPettyCashItems(pettyCashItemsPre);


        if(pettyCashWrapperDTO.getItemToBeRemove().size() > 0){
            pettyCashWrapperDTO.getItemToBeRemove().forEach(id -> {

                deleteItem(id);
            });
        }



        List<PettyCashItem> pettyCashItems = pettyCashItemMapper.dtoToModelList(PettyCashHeaderDTOIn.getPettyCashItems());



        //set header to items
        pettyCashItems.stream().forEach(pettyCashItem -> pettyCashItem.setPettyCashHeader(pettyCashHeader));

        //set items to header
        pettyCashHeader.setPettyCashItems(pettyCashItems);




        pettyCashHeader.getPettyCashItems().forEach(item -> {
            log.info("item before save:--- "+item.getId());
        });

        //save
        PettyCashHeader pettyCashHeaderOut = pettyCashRepository.save(pettyCashHeader);

        pettyCashHeaderOut.getPettyCashItems().forEach(item -> {
            log.info("item after save:--- "+item.getId());
        });



        return pettyCashHeaderMapper.modelToDTOMap(pettyCashHeaderOut);
    }

    @Override
    public TransactionResponseDTO deleteItem(String id) {
        TransactionResponseDTO res = new TransactionResponseDTO();
        res.setResponseStatus(1);
        String deleteStatus = "Grn Item Deleted Successfully";
        try {

            PettyCashItem pettyCashItemDelete = pettyCashItemRepository.getOne(id);







            pettyCashItemRepository.deleteForce(pettyCashItemDelete.getId());


        }catch(Exception e){
            deleteStatus = "Cannot Delete Grn Item";
            res.setResponseStatus(0);
            log.error("Error: "+e);
        }

        res.setResponseString(deleteStatus);
        return res;
    }



    @Override
    public TransactionResponseDTO delete(String id) {
        TransactionResponseDTO res = new TransactionResponseDTO();

        String deleteStatus = "Pettycash Deleted Successfully";
        Integer deleteStatusNum = 1;
        log.info("1");

        try {


            PettyCashHeader pettyCashDelete = pettyCashRepository.getOne(id);

            auditEntry(pettyCashDelete);


            log.info("2");
            //voucherDebug = voucherHeaderRepository.getOne(id);
            pettyCashDelete.getPettyCashItems().forEach(item -> {
                pettyCashItemRepository.delete(item);
            });

            pettyCashRepository.delete(pettyCashDelete);


        }catch(Exception e){
            deleteStatus = "Cannot Delete PetttyCash";
            deleteStatusNum = 0;
            log.info("exception: "+e);
            deleteAuditEntry(id);
            //voucherDebug = voucherHeaderRepository.getOne(id);

        }

        res.setResponseString(deleteStatus);
        res.setResponseStatus(deleteStatusNum);

        //dchDebug = deliveryChallanHeaderRepository.getOne(id);
        //log.info(dchDebug.getStatus().getName());

        return res;
    }

    private void deleteAuditEntry(String headerId){

        PettyCashHeaderAudit header = pettyCashHeaderAuditRepository.getOne(headerId);

        if(header != null ){
            List<PettyCashItemAudit> items = pettyCashItemAuditRepository.findByPettyCashHeader(headerId);
            if(items != null && items.size() > 0){
                items.forEach(item -> {
                    pettyCashItemAuditRepository.delete(item);
                });
            }

            pettyCashHeaderAuditRepository.delete(header);
        }

    }


    private void auditEntry(PettyCashHeader pettyCashHeaderDelete){

        PettyCashHeaderAudit header = pettyCashHeaderMapper.modelToAudit(pettyCashHeaderDelete);

        pettyCashHeaderAuditRepository.save(header);

        List<PettyCashItemAudit> items = pettyCashItemMapper.modelToAuditList(pettyCashHeaderDelete.getPettyCashItems());

        items.forEach(item -> {
            pettyCashItemAuditRepository.save(item);
        });

    }

    @Override
    public Long getMaxPettyCashNumberByTransactionType(Long transactionTypeId) {
        return pettyCashRepository.getMaxPettyCashNumberByTransactionType(transactionTypeId);

    }

    @Override
    public PettyCashHeader getPettyCashByHeaderId(String pettyCashId) {


        PettyCashHeader pettyCashHeader = pettyCashRepository.getOne(pettyCashId);


      return pettyCashHeader;
    }


    @Override
    public PettyCashHeaderDTO approvePettyCash(String headerId){
        PettyCashHeader pettyCashHeader = pettyCashService.getPettyCashByHeaderId(headerId);
        Optional<String> username = Optional.of(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        String user= username.toString();
        pettyCashHeader.setStatus(statusService.getStatusById(ApplicationConstants.COMPLETED_STATUS));
        pettyCashHeader.setIsDayClosed(1L);
        pettyCashHeader.setApprovedBy(user);
        pettyCashHeader.setApprovedDate(new Date());
        pettyCashRepository.save(pettyCashHeader);
        PettyCashHeaderDTO pettyCashHeaderDTO=  pettyCashHeaderMapper.modelToDTOMap(pettyCashHeader);
        return pettyCashHeaderDTO;
    }

}
