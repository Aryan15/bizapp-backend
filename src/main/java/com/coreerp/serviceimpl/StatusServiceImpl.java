package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.StatusRepository;
import com.coreerp.dto.StatusDTO;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;
import com.coreerp.service.StatusService;

@Service
public class StatusServiceImpl implements StatusService {

	final static Logger log=LogManager.getLogger(StatusServiceImpl.class);
	
	@Autowired
	StatusRepository statusRepository;
			
    @Override
	public Status saveState(StatusDTO statusDTO) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Status getStatusById(Long id) {
		// TODO Auto-generated method stub
		return statusRepository.getOne(id);
	}

	@Override
	public List<StatusDTO> getAllStatus() {
		List<StatusDTO> statusDTOList=new ArrayList<StatusDTO>();
		List<Status> statusList= statusRepository.findAll();
		for(Status status:statusList){
			StatusDTO statusDTO=new StatusDTO();
			statusDTO.setId(status.getId());
			statusDTO.setName(status.getName());
			statusDTO.setDeleted(status.getDeleted());
			statusDTOList.add(statusDTO);
		}
		
		return statusDTOList;
	}


	@Override
	public Status getStatusByName(String name) {
		return statusRepository.findByName(name);
	}

	@Override
	public Status getStatusByTransactionTypeAndName(TransactionType transactionType, String name){
		
		return statusRepository.findByTransactionTypeAndName(transactionType, name);
				
	}
}
