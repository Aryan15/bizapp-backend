package com.coreerp.serviceimpl;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.InvoiceHeaderRepository;
import com.coreerp.dao.QuotationHeaderRepository;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.model.*;
import com.coreerp.multitenancy.TenantContext;
import com.coreerp.service.CompanyService;
import com.coreerp.service.GlobalSettingService;
import com.coreerp.service.PrintCopiesService;
import com.coreerp.service.StorageService;
import com.coreerp.utils.AWSUtil;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Parameters;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class JasperReportServiceImpl {
    private static Logger logger = LogManager.getLogger(JasperReportServiceImpl.class.getName());

    private static String reportPrefix = "reports/";

    private static String customReportPrefix = "company/";

    private static String classic1SubReport = "tax_sub_report.jasper";

    private static String classic2SubReport = "tax_sub_report2.jasper";

    private static String subReportDownloadDirectory = System.getProperty("java.io.tmpdir"); //"/tmp";

    @Value("${spring.datasource.username}")
    private String dbUsername;

    @Value("${spring.datasource.password}")
    private String dbPassword;

    @Value("${report.path}")
    private String reportPath;

    @Value("${amazonProperties.reports.bucketName}")
    private String reportBucketName;

    @Value("${db.url.jasper}")
    private String jasperDBUrl;

    @Autowired
    PrintCopiesService printCopiesService;

    @Autowired
    CompanyService companyService;

    @Autowired
    TransactionTypeRepository transactionTypeRepository;

    @Autowired
    GlobalSettingService globalSettingService;

    @Autowired
    InvoiceHeaderRepository invoiceHeaderRepository;

    @Autowired
    QuotationHeaderRepository quotationHeaderRepository;

    @Autowired
    @Qualifier("AwsS3StorageService")
    StorageService storageService;

    @Autowired
    AWSUtil awsUtil;

    public ByteArrayInputStream generateReport(
            String reportName
            , String id
            , String copyText
            , String amountInWords
            ,String printHeaderText
            ,Long signatureRequired
            ,Integer allowPartyCode
            ,Long allowShipingAddress
            , HttpServletRequest request
    ) throws JRException, SQLException {

        String reportJName =  reportPrefix + getReportName(id, reportName);
        String tenantId = companyService.getTenantFromRequest(request);
        String customReportJname = customReportPrefix + tenantId + "/" + getReportName(id, reportName);
        String fullSubreportDownloadDirectory = subReportDownloadDirectory +  tenantId;
        Boolean customExists = awsUtil.doesObjectExists(reportBucketName, customReportJname);

        logger.info("is custom report? "+ customExists);
        String jasperFileUrl = "";
        if(customExists){
            jasperFileUrl = storageService.getPresignedUrl(reportBucketName, customReportJname);
            String classic1SubreportFile = customReportPrefix + tenantId + "/" + classic1SubReport;
            logger.info("downloading subreport..."+classic1SubreportFile);
            awsUtil.downloadObjectAsFile(fullSubreportDownloadDirectory, reportBucketName, classic1SubreportFile,classic1SubReport);

            String classic2SubreportFile = customReportPrefix + tenantId + "/" + classic2SubReport;
            logger.info("downloading subreport..."+classic2SubreportFile);
            awsUtil.downloadObjectAsFile(fullSubreportDownloadDirectory, reportBucketName, classic2SubreportFile,classic2SubReport);
        }else{
            jasperFileUrl = storageService.getPresignedUrl(reportBucketName, reportJName);
        }
        URL jasperFileURL = null;
        try {
            jasperFileURL = new URL(jasperFileUrl);
        }catch (MalformedURLException e){
            logger.error("incorrect url: "+e);
        }
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperFileURL);
        List<JasperPrint> jasperPrintList = new ArrayList<>();

        String companyLogoName = companyService.getCompanyLogo(1l, request);
        String secondImage = companyService.getCompanySecondImage(1l, request);
        String signatureImage = companyService.getCompanySignatureImage(1l, request);
        logger.info("companyLogoName : "+companyLogoName);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("my_inv_id", id); //"3738db09-b4d1-4261-9087-90859f035c89"
        parameters.put("image_path", companyLogoName); //"C:\\Uthkrushta\\EventzAlley\\images\\pexels-photo-1047349.jpeg"); //"3738db09-b4d1-4261-9087-90859f035c89"
        parameters.put("second_image_path", secondImage);
        parameters.put("print_copy", copyText);
        parameters.put("amountInWords", amountInWords);

        logger.info("reportPath: "+reportPath);

        if(customExists){
            parameters.put("SUBREPORT_DIR", fullSubreportDownloadDirectory +"/");
        }else {
            parameters.put("SUBREPORT_DIR", reportPath);
        }
        if(signatureRequired==1) {
            parameters.put("signature_image_path", signatureImage);
        }
         if(allowShipingAddress!=null) {
             parameters.put("allow_shiping_address", allowShipingAddress);
         }

        parameters.put("print_header_text",printHeaderText);
        parameters.put("allow_party_code", allowPartyCode);
        logger.info("Calling Jasper");
        String url = jasperDBUrl+ TenantContext.getCurrentTenant()+"?useSSL=false";
        Connection c = DriverManager.getConnection(url,dbUsername,dbPassword);

        if(copyText.equals(ApplicationConstants.PRINT_ALL)){
            List<PrintCopy> pc = printCopiesService.getAll();
            pc.forEach(pCopy -> {
                logger.info("Printing..."+pCopy.getName());
                parameters.replace("print_copy", pCopy.getName());
                try {
                    JasperPrint jasperPrint = JasperFillManager.fillReport(
                            jasperReport, parameters, c);
                    jasperPrintList.add(jasperPrint);
                } catch (JRException e) {
                    e.printStackTrace();
                }

            });
        }else{

            JasperPrint jasperPrint = JasperFillManager.fillReport(
                    jasperReport, parameters, c);
            jasperPrintList.add(jasperPrint);
        }




        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        exporter.setConfiguration(configuration);
        exporter.exportReport();

        logger.info("outputStream: "+outputStream.size());
        ByteArrayInputStream in = new ByteArrayInputStream(outputStream.toByteArray());

        logger.info("Got report: "+in.available());
        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (c != null) {
            try {
                //c.rollback();
                c.close();
                logger.info("connection closed");
            } catch (SQLException e) {
                //nothing to do
                logger.info("Sql exception "+e);
            }
        }

        return in;

    }


    public ByteArrayInputStream generateExcelReport(
            String reportName
            , String id
            , String copyText
            , String amountInWords
            , String printHeaderText
            , HttpServletRequest request
    ) throws JRException, SQLException {

        String reportJName = reportPrefix + getReportName(id, reportName);


        String jasperFileUrl = storageService.getPresignedUrl(reportBucketName, reportJName);
        URL jasperFileURL = null;
        try {
            jasperFileURL = new URL(jasperFileUrl);
        }catch (MalformedURLException e){
            logger.error("incorrect url: "+e);
        }
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperFileURL);

        List<JasperPrint> jasperPrintList = new ArrayList<>();

        String companyLogoName = companyService.getCompanyLogo(1l, request);
        String secondImage = companyService.getCompanySecondImage(1l, request);
        String signatureImage = companyService.getCompanySignatureImage(1l, request);

        logger.info("companyLogoName : "+companyLogoName);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("my_inv_id", id); //"3738db09-b4d1-4261-9087-90859f035c89"
        parameters.put("image_path", companyLogoName); //"C:\\Uthkrushta\\EventzAlley\\images\\pexels-photo-1047349.jpeg"); //"3738db09-b4d1-4261-9087-90859f035c89"
        parameters.put("second_image_path", secondImage);
        parameters.put("print_copy", copyText);
        parameters.put("amountInWords", amountInWords);
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("print_header_text",printHeaderText);
        parameters.put("signature_image_path", signatureImage);

        logger.info("Calling Jasper");
        String url = jasperDBUrl+ TenantContext.getCurrentTenant()+"?useSSL=false";
        Connection c = DriverManager.getConnection(url,dbUsername,dbPassword);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        if(copyText.equals(ApplicationConstants.PRINT_ALL)){
            List<PrintCopy> pc = printCopiesService.getAll();
            pc.forEach(pCopy -> {
                logger.info("Printing..."+pCopy.getName());
                parameters.replace("print_copy", pCopy.getName());
                try {
                    JasperPrint jasperPrint = JasperFillManager.fillReport(
                            jasperReport, parameters, c);
                    jasperPrintList.add(jasperPrint);

                    excelExport(jasperPrint, outputStream );

                } catch (JRException e) {
                    e.printStackTrace();
                }

            });
        }else{

            JasperPrint jasperPrint = JasperFillManager.fillReport(
                    jasperReport, parameters, c);
            jasperPrintList.add(jasperPrint);

            excelExport(jasperPrint, outputStream );
        }




        logger.info("outputStream: "+outputStream.size());
        ByteArrayInputStream in = new ByteArrayInputStream(outputStream.toByteArray());

        logger.info("Got report: "+in.available());
        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (c != null) {
            try {
                //c.rollback();
                c.close();
                logger.info("connection closed");
            } catch (SQLException e) {
                //nothing to do
                logger.info("Sql exception "+e);
            }
        }

        return in;

    }






    public ByteArrayInputStream generateWordReport(
            String reportName
            , String id
            , String copyText
            , String amountInWords
            , String printHeaderText
            , HttpServletRequest request
    ) throws JRException, SQLException {

        String reportJName = reportPrefix + getReportName(id, reportName);

        String jasperFileUrl = storageService.getPresignedUrl(reportBucketName, reportJName);
        URL jasperFileURL = null;
        try {
            jasperFileURL = new URL(jasperFileUrl);
        }catch (MalformedURLException e){
            logger.error("incorrect url: "+e);
        }
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperFileURL);

        List<JasperPrint> jasperPrintList = new ArrayList<>();

        String companyLogoName = companyService.getCompanyLogo(1l, request);
        String secondImage = companyService.getCompanySecondImage(1l, request);
        String signatureImage = companyService.getCompanySignatureImage(1l, request);

        logger.info("companyLogoName : "+companyLogoName);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("my_inv_id", id); //"3738db09-b4d1-4261-9087-90859f035c89"
        parameters.put("image_path", companyLogoName); //"C:\\Uthkrushta\\EventzAlley\\images\\pexels-photo-1047349.jpeg"); //"3738db09-b4d1-4261-9087-90859f035c89"
        parameters.put("second_image_path", secondImage);
        parameters.put("print_copy", copyText);
        parameters.put("amountInWords", amountInWords);
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("print_header_text",printHeaderText);
        parameters.put("signature_image_path", signatureImage);

        logger.info("Calling Jasper");
        String url = jasperDBUrl+ TenantContext.getCurrentTenant()+"?useSSL=false";
        Connection c = DriverManager.getConnection(url,dbUsername,dbPassword);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        if(copyText.equals(ApplicationConstants.PRINT_ALL)){
            List<PrintCopy> pc = printCopiesService.getAll();
            pc.forEach(pCopy -> {
                logger.info("Printing..."+pCopy.getName());
                parameters.replace("print_copy", pCopy.getName());
                try {
                    JasperPrint jasperPrint = JasperFillManager.fillReport(
                            jasperReport, parameters, c);
                    jasperPrintList.add(jasperPrint);

                    JRDocxExporter exporter = new JRDocxExporter();
                    exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));


                    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
                    SimpleDocxExporterConfiguration configuration = new SimpleDocxExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();

                } catch (JRException e) {
                    e.printStackTrace();
                }

            });
        }else{

            JasperPrint jasperPrint = JasperFillManager.fillReport(
                    jasperReport, parameters, c);
            jasperPrintList.add(jasperPrint);
            JRDocxExporter exporter = new JRDocxExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));


            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
            SimpleDocxExporterConfiguration configuration = new SimpleDocxExporterConfiguration();
            exporter.setConfiguration(configuration);
            exporter.exportReport();

        }




        logger.info("outputStream: "+outputStream.size());
        ByteArrayInputStream in = new ByteArrayInputStream(outputStream.toByteArray());

        logger.info("Got report: "+in.available());
        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (c != null) {
            try {
                //c.rollback();
                c.close();
                logger.info("connection closed");
            } catch (SQLException e) {
                //nothing to do
                logger.info("Sql exception "+e);
            }
        }

        return in;

    }








    private String getReportName(String id, String reportName){
        String reportJName = reportName + ".jasper";
        GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);

        if(reportName.contains("Invoice")) {

            InvoiceHeader invoiceHeader = invoiceHeaderRepository.findByHeaderId(id);
            if (invoiceHeader != null){

                if (globalSetting.getItemLevelTax() == 1) {
                    reportJName = reportName + "_Item.jasper";
                }


            if (invoiceHeader.getInvoiceType().getName() != null && (invoiceHeader.getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_INCOMING_JW_INVOICE)
                    || invoiceHeader.getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE))) {
                if (isItemLevelInvoice(invoiceHeader) || reportName.equals("InvoiceTaxGroup") || reportName.equals("InvoiceTaxGroup2")) {
                    reportJName = reportName + "_Item.jasper";
                } else {
                    reportJName = reportName + ".jasper";
                }
            }
        }
        } else if(reportName.contains("Quotation")){


            if (globalSetting.getItemLevelTax() == 1) {
                reportJName = reportName + "_Item.jasper";
            }else{
                reportJName = reportName + ".jasper";
            }

        }

        return reportJName;
    }

    public ByteArrayInputStream getReport(
            Long tTypeId,
            Long counterTTypeId,
            String jasperReportName,
            Integer pageNumber,
            Integer pageSize,
            String sortDir,
            String sortColumn,
            TransactionReportRequestDTO reportRequestDTO,
            String reportType
    ) throws SQLException, JRException {
        String reportName = reportPrefix + jasperReportName + ".jasper";//getReportNameForTransactionType(tTypeId, counterTTypeId);

        logger.info("Report name: "+reportName);
        if(reportName.equals("INVALID.jasper")){
            logger.info("Incorrect file name");
            return null;
        }

        String jasperFileUrl = storageService.getPresignedUrl(reportBucketName, reportName);
        URL jasperFileURL = null;
        try {
            jasperFileURL = new URL(jasperFileUrl);
        }catch (MalformedURLException e){
            logger.error("incorrect url: "+e);
        }
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperFileURL);
        logger.info("FromDate "+ reportRequestDTO.getTransactionFromDate());
        logger.info("ToDate "+reportRequestDTO.getTransactionToDate());
        Map<String, Object> parameters = new HashMap<>();

        parameters.put("v_limit", 5000); //Pagination needs lot of work. it is not that simple
        parameters.put("v_offset", 0);
        parameters.put("FromDate", reportRequestDTO.getTransactionFromDate());
        parameters.put("ToDate", reportRequestDTO.getTransactionToDate());
        parameters.put("t_type_id", tTypeId);

        parameters.put("financial_year_id", reportRequestDTO.getFinancialYearId());
        parameters.put("material_id", reportRequestDTO.getMaterialId());
        parameters.put("party_id", reportRequestDTO.getPartyId());
        parameters.put("counter_t_type_id", counterTTypeId);

        parameters.put("party_type_id",tTypeId);
        parameters.put("created_by",reportRequestDTO.getCreatedBy());

        if(tTypeId==1){
            parameters.put("transaction_name","Accounts Receivable Schedule");

            parameters.put("transaction_type_id1","1");
            parameters.put("transaction_type_id2","15");
        }
        else if(tTypeId==5){
            parameters.put("transaction_name","Accounts Payable Schedule");
            parameters.put("transaction_type_id1","5");
            parameters.put("transaction_type_id2","18");

        }



        parameters.put("SUBREPORT_DIR", reportPath);
        logger.info("Calling Jasper");
        String url = jasperDBUrl+ TenantContext.getCurrentTenant()+"?useSSL=false";
        Connection c = DriverManager.getConnection(url,dbUsername,dbPassword);
        JasperPrint jasperPrint = JasperFillManager.fillReport(
                jasperReport, parameters, c);



        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        if(reportType.equals("PDF")){
            JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
        }else{
            excelExport(jasperPrint, outputStream );
        }


        logger.info("jasperPrint.getPages(): "+jasperPrint.getPages().size());

        ByteArrayInputStream in = new ByteArrayInputStream(outputStream.toByteArray());

        logger.info("Got report: "+in.available());

        if (c != null) {
            try {
                //c.rollback();
                c.close();
                logger.info("connection closed");
            } catch (SQLException e) {
                //nothing to do
                logger.info("Sql exception "+e);
            }
        }


        return in;
    }








    public String getReportNameForTransactionType(Long tTypeId, Long counterTTypeId){


        String reportName = "";


        if(tTypeId == 13 && counterTTypeId == 0){
            reportName = "Jobwork-InDC";
        }else if(tTypeId == 14 && counterTTypeId == 0){
            reportName = "Jobwork-InDC";
        }else if(tTypeId == 15 && counterTTypeId == 0){
            reportName = "Incoming_Jobwork_Invoice_Report";
        }
        else if(tTypeId == 16 && counterTTypeId == 0){
            reportName = "Jobwork-InDC";
        }else if(tTypeId == 17 && counterTTypeId == 0){
            reportName = "Jobwork-InDC";
        } else if(tTypeId == 18 && counterTTypeId == 0){
            reportName = "Incoming_Jobwork_Invoice_Report";
        }else if(tTypeId == 13 && counterTTypeId == 14){
            reportName = "JW-IN-OUT-DC";
        }else if(tTypeId == 16 && counterTTypeId == 17 ){
            reportName = "JW-IN-OUT-DC";
        }else{
            reportName = "INVALID";
        }


        return reportName+".jasper";

    }


    private void excelExport(JasperPrint jasperPrint, ByteArrayOutputStream xlsReport) throws JRException {
        JRXlsxExporter exporter = new JRXlsxExporter();

        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReport));

        SimpleXlsxReportConfiguration reportConfig
                = new SimpleXlsxReportConfiguration();
        reportConfig.setSheetNames(new String[] { "Sheet1" });

        exporter.setConfiguration(reportConfig);

        exporter.exportReport();
    }

    private Boolean isItemLevelInvoice(InvoiceHeader invoiceHeader) {
        Boolean isItemLevel = false;


        Long distinctTaxCount = invoiceHeader.getInvoiceItems()
                .stream()
                .mapToDouble(item -> (item.getIgstTaxPercentage() != null && item.getIgstTaxPercentage() > 0.0)  ? item.getIgstTaxPercentage() : item.getCgstTaxPercentage())
                .distinct()
                .count();
        logger.info("distinct tax count: "+distinctTaxCount);

        Long distinctDiscountCount = invoiceHeader.getInvoiceItems()
                .stream()
                .mapToDouble(item -> item.getDiscountPercentage() != null ? item.getDiscountPercentage() : 0.0)
                .count();
        logger.info("distinct distinctDiscountCount count: "+distinctDiscountCount);

        if(distinctTaxCount > 1 || distinctDiscountCount > 1){
            isItemLevel = true;
        }else{
            isItemLevel = false;
        }
        logger.info("isItemLvel: "+isItemLevel);

        return isItemLevel;
    }

}
