package com.coreerp.serviceimpl;


import com.coreerp.dao.EmailSettingRepository;
import com.coreerp.dto.CompanyEmailDTO;
import com.coreerp.dto.EmailDTO;

import com.coreerp.mapper.EmailMapper;
import com.coreerp.model.EmailSetting;
import com.coreerp.service.EmailService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmailServiceImpl implements EmailService {

    final static Logger log= LogManager.getLogger(EmailServiceImpl.class);


  @Autowired
    EmailSettingRepository emailSettingRepository;
  @Autowired
    EmailMapper emailMapper;


    @Override
    public EmailDTO getAllEmailSettings() {

     EmailSetting emailSetting = emailSettingRepository.getEmail(1l);

            EmailDTO emailDTO =   emailMapper.modelToDTOMap(emailSetting);


        return emailDTO;
    }

    @Override
    public EmailDTO save(EmailDTO emailDTO) {
        return emailMapper.modelToDTOMap(emailSettingRepository.save(emailMapper.dtoToModelMap(emailDTO)));
    }




}
