package com.coreerp.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.RoleRepository;
import com.coreerp.dto.RoleDTO;
import com.coreerp.mapper.RoleMapper;
import com.coreerp.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleRepository roleRepository;
	
	@Autowired 
	RoleMapper roleMapper;
	
	@Override
	public RoleDTO save(RoleDTO role) {

		return roleMapper.modelToDTOMap(roleRepository.save(roleMapper.dtoToModelMap(role)));
	}

}
