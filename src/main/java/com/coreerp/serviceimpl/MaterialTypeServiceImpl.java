package com.coreerp.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.MaterialTypeRepository;
import com.coreerp.model.MaterialType;
import com.coreerp.service.MaterialTypeService;

@Service
public class MaterialTypeServiceImpl implements MaterialTypeService {

	@Autowired
	MaterialTypeRepository materialTypeRepository;
	
	@Override
	public List<MaterialType> getAll() {
		return materialTypeRepository.findAll();
	}

	@Override
	public MaterialType getById(Long id) {
		return materialTypeRepository.getOne(id);
	}

	@Override
	public MaterialType getByName(String name) {
		return materialTypeRepository.findByName(name);
	}
}
