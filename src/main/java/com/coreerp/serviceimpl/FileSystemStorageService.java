package com.coreerp.serviceimpl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.coreerp.service.StorageService;

@Service
@Qualifier("AFileSystemStorageService")
public class FileSystemStorageService implements StorageService {

	private static Logger logger = LogManager.getLogger(FileSystemStorageService.class.getName());

	private Path rootLocation;

	@Value("${image.path}")
	private String imagePathProperty;

	@Autowired
	public FileSystemStorageService(StorageProperties properties) {
		// String tenantIdentifier = TenantContext.getCurrentTenant();
		this.rootLocation = Paths.get(properties.getLocation()); // +tenantIdentifier+"/");
		// this.rootLocation = Paths.get("D://Ravi//");
		logger.info("this.rootLocation: " + this.rootLocation);
	}

	@Override
	public void init() {
		logger.info("in init");
		try {
			logger.info("in init, rootLocation: "+rootLocation.toString()); 
			Files.createDirectories(rootLocation);
		} catch (IOException e) {
			throw new StorageException("Could not initialize storage", e);
		}
	}

	@Override
	public void store(MultipartFile file, String tenantId) {
		String filename = StringUtils.cleanPath(file.getOriginalFilename());
		try {
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file " + filename);
			}
			if (filename.contains("..")) {
				// This is a security check
				throw new StorageException(
						"Cannot store file with relative path outside current directory " + filename);
			}
			
			
			this.rootLocation = Paths.get(imagePathProperty + tenantId + "/");
			logger.info("in store: "+this.rootLocation.toString());
			try {
				Files.createDirectories(rootLocation);
				logger.info("dir created");
			} catch (IOException e) {
				throw new StorageException("Could not initialize storage", e);
			}
			
			logger.info("in store: this.rootLocation: "+this.rootLocation.toString());
			Files.copy(file.getInputStream(), this.rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
			file.getInputStream().close();
		} catch (IOException e) {
			throw new StorageException("Failed to store file " + filename, e);
		}
	}

	@Override
	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.rootLocation, 1).filter(path -> !path.equals(this.rootLocation))
					.map(path -> this.rootLocation.relativize(path));
		} catch (IOException e) {
			throw new StorageException("Failed to read stored files", e);
		}
	}

	@Override
	public Path load(String filename, String tenantId) {
		logger.info("in load: ");
		
		this.rootLocation = Paths.get(imagePathProperty + tenantId + "/");
		return this.rootLocation.resolve(filename);
	}

	@Override
	public Resource loadAsResource(String filename, String tenantId) {
		try {
			Path file = load(filename, tenantId);
			logger.info("File load path: "+ file);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				return null;
				//throw new StorageFileNotFoundException("Could not read file: " + filename);

			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}

	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

	@Override
	public String loadAsBase64(String filename, String tenantId) {
		return null;
	}


	@Override
	public String getPresignedUrl(String bucketName, String fileName) {
		return null;
	}

	@Override
	public Boolean deleteFile(String filepath) {
		try{
				File file = new File(filepath);
		    	
		    	if(file.delete()){
		    		return true;
		    	}else{
		    		logger.info("not deleted");
		    		return false;
		    	}
		}
		catch (Exception e)
		{
			logger.info("Error: "+e.getMessage());
			return false;
		}
	
	}
}
