package com.coreerp.serviceimpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.PartyTypeRepository;
import com.coreerp.model.PartyType;
import com.coreerp.service.PartyTypeService;

@Service
public class PartyTypeServiceImpl implements PartyTypeService {

	@Autowired
	PartyTypeRepository partyTypeRepository;
	
	@Override
	public PartyType savePartyType(PartyType partyType) {
		return partyTypeRepository.save(partyType);
	}

	@Override
	public List<PartyType> getAllPartyType() {
		
		List<PartyType> partyTypes = partyTypeRepository.findAll();
		partyTypes = partyTypes.stream()
				.filter(pt -> !pt.getName().equals(ApplicationConstants.PARTY_TYPE_BOTH))
				.collect(Collectors.toList());
		return partyTypes;
	}

	@Override
	public PartyType getPartyTypeById(Long id) {
		return partyTypeRepository.getOne(id);
	}

	@Override
	public PartyType getPartyTypeByName(String name) {
		return partyTypeRepository.findByName(name);
	}

}
