package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import com.coreerp.dao.*;
import com.coreerp.model.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.coreerp.dto.VoucherHeaderDTO;
import com.coreerp.dto.VoucherWrapperDTO;
import com.coreerp.dto.CityDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.VoucherHeaderMapper;
import com.coreerp.mapper.VoucherItemMapper;
import com.coreerp.service.TransactionTypeService;
import com.coreerp.service.VoucherService;


@Service
public class VoucherServiceImpl implements VoucherService {
	
	
	final static Logger log = LogManager.getLogger(VoucherServiceImpl.class);

	@Autowired
	VoucherHeaderRepository voucherHeaderRepository;
	
	@Autowired
	VoucherItemRepository voucherItemRepository;
	
	@Autowired
	VoucherHeaderMapper voucherHeaderMapper;
	
	@Autowired
	VoucherItemMapper voucherItemMapper;
	
	@Autowired
	ExpenseRepository expenseRepository;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	TransactionStatusChangeRepository transactionStatusChangeRepository;

	@Autowired
	VoucherHeaderAuditRepository voucherHeaderAuditRepository;

	@Autowired
	VoucherItemAuditRepository  voucherItemAuditRepository;
	
	@Override
	public Page<VoucherHeader> getAllVoucherHeader(Pageable pageable) {
		// TODO Auto-generated method stub
		return voucherHeaderRepository.findAll(pageable);
	}

	@Override
	public void saveVoucherHeader(VoucherHeader voucherHeader) {
		 voucherHeaderRepository.save(voucherHeader);
		
	}


	@Override
	public void deleteVoucherHeader(String voucherHeaderId) {
		// TODO Auto-generated method stub
		voucherHeaderRepository.deleteById(voucherHeaderId);
	}

	@Override
	public Page<VoucherItem> getAllVoucherItems(Pageable pageable) {
		// TODO Auto-generated method stub
		return voucherItemRepository.findAll(pageable);
	}

	@Override
	public void saveVoucherItem(VoucherItem voucherItem) {
		voucherItemRepository.save(voucherItem);
		
	}


	@Override
	public void deleteVoucherItem(String voucherItemId) {
		// TODO Auto-generated method stub
		voucherItemRepository.deleteById(voucherItemId);
	}

	@Override
	public List<VoucherItem> getAllVoucherItemsByHeaderId(String voucherHeaderId, Pageable pageable) {
		// TODO Auto-generated method stub
		return voucherItemRepository.findByHeaderId(voucherHeaderId);
	}

	@Override
	public VoucherHeader getVoucherHeaderByHeaderId(String voucherHeaderId) {
		// TODO Auto-generated method stub
		return voucherHeaderRepository.findByHeaderId(voucherHeaderId);
		
	}

	@Override
	public List<VoucherHeaderDTO> findAllVouchers() {
		List<VoucherHeader> voucherHeaders=voucherHeaderRepository.findAll();
	    List<VoucherHeaderDTO> voucherDTOList=new ArrayList<VoucherHeaderDTO>();
	    for(VoucherHeader voucherHeader : voucherHeaders){
	    	voucherDTOList.add(voucherHeaderMapper.modelToDTOMap(voucherHeader));
	    }
	    
	    log.info("voucherDTOList.voucherItems : "+(voucherDTOList.size()>0 ? voucherDTOList.get(0).getVoucherItems().size():0));
	    return voucherDTOList;
	}

	@Override
	public VoucherHeaderDTO getVouchersByHeaderId(String voucherHeaderId) {
		VoucherHeader voucherHeader = voucherHeaderRepository.getOne(voucherHeaderId);
		log.info("in getVouchersByHeaderId Voucher header id: "+voucherHeader.getId());
		log.info("in getVouchersByHeaderId voucher item : "+voucherHeader.getVoucherItems());
		
		if(voucherHeader.getVoucherItems().size() == 0){
			List<VoucherItem> voucherItems = voucherItemRepository.findByHeaderId(voucherHeader.getId());
			log.info("2 in getVouchersByHeaderId voucher item : "+voucherItems);
			voucherHeader.setVoucherItems(voucherItems);
			log.info("3 in getVouchersByHeaderId voucher item : "+voucherHeader.getVoucherItems());
		}
		return voucherHeaderMapper.modelToDTOMap(voucherHeader);
	}

	@Override
	public VoucherHeaderDTO saveVoucher(VoucherWrapperDTO voucherWrapperDTOIn) {
		// TODO Auto-generated method stub
		
		VoucherHeaderDTO voucherDTOIn = voucherWrapperDTOIn.getVoucherHeader();
		
        VoucherHeader voucherHeader = voucherHeaderMapper.dtoToModelMap(voucherDTOIn);
        
		log.info("voucherItems size 1: "+voucherHeader.getVoucherItems().size());
		
	//Get older Voucher if exists
		
        VoucherHeader voucherHeaderPre = new VoucherHeader();
		List<VoucherItem> VoucherItemsPre = new ArrayList<VoucherItem>();
		
		if(voucherDTOIn.getId() != null){
			voucherHeaderRepository.getOne(voucherDTOIn.getId()).getVoucherItems().forEach(item -> {
				VoucherItem VoucherItemPre = new VoucherItem();
				VoucherItemPre.setId(item.getId());
				
				VoucherItemsPre.add(VoucherItemPre);
			});
		}
		

		
		voucherHeaderPre.setVoucherItems(VoucherItemsPre);
		
		log.info("voucherItems size 1: "+voucherHeader.getVoucherItems().size());
		if(voucherWrapperDTOIn.getItemToBeRemove().size() > 0){
			voucherWrapperDTOIn.getItemToBeRemove().forEach(id -> {
				log.info("deleting item..."+id);
				deleteItem(id);
			});
		}
		log.info("voucherItems size 1: "+voucherHeader.getVoucherItems().size());

		
		List<VoucherItem> voucherItems = voucherItemMapper.dtoToModelList(voucherDTOIn.getVoucherItems());
		
		log.info("voucherItems size: "+voucherItems.size());
		
		//set header to items		
		voucherItems.stream().forEach(voucherItem -> voucherItem.setVoucherHeader(voucherHeader));

		//set items to header 
		voucherHeader.setVoucherItems(voucherItems);
		
		log.info("Before save: "+ voucherHeader.getCompany().getId());
		
		
		voucherHeader.getVoucherItems().forEach(item -> {
			log.info("item before save:--- "+item.getId());
		});

		//save
		VoucherHeader voucherHeaderOut = voucherHeaderRepository.save(voucherHeader);
		
		voucherHeaderOut.getVoucherItems().forEach(item -> {
			log.info("item after save:--- "+item.getId());
		});
		
		

		return voucherHeaderMapper.modelToDTOMap(voucherHeaderOut);
	}
	@Override
	public VoucherHeader save(VoucherHeader voucherHeaderIn) {
		// TODO Auto-generated method stub
		return voucherHeaderRepository.save(voucherHeaderIn);
	}

	@Override
	public List<VoucherHeaderDTO> findVoucherByVoucherNumber(String voucherNumber) {
		// TODO Auto-generated method stub
		return voucherHeaderMapper.modelToDTOList(voucherHeaderRepository.findByVoucherNumber(voucherNumber));
	}

	

	@Override
	public Long getMaxVoucherNumber(String prefix) {
		// TODO Auto-generated method stub
		return  voucherHeaderRepository.getMaxVoucherNumber(prefix);
	}

	@Override
	public VoucherItem getItemById(String voucherItemId) {
		// TODO Auto-generated method stub
		return voucherItemRepository.getOne(voucherItemId);
	}

	@Override
	public TransactionResponseDTO delete(String id) {
		TransactionResponseDTO res = new TransactionResponseDTO();
		
		String deleteStatus = "Voucher Deleted Successfully";
		Integer deleteStatusNum = 1;
		log.info("1");
		//VoucherHeader voucherDebug = voucherHeaderRepository.getOne(id);
		try {


			VoucherHeader voucherDelete = voucherHeaderRepository.getOne(id);

			auditEntry(voucherDelete);
			
			
			log.info("2");
			//voucherDebug = voucherHeaderRepository.getOne(id);
			voucherDelete.getVoucherItems().forEach(item -> {
				voucherItemRepository.delete(item);
			});
			
			voucherHeaderRepository.delete(voucherDelete);
			
				
		}catch(Exception e){
			deleteStatus = "Cannot Delete Voucher";
			deleteStatusNum = 0;
			log.info("exception: "+e);
			deleteAuditEntry(id);
			//voucherDebug = voucherHeaderRepository.getOne(id);
			
		}
		
		res.setResponseString(deleteStatus);
		res.setResponseStatus(deleteStatusNum);
		
		//dchDebug = deliveryChallanHeaderRepository.getOne(id);
		//log.info(dchDebug.getStatus().getName());
		
		return res;
	}


	private void auditEntry(VoucherHeader voucherDelete){

		VoucherHeaderAudit header = voucherHeaderMapper.modelToAudit(voucherDelete);

		voucherHeaderAuditRepository.save(header);

		List<VoucherItemAudit> items = voucherItemMapper.modelToAuditList(voucherDelete.getVoucherItems());

		items.forEach(item -> {
			voucherItemAuditRepository.save(item);
		});

	}

	private void deleteAuditEntry(String headerId){

		VoucherHeaderAudit header = voucherHeaderAuditRepository.getOne(headerId);

		if(header != null ){
			List<VoucherItemAudit> items = voucherItemAuditRepository.findByVoucherHeaderId(headerId);
			if(items != null && items.size() > 0){
				items.forEach(item -> {
					voucherItemAuditRepository.delete(item);
				});
			}

			voucherHeaderAuditRepository.delete(header);
		}

	}

	@Override
	public TransactionResponseDTO deleteItem(String id) {
		TransactionResponseDTO res = new TransactionResponseDTO();
		res.setResponseStatus(1);
		String deleteStatus = "Grn Item Deleted Successfully";
		try {

			VoucherItem voucherDelete = voucherItemRepository.getOne(id);
			//update stock
			log.info("voucherDelete: "+voucherDelete.getId());
			
			
		
			//voucherBackTrackingService.itemDelete(voucherDelete);
			
			voucherItemRepository.deleteForce(voucherDelete.getId());
			log.info("Deleted");
		
		}catch(Exception e){
			deleteStatus = "Cannot Delete Grn Item";
			res.setResponseStatus(0);
			log.error("Error: "+e);
		}
		
		res.setResponseString(deleteStatus);
		return res;
	}

	@Override
	public Long getMaxVoucherNumberByTransactionType(Long id) {
		// TODO Auto-generated method stub
		return voucherHeaderRepository.getMaxVoucherNumberByVoucherType(id);
	}

	@Override
	public VoucherHeader getModelById(String id) {
		// TODO Auto-generated method stub
		return voucherHeaderRepository.getOne(id);
	}

	@Override
	public VoucherItem getItemModelById(String id) {
		// TODO Auto-generated method stub
		return voucherItemRepository.getOne(id);
	}

//	@Override
//	public ExpenseHeader saveExpense(ExpenseHeader expenseHeaderIn) {
//		// TODO Auto-generated method stub
//		return expenseRepository.save(expenseHeaderIn);
//		}

	public ExpenseHeader deleteExpenseById(Long id)  {
		ExpenseHeader expenseHeader = expenseRepository.getOne(id);
		expenseRepository.delete(expenseHeader);
		return expenseHeader;
	}

	@Override
	public Long countByVoucherNumberAndTransactionType(String voucherNumber, TransactionType id) {
	
		return voucherHeaderRepository.countByVoucherNumberAndVoucherType(voucherNumber, id);

	}
	
//	@Override
//	public ExpenseHeader deleteExpenseById(Long id) throws MySQLIntegrityConstraintViolationException {
//		// TODO Auto-generated method stubs
//		ExpenseHeader expenseHeader = expenseRepository.getOne(id);
//		expenseRepository.delete(expenseHeader);
//		return expenseHeader;
//	}
	
	
	
	

}
