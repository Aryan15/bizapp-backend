package com.coreerp.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.ExpenseRepository;
import com.coreerp.model.ExpenseHeader;
import com.coreerp.service.ExpenseService;

@Service
public class ExpenseServiceimpl implements ExpenseService {

	@Autowired
	ExpenseRepository expenseRepository;
	
	@Override
	public ExpenseHeader saveExpense(ExpenseHeader expenseHeaderIn) {
		// TODO Auto-generated method stub
		return expenseRepository.save(expenseHeaderIn);
	}

	@Override
	public ExpenseHeader deleteExpenseById(Long id)  {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
