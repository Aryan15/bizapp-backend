package com.coreerp.serviceimpl;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.InvoiceHeaderRepository;
import com.coreerp.dao.TransactionStatusChangeRepository;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.PayableReceivableHeader;
import com.coreerp.model.PayableReceivableItem;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionStatusChange;
import com.coreerp.model.TransactionType;
import com.coreerp.service.BackTrackingService;
import com.coreerp.service.InvoiceService;
import com.coreerp.service.StatusService;
import com.coreerp.utils.TransactionDataException;

@Component
public class PayableReceivableBackTrackingService implements BackTrackingService<PayableReceivableHeader> {

	final static Logger log = LogManager.getLogger(PayableReceivableBackTrackingService.class);

	@Autowired
	InvoiceService invoiceService;

	@Autowired
	StatusService statusService;

	@Autowired
	InvoiceHeaderRepository invoiceHeaderRepository;

	@Autowired
	InvoiceStatusService invoiceStatusService;

	@Autowired
	TransactionTypeRepository transactionTypeRepository;
	
	@Autowired
	TransactionStatusChangeRepository transactionStatusChangeRepository;

	@Override
	public PayableReceivableHeader updateTransactionStatus(PayableReceivableHeader payableReceivableHeader,
			PayableReceivableHeader payableReceivableHeaderPre) {

		// Status status = new Status();

		// Update invoice header due amount
		// invoice.due_amount = invoice.grand_total - payable.paying_amt
		payableReceivableHeader.getPayableReceivableItems().forEach(item -> {
			InvoiceHeader invoice = invoiceService.getInvoiceHeaderByHeaderId(item.getInvoiceHeader().getId());
//			Double dueAmount = ((invoice.getDueAmount() != null && invoice.getDueAmount() > 0) ? invoice.getDueAmount() : invoice.getGrandTotal())
//					//- (item.getPayingAmount());
//					- ((item.getDueAmount() != null && item.getDueAmount() > 0) ? item.getPayingAmount() : invoice.getGrandTotal());
			Double dueAmount=item.getDueAmount();

			log.info("dueAmount: "+dueAmount);
			if(dueAmount < 0) {
				dueAmount = 0.0;
			}
			log.info("dueAmount 2: "+dueAmount);
			try {
				invoice.setDueAmount(dueAmount);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// invoice.setStatus(invoiceStatusService.getStatus(invoice));

			if (dueAmount == 0) {
				Status status = statusService.getStatusByTransactionTypeAndName(invoice.getInvoiceType(),
						ApplicationConstants.INVOICE_STATUS_COMPLETED);
				invoice.setStatus(status);
			} else {
				Status status = statusService.getStatusByTransactionTypeAndName(invoice.getInvoiceType(),
						ApplicationConstants.INVOICE_STATUS_PARTIAL);
				invoice.setStatus(status);
			}

			invoiceHeaderRepository.save(invoice);
			
			statusChangeEntry(invoice, payableReceivableHeader);

			// Set note reference status
			log.info("item.getPayableReceivableHeader().getCreditDebitId(): "
					+ item.getPayableReceivableHeader().getCreditDebitId());
			if (item.getPayableReceivableHeader().getCreditDebitId() != null && item.getPayableReceivableHeader().getCreditDebitId().length() > 0 ) {

				// String tempCrDrNumber =
				// item.getPayableReceivableHeader().getCreditDebitNumber();
				//
				// log.info("tempCrDrNumber 1: "+tempCrDrNumber);
				//
				// tempCrDrNumber =
				// tempCrDrNumber.substring(tempCrDrNumber.lastIndexOf('[')+1);
				//
				// log.info("tempCrDrNumber 2: "+tempCrDrNumber);
				//
				// tempCrDrNumber = tempCrDrNumber.substring(0,
				// tempCrDrNumber.lastIndexOf(']'));
				//
				// log.info("tempCrDrNumber 3: "+tempCrDrNumber);
				//
				// //tempCrDrNumber =
				// tempCrDrNumber.substring(tempCrDrNumber.lastIndexOf('"'));
				//
				// log.info("tempCrDrNumber: "+tempCrDrNumber);

				List<String> creditDebitHeaderIds = Arrays
						.asList(item.getPayableReceivableHeader().getCreditDebitId().split(","));

				log.info("creditDebitHeaderIds: " + creditDebitHeaderIds);
				
				log.info("creditDebitHeaderIds size: " + creditDebitHeaderIds.size());

				if (creditDebitHeaderIds.size() > 0) {
					creditDebitHeaderIds
					.stream()
					.map(id -> id.trim())
					.forEach(id -> {

						log.info("id: " + id);
						InvoiceHeader crDrHeader = invoiceHeaderRepository.getOne(id);

						log.info("crDrHeader");

						TransactionType tType = transactionTypeRepository
								.findByName(ApplicationConstants.NOTE_TYPE_CREDIT);

						Status status = statusService.getStatusByTransactionTypeAndName(tType,
								ApplicationConstants.INVOICE_STATUS_NOTE_COMPLETED);

						log.info("status: " + status.getName());

						crDrHeader.setStatus(status);

						invoiceHeaderRepository.save(crDrHeader);
						statusChangeEntry(crDrHeader, payableReceivableHeader);

					});

				}
			}

		});

		return payableReceivableHeader;
	}

	public void itemDelete(PayableReceivableItem item) throws TransactionDataException {

		InvoiceHeader invHeader = item.getInvoiceHeader();
		if (invHeader.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_COMPLETED)) {

			Status status = statusService.getStatusByTransactionTypeAndName(invHeader.getInvoiceType(),
					ApplicationConstants.INVOICE_STATUS_PARTIAL);
			invHeader.setStatus(status);

		}

		Double dueAmount = (invHeader.getDueAmount() != null ? invHeader.getDueAmount() : invHeader.getGrandTotal())
				+ item.getPayingAmount();
		invHeader.setDueAmount(dueAmount);

		invoiceHeaderRepository.save(invHeader);

	}
	
	
	private void statusChangeEntry(InvoiceHeader invHeader , PayableReceivableHeader prHeader ){
		
		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();
		
		transactionStatusChange.setSourceOperation("PR "+prHeader.getStatus().getName());
		transactionStatusChange.setSourceTransactionId(prHeader.getId());
		transactionStatusChange.setSourceTransactionType(prHeader.getTransactionType());
		transactionStatusChange.setSourceTransactionNumber(prHeader.getPayReferenceNumber());
		transactionStatusChange.setStatus(invHeader.getStatus());
		transactionStatusChange.setTransactionId(invHeader.getId());
		transactionStatusChange.setTransactionType(invHeader.getInvoiceType());
		transactionStatusChange.setTransactionNumber(invHeader.getPurchaseOrderNumber());
		transactionStatusChange.setParty(invHeader.getParty());

		transactionStatusChangeRepository.save(transactionStatusChange);
		
	}

}
