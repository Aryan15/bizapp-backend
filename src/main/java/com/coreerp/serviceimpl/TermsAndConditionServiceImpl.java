package com.coreerp.serviceimpl;

import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.TermsAndCondtionRepository;
import com.coreerp.dto.TermsAndConditionDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.TermsAndCondtionMapper;
import com.coreerp.model.TermsAndCondition;
import com.coreerp.model.TransactionType;
import com.coreerp.service.TermsAndConditionService;

@Service
public class TermsAndConditionServiceImpl implements TermsAndConditionService{
	
	final static Logger log = LogManager.getLogger(TermsAndConditionServiceImpl.class);

	@Autowired
	TermsAndCondtionRepository termsAndConditionRepository;
	
	@Autowired
	TermsAndCondtionMapper termsAndCondtionMapper;
	
	
	@Override
	public TermsAndCondition getTermsAndConditionById(Long id) {
		return termsAndConditionRepository.getOne(id); 
	}

	@Override
	public List<TermsAndConditionDTO> getAll() {
		return termsAndCondtionMapper.modelToDTOList(termsAndConditionRepository.findAll());
	}

	@Override
	public TermsAndConditionDTO save(TermsAndConditionDTO dto) {
		return termsAndCondtionMapper.modelToDTOMap(termsAndConditionRepository.save(termsAndCondtionMapper.dtoToModelMap(dto)));
	}

	@Override
	public List<TermsAndConditionDTO> getByTransactionTypeAndDefault(TransactionType transactionType,
			Integer defaultTermsAndCondition) {
		
		return termsAndCondtionMapper.modelToDTOList(termsAndConditionRepository.findByTransactionTypeAndDefaultTermsAndCondition(transactionType, defaultTermsAndCondition));
		
	}
	
	@Override
	public TransactionResponseDTO delete(Long id) {
		
		TransactionResponseDTO ret = new TransactionResponseDTO();
		
		ret.setResponseString("Terms and Condition deleted successfully");
		ret.setResponseStatus(1);
		
		try {
			termsAndConditionRepository.deleteById(id);
		} catch(Exception e){
			log.info("Deletion exception: "+e);
			ret.setResponseString("Error deleting Terms and Condition");
			ret.setResponseStatus(0);
		}
		
		return ret;
	}

}
