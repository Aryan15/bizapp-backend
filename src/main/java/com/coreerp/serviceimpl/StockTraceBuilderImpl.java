package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.model.GRNHeader;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.Material;
import com.coreerp.model.StockTrace;
import com.coreerp.model.TransactionType;
import com.coreerp.service.StockTraceBuilder;
import com.coreerp.service.TransactionTypeService;

@Component
public class StockTraceBuilderImpl implements StockTraceBuilder {
	
	@Autowired
	TransactionTypeService transactionTypeService;

	@Override
	public StockTrace getReversalStockTrace(TransactionType transactionType, String transactionId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StockTrace makeMaterialCreationStockTrace(Material material) {
		StockTrace st = new StockTrace();
		
		st.setBusinessDate(material.getCreatedDateTime() != null ? material.getCreatedDateTime() : material.getUpdatedDateTime());
		st.setMaterial(material);
		st.setParty(null);
		st.setQuantityIn(material.getStock());
		st.setQuantityOut(0.0);
		st.setRemarks(null);
		st.setReverse(null);
		st.setTransactionDate(material.getCreatedDateTime() != null ? material.getCreatedDateTime() : material.getUpdatedDateTime());
		st.setTransactionHeaderId(material.getId().toString());
		st.setTransactionId(material.getId().toString());
		//TODO Make it configurable
		st.setTransactionNote("New Material"); 
		st.setTransactionType(null);
		
		
		return st;
	}

	@Override
	public List<StockTrace> makeGrnCreationStockTrace(GRNHeader grnHeader) {
		
		List<StockTrace> stList = new ArrayList<StockTrace>();
		
		grnHeader.getGrnItems().forEach(item -> {
			StockTrace st = new StockTrace();
			
			st.setBusinessDate(grnHeader.getGrnDate());
			st.setMaterial(item.getMaterial());
			st.setParty(grnHeader.getSupplier());
			st.setQuantityIn(item.getAcceptedQuantity());
			st.setQuantityOut(0.0);
			// TODO Configure
			st.setRemarks("Purchase");
			st.setReverse(null);
			st.setTransactionDate(new Date());
			st.setTransactionHeaderId(grnHeader.getId());
			st.setTransactionId(item.getId());
			//TODO Configure
			st.setTransactionNote("Purchase");
			st.setTransactionType(transactionTypeService.getByName(ApplicationConstants.GRN_TYPE_SUPPLIER));
			
			stList.add(st);
			
		});
		
		return stList;
	}

	@Override
	public List<StockTrace> makeCustomerInvoiceCreationStockTrace(InvoiceHeader invoiceHeader) {
		
		List<StockTrace> stList = new ArrayList<StockTrace>();
		
		invoiceHeader.getInvoiceItems().forEach(item -> {
			StockTrace st = new StockTrace();
			
			st.setBusinessDate(invoiceHeader.getInvoiceDate());
			st.setMaterial(item.getMaterial());
			st.setParty(invoiceHeader.getParty());
			st.setQuantityIn(0.0);
			st.setQuantityOut(item.getQuantity());
			st.setRemarks("Sales");
			st.setReverse(null);
			st.setTransactionDate(new Date());
			st.setTransactionHeaderId(invoiceHeader.getId());
			st.setTransactionId(item.getId());
			//TODO configure
			st.setTransactionNote("Sales");
			st.setTransactionType(transactionTypeService.getByName(ApplicationConstants.INVOICE_TYPE_CUSTOMER));
			
			stList.add(st);
			
		});
		
		
		return stList;
	}

}
