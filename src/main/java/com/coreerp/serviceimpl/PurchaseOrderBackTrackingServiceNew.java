package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.kie.api.event.rule.AgendaEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.TrackingAgendaEventListener;
import com.coreerp.dao.QuotationHeaderRepository;
import com.coreerp.dao.TransactionStatusChangeRepository;
import com.coreerp.dto.TransactionStatusRule;
import com.coreerp.mapper.QuotationHeaderMapper;
import com.coreerp.model.DeliveryChallanItem;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.model.PurchaseOrderItem;
import com.coreerp.model.QuotationHeader;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionStatusChange;
import com.coreerp.service.BackTrackingService;
import com.coreerp.service.QuotationService;
import com.coreerp.service.StatusService;

@Component
public class PurchaseOrderBackTrackingServiceNew implements BackTrackingService<PurchaseOrderHeader> {

	final static Logger log = LogManager.getLogger(PurchaseOrderBackTrackingServiceNew.class);
	
	String preQId = "";
	Double quotationQuantity = 0.0;
	Double poQuantity = 0.0;
	
	@Autowired
    private KieContainer kieContainer;
    
	
    private KieSession kieSession ;

	@Autowired
	QuotationHeaderRepository quotationHeaderRepository;

	
    @Autowired
	StatusService statusService;
    
	@Autowired
	TransactionStatusChangeRepository transactionStatusChangeRepository;
	
	@Override
	public PurchaseOrderHeader updateTransactionStatus(PurchaseOrderHeader in, PurchaseOrderHeader transactionComponentPre) {

		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession = kieContainer.newKieSession("rulesSession");
		
		kieSession.addEventListener(agendaEventListener);
		
		//Loop through Quotations attached to the PO
		
		
		List<QuotationHeader> poQuotationHeaders = new ArrayList<QuotationHeader>();
		
		in.getPurchaseOrderItems().forEach(item -> {
			log.info("3: "+item);
			QuotationHeader poQotHeader = item.getQuotationItem() != null ? item.getQuotationItem().getQuotationHeader() : null;

			if(poQotHeader != null && preQId != poQotHeader.getId()){
				log.info("4");
				poQuotationHeaders.add(poQotHeader);
				preQId = poQotHeader.getId();
			}
		});
		
		if(poQuotationHeaders != null){
			poQuotationHeaders.forEach(qotHeader -> quotationStatus(qotHeader, in));
		}
		
		
		
		
		
		
		return null;
	}


	private void quotationStatus(QuotationHeader qotHeader, PurchaseOrderHeader in){

		log.info("qotHeader.getQuotationType().getName(): "+qotHeader.getQuotationType().getName());
		log.info("qotHeader.getId(): "+qotHeader.getId());

		//Build TransactionStatusRule
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();

		transactionStatusRule.setTransactionType(qotHeader.getQuotationType().getName());
		transactionStatusRule.setTransactionId(qotHeader.getId());
		transactionStatusRule.setCurrentStatus(qotHeader.getStatus().getName());
		quotationQuantity = 0.0;
		qotHeader.getQuotationItems().forEach(item -> {
			quotationQuantity += item.getQuantity();
		});
		log.info("quotationQuantity: "+quotationQuantity);
		transactionStatusRule.setQuantity(quotationQuantity);
		//transactionStatusRule.setPrevTransactionId(qotHeader.getId());
		//transactionStatusRule.setPrevQuantity(quotationQuantity);
		log.info("qotHeader.getQuotationType().getName(): "+qotHeader.getQuotationType().getName());
		//transactionStatusRule.setPrevStatus(qotHeader.getStatus().getName());
		log.info("in.getPurchaseOrderType().getName(): "+in.getPurchaseOrderType().getName());
		transactionStatusRule.setSourceTransactionType(in.getPurchaseOrderType().getName());
		log.info("in.getId(): "+in.getId());
		transactionStatusRule.setSourceTransactionId(in.getId());
		transactionStatusRule.setSourceTransactionStatus(in.getStatus().getName());
		poQuantity = 0.0;
		in.getPurchaseOrderItems().forEach(item -> {
			poQuantity += item.getQuantity();
		});
		log.info("poQuantity: "+poQuantity);
		transactionStatusRule.setSourceQuantity(poQuantity);


		log.info("transactionStatusRule: "+transactionStatusRule);
		//Get status from Drools

		kieSession.insert(transactionStatusRule);

		int ruleCount = kieSession.fireAllRules();

		kieSession.dispose();

		log.info("rules count: "+ruleCount);

		log.info("Got status: "+transactionStatusRule.getNewStatus());
		//Update Quotation with derived status

		if(transactionStatusRule.getNewStatus() != null)
		{
			Status status  = statusService.getStatusByTransactionTypeAndName(qotHeader.getQuotationType(), transactionStatusRule.getNewStatus());
			qotHeader.setStatus(status);

			quotationHeaderRepository.save(qotHeader);

			//Status change entry

			statusChangeEntry(qotHeader, in);
		}


	}

	private void statusChangeEntry(QuotationHeader qotHeader , PurchaseOrderHeader poHeader ){
		
		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();
		
		transactionStatusChange.setSourceOperation("PO "+poHeader.getStatus().getName());
		transactionStatusChange.setSourceTransactionId(poHeader.getId());
		transactionStatusChange.setSourceTransactionType(poHeader.getPurchaseOrderType());
		transactionStatusChange.setSourceTransactionNumber(poHeader.getPurchaseOrderNumber());
		transactionStatusChange.setStatus(qotHeader.getStatus());
		transactionStatusChange.setTransactionId(qotHeader.getId());
		transactionStatusChange.setTransactionType(qotHeader.getQuotationType());
		transactionStatusChange.setTransactionNumber(qotHeader.getQuotationNumber());
		transactionStatusChange.setParty(qotHeader.getParty());

		transactionStatusChangeRepository.save(transactionStatusChange);
		
	}

	

	
}
