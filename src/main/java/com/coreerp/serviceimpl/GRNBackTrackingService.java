package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.DeliveryChallanHeaderRepository;
import com.coreerp.mapper.DeliveryChallanHeaderMapper;
import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.DeliveryChallanItem;
import com.coreerp.model.GRNHeader;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;
import com.coreerp.service.BackTrackingService;
import com.coreerp.service.DeliveryChallanService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TransactionTypeService;

@Component
public class GRNBackTrackingService implements BackTrackingService<GRNHeader> {

	final static Logger log = LogManager.getLogger(GRNBackTrackingService.class);
	String preDCId = "";
	
	@Autowired
	DeliveryChallanService deliveryChallanService; 
	
	@Autowired
	DeliveryChallanHeaderRepository deliveryChallanRepository;
	
	@Autowired
	DeliveryChallanHeaderMapper deliveryChallanHeaderMapper;
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Override
	public GRNHeader updateTransactionStatus(GRNHeader in, GRNHeader grnHeaderPre) {

/* ** if invoice status is 'New' or 'Open' */
		
		GRNHeader grnHeaderOut = new GRNHeader();
		
		log.info("Incoming staus: "+in.getGrnStatus().getName());
		
		String grnStatus = in.getGrnStatus().getName();
		if(grnStatus.equals(ApplicationConstants.GRN_STATUS_NEW) || grnStatus.equals(ApplicationConstants.GRN_STATUS_OPEN)){
			grnHeaderOut = newOrOpenStatus(in, grnHeaderPre);
		}
		
		/* ** if GRN status = 'Cancelled'*/
		if(grnStatus.equals(ApplicationConstants.GRN_STATUS_CANCELLED) ){
			grnHeaderOut = cancelledStatus(in);
		}
		
		/* ** if GRN status = 'Deleted'*/
		if(grnStatus.equals(ApplicationConstants.GRN_STATUS_DELETED) ){
			grnHeaderOut = deletedStatus(in);
		}		
		return grnHeaderOut;
	}

	
	private GRNHeader cancelledStatus(GRNHeader in) {
		/*
		 * 		update DC 
		 * 		** update linked DC status from Completed to Open if GRN item level accepted quantity = DC quantity
		 * 		** update linked DC status from Completed to Partial if GRN item level accepted quantity < DC quantity
		 * 		** update linked DC status from Partial to Open if GRN item level accepted quantity < DC quantity
		 */
		
		log.info("in cancelledStatus");
		GRNHeader grnHeaderOut = in;
		
		List<DeliveryChallanHeader> deliveryChallanHeaders = new ArrayList<DeliveryChallanHeader>();
		List<DeliveryChallanHeader> grnDeliveryChallanHeaders = new ArrayList<DeliveryChallanHeader>();
		

		
		in.getGrnItems().forEach(item -> {

			if(item.getDeliveryChallanHeader() != null && preDCId != item.getDeliveryChallanHeader().getId()){
				grnDeliveryChallanHeaders.add(item.getDeliveryChallanHeader());
				preDCId = item.getDeliveryChallanHeader().getId();
			}
			
			
		});
		
		if(grnDeliveryChallanHeaders != null){
			grnDeliveryChallanHeaders.forEach(deliveryChallanHeader -> {
				String dcStatus = deliveryChallanHeader.getStatus().getName();
				//String newDcStatus = "";
				
				Status newDcStatus = new Status();
				TransactionType transactionType = transactionTypeService.getModelById(deliveryChallanHeader.getDeliveryChallanType().getId());
				
				
				Double grnItemTotalQuantity = in.getGrnItems().stream()
														.filter(i -> i.getDeliveryChallanHeader().getId() == deliveryChallanHeader.getId())
														.mapToDouble(i -> i.getAcceptedQuantity())
														.sum();
				Double dcTotalQuantity = deliveryChallanHeader.getDeliveryChallanItems().stream()
														.mapToDouble(d -> d.getQuantity())
														.sum();
				
				int quantityDiff = Double.compare(grnItemTotalQuantity, dcTotalQuantity);
				
				
				if(dcStatus.equals(ApplicationConstants.DC_STATUS_GRN_GENEREATED) && quantityDiff == 0){
					newDcStatus = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_NEW);
					
				}else if(dcStatus.equals(ApplicationConstants.DC_STATUS_GRN_GENEREATED) && quantityDiff < 0){
					newDcStatus = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_PARTIAL_GRN);
				}else if(dcStatus.equals(ApplicationConstants.DC_STATUS_PARTIAL_GRN) && quantityDiff < 0){
					newDcStatus = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_NEW);
				}else {
					log.error("Incorrect status");
				}
				
				
				if (newDcStatus != null){
					//Update DC Item level balance
					
					List<DeliveryChallanItem> dcItems = new ArrayList<DeliveryChallanItem>();
					
					in.getGrnItems().forEach(grnItem -> {
						
						
						DeliveryChallanItem dcItem = grnItem.getDeliveryChallanItem();
						dcItem.setGrnBalanceQuantity((dcItem.getGrnBalanceQuantity()!=null ?dcItem.getGrnBalanceQuantity():0)+ grnItem.getAcceptedQuantity());
						dcItems.add(dcItem);
					});
					
					
					deliveryChallanHeader.setStatus(newDcStatus);
					deliveryChallanHeader.setDeliveryChallanItems(dcItems);
					DeliveryChallanHeader dcUpdate = deliveryChallanRepository.save(deliveryChallanHeader);
					deliveryChallanHeaders.add(dcUpdate);
				}
				
			});
			

		}

		
		return grnHeaderOut;
	}

	private GRNHeader deletedStatus(GRNHeader in) {
		/*
		 * 		update DC 
		 * 		** update linked DC status from Completed to Open if GRN item level accepted quantity = DC quantity
		 * 		** update linked DC status from Completed to Partial if GRN item level accepted quantity < DC quantity
		 * 		** update linked DC status from Partial to Open if GRN item level accepted quantity < DC quantity
		 */
		
		log.info("in deletedStatus");
		GRNHeader grnHeaderOut = in;
		
		List<DeliveryChallanHeader> deliveryChallanHeaders = new ArrayList<DeliveryChallanHeader>();
		List<DeliveryChallanHeader> grnDeliveryChallanHeaders = new ArrayList<DeliveryChallanHeader>();
		

		
		in.getGrnItems().forEach(item -> {

			if(item.getDeliveryChallanHeader() != null && preDCId != item.getDeliveryChallanHeader().getId()){
				grnDeliveryChallanHeaders.add(item.getDeliveryChallanHeader());
				preDCId = item.getDeliveryChallanHeader().getId();
			}
			
			
		});
		
		if(grnDeliveryChallanHeaders != null){
			grnDeliveryChallanHeaders.forEach(deliveryChallanHeader -> {
				String dcStatus = deliveryChallanHeader.getStatus().getName();
				//String newDcStatus = "";
				
				Status newDcStatus = new Status();
				TransactionType transactionType = transactionTypeService.getModelById(deliveryChallanHeader.getDeliveryChallanType().getId());
				
				Double grnItemTotalQuantity = in.getGrnItems().stream()
														.filter(i -> i.getDeliveryChallanHeader().getId() == deliveryChallanHeader.getId())
														.mapToDouble(i -> i.getAcceptedQuantity())
														.sum();
				Double dcTotalQuantity = deliveryChallanHeader.getDeliveryChallanItems().stream()
														.mapToDouble(d -> d.getQuantity())
														.sum();
				
				int quantityDiff = Double.compare(grnItemTotalQuantity, dcTotalQuantity);
				
				
				if(dcStatus.equals(ApplicationConstants.DC_STATUS_GRN_GENEREATED) && quantityDiff == 0){
					newDcStatus = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_NEW);
					
				}else if(dcStatus.equals(ApplicationConstants.DC_STATUS_GRN_GENEREATED) && quantityDiff < 0){
					newDcStatus = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_PARTIAL_GRN);
				}else if(dcStatus.equals(ApplicationConstants.DC_STATUS_PARTIAL_GRN) && quantityDiff < 0){
					newDcStatus = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_NEW);
				}else {
					log.error("Incorrect status");
				}
				
				
				if (newDcStatus != null){
					//Update DC Item level balance
					
					List<DeliveryChallanItem> dcItems = new ArrayList<DeliveryChallanItem>();
					
					in.getGrnItems().forEach(grnItem -> {
						DeliveryChallanItem dcItem = grnItem.getDeliveryChallanItem();
						dcItem.setGrnBalanceQuantity((dcItem.getGrnBalanceQuantity()!=null ?dcItem.getGrnBalanceQuantity():0)+ grnItem.getAcceptedQuantity());
						dcItems.add(dcItem);
					});
					
					
					deliveryChallanHeader.setStatus(newDcStatus);
					deliveryChallanHeader.setDeliveryChallanItems(dcItems);
					DeliveryChallanHeader dcUpdate = deliveryChallanRepository.save(deliveryChallanHeader);
					deliveryChallanHeaders.add(dcUpdate);
				}
				
			});
			

		}

		
		return grnHeaderOut;
	}

	private GRNHeader newOrOpenStatus(GRNHeader in, GRNHeader grnHeaderPre){
		/*
		 * 	 update DC 
		 * 		** update linked DC status from Open to Partial if GRN item level accepted quantity < DC quantity
		 * 		** update linked DC status from Open/Partial to Completed if GRN item level accepted quantity = DC quantity

		 */
		log.info("in newOrOpenStatus");
		
		GRNHeader grnOut = in;
		
		List<DeliveryChallanHeader> deliveryChallanHeaders = new ArrayList<DeliveryChallanHeader>();
		List<DeliveryChallanHeader> grnDeliveryChallanHeaders = new ArrayList<DeliveryChallanHeader>();
		

		
		in.getGrnItems().forEach(item -> {

			if(item.getDeliveryChallanHeader() != null && preDCId != item.getDeliveryChallanHeader().getId()){
				grnDeliveryChallanHeaders.add(item.getDeliveryChallanHeader());
				preDCId = item.getDeliveryChallanHeader().getId();
			}
						
			
		});
		
		if(grnDeliveryChallanHeaders!=null){
			grnDeliveryChallanHeaders.forEach(deliveryChallanHeader -> {
				//DeliveryChallanHeader dc = deliveryChallanService.getModelById(new TransactionPK(deliveryChallanHeader.getId(),deliveryChallanHeader.getIdVersion()));
				String dcStatus = deliveryChallanHeader.getStatus().getName();
				log.info("dc Status: "+dcStatus);
				//String newDcStatus = "";
				
				Status newDcStatus = new Status();
				TransactionType transactionType = transactionTypeService.getModelById(deliveryChallanHeader.getDeliveryChallanType().getId());
				
				
				Double grnItemTotalQuantity = in.getGrnItems().stream()
														.filter(i -> i.getDeliveryChallanHeader().getId() == deliveryChallanHeader.getId())
														.mapToDouble(i -> i.getAcceptedQuantity())
														.sum();
				log.info("grnItemTotalQuantity: "+grnItemTotalQuantity);
				Double dcTotalQuantity = deliveryChallanHeader.getDeliveryChallanItems().stream()
														.mapToDouble(d -> d.getGrnBalanceQuantity() != null ? d.getGrnBalanceQuantity() : d.getQuantity())
														.sum();
				
				log.info("dcTotalQuantity: "+dcTotalQuantity);
				int quantityDiff = Double.compare(grnItemTotalQuantity, dcTotalQuantity);
				log.info("quantityDiff: "+quantityDiff);
				
				if ((dcStatus.equals(ApplicationConstants.DC_STATUS_NEW)||dcStatus.equals(ApplicationConstants.DC_STATUS_PARTIAL_GRN) || dcStatus.equals(ApplicationConstants.DC_STATUS_NEW)) && quantityDiff < 0 ){
					newDcStatus = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_PARTIAL_GRN);
					
				}else if((dcStatus.equals(ApplicationConstants.DC_STATUS_NEW)||dcStatus.equals(ApplicationConstants.DC_STATUS_PARTIAL_GRN) || dcStatus.equals(ApplicationConstants.DC_STATUS_NEW)) && quantityDiff == 0 ){
					newDcStatus = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_GRN_GENEREATED);
				}else {
					log.error("Incorrect status");
				}
				
				log.info("newDCStatus: "+newDcStatus);
				
				if (newDcStatus != null && newDcStatus.getName() != null){
					
					log.info("before status update of DC");
					//Update DC Item level balance
					
//					List<DeliveryChallanItem> dcItems = new ArrayList<DeliveryChallanItem>();
//					
//					in.getGrnItems().forEach(grnItem -> {
//						DeliveryChallanItem dcItem = grnItem.getDeliveryChallanItem();
//						log.info("dcItem: "+dcItem);
//						log.info("dcItem.getBalanceQuantity(): "+dcItem.getBalanceQuantity());
//						log.info("dcItem.getQuantity(): "+dcItem.getQuantity());
//						log.info("invoiceItem.getQuantity(): "+grnItem.getAcceptedQuantity());
//						log.info("grnItem.getAcceptedQuantity(): "+grnItem.getAcceptedQuantity());
//						dcItem.setBalanceQuantity((dcItem.getBalanceQuantity()!=null ?dcItem.getBalanceQuantity():dcItem.getQuantity()) - grnItem.getAcceptedQuantity());
//						log.info("saving dcItem balance qty: "+ dcItem.getBalanceQuantity());
//						dcItems.add(dcItem);
//					});
					
		
					deliveryChallanHeader.setStatus(newDcStatus);
//					deliveryChallanHeader.setDeliveryChallanItems(dcItems);
					DeliveryChallanHeader dcUpdate = deliveryChallanRepository.save(deliveryChallanHeader);
					deliveryChallanHeaders.add(dcUpdate);
	
				}
			
				
			});
		}

		
		return grnOut;
	}

}
