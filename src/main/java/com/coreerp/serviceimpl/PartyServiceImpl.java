package com.coreerp.serviceimpl;


import java.util.*;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.*;
import com.coreerp.dto.*;
import com.coreerp.model.*;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.mapper.PartyMapper;
import com.coreerp.service.CityService;
import com.coreerp.service.CountryService;
import com.coreerp.service.MaterialPriceListService;
import com.coreerp.service.PartyService;
import com.coreerp.service.PartyTypeService;
import com.coreerp.service.StateService;

@Service
public class PartyServiceImpl implements PartyService {

    final static Logger log = LogManager.getLogger(PartyServiceImpl.class);

    @Autowired
    PartyRepository partyRepository;

    @Autowired
    TransactionStatusChangeRepository transactionStatusChangeRepository;

    @Autowired
    CountryService countryService;

    @Autowired
    StateService stateService;

    @Autowired
    CityService cityService;

    @Autowired
    AreaRepository areaRepository;

    @Autowired
    PartyTypeService partyTypeService;

    @Autowired
    PartyMapper partyMapper;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    MaterialPriceListService materialPriceListService;

    @Autowired
    UserTenantRelationRepository userTenantRelationRepository;

    @Override
    public PartyDTO saveParty(PartyDTO partyDTO) {
        List<Address> addresses = partyDTO.getAddressesListDTO();

        //List<Address> addressesSaved = new ArrayList<Address>();

//		addresses.forEach(add -> {
//			
//			addressesSaved.add(addressRepository.save(add));
//		});
//		System.out.println("party id........>>>>>>>> "+partyDTO.getId());


        //In case of editing a party, if party is already used in a transaction
        //And state is getting changed, do not allow it to save
        if (partyDTO.getId() != null) {

            Party partyPrev = partyRepository.getOne(partyDTO.getId());
            log.info("partyPrev: " + partyPrev);

            boolean partyExist = transactionStatusChangeRepository.existsByParty(partyPrev);
            log.info("partyExist........" + partyExist);

            if (partyPrev.getState().getId() != partyDTO.getStateId() && partyExist) {
                return null;
            }

        }


        addresses = addressRepository.saveAll(addresses);

        Party party = partyMapper.dtoToModelMap(partyDTO);

        party.setAddresses(addresses);

        return partyMapper.modelToDTOMap(partyRepository.save(party));
    }

    @Override
    public UserTenantRelation savePartyByRegistration(RegistrationDTO registrationDTO, UserTenantRelation userTenantRelation) {
        List<Address> addresses = new ArrayList<>();
        PartyDTO partyDTO = new  PartyDTO();
        State state = stateService.getStateById(registrationDTO.getCompanyDTO().getStateId());
        partyDTO.setName(registrationDTO.getCompanyDTO().getName());
        partyDTO.setCountryId(state.getCountry().getId());
        partyDTO.setStateId(registrationDTO.getCompanyDTO().getStateId());
        partyDTO.setAddress(registrationDTO.getCompanyDTO().getAddress());
        if(registrationDTO.getCompanyDTO().getGstRegistrationTypeId() !=null){

            partyDTO.setGstRegistrationTypeId(registrationDTO.getCompanyDTO().getGstRegistrationTypeId());
        }
        partyDTO.setEmail(registrationDTO.getUserDTO().getEmail());
        partyDTO.setIsIgst(0);
        partyDTO.setAddressesListDTO(addresses);
        partyDTO.setContactPersonName(registrationDTO.getUserDTO().getName());
        partyDTO.setBillAddress(registrationDTO.getCompanyDTO().getAddress());
        partyDTO.setPartyTypeId(ApplicationConstants.PARTY_TYPE_CUSTOMER_ID);

        partyDTO.setGstNumber(registrationDTO.getCompanyDTO().getGstNumber() != null ? registrationDTO.getCompanyDTO().getGstNumber() : "");
        if( registrationDTO.getCompanyDTO().getStateId() != 29){
            partyDTO.setIsIgst(1);
        }else{
            partyDTO.setIsIgst(0);
        }
        Party party = partyRepository.save(partyMapper.dtoToModelMap(partyDTO));

        userTenantRelation.setParty(party);

        UserTenantRelation SysUser = userTenantRelationRepository.findByTenantAndIsActiveAndIsPrimary(userTenantRelation.getTenant(),1,null);
        log.info("SysUser: " + SysUser);
        SysUser.setParty(party);

        userTenantRelationRepository.save(SysUser);

        //Make an entry in plan subs
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, 1);
        Date toDate = c.getTime();

        userTenantRelationRepository.insertPlanSubs(new Date(), toDate, 1, 1, userTenantRelation.getCompanyId(), party.getId());

        userTenantRelationRepository.save(userTenantRelation);


        return   userTenantRelation;
    }


    @Override
    public List<PartyDTO> getAllParties() {


        return partyMapper.modelToDTOList(partyRepository.findAll());
    }


    @Override
    public PartyDTO getPartyById(Long id) {
        return partyMapper.modelToDTOMap(partyRepository.getOne(id));
    }


    @Override
    public List<PartyDTO> getPartiesByPartyType(PartyType partyType) {
        return partyMapper.modelToDTOList(partyRepository.findByPartyType(partyType));
    }

    @Override
    public List<PartyDTO> getPartiesByPartyTypeAndNameLike(List<PartyType> partyTypes, String name) {
        List<PartyDTO> outParties = new ArrayList<PartyDTO>();


        List<PartyDTO> partyDTOS = partyMapper.modelToDTOList(partyRepository.getPartyList(partyTypes, name));


        partyDTOS.forEach(p -> {

            if (p.getAreaId() != null) {
                log.info("name: " + p.getName());

                String areaName = areaRepository.getAreaName(p.getAreaId());

                p.setAreaName(areaName);
            }

            outParties.add(p);
        });

        return outParties;
    }

    @Override
    public Party getPartyObjectById(Long id) {
        return partyRepository.getOne(id);
    }


    @Override
    public List<Party> getPartyModelsByPartyType(PartyType partyType) {
        return partyRepository.findByPartyType(partyType);
    }


    @Override
    public TransactionResponseDTO deleteParty(Long id) {
        TransactionResponseDTO response = new TransactionResponseDTO();

        List<MaterialPriceListDTO> materialPriceList = materialPriceListService.getForParty(id);

        response.setResponseStatus(1);
        response.setResponseString("Deleted successfully");
        try {


            //Clear price list first
            materialPriceList.forEach(mpl -> {
                materialPriceListService.deleteById(mpl.getId());
            });

            partyRepository.deleteById(id);
            log.info("Deleted: " + id);
        } catch (Exception e) {
            response.setResponseStatus(0);
            response.setResponseString("Error deleting party: " + e.getMessage());
            log.info("Error deleting: " + e);
        }

        return response;
    }


    @Override
    public List<Party> getPartyByName(String searchString) {

        //List<PartyDTO> outParties = new ArrayList<PartyDTO>();

        List<Party> outParties = partyRepository.findByNameIgnoreCaseContains(searchString);

        return outParties;
    }


    @Override
    public List<PartyDTO> getPartyDTOByName(String searchString) {
        return partyMapper.modelToDTOList(partyRepository.findByNameIgnoreCaseContains(searchString));
    }


    @Override
    public List<Party> getPartiesByName(String searchString) {
        return partyRepository.findByNameIgnoreCaseContains(searchString);
    }

    @Override
    public List<Party> getPartyByPartyCode(String searchString) {
        return partyRepository.findByPartyCodeIgnoreCaseContains(searchString);
    }

    @Override
    public Boolean checkPartyStatus(Long id) {
        boolean partyExist = false;
        if (id != null) {

            Party partyPrev = partyRepository.getOne(id);
            log.info("partyPrev: " + partyPrev);

            partyExist = transactionStatusChangeRepository.existsByParty(partyPrev);
            log.info("partyExist........" + partyExist);

        }
        if (partyExist == true) {
            log.info("partyExist 2........" + partyExist);
            return true;
        } else {
            return false;
        }
    }


    @Override
   public Long getMaxPartyNumberByTransactionType(String transactionTypeName){
        Long partyTypeId=0l;
        if(transactionTypeName .equalsIgnoreCase(ApplicationConstants.CUSTOMER_CODE)){
            partyTypeId=ApplicationConstants.PARTY_TYPE_CUSTOMER_ID;

        }
        else{
            partyTypeId=ApplicationConstants.PARTY_TYPE_SUPPLIER_ID;
        }

   Long maxTransactionNum= partyRepository.getMaxPartyNumber(partyTypeId);

        return maxTransactionNum;
    }

    @Override
    public TransactionResponseDTO getPartyCount(){
        TransactionResponseDTO transactionResponseDTO =new TransactionResponseDTO();
        Long partyTypeId=ApplicationConstants.PARTY_TYPE_CUSTOMER_ID;;
        Long partyTypeSup=ApplicationConstants.PARTY_TYPE_SUPPLIER_ID;
        Long   partyCount= partyRepository.getMaxPartyNumber(partyTypeId);


      Long  partySupCount= partyRepository.getMaxPartyNumber(partyTypeSup);
        if (partyCount != null && partyCount >= 1 && partySupCount != null && partySupCount >= 1) {
            transactionResponseDTO.setResponseStatus(1);
        } else if (partyCount != null && partyCount >= 1) {
            transactionResponseDTO.setResponseStatus(2);

        } else if (partySupCount != null && partySupCount >= 1) {
            transactionResponseDTO.setResponseStatus(3);

        }
     return    transactionResponseDTO;
    }


    @Override
    public Long countByPartyCodeAndPartyType(String partyCode,PartyType partyTypeId){

         return partyRepository.countByPartyCodeAndPartyType(partyCode, partyTypeId);
    }



}
