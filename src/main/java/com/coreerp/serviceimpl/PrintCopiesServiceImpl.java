package com.coreerp.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.PrintCopiesRepository;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.PrintCopy;
import com.coreerp.service.PrintCopiesService;

@Service
public class PrintCopiesServiceImpl implements PrintCopiesService{

	@Autowired
	PrintCopiesRepository printCopiesRepository;
	
	@Override
	public PrintCopy getById(Long id) {
		return printCopiesRepository.getOne(id);
	}

	@Override
	public PrintCopy save(PrintCopy printCopies) {
		return printCopiesRepository.save(printCopies);
	}

	@Override
	public List<PrintCopy> getAll() {
		return printCopiesRepository.findAll();
	}

	@Override
	public TransactionResponseDTO delete(Long id) {
		
		TransactionResponseDTO res = new TransactionResponseDTO();
		
		String deleteStatus = "Transaction setting deleted successfully";
		Integer responseStatus = 1;
		
		try {
			printCopiesRepository.deleteById(id);
		}catch(Exception e){
			deleteStatus = "Cannot Delete print copy";
			responseStatus = 0;
			
		}
		
		res.setResponseString(deleteStatus);
		res.setResponseStatus(responseStatus);
		return res;
	}
}
