package com.coreerp.serviceimpl;

import com.coreerp.ApplicationConstants;
import com.coreerp.TrackingAgendaEventListener;
import com.coreerp.dao.DeliveryChallanHeaderRepository;
import com.coreerp.dao.PurchaseOrderHeaderRepository;
import com.coreerp.dao.PurchaseOrderItemRepository;
import com.coreerp.dao.TransactionStatusChangeRepository;
import com.coreerp.dto.TransactionItemQuantityRule;
import com.coreerp.dto.TransactionStatusRule;
import com.coreerp.model.*;
import com.coreerp.service.BackTrackingService;
import com.coreerp.service.StatusService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kie.api.event.rule.AgendaEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

@Component
public class DeliveryChallanBackTrackingServiceNew implements BackTrackingService<DeliveryChallanHeader> {

	final static Logger log = LogManager.getLogger(DeliveryChallanBackTrackingServiceNew.class);

	String prePOId = "";
	String preDCId = "";
	//Double dcBalanceQuantity = 0.0;
	Double poQuantity = 0.0;
	Double dcQuantity = 0.0;
	Double prevDcBalanceQuantity = 0.0;
	Double prevPoQuantity = 0.0;
	Double prevDCQuantity = 0.0;
	Double sourceQuantity = 0.0;
	Double sourcePrevQuantity = 0.0;
	Double itemPrevQuantity = 0.0;
	@Autowired
	private KieContainer kieContainer;


	private KieSession kieSession ;

	@Autowired
	StatusService statusService;

	@Autowired
	TransactionStatusChangeRepository transactionStatusChangeRepository;


	@Autowired
	private PurchaseOrderHeaderRepository purchaseOrderRepository;

	@Autowired
	private DeliveryChallanHeaderRepository deliveryChallanHeaderRepository ;

	@Autowired
	private PurchaseOrderItemRepository purchaseOrderItemRepository ;

	@Override
	public DeliveryChallanHeader updateTransactionStatus(DeliveryChallanHeader in,
														 DeliveryChallanHeader pre) {

		log.info("in updateTransactionStatus ");
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();

		kieSession = kieContainer.newKieSession("rulesSession");

		kieSession.addEventListener(agendaEventListener);
		//Loop through POs attached to the DC

		List<PurchaseOrderHeader> dcPOHeaders = new ArrayList<PurchaseOrderHeader>();

		List<DeliveryChallanHeader> dcDCHeaders = new ArrayList<DeliveryChallanHeader>();

		in.getDeliveryChallanItems().forEach(dci -> {
			PurchaseOrderHeader dcPoHeader = dci.getPurchaseOrderItem() != null ? dci.getPurchaseOrderItem().getPurchaseOrderHeader() : null;
			DeliveryChallanHeader dcDCHeader = dci.getSourceDeliveryChallanItem() != null ? dci.getSourceDeliveryChallanItem().getDeliveryChallanHeader() : null;

			if(dcPoHeader != null && prePOId != dcPoHeader.getId()){
				dcPOHeaders.add(dcPoHeader);
				prePOId = dcPoHeader.getId();
			}

			if(dcDCHeader != null && preDCId != dcDCHeader.getId()){
				dcDCHeaders.add(dcDCHeader);
				preDCId = dcDCHeader.getId();
			}
		});



		log.info("dcPOHeaders: "+dcPOHeaders);

		if(dcPOHeaders.size() > 0){
			dcPOHeaders
					.stream()
//			.filter(poH -> (poH.getIsOpenEnded() != null && poH.getIsOpenEnded() == 1 ? (poH.getPurchaseOrderEndDate().compareTo(in.getDeliveryChallanDate()) > 0 ? false : true ) : true))
					.forEach(poHeader -> poStatusUpdate(poHeader, in, pre));
		}


		log.info("dcDCHeaders: "+dcDCHeaders);

		if(dcDCHeaders.size() > 0){
			dcDCHeaders
					.stream()
					.forEach(dcHeader -> dcStatusUpdate(dcHeader, pre, in) );
			kieSession.dispose();
		}

		return null;
	}

	private void poStatusUpdate(PurchaseOrderHeader poHeader, DeliveryChallanHeader in, DeliveryChallanHeader pre){
		{


			log.info("poH.getIsOpenEnded(): "+poHeader.getIsOpenEnded());
			log.info("poH.getPurchaseOrderEndDate(): "+poHeader.getPurchaseOrderEndDate());
			log.info("in.getDeliveryChallanDate(): "+in.getDeliveryChallanDate());
			//log.info("poH.getPurchaseOrderEndDate().compareTo(in.getDeliveryChallanDate()): "+poHeader.getPurchaseOrderEndDate().compareTo(in.getDeliveryChallanDate()));

			//For open ended PO, change status to Open (from New)
			if(poHeader.getIsOpenEnded() != null && poHeader.getIsOpenEnded() == 1 )
			{
				if(poHeader.getStatus() != null && !poHeader.getStatus().getName().equals(ApplicationConstants.PURCHASE_ORDER_STATUS_OPEN)) {
					Status openStatus = statusService.getStatusByTransactionTypeAndName(poHeader.getPurchaseOrderType(), ApplicationConstants.PURCHASE_ORDER_STATUS_OPEN);
					poHeader.setStatus(openStatus);
					purchaseOrderRepository.save(poHeader);
				}
				statusChangeEntry(poHeader, in);
				return;
			}

			TransactionStatusRule transactionStatusRule = new TransactionStatusRule();

			//Build TransactionStatusRule

			prevDcBalanceQuantity = 0.0;
			//dcBalanceQuantity = 0.0;
			prevPoQuantity = 0.0;

			poHeader.getPurchaseOrderItems().forEach(poi -> {
				prevDcBalanceQuantity += poi.getDcBalanceQuantity();
				prevPoQuantity += poi.getQuantity();

			});

			sourcePrevQuantity = 0.0;

			if(pre != null){
				pre.getDeliveryChallanItems()
				.stream()
				.filter(dci -> dci.getPurchaseOrderItem() != null)
				.forEach(di -> {
					sourcePrevQuantity += di.getQuantity();
				});
			}



			log.info("prevDcBalanceQuantity: "+prevDcBalanceQuantity);
			log.info("prevPoQuantity: "+prevPoQuantity);

			String latestStatus = null;
			if(in.getStatus().getName().equals(ApplicationConstants.DC_STATUS_DELETED)||in.getStatus().getName().equals(ApplicationConstants.DC_STATUS_CANCELLED)){
//					latestStatus = transactionStatusChangeRepository.findByLatestTransactionId(poHeader.getId()).getStatus().getName();
					//log.info("latestStatus: "+latestStatus);

				log.info("poHeader.getId(): "+poHeader.getId());
				TransactionStatusChange transactionStatusChange = transactionStatusChangeRepository.findByLatestTransactionId(poHeader.getId());

				if(transactionStatusChange != null){
					latestStatus = transactionStatusChange.getStatus().getName();
				}

				log.info("latestStatus: "+latestStatus);
			}

			dcQuantity = 0.0;

			in.getDeliveryChallanItems()
			.stream()
			.filter(dci -> dci.getPurchaseOrderItem() != null)
			.forEach(ini -> {
				dcQuantity += ini.getQuantity();
			});

			transactionStatusRule.setSourceQuantity(dcQuantity);
			transactionStatusRule.setDcBalanceQuantity(prevDcBalanceQuantity);

			transactionStatusRule.setInvoiceBalanceQuantity(null);
			transactionStatusRule.setSourceTransactionId(in.getId());
			transactionStatusRule.setSourceTransactionStatus(in.getStatus().getName());
			transactionStatusRule.setSourceTransactionType(in.getDeliveryChallanType().getName());
			transactionStatusRule.setTransactionType(poHeader.getPurchaseOrderType().getName());

			transactionStatusRule.setLatestStatus(latestStatus);
			transactionStatusRule.setQuantity(prevPoQuantity);

			dcQuantity = 0.0;

			in.getDeliveryChallanItems().forEach(dci -> {
				dcQuantity += dci.getQuantity();
			});

			transactionStatusRule.setSourceQuantity(dcQuantity);
			transactionStatusRule.setSourcePrevQuantity(sourcePrevQuantity);
			transactionStatusRule.setSourceTransactionId(in.getId());
			transactionStatusRule.setSourceTransactionStatus(in.getStatus().getName());
			transactionStatusRule.setSourceTransactionType(in.getDeliveryChallanType().getName());
			transactionStatusRule.setCurrentStatus(poHeader.getStatus().getName());
			transactionStatusRule.setTransactionId(poHeader.getId());
			transactionStatusRule.setTransactionType(poHeader.getPurchaseOrderType().getName());

			log.info("transactionStatusRule: "+transactionStatusRule);
			//Get status from Drools


			kieSession.insert(transactionStatusRule);

			int ruleCount = kieSession.fireAllRules();



			log.info("rules count: "+ruleCount);
		
			//log.info("Got status: "+transactionStatusRule.getStatus());

			//Update PO with derived status

			if(ruleCount > 0 && transactionStatusRule.getNewStatus() != null)
			{
				Status status  = statusService.getStatusByTransactionTypeAndName(poHeader.getPurchaseOrderType(), transactionStatusRule.getNewStatus());
				poHeader.setStatus(status);

				//Meke po Item updates

				if(transactionStatusRule.getItemUpdateRequired()){

					poHeader.getPurchaseOrderItems()
							.stream()
							.filter(poi -> {
										Long preDcPoItemExistCount = 0l;
										if(pre != null){
											preDcPoItemExistCount= pre.getDeliveryChallanItems()
													.stream()
													.filter(dci -> dci.getPurchaseOrderItem() != null ? dci.getPurchaseOrderItem().getId() == poi.getId() : false).count();
										}
										if(preDcPoItemExistCount >0)
											return true;
										else
											//	return false;
											return in.getDeliveryChallanItems().stream().anyMatch(di -> di.getPurchaseOrderItem() != null ? di.getPurchaseOrderItem().getId().equals(poi.getId()) : false);
									}
							)
							.forEach(poItem -> {

								Double preQuantity = null;

								if (pre != null  && pre.getDeliveryChallanItems() != null && pre.getDeliveryChallanItems().size() > 0)
								{
									preQuantity = pre.getDeliveryChallanItems()
											.stream()
											.filter(di -> di.getPurchaseOrderItem().getId() == poItem.getId())
											.mapToDouble(di -> di.getQuantity())
											.findFirst()
											.getAsDouble();
								}

								TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();

								transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
								transactionItemQuantityRule.setItemId(poItem.getId());
								transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());
								//transactionItemQuantityRule.setPrevDCBalanceQuantity(poItem.getDcBalanceQuantity());
								transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
								transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(poItem.getInvoiceBalanceQuantity());
								//transactionItemQuantityRule.setPrevQuantity(poItem.getQuantity());
								//transactionItemQuantityRule.setPrevStatus(transactionStatusRule.getCurrentStatus());
								//transactionItemQuantityRule.setQuantity(preQuantity != null ? (preQuantity - poItem.getQuantity()) : poItem.getQuantity());
								transactionItemQuantityRule.setQuantity(poItem.getQuantity());
								sourceQuantity = 0.0;

								//sourceQuantity =
								DeliveryChallanItem dItem = in.getDeliveryChallanItems()
										.stream()
										.filter(i -> i.getPurchaseOrderItem().getId().equals(poItem.getId()))
										.findFirst()
										.orElse(null);
								log.info("dItem: "+dItem);
								if(dItem != null){
									log.info("dItem.getQuantity(): "+dItem.getQuantity());
									sourceQuantity = preQuantity != null  ? (dItem.getQuantity() - preQuantity) : dItem.getQuantity();
								}else{
									sourceQuantity = -preQuantity;
								}
								log.info("pre quantity: "+preQuantity);
								log.info("sourceQuantity: "+sourceQuantity);
								//If Item is getting deleted in DC, respective PO DC balance quantity is faked to po item quantity
								if(sourceQuantity == 0 && Double.compare(dItem != null ? dItem.getQuantity() : 0.0, preQuantity) != 0){
									transactionItemQuantityRule.setPrevDCBalanceQuantity(poItem.getQuantity());
									transactionItemQuantityRule.setSourceQuantity(0.0);
								}else{
									transactionItemQuantityRule.setPrevDCBalanceQuantity(poItem.getDcBalanceQuantity());
									transactionItemQuantityRule.setSourceQuantity(sourceQuantity);
								}


								//transactionItemQuantityRule.setSourceTransactionType(transactionStatusRule.getSourceTransactionType());
								//transactionItemQuantityRule.setStatus(transactionStatusRule.getNewStatus());
								//transactionItemQuantityRule.setTransactionType(transactionStatusRule.getTransactionType());

								log.info("item rule before: "+transactionItemQuantityRule);




								int ruleCounti = 0;
								log.info("pre: "+pre);
								//log.info("preDcPoItemExistCount: "+preDcPoItemExistCount);
//				            if(pre != null && preDcPoItemExistCount > 0)
//				            {
								log.info("pre 1: "+pre);
								kieSession.insert(transactionItemQuantityRule);

								ruleCounti = kieSession.fireAllRules();
//				            }
								log.info("rules count: "+ruleCounti);
								log.info("item rule after: "+transactionItemQuantityRule);

								if(ruleCounti > 0 && transactionItemQuantityRule.getdCBalanceQuantity() != null)
									poItem.setDcBalanceQuantity(transactionItemQuantityRule.getdCBalanceQuantity());
							});


				}

				kieSession.dispose();

				purchaseOrderRepository.save(poHeader);

				//Status change entry

				if(!in.getStatus().getName().equals(ApplicationConstants.DC_STATUS_CANCELLED)) {
					statusChangeEntry(poHeader, in);
				}
			}



		}
	}

	private void dcStatusUpdate(DeliveryChallanHeader dcHeader, DeliveryChallanHeader pre, DeliveryChallanHeader in){

		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();

		//Build TransactionStatusRule

		prevDcBalanceQuantity = 0.0;
		//dcBalanceQuantity = 0.0;
		prevDCQuantity = 0.0;


		dcHeader.getDeliveryChallanItems().forEach(dci -> {
			prevDcBalanceQuantity += dci.getDcBalanceQuantity();
			prevDCQuantity += dci.getQuantity();

		});

		sourcePrevQuantity = 0.0;

		if(pre != null){
			pre.getDeliveryChallanItems()
					.stream()
					.filter(di -> di.getSourceDeliveryChallanItem() != null)
					.filter(dci -> dci.getSourceDeliveryChallanItem() == null ? false : (dci.getSourceDeliveryChallanItem().getDeliveryChallanHeader() != null && dci.getSourceDeliveryChallanItem().getDeliveryChallanHeader().getId().equals(dcHeader.getId())))
					.forEach(di -> {
				sourcePrevQuantity += di.getQuantity();
			});
		}



		log.info("prevDcBalanceQuantity: "+prevDcBalanceQuantity);
		log.info("prevDCQuantity: "+prevDCQuantity);

		String latestStatus = null;
		if(in.getStatus().getName().equals(ApplicationConstants.DC_STATUS_DELETED)||in.getStatus().getName().equals(ApplicationConstants.DC_STATUS_CANCELLED)){
//					latestStatus = transactionStatusChangeRepository.findByLatestTransactionId(poHeader.getId()).getStatus().getName();
//					log.info("latestStatus: "+latestStatus);

			log.info("dcHeader.getId(): "+dcHeader.getId());
			TransactionStatusChange transactionStatusChange = transactionStatusChangeRepository.findByLatestTransactionId(dcHeader.getId());

			if(transactionStatusChange != null){
				latestStatus = transactionStatusChange.getStatus().getName();
			}

			log.info("latestStatus: "+latestStatus);
		}

		transactionStatusRule.setDcBalanceQuantity(prevDcBalanceQuantity);

		transactionStatusRule.setInvoiceBalanceQuantity(null);
		transactionStatusRule.setSourceTransactionId(in.getId());
		transactionStatusRule.setSourceTransactionStatus(in.getStatus().getName());
		transactionStatusRule.setSourceTransactionType(in.getDeliveryChallanType().getName());
		transactionStatusRule.setTransactionType(dcHeader.getDeliveryChallanType().getName());

		transactionStatusRule.setLatestStatus(latestStatus);
		transactionStatusRule.setQuantity(prevDCQuantity);

		dcQuantity = 0.0;

		in.getDeliveryChallanItems()
				.stream()
				.filter(dci -> dci.getSourceDeliveryChallanItem() != null)
				.filter(dci -> dci.getSourceDeliveryChallanItem() == null ? false : (dci.getSourceDeliveryChallanItem().getDeliveryChallanHeader() != null && dci.getSourceDeliveryChallanItem().getDeliveryChallanHeader().getId().equals(dcHeader.getId())))
				.forEach(dci -> {
			dcQuantity += dci.getQuantity();
		});

		transactionStatusRule.setSourceQuantity(dcQuantity);
		transactionStatusRule.setSourcePrevQuantity(sourcePrevQuantity);
		transactionStatusRule.setSourceTransactionId(in.getId());
		transactionStatusRule.setSourceTransactionStatus(in.getStatus().getName());
		transactionStatusRule.setSourceTransactionType(in.getDeliveryChallanType().getName());
		transactionStatusRule.setCurrentStatus(dcHeader.getStatus().getName());
		transactionStatusRule.setTransactionId(dcHeader.getId());
		transactionStatusRule.setTransactionType(dcHeader.getDeliveryChallanType().getName());

		log.info("transactionStatusRule: "+transactionStatusRule);
		//Get status from Drools


		kieSession.insert(transactionStatusRule);

		int ruleCount = kieSession.fireAllRules();



		log.info("rules count: "+ruleCount);

		//log.info("Got status: "+transactionStatusRule.getStatus());

		//Update PO with derived status

		if(ruleCount > 0 && transactionStatusRule.getNewStatus() != null)
		{
			Status status  = statusService.getStatusByTransactionTypeAndName(dcHeader.getDeliveryChallanType(), transactionStatusRule.getNewStatus());
			//log.info(status+"--------");
			dcHeader.setStatus(status);

			//Meke dc Item updates

			if(transactionStatusRule.getItemUpdateRequired()){

				dcHeader.getDeliveryChallanItems()
						.stream()
						//.filter(poi -> in.getDeliveryChallanItems().stream().anyMatch(di -> di.getPurchaseOrderItem().getId().equals(poi.getId())))
						.filter(dci -> {
									Long preDcDcItemExistCount = 0l;
									if(pre != null){
										preDcDcItemExistCount= pre.getDeliveryChallanItems()
												.stream()
												.filter(pdci -> pdci.getSourceDeliveryChallanItem() != null ? pdci.getSourceDeliveryChallanItem().getId() == dci.getId() : false).count();
									}
									if(preDcDcItemExistCount >0)
										return true;
									else
										//	return false;
										return in.getDeliveryChallanItems().stream().anyMatch(di -> di.getSourceDeliveryChallanItem() != null ? di.getSourceDeliveryChallanItem().getId().equals(dci.getId()) : false);
								}
						)
						.forEach(dcItem -> {


							Double preQuantity = 0.0;

							if(pre != null && pre.getDeliveryChallanItems() != null && pre.getDeliveryChallanItems().size() > 0) {
								
								OptionalDouble tempQuantity = pre.getDeliveryChallanItems()
										.stream()
										.filter(prei -> prei.getSourceDeliveryChallanItem() != null ? (prei.getSourceDeliveryChallanItem().getId().equals(dcItem.getId())): false )
										.mapToDouble(ini -> ini.getQuantity())
										.findFirst()
										//.getAsDouble()
										;
								if(tempQuantity.isPresent()) {
									preQuantity  = tempQuantity.getAsDouble();

								}
							}
							TransactionItemQuantityRule transactionItemQuantityRule = new TransactionItemQuantityRule();

							transactionItemQuantityRule.setStatusRuleId(transactionStatusRule.getRuleId());
							transactionItemQuantityRule.setItemId(dcItem.getId());
							transactionItemQuantityRule.setHeaderId(transactionStatusRule.getTransactionId());

							transactionItemQuantityRule.setPrevGRNBalanceQuantity(null);
							transactionItemQuantityRule.setPrevInvoiceBalanceQuantity(dcItem.getInvoiceBalanceQuantity());
							//transactionItemQuantityRule.setPrevQuantity(poItem.getQuantity());
							//transactionItemQuantityRule.setPrevStatus(transactionStatusRule.getCurrentStatus());
							transactionItemQuantityRule.setQuantity(dcItem.getQuantity());

							sourceQuantity = 0.0;

							//sourceQuantity =
							DeliveryChallanItem dItem = in.getDeliveryChallanItems()
									.stream()
									.filter(i -> i.getSourceDeliveryChallanItem().getId().equals(dcItem.getId()))
									.findFirst()
									.orElse(null);

							if(dItem != null){
								sourceQuantity = preQuantity != null ? (dItem.getQuantity() - preQuantity) : dItem.getQuantity();
							}else{
								sourceQuantity = -preQuantity;
							}
							//If Item is getting deleted in DC, respective PO DC balance quantity is faked to po item quantity
							if(sourceQuantity == 0 && Double.compare(dItem != null ? dItem.getQuantity() : 0.0, preQuantity) != 0){
								transactionItemQuantityRule.setPrevDCBalanceQuantity(dcItem.getQuantity());
								transactionItemQuantityRule.setSourceQuantity(0.0);

							}else{
								transactionItemQuantityRule.setPrevDCBalanceQuantity(dcItem.getDcBalanceQuantity());
								transactionItemQuantityRule.setSourceQuantity(sourceQuantity);
							}


							//transactionItemQuantityRule.setSourceTransactionType(transactionStatusRule.getSourceTransactionType());
							//transactionItemQuantityRule.setStatus(transactionStatusRule.getNewStatus());
							//transactionItemQuantityRule.setTransactionType(transactionStatusRule.getTransactionType());

							log.info("item rule before: "+transactionItemQuantityRule);

							kieSession.insert(transactionItemQuantityRule);

							int ruleCounti = kieSession.fireAllRules();

							log.info("rules count: "+ruleCounti);
							log.info("item rule after: "+transactionItemQuantityRule);


							if(ruleCounti > 0 && transactionItemQuantityRule.getdCBalanceQuantity() != null)
								dcItem.setDcBalanceQuantity(transactionItemQuantityRule.getdCBalanceQuantity());
						});


			}



			deliveryChallanHeaderRepository.save(dcHeader);

			//Status change entry
			if(!in.getStatus().getName().equals(ApplicationConstants.DC_STATUS_CANCELLED)) {

				dcStatusChangeEntry(dcHeader, in);

			}
		}



	}

	private void statusChangeEntry(PurchaseOrderHeader poHeader , DeliveryChallanHeader dcHeader ){

		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();

		transactionStatusChange.setSourceOperation("DC "+dcHeader.getStatus().getName());
		transactionStatusChange.setSourceTransactionId(dcHeader.getId());
		transactionStatusChange.setSourceTransactionType(dcHeader.getDeliveryChallanType());
		transactionStatusChange.setSourceTransactionNumber(dcHeader.getDeliveryChallanNumber());
		transactionStatusChange.setStatus(poHeader.getStatus());
		transactionStatusChange.setTransactionId(poHeader.getId());
		transactionStatusChange.setTransactionType(poHeader.getPurchaseOrderType());
		transactionStatusChange.setTransactionNumber(poHeader.getPurchaseOrderNumber());
		transactionStatusChange.setParty(poHeader.getParty());

		transactionStatusChangeRepository.save(transactionStatusChange);

	}

	private void dcStatusChangeEntry(DeliveryChallanHeader dcHeader , DeliveryChallanHeader sourceDCHeader ){

		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();

		transactionStatusChange.setSourceOperation("DC "+sourceDCHeader.getStatus().getName());
		transactionStatusChange.setSourceTransactionId(sourceDCHeader.getId());
		transactionStatusChange.setSourceTransactionType(sourceDCHeader.getDeliveryChallanType());
		transactionStatusChange.setSourceTransactionNumber(sourceDCHeader.getDeliveryChallanNumber());
		transactionStatusChange.setStatus(dcHeader.getStatus());
		transactionStatusChange.setTransactionId(dcHeader.getId());
		transactionStatusChange.setTransactionType(dcHeader.getDeliveryChallanType());
		transactionStatusChange.setTransactionNumber(dcHeader.getDeliveryChallanNumber());
		transactionStatusChange.setParty(dcHeader.getParty());

		transactionStatusChangeRepository.save(transactionStatusChange);

	}

//	public void itemDelete(DeliveryChallanItem item){
//
//		log.info("in itemDelete ");
//
//
//
//		if(item.getPurchaseOrderItem() != null){
//
//			PurchaseOrderHeader poHeader = item.getPurchaseOrderHeader();
//			if(poHeader.getStatus().getName().equals(ApplicationConstants.PURCHASE_ORDER_STATUS_DC_GENERATED)){
//				Status status  = statusService.getStatusByTransactionTypeAndName(poHeader.getPurchaseOrderType(), ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_DC);
//		        poHeader.setStatus(status);
//		        purchaseOrderRepository.save(poHeader);
//			}
//
//			PurchaseOrderItem poItem = item.getPurchaseOrderItem();
//
//			poItem.setDcBalanceQuantity(poItem.getDcBalanceQuantity()+item.getQuantity());
//
//			purchaseOrderItemRepository.save(poItem);
//
//			statusChangeEntry(poHeader, item.getDeliveryChallanHeader());
//
//		}
//
//	}

}
