package com.coreerp.serviceimpl;

import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.DeliveryChallanItem;
import com.coreerp.model.GlobalSetting;
import com.coreerp.model.Material;
import com.coreerp.service.GlobalSettingService;
import com.coreerp.service.InventoryService;
import com.coreerp.service.StockTraceService;

@Component
public class DeliveryChallanInventoryService implements InventoryService<DeliveryChallanHeader> {

	final static Logger log = LogManager.getLogger(DeliveryChallanInventoryService.class);
	
	@Autowired
	MaterialRepository materialRepository;
	
	@Autowired
	GlobalSettingService globalSettingService;
	
	@Autowired
	StockTraceService stockTraceService;
	
	@Override
	public String updateInventory(DeliveryChallanHeader transactionComponent,
			DeliveryChallanHeader transactionComponentPre) {
		String updateInventoryStatus = "Failed";
		String preStatus = transactionComponentPre != null ? (transactionComponentPre.getStatus() != null ? transactionComponentPre.getStatus().getName() : "") : "";


		if(stockUpdateApplicable(transactionComponent)){
			if(transactionComponent.getDeliveryChallanType().getName().equals(ApplicationConstants.INCOMING_JOBWORK_IN_DC) || transactionComponent.getDeliveryChallanType().getName().equals(ApplicationConstants.OUTGOING_JOBWORK_IN_DC)|| transactionComponent.getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_SUPPLIER) ){
				if(transactionComponent.getStatus().getName().equals(ApplicationConstants.DC_STATUS_NEW) ){
					updateInventoryStatus = increaseStock(transactionComponent, transactionComponentPre);
				}else if(transactionComponent.getStatus().getName().equals(ApplicationConstants.DC_STATUS_DELETED)){
					updateInventoryStatus = decreaseStock(transactionComponent, transactionComponentPre);
				}
			}		
			
			if(transactionComponent.getDeliveryChallanType().getName().equals(ApplicationConstants.INCOMING_JOBWORK_OUT_DC) || transactionComponent.getDeliveryChallanType().getName().equals(ApplicationConstants.OUTGOING_JOBWORK_OUT_DC) || transactionComponent.getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_CUSTOMER) ){
				if(transactionComponent.getStatus().getName().equals(ApplicationConstants.DC_STATUS_NEW)  ){
					log.info("51: calling decreaseStock");
					updateInventoryStatus = decreaseStock(transactionComponent, transactionComponentPre);
				}else if(transactionComponent.getStatus().getName().equals(ApplicationConstants.DC_STATUS_DELETED) && !preStatus.equals(ApplicationConstants.DC_STATUS_CANCELLED) ){
					updateInventoryStatus = increaseStock(transactionComponent, transactionComponentPre);
				}
				else if(transactionComponent.getStatus().getName().equals(ApplicationConstants.DC_STATUS_CANCELLED)){
					updateInventoryStatus = increaseStock(transactionComponent, transactionComponentPre);
				}
			}
			
			
			//Stock Trace
			
			if(transactionComponent.getDeliveryChallanType().getName().equals(ApplicationConstants.INCOMING_JOBWORK_IN_DC) || transactionComponent.getDeliveryChallanType().getName().equals(ApplicationConstants.INCOMING_JOBWORK_OUT_DC) ){
				if(transactionComponent.getStatus().getName().equals(ApplicationConstants.DC_STATUS_DELETED) && !preStatus.equals(ApplicationConstants.DC_STATUS_CANCELLED)){
					stockTraceService.postReversalJWDCStockTrace(transactionComponent);
				}
				else if(transactionComponent.getStatus().getName().equals(ApplicationConstants.DC_STATUS_CANCELLED)){
					stockTraceService.postReversalJWDCStockTrace(transactionComponent);


				}
				else{
					stockTraceService.saveJWDCCreationStockTrace(transactionComponent, transactionComponentPre);
				}
			}
			
			if(transactionComponent.getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_SUPPLIER) || transactionComponent.getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_CUSTOMER) ){
				if(transactionComponent.getStatus().getName().equals(ApplicationConstants.DC_STATUS_DELETED)){
					stockTraceService.postReversalDcStockTrace(transactionComponent);
				}else{
					stockTraceService.saveDcCreationStockTrace(transactionComponent, transactionComponentPre);
				}
			}
			
		}
		
		
		
		return updateInventoryStatus;
	}
	
	
	private String decreaseStock(DeliveryChallanHeader transactionComponent, DeliveryChallanHeader transactionComponentPre) {
		String status = "Failed";
		
		transactionComponent.getDeliveryChallanItems().stream()
							.forEach(dcItem -> {
								Material material = materialRepository.getOne(dcItem.getMaterial().getId());
								
								Double quantity = dcItem.getQuantity();
								
								OptionalDouble preQuantity = transactionComponentPre != null ? transactionComponentPre.getDeliveryChallanItems()
										.stream()
										.filter(iitem -> iitem.getId().equals(dcItem.getId()))
										.mapToDouble(iitem -> iitem.getQuantity())
										.findFirst() : null;
										
								Double updateQuantity = (preQuantity != null && preQuantity.isPresent() && quantity != preQuantity.getAsDouble()) ? (quantity - preQuantity.getAsDouble()) : quantity;
								log.info("updateQuantity: "+updateQuantity);
								if(material.getSupplyType().getName().equals(ApplicationConstants.SUPPLY_TYPE_GOODS)){
								material.setStock(material.getStock() != null ? material.getStock() - updateQuantity : (0 - updateQuantity));								
								
								//MaterialDTO materialDTO = materialMapper.modelToDTOMap(material);
								materialRepository.save(material);
								}
							});
		status = "Success";
		
		return status;
	}
	
	private String increaseStock(DeliveryChallanHeader transactionComponent, DeliveryChallanHeader transactionComponentPre ) {
		String status = "Failed";
		log.info("in increaseStock"+ transactionComponentPre);
		transactionComponent.getDeliveryChallanItems().stream()
		.forEach(dcItem -> {
			Material material = materialRepository.getOne(dcItem.getMaterial().getId());
//			
//			if(material.getSupplyType().getName().equals(ApplicationConstants.SUPPLY_TYPE_GOODS)){
//				Double stockValue = dcItem.getQuantity();
//				log.info("got material: "+material);
//				log.info("got stockValue: "+stockValue);
//				//get previous invoice item
//				List<DeliveryChallanItem> tempDCItems = transactionComponentPre.getDeliveryChallanItems().stream().filter(item -> item.getId() == dcItem.getId()).collect(Collectors.toList());
//				DeliveryChallanItem DeliveryChallanItemPre = null;
//				log.info("got tempDCItems: "+tempDCItems);
//				if(tempDCItems.size() > 0){
//					DeliveryChallanItemPre = tempDCItems.get(0) ;
//				}
//					
//
//				if(DeliveryChallanItemPre != null){
//					log.info("got prev invoice item quantity: "+DeliveryChallanItemPre.getQuantity());
//					log.info("got current invoice item quantity: "+dcItem.getQuantity());
//					//if previous quantity is more than recent one, 
//					//earlier stock update should get increased to the difference value
//					if(DeliveryChallanItemPre.getQuantity() > dcItem.getQuantity()){
//						stockValue = material.getStock() +  (DeliveryChallanItemPre.getQuantity() - dcItem.getQuantity()) ;
//						log.info("1 "+stockValue);
//					}else
//					//if recent quantity is more than previous quantity,
//					//stock is just decreased to the difference amount
//					if(DeliveryChallanItemPre.getQuantity() < dcItem.getQuantity()){
//						stockValue = material.getStock() - (dcItem.getQuantity() - DeliveryChallanItemPre.getQuantity());
//						log.info("2 "+stockValue);
//					}
//					//if no change in quantity, stock should is unchanged
//					else{
//						stockValue = material.getStock();
//						log.info("3 "+stockValue);
//					}
//				}
//				else
//				{
//					log.info("4.."+stockValue);
//					//stockValue = material.getStock() + grnItem.getAcceptedQuantity();
//					stockValue = material.getStock() != null ?  material.getStock() + dcItem.getQuantity() : dcItem.getQuantity();
//					log.info("4"+stockValue);
//				}
//				
//				material.setStock(material.getStock() != null ? stockValue : (0 + stockValue));
//				log.info("5"+stockValue);
//			}
			
			if(material.getSupplyType().getName().equals(ApplicationConstants.SUPPLY_TYPE_GOODS)){
				
				Double quantity = dcItem.getQuantity();
				
				OptionalDouble preQuantity = transactionComponentPre != null ? transactionComponentPre.getDeliveryChallanItems()
									.stream()
									.filter(iitem -> iitem.getId().equals(dcItem.getId()))
									.mapToDouble(iitem -> iitem.getQuantity())
									.findFirst()
									: null;

				Double updateQuantity = (preQuantity != null && preQuantity.isPresent() && preQuantity.getAsDouble() != quantity) ? (quantity - preQuantity.getAsDouble()) : quantity;
				
				log.info("updatedQuantity: "+updateQuantity);
				
				if((preQuantity != null && preQuantity.isPresent() && preQuantity.getAsDouble() != quantity) || ( preQuantity == null || !preQuantity.isPresent()) || transactionComponent.getStatus().getName().equals(ApplicationConstants.DC_STATUS_DELETED))
				{
					log.info("Material stock update reqired: "+quantity+" : "+preQuantity);
					material.setStock(material.getStock() != null ? material.getStock() + updateQuantity : updateQuantity);								
					
					//MaterialDTO materialDTO = materialMapper.modelToDTOMap(material);
					materialRepository.save(material);
				}else {
					log.info("Material stock update NOT reqired: "+quantity+" : "+preQuantity);
				}
			}			
		});
		
		status = "Success";
		
		return status;
	}
	
	
	private boolean stockUpdateApplicable(DeliveryChallanHeader dcHeader) {
		boolean returnValue = false;
		
		
		GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);
		log.info("dcHeader.getDeliveryChallanType().getName(): "+dcHeader.getDeliveryChallanType().getName());
//		log.info("ApplicationConstants.GRN_TYPE_SUPPLIER: "+ApplicationConstants.GRN_TYPE_SUPPLIER);
		log.info("globalSetting: "+globalSetting.getStockUpdateIncrease().getTransactionType().getName());
		if(dcHeader.getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_SUPPLIER)){
			log.info("Supplier DC");
			if(globalSetting.getStockUpdateIncrease().getTransactionType().getName().equals(dcHeader.getDeliveryChallanType().getName())){
				log.info("Supplier DC mapped for stock increase");
				returnValue = true;
			}
		}
		if(dcHeader.getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_CUSTOMER)){
			log.info("Supplier DC");
			if(globalSetting.getStockUpdateDecrease().getTransactionType().getName().equals(dcHeader.getDeliveryChallanType().getName())){
				log.info("Customer DC mapped for stock decrease");
				returnValue = true;
			}
		}
		if(dcHeader.getDeliveryChallanType().getName().equals(ApplicationConstants.INCOMING_JOBWORK_IN_DC))
		{
			returnValue = true;
		}
		
		if(dcHeader.getDeliveryChallanType().getName().equals(ApplicationConstants.INCOMING_JOBWORK_OUT_DC))
		{
			returnValue = true;
		}
		
		if(dcHeader.getDeliveryChallanType().getName().equals(ApplicationConstants.OUTGOING_JOBWORK_IN_DC))
		{
			returnValue = true;
		}
		
		if(dcHeader.getDeliveryChallanType().getName().equals(ApplicationConstants.OUTGOING_JOBWORK_OUT_DC))
		{
			returnValue = true;
		}
		return returnValue;
	}

}
