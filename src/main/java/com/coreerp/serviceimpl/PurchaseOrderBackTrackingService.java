package com.coreerp.serviceimpl;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.mapper.QuotationHeaderMapper;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.service.BackTrackingService;
import com.coreerp.service.QuotationService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TransactionTypeService;

@Component
public class PurchaseOrderBackTrackingService implements BackTrackingService<PurchaseOrderHeader> {

	final static Logger log = LogManager.getLogger(PurchaseOrderBackTrackingService.class);
	
	String preQId = "";
	
	@Autowired
	QuotationService quotationService;
	
	@Autowired
	QuotationHeaderMapper quotationHeaderMapper;
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Override
	public PurchaseOrderHeader updateTransactionStatus(PurchaseOrderHeader in, PurchaseOrderHeader pre) {
		
		
		PurchaseOrderHeader purchaseOrderHeaderOut = new PurchaseOrderHeader();
		
		log.info("Incoming status: "+in.getStatus());
		
		/* ** if PO status is 'New' or 'Open' */
		if(in.getStatus().getName().equals(ApplicationConstants.PURCHASE_ORDER_STATUS_NEW) ){
			log.info("1");
			//purchaseOrderHeaderOut = newOrOpenStatus(in);
		}
		
		/* * ** if PO status = 'Deleted' */
		if(in.getStatus().getName().equals(ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED) ){
			//purchaseOrderHeaderOut = deletedStatus(in);
			log.info("1");
		}else
		{
			purchaseOrderHeaderOut = in;
		
		return purchaseOrderHeaderOut;
	}

//	private PurchaseOrderHeader deletedStatus(PurchaseOrderHeader in) {
//		 /* 
//		 * 		update Quotation 
//		 * 		** update linked Quotation status from Completed to Open 
//		 */ 
//		
//		PurchaseOrderHeader purchaseOrderHeaderOut = in;
//		List<QuotationHeader> quotationHeaders = new ArrayList<QuotationHeader>();
//		List<QuotationHeader> poQuotationHeaders = new ArrayList<QuotationHeader>();
//		log.info("in deletedStatus");
//		
//		in.getPurchaseOrderItems().forEach(item -> {
//			
//			if(item.getQuotationHeader() != null && preQId != item.getQuotationHeader().getId()){
//				poQuotationHeaders.add(item.getQuotationHeader());
//				preQId = item.getQuotationHeader().getId();
//			}
//		});
//		
//		
//		if(poQuotationHeaders!=null){
//			poQuotationHeaders.forEach(quotationHeader -> {
//			String qotStatus = quotationHeader.getStatus().getName();
//			Status newStatus = new Status();
//			TransactionType transactionType = transactionTypeService.getModelById(quotationHeader.getQuotationType().getId());
//			if(qotStatus.equals(ApplicationConstants.QUOTATION_STATUS_COMPLETED) ){
//				newStatus = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.QUOTATION_STATUS_OPEN);				
//			}else {
//				log.error("Incorrect status: "+qotStatus);
//			}
//			
//			
//			if (newStatus != null){				
//				
//				QuotationDTO qSave = quotationHeaderMapper.modelToDTOMap(quotationHeader);
//				qSave.setStatusId(newStatus.getId());
//				
//				QuotationHeader qOut = quotationHeaderMapper.dtoToModelMap(quotationService.save(qSave));
//				quotationHeaders.add(qOut);				
//
//			}
//			
//		});	
//		}

		
		return purchaseOrderHeaderOut;
		
	}



//	private PurchaseOrderHeader newOrOpenStatus(PurchaseOrderHeader in) {
//		/*
//		 * 	 update Quotation 
//		 * 		** update linked Quotation status from Open/New to Completed
//
//		 */
//		
//		log.info("2");
//		PurchaseOrderHeader purchaseOrderHeaderOut = in;
//		List<QuotationHeader> quotationHeaders = new ArrayList<QuotationHeader>();
//		List<QuotationHeader> poQuotationHeaders = new ArrayList<QuotationHeader>();
//		
//		//log.info("in newOrOpenStatus...qth header size: "+in.getQuotationHeaders().size());
//		
//		in.getPurchaseOrderItems().forEach(item -> {
//			log.info("3: "+item);
//			if(item.getQuotationHeader() != null && preQId != item.getQuotationHeader().getId()){
//				log.info("4");
//				poQuotationHeaders.add(item.getQuotationHeader());
//				preQId = item.getQuotationHeader().getId();
//			}
//		});
//		
//		if(poQuotationHeaders != null){
//			
//			log.info("5");
//			poQuotationHeaders.forEach(qotHeader -> {
//					log.info("qotHeader.getId(): "+qotHeader.getId());
//					String quotationStatus = qotHeader.getStatus().getName();
//					log.info("quotationStatus: "+quotationStatus);
//					//String newQuotationStatus = "";
//					Status newStatus = new Status();
//					TransactionType transactionType = qotHeader.getQuotationType();//transactionTypeService.getModelById(qotHeader.getQuotationType().getId());
//					
//					if((quotationStatus.equals(ApplicationConstants.QUOTATION_STATUS_OPEN) || quotationStatus.equals(ApplicationConstants.QUOTATION_STATUS_NEW))){
//						log.info("6");
//						newStatus = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.QUOTATION_STATUS_COMPLETED);						
//					}else {
//						log.error("Incorrect status");
//					}
//					
//					log.info("newQuotationStatus: "+newStatus);
//					
//					if (newStatus != null && newStatus.getName() != null){
//						
//						//Create new version of Quotation with new status
//						
//						QuotationDTO qSave = quotationHeaderMapper.modelToDTOMap(qotHeader);
//						qSave.setStatusId(newStatus.getId());
//						
//						QuotationHeader qOut = quotationHeaderMapper.dtoToModelMap(quotationService.save(qSave));
//						quotationHeaders.add(qOut);
//
//					}
//					
//					
//				});			
//			
//		}
//		
//
//		
//		return purchaseOrderHeaderOut;
//		
//	}



}
