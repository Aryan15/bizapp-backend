package com.coreerp.serviceimpl;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.model.QuotationHeader;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;
import com.coreerp.service.StatusService;
import com.coreerp.service.TransactionStatusService;
import com.coreerp.service.TransactionTypeService;

@Component
public class QuotationStatusService implements TransactionStatusService<QuotationHeader>{

	final static Logger log = LogManager.getLogger(QuotationStatusService.class);
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	
	@Override
	public Status getStatus(QuotationHeader qot) {
		/*
		 * 
		 * ** derive status as New if incoming id is null and/or status is null/ 'New'
		 * ** derive status as Open if incoming id is not null and status != 'Cancelled' 
		 * 
		 * ** derive status as Cancelled if incoming id is not null/ 'New' and status = 'Cancelled'
		 * 
		 * ** validate if the deleting invoice is the latest in the series
		 * ** derive status as Deleted if incoming id is not null and status = 'Deleted'
		 */
			Status status = new Status();
			
			log.info("Status derivation parameters....");
			log.info("qot.getId: "+qot.getId());
			log.info("qot.getStatus(): "+qot.getStatus());
			
			TransactionType transactionType = transactionTypeService.getModelById(qot.getQuotationType().getId());
			
			String qotStatus = qot.getStatus() != null ? qot.getStatus().getName() : null;
			
			if((qot.getId() == null || qot.getId().isEmpty()) || qotStatus.equals(ApplicationConstants.QUOTATION_STATUS_NEW)){
				log.info("1");
				status = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.QUOTATION_STATUS_NEW);
			}else if((qot.getId() != null && !qot.getId().isEmpty())&& !qotStatus.equals(ApplicationConstants.QUOTATION_STATUS_CANCELLED) && qotStatus.equals(ApplicationConstants.QUOTATION_STATUS_COMPLETED)){
				log.info("2");
				status = qot.getStatus();
			}else if((qot.getId() != null && !qot.getId().isEmpty())&& !qotStatus.equals(ApplicationConstants.QUOTATION_STATUS_CANCELLED)){
				log.info("3");
				status = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.QUOTATION_STATUS_OPEN);
			}else if((qot.getId() != null && !qot.getId().isEmpty()) && qotStatus.equals(ApplicationConstants.QUOTATION_STATUS_CANCELLED)){
				log.info("4");
				status = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.QUOTATION_STATUS_CANCELLED);
			}else if((qot.getId() != null && !qot.getId().isEmpty()) && qotStatus.equals(ApplicationConstants.QUOTATION_STATUS_DELETED)){
				log.info("5");
				status = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.QUOTATION_STATUS_DELETED);
			}else {
				log.info("6");
				status = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.QUOTATION_STATUS_OPEN);
			}
			log.info("out status: "+status);
			return status;
	}

}
