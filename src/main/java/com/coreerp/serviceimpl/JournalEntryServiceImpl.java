package com.coreerp.serviceimpl;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.JournalEntryRepository;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dto.JournalEntryDTO;
import com.coreerp.mapper.JournalEntryMapper;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.JournalEntry;
import com.coreerp.model.PayableReceivableHeader;
import com.coreerp.model.TransactionType;
import com.coreerp.service.JournalEntryBuilder;
import com.coreerp.service.JournalEntryService;

@Service
public class JournalEntryServiceImpl implements JournalEntryService {

	final static Logger log = LogManager.getLogger(JournalEntryServiceImpl.class);
	
	@Autowired
	JournalEntryRepository journalEntryRepository;
	
	@Autowired
	JournalEntryMapper journalEntryMapper;
	
	@Autowired
	JournalEntryBuilder journalEntryBuilder;
	
	@Autowired
	TransactionTypeRepository transactionTypeRepository ; 
	
	@Override
	public JournalEntryDTO save(JournalEntryDTO journalEntryDTO) {
		return journalEntryMapper.modelToDTOMap(journalEntryRepository.save(journalEntryMapper.dtoToModelMap(journalEntryDTO)));
	}

	@Override
	public JournalEntryDTO findById(String id) {
		return journalEntryMapper.modelToDTOMap(journalEntryRepository.getOne(id));
	}

	@Override
	public JournalEntry findModelById(String id) {
		return journalEntryRepository.getOne(id);
	}

	@Override
	public JournalEntry saveModel(JournalEntry journalEntry) {
		return journalEntryRepository.save(journalEntry);
	}

	@Override
	public JournalEntry findByTransactionTypeAndTransactionId(TransactionType transactionType, String transactionId) {
		return journalEntryRepository.findByTransactionTypeAndTransactionId(transactionType, transactionId);
	}

	@Override
	public JournalEntry saveInvoiceJournalEntry(InvoiceHeader invoiceHeader) {
		//Check if reversal required
		
		JournalEntry jeReversalRequired = findByTransactionTypeAndTransactionId(transactionTypeRepository.findByName(ApplicationConstants.TRANSACTION_CUSTOMER_INVOICE), invoiceHeader.getId());
		
		JournalEntry jeSaved;
		
		log.info("in saveInvoiceJournalEntry");
		
		Integer transactionIdSequence =1;
		//Post reversal if required
		if (jeReversalRequired != null){
			
			
			log.info("is reversal required: "+jeReversalRequired.getId());
			//Increment transaction id sequence
			
			JournalEntry jeReversal = new JournalEntry();
			jeReversal.setAmount(jeReversalRequired.getAmount()* -1.0);
			jeReversal.setBusinessDate(jeReversalRequired.getBusinessDate());
			jeReversal.setCreditAccount(jeReversalRequired.getCreditAccount());
			jeReversal.setDebitAccount(jeReversalRequired.getDebitAccount());
			jeReversal.setIsReversal("Y");
			jeReversal.setParty(jeReversalRequired.getParty());
			jeReversal.setRemarks(jeReversalRequired.getRemarks());
			jeReversal.setTransactionId(jeReversalRequired.getTransactionId());
			jeReversal.setTransactionDate(jeReversalRequired.getTransactionDate());
			jeReversal.setTransactionIdSequence(jeReversalRequired.getTransactionIdSequence() + 1);
			jeReversal.setTransactionType(jeReversalRequired.getTransactionType());					
			
			transactionIdSequence = jeReversal.getTransactionIdSequence() + 1;
//			transactionIdSequence++;
//			jeReversalRequired.setTransactionIdSequence( transactionIdSequence);
//			transactionIdSequence++;
//			jeReversalRequired.setIsReversal("Y");
//			jeReversalRequired.setAmount(jeReversalRequired.getAmount() * -1.0);
			journalEntryRepository.save(jeReversal);
		}
		
		//Build Journal Entry  for Invoice 
		//If Customer Invoice
		log.info("type of invoice: "+invoiceHeader.getInvoiceType().getName());
		if (invoiceHeader.getInvoiceType().getName().equals(ApplicationConstants.INVOICE_TYPE_CUSTOMER)){ 
			
			JournalEntry customerInvoiceJournalEntry = journalEntryBuilder.makeCustomerInvoiceJournalEntry(invoiceHeader);
			customerInvoiceJournalEntry.setTransactionIdSequence(transactionIdSequence);
			

			//Post Journal Entry for customer invoice
			jeSaved = journalEntryRepository.save(customerInvoiceJournalEntry);
			
			log.info("JE saved: "+jeSaved);
		}else{
			log.info("not customer invoice: "+invoiceHeader.getInvoiceType().getName());
		}
		
		
		
		//Return saved Journal Entry
		
		return null;
	}

	@Override
	public JournalEntry savePayableReceivableJournalEntry(PayableReceivableHeader payableReceivableHeader) {
		
		

		
		payableReceivableHeader.getPayableReceivableItems().forEach(prItem -> {
			
			//Check if reversal required
			
			JournalEntry jeReversalRequired = findByTransactionTypeAndTransactionId(payableReceivableHeader.getTransactionType(), prItem.getId());
			
			Integer transactionIdSequence =1;
			//Post reversal if required
			if (jeReversalRequired != null){
				//Increment transaction id sequence 
				transactionIdSequence = jeReversalRequired.getTransactionIdSequence();
				transactionIdSequence++;
				jeReversalRequired.setTransactionIdSequence( transactionIdSequence);
				transactionIdSequence++;
				jeReversalRequired.setIsReversal("Y");
				jeReversalRequired.setAmount(jeReversalRequired.getAmount() * -1.0);
				journalEntryRepository.save(jeReversalRequired);
			}
			
			//Build Journal Entry  for Payable Receivable 
			//If Customer Receivable
			if (payableReceivableHeader.getTransactionType().getName().equals(ApplicationConstants.PR_CUSTOMER_RECEIVABLE)){ 
				
				JournalEntry customerReceivableJournalEntry = journalEntryBuilder.makeCustomerReceivableJournalEntry(prItem);
				customerReceivableJournalEntry.setTransactionIdSequence(transactionIdSequence);
				

				//Post Journal Entry for customer Receivable
				journalEntryRepository.save(customerReceivableJournalEntry);
			}
			
			//If Customer Payable
			if (payableReceivableHeader.getTransactionType().getName().equals(ApplicationConstants.PR_SUPPLIER_PAYABLE)){ 
				
				JournalEntry supplierPayableJournalEntry = journalEntryBuilder.makeSupplierPayableJournalEntry(prItem);
				supplierPayableJournalEntry.setTransactionIdSequence(transactionIdSequence);
				

				//Post Journal Entry for customer Receivable
				journalEntryRepository.save(supplierPayableJournalEntry);
			}
			
		});
		
		
		return null;
	}

	@Override
	public JournalEntry postReversalInvoiceJournalEntry(InvoiceHeader invoiceHeader) {

		JournalEntry jeReversalRequired = findByTransactionTypeAndTransactionId(transactionTypeRepository.findByName(ApplicationConstants.TRANSACTION_CUSTOMER_INVOICE), invoiceHeader.getId());
		
		
		log.info("in postReversalInvoiceJournalEntry");
		
		//Integer transactionIdSequence =1;
		//Post reversal if required
		if (jeReversalRequired != null){
			
			
			log.info("is reversal required: "+jeReversalRequired.getId());
			//Increment transaction id sequence
			
			JournalEntry jeReversal = new JournalEntry();
			jeReversal.setAmount(jeReversalRequired.getAmount()* -1.0);
			jeReversal.setBusinessDate(jeReversalRequired.getBusinessDate());
			jeReversal.setCreditAccount(jeReversalRequired.getCreditAccount());
			jeReversal.setDebitAccount(jeReversalRequired.getDebitAccount());
			jeReversal.setIsReversal("Y");
			jeReversal.setParty(jeReversalRequired.getParty());
			jeReversal.setRemarks(jeReversalRequired.getRemarks());
			jeReversal.setTransactionId(jeReversalRequired.getTransactionId());
			jeReversal.setTransactionDate(jeReversalRequired.getTransactionDate());
			jeReversal.setTransactionIdSequence(jeReversalRequired.getTransactionIdSequence() + 1);
			jeReversal.setTransactionType(jeReversalRequired.getTransactionType());					
			
			//transactionIdSequence = jeReversal.getTransactionIdSequence() + 1;
//			transactionIdSequence++;
//			jeReversalRequired.setTransactionIdSequence( transactionIdSequence);
//			transactionIdSequence++;
//			jeReversalRequired.setIsReversal("Y");
//			jeReversalRequired.setAmount(jeReversalRequired.getAmount() * -1.0);
			journalEntryRepository.save(jeReversal);
		}
		
		return null;
	}

	@Override
	public JournalEntry postReversalPayableReceivableJournalEntry(PayableReceivableHeader payableReceivableHeader) {

		payableReceivableHeader.getPayableReceivableItems().forEach(prItem -> {
			
			//Check if reversal required
			
			JournalEntry jeReversalRequired = findByTransactionTypeAndTransactionId(payableReceivableHeader.getTransactionType(), prItem.getId());
			
			Integer transactionIdSequence =1;
			//Post reversal if required
			if (jeReversalRequired != null){
				//Increment transaction id sequence 
				transactionIdSequence = jeReversalRequired.getTransactionIdSequence();
				transactionIdSequence++;
				jeReversalRequired.setTransactionIdSequence( transactionIdSequence);
				transactionIdSequence++;
				jeReversalRequired.setIsReversal("Y");
				jeReversalRequired.setAmount(jeReversalRequired.getAmount() * -1.0);
				journalEntryRepository.save(jeReversalRequired);
			}
		});
		
		return null;
	}

}
