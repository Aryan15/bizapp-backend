package com.coreerp.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.AccountClassRepository;
import com.coreerp.dto.AccountClassDTO;
import com.coreerp.mapper.AccountClassMapper;
import com.coreerp.model.AccountClass;
import com.coreerp.service.AccountClassService;

@Service
public class AccountClassServiceImpl implements AccountClassService {

	@Autowired
	AccountClassRepository accountClassRepository;
	
	@Autowired
	AccountClassMapper accountClassMapper;
	
	
	@Override
	public AccountClassDTO save(AccountClassDTO accountDTO) {
		
		
		return accountClassMapper.modelToDTOMap(accountClassRepository.save(accountClassMapper.dtoToModelMap(accountDTO)));
	}

	@Override
	public AccountClassDTO getById(Long id) {
		return accountClassMapper.modelToDTOMap(accountClassRepository.getOne(id));
	}

	@Override
	public AccountClass getModelById(Long id) {
		return accountClassRepository.getOne(id);
	}

}
