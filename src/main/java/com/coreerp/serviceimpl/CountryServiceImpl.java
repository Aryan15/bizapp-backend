package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.CountryRepository;
import com.coreerp.dto.CountryDTO;
import com.coreerp.model.Country;
import com.coreerp.service.CountryService;

@Service
public class CountryServiceImpl implements CountryService {
	
	final static Logger log = LogManager.getLogger(CountryServiceImpl.class);

	@Autowired
	CountryRepository countryRepository;
	
	@Override
	public Country saveCountry(CountryDTO countryDTO) {
		
		//If country id exist, then update
		Country country; // = new Country();
		
		if(countryDTO.getId() != null)			
			country = countryRepository.getOne(countryDTO.getId());
		else
			country = new Country();
		
		
		country.setName(countryDTO.getName());
		country.setDeleted(countryDTO.getDeleted());
		
		countryRepository.save(country);
		log.info("contry");
		
		return country;
		//log.info("Saved country id: "+ country.getId());
		
		
	}

//	@Override
//	public List<Country> getAllCountries() {
//		return countryRepository.findAll();
//	}

	@Override
	public List<CountryDTO> getAllCountries(){
		List<CountryDTO> countryDTOList = new ArrayList<CountryDTO>();
		List<Country> countryList = countryRepository.findAll();
		
		for(Country country : countryList){
			CountryDTO countryDTO = new CountryDTO();
			countryDTO.setId(country.getId());
			countryDTO.setName(country.getName());
			countryDTO.setDeleted(country.getDeleted());
			countryDTOList.add(countryDTO);
	}
	
	return countryDTOList;
	
	}

	@Override
	public Country getCountryById(Long id) {
		return countryRepository.getOne(id);
	}

	@Override
	public Country deleteCountryById(Long id) {

		Country country = countryRepository.getOne(id);
		countryRepository.delete(country);
		
		return country;
	}
}
