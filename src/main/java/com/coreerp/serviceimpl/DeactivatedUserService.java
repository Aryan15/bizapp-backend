package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.coreerp.dao.UserTenantRelationRepository;
import com.coreerp.dto.DeactivatedUser;
import com.coreerp.model.UserTenantRelation;

@Service
public class DeactivatedUserService {
	
	final static Logger log = LogManager.getLogger(DeactivatedUserService.class);

	private List<DeactivatedUser> deactivatedUsers = new ArrayList<>();
	
	
	@Autowired
	UserTenantRelationRepository userTenantRelationRepository;
	
	public void addToList(List<DeactivatedUser> userList) {
		this.deactivatedUsers.addAll(userList);
	}
	
	public void clearList() {
		this.deactivatedUsers.clear();
	}
	
	public List<DeactivatedUser> getAllDeactivatedUsers(){
		return this.deactivatedUsers;
	}
	
	public Optional<DeactivatedUser> getDeactivatedUser(String tenant) {
		if(this.deactivatedUsers != null && this.deactivatedUsers.size() > 0) {
			return this.deactivatedUsers.stream()
					.filter(user -> user.getTenant().equals(tenant))
					.findFirst();
		}
		else return null;
	}
	
	public void deleteFromList(DeactivatedUser deactivatedUser) {
		
		List<DeactivatedUser> filteredList = 
				this.deactivatedUsers.stream().filter(user -> user.getId() != deactivatedUser.getId())
				.collect(Collectors.toList());
		
		this.deactivatedUsers = filteredList;
		
	}
	
	public boolean findByTenant(String tenant) {
		
		//log.info("this.deactivatedUsers: "+this.deactivatedUsers);
		
		List<DeactivatedUser> filteredList = this.deactivatedUsers.stream()
		.filter(user -> user.getTenant().equals(tenant))
		.collect(Collectors.toList());
		
		if(filteredList != null && filteredList.size() > 0) {
			return true;
		}else {
			return false;
		}
		
		
	}
	
	
	@EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.info("Getting all deactivated users");
        List<UserTenantRelation> userTenantList = userTenantRelationRepository.findByIsActive(0);
        
        List<DeactivatedUser> deactivatedUserList = new ArrayList<>();
        
        userTenantList
        .forEach(user -> {
        	DeactivatedUser deactivatedUser = new DeactivatedUser();
        	deactivatedUser.setId(user.getId());
        	deactivatedUser.setTenant(user.getTenant());
        	
        	deactivatedUserList.add(deactivatedUser);
        });
        
        clearList();
        addToList(deactivatedUserList);
        
    }
}
