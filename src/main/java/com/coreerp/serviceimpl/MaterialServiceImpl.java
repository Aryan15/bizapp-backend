package com.coreerp.serviceimpl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.dto.MaterialDTO;
import com.coreerp.dto.MaterialRecentDTO;
import com.coreerp.dto.MaterialStockCheckDTO;
import com.coreerp.dto.MaterialStockCheckDTOWrapper;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.MaterialMapper;
import com.coreerp.model.Material;
import com.coreerp.model.MaterialType;
import com.coreerp.model.Party;
import com.coreerp.model.SupplyType;
import com.coreerp.service.GlobalSettingService;
import com.coreerp.service.MaterialService;
import com.coreerp.service.MaterialTypeService;
import com.coreerp.service.PartyService;
import com.coreerp.service.StockTraceService;

@Service
public class MaterialServiceImpl implements MaterialService {

	final static Logger log = LogManager.getLogger(MaterialServiceImpl.class);
	
	@Autowired
	MaterialRepository materialRepository;
	
	@Autowired
	MaterialTypeService materialTypeService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	MaterialMapper materialMapper;
	
	@Autowired
	StockTraceService stockTraceService;

	@Autowired
	GlobalSettingService globalSettingService;
	
	@Override
	public MaterialDTO save(MaterialDTO materialDTO) {
		
		//Get previous state of material
		Material materialPre = new Material();
		
		if(materialDTO.getId() != null ){
			Material matPre = materialRepository.getOne(materialDTO.getId());
			materialPre.setId(matPre.getId());
			materialPre.setStock(matPre.getStock());
		}


		
		log.info("material stock"+ materialDTO.getStock());
		log.info("material opening stock"+ materialDTO.getOpeningStock());
		
		Material material = materialMapper.dtoToModelMap(materialDTO);
		if(material.getPartNumber()!=null) {
			if (material.getPartNumber().isEmpty() || material.getPartNumber().equalsIgnoreCase("")) {
				material.setPartNumber(null);
			}
		}
		
		log.info("material stock"+ material.getStock());
		log.info("material opening stock"+ material.getOpeningStock());
		
		Material materialOut = materialRepository.save(material);
		
		log.info("material out stock"+ materialOut.getStock());
		log.info("material out opening stock"+ materialOut.getOpeningStock());
		
		//make an entry in stock trace
		
		Double prevStock = materialPre.getStock() != null ? materialPre.getStock() : 0.0;
		
		if(materialOut.getSupplyType().getId() == 1){
			if(materialOut.getStock()!= null && materialOut.getStock() != prevStock){
				log.info("Calling material creation stock trace entry ");
				stockTraceService.saveMaterialCreationStockTrace(materialOut);	
			}
			
		}
		return materialMapper.modelToDTOMap(materialOut);
	}

	@Override
	public Material getMaterialById(Long id) {
		return materialRepository.getOne(id);
	}

	@Override
	public List<MaterialDTO> getAllMaterials() {
		return materialMapper.modelToDTOList(materialRepository.findAll());
	}

	@Override
	public MaterialDTO getMaterialDTOById(Long id) {
		return materialMapper.modelToDTOMap(materialRepository.getOne(id));
	}

	@Override
	public List<MaterialDTO>  getMaterialDTOByMaterialTypeId(Long materialTypeId) {
		
		MaterialType materialType =  materialTypeService.getById(materialTypeId);
		
		return materialMapper.modelToDTOList(materialRepository.findByMaterialType(materialType));
	}

	@Override
	public List<MaterialDTO>  getMaterialDTOByMaterialTypeIdJobwork(Long materialTypeId, String name) {

		MaterialType materialType =  materialTypeService.getById(materialTypeId);

		return materialMapper.modelToDTOList(materialRepository.materialSearchForJobwork(materialTypeId, name, name));
	}

	@Override
	public List<MaterialDTO> searchMaterialByNamePattern(String name) {
		
		return materialMapper.modelToDTOList(materialRepository.findByNameIgnoreCaseContaining(name,name));
	}
	
//	@Override
//	public List<MaterialDTO> getMaterialByPartyId(Long partyId) {
//		 
//		PartyDTO party = partyService.getPartyById(partyId);
//		
//		return materialMapper.modelToDTOList(materialRepository.findByPartyId(party));
//	}
//	
	

	@Override
	public TransactionResponseDTO deleteMaterial(Long id) {
		
		log.info("in delete material: "+id);
		
		TransactionResponseDTO response = new TransactionResponseDTO();
		
		response.setResponseStatus(1);
		response.setResponseString("Deleted successfully");
		
		try{
			Material material=materialRepository.getById(id);
		
			if(stockTraceService.deleteFirstMaterialInStockTrace(material,id)) {
				log.info("Before material delete");
				materialRepository.deleteById(id);
				
			}
			else {
				
				response.setResponseStatus(0);
				response.setResponseString("Error deleting material (stock trace): ");
			}
			
		}
		catch(Exception e){
			response.setResponseStatus(0);
			response.setResponseString("Material is being used elsewhere cannot delete");
			log.info("Error deleting: "+e);
		}
		
		
		return response;
	}

	@Override
	public Material getMaterialByPartNumber(String partNumber) {

		return materialRepository.findByPartNumber(partNumber);
	}

	@Override
	public List<MaterialDTO> getMaterialByParty(Party party) {

		return materialMapper.modelToDTOList(materialRepository.findByParty(party));
	}

	@Override
	public List<MaterialDTO> getMaterialByPartyAndSupplyTypeAndNameLike(Long partyId, Long supplyTypeId, String name, String buyOrSell){


		if(buyOrSell.equals(ApplicationConstants.TXN_SELL) && (globalSettingService.getAllGlobalSettings().get(0).getShowOnlyCustomerMaterialInSales() == 1)){
			
			return materialMapper.modelToDTOList(materialRepository.materialSearchWithParty(partyId, name, name, supplyTypeId, ApplicationConstants.MATERIAL_TYPE_JOBWORK));
			
		}else{
			return materialMapper.modelToDTOList(materialRepository.materialSearch(name,name, supplyTypeId, ApplicationConstants.MATERIAL_TYPE_JOBWORK));
		}
		

	}

	@Override
	public Boolean updateMaterialHsnAndSpec(Long materialId, String hsnCode, String spec) {
		
		Material material = materialRepository.getOne(materialId);
		material.setHsnCode(hsnCode);
		material.setSpecification(spec);
		
		try {
			
			materialRepository.save(material);
			return true;
		}catch(Exception e) {
			log.info("Error saving material: "+e);
			return false;
		}
	}

	@Override
	public List<MaterialDTO> getMaterialsByName(String name,Long id){

		String upperName = name.toUpperCase().replace(" ","");

		List<Material> outMaterials = materialRepository.materialSearchByName(upperName,id);
		log.info("outmaterials are:"+ outMaterials);
		return materialMapper.modelToDTOList(outMaterials);

	}

	@Override
	public List<MaterialStockCheckDTO> validateMinimumStock(MaterialStockCheckDTOWrapper inMaterials){
		
		List<Long> materialIds = inMaterials.getMaterialStockCheckDTOs()
				
							    .stream()
								.mapToLong(material -> material.getId())
								.boxed()
								.collect(Collectors.toList());
		
		List<Material> materials = materialRepository.findByIdIn(materialIds);
		
		List<MaterialStockCheckDTO> outArray = new ArrayList<MaterialStockCheckDTO>();
		
		inMaterials.getMaterialStockCheckDTOs().forEach(inMaterial -> {
			
			Material material = materials
						.stream()
						.filter(m -> m.getId().equals(inMaterial.getId()))
						.findFirst()
						.get();
			log.info("material: "+material);
			log.info("inMaterial: "+inMaterial);
			Double remainingQuantity=(material.getStock() != null ?  material.getStock() : 0 )- inMaterial.getItemQuantity();
			
			
			if(material.getMinimumStock() != null && material.getMinimumStock() > remainingQuantity) {
				MaterialStockCheckDTO outItem = inMaterial;
				outItem.setMinimumStock(material.getMinimumStock());
				outItem.setName(material.getName());
				outItem.setPartNumber(material.getPartNumber());
				outArray.add(outItem);
			}
			
			
		});
		
		return outArray;
	}

	public Integer materialExistsinTransactions(Long materialId){
		return materialRepository.materialExistsinTransactions(materialId);
	}
}
