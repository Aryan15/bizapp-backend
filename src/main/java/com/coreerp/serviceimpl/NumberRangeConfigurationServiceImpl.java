package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import com.coreerp.dao.PrintTemplateRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.NumberRangeConfigurationRepository;
import com.coreerp.dto.NumberRangeConfigurationDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.NumberRangeConfigurationMapper;
import com.coreerp.model.NumberRangeConfiguration;
import com.coreerp.model.TransactionType;
import com.coreerp.service.NumberRangeConfigurationService;

@Service
public class NumberRangeConfigurationServiceImpl implements NumberRangeConfigurationService {

	final static Logger log = LogManager.getLogger(NumberRangeConfigurationServiceImpl.class);
	
	@Autowired
	NumberRangeConfigurationRepository numberRangeConfigurationRepository ;
	
	@Autowired
	NumberRangeConfigurationMapper numberRangeConfigurationMapper;

	@Autowired
	PrintTemplateRepository printTemplateRepository;
	
	@Override
	public List<NumberRangeConfigurationDTO> getAll() {
		
		List<NumberRangeConfiguration>  outList = numberRangeConfigurationRepository.findAll();
		
		//log.info("returning.." + outList);
		
		return numberRangeConfigurationMapper.modelToDTOList(outList);
	}

	@Override
	public NumberRangeConfiguration getByTransactionType(TransactionType transactionType) {
		return numberRangeConfigurationRepository.findByTransactionType(transactionType);
	}
	
	@Override
	public NumberRangeConfigurationDTO getDTOByTransactionType(TransactionType transactionType){
		return numberRangeConfigurationMapper.modelToDTOMap(numberRangeConfigurationRepository.findByTransactionType(transactionType));	
	}

	@Override
	public List<NumberRangeConfigurationDTO> saveAll(List<NumberRangeConfigurationDTO> dtoList) {
		
		List<NumberRangeConfigurationDTO> outList = new  ArrayList<NumberRangeConfigurationDTO>();
		
		dtoList.forEach(dto -> {
			NumberRangeConfiguration nout = numberRangeConfigurationRepository.save(numberRangeConfigurationMapper.dtoToModelMap(dto));
			outList.add(numberRangeConfigurationMapper.modelToDTOMap(nout));
		});
		
		return outList;
	
	}

	
	@Override
	public List<NumberRangeConfigurationDTO> updatePrintTemplete(List<NumberRangeConfigurationDTO> dtoList) {
		
		List<NumberRangeConfigurationDTO> outList = new  ArrayList<NumberRangeConfigurationDTO>();
		
		
		dtoList
		.forEach(dto -> {
			NumberRangeConfiguration toSave = numberRangeConfigurationRepository.getOne(dto.getId());
			toSave.setPrintTemplate(dto.getPrintTemplateId() != null ? printTemplateRepository.getOne(dto.getPrintTemplateId()) : null);
			NumberRangeConfiguration nout = numberRangeConfigurationRepository.save(toSave);
			outList.add(numberRangeConfigurationMapper.modelToDTOMap(nout));
		});
		
		
		
		return outList;
	
	}
	
	@Override
	public TransactionResponseDTO delete(Long id) {
		
		TransactionResponseDTO res = new TransactionResponseDTO();
		
		String deleteStatus = "Transaction setting deleted successfully";
		Integer responseStatus = 1;
		
		try {
			numberRangeConfigurationRepository.deleteById(id);
		}catch(Exception e){
			deleteStatus = "Cannot Delete quotation";
			responseStatus = 0;
			
		}
		
		res.setResponseString(deleteStatus);
		res.setResponseStatus(responseStatus);
		return res;
	}

	@Override
	public Boolean updatePrintTopMargin(NumberRangeConfigurationDTO dto) {
		NumberRangeConfiguration toSave = numberRangeConfigurationRepository.getOne(dto.getId());
		toSave.setPrintTemplateTopSize(dto.getPrintTemplateTopSize());
		toSave.setPrintHeaderText(dto.getPrintHeaderText());
		toSave.setAllowShipingAddress(dto.getAllowShipingAddress());
		NumberRangeConfiguration nout = numberRangeConfigurationRepository.save(toSave);
		return true;
	}


}
