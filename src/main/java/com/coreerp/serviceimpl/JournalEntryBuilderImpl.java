package com.coreerp.serviceimpl;

import java.util.Date;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.JournalEntry;
import com.coreerp.model.PayableReceivableItem;
import com.coreerp.service.AccountPostingRuleService;
import com.coreerp.service.JournalEntryBuilder;
import com.coreerp.service.TransactionTypeService;

@Component
public class JournalEntryBuilderImpl implements JournalEntryBuilder {
	
	final static Logger log = LogManager.getLogger(JournalEntryBuilderImpl.class);
	
	@Autowired
	AccountPostingRuleService accountPostingRuleService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	
	@Autowired
	TransactionTypeRepository transactionTypeRepository ; 

	@Override
	public JournalEntry getReversalEntry(String transactionId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JournalEntry makeCustomerInvoiceJournalEntry(InvoiceHeader invoiceHeader) {
		
		log.info("in makeCustomerInvoiceJournalEntry: "+invoiceHeader);
		JournalEntry je = new JournalEntry();
		
		je.setAmount(invoiceHeader.getGrandTotal());
		je.setBusinessDate(new Date());
		je.setCreditAccount(accountPostingRuleService.getByTransactionType(transactionTypeRepository.findByName(invoiceHeader.getInvoiceType().getName())).getCreditAccount());
		je.setDebitAccount(accountPostingRuleService.getByTransactionType(transactionTypeRepository.findByName(invoiceHeader.getInvoiceType().getName())).getDebitAccount());
		je.setIsReversal(null);
		je.setParty(invoiceHeader.getParty());
		je.setRemarks(null);
		je.setTransactionDate(invoiceHeader.getInvoiceDate());
		je.setTransactionId(invoiceHeader.getId());
		je.setTransactionIdSequence(1);
		je.setTransactionType(transactionTypeService.getByName(ApplicationConstants.INVOICE_TYPE_CUSTOMER));
		
		log.info("returning journal entry: "+je);
		return je;
	}

	@Override
	public JournalEntry makeSupplierPayableJournalEntry(PayableReceivableItem payableReceivableItem) {

		JournalEntry je = new JournalEntry();
		
		je.setAmount(payableReceivableItem.getPayingAmount());
		je.setBusinessDate(new Date());
		je.setCreditAccount(accountPostingRuleService.getByTransactionType(transactionTypeRepository.findByName(ApplicationConstants.PR_SUPPLIER_PAYABLE)).getCreditAccount());
		je.setDebitAccount(accountPostingRuleService.getByTransactionType(transactionTypeRepository.findByName(ApplicationConstants.PR_SUPPLIER_PAYABLE)).getDebitAccount());
		je.setIsReversal(null);
		je.setParty(payableReceivableItem.getPayableReceivableHeader().getParty());
		je.setRemarks(null);
		je.setTransactionDate(payableReceivableItem.getPayableReceivableHeader().getPaymentDocumentDate());
		je.setTransactionId(payableReceivableItem.getId());
		je.setTransactionIdSequence(1);
		je.setTransactionType(transactionTypeService.getByName(ApplicationConstants.PR_SUPPLIER_PAYABLE));
		
		
		return je;
	}

	@Override
	public JournalEntry makeCustomerReceivableJournalEntry(PayableReceivableItem payableReceivableItem) {
		JournalEntry je = new JournalEntry();
		
		je.setAmount(payableReceivableItem.getPayingAmount());
		je.setBusinessDate(new Date());
		je.setCreditAccount(accountPostingRuleService.getByTransactionType(transactionTypeRepository.findByName(ApplicationConstants.PR_CUSTOMER_RECEIVABLE)).getCreditAccount());
		je.setDebitAccount(accountPostingRuleService.getByTransactionType(transactionTypeRepository.findByName(ApplicationConstants.PR_CUSTOMER_RECEIVABLE)).getDebitAccount());
		je.setIsReversal(null);
		je.setParty(payableReceivableItem.getPayableReceivableHeader().getParty());
		je.setRemarks(null);
		je.setTransactionDate(payableReceivableItem.getPayableReceivableHeader().getPaymentDocumentDate());
		je.setTransactionId(payableReceivableItem.getId());
		je.setTransactionIdSequence(1);
		je.setTransactionType(transactionTypeService.getByName(ApplicationConstants.PR_CUSTOMER_RECEIVABLE));
		
		return je;
	}

	@Override
	public JournalEntry makeIncomingDeliveryChallanJournalEntry(DeliveryChallanHeader deliveryChallanHeader) {
		JournalEntry je = new JournalEntry();
		
		
		
		
		return je;
	}

}
