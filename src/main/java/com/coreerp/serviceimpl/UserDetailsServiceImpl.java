package com.coreerp.serviceimpl;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.coreerp.model.User;
import com.coreerp.service.UserService;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService{

	private static Logger logger = LogManager.getLogger(UserDetailsServiceImpl.class.getName());
	
	@Autowired
	UserService userService;
	
	@Autowired
	@Qualifier(value = "userToUserDetails")
	private Converter<User, UserDetails> userUserDetailsConverter;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		//return userUserDetailsConverter.convert(userService.findByEmail(email));
		logger.info("in loadUserByUsername: "+username);
		return userUserDetailsConverter.convert(userService.findByUsername(username));
	}

}
