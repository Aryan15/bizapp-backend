package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.model.*;
import com.coreerp.service.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.DeliveryChallanHeaderRepository;
import com.coreerp.dao.DeliveryChallanItemRepository;
import com.coreerp.dao.InvoiceItemRepository;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.dao.TransactionStatusChangeRepository;
import com.coreerp.dto.DeliveryChallanDTO;
import com.coreerp.dto.DeliveryChallanHeaderWrapperDTO;
import com.coreerp.dto.DeliveryChallanItemDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.DeliveryChallanHeaderMapper;
import com.coreerp.mapper.DeliveryChallanItemMapper;

@Service
public class DeliveryChallanServiceImpl implements DeliveryChallanService {

	final static Logger log = LogManager.getLogger(DeliveryChallanServiceImpl.class);
	
	@Autowired
	DeliveryChallanHeaderRepository deliveryChallanHeaderRepository;
	
	@Autowired
	InvoiceItemRepository invoiceItemRepository;
	
	@Autowired
	DeliveryChallanItemRepository deliveryChallanItemRepository;
	
	@Autowired
	StatusService statusService;
	
	@Autowired 
	PartyService partyService;
	
	@Autowired
	DeliveryChallanHeaderMapper deliveryChallanHeaderMapper;
	
	@Autowired
	DeliveryChallanItemMapper deliveryChallanItemMapper;

	@Autowired
	DeliveryChallanStatusService deliveryChallanStatusService;
	
	//@Autowired
	//DeliveryChallanBackTrackingService deliveryChallanBackTrackingService ; 
	
	@Autowired
	DeliveryChallanBackTrackingServiceNew deliveryChallanBackTrackingServiceNew ;
	
	//boolean setStatus = true;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	//MaterialService materialService;
	MaterialRepository materialRepository;
	
	@Autowired
	TransactionStatusChangeRepository transactionStatusChangeRepository;
	
	@Autowired
	DeliveryChallanInventoryService deliveryChallanInventoryService;
	

	@Autowired
	MaterialService materialService;

	@Autowired
	FinancialYearService financialYearService;
	
	@Override
	public DeliveryChallanDTO save(DeliveryChallanHeaderWrapperDTO deliveryChallanHeaderWrapperDTO) {
		
		DeliveryChallanDTO deliveryChallanDTO = deliveryChallanHeaderWrapperDTO.getDeliveryChallanHeader();
		
		DeliveryChallanHeader dcPre = new DeliveryChallanHeader();
		List<DeliveryChallanItem> dcItemsPre = new ArrayList<DeliveryChallanItem>();
		
		if(deliveryChallanDTO.getId() != null){
			deliveryChallanHeaderRepository.getOne(deliveryChallanDTO.getId()).getDeliveryChallanItems().forEach(item -> {
				DeliveryChallanItem dcItemPre = new DeliveryChallanItem();
				dcItemPre.setId(item.getId());
						if(deliveryChallanDTO.getStatusName()!=null && deliveryChallanDTO.getStatusName().equals(ApplicationConstants.TRANSACTION_CANCELLED)) {
							dcItemPre.setQuantity(0.0);
						}
						else{
							dcItemPre.setQuantity(item.getQuantity());
						}
				dcItemPre.setMaterial(item.getMaterial());
				//dcItemPre.setPurchaseOrderHeader(item.getPurchaseOrderHeader());
				dcItemPre.setPurchaseOrderItem(item.getPurchaseOrderItem());
				dcItemPre.setSourceDeliveryChallanItem(item.getSourceDeliveryChallanItem());
				dcItemsPre.add(dcItemPre);
			});
		}
		
		//invoiceHeaderPre.setInvoiceItems(invoiceDTOIn.getId() != null ? invoiceHeaderRepository.getOne(invoiceDTOIn.getId()).getInvoiceItems() : null);
		
		dcPre.setDeliveryChallanItems(dcItemsPre);
		
		
		
		
		if(deliveryChallanHeaderWrapperDTO.getItemToBeRemove()!=null && deliveryChallanHeaderWrapperDTO.getItemToBeRemove().size() > 0 ){
			deliveryChallanHeaderWrapperDTO.getItemToBeRemove().forEach(id -> {
				deleteItem(id);
			});
		}
		
	

		DeliveryChallanHeader dc = deliveryChallanHeaderMapper.dtoToModelMap(deliveryChallanDTO);
		
		//Update material spec and hsn
		deliveryChallanDTO.getDeliveryChallanItems()
		.forEach(DeliveryChallanItem -> {
		 	materialService.updateMaterialHsnAndSpec(DeliveryChallanItem.getMaterialId(), DeliveryChallanItem.getHsnOrSac(), DeliveryChallanItem.getSpecification() );
		});
		
		//log.info("mapped dc header: "+dc);
	
		if (saveAllowed(dc)){

			dc.setStatus(getDCStatus(dc));

			
			List<DeliveryChallanItem> dcItems = deliveryChallanItemMapper.dtoToModelList(deliveryChallanDTO.getDeliveryChallanItems());
			//log.info(dcItems.toString()+"---------------------");
			//set header for all items
			
			dcItems.stream().forEach(dcItem -> dcItem.setDeliveryChallanHeader(dc));
			
			//log.info("mapped dc item: "+dcItems);
			
			//log.info("mapped dc item header id: "+dcItems.get(0).getAmount());
			
			dc.setDeliveryChallanItems(dcItems);
			
	
			if(dc.getStatus().getName().equals(ApplicationConstants.DC_STATUS_CANCELLED)){
				Status status=statusService.getStatusByTransactionTypeAndName(dc.getDeliveryChallanType(), ApplicationConstants.DC_STATUS_CANCELLED);
				dc.setStatus(status);
			}
			
			DeliveryChallanHeader dcOut =  deliveryChallanHeaderRepository.save(dc);
			
			//log.info("after save items.."+dcOut.getDeliveryChallanItems());
			
			//update material
			materialHsnOrSacUpdate(deliveryChallanDTO);
			
			//Stock update in case of jobwork DC
			

			
			
			
			statusChangeEntry(dcOut);

            if (dcOut.getStatus().getName().equals(ApplicationConstants.DC_STATUS_CANCELLED)) {
                deleteStatusChangeEntry(dcOut.getId());

            }
			
			//log.info("after statusChangeEntry ");
			
			deliveryChallanBackTrackingServiceNew.updateTransactionStatus(dcOut, dcPre);

			deliveryChallanInventoryService.updateInventory(dcOut, dcPre);


			List<DeliveryChallanItem> deliveryChallanItemUpdate = new ArrayList<DeliveryChallanItem>();
			if(dcOut.getStatus().getName().equals(ApplicationConstants.TRANSACTION_CANCELLED)){
				dcOut.getDeliveryChallanItems().forEach(item -> {
					if(item.getSourceDeliveryChallanItem()!=null){
						item.setSourceDeliveryChallanItem(null);
						deliveryChallanItemUpdate.add(item);

					}



				});

				deliveryChallanItemUpdate.forEach(item ->{
					deliveryChallanItemRepository.save(item);

				});

			}
			
			return deliveryChallanHeaderMapper.modelToDTOMap(dcOut);
		}
		else{
			return null;
		}
	}

	private boolean saveAllowed(DeliveryChallanHeader dc){
		
		//Not allow to delete if incoming status is deleted/Cancelled but there is a reference in invoice
		boolean returnValue = false;
		
		if(dc.getStatus() != null){
			
			
			String dcStatus = dc.getStatus().getName();
			
			if((dcStatus.equals(ApplicationConstants.DC_STATUS_DELETED))){
				//deliveryChallanHeaderOut = cancelledStatus(in);
				
				List<InvoiceItem> invItems = invoiceItemRepository.findByDeliveryChallanHeader(dc.getId());
				
				if(invItems.isEmpty()){
					
					returnValue = true;					
					
				}
			}else{
				returnValue = true;
			}

		}else{
			returnValue = true;
		}
		
		return returnValue;
		
	}
	
	private void materialHsnOrSacUpdate(DeliveryChallanDTO dcHeader){
		

		dcHeader.getDeliveryChallanItems().forEach(item -> {
			
			Material material = materialRepository.getOne(item.getMaterialId()); // materialService.getMaterialById(item.getMaterial().getId());
			
			Boolean updateRequired = false;
			
			

					
			
			if(item.getHsnOrSac()!= null && (!item.getHsnOrSac().equals(material.getHsnCode()))){
				
				material.setHsnCode(item.getHsnOrSac());
				updateRequired = true;
			}
			
			//log.info("updateRequired: "+updateRequired);
			
			if(updateRequired){
				
				materialRepository.save(material);
			}
			
			
		});
		
	}

	private Status getDCStatus(DeliveryChallanHeader dc) {
		
		/*if(poHeader.getStatus() == null || poHeader.getStatus().isEmpty())
			return purchaseOrderStatusService.getStatus(poHeader);
		else
			return poHeader.getStatus();
			*/
	//	log.info(dc.getStatus()+" dc status");
		if(dc.getStatus() == null ) {

			return deliveryChallanStatusService.getStatus(dc);

		}

		else {
			return dc.getStatus();
		}


	}


	@Override
	public List<DeliveryChallanDTO> getByParty(Long partyId) {
		return deliveryChallanHeaderMapper.modelToDTOList(deliveryChallanHeaderRepository.findByParty(partyService.getPartyObjectById(partyId)));
	}




	@Override
	public List<DeliveryChallanDTO> getAll() {
		return deliveryChallanHeaderMapper.modelToDTOList(deliveryChallanHeaderRepository.findAll());
	}


	@Override
	public DeliveryChallanDTO getById(String id) {
		return deliveryChallanHeaderMapper.modelToDTOMap(deliveryChallanHeaderRepository.getOne(id));
	}


	@Override
	public DeliveryChallanHeader getModelById(String id) {
		return deliveryChallanHeaderRepository.getOne(id);
	}


	@Override
	public DeliveryChallanItem getItemModelById(String id) {
		return deliveryChallanItemRepository.getOne(id);
	}


	@Override
	public Long getMaxDCNumber(String prefix) {
		return deliveryChallanHeaderRepository.getMaxDCNumber(prefix);
	}


	@Override
	public List<DeliveryChallanDTO> getByPartyAndTransactionTypeWithNonZeroBalanceQuantity(Long partyId, Long transactionTypeId) {
		return deliveryChallanHeaderMapper.modelToDTOList(deliveryChallanHeaderRepository.findByPartyAndTransactionTypeWithNonZeroBalanceQuantity(
				partyService.getPartyObjectById(partyId),
				transactionTypeService.getModelById(transactionTypeId)));
	}


	@Override
	public List<DeliveryChallanDTO> getByDCNumber(String dcNumber) {
		return deliveryChallanHeaderMapper.modelToDTOList(deliveryChallanHeaderRepository.findByDeliveryChallanNumber(dcNumber));
	}


	@Override
	public List<DeliveryChallanDTO> getByPartyAndStatusNotIn(Party party, List<Status> statuses) {
		return deliveryChallanHeaderMapper.modelToDTOList(deliveryChallanHeaderRepository.findByPartyAndStatusNotIn(party, statuses));
	}

	@Override
	public List<DeliveryChallanDTO> getByPartyAndTransactionTypeWithNonZeroBalanceQuantityAndStatusNotIn(Long partyId,
			String transactionTypeName, List<Status> statuses, TransactionType requestingTransactionType){
		
		Party party = partyService.getPartyObjectById(partyId);
		TransactionType dcType = transactionTypeService.getByName(transactionTypeName);
		//log.info("dcType"+dcType);
		//return
		List<DeliveryChallanDTO> returnList = 
				deliveryChallanHeaderMapper.modelToDTOList(
				deliveryChallanHeaderRepository.findByPartyAndDeliveryChallanTypeAndStatusNotIn(party, dcType,statuses));
		

		List<DeliveryChallanDTO> dcDTOList = new ArrayList<DeliveryChallanDTO>();
		
		
		
		returnList.forEach(dc -> {	
			
			DeliveryChallanDTO dcDTO = dc; //new DeliveryChallanDTO();
			List<DeliveryChallanItemDTO> dcItem = new ArrayList<DeliveryChallanItemDTO>();

			List<String> invTransList = Arrays.asList(ApplicationConstants.INVOICE_TYPE_CUSTOMER,
					ApplicationConstants.INVOICE_TYPE_SUPPLIER,
					ApplicationConstants.INVOICE_TYPE_PROFORMA,
					ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE,
					ApplicationConstants.INVOICE_TYPE_INCOMING_JW_INVOICE
					);
			//log.info("requesting TransactionType Name :" + requestingTransactionType.getName());
			//log.info("invTransList.contains(requestingTransactionType.getName()"+invTransList.contains(requestingTransactionType.getName()));

			if(invTransList.contains(requestingTransactionType.getName()))
			{

				dcItem.addAll(dc.getDeliveryChallanItems().stream().filter(dci -> dci.getInvoiceBalanceQuantity() > 0).collect(Collectors.toList()));
				dcDTO.setDeliveryChallanItems(dcItem);
				//log.info("in invoice fill..."+ dc.getId()+ " : "+dcItem.size());
			}

			List<String> dcTransList = Arrays.asList(
					ApplicationConstants.INCOMING_JOBWORK_OUT_DC,
					ApplicationConstants.OUTGOING_JOBWORK_IN_DC
			);
			//log.info("dcTransList.contains(requestingTransactionType.getName()"+dcTransList.contains(requestingTransactionType.getName()));
			if(dcTransList.contains(requestingTransactionType.getName()))
			{
				dcItem.addAll(dc.getDeliveryChallanItems().stream().filter(dci -> (dci.getDcBalanceQuantity() != null ? dci.getDcBalanceQuantity() : 0) > 0).collect(Collectors.toList()));
				dcDTO.setDeliveryChallanItems(dcItem);
				//log.info("in dc fill..."+ dc.getId()+ " : " +dcItem.size());
			}
			
			dcDTOList.add(dcDTO);
		});
		
		


		
		
//		returnList.stream()
//					.forEach(action);
		
		return dcDTOList;
	}


	@Override
	public TransactionResponseDTO deleteItem(String id){
		TransactionResponseDTO res = new TransactionResponseDTO();
		res.setResponseStatus(1);
		String deleteStatus = "DC Item Deleted Successfully";
		try {
		DeliveryChallanItem dcDelete = deliveryChallanItemRepository.getOne(id);
		
		//deliveryChallanBackTrackingServiceNew.itemDelete(dcDelete);
		
		deliveryChallanItemRepository.delete(dcDelete);
		
		}catch(Exception e){
			deleteStatus = "Cannot Delete DC Item";
			res.setResponseStatus(0);
		}
		
		res.setResponseString(deleteStatus);
		return res;
	}


	@Override
	public TransactionResponseDTO delete(String id) {
		TransactionResponseDTO res = new TransactionResponseDTO();
		
		String deleteStatus = "DC Deleted Successfully";
		Integer deleteStatusNum = 1;
		//log.info("1");
		DeliveryChallanHeader dchPre = deliveryChallanHeaderRepository.getOne(id);
		//log.info(dchPre.getStatus().getName());
		Status originalStatus = dchPre.getStatus(); 
		try {
			DeliveryChallanHeader dchDelete = deliveryChallanHeaderRepository.getOne(id);
			
			//log.info("2");
			//dchDebug = deliveryChallanHeaderRepository.getOne(id);
			//log.info(dchDebug.getStatus().getName());
			
			//log.info("dchDelete.getDeliveryChallanId(), dchDelete.getDeliveryChallanType(): "+dchDelete.getDeliveryChallanId() + " :: " +dchDelete.getDeliveryChallanType());

			Long dcCount = 0l;
			String commonSequence=deliveryChallanHeaderRepository.getCommonSequence(dchDelete.getDeliveryChallanType().getId());
			List<Long> commonSequenceList = new ArrayList<>();
			if(commonSequence!=null) {
				commonSequenceList = Arrays.stream(commonSequence.split(",")).map(Long::parseLong).collect(Collectors.toList());
			}
			else {
				commonSequenceList.add(dchDelete.getDeliveryChallanType().getId());
			}
			 dcCount=deliveryChallanHeaderRepository.countMaxDcCount(dchDelete.getDeliveryChallanId(),commonSequenceList,dchDelete.getFinancialYear().getId());

			//log.info("3: "+dcCount);
			//dchDebug = deliveryChallanHeaderRepository.getOne(id);
			//log.info(dchDebug.getStatus().getName());
			
			if(dcCount > 0){
				
				deleteStatus = "Cannot delete older Delivery Challan";
				deleteStatusNum = 0;
				dchDelete.setStatus(originalStatus);
				//log.info("4");
				//dchDebug = deliveryChallanHeaderRepository.getOne(id);
				//log.info(dchDebug.getStatus().getName());
				
			}else{
			
				//log.info("5");
				//dchDebug = deliveryChallanHeaderRepository.getOne(id);
				//log.info(dchDebug.getStatus().getName());
				if(saveAllowed(dchDelete)){
					
					//log.info("6");
					//dchDebug = deliveryChallanHeaderRepository.getOne(id);
					//log.info(dchDebug.getStatus().getName());
					
					dchDelete.setStatus(statusService.getStatusByTransactionTypeAndName(dchDelete.getDeliveryChallanType(), ApplicationConstants.DC_STATUS_DELETED));
					
					deleteStatusChangeEntry(dchDelete.getId());
					
					deliveryChallanBackTrackingServiceNew.updateTransactionStatus(dchDelete, null);
					
					
					//Manage inventory
					String updateInventoryStatus = deliveryChallanInventoryService.updateInventory(dchDelete, null);
					
					//log.info("updateInventoryStatus: "+updateInventoryStatus);
					
					dchDelete.getDeliveryChallanItems().forEach(item -> {
						deliveryChallanItemRepository.delete(item);
					});
					
					deliveryChallanHeaderRepository.delete(dchDelete);
					
					//log.info("7");
					//dchDebug = deliveryChallanHeaderRepository.getOne(id);
					//log.info(dchDebug.getStatus().getName());
					
				}else{
					deleteStatus = "Cannot Delete DC";
					deleteStatusNum = 0;
					dchDelete.setStatus(originalStatus);
					
					//log.info("8");
					//dchDebug = deliveryChallanHeaderRepository.getOne(id);
					//log.info(dchDebug.getStatus().getName());
				}
			}
				
		}catch(Exception e){
			deleteStatus = "Cannot Delete DC";
			deleteStatusNum = 0;
			log.info("9: ", e);
			//dchDebug = deliveryChallanHeaderRepository.getOne(id);
			//log.info(dchDebug.getStatus().getName());
			throw e;
		}
		
		res.setResponseString(deleteStatus);
		res.setResponseStatus(deleteStatusNum);
		
		//dchDebug = deliveryChallanHeaderRepository.getOne(id);
		//log.info(dchDebug.getStatus().getName());
		
		return res;
	}

	@Override
	public Long getMaxDCNumberByTransactionType(Long transactionTypeId) {
		// TODO Auto-generated method stub
		return deliveryChallanHeaderRepository.getMaxDCNumberByTransactionType(transactionTypeId);
	}

	@Override
	public Long countByDeliveryChallanNumberAndTransactionType(String dcNumber, TransactionType dcType, Long partyId) {
		FinancialYear financialYear = financialYearService.findByIsActive(1);
		Party party = partyService.getPartyObjectById(partyId);
		List<String> buyDCTypes = Arrays.asList(ApplicationConstants.DC_TYPE_SUPPLIER,ApplicationConstants.INCOMING_JOBWORK_IN_DC, ApplicationConstants.OUTGOING_JOBWORK_IN_DC);
		//if(dcType.getName().equals(ApplicationConstants.DC_TYPE_SUPPLIER) || dcType.getName().equals(ApplicationConstants.INCOMING_JOBWORK_IN_DC)) {
		if(buyDCTypes.contains(dcType.getName())){
			return deliveryChallanHeaderRepository.countByDeliveryChallanNumberAndDeliveryChallanTypeAndPartyAndFinancialYear(dcNumber,dcType,party,financialYear);
		}
		else {
			return deliveryChallanHeaderRepository.countByDeliveryChallanNumberAndDeliveryChallanTypeAndFinancialYear(dcNumber,dcType,financialYear);
		}
	}

	private void statusChangeEntry(DeliveryChallanHeader dcHeader){
		
		log.info("in statusChangeEntry: "+dcHeader.getStatus().getName());
		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();
		
		transactionStatusChange.setSourceOperation(null);
		transactionStatusChange.setSourceTransactionId(null);
		transactionStatusChange.setSourceTransactionType(null);
		transactionStatusChange.setSourceTransactionNumber(null);
		transactionStatusChange.setStatus(dcHeader.getStatus());
		log.info(dcHeader.getStatus()+"-------------");
		transactionStatusChange.setTransactionId(dcHeader.getId());
		transactionStatusChange.setTransactionType(dcHeader.getDeliveryChallanType());
		transactionStatusChange.setTransactionNumber(dcHeader.getDeliveryChallanNumber());
		transactionStatusChange.setParty(dcHeader.getParty());

		transactionStatusChangeRepository.save(transactionStatusChange);
		
	}
	
	private void deleteStatusChangeEntry(String id){
		
		List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository.findByTransactionId(id);
		
		transactionStatusChangeList.forEach(tr -> {
			transactionStatusChangeRepository.delete(tr);
		});
		
		transactionStatusChangeList = transactionStatusChangeRepository.findBySourceTransactionId(id);
		
		transactionStatusChangeList.forEach(tr -> {
			transactionStatusChangeRepository.delete(tr);
		});
		
		
	}

	@Override
	public DeliveryChallanHeader findByNoteId(String noteId) {

		DeliveryChallanHeader deliveryChallanHeader =deliveryChallanHeaderRepository.findByJwNoteId(noteId);
		return deliveryChallanHeader;
	}
}
