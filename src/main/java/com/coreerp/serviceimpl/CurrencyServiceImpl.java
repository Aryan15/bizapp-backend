package com.coreerp.serviceimpl;

import com.coreerp.dao.CurrencyRepository;
import com.coreerp.dto.CurrencyDTO;
import com.coreerp.mapper.CurrencyMapper;
import com.coreerp.model.Currency;
import com.coreerp.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyServiceImpl implements CurrencyService {


    @Autowired
    CurrencyRepository currencyRepository;

    @Autowired
    CurrencyMapper currencyMapper;
    @Override
    public List<CurrencyDTO> getAllCurrency() {
        List<Currency> currencyList = currencyRepository.findAll();
        List<CurrencyDTO>  currencyDTO=currencyMapper.modelToDTOList(currencyList);

        return currencyDTO;


    }

    @Override
    public CurrencyDTO getCurrencyById(Long id) {
        Currency currency = currencyRepository.getOne(id);
        CurrencyDTO  currencyDTO=currencyMapper.modelToDTOMap(currency);

        return currencyDTO;


    }
}
