package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.controller.TaxController;
import com.coreerp.dao.TaxRepository;
import com.coreerp.dto.TaxDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.TaxMapper;
import com.coreerp.model.Tax;
import com.coreerp.service.TaxService;

@Service
public class TaxServiceImpl implements TaxService {

	final static Logger log = LogManager.getLogger(TaxServiceImpl.class);
	
	@Autowired
	TaxRepository taxRepository;
	
	@Autowired
	TaxMapper taxMapper;
	
	@Override
	public Tax getById(Long id) {
		return taxRepository.getOne(id);
	}

	@Override
	public TaxDTO saveTax(TaxDTO taxDTO) {
		return taxMapper.modelToDTOMap(taxRepository.save(taxMapper.dtoToModelMap(taxDTO)));
	}

	@Override
	public List<TaxDTO> getAll() {

		List l =taxRepository.findAll();
		List taxes = new ArrayList();
		for(int index =1;index<l.size();index++){
			taxes.add(l.get(index));
		}
		return taxMapper.modelToDTOList(taxes);
	}

	@Override
	public TransactionResponseDTO delete(Long id) {
		
		TransactionResponseDTO ret = new TransactionResponseDTO();
		
		ret.setResponseString("Tax deleted successfully");
		ret.setResponseStatus(1);
		
		try {
			taxRepository.deleteById(id);
		} catch(Exception e){
			log.info("Deletion exception: "+e);
			ret.setResponseString("Error deleting tax");
			ret.setResponseStatus(0);
		}
		
		return ret;
	}

}
