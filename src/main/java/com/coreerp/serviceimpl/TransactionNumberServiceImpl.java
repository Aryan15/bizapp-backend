package com.coreerp.serviceimpl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.coreerp.service.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dto.TransactionNumber;
import com.coreerp.model.FinancialYear;
import com.coreerp.model.NumberRangeConfiguration;
import com.coreerp.model.TransactionType;

@Service
public class TransactionNumberServiceImpl implements TransactionNumberService {

	final static Logger log = LogManager.getLogger(TransactionNumberServiceImpl.class);

	@Autowired
	NumberRangeConfigurationService numberRangeConfigurationService;

	@Autowired
	InvoiceService invoiceService;

	@Autowired
	DeliveryChallanService deliveryChallanService;

	@Autowired
	GRNService grnService;


	@Autowired
	PayableReceivableService payableReceivableService;

	@Autowired
	QuotationService quotationService;

	@Autowired
	PurchaseOrderService purchaseOrderService;

	@Autowired
	VoucherService voucherService;

	@Autowired
	FinancialYearService financialYearService;


	@Autowired
	PettyCashService pettyCashService;

	@Autowired
	PartyService partyService;

	@Override
	public HashMap<String, Long> getNextTransactionNumber(TransactionType transactionType) {
		log.info("transactionType........." + transactionType);
		NumberRangeConfiguration numConfig = getConfig(transactionType);
		Long maxTransactionNumber = 0l;
		String prefix = getPrefix(transactionType, numConfig);
		String postfix = getPostfix(transactionType, numConfig);
		Integer startNumber = getStartNumber(transactionType, numConfig);
		String delimiter = getDelimiter(transactionType, numConfig);
		String financialYearDate = getFinancialYear(transactionType, numConfig);

			maxTransactionNumber = getMaxTxnNumber(transactionType);

		//log.info("Transaction Type: "+transactionType.getId()+ " : "+transactionType.getName());


		if (maxTransactionNumber == null) {
			//log.info(""+startNumber);
			maxTransactionNumber = startNumber != null ? startNumber.longValue() : 1l;
		} else {
//		maxTransactionNumber = maxTransactionNumber!=null ? maxTransactionNumber+1 : 1l;
			maxTransactionNumber = maxTransactionNumber + 1;
		}

		String maxTxnNumber = getMaxTxnNumberString(maxTransactionNumber, numConfig);

		return makeMap(
				(prefix != null && prefix.trim().length() > 0 ? prefix : "")
						+ (financialYearDate != null && financialYearDate.trim().length() > 0 && prefix != null && prefix.trim().length() > 0 ? delimiter : "")
						+ (financialYearDate != "" ? financialYearDate : "")
						+ (maxTxnNumber != null && financialYearDate != null && financialYearDate.trim().length() > 0 ? delimiter : "")
						+ (maxTxnNumber != null ? maxTxnNumber : "")
						+ (postfix != null && postfix.trim().length() > 0 ? delimiter : "")
						+ (postfix != null && postfix.trim().length() > 0 ? postfix : ""), maxTransactionNumber);


		//return transactionNumber ;
	}

	private String getMaxTxnNumberString(Long maxTransactionNumber, NumberRangeConfiguration numConfig) {
		String returnNumber = "";
		if (numConfig.getIsZeroPrefix() != null && numConfig.getIsZeroPrefix() == 1) {
			String pref = maxTransactionNumber < 9 ? "00" : (maxTransactionNumber < 99 ? "0" : "");
			//String pref = maxTransactionNumber < 99 ? "0" : "";
			returnNumber = pref + maxTransactionNumber;
		} else {
			returnNumber = maxTransactionNumber.toString();
		}
		return returnNumber;
	}

	private Long getMaxTxnNumber(TransactionType transactionType) {
		Long maxTransactionNumber = 0l;
		switch (transactionType.getName()) {
			case ApplicationConstants.INVOICE_TYPE_CUSTOMER:
//				transactionNumber = getNextInvoiceNumber(transactionType);
				maxTransactionNumber = invoiceService.getMaxInvoiceNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.INVOICE_TYPE_SUPPLIER:
				maxTransactionNumber = invoiceService.getMaxInvoiceNumberByTransactionType(transactionType.getId());
//				transactionNumber = getNextInvoiceNumber(transactionType);
				break;
			case ApplicationConstants.INVOICE_TYPE_PROFORMA:
				//log.info("in proforma");
				maxTransactionNumber = invoiceService.getMaxInvoiceNumberByTransactionType(transactionType.getId());
				//log.info("maxTransactionNumber: "+maxTransactionNumber);
				break;
			case ApplicationConstants.DC_TYPE_CUSTOMER:
				maxTransactionNumber = deliveryChallanService.getMaxDCNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.DC_TYPE_SUPPLIER:
				maxTransactionNumber = deliveryChallanService.getMaxDCNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.GRN_TYPE_SUPPLIER:
				maxTransactionNumber = grnService.getMaxGRNNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.NOTE_TYPE_CREDIT:
				maxTransactionNumber = invoiceService.getMaxInvoiceNumberByTransactionType(transactionType.getId()); //noteService.getMaxNoteNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.NOTE_TYPE_DEBIT:
				maxTransactionNumber = invoiceService.getMaxInvoiceNumberByTransactionType(transactionType.getId());//noteService.getMaxNoteNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.PURCHASE_ORDER_TYPE_CUSTOMER:
				maxTransactionNumber = purchaseOrderService.getMaxPONumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.PURCHASE_ORDER_TYPE_SUPPLIER:
				maxTransactionNumber = purchaseOrderService.getMaxPONumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.PR_CUSTOMER_RECEIVABLE:
				maxTransactionNumber = payableReceivableService.getMaxPRNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.PR_SUPPLIER_PAYABLE:
				maxTransactionNumber = payableReceivableService.getMaxPRNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.QUOTATION_TYPE_CUSTOMER:
				maxTransactionNumber = quotationService.getMaxQuotationNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.QUOTATION_TYPE_SUPPLIER:
				maxTransactionNumber = quotationService.getMaxQuotationNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.PURCHASE_ORDER_INCOMING_JOBWORK:
				maxTransactionNumber = purchaseOrderService.getMaxPONumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.PURCHASE_ORDER_OUTGOING_JOBWORK:
				maxTransactionNumber = purchaseOrderService.getMaxPONumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.INCOMING_JOBWORK_IN_DC:
				maxTransactionNumber = deliveryChallanService.getMaxDCNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.INCOMING_JOBWORK_OUT_DC:
				maxTransactionNumber = deliveryChallanService.getMaxDCNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.CASH_VOUCHER:
				maxTransactionNumber = voucherService.getMaxVoucherNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.CHEQUE_VOUCHER:
				maxTransactionNumber = voucherService.getMaxVoucherNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.EXPENSE_VOUCHER:
				maxTransactionNumber = voucherService.getMaxVoucherNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.OUTGOING_JOBWORK_OUT_DC:
				maxTransactionNumber = deliveryChallanService.getMaxDCNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.OUTGOING_JOBWORK_IN_DC:
				maxTransactionNumber = deliveryChallanService.getMaxDCNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.INVOICE_TYPE_INCOMING_JW_INVOICE:
				maxTransactionNumber = invoiceService.getMaxInvoiceNumberByTransactionType(transactionType.getId());
				break;
			case ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE:
				maxTransactionNumber = invoiceService.getMaxInvoiceNumberByTransactionType(transactionType.getId());
				break;

			case ApplicationConstants.PETTY_CASH:
				maxTransactionNumber = pettyCashService.getMaxPettyCashNumberByTransactionType(transactionType.getId());
				break;

			case ApplicationConstants.CUSTOMER_CODE:
				maxTransactionNumber = partyService.getMaxPartyNumberByTransactionType(transactionType.getName());
				break;

			case ApplicationConstants.SUPPLIER_CODE:
				maxTransactionNumber = partyService.getMaxPartyNumberByTransactionType(transactionType.getName());
				break;


			case ApplicationConstants.JOBWORK_PROFORMA_INVOICE:
				maxTransactionNumber = invoiceService.getMaxInvoiceNumberByTransactionType(transactionType.getId());
				break;

			case ApplicationConstants.QUOTATION_TYPE_JOBWORK:
				maxTransactionNumber = quotationService.getMaxQuotationNumberByTransactionType(transactionType.getId());
				break;

			case ApplicationConstants.JOBWORK_CREDIT_NOTE:
				maxTransactionNumber = invoiceService.getMaxInvoiceNumberByTransactionType(transactionType.getId());
				break;

			case ApplicationConstants.SUBCONTRACT_DEBIT_NOTE:
				maxTransactionNumber = invoiceService.getMaxInvoiceNumberByTransactionType(transactionType.getId());
				break;

		}
		return maxTransactionNumber;
	}




	@Override
	public TransactionNumber  getTransactionNumber(TransactionType transactionType){
		
		String invoiceNumber = "";
		Long invId=0l;
		
		TransactionNumber tn = new TransactionNumber();
		
		
		
		for(Map.Entry<String, Long> m : getNextTransactionNumber(transactionType).entrySet()){
			
//			invoiceNumber = m.getKey()+m.getValue();
			invoiceNumber = m.getKey();
			invId = m.getValue();
			
			log.info("hash map: "+ invoiceNumber +":"+invId);
		}
		
		
		tn.setTransactionId(invId);
		tn.setTransactionNumber(invoiceNumber);
		return tn;
	}
	
	private String getPrefix(TransactionType transactionType, NumberRangeConfiguration numberRangeConfiguration){
		
		//NumberRangeConfiguration numberRangeConfiguration = numberRangeConfigurationService.getByTransactionType(transactionType);
		
		String prefix = numberRangeConfiguration!=null ? numberRangeConfiguration.getPrefix(): "";
		
		
		//log.info("Prefix: "+prefix);
		return prefix;
	}
	
	private String getPostfix(TransactionType transactionType, NumberRangeConfiguration numberRangeConfiguration){
		
		//NumberRangeConfiguration numberRangeConfiguration = numberRangeConfigurationService.getByTransactionType(transactionType);
		
		String postfix = numberRangeConfiguration!=null ? numberRangeConfiguration.getPostfix(): "";
		
		
		//log.info("postfix: "+postfix);
		return postfix;
	}
	
	
	private String getDelimiter(TransactionType transactionType, NumberRangeConfiguration numberRangeConfiguration){
		
		//NumberRangeConfiguration numberRangeConfiguration = numberRangeConfigurationService.getByTransactionType(transactionType);
		
	
		String delimiter = numberRangeConfiguration!=null ? numberRangeConfiguration.getDelimiter(): "";
		
		delimiter = delimiter != null ? delimiter : "-";
		
		//log.info("delimiter: "+delimiter);
		return delimiter;
	}
	
	private Integer getStartNumber(TransactionType transactionType, NumberRangeConfiguration numberRangeConfiguration){
		
		//NumberRangeConfiguration numberRangeConfiguration = numberRangeConfigurationService.getByTransactionType(transactionType);
		
		Integer startNumber = numberRangeConfiguration!=null ? numberRangeConfiguration.getStartNumber(): null;
		
		
		//log.info("startNumber: "+startNumber);
		return startNumber;
	}
	
	private NumberRangeConfiguration getConfig(TransactionType transactionType){
		return  numberRangeConfigurationService.getByTransactionType(transactionType);

	}

	private String getFinancialYear(TransactionType transactionType, NumberRangeConfiguration numberRangeConfiguration){
		
		//NumberRangeConfiguration numberRangeConfiguration = numberRangeConfigurationService.getByTransactionType(transactionType);
		//log.info("numberRangeConfiguration: "+numberRangeConfiguration);
		
		int isFinancialYearChecked = numberRangeConfiguration!=null ? (numberRangeConfiguration.getFinancialYearCheck() != null ? numberRangeConfiguration.getFinancialYearCheck() : 0): 0;
		//log.info("isFinancialYearChecked "+isFinancialYearChecked);
		
		if (isFinancialYearChecked ==1)
		{
			FinancialYear financialYear = financialYearService.findByIsActive(1);
		
			Calendar calendar = Calendar.getInstance();
			
			calendar.setTime(financialYear.getStartDate());
			//log.info("calendar year "+calendar.get(Calendar.YEAR));
			String startDate= calendar.get(Calendar.YEAR)+"";
			String NewStartDate = startDate.substring(startDate.length()-2,startDate.length());
		
			
			calendar.setTime(financialYear.getEndDate());
			String EndDate = calendar.get(Calendar.YEAR)+"";
			String NewEndDate = EndDate.substring(EndDate.length()-2,EndDate.length());
		
	
			String financialYearDate = NewStartDate + "-" + NewEndDate;
			//log.info("FinancialYear Date "+financialYearDate);
			
			return financialYearDate;
		}
		else
			return "";
	}
	
	private HashMap<String, Long> makeMap(String prefix, Long maxDCNumber){
	
		HashMap<String, Long> mapOut = new HashMap<String, Long>();
		mapOut.put(prefix, maxDCNumber);
		return mapOut;
	}

}
