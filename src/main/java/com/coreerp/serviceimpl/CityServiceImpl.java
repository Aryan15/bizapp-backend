package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.coreerp.dao.CityRepository;
import com.coreerp.dao.StateRepository;
import com.coreerp.dto.CityDTO;
import com.coreerp.model.City;
import com.coreerp.model.State;
import com.coreerp.service.CityService;

@Service
public class CityServiceImpl implements CityService {

	final static Logger log = LogManager.getLogger(CityServiceImpl.class);
	
	@Autowired
	CityRepository cityRepository;
	
	@Autowired
	StateRepository stateRepository;
	
	@Override
	public CityDTO saveCity(CityDTO cityDTO) {
		City city = DTOToModelMap(cityDTO);
		return modelToDTOMap(cityRepository.save(city));
	}
	
	private CityDTO modelToDTOMap(City model){
		
		CityDTO dto = new CityDTO();
		
		dto.setDeleted(model.getDeleted());
		dto.setId(model.getId());
		dto.setName(model.getName());
		dto.setStateId(model.getState() != null ? model.getState().getId() : null);
		
		return dto;
	}

	private City DTOToModelMap(CityDTO cityDTO) {
		City city = new City();
		city.setId(cityDTO.getId());
		city.setName(cityDTO.getName());
		city.setState(cityDTO.getStateId() != null ? stateRepository.getOne(cityDTO.getStateId()) : null);
		city.setAreas(cityDTO.getAreas());
		city.setDeleted(cityDTO.getDeleted());
		return city;
	}

	/*@Override
	public List<City> getAllCities() {
		return cityRepository.findAll();
	}*/

	@Override
	public List<CityDTO> getAllCities() {
		// TODO Auto-generated method stub
		List<CityDTO> cityDTOList= new ArrayList<CityDTO>();
		List<City> cityList=cityRepository.findAll();
		
		for(City city:cityList){
			CityDTO cityDTO=new CityDTO();
			cityDTO.setId(city.getId());
			cityDTO.setName(city.getName());
			cityDTO.setDeleted(city.getDeleted());
			cityDTO.setStateId(city.getState() != null ? city.getState().getId() : null);
			cityDTOList.add(cityDTO);
		}
		
		//log.info("returning cityDTOList: "+cityDTOList);
		return cityDTOList;
	}
	@Override
	public City getCityById(Long id) {
		return cityRepository.getOne(id);
	}

	@Override
	public List<CityDTO> getAllCitiesForState(Long stateId) {
		State state = stateRepository.getOne(stateId);
		
		List<CityDTO> cityDTOList= new ArrayList<CityDTO>();
		List<City> cityList=cityRepository.findByState(state);
		
		for(City city:cityList){
			CityDTO cityDTO=new CityDTO();
			cityDTO.setId(city.getId());
			cityDTO.setName(city.getName());
			cityDTO.setDeleted(city.getDeleted());
			cityDTO.setStateId(city.getState() != null ? city.getState().getId() : null);
			cityDTOList.add(cityDTO);
		}
		return cityDTOList;
		
	
	}

	@Override
	public City deleteCityById(Long id) {
		this.cityRepository.deleteById(id);
		return null;
	}

	

}
