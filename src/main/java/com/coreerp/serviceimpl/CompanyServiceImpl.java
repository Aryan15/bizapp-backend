package com.coreerp.serviceimpl;

import java.util.List;

import com.coreerp.jwt.JwtTokenUtil;
import com.coreerp.service.StorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.coreerp.dao.AddressRepository;
import com.coreerp.dao.CompanyRepository;
import com.coreerp.dto.CompanyDTO;
import com.coreerp.mapper.CompanyMapper;
import com.coreerp.model.Address;
import com.coreerp.model.Company;
import com.coreerp.service.CompanyService;

import javax.servlet.http.HttpServletRequest;

@Service
public class CompanyServiceImpl implements CompanyService {
	final static Logger log = LogManager.getLogger(CompanyServiceImpl.class);
	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	CompanyMapper companyMapper;
	
	@Autowired
	AddressRepository addressRepository;

	@Value("${jwt.header}")
	private String tokenHeader;

	@Value("${image.path}")
	private String imagePathProperty;

	@Value("${amazonProperties.bucketName}")
	private String bucketName;

	@Autowired
	JwtTokenUtil jwtTokenUtil;

	@Autowired
	@Qualifier("AwsS3StorageService")
	StorageService storageService;

	@Override
	public CompanyDTO saveCompany(CompanyDTO companyDTO) {
		List<Address> addresses = companyDTO.getAddressesListDTO();
		addresses = addressRepository.saveAll(addresses);
		Company company = companyMapper.dtoToModelMap(companyDTO);
		company.setAddresses(addresses);
		return companyMapper.modelToDTOMap(companyRepository.save(company));
	}

	@Override
	public CompanyDTO getCompanyById(Long id) {
		return companyMapper.modelToDTOMap(companyRepository.getOne(id));
	}

	@Override
	public Company getModelById(Long id) {
		return companyRepository.getOne(id);
	}
	
	public Company saveModel(Company company){
		return companyRepository.save(company);
	}

	@Override
	public void delete(Company companyPre) {
		companyRepository.delete(companyPre);
	}

	@Override
	public Company saveCompanyDTO(CompanyDTO companyDTO) {
		Company company = companyMapper.dtoToModelMap(companyDTO);
		
		return companyRepository.save(company);
	}

	@Override
	public String getTenantFromRequest(HttpServletRequest request){
		String authToken = request.getHeader(this.tokenHeader);
		String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);

		return tenantId;
	}

	public String getCompanyLogo(Long id, HttpServletRequest request){
		Company company = companyRepository.getOne(id);
		String fileName = company.getCompanyLogoPath();

		String authToken = request.getHeader(this.tokenHeader);
		String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);
//
//		return  this.imagePathProperty + tenantId + "/" + fileName;

		String fullName = "company/" + tenantId + "/" + fileName;
		return storageService.getPresignedUrl(bucketName, fullName);
	}

	public String getCompanySecondImage(Long id, HttpServletRequest request){
		Company company = companyRepository.getOne(id);
		String fileName = company.getCeritificateImagePath();

		String authToken = request.getHeader(this.tokenHeader);
		String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);

		String fullName = "company/" + tenantId + "/" + fileName;
		return storageService.getPresignedUrl(bucketName, fullName);
	}

	public String getCompanySignatureImage(Long id, HttpServletRequest request){
		Company company = companyRepository.getOne(id);
		String fileName = company.getSignatureImagePath();

		String authToken = request.getHeader(this.tokenHeader);
		String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);

		String fullName = "company/" + tenantId + "/" + fileName;
		return storageService.getPresignedUrl(bucketName, fullName);
	}

}
