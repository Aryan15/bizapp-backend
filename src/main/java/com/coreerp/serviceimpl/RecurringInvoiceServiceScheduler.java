package com.coreerp.serviceimpl;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.EmailSettingRepository;
import com.coreerp.dao.UserRepository;
import com.coreerp.dao.UserTenantRelationRepository;
import com.coreerp.dto.UserTenantRelationDTO;
import com.coreerp.model.EmailSetting;
import com.coreerp.model.User;
import com.coreerp.model.UserTenantRelation;
import com.coreerp.multitenancy.TenantContext;
import com.coreerp.multitenancy.TenantNameFetcher;
import com.coreerp.service.InvoiceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Component
public class RecurringInvoiceServiceScheduler {

    final static Logger log = LogManager.getLogger(RecurringInvoiceServiceScheduler.class);

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    UserTenantRelationRepository userTenantRelationRepository;

    @Autowired
    private EmailSettingRepository emailSettingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TenantNameFetcher tenantResolver;

    @Scheduled(cron = "00 38 15 * * ?") // 4.06PM
    public void startRecurringInvoice() throws Exception {
        log.info("calling invoiceService.generateAutoTransaction()");
        log.info("Recurring Invoice Scheduler started " + new Date());
        log.info("current tenant: " + TenantContext.getCurrentTenant());
        TenantContext.setCurrentTenant(ApplicationConstants.DEFAULT_TENANT_ID);
        log.info("current tenant2: " + TenantContext.getCurrentTenant());
        EmailSetting emailSetting = emailSettingRepository.findAllUser();


        List<UserTenantRelation> usernames = userTenantRelationRepository.findAllUser();


        usernames.forEach(username -> {
            try {
                log.info("processing for : " + username.getName());


                // EmailSetting emailSetting = emailSettingRepository.getOne(1);


                log.info(emailSetting.getEmailFrom());
                tenantResolver.setUsername(username.getUsername());
                ExecutorService es = Executors.newSingleThreadExecutor();
                Future<UserTenantRelationDTO> utrFuture = es.submit(tenantResolver);

                while (!utrFuture.isDone()) {
                    System.out.println("Waiting to complete...");
                    Thread.sleep(300);
                }
                if (utrFuture != null) {
                    UserTenantRelationDTO userTenantRelationCreated = utrFuture.get();
                    es.shutdown();
                    //handle utr == null, user is not found
                    if (userTenantRelationCreated != null) {

                        //Got the tenant, now switch to the context

                        String tenantName = null;
                        try {
                            tenantName = userTenantRelationCreated.getTenant();
                        } catch (Exception e) {
                            log.info("not a valid customer: " + username + " " + e);
                        }
                        if (tenantName != null) {
                            TenantContext.setCurrentTenant(userTenantRelationCreated.getTenant());
                            log.info("setting tenant: " + TenantContext.getCurrentTenant());
                            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(username, null, new ArrayList<>());
                            SecurityContextHolder.getContext().setAuthentication(authentication);
                            log.info(authentication.getCredentials()+"-----------------");
                           User user= userRepository.findByUsername(username.getUsername());

                         // invoiceService.generateAutoTransaction(emailSetting, username.getEmail(), username.getName(), username.getUsername(),user.getPassword());

                            invoiceService.weeklyTransactionReport(emailSetting,username.getEmail(), username.getName(), username.getUsername(),user.getPassword());

                        }
                    } else {
                        log.info("No such user " + username + " exists: ");
                    }

                }
            } catch (Exception e) {
                log.error("SCHEDULER EXCEPTION : " + e);
                log.error(e.getStackTrace());



            }
        });

        log.info("Recurring Invoice Scheduler ended " + new Date());
    }
}
