package com.coreerp.serviceimpl;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.EwayBillTrackingRepository;
import com.coreerp.dto.*;
import com.coreerp.mapper.EwayBillTrackingMapper;
import com.coreerp.model.EwayBillTracking;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.InvoiceItem;
import com.coreerp.service.EwayBillGenerationService;
import com.coreerp.service.EwayBillTrackingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class EwayBillGenerationServiceImpl implements EwayBillGenerationService {


    @Autowired
    EwayBillTrackingService ewayBillTrackingService;

    @Autowired
    EwayBillTrackingRepository ewayBillTrackingRepository;

    @Autowired
    EwayBillTrackingMapper ewayBillTrackingMapper;

    final static Logger log = LogManager.getLogger(EwayBillGenerationServiceImpl.class);

    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Value("${eway.bill.base.url.path}")
    private String eWayBillBaseUrlProperty;

    @Value("${eway.bill.uri.path}")
    private String eWayBillUriProperty;

    @Value("${eway.bill.cancel.uri.path}")
    private String cancelEWayBillUriProperty;

    @Value("${eway.bill.vehicle.update.uri.path}")
    private String eWayBillVehicleUpdateUri;

    @Value("${eway.bill.transporter.id.uri.path}")
    private String eWayBillTransporterIdUpdateUri;


    @Value("${eway.bill.extend.validity.uri.path}")
    private String eWayBillExtendValidityUri;

    @Value("${eway.bill.number.uri.path}")
    private String eWayBillByNumberUri;

    @Value("${eway.bill.version}")
    private String eWayBillVersion;

    @Value("${eway.bill.user.code}")
    private String eWayBillUserCode;

    @Value("${eway.bill.password}")
    private String eWayBillPassword;

    @Value("${eway.bill.client.code}")
    private String eWayBillClientCode;


    @Override
    public List<EwayBillInvoiceResponseDTO> generateEwayBill(EwayBillTrackingDTO ewayBillTrackingDTO) {

        EwayBillTracking ewayBillTracking1= ewayBillTrackingMapper.dtoToModelMap(ewayBillTrackingDTO);
        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTO = new ArrayList<>();
        try {
            EwayBillInvoiceInputDTO ewayBillInvoiceInputDTO = getEwayBillInvoiceInputDTO(ewayBillTracking1);
            ArrayList<Integer> statusId = new ArrayList<Integer>();
            statusId.add(ApplicationConstants.EWAY_BILL_INITIAL_RECORD);
            statusId.add(ApplicationConstants.EWAY_BILL_FAILED);
           // log.info(ewayBillTracking1.getInvoiceHeader().getId()+"-----===+++=+++"+statusId);
            EwayBillTracking ewayBillTracking = ewayBillTrackingService.getActiveEwayBillByInvoiceHeaderAndStatus(ewayBillTracking1.getInvoiceHeader().getId(), statusId);
          //  log.info(ewayBillTracking.toString()+"------------");
            if (ewayBillTracking == null) {
                //initial record
                EwayBillInvoiceResponseDTO response = new EwayBillInvoiceResponseDTO();

                ewayBillTracking = ewayBillTrackingService.saveEwayBillTracking(ewayBillTracking1, response);
            }else{
                // update iscurrentvalue
                System.out.println("ewayBillTracking intial rec  : "+ewayBillTracking);
                ewayBillTracking.setIsCurrent(0);
                ewayBillTrackingRepository.save(ewayBillTracking);
            }

            if (ewayBillTracking.geteWayBillNo() != null) {

                EwayBillInvoiceResponseDTO response = new EwayBillInvoiceResponseDTO();
                response.setFlag(false);
                response.setMessage(ApplicationConstants.EWAY_BILL_FAILED_TO_GENERATE);

                ewayBillInvoiceResponseDTO.add(response);
            } else {
                //Creating the ObjectMapper object
                ObjectMapper mapper = new ObjectMapper();
                //Converting the Object to JSONString
                String jsonString = mapper.writeValueAsString(ewayBillInvoiceInputDTO);
                System.out.println("jsonString : "+jsonString);

                ewayBillInvoiceResponseDTO = callAPIForEwayBillGeneration(ewayBillInvoiceInputDTO);
               // EwayBillTracking finalEwayBillTracking = ewayBillTracking;
                ewayBillInvoiceResponseDTO.forEach(ewayBillResponse -> {
                    ewayBillTrackingService.updateEwayBillTracking(ewayBillTracking1, ewayBillResponse);

                });
            }
            // check if any ewaybill cancelled
            statusId.add(ApplicationConstants.EWAY_BILL_CANCELLED);
            ewayBillTracking = ewayBillTrackingService.getActiveEwayBillByInvoiceHeaderAndStatus(ewayBillTracking1.getInvoiceHeader().getId(), statusId);
            if (ewayBillTracking != null) {

                System.out.println("updating cancelled record  : "+ewayBillTracking);
                ewayBillTracking.setIsCurrent(0);
                ewayBillTrackingRepository.save(ewayBillTracking);
            }
            log.info("input: "+ewayBillInvoiceInputDTO);

        }catch (Exception e) {
            log.info("error: "+e);
            e.printStackTrace();
            EwayBillInvoiceResponseDTO response = new EwayBillInvoiceResponseDTO();
            response.setFlag(false);
            response.setMessage(e.getMessage());

            ewayBillInvoiceResponseDTO.add(response);
        }



        return ewayBillInvoiceResponseDTO;
    }


    @Override
    public List<EwayBillInvoiceResponseDTO> cancelEwayBill(EwayBillTrackingDTO ewayBillTrackingDTO) {
        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTO = new ArrayList<>();
        EwayBillTracking ewayBillTracking= ewayBillTrackingMapper.dtoToModelMap(ewayBillTrackingDTO);
        if(ewayBillTracking.geteWayBillNo() != null){
            EwayBillCancelInputDTO ewayBillCancelInputDTO = getEwayBillCancelInputDTO(ewayBillTracking);
            log.info("ewayBillCancelInputDTO  : "+ewayBillCancelInputDTO);
            ewayBillInvoiceResponseDTO =  callAPIForEwayBillCancellation(ewayBillCancelInputDTO);
        }
        ewayBillInvoiceResponseDTO.forEach(response ->{
            if(response.isFlag()==true){
                ewayBillTrackingService.updateEwayBillTrackingAfterCancel(ewayBillTracking);

            }
        });
        log.info("ewayBillTracking  : "+ewayBillTracking);
        return ewayBillInvoiceResponseDTO;
    }

    @Override
    public List<EwayBillInvoiceResponseDTO> ewayBillVehicleUpdate(EwayBillTrackingDTO ewayBillTrackingDTO) {

        EwayBillTracking ewayBillTracking= ewayBillTrackingMapper.dtoToModelMap(ewayBillTrackingDTO);

        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTO = new ArrayList<>();
        if(ewayBillTracking.geteWayBillNo() != null){
            EwayBillVehicleUpdateInputDTO ewayBillVehicleUpdateInputDTO = getEwayBillVehicleUpdateInputDTO(ewayBillTracking);
            log.info("ewayBillVehicleUpdateInputDTO  : "+ewayBillVehicleUpdateInputDTO);
            ewayBillInvoiceResponseDTO =  callAPIForEwayBillVehicleUpdate(ewayBillVehicleUpdateInputDTO);
        }
        ewayBillInvoiceResponseDTO.forEach(response ->{
            if(response.isFlag()==true){
                ewayBillTrackingService.saveEwayBillTracking(ewayBillTracking,response);
            }
        });
        log.info("ewayBillTracking  : "+ewayBillTracking);
        return ewayBillInvoiceResponseDTO;
    }

    @Override
    public List<EwayBillInvoiceResponseDTO> extendValidityOfEwayBill(EwayBillTrackingDTO ewayBillTrackingDTO) {
        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTO = new ArrayList<>();
        EwayBillTracking ewayBillTracking= ewayBillTrackingMapper.dtoToModelMap(ewayBillTrackingDTO);

        if(ewayBillTracking.geteWayBillNo() != null){
            EwayBillExtendValidityInputDTO ewayBillVehicleUpdateInputDTO = getEwayBillExtendValidityInputDTO(ewayBillTracking);
            log.info("ewayBillVehicleUpdateInputDTO  : "+ewayBillVehicleUpdateInputDTO);
            ewayBillInvoiceResponseDTO =  callAPIForEwayBillExtendValidity(ewayBillVehicleUpdateInputDTO);
        }
        ewayBillInvoiceResponseDTO.forEach(response ->{
            if(response.isFlag()==true){
                ewayBillTrackingService.saveEwayBillTracking(ewayBillTracking, response);
            }
        });
        return ewayBillInvoiceResponseDTO;
    }

    @Override
    public List<EwayBillInvoiceResponseDTO> getEwayBillDetailsByNumber(String eWayBillNum) {
        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTO = new ArrayList<>();
        EwayBillTracking ewayBillTracking= ewayBillTrackingService.getByEWayBillNo(eWayBillNum);
        if(ewayBillTracking.geteWayBillNo() != null){
            EwayBillByNumberInputDTO  ewayBillByNumberInputDTO = getEwayBillByNumberInputDTO(ewayBillTracking);
            log.info("ewayBillByNumberInputDTO  : "+ewayBillByNumberInputDTO);
            ewayBillInvoiceResponseDTO =  callAPIForEwayBillByNumber(ewayBillByNumberInputDTO);
        }

        return ewayBillInvoiceResponseDTO;
    }

    @Override
    public List<EwayBillInvoiceResponseDTO> ewayBillTransporterIdUpdate(EwayBillTrackingDTO ewayBillTrackingDTO) {

        EwayBillTracking ewayBillTracking= ewayBillTrackingMapper.dtoToModelMap(ewayBillTrackingDTO);

        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTO = new ArrayList<>();
        if(ewayBillTracking.geteWayBillNo() != null){
            EwayBillTransporterIDUpdateInputDTO ewayBillTransporterIDUpdateInputDTO = getEwayBillTransporterIDUpdateInputDTO(ewayBillTracking);
            log.info("ewayBillVehicleUpdateInputDTO  : "+ewayBillTransporterIDUpdateInputDTO);
            ewayBillInvoiceResponseDTO =  callAPIForEwayBillTransporterIdUpdate(ewayBillTransporterIDUpdateInputDTO);
        }
        ewayBillInvoiceResponseDTO.forEach(response ->{
            if(response.isFlag()==true){
                ewayBillTrackingService.saveEwayBillTracking(ewayBillTracking,response);
            }
        });
         return ewayBillInvoiceResponseDTO;
    }


    private List<EwayBillInvoiceResponseDTO> callAPIForEwayBillGeneration(EwayBillInvoiceInputDTO ewayBillInvoiceInputDTO) {


        List<EwayBillInvoiceResponseDTO> response = null;
        try {

            WebClient client = WebClient
                    .builder()
                    .baseUrl(eWayBillBaseUrlProperty)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .build();

            response = Arrays.asList(client.post()
                    .uri(eWayBillUriProperty)
                    .body(BodyInserters.fromObject(ewayBillInvoiceInputDTO))
                    .retrieve()
                    .bodyToMono(EwayBillInvoiceResponseDTO[].class)
                    .block());

            response.forEach(ewayBillResponse -> {
                log.info("response: "+ewayBillResponse);
            });


        }catch(Exception e) {
            log.info("Connect exceptio: "+e);
        }

        log.info("response3 : "+response);
        return  response;
    }


    private List<EwayBillInvoiceResponseDTO> callAPIForEwayBillCancellation(EwayBillCancelInputDTO ewayBillCancelInputDTO) {


        List<EwayBillInvoiceResponseDTO> response = null;
        try {

            WebClient client = WebClient
                    .builder()
                    .baseUrl(eWayBillBaseUrlProperty)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .build();

            response = Arrays.asList(client.post()
                    .uri(cancelEWayBillUriProperty)
                    .body(BodyInserters.fromObject(ewayBillCancelInputDTO))
                    .retrieve()
                    .bodyToMono(EwayBillInvoiceResponseDTO[].class)
                    .block());

            response.forEach(ewayBillResponse -> {
                log.info("response: "+ewayBillResponse);
            });


        }catch(Exception e) {
            log.info("Connect exceptio: "+e);
        }

        log.info("response3 : "+response);
        return  response;
    }

    private List<EwayBillInvoiceResponseDTO> callAPIForEwayBillVehicleUpdate(EwayBillVehicleUpdateInputDTO ewayBillVehicleUpdateInputDTO) {


        List<EwayBillInvoiceResponseDTO> response = null;
        try {

            WebClient client = WebClient
                    .builder()
                    .baseUrl(eWayBillBaseUrlProperty)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .build();

            response = Arrays.asList(client.post()
                    .uri(eWayBillVehicleUpdateUri)
                    .body(BodyInserters.fromObject(ewayBillVehicleUpdateInputDTO))
                    .retrieve()
                    .bodyToMono(EwayBillInvoiceResponseDTO[].class)
                    .block());

            response.forEach(ewayBillResponse -> {
                log.info("response: "+ewayBillResponse);
            });


        }catch(Exception e) {
            log.info("Connect exceptio: "+e);
        }

        log.info("response3 : "+response);
        return  response;
    }


    private List<EwayBillInvoiceResponseDTO> callAPIForEwayBillExtendValidity(EwayBillExtendValidityInputDTO ewayBillExtendValidityInputDTO) {


        List<EwayBillInvoiceResponseDTO> response = null;
        try {

            WebClient client = WebClient
                    .builder()
                    .baseUrl(eWayBillBaseUrlProperty)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .build();

            response = Arrays.asList(client.post()
                    .uri(eWayBillExtendValidityUri)
                    .body(BodyInserters.fromObject(ewayBillExtendValidityInputDTO))
                    .retrieve()
                    .bodyToMono(EwayBillInvoiceResponseDTO[].class)
                    .block());

            response.forEach(ewayBillResponse -> {
                log.info("response: "+ewayBillResponse);
            });


        }catch(Exception e) {
            log.info("Connect exceptio: "+e);
        }

        log.info("response3 : "+response);
        return  response;
    }


    private List<EwayBillInvoiceResponseDTO> callAPIForEwayBillByNumber(EwayBillByNumberInputDTO ewayBillByNumberInputDTO) {


        List<EwayBillInvoiceResponseDTO> response = null;
        try {

            WebClient client = WebClient
                    .builder()
                    .baseUrl(eWayBillBaseUrlProperty)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .build();

            response = Arrays.asList(client.post()
                    .uri(eWayBillByNumberUri)
                    .body(BodyInserters.fromObject(ewayBillByNumberInputDTO))
                    .retrieve()
                    .bodyToMono(EwayBillInvoiceResponseDTO[].class)
                    .block());
//            log.info(client.post()
//                    .uri(eWayBillByNumberUri)
//                    .body(BodyInserters.fromObject(ewayBillByNumberInputDTO))
//                    .retrieve()
//                    .bodyToMono(String.class)
//                    .block());

            response.forEach(ewayBillResponse -> {
                log.info("response: "+ewayBillResponse);
            });


        }catch(Exception e) {
            log.info("Connect exceptio: "+e);
        }

        log.info("response3 : "+response);
        return  response;
    }

    private List<EwayBillInvoiceResponseDTO> callAPIForEwayBillTransporterIdUpdate(EwayBillTransporterIDUpdateInputDTO ewayBillTransporterIDUpdateInputDTO) {


        List<EwayBillInvoiceResponseDTO> response = null;
        try {

            WebClient client = WebClient
                    .builder()
                    .baseUrl(eWayBillBaseUrlProperty)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .build();

            response = Arrays.asList(client.post()
                    .uri(eWayBillTransporterIdUpdateUri)
                    .body(BodyInserters.fromObject(ewayBillTransporterIDUpdateInputDTO))
                    .retrieve()
                    .bodyToMono(EwayBillInvoiceResponseDTO[].class)
                    .block());

            response.forEach(ewayBillResponse -> {
                log.info("response: "+ewayBillResponse);
            });


        }catch(Exception e) {
            log.info("Connect exceptio: "+e);
        }

        log.info("response3 : "+response);
        return  response;
    }

    private EwayBillInvoiceInputDTO getEwayBillInvoiceInputDTO(EwayBillTracking ewayBillTracking) {

        EwayBillInvoiceInputDTO ewayBillInvoiceInputDTO = new EwayBillInvoiceInputDTO();
        log.info("invoiceHeader : "+ewayBillTracking.getInvoiceHeader().getId());
        ewayBillInvoiceInputDTO.setVersion(eWayBillVersion);
        ewayBillInvoiceInputDTO.setUserCode(eWayBillUserCode);
        ewayBillInvoiceInputDTO.setPassword(eWayBillPassword);
        ewayBillInvoiceInputDTO.setClientCode(eWayBillClientCode);
        ewayBillInvoiceInputDTO.setBillLists(getIvoiceHeaderDetails(ewayBillTracking));

        return ewayBillInvoiceInputDTO;
    }

    private EwayBillCancelInputDTO getEwayBillCancelInputDTO(EwayBillTracking ewayBillTracking) {

        EwayBillCancelInputDTO ewayBillCancelInputDTO = new EwayBillCancelInputDTO();
        ewayBillCancelInputDTO.setUserCode(eWayBillUserCode);
        ewayBillCancelInputDTO.setPassword(eWayBillPassword);
        ewayBillCancelInputDTO.setClientCode(eWayBillClientCode);
        ewayBillCancelInputDTO.setCancelledewblist(getCancelledEwayBillDetails(ewayBillTracking));


        return ewayBillCancelInputDTO;
    }

    private EwayBillVehicleUpdateInputDTO getEwayBillVehicleUpdateInputDTO(EwayBillTracking ewayBillTracking) {

        EwayBillVehicleUpdateInputDTO  ewayBillVehicleUpdateInputDTO = new EwayBillVehicleUpdateInputDTO();
        ewayBillVehicleUpdateInputDTO.setUserCode(eWayBillUserCode);
        ewayBillVehicleUpdateInputDTO.setPassword(eWayBillPassword);
        ewayBillVehicleUpdateInputDTO.setClientCode(eWayBillClientCode);
        ewayBillVehicleUpdateInputDTO.setVehicleupdatelist(getEwayBillVehicleUpdateDetails(ewayBillTracking));

        return ewayBillVehicleUpdateInputDTO;
    }

    private EwayBillExtendValidityInputDTO getEwayBillExtendValidityInputDTO(EwayBillTracking ewayBillTracking) {

        EwayBillExtendValidityInputDTO  ewayBillVehicleUpdateInputDTO = new EwayBillExtendValidityInputDTO();
        ewayBillVehicleUpdateInputDTO.setUserCode(eWayBillUserCode);
        ewayBillVehicleUpdateInputDTO.setPassword(eWayBillPassword);
        ewayBillVehicleUpdateInputDTO.setClientCode(eWayBillClientCode);
        ewayBillVehicleUpdateInputDTO.setExtendewblist(getEwayBillExtendValidityDetails(ewayBillTracking));

        return ewayBillVehicleUpdateInputDTO;
    }
    private EwayBillByNumberInputDTO getEwayBillByNumberInputDTO(EwayBillTracking ewayBillTracking) {

        EwayBillByNumberInputDTO ewayBillByNumberInputDTO = new EwayBillByNumberInputDTO();
        ewayBillByNumberInputDTO.setUserCode(eWayBillUserCode);
        ewayBillByNumberInputDTO.setPassword(eWayBillPassword);
        ewayBillByNumberInputDTO.setClientCode(eWayBillClientCode);
         ewayBillByNumberInputDTO.setRequestorGSTIN(ewayBillTracking.getInvoiceHeader().getGstNumber());

        ewayBillByNumberInputDTO.setEwblist(getEwayBillListInputDetails(ewayBillTracking));
        return ewayBillByNumberInputDTO;
    }


    private EwayBillTransporterIDUpdateInputDTO getEwayBillTransporterIDUpdateInputDTO(EwayBillTracking ewayBillTracking) {

        EwayBillTransporterIDUpdateInputDTO  ewayBillTransporterIDUpdateInputDTO = new EwayBillTransporterIDUpdateInputDTO();
        ewayBillTransporterIDUpdateInputDTO.setUserCode(eWayBillUserCode);
        ewayBillTransporterIDUpdateInputDTO.setPassword(eWayBillPassword);
        ewayBillTransporterIDUpdateInputDTO.setClientCode(eWayBillClientCode);
        ewayBillTransporterIDUpdateInputDTO.setTransporteridlist(getEwayBillTransporterIDListUpdateInputDetails(ewayBillTracking));

        return ewayBillTransporterIDUpdateInputDTO;
    }
    private List<EwayBillInvoiceItemInputDTO> getItemList(List<InvoiceItem> invoiceItems) {

        List<EwayBillInvoiceItemInputDTO> outItemList = new ArrayList<>();

        invoiceItems.forEach(item -> {
            EwayBillInvoiceItemInputDTO ewayBillInvoiceItemInputDTO = getIvoiceItem(item);
            outItemList.add(ewayBillInvoiceItemInputDTO);
        });

        return outItemList;
    }

    private EwayBillInvoiceItemInputDTO getIvoiceItem(InvoiceItem invoiceItem) {
        EwayBillInvoiceItemInputDTO ewayBillInvoiceItemInputDTO = new EwayBillInvoiceItemInputDTO();

        ewayBillInvoiceItemInputDTO.setItemNo(invoiceItem.getSlNo());
        ewayBillInvoiceItemInputDTO.setProductName(invoiceItem.getMaterial().getName());
        ewayBillInvoiceItemInputDTO.setProductDesc(invoiceItem.getMaterial().getSpecification()==null ? invoiceItem.getMaterial().getName() : invoiceItem.getMaterial().getSpecification() );
        if(invoiceItem.getMaterial().getHsnCode()!=null) {
            ewayBillInvoiceItemInputDTO.setHsnCode(Integer.valueOf(invoiceItem.getMaterial().getHsnCode()));
        }
        ewayBillInvoiceItemInputDTO.setQuantity(invoiceItem.getQuantity());
        ewayBillInvoiceItemInputDTO.setQtyUnit(invoiceItem.getMaterial().getUnitOfMeasurement().getName()==null ? "" : invoiceItem.getMaterial().getUnitOfMeasurement().getName());
        ewayBillInvoiceItemInputDTO.setTaxableAmount(invoiceItem.getAmountAfterDiscount());
        ewayBillInvoiceItemInputDTO.setSgstRate(invoiceItem.getSgstTaxPercentage());
        ewayBillInvoiceItemInputDTO.setCessRate(invoiceItem.getCessPercentage() ==null ? 0.00 : invoiceItem.getCessPercentage());
        ewayBillInvoiceItemInputDTO.setCgstRate(invoiceItem.getCgstTaxPercentage());
        ewayBillInvoiceItemInputDTO.setIgstRate(invoiceItem.getIgstTaxPercentage());
        ewayBillInvoiceItemInputDTO.setCessNonAdvol(null);


        return ewayBillInvoiceItemInputDTO;
    }

    private List<EwayBillInvoiceBillInputDTO> getIvoiceHeaderDetails(EwayBillTracking ewayBillTracking) {

        List<EwayBillInvoiceBillInputDTO> ewayBillInvoiceBillInputDTOList = new ArrayList<>();
        EwayBillInvoiceBillInputDTO ewayBillInvoiceBillInputDTO = new EwayBillInvoiceBillInputDTO();
        InvoiceHeader invoiceHeader = ewayBillTracking.getInvoiceHeader();
        ewayBillInvoiceBillInputDTO.setUserGstin(invoiceHeader.getCompany().getGstNumber());
        ewayBillInvoiceBillInputDTO.setSupplyType("O");
       log.info(invoiceHeader.getPartyCountryId()+"country id ");
        if(invoiceHeader.getInvoiceType().getId()==15)
        {
            ewayBillInvoiceBillInputDTO.setSubSupplyType(4);
            ewayBillInvoiceBillInputDTO.setDocType("CHL");
        }
        else if(invoiceHeader.getInvoiceType().getId()==1 && invoiceHeader.getPartyCountryId()!=1) {
            //export inv
            ewayBillInvoiceBillInputDTO.setSubSupplyType(3);
            ewayBillInvoiceBillInputDTO.setDocType("INV");
        }
        else
        {
            ewayBillInvoiceBillInputDTO.setSubSupplyType(1);
            ewayBillInvoiceBillInputDTO.setDocType("INV");
        }

        ewayBillInvoiceBillInputDTO.setDocNo(invoiceHeader.getInvoiceNumber());
        ewayBillInvoiceBillInputDTO.setDocDate(dateFormat.format(invoiceHeader.getInvoiceDate()));
        ewayBillInvoiceBillInputDTO.setFromGstin(invoiceHeader.getCompany().getGstNumber() == null ? "" : invoiceHeader.getCompany().getGstNumber());
        ewayBillInvoiceBillInputDTO.setFromTrdName(invoiceHeader.getCompany().getName() == null ? "" : invoiceHeader.getCompany().getName() );
        ewayBillInvoiceBillInputDTO.setFromAddr1(invoiceHeader.getCompany().getAddress() ==  null ? "" : invoiceHeader.getCompany().getAddress());
        ewayBillInvoiceBillInputDTO.setFromAddr2("");
        ewayBillInvoiceBillInputDTO.setFromPlace(invoiceHeader.getCompany().getCity() ==  null ? "" : invoiceHeader.getCompany().getCity().getName());
        ewayBillInvoiceBillInputDTO.setFromPincode(invoiceHeader.getCompany().getPinCode() == null ? 0 : Integer.valueOf(invoiceHeader.getCompany().getPinCode()));
        ewayBillInvoiceBillInputDTO.setFromStateCode(invoiceHeader.getCompany().getState().getStateCode() == null ? 0 : Integer.valueOf(invoiceHeader.getCompany().getState().getStateCode()));
        ewayBillInvoiceBillInputDTO.setActualFromStateCode(invoiceHeader.getCompany().getState().getStateCode() == null ? 0 : Integer.valueOf(invoiceHeader.getCompany().getState().getStateCode()));
        if(invoiceHeader.getInvoiceType().getId()==1 && invoiceHeader.getPartyCountryId()!=1)
        {//export inv
          ewayBillInvoiceBillInputDTO.setToGstin("URP");
          ewayBillInvoiceBillInputDTO.setShipToGstin("URP");
        }
        else
        {   ewayBillInvoiceBillInputDTO.setShipToGstin(invoiceHeader.getGstNumber() == null ? "" : invoiceHeader.getGstNumber());
            ewayBillInvoiceBillInputDTO.setToGstin(invoiceHeader.getGstNumber() == null ? "" : invoiceHeader.getGstNumber());
        }
        ewayBillInvoiceBillInputDTO.setToTrdName(invoiceHeader.getParty().getName() == null ? "" : invoiceHeader.getParty().getName());
        ewayBillInvoiceBillInputDTO.setToAddr1(invoiceHeader.getShipToAddress()==null ? "" : invoiceHeader.getShipToAddress());
        if(ewayBillInvoiceBillInputDTO.getToAddr1().equalsIgnoreCase("")){
            ewayBillInvoiceBillInputDTO.setToAddr1(invoiceHeader.getParty().getAddress() == null ? "" : invoiceHeader.getParty().getAddress());
        }
        ewayBillInvoiceBillInputDTO.setToAddr2(invoiceHeader.getParty().getAddress() == null ? "" : invoiceHeader.getParty().getAddress());
        ewayBillInvoiceBillInputDTO.setToPlace(invoiceHeader.getParty().getCity()==null ? "" : invoiceHeader.getParty().getCity().getName());
        ewayBillInvoiceBillInputDTO.setToPincode(invoiceHeader.getParty().getPinCode() == null ? 0 : Integer.valueOf(invoiceHeader.getParty().getPinCode()));
        ewayBillInvoiceBillInputDTO.setToStateCode(invoiceHeader.getParty().getState().getStateCode() == null ? 0 : Integer.valueOf(invoiceHeader.getParty().getState().getStateCode()));
        if(invoiceHeader.getInvoiceType().getId()==1 && invoiceHeader.getPartyCountryId()!=1)
        {
            ewayBillInvoiceBillInputDTO.setActualToStateCode(ewayBillTracking.getPortStateId());
        }
        else
        { ewayBillInvoiceBillInputDTO.setActualToStateCode(invoiceHeader.getParty().getState().getStateCode() == null ? 0 : Integer.valueOf(invoiceHeader.getParty().getState().getStateCode()));

        }

        ewayBillInvoiceBillInputDTO.setTotalValue(invoiceHeader.getTotalTaxableAmount());
        ewayBillInvoiceBillInputDTO.setCgstValue(invoiceHeader.getCgstTaxAmount());
        ewayBillInvoiceBillInputDTO.setSgstValue(invoiceHeader.getSgstTaxAmount());
        ewayBillInvoiceBillInputDTO.setIgstValue(invoiceHeader.getIgstTaxAmount());
        ewayBillInvoiceBillInputDTO.setCessValue(invoiceHeader.getCessAmount() == null ? 0.00 : invoiceHeader.getCessAmount());
        ewayBillInvoiceBillInputDTO.setTotNonAdvolVal(null);
        ewayBillInvoiceBillInputDTO.setOthValue(null);
        ewayBillInvoiceBillInputDTO.setTotInvValue(invoiceHeader.getGrandTotal());
        ewayBillInvoiceBillInputDTO.setTransMode(ewayBillTracking.getTransactionMode());
        ewayBillInvoiceBillInputDTO.setTransDistance(Double.valueOf(ewayBillTracking.getTransDistance()));
        ewayBillInvoiceBillInputDTO.setTransporterName(ewayBillTracking.getTransporterName() == null ? "" : ewayBillTracking.getTransporterName());
        ewayBillInvoiceBillInputDTO.setTransporterId(ewayBillTracking.getTransporterId() == null ? "" : ewayBillTracking.getTransporterId());
        ewayBillInvoiceBillInputDTO.setTransDocNo(ewayBillTracking.getTransDocNo() == null ? "" : ewayBillTracking.getTransDocNo());
        ewayBillInvoiceBillInputDTO.setTransDocDate(ewayBillTracking.getTransDocDate() == null ? "" : dateFormat.format(ewayBillTracking.getTransDocDate()));
        ewayBillInvoiceBillInputDTO.setVehicleNo(ewayBillTracking.getVehicleNo() == null ? "" : ewayBillTracking.getVehicleNo());
        ewayBillInvoiceBillInputDTO.setVehicleType(ewayBillTracking.getVehicleType() == null ? "" : ewayBillTracking.getVehicleType());
        ewayBillInvoiceBillInputDTO.setMainHsnCode("");

        ewayBillInvoiceBillInputDTO.setShipToTradename(invoiceHeader.getShipToAddress()==null ? "" : invoiceHeader.getShipToAddress());
        ewayBillInvoiceBillInputDTO.setDispatchFromGstin(invoiceHeader.getCompany().getGstNumber() == null ? "" : invoiceHeader.getCompany().getGstNumber());
        ewayBillInvoiceBillInputDTO.setDispatchFromTradename(invoiceHeader.getCompany().getName() == null ? "" : invoiceHeader.getCompany().getName());
        ewayBillInvoiceBillInputDTO.setPortPin(ewayBillTracking.getPortPin()== null ? "" : ewayBillTracking.getPortPin());
        ewayBillInvoiceBillInputDTO.setPortName(ewayBillTracking.getPortName() == null ? "" : ewayBillTracking.getPortName());


        ewayBillInvoiceBillInputDTOList.add(ewayBillInvoiceBillInputDTO);
        List<EwayBillInvoiceItemInputDTO> itemList = getItemList(invoiceHeader.getInvoiceItems());
        ewayBillInvoiceBillInputDTO.setItemList(itemList);

        return ewayBillInvoiceBillInputDTOList;
    }


    private List<CancelledEwayBillInputDTO> getCancelledEwayBillDetails(EwayBillTracking ewayBillTracking) {

        List<CancelledEwayBillInputDTO> cancelledEwayBillInputDTOS = new ArrayList<>();
        CancelledEwayBillInputDTO cancelledEwayBillInputDTO = new CancelledEwayBillInputDTO();
        cancelledEwayBillInputDTO.setEwbNo(ewayBillTracking.geteWayBillNo());
        cancelledEwayBillInputDTO.setCancelledReason(ewayBillTracking.getCancelledReason());
        cancelledEwayBillInputDTO.setCancelledRemarks(ewayBillTracking.getReasonRemarks());
        cancelledEwayBillInputDTOS.add(cancelledEwayBillInputDTO);

        return cancelledEwayBillInputDTOS;
    }

    private List<EwayBillVehicleUpdateListInputDTO> getEwayBillVehicleUpdateDetails(EwayBillTracking ewayBillTracking) {
        log.info("ewayBillTracking.geteWayBillNo : "+Long.parseLong(ewayBillTracking.geteWayBillNo()));

        List<EwayBillVehicleUpdateListInputDTO>  ewayBillVehicleUpdateListInputDTOS = new ArrayList<>();
        EwayBillVehicleUpdateListInputDTO  ewayBillVehicleUpdateListInputDTO = new EwayBillVehicleUpdateListInputDTO();
        ewayBillVehicleUpdateListInputDTO.setEwbNo(Long.parseLong(ewayBillTracking.geteWayBillNo()));
        ewayBillVehicleUpdateListInputDTO.setFromPlace(ewayBillTracking.getInvoiceHeader().getCompany().getAddress());
        ewayBillVehicleUpdateListInputDTO.setFromState(ewayBillTracking.getInvoiceHeader().getCompany().getState().getStateCode() == null ? 0 : Integer.valueOf(ewayBillTracking.getInvoiceHeader().getCompany().getState().getStateCode()));
        ewayBillVehicleUpdateListInputDTO.setReasonCode(ewayBillTracking.getReasonCode()== null ? "" : ewayBillTracking.getReasonCode()+"");
        ewayBillVehicleUpdateListInputDTO.setReasonRem(ewayBillTracking.getReasonRemarks() == null ? "" : ewayBillTracking.getVehicleNo());
        ewayBillVehicleUpdateListInputDTO.setTransDocDate(ewayBillTracking.getTransDocDate()== null ? "" :   dateFormat.format(ewayBillTracking.getTransDocDate()));
        ewayBillVehicleUpdateListInputDTO.setTransDocNo(ewayBillTracking.getTransDocNo() == null ? "" : ewayBillTracking.getTransDocNo());
        ewayBillVehicleUpdateListInputDTO.setVehicleType(ewayBillTracking.getVehicleType() == null ? "" : ewayBillTracking.getVehicleType());
        ewayBillVehicleUpdateListInputDTO.setTransMode(ewayBillTracking.getTransactionMode()==0 ? "" : ewayBillTracking.getTransactionMode()+"");
        ewayBillVehicleUpdateListInputDTO.setVehicleNo(ewayBillTracking.getVehicleNo() == null ? "" : ewayBillTracking.getVehicleNo());
        ewayBillVehicleUpdateListInputDTOS.add(ewayBillVehicleUpdateListInputDTO);

        return ewayBillVehicleUpdateListInputDTOS;
    }


    private List<EwayBillExtendValidityListInputDTO> getEwayBillExtendValidityDetails(EwayBillTracking ewayBillTracking) {
        log.info("ewayBillTracking.geteWayBillNo : "+Long.parseLong(ewayBillTracking.geteWayBillNo()));

        List<EwayBillExtendValidityListInputDTO>  ewayBillVehicleUpdateListInputDTOS = new ArrayList<>();
        EwayBillExtendValidityListInputDTO   ewayBillExtendValidityListInputDTO = new EwayBillExtendValidityListInputDTO();
        ewayBillExtendValidityListInputDTO.setEwbNo(Long.parseLong(ewayBillTracking.geteWayBillNo()));
        ewayBillExtendValidityListInputDTO.setVehicleNo(ewayBillTracking.getVehicleNo());
        ewayBillExtendValidityListInputDTO.setFromPlace(ewayBillTracking.getCity().getName());
        ewayBillExtendValidityListInputDTO.setFromState(ewayBillTracking.getState().getStateCode() == null ? "" :  ewayBillTracking.getState().getStateCode());
        ewayBillExtendValidityListInputDTO.setRemainingDistance(ewayBillTracking.getRemainingDistance());
        ewayBillExtendValidityListInputDTO.setTransDocDate(ewayBillTracking.getTransDocDate()== null ? "" :   dateFormat.format(ewayBillTracking.getTransDocDate()));
        ewayBillExtendValidityListInputDTO.setTransDocNo(ewayBillTracking.getTransDocNo() ==null ? "" : ewayBillTracking.getTransDocNo());
        ewayBillExtendValidityListInputDTO.setTransMode(ewayBillTracking.getTransactionMode() == 0 ? "": ewayBillTracking.getTransactionMode()+"");
        ewayBillExtendValidityListInputDTO.setConsignmentStatus(ewayBillTracking.getTransactionMode()!=5 ? "M" : "T");
        ewayBillExtendValidityListInputDTO.setTransitType(ewayBillTracking.getTransactionMode()!=5 ? "" : "R");
        ewayBillExtendValidityListInputDTO.setExtnRsnCode(ewayBillTracking.getExtensionReasonCode());
        ewayBillExtendValidityListInputDTO.setExtnRemarks(ewayBillTracking.getExtensionReasonRemarks());
        ewayBillExtendValidityListInputDTO.setFromPincode(Integer.valueOf(ewayBillTracking.getFromPincode()));
        ewayBillExtendValidityListInputDTO.setAddressLine1("");
        ewayBillExtendValidityListInputDTO.setAddressLine2("");
        ewayBillExtendValidityListInputDTO.setAddressLine3("");

        ewayBillVehicleUpdateListInputDTOS.add(ewayBillExtendValidityListInputDTO);

        return ewayBillVehicleUpdateListInputDTOS;
    }


    private List<EwayBillListInputDTO> getEwayBillListInputDetails(EwayBillTracking ewayBillTracking) {

        List<EwayBillListInputDTO> ewayBillListInputDTOS = new ArrayList<>();
        EwayBillListInputDTO ewayBillListInputDTO = new EwayBillListInputDTO();
        ewayBillListInputDTO.setEwbNo(ewayBillTracking.geteWayBillNo());

        ewayBillListInputDTOS.add(ewayBillListInputDTO);

        return ewayBillListInputDTOS;
    }

    private List<EwayBillTransporterIDListUpdateInputDTO> getEwayBillTransporterIDListUpdateInputDetails(EwayBillTracking ewayBillTracking) {

        List<EwayBillTransporterIDListUpdateInputDTO> ewayBillTransporterIDListUpdateInputDTOS = new ArrayList<>();
        EwayBillTransporterIDListUpdateInputDTO ewayBillTransporterIDListUpdateInputDTO = new EwayBillTransporterIDListUpdateInputDTO();
        ewayBillTransporterIDListUpdateInputDTO.setEwbNo(Long.parseLong(ewayBillTracking.geteWayBillNo()));
        ewayBillTransporterIDListUpdateInputDTO.setTransporterId(ewayBillTracking.getTransporterId());

        ewayBillTransporterIDListUpdateInputDTOS.add(ewayBillTransporterIDListUpdateInputDTO);

        return ewayBillTransporterIDListUpdateInputDTOS;
    }
}
