package com.coreerp.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.coreerp.dao.QuotationHeaderRepository;
import com.coreerp.model.QuotationHeader;
import com.coreerp.service.QuotationHeaderService;

@Service
public class QuotationHeaderServiceImpl implements QuotationHeaderService {
	
	
	@Autowired
	QuotationHeaderRepository quotationHeaderRepository;
	

	@Override
	public Page<QuotationHeader> getAllQuotationHeader(Pageable pageable) {
		 return quotationHeaderRepository.findAll(pageable);
	}


	@Override
	public void save(QuotationHeader quotationHeader) {
		quotationHeaderRepository.save(quotationHeader);
	}


	@Override
	public QuotationHeader findByCreatedBy(String createdBy) {
		return quotationHeaderRepository.findByCreatedBy(createdBy);
	}


	@Override
	public Iterable<QuotationHeader> findAll() {
		return quotationHeaderRepository.findAll();
	}


	@Override
	public void delete(String id) {
		quotationHeaderRepository.deleteById(id);
	}


	@Override
	public QuotationHeader getById(String id) {
		return quotationHeaderRepository.getOne(id);
	}

}
