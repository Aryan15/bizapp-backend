package com.coreerp.serviceimpl;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;
import com.coreerp.service.StatusService;
import com.coreerp.service.TransactionStatusService;
import com.coreerp.service.TransactionTypeService;

@Component
public class DeliveryChallanStatusService implements TransactionStatusService<DeliveryChallanHeader>{
	final static Logger log = LogManager.getLogger(DeliveryChallanStatusService.class);
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Override
	public Status getStatus(DeliveryChallanHeader dc) {
		/*
		 * 
		 * ** derive status as New if incoming id is null and/or status is null/ 'New'
		 * ** derive status as Open if incoming id is not null and status != 'Cancelled' 
		 * 
		 * ** derive status as Cancelled if incoming id is not null/ 'New' and status = 'Cancelled'
		 * 
		 * ** validate if the deleting invoice is the latest in the series
		 * ** derive status as Deleted if incoming id is not null and status = 'Deleted'
		 */
			Status status = new Status();
			
			TransactionType transactionType = transactionTypeService.getModelById(dc.getDeliveryChallanType().getId());
			

			
			String dcStatus = dc.getStatus() != null ? dc.getStatus().getName() : null;
			
			if((dc.getId() == null || dc.getId().isEmpty()) || dcStatus.equals(ApplicationConstants.DC_STATUS_NEW)){
				log.info("1");
				status = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_NEW);
			}
//			else if((dc.getId() != null && !dc.getId().isEmpty())&& !dcStatus.equals(ApplicationConstants.DC_STATUS_CANCELLED)){
//				log.info("2");
//				status = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_OPEN);
//			}else if((dc.getId() != null && !dc.getId().isEmpty()) && dcStatus.equals(ApplicationConstants.DC_STATUS_CANCELLED)){
//				log.info("3");
//				status = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_CANCELLED);
//			}
	else if((dc.getId() != null && !dc.getId().isEmpty()) && dcStatus.equals(ApplicationConstants.DC_STATUS_DELETED)){
				log.info("4");
				status = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_DELETED);
			}else {
				log.info("5");
				status = statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_NEW);
			}
			log.info("out status: "+status);
			return status;
	}

}
