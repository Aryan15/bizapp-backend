package com.coreerp.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.dao.UnitOfMeasurementRepository;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.UnitOfMeasurement;
import com.coreerp.service.UnitOfMeasurementService;

@Service
public class UnitOfMeasurementServiceImpl implements UnitOfMeasurementService {

	@Autowired
	UnitOfMeasurementRepository unitOfMeasurementRepository;
	
	@Override
	public UnitOfMeasurement getById(Long id) {
		return unitOfMeasurementRepository.getOne(id);
	}

	@Override
	public UnitOfMeasurement save(UnitOfMeasurement unitOfMeasurement) {
		return unitOfMeasurementRepository.save(unitOfMeasurement);
	}

	@Override
	public List<UnitOfMeasurement> getAll() {
		return unitOfMeasurementRepository.findAll();
	}

	@Override
	public TransactionResponseDTO delete(Long id) {
		
		TransactionResponseDTO response = new TransactionResponseDTO();
		
		response.setResponseString("Unit of measurement deleted successfully");
		response.setResponseStatus(1);
		
		try{
			unitOfMeasurementRepository.deleteById(id);
		}catch(Exception e){
			response.setResponseString("Failed to delete unit of measurement");
			response.setResponseStatus(0);
		}
		
		
		return response;
	}

}
