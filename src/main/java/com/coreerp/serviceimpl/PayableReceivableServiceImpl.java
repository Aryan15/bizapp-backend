package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.InvoiceItemRepository;
import com.coreerp.dao.PartyRepository;
import com.coreerp.dao.PayableReceivableHeaderRepository;
import com.coreerp.dao.PayableReceivableItemRepository;
import com.coreerp.dto.PayableReceivableHeaderDTO;
import com.coreerp.dto.PayableReceivableWrapperDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.PayableReceivableHeaderMapper;
import com.coreerp.mapper.PayableReceivableItemMapper;
import com.coreerp.model.Party;
import com.coreerp.model.PayableReceivableHeader;
import com.coreerp.model.PayableReceivableItem;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;
import com.coreerp.service.JournalEntryService;
import com.coreerp.service.PayableReceivableService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TransactionTypeService;

@Service
public class PayableReceivableServiceImpl implements PayableReceivableService {

	final static Logger log = LogManager.getLogger(PayableReceivableServiceImpl.class);
	
	@Autowired
	PayableReceivableStatusService payableReceivableStatusService;
	
	@Autowired
	PayableReceivableHeaderRepository payableReceivableHeaderRepository; 
	
	@Autowired
	PayableReceivableHeaderMapper payableReceivableHeaderMapper;
	
	
	@Autowired
	PayableReceivableItemMapper payableReceivableItemMapper;
	
	@Autowired
	PayableReceivableItemRepository payableReceivableItemRepository; 
	
	@Autowired	
	PayableReceivableBackTrackingService payableReceivableBackTrackingService;
	
	@Autowired
	InvoiceItemRepository invoiceItemRepository;
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	JournalEntryService JournalEntryService;
	
	@Autowired
	PartyRepository partyRepository;
	
	@Override
	public PayableReceivableHeaderDTO save(PayableReceivableWrapperDTO payableReceivableWrapperDTO) {


		PayableReceivableHeaderDTO payableReceivableHeaderDTO = payableReceivableWrapperDTO.getPayableReceivableHeader();
		
		if(payableReceivableWrapperDTO.getItemToBeRemove().size() > 0){
			payableReceivableWrapperDTO.getItemToBeRemove().forEach(id -> {
				deleteItem(id);
				
			});
		}
		
		log.info("incoming: "+payableReceivableHeaderDTO);
		
		PayableReceivableHeader prHeaderSave = payableReceivableHeaderMapper.dtoToModelMap(payableReceivableHeaderDTO);
		log.info("status" + payableReceivableHeaderDTO.getStatusName());
		// payableReceivableHeaderDTO.getStatusName()?
		// prHeaderSave.setStatus(getStatus(prHeaderSave)) :
		// prHeaderSave.setStatus(statusService.getStatusByName(payableReceivableHeaderDTO.getStatusName().equals(ApplicationConstants.PR_STATUS_DRAFT)));
		if (!payableReceivableHeaderDTO.getStatusName().equals(ApplicationConstants.PR_STATUS_DRAFT)) {
			log.info("1");
			// prHeaderSave.setStatus(getStatus(prHeaderSave));
			prHeaderSave.setStatus(statusService.getStatusByTransactionTypeAndName(prHeaderSave.getTransactionType(),
					ApplicationConstants.PR_STATUS_COMPLETED));

		} else {
			TransactionType transaction = transactionTypeService
					.getModelById(payableReceivableHeaderDTO.getTransactionTypeId());
			// TransactionType transaction =
			// transactionTypeService.get(payableReceivableHeaderDTO.getTransactionTypeId());
			prHeaderSave.setStatus(statusService.getStatusByTransactionTypeAndName(transaction,
					payableReceivableHeaderDTO.getStatusName()));

		}

		log.info("2" + prHeaderSave.getStatus());
		List<PayableReceivableItem> prItemsSave = payableReceivableItemMapper
				.dtoToModelList(payableReceivableHeaderDTO.getPayableReceivableItems());

		prItemsSave.forEach(item -> item.setPayableReceivableHeader(prHeaderSave));

		prItemsSave.forEach(item -> {
			if (!payableReceivableHeaderDTO.getStatusName().equals(ApplicationConstants.PR_STATUS_DRAFT)) {
				item.setPaidAmount(item.getPayingAmount());
			}
			// item.setDueAmount(item.getDueAmount() - item.getPayingAmount());

		});

		prHeaderSave.setPayableReceivableItems(prItemsSave);

		// Save record
		PayableReceivableHeader payableReceivableHeader = payableReceivableHeaderRepository.save(prHeaderSave);

		log.info("Saved record: " + payableReceivableHeader);
		if (!payableReceivableHeaderDTO.getStatusName().equals(ApplicationConstants.PR_STATUS_DRAFT)) {
			// Update corresponding invoice due amount
			payableReceivableBackTrackingService.updateTransactionStatus(payableReceivableHeader, null);

			// Post a Journal Entry
			JournalEntryService.savePayableReceivableJournalEntry(payableReceivableHeader);
		}
		return payableReceivableHeaderMapper.modelToDTOMap(payableReceivableHeader);

	}
	
//	private Status getStatus(PayableReceivableHeader prHeaderSave){
//		
//		return payableReceivableStatusService.getStatus(prHeaderSave);
//	}

	@Override
	public PayableReceivableHeaderDTO getById(String id) {
		return payableReceivableHeaderMapper.modelToDTOMap(payableReceivableHeaderRepository.getOne(id));
	}

	@Override
	public PayableReceivableHeader getModelById(String id) {
		return payableReceivableHeaderRepository.getOne(id);
	}

	@Override
	public Long getMaxPRNumber(String prefix) {
		return payableReceivableHeaderRepository.getMaxPRNumber(prefix);
	}

	@Override
	public Long getMaxPRNumberByTransactionType(Long id) {
		// TODO Auto-generated method stub
		return payableReceivableHeaderRepository.getMaxPRNumberByTransactionType(id);
	}
	


	@Override
	public TransactionResponseDTO delete(String id) {
		
		TransactionResponseDTO res = new TransactionResponseDTO();
		String deleteStatus = "Payable Receivable Deleted Successfully";		
		Integer deleteStatusNum = 1;
		
		PayableReceivableHeader prToDelete = payableReceivableHeaderRepository.getOne(id);
		log.info("prToDelete.getStatus().getName(): "+prToDelete.getStatus().getName());
		Long greaterCount = 0l;

		if(!prToDelete.getStatus().getName().equals(ApplicationConstants.PR_STATUS_DRAFT)){
			greaterCount = payableReceivableHeaderRepository.countByPayReferenceIdGreaterThanAndTransactionType(prToDelete.getPayReferenceId(), prToDelete.getTransactionType());
		}

		
		log.info("greaterCount"+greaterCount);
		
		if(greaterCount > 0){
			
			deleteStatus = "Cannot delete older Payable Receivable";
			deleteStatusNum = 0;
			log.info("3");
			
			}
		else{
	
				try{
							if(greaterCount == 0){
		
				
											prToDelete.setStatus(statusService.getStatusByTransactionTypeAndName(prToDelete.getTransactionType(), ApplicationConstants.PR_STATUS_DELETED));
//											payableReceivableBackTrackingService.updateTransactionStatus(prToDelete, null);
											prToDelete.getPayableReceivableItems().forEach(item -> {
											payableReceivableItemRepository.delete(item);
					
										});
				
											payableReceivableHeaderRepository.delete(prToDelete);
						}
						
						else{
								deleteStatus = "Cannot Delete PR";
								deleteStatusNum = 0;
							}

		}
		
		catch(Exception e){
			deleteStatus = "Cannot Delete PR";
			deleteStatusNum = 0;
		}
		}
		res.setResponseString(deleteStatus);
		res.setResponseStatus(deleteStatusNum);
		
		return res;
	}
	@Override
	public TransactionResponseDTO deleteItem(String id){
		TransactionResponseDTO res = new TransactionResponseDTO();
		res.setResponseStatus(1);
		String deleteStatus = "PR Item Deleted Successfully";
		try {
		PayableReceivableItem prDelete = payableReceivableItemRepository.getOne(id);
		
		payableReceivableBackTrackingService.itemDelete(prDelete);
		
		payableReceivableItemRepository.delete(prDelete);
		
		}catch(Exception e){
			deleteStatus = "Cannot Delete PR Item";
			res.setResponseStatus(0);
		}
		
		res.setResponseString(deleteStatus);
		return res;
	}
	
	public Long getCountByPartyAndStatusIn(Long partyId, Long transactionTypeId) {
		
		Party party = partyRepository.getOne(partyId);
		
		List<Status> statuses = new ArrayList<Status>();
		
		TransactionType transaction = transactionTypeService
				.getModelById(transactionTypeId);
		
		Status status = statusService.getStatusByTransactionTypeAndName(transaction,
				ApplicationConstants.PR_STATUS_DRAFT);
		
		statuses.add(status);
		
		return payableReceivableHeaderRepository.countByPartyAndStatusIn(party, statuses);
	}

}
