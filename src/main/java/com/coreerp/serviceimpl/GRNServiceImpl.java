package com.coreerp.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.model.*;
import com.coreerp.service.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.DeliveryChallanItemRepository;
import com.coreerp.dao.GRNHeaderRepository;
import com.coreerp.dao.GRNItemRepository;
import com.coreerp.dao.MaterialRepository;
import com.coreerp.dao.TransactionStatusChangeRepository;
import com.coreerp.dto.GRNHeaderDTO;
import com.coreerp.dto.GRNWrapperDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.GRNHeaderMapper;
import com.coreerp.mapper.GRNItemMapper;

@Service
public class GRNServiceImpl implements GRNService  {

	final static Logger log = LogManager.getLogger(GRNServiceImpl.class);

	@Autowired
	GRNHeaderRepository grnHeaderRepository;
	
	@Autowired
	GRNItemRepository grnItemRepository;
	
	@Autowired
	GRNHeaderMapper grnHeaderMapper;
	
	@Autowired
	GRNItemMapper grnItemMapper;
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	MaterialRepository materialRepository;

	@Autowired
	FinancialYearService financialYearService;
	//@Autowired
	//DeliveryChallanHeaderRepository deliveryChallanRepository;
	
	@Autowired
	DeliveryChallanItemRepository deliveryChallanItemRepository;
	
	@Autowired
	GRNInventoryService grnInventoryService;
	
	@Autowired
	GRNBackTrackingServiceNew grnBackTrackingService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	TransactionStatusChangeRepository transactionStatusChangeRepository;
	
	@Override
	public Page<GRNHeader> getAllGRNHeader(Pageable pageable) {
		return grnHeaderRepository.findAll(pageable);
	}

	@Override
	public void saveGRNHeader(GRNHeader grnHeader) {
		 grnHeaderRepository.save(grnHeader);
		
	}
	
	@Autowired
	PartyService partyService;

	@Override
	public Iterable<GRNHeader> findAllGRNHeaders() {
		return null;
	}

	@Override
	public void deleteGRNHeader(String grnHeaderId) {
		grnHeaderRepository.deleteById(grnHeaderId);
		
	}

	@Override
	public Page<GRNItem> getAllGRNItems(Pageable pageable) {
		return grnItemRepository.findAll(pageable);
	}

	@Override
	public void saveGRNItem(GRNItem grnItem) {
		grnItemRepository.save(grnItem);
	}

	@Override
	public Iterable<GRNItem> findAllGRNItems() {
		return null;
	}

	@Override
	public void deleteGRNItem(String grnItemId) {
		grnItemRepository.deleteById(grnItemId);
	}

	@Override
	public List<GRNItem> getAllGRNItemsByHeaderId(String grnHeaderId, Pageable pageable) {
		return grnItemRepository.findByHeaderId(grnHeaderId);
	}

	@Override
	public GRNHeader getGRNHeaderByHeaderId(String grnHeaderId) {
		return grnHeaderRepository.findByHeaderId(grnHeaderId);
	}

	@Override
	public List<GRNHeaderDTO> findAllGRNs() {
		List<GRNHeader> grnHeaders=grnHeaderRepository.findAll();
	    List<GRNHeaderDTO> grnDTOList=new ArrayList<GRNHeaderDTO>();
	    for(GRNHeader grnHeader : grnHeaders){
	    	grnDTOList.add(grnHeaderMapper.modelToDTOMap(grnHeader));
	    }
	    
	    log.info("grnDTOList.grnItems : "+(grnDTOList.size()>0 ? grnDTOList.get(0).getGrnItems().size():0));
	    return grnDTOList;
	}

	@Override
	public GRNHeaderDTO getGRNsByHeaderId(String grnHeaderId) {
		GRNHeader grnHeader = grnHeaderRepository.getOne(grnHeaderId);
		log.info("in getGRNsByHeaderId GRN header id: "+grnHeader.getId());
		log.info("in getGRNsByHeaderId grn item ----:"+grnHeader.getGrnItems());
		/*log.info("in getGRNsByRegistergst grn item : "+grnHeader.);*/
		if(grnHeader.getGrnItems().size() == 0){
			List<GRNItem> grnItems = grnItemRepository.findByHeaderId(grnHeader.getId());
			log.info("2 in getGRNsByHeaderId grn item : "+grnItems);
			grnHeader.setGrnItems(grnItems);
			log.info("3 in getGRNsByHeaderId grn item : "+grnHeader.getGrnItems());
		}
		return grnHeaderMapper.modelToDTOMap(grnHeader);
	}

	@Override
	public GRNHeaderDTO saveGRN(GRNWrapperDTO grnWrapperDTOIn) {
		
		GRNHeaderDTO grnDTOIn = grnWrapperDTOIn.getGrnHeader();
		
        GRNHeader grnHeader = grnHeaderMapper.dtoToModelMap(grnDTOIn);
      
		
		
	//Get older GRN if exists
		
        GRNHeader grnHeaderPre = new GRNHeader();
		List<GRNItem> GRNItemsPre = new ArrayList<GRNItem>();
		
		if(grnDTOIn.getId() != null){
			grnHeaderRepository.getOne(grnDTOIn.getId()).getGrnItems().forEach(item -> { 
				GRNItem GRNItemPre = new GRNItem();
				GRNItemPre.setId(item.getId());
				GRNItemPre.setAcceptedQuantity(item.getAcceptedQuantity());
				GRNItemPre.setMaterial(item.getMaterial());
				GRNItemPre.setDeliveryChallanItem(item.getDeliveryChallanItem());
				GRNItemsPre.add(GRNItemPre);
			});
		}
		

		
		grnHeaderPre.setGrnItems(GRNItemsPre);
		
		log.info("grnItems size 1: "+grnHeader.getGrnItems().size());
		if(grnWrapperDTOIn.getItemToBeRemove().size() > 0){
			grnWrapperDTOIn.getItemToBeRemove().forEach(id -> {
				log.info("deleting item..."+id);
				deleteItem(id);
			});
		}
		log.info("grnItems size 1: "+grnHeader.getGrnItems().size());
//		grnHeaderPre.getGrnItems().forEach(item -> {
//			log.info("prev:--- "+item.getMaterial().getName()+": "+item.getAcceptedQuantity());
//		});
		
		//InvoiceHeader invoiceHeaderPre = invoiceDTOIn.getId() != null ? invoiceHeaderRepository.getOne(invoiceDTOIn.getId()) : null;
		
		
		//Derive status has to be checked
		grnHeader.setGrnStatus(getGrnStatus(grnHeader));

		List<GRNItem> grnItems = grnItemMapper.dtoToModelList(grnDTOIn.getGrnItems());//invoiceDTOItemToInvoiceItemMap(invoiceDTOIn.getInvoiceItems(), invoiceHeader);
		
		
		log.info("grnItems size: "+grnItems.size());
		
		//set header to items		
		grnItems.stream().forEach(grnItem -> grnItem.setGrnHeader(grnHeader));

		//set items to header 
		grnHeader.setGrnItems(grnItems);
		
		log.info("Before save: "+ grnHeader.getCompany().getId());
		
		
//		grnHeader.getGrnItems().forEach(item -> {
//			log.info("item before save:--- "+item.getId());
//		});
//		log.info("balance quantity before save");
//		grnHeader.getGrnItems()
//			.forEach(grnitem -> { 
//				grnitem.getDeliveryChallanHeader().getDeliveryChallanItems().forEach(dci ->{
//					log.info("dci.getGrnBalanceQuantity(): "+dci.getGrnBalanceQuantity());
//				});});
//		
//		log.info("balance quantity before save");
//		grnHeader.getGrnItems()
//			.forEach(grnitem -> {
//				
//				log.info("grnitem.getDeliveryChallanItem().getBalanceQuantity(): "+grnitem.getDeliveryChallanItem().getGrnBalanceQuantity());
//				});		
//		
		//save
		GRNHeader grnHeaderOut = grnHeaderRepository.save(grnHeader);
		
//		grnHeaderOut.getGrnItems().forEach(item -> {
//			log.info("item after save:--- "+item.getId());
//		});
		
		
//		log.info("balance quantity aftter save");
//		grnHeaderOut.getGrnItems()
//			.forEach(grnitem -> { 
//				grnitem.getDeliveryChallanHeader().getDeliveryChallanItems().forEach(dci ->{
//					log.info("dci.getGrnBalanceQuantity(): "+dci.getGrnBalanceQuantity());
//				});});
//		
//		log.info("After save: "+ grnHeaderOut.getCompany().getId());
//		//back track DC
//		
		statusChangeEntry(grnHeaderOut);
		
//		grnHeaderOut.getGrnItems().forEach(item -> {
//			log.info("item after save 2:--- "+item.getId());
//		});
		
		grnBackTrackingService.updateTransactionStatus(grnHeaderOut, grnHeaderPre);
		
		
//		grnHeaderOut.getGrnItems().forEach(item -> {
//			log.info("item after save 3:--- "+item.getId());
//		});
//		
//		grnHeaderPre.getGrnItems().forEach(item -> {
//			log.info("2 prev:--- "+item.getMaterial().getName()+": "+item.getAcceptedQuantity());
//		});
//		
//		grnHeaderOut.getGrnItems().forEach(item -> {
//			log.info("item after save 4:--- "+item.getId());
//		});
		
		
		log.info("balance quantity aftter back track");
//		grnHeaderOut.getGrnItems()
//			.forEach(grnitem -> { 
//				grnitem.getDeliveryChallanHeader().getDeliveryChallanItems().forEach(dci ->{
//					log.info("dci.getBalanceQuantity(): "+dci.getGrnBalanceQuantity());
//				});});
		
//		grnHeaderOut.getGrnItems().forEach(item -> {
//			log.info("item after save 5:--- "+item.getId());
//		});
		
		
//		log.info("balance quantity aftter inventory");
//		grnHeaderOut.getGrnItems()
//			.forEach(grnitem -> { 
//				grnitem.getDeliveryChallanHeader().getDeliveryChallanItems().forEach(dci ->{
//					log.info("dci.getBalanceQuantity(): "+dci.getGrnBalanceQuantity());
//				});});
//		
//		grnHeaderOut.getGrnItems().forEach(item -> {
//			log.info("item after save 6:--- "+item.getId());
//		});

		
		//Manage inventory
		String updateInventoryStatus =  grnInventoryService.updateInventory(grnHeaderOut, grnHeaderPre); //TO DO pass prev GRN
		
//		log.info("updateInventoryStatus: "+updateInventoryStatus);
//		log.info("(grnHeaderOut.getGrnStatus().getName(): "+grnHeaderOut.getGrnStatus().getName());
//		
//		grnHeaderOut.getGrnItems().forEach(item -> {
//			log.info("item after save 7:--- "+item.getId());
//		});

		
//		grnHeaderOut.getGrnItems().forEach(item -> {
//			log.info("item after save 8:--- "+item.getId());
//		});
//		
//		log.info("balance quantity aftter stock trace");
//		grnHeaderOut.getGrnItems()
//			.forEach(grnitem -> { 
//				grnitem.getDeliveryChallanHeader().getDeliveryChallanItems().forEach(dci ->{
//					log.info("dci.getGrnBalanceQuantity(): "+dci.getGrnBalanceQuantity());
//				});});
		
		//Post a journal entry
		//JournalEntryService.saveInvoiceJournalEntry(invoiceHeaderOut);
		
		//Update balance quantity of DC
		
		//updateDCBalanceQuantity(grnHeaderOut, grnHeaderPre);

//		grnHeaderOut.getGrnItems().forEach(item -> {
//			log.info("item after save 9:--- "+item.getId());
//		});
		
		return grnHeaderMapper.modelToDTOMap(grnHeaderOut);
	}

//	private void updateDCBalanceQuantity(GRNHeader grnHeaderOut, GRNHeader grnHeaderPre) {
//
//		//Get referenced DC
//		log.info("Updating balance quantity of delivery challan");
//		//List<DeliveryChallanHeader> dcList = grnHeaderOut.getGrnItems().stream().map(item -> item.getDeliveryChallanHeader()).collect(Collectors.toList());
//		
//		
//		//Update dc item balance quantity = dc item balance quantity - grn item accepted quantity
//		
////		dcList.forEach(dcHeader -> {
////			dcHeader.getDeliveryChallanItems().forEach(dcItem -> {
////				Double grnItemAcceptedQty = grnHeaderOut.getGrnItems().stream().filter(item -> item.getDeliveryChallanItem().getId() == dcItem.getId()).mapToDouble(item -> item.getAcceptedQuantity()).sum();
////				dcItem.setBalanceQuantity(dcItem.getBalanceQuantity() - grnItemAcceptedQty);
////				log.info("dc item bal quantity: "+dcItem.getBalanceQuantity());
////				deliveryChallanItemRepository.save(dcItem);
////			});
////		});
//		
//		grnHeaderOut.getGrnItems()
//		.forEach(grnitem -> {
//			
//			List<GRNItem> preItemList = grnHeaderPre.getGrnItems().stream().filter(item -> item.getId().equals(grnitem.getId())).collect(Collectors.toList());
//			
//			log.info("preItemList: "+preItemList.size());
//			GRNItem preItem = null;
//			
//			if(preItemList.size() > 0){
//				preItem = preItemList.get(0);
//			}
//			log.info("preItem: "+preItem);
//			if(Double.compare(grnitem.getAcceptedQuantity(), (preItem != null ? preItem.getAcceptedQuantity() : -1.0)) != 0){
//				log.info("grnitem.getDeliveryChallanItem().getBalanceQuantity(): "+grnitem.getDeliveryChallanItem().getGrnBalanceQuantity());
//				log.info("grnitem.getAcceptedQuantity(): "+grnitem.getAcceptedQuantity());
//				//log.info("preItem.getAcceptedQuantity(): "+preItem.getAcceptedQuantity());
//				
//				DeliveryChallanItem dcItem = grnitem.getDeliveryChallanItem();
//				
//				dcItem.setGrnBalanceQuantity(dcItem.getGrnBalanceQuantity() - grnitem.getAcceptedQuantity());
//				
//				log.info("dc item bal quantity: "+dcItem.getGrnBalanceQuantity());
//				
//				deliveryChallanItemRepository.save(dcItem);
//			}
//			 
//			});	
//				
//		
//	}

	private Status getGrnStatus(GRNHeader grnHeader){
		return statusService.getStatusByTransactionTypeAndName(transactionTypeService.getModelById(grnHeader.getGrnType().getId()), ApplicationConstants.GRN_STATUS_NEW);
	}
	@Override
	public GRNHeader save(GRNHeader grnHeaderIn) {
		return grnHeaderRepository.save(grnHeaderIn);
	}

	@Override
	public List<GRNHeaderDTO> findGRNByGRNNumber(String grnNumber) {
		return grnHeaderMapper.modelToDTOList(grnHeaderRepository.findByGrnNumber(grnNumber));
	}

	@Override
	public Long getMaxGRNNumber(String prefix) {
		return  grnHeaderRepository.getMaxGrnNumber(prefix);
	}

	@Override
	public GRNItem getItemById(String grnItemId) {
		return grnItemRepository.getOne(grnItemId);
	}

	@Override
	public List<GRNHeaderDTO> findAllGrnForAParty(Long partyId) {
		return grnHeaderMapper.modelToDTOList(grnHeaderRepository.findBySupplierId(partyId));
	}
	
	private boolean saveAllowed(GRNHeader grn){
		
		//Not allow to delete if incoming status is deleted/Cancelled but there is a reference in invoice
		boolean returnValue = false;
		
		if(grn.getGrnStatus() != null){
			
			
			String grnStatus = grn.getGrnStatus().getName();
			
			if((grnStatus.equals(ApplicationConstants.GRN_STATUS_DELETED))){
				
				
				List<GRNItem> grnItems = grnItemRepository.findByGrnHeader(grn);
				
				if(grnItems.isEmpty()){
					
					returnValue = true;					
					
				}
			}else{
				returnValue = true;
			}

		}else{
			returnValue = true;
		}
		
		return returnValue;
		
	}


	
	@Override
	public TransactionResponseDTO delete(String id) {
		TransactionResponseDTO res = new TransactionResponseDTO();
		
		String deleteStatus = "GRN Deleted Successfully";
		Integer deleteStatusNum = 1;
		log.info("1");
		GRNHeader grnDebug = grnHeaderRepository.getOne(id);
		log.info(grnDebug.getGrnStatus().getName());
		Status originalStatus = grnDebug.getGrnStatus(); 
		try {
			GRNHeader grnDelete = grnHeaderRepository.getOne(id);
			
			log.info("2");
			grnDebug = grnHeaderRepository.getOne(id);
			log.info(grnDebug.getGrnStatus().getName());

			Long grnCount = grnHeaderRepository.countByGrnIdGreaterThan(grnDelete.getGrnId());
			
			log.info("3: "+grnCount);
			grnDebug = grnHeaderRepository.getOne(id);
			log.info(grnDebug.getGrnStatus().getName());
			
			if(grnCount > 0){
				
				deleteStatus = "Cannot delete older Grn";
				deleteStatusNum = 0;
				grnDelete.setGrnStatus(originalStatus);
				log.info("4");
				grnDebug = grnHeaderRepository.getOne(id);
				log.info(grnDebug.getGrnStatus().getName());
				
			}else{
			
				log.info("5");
				grnDebug = grnHeaderRepository.getOne(id);
				log.info(grnDebug.getGrnStatus().getName());
				if(saveAllowed(grnDelete)){
					
					log.info("6");
					grnDebug = grnHeaderRepository.getOne(id);
					log.info(grnDebug.getGrnStatus().getName());
					
					grnDelete.setGrnStatus(statusService.getStatusByTransactionTypeAndName(grnDelete.getGrnType(), ApplicationConstants.GRN_STATUS_DELETED));
		
					deleteStatusChangeEntry(id);
					grnBackTrackingService.updateTransactionStatus(grnDelete, null);
					
					log.info("7");
					//Manage inventory
					String updateInventoryStatus = grnInventoryService.updateInventory(grnDelete, null);
					
					log.info("updateInventoryStatus: "+updateInventoryStatus);
					//Stock Trace fo rgrn
					
//					log.info("8");
//					grnDelete.getGrnItems().forEach(item -> {
//						grnItemRepository.delete(item);
//					});
					
					grnHeaderRepository.delete(grnDelete);
					
					log.info("9");
					//dchDebug = deliveryChallanHeaderRepository.getOne(id);
					//log.info(dchDebug.getStatus().getName());
					deleteStatusChangeEntry(grnDelete.getId());
					
				}else{
					deleteStatus = "Cannot Delete GRN";
					deleteStatusNum = 0;
					grnDelete.setGrnStatus(originalStatus);
					
					log.info("10");
					grnDebug = grnHeaderRepository.getOne(id);
					log.info(grnDebug.getGrnStatus().getName());
				}
			}
				
		}catch(Exception e){
			deleteStatus = "Cannot Delete GRN";
			deleteStatusNum = 0;
			log.info("11");
			grnDebug = grnHeaderRepository.getOne(id);
			log.info(grnDebug.getGrnStatus().getName());
		}
		
		res.setResponseString(deleteStatus);
		res.setResponseStatus(deleteStatusNum);
		
		//dchDebug = deliveryChallanHeaderRepository.getOne(id);
		//log.info(dchDebug.getStatus().getName());
		
		return res;
	}
	
//	private void deleteStatusChangeEntry(String id) {
//		List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository
//				.findByTransactionId(id);
//
//		transactionStatusChangeList.forEach(tr -> {
//			transactionStatusChangeRepository.delete(tr);
//		});
//
//		transactionStatusChangeList = transactionStatusChangeRepository.findBySourceTransactionId(id);
//
//		transactionStatusChangeList.forEach(tr -> {
//			transactionStatusChangeRepository.delete(tr);
//		});
//		
//	}

	@Override
	public TransactionResponseDTO deleteItem(String id){
		TransactionResponseDTO res = new TransactionResponseDTO();
		res.setResponseStatus(1);
		String deleteStatus = "Grn Item Deleted Successfully";
		try {

			GRNItem grnDelete = grnItemRepository.getOne(id);
			//update stock
			log.info("grnDelete: "+grnDelete.getId());
			
//			Material material = materialRepository.getOne(grnDelete.getMaterial().getId());
//			
//			log.info("material.getSupplyType().getName(): "+material.getSupplyType().getName());
//			
//			if(material.getSupplyType().getName().equals(ApplicationConstants.SUPPLY_TYPE_GOODS)){
//				material.setStock(material.getStock() != null ? material.getStock() - grnDelete.getAcceptedQuantity() : null);
//				log.info("stock changed: "+material.getStock());
//				materialRepository.save(material);
//			}
			
			//grnBackTrackingService.itemDelete(grnDelete);
			
			grnItemRepository.deleteForce(grnDelete.getId());
			log.info("Deleted");
		
		}catch(Exception e){
			deleteStatus = "Cannot Delete Grn Item";
			res.setResponseStatus(0);
			log.error("Error: "+e);
		}
		
		res.setResponseString(deleteStatus);
		return res;
	}

	@Override
	public Long getMaxGRNNumberByTransactionType(Long id) {
		// TODO Auto-generated method stub
		return grnHeaderRepository.getMaxGrnNumberByTransactionType(id);
	}

	
	private void statusChangeEntry(GRNHeader grnHeader){
		
		TransactionStatusChange transactionStatusChange = new TransactionStatusChange();
		
		transactionStatusChange.setSourceOperation(null);
		transactionStatusChange.setSourceTransactionId(null);
		transactionStatusChange.setSourceTransactionType(null);
		transactionStatusChange.setSourceTransactionNumber(null);
		transactionStatusChange.setStatus(grnHeader.getGrnStatus());
		transactionStatusChange.setTransactionId(grnHeader.getId());
		transactionStatusChange.setTransactionType(grnHeader.getGrnType());
		transactionStatusChange.setTransactionNumber(grnHeader.getGrnNumber());
		transactionStatusChange.setParty(grnHeader.getSupplier());
		

		transactionStatusChangeRepository.save(transactionStatusChange);
		
	}
	
	private void deleteStatusChangeEntry(String id){
		
		List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository.findByTransactionId(id);
		
		transactionStatusChangeList.forEach(tr -> {
			transactionStatusChangeRepository.delete(tr);
		});
		
		transactionStatusChangeList = transactionStatusChangeRepository.findBySourceTransactionId(id);
		
		transactionStatusChangeList.forEach(tr -> {
			transactionStatusChangeRepository.delete(tr);
		});
		
		
	}

	@Override
	public List<GRNHeaderDTO> getByPartyAndStatusNotIn(Party party, List<Status> statuses) {
		return grnHeaderMapper.modelToDTOList(grnHeaderRepository.findBySupplierAndGrnStatusNotIn(party, statuses));
	}
	
	@Override
	public GRNHeader getModelById(String id) {

		return grnHeaderRepository.getOne(id);
	}
	
	@Override
	public GRNItem getItemModelById(String id) {
		return grnItemRepository.getOne(id);
	}
	
	@Override
	public Long countGRNByGRNNumber(String grnNumber, Long partyId,String id) {
		FinancialYear financialYear = financialYearService.findByIsActive(1);
		log.info("id from front end :"+ id);
		Party party = partyService.getPartyObjectById(partyId);	
		log.info("party :"+ party);

		 return grnHeaderRepository.countByGrnNumberAndSupplierAndIdNotInAndFinancialYear(grnNumber, party,id,financialYear);
	}
}
