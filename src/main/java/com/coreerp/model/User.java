package com.coreerp.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="ma_user", uniqueConstraints = {
	      @UniqueConstraint(name="UQ_USER_EMAIL", columnNames={"email"})
	      ,@UniqueConstraint(name="UQ_USER_EMPNUMBER", columnNames={"emp_no"})
	      ,@UniqueConstraint(name="UQ_USER_USERNAME", columnNames={"username"})})
public class User extends AuditModelAbstractEntity{
	

	private String username;
	
	private String email;

	private String password;

	private String name;

	private String lastName;
	
	private int active;
	
	private Set<Role> roles;
	
	private String currentAddress;
	private String permanentAddress;
    private NamePrefix namePrefix ;
	private Country country;
	private State state;
	private City city;
	private Area area;
	private PersonGender gender;
	private String pinCode;
	private String employeeNumber;
	private String telephoneNumber;
	private String mobileNumber;
	private Date dateOfBirth;
	private Date dateOfJoin;
	private Date dateOfLeaving;
	private String comments;
	private String panNumber;
	private String displayName;
	private Company company;
	private EmployeeType employeeType;
	private String employeeId;
	private SecurityQuestion securityQuestion;
	private String securityAnswer;
	private Long isLogInRequired;
	private String deleted = "N";
	private String userThemeName;
	private  String userLanguage;
	
	@Column(name="username")
	//@NotEmpty(message = "*Please provide username")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Column(name = "email")
//	@Email(message = "*Please provide a valid Email")
//	@NotEmpty(message = "*Please provide an email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "password")
	@Length(min = 5, message = "*Your password must have at least 5 characters")
	//@NotEmpty(message = "*Please provide your password")
	//@Transient
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "name")
	@NotEmpty(message = "*Please provide your name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "last_name")
	//@NotEmpty(message = "*Please provide your last name")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name = "active")
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "ma_user_role"
			, joinColumns = @JoinColumn(name = "user_id")
			, inverseJoinColumns = @JoinColumn(name = "role_id")
			, foreignKey = @ForeignKey(name = "FK_USER_ROLE")
    		, inverseForeignKey = @ForeignKey(name = "FK_ROLE_USER")
	)	
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	@Column(name = "current_address")
	public String getCurrentAddress() {
		return currentAddress;
	}
	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}
	@Column(name = "permanent_address")
	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
	@ManyToOne
	@JoinColumn(name="prefix_id", foreignKey=@ForeignKey(name="FK_USER_PREFIX"))
	public NamePrefix getNamePrefix() {
		return namePrefix;
	}
	public void setNamePrefix(NamePrefix namePrefix) {
		this.namePrefix = namePrefix;
	}
	@ManyToOne
	@JoinColumn(name="country_id", foreignKey=@ForeignKey(name="FK_USER_COUNTRY"))
	public Country getCountry() {
		return country;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	@ManyToOne
	@JoinColumn(name="state_id", foreignKey=@ForeignKey(name="FK_USER_STATE"))
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	@ManyToOne
	@JoinColumn(name="city_id", foreignKey=@ForeignKey(name="FK_USER_CITY"))
	public City getCity() {
		return city;
	}
	public void setCity(City city) {
		this.city = city;
	}
	@ManyToOne
	@JoinColumn(name="area_id", foreignKey=@ForeignKey(name="FK_USER_AREA"))
	public Area getArea() {
		return area;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	@ManyToOne
	@JoinColumn(name="gender_id", foreignKey=@ForeignKey(name="FK_USER_GENDER"))
	public PersonGender getGender() {
		return gender;
	}
	public void setGender(PersonGender gender) {
		this.gender = gender;
	}
	@Column(name = "pin_code")
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	
	@Column(name = "emp_no")
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	
	@Column(name = "tel_no")
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	@Column(name = "mobile_no")
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	@Column(name = "dob")
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	@Column(name = "doj")
	public Date getDateOfJoin() {
		return dateOfJoin;
	}
	public void setDateOfJoin(Date dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}
	@Column(name = "dol")
	public Date getDateOfLeaving() {
		return dateOfLeaving;
	}
	public void setDateOfLeaving(Date dateOfLeaving) {
		this.dateOfLeaving = dateOfLeaving;
	}
	@Column(name = "comments")
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	@Column(name = "pan_no")
	public String getPanNumber() {
		return panNumber;
	}
	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	@Column(name = "display_name")
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	@ManyToOne
	@JoinColumn(name="company_id", foreignKey=@ForeignKey(name="FK_USER_COMPANY"))
	@NotNull
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	@ManyToOne
	@JoinColumn(name="emp_type_id", foreignKey=@ForeignKey(name="FK_USER_EMPTYP"))
	@NotNull
	public EmployeeType getEmployeeType() {
		return employeeType;
	}
	public void setEmployeeType(EmployeeType employeeType) {
		this.employeeType = employeeType;
	}
	@Column(name = "emp_id")
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	@ManyToOne
	@JoinColumn(name="sec_quest_id", foreignKey=@ForeignKey(name="FK_USER_SECQ"))
	public SecurityQuestion getSecurityQuestion() {
		return securityQuestion;
	}
	public void setSecurityQuestion(SecurityQuestion securityQuestion) {
		this.securityQuestion = securityQuestion;
	}
	
	@Column(name = "sec_quest_ans")
	public String getSecurityAnswer() {
		return securityAnswer;
	}
	
	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}
	@Column(name = "is_login_required")
	public Long getIsLogInRequired() {
		return isLogInRequired;
	}
	public void setIsLogInRequired(Long isLogInRequired) {
		this.isLogInRequired = isLogInRequired;
	}
	@Column(name = "deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		if(deleted != null)
		this.deleted = deleted;
	}
	
	@Column(name = "user_theme_name")
	public String getUserThemeName() {
		return userThemeName;
	}
	public void setUserThemeName(String userThemeName) {
		this.userThemeName = userThemeName;
	}

	@Column(name = "user_language")
	public String getUserLanguage() {
		return userLanguage;
	}

	public void setUserLanguage(String userLanguage) {
		this.userLanguage = userLanguage;
	}
}
