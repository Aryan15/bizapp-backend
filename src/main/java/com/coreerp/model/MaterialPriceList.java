package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name="ma_price_list")
@Where(clause = "deleted='N'")
public class MaterialPriceList extends AuditModelAbstractEntity{
	
	private Party party;
	
	private Material material;
	
	private Double discountPercentage;
	
	private Double sellingPrice;
	
	private String comments;
	
	private String deleted= "N";

	@ManyToOne
	@JoinColumn(name="party_id" , foreignKey=@ForeignKey(name="FK_MPL_PARTY"))
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@ManyToOne
	@JoinColumn(name="material_id" , foreignKey=@ForeignKey(name="FK_MPL_MATERIAL"))
	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	@Column(name="disc_percentage")
	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	@Column(name="selling_price")
	public Double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	@Column(name="comments")
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Column(name="deleted")
	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		if(deleted != null){
			this.deleted = deleted;
		}
	}


	
	

}
