package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_grn_item")
public class GRNItem extends AbstractTransactionModel{

	private GRNHeader grnHeader;
	private Material material;
	private Integer slNo;
	private Double deliveryChallanQuantity;
	private Double acceptedQuantity;
	private Double rejectedQuantity;
	private PurchaseOrderHeader purchaseOrderHeader;
	private Double balanceQuantity;
	
	private String remarks;
	private Double amount;
	private DeliveryChallanHeader deliveryChallanHeader;
	private DeliveryChallanItem deliveryChallanItem;
	private UnitOfMeasurement unitOfMeasurement;
	private Double unitPrice;
	private String materialSpecification;
	private Tax tax;

	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="grn_header_id",foreignKey=@ForeignKey(name="FK_GRNI_GRNH"))
	@NotNull
	public GRNHeader getGrnHeader() {
		return grnHeader;
	}
	public void setGrnHeader(GRNHeader grnHeader) {
		this.grnHeader = grnHeader;
	}
	
	@ManyToOne
	@JoinColumn(name="material_id",foreignKey=@ForeignKey(name="FK_GRNI_MAT"))
	@NotNull
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	@Column(name="dc_quantity")
	public Double getDeliveryChallanQuantity() {
		return deliveryChallanQuantity;
	}
	public void setDeliveryChallanQuantity(Double deliveryChallanQuantity) {
		this.deliveryChallanQuantity = deliveryChallanQuantity;
	}
	
	@Column(name="accepted_quantity")
	@NotNull
	public Double getAcceptedQuantity() {
		return acceptedQuantity;
	}
	public void setAcceptedQuantity(Double acceptedQuantity) {
		this.acceptedQuantity = acceptedQuantity;
	}
	
	@Column(name="rejected_quantity")
	@NotNull
	public Double getRejectedQuantity() {
		return rejectedQuantity;
	}
	public void setRejectedQuantity(Double rejectedQuantity) {
		this.rejectedQuantity = rejectedQuantity;
	}
	
	
	@ManyToOne
	@JoinColumn(name="po_header_id",foreignKey=@ForeignKey(name="FK_GRNI_POH"))
	public PurchaseOrderHeader getPurchaseOrderHeader() {
		return purchaseOrderHeader;
	}
	public void setPurchaseOrderHeader(PurchaseOrderHeader purchaseOrderHeader) {
		this.purchaseOrderHeader = purchaseOrderHeader;
	}
	@Column(name="balance_quantity")
	@Min(0)
	public Double getBalanceQuantity() {
		return balanceQuantity;
	}
	public void setBalanceQuantity(Double balanceQuantity) {
		this.balanceQuantity = balanceQuantity;
	}
	
	
	@Column(name="remarks")
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	@Column(name="amount")
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	@ManyToOne
	@JoinColumn(name="dc_id",foreignKey=@ForeignKey(name="FK_GRNI_DCH") )
	public DeliveryChallanHeader getDeliveryChallanHeader() {
		return deliveryChallanHeader;
	}
	public void setDeliveryChallanHeader(DeliveryChallanHeader deliveryChallanHeader) {
		this.deliveryChallanHeader = deliveryChallanHeader;
	}
	
	@OneToOne
	@JoinColumn(name="dc_item_id",foreignKey=@ForeignKey(name="FK_GRNI_DCI"))
	public DeliveryChallanItem getDeliveryChallanItem() {
		return deliveryChallanItem;
	}
	public void setDeliveryChallanItem(DeliveryChallanItem deliveryChallanItem) {
		this.deliveryChallanItem = deliveryChallanItem;
	}
	
	@ManyToOne
	@JoinColumn(name="uom_id",foreignKey=@ForeignKey(name="FK_GRNI_UOM"))
	public UnitOfMeasurement getUnitOfMeasurement() {
		return unitOfMeasurement;
	}
	public void setUnitOfMeasurement(UnitOfMeasurement unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}
	
	@Column(name="unit_price")
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	@Column(name="mat_spec")
	public String getMaterialSpecification() {
		return materialSpecification;
	}
	public void setMaterialSpecification(String materialSpecification) {
		this.materialSpecification = materialSpecification;
	}
	@ManyToOne
	@JoinColumn(name="tax_id",foreignKey=@ForeignKey(name="FK_GRNI_TAX"))
	public Tax getTax() {
		return tax;
	}
	public void setTax(Tax tax) {
		this.tax = tax;
	}

	@Column(name="sl_no")
	public Integer getSlNo() {
		return slNo;
	}

	/**
	 * @param slNo the slNo to set
	 */
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	
	
	
	
}
