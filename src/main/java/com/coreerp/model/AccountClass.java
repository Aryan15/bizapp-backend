package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="cd_account_class")
public class AccountClass extends AuditModelAbstractEntity{
	
	private String name;
	
	private String description;

	@Column(name="name")
	public String getName() {
		return name;
	}

	@Column(name="desc")
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	

}
