package com.coreerp.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;
@Entity
@Table(name="conf_auto_transaction")
public class AutoTransactionGeneration extends AuditModelAbstractEntity{

    private String invoiceHeaderId;

    private Integer periodToCreateTransaction;

    private Integer  daysOfMonth;

    private  Integer countOfInvoice;

    private Date startDate;

    private Integer daysOfWeek;

    private String invoiceNumber;


    private Integer stopTranaction;

    private Integer email;

    private Date endDate;

    private Long partyId;

    private String sheaduledDates;


    @Column(name="source_invoice_id")
    public String getInvoiceHeaderId() {
        return invoiceHeaderId;
    }

    public void setInvoiceHeaderId(String invoiceHeaderId) {
        this.invoiceHeaderId = invoiceHeaderId;
    }

    @Column(name="period_to_create_invoice")
    public Integer getPeriodToCreateTransaction() {
        return periodToCreateTransaction;
    }

    public void setPeriodToCreateTransaction(Integer periodToCreateTransaction) {
        this.periodToCreateTransaction = periodToCreateTransaction;
    }

    @Column(name="days_of_month")
    public Integer getDaysOfMonth() {
        return daysOfMonth;
    }

    public void setDaysOfMonth(Integer daysOfMonth) {
        this.daysOfMonth = daysOfMonth;
    }


    @Column(name="count_of_invoice")
    public Integer getCountOfInvoice() {
        return countOfInvoice;
    }

    public void setCountOfInvoice(Integer countOfInvoice) {
        this.countOfInvoice = countOfInvoice;
    }


    @Column(name="start_date")


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }




    @Column(name="days_of_week")
    public Integer getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(Integer daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    @Column(name="invoice_number")
    public String getInvoiceNumber() {
        return invoiceNumber;
    }



    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }


    @Column(name="stop_transaction")
    public Integer getStopTranaction() {
        return stopTranaction;
    }

    public void setStopTranaction(Integer stopTranaction) {
        this.stopTranaction = stopTranaction;
    }

    @Column(name="allow_email")
    public Integer getEmail() {
        return email;
    }

    public void setEmail(Integer email) {
        this.email = email;
    }


    @Column(name="end_date")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Column(name="party_id")
    public Long getPartyId() {
        return partyId;
    }

    public void setPartyId(Long partyId) {
        this.partyId = partyId;
    }

    @Column(name="scheduled_dates")
    public String getSheaduledDates() {
        return sheaduledDates;
    }

    public void setSheaduledDates(String sheaduledDates) {
        this.sheaduledDates = sheaduledDates;
    }





    @Override
    public String toString() {
        return "AutoTransactionGeneration{" +
                "invoiceHeaderId='" + invoiceHeaderId + '\'' +
                ", periodToCreateTransaction=" + periodToCreateTransaction +
                ", daysOfMonth=" + daysOfMonth +
                ", countOfInvoice=" + countOfInvoice +
                ", startDate=" + startDate +
                ", daysOfWeek=" + daysOfWeek +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", stopTranaction=" + stopTranaction +
                ", email=" + email +
                ", endDate=" + endDate +
                ", partyId=" + partyId +
                ", sheaduledDates="+sheaduledDates+
                '}';
    }



}
