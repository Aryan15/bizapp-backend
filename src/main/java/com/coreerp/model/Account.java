package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ma_account")
public class Account extends AuditModelAbstractEntity{
	
	private String accountNumber;
	
	private String name;
	
	private String description;
	
	private AccountClass accountClass;

	@Column(name="acct_no")
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="descr")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne
	@JoinColumn(name="acct_class_id", foreignKey = @ForeignKey(name="FK_ACCT_ACCTCLS"))
	public AccountClass getAccountClass() {
		return accountClass;
	}

	public void setAccountClass(AccountClass accountClass) {
		this.accountClass = accountClass;
	}
	
	
	
	

}
