package com.coreerp.model;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="ma_party", uniqueConstraints = {
	      @UniqueConstraint(name="UQ_PARTY_EMAIL", columnNames={"email"})})
@Where(clause = "deleted='N'")
public class Party extends AuditModelAbstractEntity{
	
	private String name;
	
	private String address;
	
	private Country country;
	
	private State state;
	
	private City city;
	
	private Area area;
	
	private String pinCode;
	
	private String primaryTelephone;
	
	private String secondaryTelephone;
	
	private String primaryMobile;
	
	private String secondaryMobile;
	
	private String email;
	
	private String contactPersonName;
	
	private String contactPersonNumber;
	
	private String webSite;
	
	private PartyType partyType;
	
	private String deleted = "N";
	
	private String billAddress;
	
	private String panNumber;
	
	private GstRegistrationType gstRegistrationType;
	
	private String gstNumber;
	
	private List<Material> materials;
	
	private List<Address> addresses;
	
	private Integer dueDaysLimit;
	
	private Integer isIgst;

	private Long partyId;

	private  String partyCode;

	private Long partyCurrencyId;
    private String partyCurrencyName;

	//private Integer portStateId;




	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="address")
	@NotNull
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@ManyToOne
	@JoinColumn(name="country_id", foreignKey=@ForeignKey(name="FK_PARTY_COUNTRY"))
	@NotNull
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@ManyToOne
	@JoinColumn(name="state_id", foreignKey=@ForeignKey(name="FK_PARTY_STATE"))
	@NotNull
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@ManyToOne
	@JoinColumn(name="city_id", foreignKey=@ForeignKey(name="FK_PARTY_CITY"))
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@ManyToOne
	@JoinColumn(name="area_id", foreignKey=@ForeignKey(name="FK_PARTY_AREA"))
	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	@Column(name="pin_code")
	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	@Column(name="primary_telephone")
	public String getPrimaryTelephone() {
		return primaryTelephone;
	}

	public void setPrimaryTelephone(String primaryTelephone) {
		this.primaryTelephone = primaryTelephone;
	}

	@Column(name="secondary_telephone")
	public String getSecondaryTelephone() {
		return secondaryTelephone;
	}

	public void setSecondaryTelephone(String secondaryTelephone) {
		this.secondaryTelephone = secondaryTelephone;
	}

	@Column(name="primary_mobile")
	public String getPrimaryMobile() {
		return primaryMobile;
	}

	public void setPrimaryMobile(String primaryMobile) {
		this.primaryMobile = primaryMobile;
	}

	@Column(name="secondary_mobile")
	public String getSecondaryMobile() {
		return secondaryMobile;
	}

	public void setSecondaryMobile(String secondaryMobile) {
		this.secondaryMobile = secondaryMobile;
	}

	@Column(name="email") //,unique=true
	@NotNull
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if(email == null) {
			email = "notnull@notnull.com"+Math.random();
		}
		this.email = email;
	}

	@Column(name="contact_person")
	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	@Column(name="contact_person_number")
	public String getContactPersonNumber() {
		return contactPersonNumber;
	}

	public void setContactPersonNumber(String contactPersonNumber) {
		this.contactPersonNumber = contactPersonNumber;
	}

	@Column(name="website")
	public String getWebSite() {
		return webSite;
	}

	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}
	
	@ManyToOne
	@JoinColumn(name="party_type_id", foreignKey=@ForeignKey(name="FK_PARTY_PTYPE"))
	@NotNull
	public PartyType getPartyType() {
		return partyType;
	}

	public void setPartyType(PartyType partyType) {
		this.partyType = partyType;
	}

	@Column(name="deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		if(deleted != null){
			this.deleted = deleted;
		}
	}

	@Column(name="bill_address")
	public String getBillAddress() {
		return billAddress;
	}

	public void setBillAddress(String billAddress) {
		this.billAddress = billAddress;
	}

	@Column(name="pan_number")
	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	
	
	@ManyToOne
	@JoinColumn(name="gst_reg_type_id", foreignKey=@ForeignKey(name="FK_PARTY_GSTRT"))
	@NotNull
	public GstRegistrationType getGstRegistrationType() {
		return gstRegistrationType;
	}

	public void setGstRegistrationType(GstRegistrationType gstRegistrationType) {
		this.gstRegistrationType = gstRegistrationType;
	}

	@Column(name="gst_number")
	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	@OneToMany(mappedBy="party")
	@JsonIgnore
	public List<Material> getMaterials() {
		return materials;
	}

	public void setMaterials(List<Material> materials) {
		this.materials = materials;
	}
	
	
	@OneToMany()
	@JoinTable(name = "ma_party_address"
			, joinColumns = @JoinColumn(name = "party_id")
			, inverseJoinColumns = @JoinColumn(name = "address_id")
			, foreignKey = @ForeignKey(name = "FK_PARTY_ADDRESS")
    		, inverseForeignKey = @ForeignKey(name = "FK_ADDRESS_PARTY")
			, uniqueConstraints=@UniqueConstraint(name="UK_PARTY_ADDRESS",columnNames={"address_id"})
	)
	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}



    @Column(name="due_days_limit")
	public Integer getDueDaysLimit() {
		return dueDaysLimit;
	}

	public void setDueDaysLimit(Integer dueDaysLimit) {
		this.dueDaysLimit = dueDaysLimit;
	}

	@Column(name="is_igst")
	public Integer getIsIgst() {
		return isIgst;
	}

	public void setIsIgst(Integer isIgst) {
		this.isIgst = isIgst;
	}

	@Column(name="party_code_id")
	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	@Column(name="party_code")

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	@Column(name="party_currency_id")

	public Long getPartyCurrencyId() {
		return partyCurrencyId;
	}

	public void setPartyCurrencyId(Long partyCurrencyId) {
		this.partyCurrencyId = partyCurrencyId;
	}
	@Column(name="party_currency_name")
	public String getPartyCurrencyName() {
		return partyCurrencyName;
	}

	public void setPartyCurrencyName(String partyCurrencyName) {
		this.partyCurrencyName = partyCurrencyName;
	}

	@Override
	public String toString() {
		return "Party{" +
				"name='" + name + '\'' +
				", address='" + address + '\'' +
				", country=" + country +
				", state=" + state +
				", city=" + city +
				", area=" + area +
				", pinCode='" + pinCode + '\'' +
				", primaryTelephone='" + primaryTelephone + '\'' +
				", secondaryTelephone='" + secondaryTelephone + '\'' +
				", primaryMobile='" + primaryMobile + '\'' +
				", secondaryMobile='" + secondaryMobile + '\'' +
				", email='" + email + '\'' +
				", contactPersonName='" + contactPersonName + '\'' +
				", contactPersonNumber='" + contactPersonNumber + '\'' +
				", webSite='" + webSite + '\'' +
				", partyType=" + partyType +
				", deleted='" + deleted + '\'' +
				", billAddress='" + billAddress + '\'' +
				", panNumber='" + panNumber + '\'' +
				", gstRegistrationType=" + gstRegistrationType +
				", gstNumber='" + gstNumber + '\'' +
				", materials=" + materials +
				", addresses=" + addresses +
				", dueDaysLimit=" + dueDaysLimit +
				", isIgst=" + isIgst +
				", partyId=" + partyId +
				", partyCode='" + partyCode + '\'' +
				", partyCurrencyId=" + partyCurrencyId +
				", partyCurrencyName=" + partyCurrencyName +
				'}';
	}
}
