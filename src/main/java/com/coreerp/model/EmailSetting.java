package com.coreerp.model;

import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="conf_email_setting")
@Where(clause = "deleted='N'")
public class EmailSetting extends AuditModelAbstractEntity{
	private String emailHost;
	private Integer emailPort;
	private String emailUsername;
	private String emailPassword;
	private Integer isEmailEnabled;
	private String emailSmtpStarttlsEnable;
	private String emailSmtpAuth;
	private String emailTransportProtocol;
	private String emailDebug;
	private String emailFrom;
	private String deleted="N";

	private String emailText;
	private String emailSubject;
	private Integer emailType;


	@Column(name="email_host")
	public String getEmailHost() {
		return emailHost;
	}
	public void setEmailHost(String emailHost) {
		this.emailHost = emailHost;
	}
	
	@Column(name="email_port")
	public Integer getEmailPort() {
		return emailPort;
	}
	public void setEmailPort(Integer emailPort) {
		this.emailPort = emailPort;
	}
	
	@Column(name="email_user_name")
	public String getEmailUsername() {
		return emailUsername;
	}
	public void setEmailUsername(String emailUsername) {
		this.emailUsername = emailUsername;
	}
	
	@Column(name="email_password")
	public String getEmailPassword() {
		return emailPassword;
	}
	public void setEmailPassword(String emailPassword) {
		this.emailPassword = emailPassword;
	}
	
	@Column(name="email_enabled")
	public Integer getIsEmailEnabled() {
		return isEmailEnabled;
	}
	public void setIsEmailEnabled(Integer isEmailEnabled) {
		this.isEmailEnabled = isEmailEnabled;
	}
	
	@Column(name="email_smtp_start_tls_enable")
	public String getEmailSmtpStarttlsEnable() {
		return emailSmtpStarttlsEnable;
	}
	public void setEmailSmtpStarttlsEnable(String emailSmtpStarttlsEnable) {
		this.emailSmtpStarttlsEnable = emailSmtpStarttlsEnable;
	}
	
	@Column(name="email_smtp_auth")
	public String getEmailSmtpAuth() {
		return emailSmtpAuth;
	}
	public void setEmailSmtpAuth(String emailSmtpAuth) {
		this.emailSmtpAuth = emailSmtpAuth;
	}
	
	@Column(name="email_transport_protocol")
	public String getEmailTransportProtocol() {
		return emailTransportProtocol;
	}
	public void setEmailTransportProtocol(String emailTransportProtocol) {
		this.emailTransportProtocol = emailTransportProtocol;
	}
	
	@Column(name="email_debug")
	public String getEmailDebug() {
		return emailDebug;
	}
	public void setEmailDebug(String emailDebug) {
		this.emailDebug = emailDebug;
	}
	
	@Column(name="email_from")
	public String getEmailFrom() {
		return emailFrom;
	}
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	@Column(name="deleted")
	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	@Column(name="email_text")
	public String getEmailText() {
		return emailText;
	}

	public void setEmailText(String emailText) {
		this.emailText = emailText;
	}

	@Column(name="email_subject")
	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	@Column(name="email_type")
	public Integer getEmailType() {
		return emailType;
	}

	public void setEmailType(Integer emailType) {
		this.emailType = emailType;
	}
}
