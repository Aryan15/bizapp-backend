package com.coreerp.model;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="tr_voucher_header_audit")
public class VoucherHeaderAudit {

    private String id;

    private Date createdDateTime;

    private String createdBy;

    private Date updatedDateTime;

    private String updatedBy;


    private Long voucherId;

    private Date voucherDate;

    private Long voucherTypeId;

    private Double amount;

    private String voucherNumber;

    private String comments;

    private String paidTo;

    private String chequeNumber;

    private String bankName;

    private Date chequeDate;

    private Long financialYearId;

    private Long companyId;


    @Id
    @Column(name="id", length=255)
    @Size(min=1)
    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }
    @Column(name="created_date_time",nullable = false, updatable = false)
    public Date getCreatedDateTime() {
        return createdDateTime;
    }
    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Column(name="created_by",nullable = false, updatable = false)
    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name="updated_date_time")
    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }
    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    @Column(name="updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name="voucher_date")
    public Date getVoucherDate() {
        return voucherDate;
    }


    public void setVoucherDate(Date voucherDate) {
        this.voucherDate = voucherDate;
    }

    @Column(name="voucher_type")
    public Long getVoucherTypeId() {
        return voucherTypeId;
    }

    public void setVoucherTypeId(Long  voucherTypeId) {
        this.voucherTypeId = voucherTypeId;
    }

    @Column(name="total_amount")
    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Column(name="voucher_number")
    public String getVoucherNumber() {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    @Column(name="comments")
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Column(name="paid_to")
    public String getPaidTo() {
        return paidTo;
    }

    public void setPaidTo(String paidTo) {
        this.paidTo = paidTo;
    }

    @Column(name="cheque_number")
    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    @Column(name="bank_name")
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Column(name="cheque_date")
    public Date getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(Date chequeDate) {
        this.chequeDate = chequeDate;
    }




    @Column(name="company_id")
    public Long getCompanyId() {
        return companyId;
    }

    @Column(name="financial_year_id")
    public Long getFinancialYearId() {
        return financialYearId;
    }


    public void setFinancialYearId(Long financialYearId) {
        this.financialYearId = financialYearId;
    }


    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Column(name="voucher_id")
    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }


}
