package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

@Entity
@Table(name="ma_bank")
@Where(clause = "deleted='N'")
public class Bank extends AuditModelAbstractEntity {
	
	 
	 private String bankName;
	 
	 private String bankAddress;
	 
	 private String deleted="N";
	 
//	 private String bankBranch;
//	 
//	 private String ifscCode;
//	 
//	 private String accountNumber;
//	 
//	 private String deleted="N";
//	 
//	 private Double openingBalance;
	 
	 
	 @Column(name="bank_name")
	 @NotNull
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Column(name="bank_address")
	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	

	@Column(name="deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}

//	@Column(name="opening_bal")
//	public Double getOpeningBalance() {
//		return openingBalance;
//	}
//
//	public void setOpeningBalance(Double openingBalance) {
//		this.openingBalance = openingBalance;
//	}
	 
	 
	 

}
