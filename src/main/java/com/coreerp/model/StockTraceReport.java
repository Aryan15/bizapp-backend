package com.coreerp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;


@Entity
@NamedNativeQuery(
		  name = "qryStockTraceReport"
		, query = " SELECT st.id "
				+ ", st.bus_date "
				+ ", st.trans_date"
				+ ", st.material_id"
				+ ", m.name AS material_name"
				+ ", CASE st.reverse "
				+ "		WHEN 'Y' THEN CONCAT('Reverse ',st.trans_note)"
				+ "		ELSE st.trans_note"
				+ "  END AS trans_note"
				+ ", st.trans_type_id"
				+ ", st.trans_header_id"
				+ ", st.trans_number"
				+ ", st.party_id"
				+ ", p.name AS party_name"
		        + ", st.qty_in"
				+ ", st.qty_out"
				+ ", (SELECT SUM(qty_in)-SUM(qty_out) FROM tr_stock_trace sti"
				+ "				WHERE 1 = 1"
				+ "				AND sti.created_date_time <= st.created_date_time"
				+ "				AND date(sti.trans_date)   BETWEEN :from_date AND :to_date"
				+ "				AND sti.material_id = st.material_id)"
				+ "	 AS closing_stock"
				+ ", IFNULL(st.reverse, 'N') AS reverse "
		        + ", p.party_code AS party_code"
		        + ", m.number AS number"
				+ "	FROM tr_stock_trace st"
				+ " INNER JOIN ma_material m "
				+ " ON (m.id = st.material_id)"
				+ " LEFT OUTER JOIN ma_party p "
				+ " ON (	p.id 		= st.party_id "
				+ "		AND p.deleted 	= 'N') "
				+ "	WHERE 1 = 1"
				+ "	AND date(st.trans_date) BETWEEN :from_date AND :to_date"
				+ "	AND st.material_id = IFNULL(:material_id, st.material_id)"
				+ " AND st.trans_date = (SELECT MAX(stt.trans_date) FROM tr_stock_trace stt " 
				+ "								WHERE stt.material_id = st.material_id " 
				+ "								AND stt.trans_header_id = st.trans_header_id) "
				+ " AND m.deleted = 'N'"
				+ " AND m.material_type_id != 4 "
				+ "	ORDER BY st.created_date_time "
		, resultClass = StockTraceReport.class)
public class StockTraceReport {

	 @Id
	 @Column
	 private String id;
	 
	 @Column(name="bus_date")
	 private Date businessDate;
	 
	 @Column(name="trans_date")
	 private Date transactionDate;
	 
	 @Column(name="material_id")
	 private Long materialId;
	 
	 @Column(name="material_name")
	 private String materialName;
	 
	 @Column(name="trans_note")
	 private String transactionNote;
	 
	 @Column(name="trans_type_id")
	 private Long transactionTypeId;
	 
	 @Column(name="trans_header_id")
	 private String transactionHeaderId;
	 
	 @Column(name="trans_number")
	 private String transactionNumber;
	 
	 @Column(name="party_id")
	 private Long partyId;
	 
	 @Column(name="party_name")
	 private String partyName;
	 
	 @Column(name="qty_in")
	 private Double quantityIn;
	 
	 @Column(name="qty_out")
	 private Double quantityOut;
	 
	 @Column(name="closing_stock")
	 private Double closingStock;
	 
	 @Column(name="reverse")
	 private String reverse;

	@Column(name="party_code")
	private String partyCode;

	@Column(name="number")
	private String materialPartNumber;
	  
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public String getTransactionNote() {
		return transactionNote;
	}

	public void setTransactionNote(String transactionNote) {
		this.transactionNote = transactionNote;
	}

	public Long getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public String getTransactionHeaderId() {
		return transactionHeaderId;
	}

	public void setTransactionHeaderId(String transactionHeaderId) {
		this.transactionHeaderId = transactionHeaderId;
	}

	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}	

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public Double getQuantityIn() {
		return quantityIn;
	}

	public void setQuantityIn(Double quantityIn) {
		this.quantityIn = quantityIn;
	}

	public Double getQuantityOut() {
		return quantityOut;
	}

	public void setQuantityOut(Double quantityOut) {
		this.quantityOut = quantityOut;
	}

	public Double getClosingStock() {
		return closingStock;
	}

	public void setClosingStock(Double closingStock) {
		this.closingStock = closingStock;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public String getReverse() {
		return reverse;
	}

	public void setReverse(String reverse) {
		this.reverse = reverse;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getMaterialPartNumber() {
		return materialPartNumber;
	}

	public void setMaterialPartNumber(String materialPartNumber) {
		this.materialPartNumber = materialPartNumber;
	}
}
