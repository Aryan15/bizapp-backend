package com.coreerp.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_pettycash_item")
public class PettyCashItem extends AbstractTransactionModel{


    private  PettyCashHeader pettyCashHeader;
    private Category category;
    private PaymentMethod paymentMethod;
    private String remarks;
    private Tag tag;
    private  Double incomingAmount;
    private Double outgoingAmount;


    @ManyToOne
    @JoinColumn(name="petty_cash_header_id", foreignKey=@ForeignKey(name="FK_PETI_PETH"))
    @NotNull
    public PettyCashHeader getPettyCashHeader() {
        return pettyCashHeader;
    }

    public void setPettyCashHeader(PettyCashHeader pettyCashHeader) {
        this.pettyCashHeader = pettyCashHeader;
    }

    @ManyToOne
    @JoinColumn(name="category_id", foreignKey=@ForeignKey(name="FK_PETI_CAT"))
    @NotNull
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @ManyToOne
    @JoinColumn(name="paymment_id", foreignKey=@ForeignKey(name="FK_PETI_PAYMENT"))
    @NotNull
    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }


    @ManyToOne
    @JoinColumn(name="tag_id", foreignKey=@ForeignKey(name="FK_PETI_TAG"))
    @NotNull
    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public Double getIncomingAmount() {
        return incomingAmount;
    }

    public void setIncomingAmount(Double incomingAmount) {
        this.incomingAmount = incomingAmount;
    }

    public Double getOutgoingAmount() {
        return outgoingAmount;
    }

    public void setOutgoingAmount(Double outgoingAmount) {
        this.outgoingAmount = outgoingAmount;
    }


}
