package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

@Entity
@Table(name="ma_party_bank_map")
@Where(clause = "deleted='N'")
public class PartyBankMap extends AuditModelAbstractEntity {

	private Company company;
	private Party party;
	private Bank bank;
	private String branch;
	private String ifsc;
	private String accountNumber;
	private Long bankAdCode;
	private Double openingBalance;
	private String contactNumber;
	private String deleted = "N";
	
	@ManyToOne
	@JoinColumn(name="company_id", foreignKey=@ForeignKey(name="FK_BANKMAP_COMPANY"))
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	@ManyToOne
	@JoinColumn(name="party_id", foreignKey=@ForeignKey(name="FK_BANKMAP_PARTY"))
	public Party getParty() {
		return party;
	}
	public void setParty(Party party) {
		this.party = party;
	}
	
	@ManyToOne
	@JoinColumn(name="bank_id", foreignKey=@ForeignKey(name="FK_BANKMAP_BANK"))
	@NotNull
	public Bank getBank() {
		return bank;
	}
	public void setBank(Bank bank) {
		this.bank = bank;
	}
	
	@Column(name="branch")
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	@Column(name="ifsc")
	public String getIfsc() {
		return ifsc;
	}
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	@Column(name="bank_Ad_Code")
	public Long getBankAdCode() {
		return bankAdCode;
	}

	public void setBankAdCode(Long bankAdCode) {
		this.bankAdCode = bankAdCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	@Column(name="opening_balance")
	public Double getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(Double openingBalance) {
		this.openingBalance = openingBalance;
	}
	
	@Column(name="contact_number")
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	@Column(name="deleted")
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}
	
	
	
}
