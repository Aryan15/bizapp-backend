package com.coreerp.model;

import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="cd_print_template")
@Where(clause = "deleted='N'")
public class PrintTemplate extends AuditModelAbstractEntity   {

    private String deleted="N";
    private String name;
    private Integer isJasperPrint;
    private String jasperFileName;

    @Column(name="deleted")
    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    @Column(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="is_jasper_print")
    public Integer getIsJasperPrint() {
        return isJasperPrint;
    }

    public void setIsJasperPrint(Integer isJasperPrint) {
        this.isJasperPrint = isJasperPrint;
    }

    @Column(name="jasper_file_name")
    public String getJasperFileName() {
        return jasperFileName;
    }

    public void setJasperFileName(String jasperFileName) {
        this.jasperFileName = jasperFileName;
    }
}
