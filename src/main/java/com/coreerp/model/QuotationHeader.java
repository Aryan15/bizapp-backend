package com.coreerp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="tr_quotation_header")
public class QuotationHeader extends AbstractTransactionModel{
	
	
	private String quotationNumber;
	
	private Long quotationId;
	
	private Date quotationDate;
	
	private String enquiryNumber;
	
	private Date enquiryDate;
	
	private Party party;
	
	private Double subTotalAmount;
	
	private Double discountAmount;
	
	private Double discountPercent;
	
	private Double netAmount;
	
//	private Double packAndForwardAmount;
//	
//	private Double freightCharges;
//	
	private Tax tax;
	
	private Double roundOffAmount;
	
	private Double grandTotal;
	
	//private String status;
	
	private Status status;
	
	private FinancialYear financialYear;
	
	private Company company;
	
	private String paymentTerms;
	
	private String deliveryTerms;
	
	private String termsAndConditions;
	
	private Double taxAmount;
	
//	private Double packAndForwardPercentage;
	
	private String kindAttention;
	
	private String quotationSubject;
	
	//private String quotationStatus;
	
	private String isPaymentChecked;
	
	private Double sgstTaxRate;
	
	private Double sgstTaxAmount;
	
	private Double cgstTaxRate;
	
	private Double cgstTaxAmount;
	
	private Double igstTaxRate;
	
	private Double igstTaxAmount;	
	
	private Double amount;
	
	private TransactionType quotationType;
	
	private String remarks;
	
	private List<QuotationItem> quotationItems;
	
	private Integer inclusiveTax;
	
	private String address;
	private String gstNumber;
	
    private String companyName; 
    private Long companygGstRegistrationTypeId; //companyGstRegistrationnTypeId
    private String companyPinCode; //companyPinCode .. etc
    private Long companyStateId;
    private String companyStateName;
    private Long companyCountryId;
    private String companyPrimaryMobile;
    private String companySecondaryMobile;
    private String companyContactPersonNumber;
    private String companyContactPersonName;
    private String companyPrimaryTelephone;
    private String companySecondaryTelephone;
    private String companyWebsite;
    private String companyEmail;
    private String companyFaxNumber;
    private String companyAddress;
    private String companyTagLine;
    private String companyGstNumber;
    private String companyPanNumber;
    private String companyPanDate;
    private String companyCeritificateImagePath;
    private String companyLogoPath;
    
    private String partyName;
    private String partyContactPersonNumber;
    private String partyPinCode;
    private Long partyAreaId;
    private Long partyCityId;
    private Long partyStateId;
    private Long partyCountryId;
    private Long partyCurrencyId;
    private String partyPrimaryTelephone;
    private String partySecondaryTelephone;
    private String partyPrimaryMobile;
    private String partySecondaryMobile;
    private String partyEmail;
    private String partyWebsite;
    private String partyContactPersonName;
//    private String partyBillToAddress;
//    private String partyShipAddress;
    private String partyDueDaysLimit;
    private String partyGstRegistrationTypeId;
//    private String partyGstNumber;
    private String partyPanNumber;
    private String isIgst;
    private String currencyName;



	@Column (name="remarks")
	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	@Column(name="amount")
	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name="quotation_number")
	public String getQuotationNumber() {
		return quotationNumber;
	}


	public void setQuotationNumber(String quotationNumber) {
		this.quotationNumber = quotationNumber;
	}


	@Column(name="quotation_id")
	public Long getQuotationId() {
		return quotationId;
	}


	public void setQuotationId(Long quotationId) {
		this.quotationId = quotationId;
	}

	@Column(name="quotation_date")
	@NotNull
	public Date getQuotationDate() {
		return quotationDate;
	}


	public void setQuotationDate(Date quotationDate) {
		this.quotationDate = quotationDate;
	}


	@Column(name="enq_number")
	public String getEnquiryNumber() {
		return enquiryNumber;
	}


	public void setEnquiryNumber(String enquiryNumber) {
		this.enquiryNumber = enquiryNumber;
	}

	@Column(name="enq_date")
	public Date getEnquiryDate() {
		return enquiryDate;
	}


	public void setEnquiryDate(Date enquiryDate) {
		this.enquiryDate = enquiryDate;
	}

	@ManyToOne
	@JoinColumn(name="party_id", foreignKey=@ForeignKey(name="FK_QOTH_PARTY"))
	@NotNull
	public Party getParty() {
		return party;
	}


	public void setParty(Party party) {
		this.party = party;
	}

	@Column(name="sub_total_amt")
	public Double getSubTotalAmount() {
		return subTotalAmount;
	}


	public void setSubTotalAmount(Double subTotalAmount) {
		this.subTotalAmount = subTotalAmount;
	}

	@Column(name="discount_amt")
	public Double getDiscountAmount() {
		return discountAmount;
	}


	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	@Column(name="discount_percent")
	public Double getDiscountPercent() {
		return discountPercent;
	}


	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}

	@Column(name="net_amt")
	public Double getNetAmount() {
		return netAmount;
	}


	public void setNetAmount(Double netAmount) {
		this.netAmount = netAmount;
	}

	@ManyToOne
	@JoinColumn(name="tax_id", foreignKey=@ForeignKey(name="FK_QOTH_TAX"))
	@NotNull
	public Tax getTax() {
		return tax;
	}


	public void setTax(Tax tax) {
		this.tax = tax;
	}

	@Column(name="round_off_amt")
	public Double getRoundOffAmount() {
		return roundOffAmount;
	}


	public void setRoundOffAmount(Double roundOffAmount) {
		this.roundOffAmount = roundOffAmount;
	}

	@Column(name="grand_total")
	@NotNull
	public Double getGrandTotal() {
		return grandTotal;
	}


	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	
	@ManyToOne
	@JoinColumn(name="status_id",foreignKey=@ForeignKey(name="FK_QOTH_ST"))
	@NotNull
	public Status getStatus() {
		return status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}

	@ManyToOne
	@JoinColumn(name="financial_year_id", foreignKey=@ForeignKey(name="FK_QOTH_FY"))
	@NotNull
	public FinancialYear getFinancialYear() {
		return financialYear;
	}


	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	@ManyToOne
	@JoinColumn(name="company_id", foreignKey=@ForeignKey(name="FK_QOTH_COMPANY"))
	@NotNull
	public Company getCompany() {
		return company;
	}


	public void setCompany(Company company) {
		this.company = company;
	}

	@Column(name="payment_terms")
	public String getPaymentTerms() {
		return paymentTerms;
	}


	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	@Column(name="delivery_terms")
	public String getDeliveryTerms() {
		return deliveryTerms;
	}


	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}

	@Column(name="terms_conditions")
	public String getTermsAndConditions() {
		return termsAndConditions;
	}


	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	@Column(name="tax_amt")
	public Double getTaxAmount() {
		return taxAmount;
	}


	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	@Column(name="kind_attention")
	public String getKindAttention() {
		return kindAttention;
	}


	public void setKindAttention(String kindAttention) {
		this.kindAttention = kindAttention;
	}

	@Column(name="quotation_subject")
	public String getQuotationSubject() {
		return quotationSubject;
	}


	public void setQuotationSubject(String quotationSubject) {
		this.quotationSubject = quotationSubject;
	}

//	@Column(name="quotation_status")
//	public String getQuotationStatus() {
//		return quotationStatus;
//	}
//
//
//	public void setQuotationStatus(String quotationStatus) {
//		this.quotationStatus = quotationStatus;
//	}

	@Column(name="is_payment_checked")
	public String getIsPaymentChecked() {
		return isPaymentChecked;
	}


	public void setIsPaymentChecked(String isPaymentChecked) {
		this.isPaymentChecked = isPaymentChecked;
	}

	@Column(name="sgst_tax_rate")
	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}


	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	@Column(name="sgst_tax_amt")
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}


	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}

	@Column(name="cgst_tax_rate")
	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}


	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}

	@Column(name="cgst_tax_amt")
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}


	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}

	@Column(name="igst_tax_rate")
	public Double getIgstTaxRate() {
		return igstTaxRate;
	}


	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}

	@Column(name="igst_tax_amt")
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}


	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	@OneToMany(mappedBy="quotationHeader", cascade=CascadeType.ALL)
	@NotNull
	@JsonIgnore
	@OrderBy("slNo ASC")
	public List<QuotationItem> getQuotationItems() {
		return quotationItems;
	}

	@ManyToOne
	@JoinColumn(name="quotation_type_id", foreignKey=@ForeignKey(name="FK_QOTH_TT"))
	public TransactionType getQuotationType() {
		return quotationType;
	}


	public void setQuotationType(TransactionType quotationType) {
		this.quotationType = quotationType;
	}


	public void setQuotationItems(List<QuotationItem> quotationItems) {
		this.quotationItems = quotationItems;
	}



	@Column(name="inclusive_tax")
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}


	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	@Column(name="address")
	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name="gst_number")
	public String getGstNumber() {
		return gstNumber;
	}


	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

//...................
	
	
	@Column(name="company_name")
	public String getCompanyName() {
		return companyName;
	}



	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	@Column(name="comp_gst_registration_type_id")
	public Long getCompanygGstRegistrationTypeId() {
		return companygGstRegistrationTypeId;
	}



	public void setCompanygGstRegistrationTypeId(Long companygGstRegistrationTypeId) {
		this.companygGstRegistrationTypeId = companygGstRegistrationTypeId;
	}


	@Column(name="comp_pincode")
	public String getCompanyPinCode() {
		return companyPinCode;
	}



	public void setCompanyPinCode(String companyPinCode) {
		this.companyPinCode = companyPinCode;
	}


	@Column(name="comp_state_id")
	public Long getCompanyStateId() {
		return companyStateId;
	}



	public void setCompanyStateId(Long companyStateId) {
		this.companyStateId = companyStateId;
	}

	@Column(name="comp_state_name")
	public String getCompanyStateName() {
		return companyStateName;
	}


	public void setCompanyStateName(String companyStateName) {
		this.companyStateName = companyStateName;
	}

	@Column(name="comp_country_id")
	public Long getCompanyCountryId() {
		return companyCountryId;
	}



	public void setCompanyCountryId(Long companyCountryId) {
		this.companyCountryId = companyCountryId;
	}


	@Column(name="comp_primary_mobile")
	public String getCompanyPrimaryMobile() {
		return companyPrimaryMobile;
	}



	public void setCompanyPrimaryMobile(String companyPrimaryMobile) {
		this.companyPrimaryMobile = companyPrimaryMobile;
	}


	@Column(name="comp_secondary_mobile")
	public String getCompanySecondaryMobile() {
		return companySecondaryMobile;
	}



	public void setCompanySecondaryMobile(String companySecondaryMobile) {
		this.companySecondaryMobile = companySecondaryMobile;
	}


	@Column(name="comp_contact_person_number")
	public String getCompanyContactPersonNumber() {
		return companyContactPersonNumber;
	}



	public void setCompanyContactPersonNumber(String companyContactPersonNumber) {
		this.companyContactPersonNumber = companyContactPersonNumber;
	}


	@Column(name="comp_contact_person_name")
	public String getCompanyContactPersonName() {
		return companyContactPersonName;
	}



	public void setCompanyContactPersonName(String companyContactPersonName) {
		this.companyContactPersonName = companyContactPersonName;
	}


	@Column(name="comp_primary_telephone")
	public String getCompanyPrimaryTelephone() {
		return companyPrimaryTelephone;
	}



	public void setCompanyPrimaryTelephone(String companyPrimaryTelephone) {
		this.companyPrimaryTelephone = companyPrimaryTelephone;
	}


	@Column(name="comp_secondary_telephone")
	public String getCompanySecondaryTelephone() {
		return companySecondaryTelephone;
	}



	public void setCompanySecondaryTelephone(String companySecondaryTelephone) {
		this.companySecondaryTelephone = companySecondaryTelephone;
	}


	@Column(name="comp_website")
	public String getCompanyWebsite() {
		return companyWebsite;
	}



	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}


	@Column(name="comp_email")
	public String getCompanyEmail() {
		return companyEmail;
	}



	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}


	@Column(name="comp_fax_number")
	public String getCompanyFaxNumber() {
		return companyFaxNumber;
	}



	public void setCompanyFaxNumber(String companyFaxNumber) {
		this.companyFaxNumber = companyFaxNumber;
	}


	@Column(name="comp_address")
	public String getCompanyAddress() {
		return companyAddress;
	}



	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	@Column(name="comp_tagline")
	public String getCompanyTagLine() {
		return companyTagLine;
	}

	public void setCompanyTagLine(String companyTagLine) {
		this.companyTagLine = companyTagLine;
	}




	@Column(name="comp_gst_number")
	public String getCompanyGstNumber() {
		return companyGstNumber;
	}



	public void setCompanyGstNumber(String companyGstNumber) {
		this.companyGstNumber = companyGstNumber;
	}




	@Column(name="comp_pan_number")
	public String getCompanyPanNumber() {
		return companyPanNumber;
	}

	public void setCompanyPanNumber(String companyPanNumber) {
		this.companyPanNumber = companyPanNumber;
	}

	@Column(name="comp_pan_date")
	public String getCompanyPanDate() {
		return companyPanDate;
	}



	public void setCompanyPanDate(String companyPanDate) {
		this.companyPanDate = companyPanDate;
	}


	@Column(name="comp_certificate_image_path")
	public String getCompanyCeritificateImagePath() {
		return companyCeritificateImagePath;
	}



	public void setCompanyCeritificateImagePath(String companyCeritificateImagePath) {
		this.companyCeritificateImagePath = companyCeritificateImagePath;
	}


	@Column(name="comp_logo_path")
	public String getCompanyLogoPath() {
		return companyLogoPath;
	}



	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}
	
	@Column(name="party_name")
	public String getPartyName() {
		return partyName;
	}



	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}


	@Column(name="party_contact_person_number")
	public String getPartyContactPersonNumber() {
		return partyContactPersonNumber;
	}



	public void setPartyContactPersonNumber(String partyContactPersonNumber) {
		this.partyContactPersonNumber = partyContactPersonNumber;
	}


	@Column(name="party_pin_code")
	public String getPartyPinCode() {
		return partyPinCode;
	}



	public void setPartyPinCode(String partyPinCode) {
		this.partyPinCode = partyPinCode;
	}


	@Column(name="party_area_id")
	public Long getPartyAreaId() {
		return partyAreaId;
	}



	public void setPartyAreaId(Long partyAreaId) {
		this.partyAreaId = partyAreaId;
	}


	@Column(name="party_city_id")
	public Long getPartyCityId() {
		return partyCityId;
	}



	public void setPartyCityId(Long partyCityId) {
		this.partyCityId = partyCityId;
	}


	@Column(name="party_state_id")
	public Long getPartyStateId() {
		return partyStateId;
	}



	public void setPartyStateId(Long partyStateId) {
		this.partyStateId = partyStateId;
	}


	@Column(name="party_country_id")
	public Long getPartyCountryId() {
		return partyCountryId;
	}



	public void setPartyCountryId(Long partyCountryId) {
		this.partyCountryId = partyCountryId;
	}


	@Column(name="party_primary_telephone")
	public String getPartyPrimaryTelephone() {
		return partyPrimaryTelephone;
	}



	public void setPartyPrimaryTelephone(String partyPrimaryTelephone) {
		this.partyPrimaryTelephone = partyPrimaryTelephone;
	}


	@Column(name="party_secondary_telephone")
	public String getPartySecondaryTelephone() {
		return partySecondaryTelephone;
	}


	public void setPartySecondaryTelephone(String partySecondaryTelephone) {
		this.partySecondaryTelephone = partySecondaryTelephone;
	}


	@Column(name="party_primary_mobile_number")
	public String getPartyPrimaryMobile() {
		return partyPrimaryMobile;
	}



	public void setPartyPrimaryMobile(String partyPrimaryMobile) {
		this.partyPrimaryMobile = partyPrimaryMobile;
	}


	@Column(name="party_secondary_mobile")
	public String getPartySecondaryMobile() {
		return partySecondaryMobile;
	}



	public void setPartySecondaryMobile(String partySecondaryMobile) {
		this.partySecondaryMobile = partySecondaryMobile;
	}


	@Column(name="party_email")
	public String getPartyEmail() {
		return partyEmail;
	}



	public void setPartyEmail(String partyEmail) {
		this.partyEmail = partyEmail;
	}


	@Column(name="party_website")
	public String getPartyWebsite() {
		return partyWebsite;
	}



	public void setPartyWebsite(String partyWebsite) {
		this.partyWebsite = partyWebsite;
	}


	@Column(name="party_contact_person_name")
	public String getPartyContactPersonName() {
		return partyContactPersonName;
	}



	public void setPartyContactPersonName(String partyContactPersonName) {
		this.partyContactPersonName = partyContactPersonName;
	}


//	@Column(name="party_bill_to_address")
//	public String getPartyBillToAddress() {
//		return partyBillToAddress;
//	}
//
//
//
//	public void setPartyBillToAddress(String partyBillToAddress) {
//		this.partyBillToAddress = partyBillToAddress;
//	}
//
//
//	@Column(name="party_ship_to_address")
//	public String getPartyShipAddress() {
//		return partyShipAddress;
//	}
//
//
//
//	public void setPartyShipAddress(String partyShipAddress) {
//		this.partyShipAddress = partyShipAddress;
//	}


	@Column(name="party_due_days_limit")
	public String getPartyDueDaysLimit() {
		return partyDueDaysLimit;
	}



	public void setPartyDueDaysLimit(String partyDueDaysLimit) {
		this.partyDueDaysLimit = partyDueDaysLimit;
	}


	@Column(name="party_gst_registration_type_id")
	public String getPartyGstRegistrationTypeId() {
		return partyGstRegistrationTypeId;
	}



	public void setPartyGstRegistrationTypeId(String partyGstRegistrationTypeId) {
		this.partyGstRegistrationTypeId = partyGstRegistrationTypeId;
	}


//	@Column(name="party_gst_number")
//	public String getPartyGstNumber() {
//		return partyGstNumber;
//	}
//
//
//
//	public void setPartyGstNumber(String partyGstNumber) {
//		this.partyGstNumber = partyGstNumber;
//	}


	@Column(name="party_pan_number")
	public String getPartyPanNumber() {
		return partyPanNumber;
	}



	public void setPartyPanNumber(String partyPanNumber) {
		this.partyPanNumber = partyPanNumber;
	}


	@Column(name="is_igst")
	public String getIsIgst() {
		return isIgst;
	}



	public void setIsIgst(String isIgst) {
		this.isIgst = isIgst;
	}

	@Column(name="party_currency_id")
	public Long getPartyCurrencyId() {
		return partyCurrencyId;
	}

	public void setPartyCurrencyId(Long partyCurrencyId) {
		this.partyCurrencyId = partyCurrencyId;
	}
	@Column(name="party_currency_name")

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	@Override
	public String toString() {
		return "QuotationHeader{" +
				"quotationNumber='" + quotationNumber + '\'' +
				", quotationId=" + quotationId +
				", quotationDate=" + quotationDate +
				", enquiryNumber='" + enquiryNumber + '\'' +
				", enquiryDate=" + enquiryDate +
				", party=" + party +
				", subTotalAmount=" + subTotalAmount +
				", discountAmount=" + discountAmount +
				", discountPercent=" + discountPercent +
				", netAmount=" + netAmount +
				", tax=" + tax +
				", roundOffAmount=" + roundOffAmount +
				", grandTotal=" + grandTotal +
				", status=" + status +
				", financialYear=" + financialYear +
				", company=" + company +
				", paymentTerms='" + paymentTerms + '\'' +
				", deliveryTerms='" + deliveryTerms + '\'' +
				", termsAndConditions='" + termsAndConditions + '\'' +
				", taxAmount=" + taxAmount +
				", kindAttention='" + kindAttention + '\'' +
				", quotationSubject='" + quotationSubject + '\'' +
				", isPaymentChecked='" + isPaymentChecked + '\'' +
				", sgstTaxRate=" + sgstTaxRate +
				", sgstTaxAmount=" + sgstTaxAmount +
				", cgstTaxRate=" + cgstTaxRate +
				", cgstTaxAmount=" + cgstTaxAmount +
				", igstTaxRate=" + igstTaxRate +
				", igstTaxAmount=" + igstTaxAmount +
				", amount=" + amount +
				", quotationType=" + quotationType +
				", remarks='" + remarks + '\'' +
				", quotationItems=" + quotationItems +
				", inclusiveTax=" + inclusiveTax +
				", address='" + address + '\'' +
				", gstNumber='" + gstNumber + '\'' +
				", companyName='" + companyName + '\'' +
				", companygGstRegistrationTypeId=" + companygGstRegistrationTypeId +
				", companyPinCode='" + companyPinCode + '\'' +
				", companyStateId=" + companyStateId +
				", companyStateName='" + companyStateName + '\'' +
				", companyCountryId=" + companyCountryId +
				", companyPrimaryMobile='" + companyPrimaryMobile + '\'' +
				", companySecondaryMobile='" + companySecondaryMobile + '\'' +
				", companyContactPersonNumber='" + companyContactPersonNumber + '\'' +
				", companyContactPersonName='" + companyContactPersonName + '\'' +
				", companyPrimaryTelephone='" + companyPrimaryTelephone + '\'' +
				", companySecondaryTelephone='" + companySecondaryTelephone + '\'' +
				", companyWebsite='" + companyWebsite + '\'' +
				", companyEmail='" + companyEmail + '\'' +
				", companyFaxNumber='" + companyFaxNumber + '\'' +
				", companyAddress='" + companyAddress + '\'' +
				", companyTagLine='" + companyTagLine + '\'' +
				", companyGstNumber='" + companyGstNumber + '\'' +
				", companyPanNumber='" + companyPanNumber + '\'' +
				", companyPanDate='" + companyPanDate + '\'' +
				", companyCeritificateImagePath='" + companyCeritificateImagePath + '\'' +
				", companyLogoPath='" + companyLogoPath + '\'' +
				", partyName='" + partyName + '\'' +
				", partyContactPersonNumber='" + partyContactPersonNumber + '\'' +
				", partyPinCode='" + partyPinCode + '\'' +
				", partyAreaId=" + partyAreaId +
				", partyCityId=" + partyCityId +
				", partyStateId=" + partyStateId +
				", partyCountryId=" + partyCountryId +
				", partyCurrencyId=" + partyCurrencyId +
				", partyPrimaryTelephone='" + partyPrimaryTelephone + '\'' +
				", partySecondaryTelephone='" + partySecondaryTelephone + '\'' +
				", partyPrimaryMobile='" + partyPrimaryMobile + '\'' +
				", partySecondaryMobile='" + partySecondaryMobile + '\'' +
				", partyEmail='" + partyEmail + '\'' +
				", partyWebsite='" + partyWebsite + '\'' +
				", partyContactPersonName='" + partyContactPersonName + '\'' +
				", partyDueDaysLimit='" + partyDueDaysLimit + '\'' +
				", partyGstRegistrationTypeId='" + partyGstRegistrationTypeId + '\'' +
				", partyPanNumber='" + partyPanNumber + '\'' +
				", isIgst='" + isIgst + '\'' +
				", currencyName='" + currencyName + '\'' +
				'}';
	}
}
