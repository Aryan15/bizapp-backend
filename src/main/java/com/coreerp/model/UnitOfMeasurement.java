package com.coreerp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="ma_uom")
@Where(clause = "deleted='N'")
public class UnitOfMeasurement extends AuditModelAbstractEntity {

	private String deleted="N";
	private String name;
	private List<Material> materials;
	
	@Column(name="deleted")
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@OneToMany(mappedBy="unitOfMeasurement")
	@JsonIgnore
	public List<Material> getMaterials() {
		return materials;
	}
	public void setMaterials(List<Material> materials) {
		this.materials = materials;
	}
	
	
}
