package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "cd_about_company")
public class AboutCompany extends AuditModelAbstractEntity {

	private String productName;

	private String productVersion;

	private String companyName;

	private String companyEmail;

	private String companyWebsite;

	private String companyContactNumber;

	@Column(name = "product_name")
	public String getProductName() {
		return productName;
	}

	public void setProductName(String producttName) {
		this.productName = producttName;
	}

	@Column(name = "product_version")
	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	@Column(name = "company_name")
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(name = "company_email")
	public String getCompanyEmail() {
		return companyEmail;
	}

	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}

	@Column(name = "company_website")
	public String getCompanyWebsite() {
		return companyWebsite;
	}

	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}

	@Column(name="company_contact_number")
	public String getCompanyContactNumber() {
		return companyContactNumber;
	}

	public void setCompanyContactNumber(String companyContactNumber) {
		this.companyContactNumber = companyContactNumber;
	}

	@Override
	public String toString() {
		return "AboutCompany [producttName=" + productName + ", productVersion=" + productVersion + ", companyName="
				+ companyName + ", companyEmail=" + companyEmail + ", companyWebsite=" + companyWebsite
				+ ", companyContactNumber=" + companyContactNumber + "]";
	}



}
