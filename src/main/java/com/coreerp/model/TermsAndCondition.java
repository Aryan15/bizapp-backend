package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

@Entity
@Table(name="conf_terms_condition")
@Where(clause="deleted='N'")
public class TermsAndCondition extends AuditModelAbstractEntity {

	private String name;
	private String termsAndCondition;
	private String paymentTerms;
	private String deliveryTerms;
	private Integer defaultTermsAndCondition;
	private TransactionType transactionType;
	private String deleted = "N";
	
	@Column(name="name")
//	@NotNull
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="terms_and_condition", length = 2000)
	public String getTermsAndCondition() {
		return termsAndCondition;
	}
	public void setTermsAndCondition(String termsAndCondition) {
		this.termsAndCondition = termsAndCondition;
	}
	@Column(name="payment_terms", length = 2000)
	public String getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	@Column(name="delivery_terms", length = 2000)
	public String getDeliveryTerms() {
		return deliveryTerms;
	}
	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}
	@Column(name="is_default")
	public Integer getDefaultTermsAndCondition() {
		return defaultTermsAndCondition;
	}
	public void setDefaultTermsAndCondition(Integer defaultTermsAndCondition) {
		this.defaultTermsAndCondition = defaultTermsAndCondition;
	}
	
	@ManyToOne
	@JoinColumn(name="fk_transaction_type_id",foreignKey=@ForeignKey(name="FK_TRN_TYP") )
	public TransactionType getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
	
	@Column(name="deleted")
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}
	
	
}
