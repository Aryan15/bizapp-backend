package com.coreerp.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

@Entity
@Table(name="conf_global_master_settings")
@Where(clause = "deleted='N'")
public class GlobalSetting extends AuditModelAbstractEntity {

	private String deleted="N";
	//private Long financialYearId;
	private FinancialYear financialYear;
	/*private Integer increaseStockOn;
	private Integer decreaseStockOn;*/
//	private Integer customerAutoNumber;
//	private Integer supAutoNumber;
//	private Integer autoNumberReset;
	private StockUpdateOn stockUpdateIncrease;
	private StockUpdateOn stockUpdateDecrease;
	private Integer itemLevelTax;
	private Integer itemLevelDiscount;
	private String browserPath;
	private Integer salePrice;
	private Integer purchasePrice;
	private Integer specialPriceCalculation;
	private Integer inclusiveTax;
	private Long barCodeStatus;
	private Long printStatus;
	private Long includeSignatureStatus;
	private String helpDocument;
	/*private Long gstStatus;
	private Date gstApplicableDate;*/
	private Double sgstPercentage;
	private Double cgstPercentage;
	private Integer disableItemGroup;
	private Integer customerMaterialBinding;
	private Integer showOnlyCustomerMaterialInSales;
	private Integer roundOffTaxTransaction;
	private Integer stockCheckRequired;
	private Integer itemLevelTaxPurchase;
	private Integer enableTally;
	private Integer allowReuse;
	private Integer allowPartyCode;
	private Integer allowEwayBill;


	@Column(name="deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}

	@Column(name="is_item_level_tax")
	public Integer getItemLevelTax() {
		return itemLevelTax;
	}
	
	
	public void setItemLevelTax(Integer itemLevelTax) {
		this.itemLevelTax = itemLevelTax;
	}
	
	@Column(name="is_item_level_disc")
	public Integer getItemLevelDiscount() {
		return itemLevelDiscount;
	}
	public void setItemLevelDiscount(Integer itemLevelDiscount) {
		this.itemLevelDiscount = itemLevelDiscount;
	}
	
	@Column(name="browser_path")
	public String getBrowserPath() {
		return browserPath;
	}
	public void setBrowserPath(String browserPath) {
		this.browserPath = browserPath;
	}
	
	@Column(name="sale_price")
	public Integer getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(Integer salePrice) {
		this.salePrice = salePrice;
	}
	
	@Column(name="purchase_price")
	public Integer getPurchasePrice() {
		return purchasePrice;
	}
	public void setPurchasePrice(Integer purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	
	@Column(name="is_special_price_cal")
	public Integer getSpecialPriceCalculation() {
		return specialPriceCalculation;
	}
	public void setSpecialPriceCalculation(Integer specialPriceCalculation) {
		this.specialPriceCalculation = specialPriceCalculation;
	}
	
	@Column(name="is_tax_inclusive")
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}
	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}
	
	@Column(name="bar_code_status")
	public Long getBarCodeStatus() {
		return barCodeStatus;
	}
	public void setBarCodeStatus(Long barCodeStatus) {
		this.barCodeStatus = barCodeStatus;
	}
	
	@Column(name="print_status")
	public Long getPrintStatus() {
		return printStatus;
	}
	public void setPrintStatus(Long printStatus) {
		this.printStatus = printStatus;
	}
	
	@Column(name="include_signature_status")
	public Long getIncludeSignatureStatus() {
		return includeSignatureStatus;
	}
	public void setIncludeSignatureStatus(Long includeSignatureStatus) {
		this.includeSignatureStatus = includeSignatureStatus;
	}
	
	@Column(name="help_doc")
	public String getHelpDocument() {
		return helpDocument;
	}
	public void setHelpDocument(String helpDocument) {
		this.helpDocument = helpDocument;
	}
	
	
@Column(name="sgst_percent")
	public Double getSgstPercentage() {
		return sgstPercentage;
	}
	public void setSgstPercentage(Double sgstPercentage) {
		this.sgstPercentage = sgstPercentage;
	}
	@Column(name="cgst_percent")	
	public Double getCgstPercentage() {
		return cgstPercentage;
	}
	public void setCgstPercentage(Double cgstPercentage) {
		this.cgstPercentage = cgstPercentage;
	}
	
	@Column(name="is_disable_item_grp")
	public Integer getDisableItemGroup() {
		return disableItemGroup;
	}
	public void setDisableItemGroup(Integer disableItemGroup) {
		this.disableItemGroup = disableItemGroup;
	}
	
	@ManyToOne
	@JoinColumn(name="financial_year_id", foreignKey=@ForeignKey(name="FK_GSTG_FY"))
	public FinancialYear getFinancialYear() {
		return financialYear;
	}
	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}
	
	@Column(name="is_cust_mat_binding")
	public Integer getCustomerMaterialBinding() {
		return customerMaterialBinding;
	}
	public void setCustomerMaterialBinding(Integer customerMaterialBinding) {
		this.customerMaterialBinding = customerMaterialBinding;
	}
	
	@Column(name="is_show_cust_mat_in_sales")
	public Integer getShowOnlyCustomerMaterialInSales() {
		return showOnlyCustomerMaterialInSales;
	}
	public void setShowOnlyCustomerMaterialInSales(Integer showOnlyCustomerMaterialInSales) {
		this.showOnlyCustomerMaterialInSales = showOnlyCustomerMaterialInSales;
	}
	
	@ManyToOne
	@JoinColumn(name="increase_stock_on_id", foreignKey=@ForeignKey(name="FK_GSTG_STOI"))
	public StockUpdateOn getStockUpdateIncrease() {
		return stockUpdateIncrease;
	}
	public void setStockUpdateIncrease(StockUpdateOn stockUpdateIncrease) {
		this.stockUpdateIncrease = stockUpdateIncrease;
	}
	
	@ManyToOne
	@JoinColumn(name="decrease_stock_on_id", foreignKey=@ForeignKey(name="FK_GSTG_STOD"))
	public StockUpdateOn getStockUpdateDecrease() {
		return stockUpdateDecrease;
	}
	public void setStockUpdateDecrease(StockUpdateOn stockUpdateDecrease) {
		this.stockUpdateDecrease = stockUpdateDecrease;
	}
	
	@Column(name="roundoff_tax_transaction")
	public Integer getRoundOffTaxTransaction() {
		return roundOffTaxTransaction;
	}
	public void setRoundOffTaxTransaction(Integer roundOffTaxTransaction) {
		this.roundOffTaxTransaction = roundOffTaxTransaction;
	}
	
	
	@Column(name="stock_check_required")
	public Integer getStockCheckRequired() {
		return stockCheckRequired;
	}
	public void setStockCheckRequired(Integer stockCheckRequired) {
		this.stockCheckRequired = stockCheckRequired;
	}

	@Override
	public String toString() {
		return "GlobalSetting{" +
				"deleted='" + deleted + '\'' +
				", financialYear=" + financialYear +
				", stockUpdateIncrease=" + stockUpdateIncrease +
				", stockUpdateDecrease=" + stockUpdateDecrease +
				", itemLevelTax=" + itemLevelTax +
				", itemLevelDiscount=" + itemLevelDiscount +
				", browserPath='" + browserPath + '\'' +
				", salePrice=" + salePrice +
				", purchasePrice=" + purchasePrice +
				", specialPriceCalculation=" + specialPriceCalculation +
				", inclusiveTax=" + inclusiveTax +
				", barCodeStatus=" + barCodeStatus +
				", printStatus=" + printStatus +
				", includeSignatureStatus=" + includeSignatureStatus +
				", helpDocument='" + helpDocument + '\'' +
				", sgstPercentage=" + sgstPercentage +
				", cgstPercentage=" + cgstPercentage +
				", disableItemGroup=" + disableItemGroup +
				", customerMaterialBinding=" + customerMaterialBinding +
				", showOnlyCustomerMaterialInSales=" + showOnlyCustomerMaterialInSales +
				", roundOffTaxTransaction=" + roundOffTaxTransaction +
				", stockCheckRequired=" + stockCheckRequired +
				", itemLevelTaxPurchase=" + itemLevelTaxPurchase +
				", enableTally=" + enableTally +
				", allowReuse=" + allowReuse +
				", allowPartyCode=" + allowPartyCode +
				", allowEwayBill=" + allowEwayBill +
				'}';
	}

	@Column(name="is_item_level_tax_purchase")
	public Integer getItemLevelTaxPurchase() {
		return itemLevelTaxPurchase;
	}

	public void setItemLevelTaxPurchase(Integer itemLevelTaxPurchase) {
		this.itemLevelTaxPurchase = itemLevelTaxPurchase;
	}

	@Column(name="enable_tally")
	public Integer getEnableTally() {
		return enableTally;
	}

	public void setEnableTally(Integer enableTally) {
		this.enableTally = enableTally;
	}

	@Column(name="allow_reuse")
	public Integer getAllowReuse() {
		return allowReuse;
	}

	public void setAllowReuse(Integer allowReuse) {
		this.allowReuse = allowReuse;
	}


	@Column(name="allow_party_code")
	public Integer getAllowPartyCode() {
		return allowPartyCode;
	}

	public void setAllowPartyCode(Integer allowPartyCode) {
		this.allowPartyCode = allowPartyCode;
	}

	@Column(name="allow_eway_bill")
	public Integer getAllowEwayBill() {
		return allowEwayBill;
	}

	public void setAllowEwayBill(Integer allowEwayBill) {
		this.allowEwayBill = allowEwayBill;
	}
}
