package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="fw_activity_role_binding")
@Where(clause = "deleted='N'")
public class ActivityRoleBinding extends AuditModelAbstractEntity{

	
	private Activity activity;
	
	private Role role;
	
	private User user;
	
	private String deleted;
	
	private Company company;
	
	private Integer permissionToCreate;	
	
	private Integer permissionToUpdate;
	
	private Integer permissionToDelete;
	
	private Integer permissionToCancel;
	
	private Integer permissionToPrint;
	
	private Integer permissionToApprove;

	@ManyToOne
	@JoinColumn(name="activity_id",  foreignKey=@ForeignKey(name="FK_ARB_ACT"))
	@JsonIgnore
	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	@ManyToOne
	@JoinColumn(name="role_id",  foreignKey=@ForeignKey(name="FK_ARB_ROLE"))
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	@ManyToOne
	@JoinColumn(name="user_id",  foreignKey=@ForeignKey(name="FK_ARB_USER"))
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@Column(name="deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}

	@ManyToOne
	@JoinColumn(name="company_id",  foreignKey=@ForeignKey(name="FK_ARB_COMP"))
	@JsonIgnore
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Column(name="permission_to_create")
	public Integer getPermissionToCreate() {
		return permissionToCreate;
	}

	public void setPermissionToCreate(Integer permissionToCreate) {
		this.permissionToCreate = permissionToCreate;
	}

	@Column(name="permission_to_update")
	public Integer getPermissionToUpdate() {
		return permissionToUpdate;
	}

	public void setPermissionToUpdate(Integer permissionToUpdate) {
		this.permissionToUpdate = permissionToUpdate;
	}

	@Column(name="permission_to_delete")
	public Integer getPermissionToDelete() {
		return permissionToDelete;
	}
	
	
	@Column(name="permission_to_cancel")
	public Integer getPermissionToCancel() {
		return permissionToCancel;
	}

	public void setPermissionToCancel(Integer permissionToCancel) {
		this.permissionToCancel = permissionToCancel;
	}

	public void setPermissionToDelete(Integer permissionToDelete) {
		this.permissionToDelete = permissionToDelete;
	}
	
	@Column(name="permission_to_print")
	public Integer getPermissionToPrint() {
		return permissionToPrint;
	}

	public void setPermissionToPrint(Integer permissionToPrint) {
		this.permissionToPrint = permissionToPrint;
	}

	@Column(name="permission_to_approve")
	public Integer getPermissionToApprove() {
		return permissionToApprove;
	}

	public void setPermissionToApprove(Integer permissionToApprove) {
		this.permissionToApprove = permissionToApprove;
	}

	@Override
	public String toString() {
		return "ActivityRoleBinding "
				+ ", deleted="+deleted + ", company=" + company + ", permissionToCreate=" + permissionToCreate
				+ ", permissionToUpdate=" + permissionToUpdate + ", permissionToDelete=" + permissionToDelete
				+ ", permissionToCancel=" + permissionToCancel + ", permissionToPrint=" + permissionToPrint
				+ ", permissionToApprove=" + permissionToApprove + "]";
	}
	
	
}
