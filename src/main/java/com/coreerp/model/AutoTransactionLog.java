package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;



@Entity
@Table(name="tr_auto_transaction_tracker")
public class AutoTransactionLog extends AuditModelAbstractEntity{

    private String invoiceHeaderId;

    private Date transactionGenratedDate;

    private  String generatedInvoiceId;

    private Integer  transactionCount;

    @Column(name="source_invoice_id")
    public String getInvoiceHeaderId() {
        return invoiceHeaderId;
    }

    public void setInvoiceHeaderId(String invoiceHeaderId) {
        this.invoiceHeaderId = invoiceHeaderId;
    }

    @Column(name="transaction_generated_date")
    public Date getTransactionGenratedDate() {
        return transactionGenratedDate;
    }

    public void setTransactionGenratedDate(Date transactionGenratedDate) {
        this.transactionGenratedDate = transactionGenratedDate;
    }

    @Column(name="generated_invoice_id")
    public String getGeneratedInvoiceId() {
        return generatedInvoiceId;
    }

    public void setGeneratedInvoiceId(String generatedInvoiceId) {
        this.generatedInvoiceId = generatedInvoiceId;
    }

    @Column(name="transaction_count")

    public Integer getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(Integer transactionCount) {
        this.transactionCount = transactionCount;
    }
}
