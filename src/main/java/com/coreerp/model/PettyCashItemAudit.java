package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name="tr_pettycash_item_audit")
public class PettyCashItemAudit {

    private String id;

    private Date createdDateTime;

    private String createdBy;

    private Date updatedDateTime;

    private String updatedBy;

    private  String pettyCashHeader;
    private Long categoryId;
    private Long paymentMethodId;
    private String remarks;
    private Long tagId;
    private  Double incomingAmount;
    private Double outgoingAmount;



    @Id
    @Column(name="id", length=255)
    @Size(min=1)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name="petty_cash_header_id")
    public String getPettyCashHeader() {
        return pettyCashHeader;
    }

    public void setPettyCashHeader(String pettyCashHeader) {
        this.pettyCashHeader = pettyCashHeader;
    }

    @Column(name="category_id")
    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @Column(name="paymment_id")
    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    @Column(name="remarks")
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(name="tag_id")
    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    @Column(name="incoming_amount")
    public Double getIncomingAmount() {
        return incomingAmount;
    }

    public void setIncomingAmount(Double incomingAmount) {
        this.incomingAmount = incomingAmount;
    }

    @Column(name="outgoing_amount")
    public Double getOutgoingAmount() {
        return outgoingAmount;
    }

    public void setOutgoingAmount(Double outgoingAmount) {
        this.outgoingAmount = outgoingAmount;
    }

    @Column(name="created_date_time",nullable = false, updatable = false)
    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }
    @Column(name="created_by",nullable = false, updatable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    @Column(name="updated_date_time")
    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }
    @Column(name="updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
