package com.coreerp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.coreerp.utils.TransactionDataException;



@Entity
@Table(name="tr_invoice_header")
@SecondaryTable(name="tr_invoice_details")
public class InvoiceHeader extends AbstractTransactionModel{


	private Date invoiceDate;
    private String invoiceNumber ;
    private Long invId;
    private Date purchaseOrderDate ;
    private Date internalReferenceDate;
    private String internalReferenceNumber;
    private Party party ;
    private Double subTotal;  
    private Double discountAmount;
    private Double discountPercent;
    private Double netAmount ;
    private Tax tax;
    private Double roundOffAmount ;
    private Double grandTotal;
    private FinancialYear financialYear;
    private Company company;
    private TransactionType invoiceType;
    private String paymentTerms;
    private String deliveryTerms;
    private String termsAndConditions;
    private String modeOfDispatch ;
    private String vehicleNumber ;
    private String numOfPackages;
    private String documentThrough ;
    private Double taxAmount;
    private String purchaseOrderNumber ;
    private Status status;
    private Double advanceAmount;
    private Date paymentDueDate ;
    private Double dueAmount;
    private String partyAddress ;
    private String deliveryChallanNumber ;
    private Date deliveryChallanDate ;
    private String grnNumber ;
    private Date grnDate ;
    private String proformaInvoiceNumber;
    private Date proformaInvoiceDate;
    private String sourceInvoiceNumber;
    private Date sourceInvoiceDate;    
    private Long labourChargesOnly  ;
    private String billToAddress ;
    private String shipToAddress ;
    private String notForSale;
    private String timeOfInvoice;
    private String vendorCode;
    private Double transportationCharges;
    private Double cessAmount;
    private Double cessPercent;
    private Double sgstTaxRate;
    private Double sgstTaxAmount;
    private Double cgstTaxRate;
    private Double cgstTaxAmount;
    private Double igstTaxRate;
    private Double igstTaxAmount;
    private String eWayBillNumber ; 
    private Double totalTaxableAmount ;
    private Long isReverseCharge;
    private String remarks;

	private List<InvoiceItem> invoiceItems;
	private Integer inclusiveTax;
	private String deliveryChallanDateString;
	
	private String gstNumber;
	
    private String companyName; 
    private Long companygGstRegistrationTypeId; //companyGstRegistrationnTypeId
    private String companyPinCode; //companyPinCode .. etc
    private Long companyStateId;
    private String companyStateName;
    private Long companyCountryId;
    private String companyPrimaryMobile;
    private String companySecondaryMobile;
    private String companyContactPersonNumber;
    private String companyContactPersonName;
    private String companyPrimaryTelephone;
    private String companySecondaryTelephone;
    private String companyWebsite;
    private String companyEmail;
    private String companyFaxNumber;
    private String companyAddress;
    private String companyTagLine;
    private String companyGstNumber;
    private String companyPanNumber;
    private String companyPanDate;
    private String companyCeritificateImagePath;
    private String companyLogoPath;
    
    private String partyName;
    private String partyContactPersonNumber;
    private String partyPinCode;
    private Long partyAreaId;
    private Long partyCityId;
    private Long partyStateId;
    private Long partyCountryId;
    private String partyPrimaryTelephone;
    private String partySecondaryTelephone;
    private String partyPrimaryMobile;
    private String partySecondaryMobile;
    private String partyEmail;
    private String partyWebsite;
    private String partyContactPersonName;
//    private String partyBillToAddress;
//    private String partyShipAddress;
    private String partyDueDaysLimit;
    private String partyGstRegistrationTypeId;
//    private String partyGstNumber;
    private String partyPanNumber;
    private String isIgst;

    private Integer isReused;

	private Double tcsPercentage;
	private Double tcsAmount;
	private Long materialNoteType;
	private Long currencyId;
	private Double currencyRate;
	private Double totalAmountCurrency;
	private String currencyName;



	private String finalDestination;
	private String vesselNumber;
	private String shipper;
	private String cityOfLoading;
	private String cityOfDischarge;
	private String countryOfOriginOfGoods;
	private String countryOfDestination;



	@Column(name ="remarks")
    public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}



	@Column(name="inv_date")
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	@Column(name="inv_number")
	public String getInvoiceNumber() {
		return invoiceNumber;
	}


	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	


	@Column(name="inv_id")
	@NotNull
	public Long getInvId() {
		return invId;
	}


	public void setInvId(Long invId) {
		this.invId = invId;
	}


	@Column(name="payment_due_date")
	public Date getPaymentDueDate() {
		return paymentDueDate;
	}


	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	
	
	@Column(name="due_amount")
	public Double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(Double dueAmount) throws TransactionDataException {
		
		// if(this.grandTotal != null && dueAmount > this.grandTotal){
		// 	throw new TransactionDataException("Due Amount being set greater than grand total", null);
		// }
		// else{
		// 	this.dueAmount = dueAmount;
		// }

		this.dueAmount = dueAmount;
	}

	@Column(name="e_way_bill_number")
	public String geteWayBillNumber() {
		return eWayBillNumber;
	}


	public void seteWayBillNumber(String eWayBillNumber) {
		this.eWayBillNumber = eWayBillNumber;
	}


	@Column(name="total_taxable_amount")
	public Double getTotalTaxableAmount() {
		return totalTaxableAmount;
	}


	public void setTotalTaxableAmount(Double totalTaxableAmount) {
		this.totalTaxableAmount = totalTaxableAmount;
	}



	@Column(name="grand_total")
	public Double getGrandTotal() {
		return grandTotal;
	}


	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	@Column(name="bill_to_address")
	public String getBillToAddress() {
		return billToAddress;
	}


	public void setBillToAddress(String billToAddress) {
		this.billToAddress = billToAddress;
	}

	@Column(name="labour_charges_only")
	public Long getLabourChargesOnly() {
		return labourChargesOnly;
	}


	public void setLabourChargesOnly(Long labourChargesOnly) {
		this.labourChargesOnly = labourChargesOnly;
	}

	@Column(name="sub_total")
	public Double getSubTotal() {
		return subTotal;
	}


	public void setSubTotal(Double subTotal ){
		this.subTotal = subTotal;
	}

	@Column(name="payment_terms", length=500)
	public String getPaymentTerms() {
		return paymentTerms;
	}


	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	@Column(name="delivery_terms", length=500)
	public String getDeliveryTerms() {
		return deliveryTerms;
	}


	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}

	@Column(name="terms_conditions", length =2500)
	public String getTermsAndConditions() {
		return termsAndConditions;
	}


	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	@Column(name="mode_of_dispatch")
	public String getModeOfDispatch() {
		return modeOfDispatch;
	}


	public void setModeOfDispatch(String modeOfDispatch) {
		this.modeOfDispatch = modeOfDispatch;
	}

	@Column(name="vehicle_number")
	public String getVehicleNumber() {
		return vehicleNumber;
	}


	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	
	@Column(name="number_of_packages")
	public String getNumOfPackages() {
		return numOfPackages;
	}


	public void setNumOfPackages(String numOfPackages) {
		this.numOfPackages = numOfPackages;
	}

	@Column(name="doc_through", length=2500)
	public String getDocumentThrough() {
		return documentThrough;
	}


	public void setDocumentThrough(String documentThrough) {
		this.documentThrough = documentThrough;
	}

	@Column(name="ship_address", length=1500)
	public String getShipToAddress() {
		return shipToAddress;
	}


	public void setShipToAddress(String shipToAddress) {
		this.shipToAddress = shipToAddress;
	}



	@Column(name="net_amount")
	public Double getNetAmount() {
		return netAmount;
	}


	public void setNetAmount(Double netAmount) {
		this.netAmount = netAmount;
	}

	@Column(name="transportation_amt")
	public Double getTransportationCharges() {
		return transportationCharges;
	}


	public void setTransportationCharges(Double transportationCharges) {
		this.transportationCharges = transportationCharges;
	}

	@Column(name="advance_amount")
	public Double getAdvanceAmount() {
		return advanceAmount;
	}


	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

	@Column(name="round_off_amount")
	public Double getRoundOffAmount() {
		return roundOffAmount;
	}


	public void setRoundOffAmount(Double roundOffAmount) {
		this.roundOffAmount = roundOffAmount;
	}

	@OneToMany(mappedBy = "invoiceHeader", cascade = CascadeType.ALL)
	@NotNull
	@OrderBy("slNo ASC")
	public List<InvoiceItem> getInvoiceItems() {
		return invoiceItems;
	}


	public void setInvoiceItems(List<InvoiceItem> invoiceItems) {
		this.invoiceItems = invoiceItems;
	}


	@Column(name="po_date")
	public Date getPurchaseOrderDate() {
		return purchaseOrderDate;
	}


	public void setPurchaseOrderDate(Date purchaseOrderDate) {
		this.purchaseOrderDate = purchaseOrderDate;
	}

	@Column(name="po_number")
	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}


	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	
	@Column(name="proforma_invoice_number")
	public String getProformaInvoiceNumber() {
		return proformaInvoiceNumber;
	}

	public void setProformaInvoiceNumber(String proformaInvoiceNumber) {
		this.proformaInvoiceNumber = proformaInvoiceNumber;
	}

	@Column(name="proforma_invoice_date")
	public Date getProformaInvoiceDate() {
		return proformaInvoiceDate;
	}

	public void setProformaInvoiceDate(Date proformaInvoiceDate) {
		this.proformaInvoiceDate = proformaInvoiceDate;
	}

	@Column(name="dc_date")
	public Date getDeliveryChallanDate() {
		return deliveryChallanDate;
	}


	public void setDeliveryChallanDate(Date deliveryChallanDate) {
		this.deliveryChallanDate = deliveryChallanDate;
	}

	
	@Column(name="dc_number")
	public String getDeliveryChallanNumber() {
		return deliveryChallanNumber;
	}


	public void setDeliveryChallanNumber(String deliveryChallanNumber) {
		this.deliveryChallanNumber = deliveryChallanNumber;
	}

	@Column(name="int_ref_date")
	public Date getInternalReferenceDate() {
		return internalReferenceDate;
	}


	public void setInternalReferenceDate(Date internalReferenceDate) {
		this.internalReferenceDate = internalReferenceDate;
	}

	@Column(name="int_ref_number")
	public String getInternalReferenceNumber() {
		return internalReferenceNumber;
	}


	public void setInternalReferenceNumber(String internalReferenceNumber) {
		this.internalReferenceNumber = internalReferenceNumber;
	}

	@ManyToOne
	@JoinColumn(name="party_id", foreignKey=@ForeignKey(name="FK_INVH_PARTY"))
	@NotNull
	public Party getParty() {
		return party;
	}


	public void setParty(Party party) {
		this.party = party;
	}

	@Column(name="discount_amount")
	public Double getDiscountAmount() {
		return discountAmount;
	}


	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	@Column(name="discount_percent")
	public Double getDiscountPercent() {
		return discountPercent;
	}


	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}


	@ManyToOne
	@JoinColumn(name="tax_id", foreignKey=@ForeignKey(name="FK_INVH_TAX"))
	//@NotNull
	public Tax getTax() {
		return tax;
	}


	public void setTax(Tax tax) {
		this.tax = tax;
	}


	@ManyToOne
	@JoinColumn(name="financial_year_id", foreignKey=@ForeignKey(name="FK_INVH_FY"))
	@NotNull
	public FinancialYear getFinancialYear() {
		return financialYear;
	}


	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	@ManyToOne
	@JoinColumn(name="company_id", foreignKey=@ForeignKey(name="FK_INVH_COMPANY"))
	@NotNull
	public Company getCompany() {
		return company;
	}


	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne
	@JoinColumn(name="invoice_type", foreignKey=@ForeignKey(name="FK_INVH_TT"))
	@NotNull
	public TransactionType getInvoiceType() {
		return invoiceType;
	}


	public void setInvoiceType(TransactionType invoiceType) {
		this.invoiceType = invoiceType;
	}



	@Column(name="tax_amount")	
	public Double getTaxAmount() {
		return taxAmount;
	}


	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	@ManyToOne
	@JoinColumn(name="status_id",foreignKey=@ForeignKey(name="FK_INVH_ST"))
	@NotNull
	public Status getStatus() {
		return status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


//	public void setDueAmount(Double dueAmount) {
//		this.dueAmount = dueAmount;
//	}


	@Column(name="party_address")
	public String getPartyAddress() {
		return partyAddress;
	}


	public void setPartyAddress(String partyAddress) {
		this.partyAddress = partyAddress;
	}


	@Column(name="not_for_sale")
	public String getNotForSale() {
		return notForSale;
	}


	public void setNotForSale(String notForSale) {
		this.notForSale = notForSale;
	}

	@Column(name="time_of_invoice")
	public String getTimeOfInvoice() {
		return timeOfInvoice;
	}


	public void setTimeOfInvoice(String timeOfInvoice) {
		this.timeOfInvoice = timeOfInvoice;
	}

	@Column(name="vendor_code")
	public String getVendorCode() {
		return vendorCode;
	}


	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	@Column(name="cess_amt")
	public Double getCessAmount() {
		return cessAmount;
	}


	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}

	@Column(name="cess_percent")
	public Double getCessPercent() {
		return cessPercent;
	}


	public void setCessPercent(Double cessPercent) {
		this.cessPercent = cessPercent;
	}

	@Column(name="sgst_tax_rate")
	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}


	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	@Column(name="sgst_tax_amount")
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}


	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}

	@Column(name="cgst_tax_rate")
	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}


	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}

	@Column(name="cgst_tax_amount")
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}


	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}

	@Column(name="igst_tax_rate")
	public Double getIgstTaxRate() {
		return igstTaxRate;
	}


	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}

	@Column(name="igst_tax_amount")
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}


	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	
	
	@Column(name="source_invoice_number")
	public String getSourceInvoiceNumber() {
		return sourceInvoiceNumber;
	}

	public void setSourceInvoiceNumber(String sourceInvoiceNumber) {
		this.sourceInvoiceNumber = sourceInvoiceNumber;
	}

	@Column(name="source_invoice_date")
	public Date getSourceInvoiceDate() {
		return sourceInvoiceDate;
	}

	public void setSourceInvoiceDate(Date sourceInvoiceDate) {
		this.sourceInvoiceDate = sourceInvoiceDate;
	}
	
	@Column(name="is_revserse_charge")
	public Long getIsReverseCharge() {
		return isReverseCharge;
	}

	public void setIsReverseCharge(Long isReverseCharge) {
		this.isReverseCharge = isReverseCharge;
	}
	@Column(name="grn_number")
	public String getGrnNumber() {
		return grnNumber;
	}

	public void setGrnNumber(String grnNumber) {
		this.grnNumber = grnNumber;
	}
	@Column(name="grn_date")
	public Date getGrnDate() {
		return grnDate;
	}

	public void setGrnDate(Date grnDate) {
		this.grnDate = grnDate;
	}

	@Column(name="inclusive_tax")
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}

	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	@Column(name="dc_date_str")
	public String getDeliveryChallanDateString() {
		return deliveryChallanDateString;
	}

	public void setDeliveryChallanDateString(String deliveryChallanDateString) {
		this.deliveryChallanDateString = deliveryChallanDateString;
	}

	@Column(name="gst_number")
	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}
	
	//...................
	
	
		@Column(name="company_name", table = "tr_invoice_details")
		public String getCompanyName() {
			return companyName;
		}



		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}
		
		@Column(name="comp_gst_registration_type_id", table = "tr_invoice_details")
		public Long getCompanygGstRegistrationTypeId() {
			return companygGstRegistrationTypeId;
		}



		public void setCompanygGstRegistrationTypeId(Long companygGstRegistrationTypeId) {
			this.companygGstRegistrationTypeId = companygGstRegistrationTypeId;
		}


		@Column(name="comp_pincode", table = "tr_invoice_details")
		public String getCompanyPinCode() {
			return companyPinCode;
		}



		public void setCompanyPinCode(String companyPinCode) {
			this.companyPinCode = companyPinCode;
		}


		@Column(name="comp_state_id", table = "tr_invoice_details")
		public Long getCompanyStateId() {
			return companyStateId;
		}



		public void setCompanyStateId(Long companyStateId) {
			this.companyStateId = companyStateId;
		}

		@Column(name="comp_state_name", table = "tr_invoice_details")
		public String getCompanyStateName() {
			return companyStateName;
		}

		public void setCompanyStateName(String companyStateName) {
			this.companyStateName = companyStateName;
		}

		@Column(name="comp_country_id", table = "tr_invoice_details")
		public Long getCompanyCountryId() {
			return companyCountryId;
		}



		public void setCompanyCountryId(Long companyCountryId) {
			this.companyCountryId = companyCountryId;
		}


		@Column(name="comp_primary_mobile", table = "tr_invoice_details")
		public String getCompanyPrimaryMobile() {
			return companyPrimaryMobile;
		}



		public void setCompanyPrimaryMobile(String companyPrimaryMobile) {
			this.companyPrimaryMobile = companyPrimaryMobile;
		}


		@Column(name="comp_secondary_mobile", table = "tr_invoice_details")
		public String getCompanySecondaryMobile() {
			return companySecondaryMobile;
		}



		public void setCompanySecondaryMobile(String companySecondaryMobile) {
			this.companySecondaryMobile = companySecondaryMobile;
		}


		@Column(name="comp_contact_person_number", table = "tr_invoice_details")
		public String getCompanyContactPersonNumber() {
			return companyContactPersonNumber;
		}



		public void setCompanyContactPersonNumber(String companyContactPersonNumber) {
			this.companyContactPersonNumber = companyContactPersonNumber;
		}


		@Column(name="comp_contact_person_name", table = "tr_invoice_details")
		public String getCompanyContactPersonName() {
			return companyContactPersonName;
		}



		public void setCompanyContactPersonName(String companyContactPersonName) {
			this.companyContactPersonName = companyContactPersonName;
		}


		@Column(name="comp_primary_telephone", table = "tr_invoice_details")
		public String getCompanyPrimaryTelephone() {
			return companyPrimaryTelephone;
		}



		public void setCompanyPrimaryTelephone(String companyPrimaryTelephone) {
			this.companyPrimaryTelephone = companyPrimaryTelephone;
		}


		@Column(name="comp_secondary_telephone", table = "tr_invoice_details")
		public String getCompanySecondaryTelephone() {
			return companySecondaryTelephone;
		}



		public void setCompanySecondaryTelephone(String companySecondaryTelephone) {
			this.companySecondaryTelephone = companySecondaryTelephone;
		}


		@Column(name="comp_website", table = "tr_invoice_details")
		public String getCompanyWebsite() {
			return companyWebsite;
		}



		public void setCompanyWebsite(String companyWebsite) {
			this.companyWebsite = companyWebsite;
		}


		@Column(name="comp_email", table = "tr_invoice_details")
		public String getCompanyEmail() {
			return companyEmail;
		}



		public void setCompanyEmail(String companyEmail) {
			this.companyEmail = companyEmail;
		}


		@Column(name="comp_fax_number", table = "tr_invoice_details")
		public String getCompanyFaxNumber() {
			return companyFaxNumber;
		}



		public void setCompanyFaxNumber(String companyFaxNumber) {
			this.companyFaxNumber = companyFaxNumber;
		}


		@Column(name="comp_address", table = "tr_invoice_details")
		public String getCompanyAddress() {
			return companyAddress;
		}



		public void setCompanyAddress(String companyAddress) {
			this.companyAddress = companyAddress;
		}

		@Column(name="comp_tagline", table = "tr_invoice_details")
	    public String getCompanyTagLine() {
		return companyTagLine;
	     }

	     public void setCompanyTagLine(String companyTagLine) {
		this.companyTagLine = companyTagLine;
	     }





		@Column(name="comp_gst_number", table = "tr_invoice_details")
		public String getCompanyGstNumber() {
			return companyGstNumber;
		}



		public void setCompanyGstNumber(String companyGstNumber) {
			this.companyGstNumber = companyGstNumber;
		}




	   @Column(name="comp_pan_number", table = "tr_invoice_details")
	   public String getCompanyPanNumber() {
		return companyPanNumber;
	  }

	  public void setCompanyPanNumber(String companyPanNumber) {
		this.companyPanNumber = companyPanNumber;
	  }

		@Column(name="comp_pan_date", table = "tr_invoice_details")
		public String getCompanyPanDate() {
			return companyPanDate;
		}



		public void setCompanyPanDate(String companyPanDate) {
			this.companyPanDate = companyPanDate;
		}


		@Column(name="comp_certificate_image_path", table = "tr_invoice_details")
		public String getCompanyCeritificateImagePath() {
			return companyCeritificateImagePath;
		}



		public void setCompanyCeritificateImagePath(String companyCeritificateImagePath) {
			this.companyCeritificateImagePath = companyCeritificateImagePath;
		}


		@Column(name="comp_logo_path", table = "tr_invoice_details")
		public String getCompanyLogoPath() {
			return companyLogoPath;
		}



		public void setCompanyLogoPath(String companyLogoPath) {
			this.companyLogoPath = companyLogoPath;
		}
		
		@Column(name="party_name", table = "tr_invoice_details")
		public String getPartyName() {
			return partyName;
		}



		public void setPartyName(String partyName) {
			this.partyName = partyName;
		}


		@Column(name="party_contact_person_number", table = "tr_invoice_details")
		public String getPartyContactPersonNumber() {
			return partyContactPersonNumber;
		}



		public void setPartyContactPersonNumber(String partyContactPersonNumber) {
			this.partyContactPersonNumber = partyContactPersonNumber;
		}


		@Column(name="party_pin_code", table = "tr_invoice_details")
		public String getPartyPinCode() {
			return partyPinCode;
		}



		public void setPartyPinCode(String partyPinCode) {
			this.partyPinCode = partyPinCode;
		}


		@Column(name="party_area_id", table = "tr_invoice_details")
		public Long getPartyAreaId() {
			return partyAreaId;
		}



		public void setPartyAreaId(Long partyAreaId) {
			this.partyAreaId = partyAreaId;
		}


		@Column(name="party_city_id", table = "tr_invoice_details")
		public Long getPartyCityId() {
			return partyCityId;
		}



		public void setPartyCityId(Long partyCityId) {
			this.partyCityId = partyCityId;
		}


		@Column(name="party_state_id", table = "tr_invoice_details")
		public Long getPartyStateId() {
			return partyStateId;
		}



		public void setPartyStateId(Long partyStateId) {
			this.partyStateId = partyStateId;
		}


		@Column(name="party_country_id", table = "tr_invoice_details")
		public Long getPartyCountryId() {
			return partyCountryId;
		}



		public void setPartyCountryId(Long partyCountryId) {
			this.partyCountryId = partyCountryId;
		}


		@Column(name="party_primary_telephone", table = "tr_invoice_details")
		public String getPartyPrimaryTelephone() {
			return partyPrimaryTelephone;
		}



		public void setPartyPrimaryTelephone(String partyPrimaryTelephone) {
			this.partyPrimaryTelephone = partyPrimaryTelephone;
		}


		@Column(name="party_secondary_telephone", table = "tr_invoice_details")
		public String getPartySecondaryTelephone() {
			return partySecondaryTelephone;
		}


		public void setPartySecondaryTelephone(String partySecondaryTelephone) {
			this.partySecondaryTelephone = partySecondaryTelephone;
		}


		@Column(name="party_primary_mobile_number", table = "tr_invoice_details")
		public String getPartyPrimaryMobile() {
			return partyPrimaryMobile;
		}



		public void setPartyPrimaryMobile(String partyPrimaryMobile) {
			this.partyPrimaryMobile = partyPrimaryMobile;
		}


		@Column(name="party_secondary_mobile", table = "tr_invoice_details")
		public String getPartySecondaryMobile() {
			return partySecondaryMobile;
		}



		public void setPartySecondaryMobile(String partySecondaryMobile) {
			this.partySecondaryMobile = partySecondaryMobile;
		}


		@Column(name="party_email", table = "tr_invoice_details")
		public String getPartyEmail() {
			return partyEmail;
		}



		public void setPartyEmail(String partyEmail) {
			this.partyEmail = partyEmail;
		}


		@Column(name="party_website", table = "tr_invoice_details")
		public String getPartyWebsite() {
			return partyWebsite;
		}



		public void setPartyWebsite(String partyWebsite) {
			this.partyWebsite = partyWebsite;
		}


		@Column(name="party_contact_person_name", table = "tr_invoice_details")
		public String getPartyContactPersonName() {
			return partyContactPersonName;
		}



		public void setPartyContactPersonName(String partyContactPersonName) {
			this.partyContactPersonName = partyContactPersonName;
		}


//		@Column(name="party_bill_to_address")
//		public String getPartyBillToAddress() {
//			return partyBillToAddress;
//		}
	//
	//
	//
//		public void setPartyBillToAddress(String partyBillToAddress) {
//			this.partyBillToAddress = partyBillToAddress;
//		}
	//
	//
//		@Column(name="party_ship_to_address")
//		public String getPartyShipAddress() {
//			return partyShipAddress;
//		}
	//
	//
	//
//		public void setPartyShipAddress(String partyShipAddress) {
//			this.partyShipAddress = partyShipAddress;
//		}


		@Column(name="party_due_days_limit", table = "tr_invoice_details")
		public String getPartyDueDaysLimit() {
			return partyDueDaysLimit;
		}



		public void setPartyDueDaysLimit(String partyDueDaysLimit) {
			this.partyDueDaysLimit = partyDueDaysLimit;
		}


		@Column(name="party_gst_registration_type_id", table = "tr_invoice_details")
		public String getPartyGstRegistrationTypeId() {
			return partyGstRegistrationTypeId;
		}



		public void setPartyGstRegistrationTypeId(String partyGstRegistrationTypeId) {
			this.partyGstRegistrationTypeId = partyGstRegistrationTypeId;
		}


//		@Column(name="party_gst_number")
//		public String getPartyGstNumber() {
//			return partyGstNumber;
//		}
	//
	//
	//
//		public void setPartyGstNumber(String partyGstNumber) {
//			this.partyGstNumber = partyGstNumber;
//		}


		@Column(name="party_pan_number", table = "tr_invoice_details")
		public String getPartyPanNumber() {
			return partyPanNumber;
		}



		public void setPartyPanNumber(String partyPanNumber) {
			this.partyPanNumber = partyPanNumber;
		}


		@Column(name="is_igst", table = "tr_invoice_details")
		public String getIsIgst() {
			return isIgst;
		}



		public void setIsIgst(String isIgst) {
			this.isIgst = isIgst;
		}

	@Column(name="is_reused", table = "tr_invoice_details")
	public Integer getIsReused() {
		return isReused;
	}

	public void setIsReused(Integer isReused) {
		this.isReused = isReused;
	}

	@Column(name="tcs_percentage")
	public Double getTcsPercentage() {
		return tcsPercentage;
	}

	public void setTcsPercentage(Double tcsPercentage) {
		this.tcsPercentage = tcsPercentage;
	}
	@Column(name="tcs_amount")
	public Double getTcsAmount() {
		return tcsAmount;
	}

	public void setTcsAmount(Double tcsAmount) {
		this.tcsAmount = tcsAmount;
	}

	@Column(name="material_note_type")
	public Long getMaterialNoteType() {
		return materialNoteType;
	}

	public void setMaterialNoteType(Long materialNoteType) {
		this.materialNoteType = materialNoteType;
	}

	@Column(name="currency_id")
	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}
	@Column(name="currency_rate")
	public Double getCurrencyRate() {
		return currencyRate;
	}

	public void setCurrencyRate(Double currencyRate) {
		this.currencyRate = currencyRate;
	}
	@Column(name="total_amount_currency")
	public Double getTotalAmountCurrency() {
		return totalAmountCurrency;
	}

	public void setTotalAmountCurrency(Double totalAmountCurrency) {
		this.totalAmountCurrency = totalAmountCurrency;
	}

	@Column(name="final_destination")
	public String getFinalDestination() {
		return finalDestination;
	}

	public void setFinalDestination(String finalDestination) {
		this.finalDestination = finalDestination;
	}

	@Column(name="vessel_number")
	public String getVesselNumber() {
		return vesselNumber;
	}

	public void setVesselNumber(String vesselNumber) {
		this.vesselNumber = vesselNumber;
	}

	@Column(name="shipper")
	public String getShipper() {
		return shipper;
	}

	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	@Column(name="city_of_loading")
	public String getCityOfLoading() {
		return cityOfLoading;
	}

	public void setCityOfLoading(String cityOfLoading) {
		this.cityOfLoading = cityOfLoading;
	}

	@Column(name="city_of_discharge")
	public String getCityOfDischarge() {
		return cityOfDischarge;
	}

	public void setCityOfDischarge(String cityOfDischarge) {
		this.cityOfDischarge = cityOfDischarge;
	}
	@Column(name="country_origin_of_goods")
	public String getCountryOfOriginOfGoods() {
		return countryOfOriginOfGoods;
	}

	public void setCountryOfOriginOfGoods(String countryOfOriginOfGoods) {
		this.countryOfOriginOfGoods = countryOfOriginOfGoods;
	}


	@Column(name="country_of_destination")
	public String getCountryOfDestination() {
		return countryOfDestination;
	}

	public void setCountryOfDestination(String countryOfDestination) {
		this.countryOfDestination = countryOfDestination;
	}

	@Column(name="party_currency_name")

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
}
