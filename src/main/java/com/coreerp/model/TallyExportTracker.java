package com.coreerp.model;

import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="tr_tally_export_tracker")
public class TallyExportTracker extends AbstractTransactionModel{

    private Date fromDate;

    private Date toDate;

    private Long invoiceTypeId;

    private Integer voucherCount;

    private Integer stockItemCount;

    private Long partyTypeId;

    private Integer partyCount;

    @Column(name="from_date")
    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Column(name="to_date")
    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    @Column(name="voucher_count")
    public Integer getVoucherCount() {
        return voucherCount;
    }

    public void setVoucherCount(Integer voucherCount) {
        this.voucherCount = voucherCount;
    }

    @Column(name="stock_item_count")
    public Integer getStockItemCount() {
        return stockItemCount;
    }

    public void setStockItemCount(Integer stockItemCount) {
        this.stockItemCount = stockItemCount;
    }


    @Column(name="invoice_type_id")
    public Long getInvoiceTypeId() {
        return invoiceTypeId;
    }

    public void setInvoiceTypeId(Long invoiceTypeId) {
        this.invoiceTypeId = invoiceTypeId;
    }

    @Column(name="party_type_id")
    public Long getPartyTypeId() {
        return partyTypeId;
    }

    public void setPartyTypeId(Long partyTypeId) {
        this.partyTypeId = partyTypeId;
    }

    @Column(name="party_count")
    public Integer getPartyCount() {
        return partyCount;
    }

    public void setPartyCount(Integer partyCount) {
        this.partyCount = partyCount;
    }
}
