package com.coreerp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_voucher_header")
public class VoucherHeader extends AbstractTransactionModel{
	
	private Long voucherId;
	
	private Date voucherDate;
	
	private TransactionType voucherType;
	
	private Double amount;

	private String voucherNumber;
	
	private String comments;
	
	private String paidTo;
	
	private String chequeNumber;
	
	private String bankName;
	
	private Date chequeDate;
	
	private FinancialYear financialYear;
	
    private List<VoucherItem> voucherItems;
	
	private Company company;
	
    private String companyName; 
    private Long companygGstRegistrationTypeId; //companyGstRegistrationnTypeId
    private String companyPinCode; //companyPinCode .. etc
    private Long companyStateId;
    private String companyStateName;
    private Long companyCountryId;
    private String companyPrimaryMobile;
    private String companySecondaryMobile;
    private String companyContactPersonNumber;
    private String companyContactPersonName;
    private String companyPrimaryTelephone;
    private String companySecondaryTelephone;
    private String companyWebsite;
    private String companyEmail;
    private String companyFaxNumber;
    private String companyAddress;
    private String companytagLine;
    private String companyGstNumber;
    private String compapanNumber;
    private String companyPanDate;
    private String companyCeritificateImagePath;
    private String companyLogoPath;

	
	
	
	
	@Column(name="voucher_date")
	public Date getVoucherDate() {
		return voucherDate;
	}
	
	
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	@ManyToOne
	@JoinColumn(name="voucher_type", foreignKey=@ForeignKey(name="FK_VOTH_TT"))
	@NotNull
	public TransactionType getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(TransactionType voucherType) {
		this.voucherType = voucherType;
	}

	@Column(name="total_amount")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name="voucher_number")
	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	@Column(name="comments")
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Column(name="paid_to")
	public String getPaidTo() {
		return paidTo;
	}

	public void setPaidTo(String paidTo) {
		this.paidTo = paidTo;
	}

	@Column(name="cheque_number")
	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	@Column(name="bank_name")
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Column(name="cheque_date")
	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	
	

	@ManyToOne
	@JoinColumn(name="company_id", foreignKey=@ForeignKey(name="FK_VOTH_COMPANY"))
	@NotNull
	public Company getCompany() {
		return company;
	}

	@ManyToOne
	@JoinColumn(name="financial_year_id", foreignKey=@ForeignKey(name="FK_VOTH_FY"))
	@NotNull
	public FinancialYear getFinancialYear() {
		return financialYear;
	}


	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}


	public void setCompany(Company company) {
		this.company = company;
	}
	@OneToMany(mappedBy="voucherHeader",cascade=CascadeType.ALL)
	public List<VoucherItem> getVoucherItems() {
		return voucherItems;
	}

	public void setVoucherItems(List<VoucherItem> voucherItems) {
		this.voucherItems = voucherItems;
	}
	@Column(name="voucher_id")

	public Long getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(Long voucherId) {
		this.voucherId = voucherId;
	}
	
	
	//............................
	
	
		@Column(name="company_name")
		public String getCompanyName() {
			return companyName;
		}



		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}
		
		@Column(name="comp_gst_registration_type_id")
		public Long getCompanygGstRegistrationTypeId() {
			return companygGstRegistrationTypeId;
		}



		public void setCompanygGstRegistrationTypeId(Long companygGstRegistrationTypeId) {
			this.companygGstRegistrationTypeId = companygGstRegistrationTypeId;
		}


		@Column(name="comp_pincode")
		public String getCompanyPinCode() {
			return companyPinCode;
		}



		public void setCompanyPinCode(String companyPinCode) {
			this.companyPinCode = companyPinCode;
		}


		@Column(name="comp_state_id")
		public Long getCompanyStateId() {
			return companyStateId;
		}



		public void setCompanyStateId(Long companyStateId) {
			this.companyStateId = companyStateId;
		}
		
		@Column(name="comp_state_name")
		public String getCompanyStateName() {
			return companyStateName;
		}

		public void setCompanyStateName(String companyStateName) {
			this.companyStateName = companyStateName;
		}


		@Column(name="comp_country_id")
		public Long getCompanyCountryId() {
			return companyCountryId;
		}



		public void setCompanyCountryId(Long companyCountryId) {
			this.companyCountryId = companyCountryId;
		}


		@Column(name="comp_primary_mobile")
		public String getCompanyPrimaryMobile() {
			return companyPrimaryMobile;
		}



		public void setCompanyPrimaryMobile(String companyPrimaryMobile) {
			this.companyPrimaryMobile = companyPrimaryMobile;
		}


		@Column(name="comp_secondary_mobile")
		public String getCompanySecondaryMobile() {
			return companySecondaryMobile;
		}



		public void setCompanySecondaryMobile(String companySecondaryMobile) {
			this.companySecondaryMobile = companySecondaryMobile;
		}


		@Column(name="comp_contact_person_number")
		public String getCompanyContactPersonNumber() {
			return companyContactPersonNumber;
		}



		public void setCompanyContactPersonNumber(String companyContactPersonNumber) {
			this.companyContactPersonNumber = companyContactPersonNumber;
		}


		@Column(name="comp_contact_person_name")
		public String getCompanyContactPersonName() {
			return companyContactPersonName;
		}



		public void setCompanyContactPersonName(String companyContactPersonName) {
			this.companyContactPersonName = companyContactPersonName;
		}


		@Column(name="comp_primary_telephone")
		public String getCompanyPrimaryTelephone() {
			return companyPrimaryTelephone;
		}



		public void setCompanyPrimaryTelephone(String companyPrimaryTelephone) {
			this.companyPrimaryTelephone = companyPrimaryTelephone;
		}


		@Column(name="comp_secondary_telephone")
		public String getCompanySecondaryTelephone() {
			return companySecondaryTelephone;
		}



		public void setCompanySecondaryTelephone(String companySecondaryTelephone) {
			this.companySecondaryTelephone = companySecondaryTelephone;
		}


		@Column(name="comp_website")
		public String getCompanyWebsite() {
			return companyWebsite;
		}



		public void setCompanyWebsite(String companyWebsite) {
			this.companyWebsite = companyWebsite;
		}


		@Column(name="comp_email")
		public String getCompanyEmail() {
			return companyEmail;
		}



		public void setCompanyEmail(String companyEmail) {
			this.companyEmail = companyEmail;
		}


		@Column(name="comp_fax_number")
		public String getCompanyFaxNumber() {
			return companyFaxNumber;
		}



		public void setCompanyFaxNumber(String companyFaxNumber) {
			this.companyFaxNumber = companyFaxNumber;
		}


		@Column(name="comp_address")
		public String getCompanyAddress() {
			return companyAddress;
		}



		public void setCompanyAddress(String companyAddress) {
			this.companyAddress = companyAddress;
		}


		@Column(name="comp_tagline")
		public String getCompanytagLine() {
			return companytagLine;
		}



		public void setCompanytagLine(String companytagLine) {
			this.companytagLine = companytagLine;
		}


		@Column(name="comp_gst_number")
		public String getCompanyGstNumber() {
			return companyGstNumber;
		}



		public void setCompanyGstNumber(String companyGstNumber) {
			this.companyGstNumber = companyGstNumber;
		}


		@Column(name="comp_pan_number")
		public String getCompapanNumber() {
			return compapanNumber;
		}



		public void setCompapanNumber(String compapanNumber) {
			this.compapanNumber = compapanNumber;
		}


		@Column(name="comp_pan_date")
		public String getCompanyPanDate() {
			return companyPanDate;
		}



		public void setCompanyPanDate(String companyPanDate) {
			this.companyPanDate = companyPanDate;
		}


		@Column(name="comp_certificate_image_path")
		public String getCompanyCeritificateImagePath() {
			return companyCeritificateImagePath;
		}



		public void setCompanyCeritificateImagePath(String companyCeritificateImagePath) {
			this.companyCeritificateImagePath = companyCeritificateImagePath;
		}


		@Column(name="comp_logo_path")
		public String getCompanyLogoPath() {
			return companyLogoPath;
		}



		public void setCompanyLogoPath(String companyLogoPath) {
			this.companyLogoPath = companyLogoPath;
		}
	

}
