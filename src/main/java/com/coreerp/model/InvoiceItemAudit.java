package com.coreerp.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name="tr_invoice_item_audit")
public class InvoiceItemAudit {
    private String id;

    private Date createdDateTime;

    private String createdBy;

    private Date updatedDateTime;

    private String updatedBy;

    private String invoiceHeaderId;
    private Long materialId;
    private Integer slNo;
    private Double quantity;
    private Double price;
    private Double amount;
    private String purchaseOrderHeaderId;
    private String purchaseOrderItemId;
    private String deliveryChallanHeaderId ;
    private String deliveryChallanItemId;
    private String grnHeaderId ;
    private String grnItemId;
    private String proformaInvoiceHeaderId;
    private String proformaInvoiceItemId;
    private String sourceInvoiceHeaderId;
    private String sourceInvoiceItemId;
    private Double basicAmount;
    private Double taxPercent;
    private Double taxAmount;
    private Double amountAfterTax;
    private Double discountPercentage;
    private Double discountAmount;
    private Double amountAfterDiscount;
    private Long unitOfMeasurementId;
    private Long taxId;
    private Integer inclusiveTax;
    private Double taxRate;
    private String remarks;
    private String outDeliveryChallanNumber;
    private String batchCode;
    private Date outDeliveryChallanDate;
    private String inspectionNumber;
    private String inDeliveryChallanNumber;
    private Date inDeliveryChallanDate;
    private String descriptionOfPackages;
    private String averageContentPerPackage;
    private Double processQuantity;
    private Double transportationAmount;
    private Double cessPercentage;
    private Double cessAmount;
    private Double igstTaxPercentage;
    private Double igstTaxAmount;
    private Double sgstTaxPercentage;
    private Double sgstTaxAmount;
    private Double cgstTaxPercentage;
    private Double cgstTaxAmount;
    private Double noteBalanceQuantity;
    private String specification;
    private String processId;
    private String processName;


    @Id
    @Column(name="id", length=255)
    @Size(min=1)
    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }
    @Column(name="created_date_time")
    public Date getCreatedDateTime() {
        return createdDateTime;
    }
    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Column(name="created_by")
    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name="updated_date_time")
    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }
    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    @Column(name="updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name="sl_no")
    public Integer getSlNo() {
        return slNo;
    }



    public void setSlNo(Integer slNo) {
        this.slNo = slNo;
    }

    @Column(name="quantity")
    public Double getQuantity() {
        return quantity;
    }


    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    @Column(name="price")
    public Double getPrice() {
        return price;
    }


    public void setPrice(Double price) {
        this.price = price;
    }

    @Column(name="amount")
    public Double getAmount() {
        return amount;
    }


    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Column(name="disc_per")
    public Double getDiscountPercentage() {
        return discountPercentage;
    }


    public void setDiscountPercentage(Double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    @Column(name="disc_amt")
    public Double getDiscountAmount() {
        return discountAmount;
    }


    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    @Column(name="amt_after_disc")
    public Double getAmountAfterDiscount() {
        return amountAfterDiscount;
    }


    public void setAmountAfterDiscount(Double amountAfterDiscount) {
        this.amountAfterDiscount = amountAfterDiscount;
    }

    @Column(name="transportation_amt")
    public Double getTransportationAmount() {
        return transportationAmount;
    }


    public void setTransportationAmount(Double transportationAmount) {
        this.transportationAmount = transportationAmount;
    }

    @Column(name="cess_per")
    public Double getCessPercentage() {
        return cessPercentage;
    }


    public void setCessPercentage(Double cessPercentage) {
        this.cessPercentage = cessPercentage;
    }

    @Column(name="cess_amt")
    public Double getCessAmount() {
        return cessAmount;
    }


    public void setCessAmount(Double cessAmount) {
        this.cessAmount = cessAmount;
    }

    @Column(name="igst_tax_per")
    public Double getIgstTaxPercentage() {
        return igstTaxPercentage;
    }


    public void setIgstTaxPercentage(Double igstTaxPercentage) {
        this.igstTaxPercentage = igstTaxPercentage;
    }

    @Column(name="igst_tax_amt")
    public Double getIgstTaxAmount() {
        return igstTaxAmount;
    }


    public void setIgstTaxAmount(Double igstTaxAmount) {
        this.igstTaxAmount = igstTaxAmount;
    }

    @Column(name="sgst_tax_per")
    public Double getSgstTaxPercentage() {
        return sgstTaxPercentage;
    }


    public void setSgstTaxPercentage(Double sgstTaxPercentage) {
        this.sgstTaxPercentage = sgstTaxPercentage;
    }

    @Column(name="sgst_tax_amt")
    public Double getSgstTaxAmount() {
        return sgstTaxAmount;
    }


    public void setSgstTaxAmount(Double sgstTaxAmount) {
        this.sgstTaxAmount = sgstTaxAmount;
    }

    @Column(name="cgst_tax_per")
    public Double getCgstTaxPercentage() {
        return cgstTaxPercentage;
    }


    public void setCgstTaxPercentage(Double cgstTaxPercentage) {
        this.cgstTaxPercentage = cgstTaxPercentage;
    }

    @Column(name="cgst_tax_amt")
    public Double getCgstTaxAmount() {
        return cgstTaxAmount;
    }


    public void setCgstTaxAmount(Double cgstTaxAmount) {
        this.cgstTaxAmount = cgstTaxAmount;
    }

    @Column(name="amt_after_tax")
    public Double getAmountAfterTax() {
        return amountAfterTax;
    }


    public void setAmountAfterTax(Double amountAfterTax) {
        this.amountAfterTax = amountAfterTax;
    }

    @Column(name="remarks")
    public String getRemarks() {
        return remarks;
    }


    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }



    @Column(name = "invoice_header_id")
    public String getInvoiceHeaderId() {
        return invoiceHeaderId;
    }


    public void setInvoiceHeaderId(String invoiceHeaderId) {
        this.invoiceHeaderId = invoiceHeaderId;
    }


    @Column(name="material_id")
    public Long getMaterialId() {
        return materialId;
    }


    public void setMaterialId(Long materialId) {
        this.materialId = materialId;
    }


    @Column(name = "po_header_id")
    public String getPurchaseOrderHeaderId() {
        return purchaseOrderHeaderId;
    }


    public void setPurchaseOrderHeaderId(String purchaseOrderHeaderId) {
        this.purchaseOrderHeaderId = purchaseOrderHeaderId;
    }




    @Column(name = "dc_header_id")
    public String getDeliveryChallanHeaderId() {
        return deliveryChallanHeaderId;
    }


    public void setDeliveryChallanHeaderId(String deliveryChallanHeaderId) {
        this.deliveryChallanHeaderId = deliveryChallanHeaderId;
    }


    @Column(name="basic_amt")
    public Double getBasicAmount() {
        return basicAmount;
    }


    public void setBasicAmount(Double basicAmount) {
        this.basicAmount = basicAmount;
    }

    @Column(name="tax_percent")
    public Double getTaxPercent() {
        return taxPercent;
    }


    public void setTaxPercent(Double taxPercent) {
        this.taxPercent = taxPercent;
    }

    @Column(name="tax_amt")
    public Double getTaxAmount() {
        return taxAmount;
    }


    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    @Column(name="uom_id")
    public Long getUnitOfMeasurementId() {
        return unitOfMeasurementId;
    }


    public void setUnitOfMeasurementId(Long unitOfMeasurementId) {
        this.unitOfMeasurementId = unitOfMeasurementId;
    }

    @Column(name="tax_id")
    public Long getTaxId() {
        return taxId;
    }


    public void setTaxId(Long taxId) {
        this.taxId = taxId;
    }

    @Column(name="inclusive_tax")
    public Integer getInclusiveTax() {
        return inclusiveTax;
    }


    public void setInclusiveTax(Integer inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
    }

    @Column(name="tax_rate")
    public Double getTaxRate() {
        return taxRate;
    }


    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    @Column(name="dc_number")
    public String getOutDeliveryChallanNumber() {
        return outDeliveryChallanNumber;
    }


    public void setOutDeliveryChallanNumber(String outDeliveryChallanNumber) {
        this.outDeliveryChallanNumber = outDeliveryChallanNumber;
    }

    @Column(name="batch_code")
    public String getBatchCode() {
        return batchCode;
    }


    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    @Column(name="out_dc_date")
    public Date getOutDeliveryChallanDate() {
        return outDeliveryChallanDate;
    }


    public void setOutDeliveryChallanDate(Date outDeliveryChallanDate) {
        this.outDeliveryChallanDate = outDeliveryChallanDate;
    }

    @Column(name="inspection_number")
    public String getInspectionNumber() {
        return inspectionNumber;
    }


    public void setInspectionNumber(String inspectionNumber) {
        this.inspectionNumber = inspectionNumber;
    }

    @Column(name="in_dc_number")
    public String getInDeliveryChallanNumber() {
        return inDeliveryChallanNumber;
    }


    public void setInDeliveryChallanNumber(String inDeliveryChallanNumber) {
        this.inDeliveryChallanNumber = inDeliveryChallanNumber;
    }

    @Column(name="in_dc_date")
    public Date getInDeliveryChallanDate() {
        return inDeliveryChallanDate;
    }


    public void setInDeliveryChallanDate(Date inDeliveryChallanDate) {
        this.inDeliveryChallanDate = inDeliveryChallanDate;
    }

    @Column(name="description_of_packages")
    public String getDescriptionOfPackages() {
        return descriptionOfPackages;
    }


    public void setDescriptionOfPackages(String descriptionOfPackages) {
        this.descriptionOfPackages = descriptionOfPackages;
    }

    @Column(name="average_content_per_package")
    public String getAverageContentPerPackage() {
        return averageContentPerPackage;
    }


    public void setAverageContentPerPackage(String averageContentPerPackage) {
        this.averageContentPerPackage = averageContentPerPackage;
    }

    @Column(name="process_qty")
    public Double getProcessQuantity() {
        return processQuantity;
    }


    public void setProcessQuantity(Double processQuantity) {
        this.processQuantity = processQuantity;
    }


    @Column(name = "po_item_id")
    public String getPurchaseOrderItemId() {
        return purchaseOrderItemId;
    }



    public void setPurchaseOrderItemId(String purchaseOrderItemId) {
        this.purchaseOrderItemId = purchaseOrderItemId;
    }


    @Column(name = "grn_header_id")
    public String getGrnHeaderId() {
        return grnHeaderId;
    }



    public void setGrnHeaderId(String grnHeaderId) {
        this.grnHeaderId = grnHeaderId;
    }


    @JoinColumn(name = "grn_item_id")
    public String getGrnItemId() {
        return grnItemId;
    }



    public void setGrnItemId(String grnItemId) {
        this.grnItemId = grnItemId;
    }



    @Column(name = "dc_item_id")
    public String getDeliveryChallanItemId() {
        return deliveryChallanItemId;
    }



    public void setDeliveryChallanItemId(String deliveryChallanItemId) {
        this.deliveryChallanItemId = deliveryChallanItemId;
    }


    @Column(name = "proforma_invoice_header_id")
    public String getProformaInvoiceHeaderId() {
        return proformaInvoiceHeaderId;
    }



    public void setProformaInvoiceHeaderId(String proformaInvoiceHeaderId) {
        this.proformaInvoiceHeaderId = proformaInvoiceHeaderId;
    }


    @Column(name = "proforma_invoice_item_id")
    public String getProformaInvoiceItemId() {
        return proformaInvoiceItemId;
    }



    public void setProformaInvoiceItemId(String proformaInvoiceItemId) {
        this.proformaInvoiceItemId = proformaInvoiceItemId;
    }


    @Column(name = "source_invoice_header_id")
    public String getSourceInvoiceHeaderId() {
        return sourceInvoiceHeaderId;
    }



    public void setSourceInvoiceHeaderId(String sourceInvoiceHeaderId) {
        this.sourceInvoiceHeaderId = sourceInvoiceHeaderId;
    }


    @Column(name = "source_invoice_item_id")
    public String getSourceInvoiceItemId() {
        return sourceInvoiceItemId;
    }



    public void setSourceInvoiceItemId(String sourceInvoiceItemId) {
        this.sourceInvoiceItemId = sourceInvoiceItemId;
    }


    @Column(name="note_balance_quantity")
    public Double getNoteBalanceQuantity() {
        return noteBalanceQuantity;
    }



    public void setNoteBalanceQuantity(Double noteBalanceQuantity) {
        this.noteBalanceQuantity = noteBalanceQuantity;
    }


    @Column(name="specification")
    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    @Column(name="process_id")
    public String getProcessId() {
        return processId;
    }
    public void setProcessId(String processId) {
        this.processId = processId;
    }

    @Column(name="process_name")
    public String getProcessName() {
        return processName;
    }
    public void setProcessName(String processName) {
        this.processName = processName;
    }
}
