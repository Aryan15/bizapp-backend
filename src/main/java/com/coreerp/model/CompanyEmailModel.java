package com.coreerp.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cf_company_email_setting")
public class CompanyEmailModel {

    private Long id;
    private Long companyId;
    private String emailUserName;
    private String emailPassword;
    private Long emailPort;
    private String emailTemplate;
    private String emailSubject;
    private String emailHost;
    private Integer isEmailEnabled;
    private String emailSmtpStarttlsEnable;
    private String emailSmtpAuth;
    private String emailTransportProtocol;
    private String emailDebug;
    private String emailFrom;



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="company_id")
    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Column(name="email_username")
    public String getEmailUserName() {
        return emailUserName;
    }

    public void setEmailUserName(String emailUserName) {
        this.emailUserName = emailUserName;
    }

    @Column(name="email_password")
    public String getEmailPassword() {
        return emailPassword;
    }


    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    @Column(name="email_port")
    public Long getEmailPort() {
        return emailPort;
    }


    public void setEmailPort(Long emailPort) {
        this.emailPort = emailPort;
    }

    @Column(name="email_template")
    public String getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(String emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    @Column(name="email_subject")
    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    @Column(name="email_host")
    public String getEmailHost() {
        return emailHost;
    }

    public void setEmailHost(String emailHost) {
        this.emailHost = emailHost;
    }

    @Column(name="email_enabled")
    public Integer getIsEmailEnabled() {
        return isEmailEnabled;
    }

    public void setIsEmailEnabled(Integer isEmailEnabled) {
        this.isEmailEnabled = isEmailEnabled;
    }

    @Column(name="email_email_smtp_is_enabled")
    public String getEmailSmtpStarttlsEnable() {
        return emailSmtpStarttlsEnable;
    }

    public void setEmailSmtpStarttlsEnable(String emailSmtpStarttlsEnable) {
        this.emailSmtpStarttlsEnable = emailSmtpStarttlsEnable;
    }

    @Column(name="email_smtp_auth")
    public String getEmailSmtpAuth() {
        return emailSmtpAuth;
    }

    public void setEmailSmtpAuth(String emailSmtpAuth) {
        this.emailSmtpAuth = emailSmtpAuth;
    }

    @Column(name="email_trans_protocol")
    public String getEmailTransportProtocol() {
        return emailTransportProtocol;
    }

    public void setEmailTransportProtocol(String emailTransportProtocol) {
        this.emailTransportProtocol = emailTransportProtocol;
    }

    @Column(name="email_debug")
    public String getEmailDebug() {
        return emailDebug;
    }

    public void setEmailDebug(String emailDebug) {
        this.emailDebug = emailDebug;
    }

    @Column(name="email_from")
    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }
}
