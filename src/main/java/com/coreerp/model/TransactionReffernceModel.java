package com.coreerp.model;

public class TransactionReffernceModel {

    private Long transactionTypeId;

    private String transactionTypeName;

    private String transactionId;

    private String transactionNumber;

    private Long statusId;

    private String statusName;

    private String latest;

    private Long sourceTransactionTypeId;

    private String sourceTransactionTypeName;

    private String sourceTransactionId;

    private String sourceTransactionNumber;

    private String sourceOperation;



    public TransactionReffernceModel(String sourceTransactionId,Long transactionTypeId, String transactionTypeName, String transactionId, String transactionNumber, Long statusId,
                                     String statusName,String latest,Long sourceTransactionTypeId,String sourceTransactionTypeName,String sourceTransactionNumber,String sourceOperation   ) {
        super();
        this.sourceTransactionId =sourceTransactionId;
        this.transactionTypeId = transactionTypeId;
        this.transactionTypeName = transactionTypeName;
        this.transactionId = transactionId;
        this.transactionNumber = transactionNumber;
        this.statusId = statusId;
        this.statusName = statusName;
        this.latest = latest;
        this.sourceTransactionTypeId = sourceTransactionTypeId;
        this.sourceTransactionTypeName = sourceTransactionTypeName;
        this.sourceTransactionNumber = sourceTransactionNumber;
        this.sourceOperation = sourceOperation;

    }

    public Long getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(Long transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public String getTransactionTypeName() {
        return transactionTypeName;
    }

    public void setTransactionTypeName(String transactionTypeName) {
        this.transactionTypeName = transactionTypeName;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getLatest() {
        return latest;
    }

    public void setLatest(String latest) {
        this.latest = latest;
    }

    public Long getSourceTransactionTypeId() {
        return sourceTransactionTypeId;
    }

    public void setSourceTransactionTypeId(Long sourceTransactionTypeId) {
        this.sourceTransactionTypeId = sourceTransactionTypeId;
    }

    public String getSourceTransactionTypeName() {
        return sourceTransactionTypeName;
    }

    public void setSourceTransactionTypeName(String sourceTransactionTypeName) {
        this.sourceTransactionTypeName = sourceTransactionTypeName;
    }

    public String getSourceTransactionId() {
        return sourceTransactionId;
    }

    public void setSourceTransactionId(String sourceTransactionId) {
        this.sourceTransactionId = sourceTransactionId;
    }

    public String getSourceTransactionNumber() {
        return sourceTransactionNumber;
    }

    public void setSourceTransactionNumber(String sourceTransactionNumber) {
        this.sourceTransactionNumber = sourceTransactionNumber;
    }

    public String getSourceOperation() {
        return sourceOperation;
    }

    public void setSourceOperation(String sourceOperation) {
        this.sourceOperation = sourceOperation;
    }
}
