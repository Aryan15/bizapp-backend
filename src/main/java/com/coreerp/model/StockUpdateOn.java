package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="cd_stock_update_on")

public class StockUpdateOn extends AuditModelAbstractEntity {

	private String deleted="N";
	private TransactionType transactionType;
	//private UserType userType;
	private PartyType partyType;
	
	@Column(name="deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	
	@ManyToOne
	@JoinColumn(name="transaction_type_id", foreignKey=@ForeignKey(name="FK_STKU_TT"))
	public TransactionType getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
//	@ManyToOne
//	@JoinColumn(name="user_type_id", foreignKey=@ForeignKey(name="FK_STKU_UT"))
//	public UserType getUserType() {
//		return userType;
//	}
//	public void setUserType(UserType userType) {
//		this.userType = userType;
//	}
//	@Override
//	public String toString() {
//		return "StockUpdateOn [deleted=" + deleted + ", transactionType=" + transactionType + ", userType=" + userType
//				+ "]";
//	}
	@ManyToOne
	@JoinColumn(name="party_type_id", foreignKey=@ForeignKey(name="FK_STKU_PT"))
	public PartyType getPartyType() {
		return partyType;
	}
	public void setPartyType(PartyType partyType) {
		this.partyType = partyType;
	}
	
	
	
	
}
