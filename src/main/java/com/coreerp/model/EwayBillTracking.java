package com.coreerp.model;

import javax.persistence.*;
 import java.util.Date;

@Entity
@Table(name="tr_eway_bill_tracking")
public class EwayBillTracking extends AuditModelAbstractEntity{

   private String eWayBillNo;

   private Date eWayBillDate;

   private Date validUpto;

   private Date docDate;

   private String docNo;

   private String pdfUrl;

   private String detailedpdfUrl;

   private String jsonData;

   private InvoiceHeader invoiceHeader;

    private Integer status;

    private  String error;

    private String transporterId;

    private String transporterName;

    private String transDocNo;

    private Date transDocDate;

    private String vehicleNo;

    private String portName;

    private String portPin;

    private Integer portStateId;

    private Date cancelledDate;

    private String response;

    private String vehicleType;

    private Integer reasonCode;

    private String reasonRemarks;

    private Integer transactionMode;

    private Integer remainingDistance;

    private Integer extensionReasonCode;

    private String extensionReasonRemarks;

    private State state;

    private City city;

    private String fromPincode;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;

    private Integer transDistance;
    private Integer noOfItems;
    private String hsnCode;
    private Integer isCurrent;
    private String cancelledReason;
    private String cancelledRemarks;


    @Column(name="eway_bill_no")
    public String geteWayBillNo() {
        return eWayBillNo;
    }


    public void seteWayBillNo(String eWayBillNo) {
        this.eWayBillNo = eWayBillNo;
    }

    @Column(name="eway_bill_date")
    public Date geteWayBillDate() {
        return eWayBillDate;
    }

    public void seteWayBillDate(Date eWayBillDate) {
        this.eWayBillDate = eWayBillDate;
    }

    @Column(name="valid_upto")
    public Date getValidUpto() {
        return validUpto;
    }

    public void setValidUpto(Date validUpto) {
        this.validUpto = validUpto;
    }

    @Column(name="doc_date")
    public Date getDocDate() {
        return docDate;
    }

    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }

    @Column(name="doc_no")
    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    @Column(name="pdf_url")
    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    @Column(name="detailed_pdf_url")
    public String getDetailedpdfUrl() {
        return detailedpdfUrl;
    }

    public void setDetailedpdfUrl(String detailedpdfUrl) {
        this.detailedpdfUrl = detailedpdfUrl;
    }

    @Column(name="json_data")
    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    @ManyToOne
    @JoinColumn(name = "invoice_header_id", foreignKey=@ForeignKey(name="FK_EBT_INVH"))
    public InvoiceHeader getInvoiceHeader() {
        return invoiceHeader;
    }

    public void setInvoiceHeader(InvoiceHeader invoiceHeader) {
        this.invoiceHeader = invoiceHeader;
    }

    @Column(name="status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Column(name="error")
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Column(name="transporter_id")
    public String getTransporterId() {
        return transporterId;
    }

    public void setTransporterId(String transporterId) {
        this.transporterId = transporterId;
    }

    @Column(name="transporter_name")
    public String getTransporterName() {
        return transporterName;
    }

    public void setTransporterName(String transporterName) {
        this.transporterName = transporterName;
    }

    @Column(name="trans_doc_no")
    public String getTransDocNo() {
        return transDocNo;
    }

    public void setTransDocNo(String transDocNo) {
        this.transDocNo = transDocNo;
    }

    @Column(name="vehicle_no")
    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    @Column(name="port_name")
    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    @Column(name="port_pin")
    public String getPortPin() {
        return portPin;
    }

    public void setPortPin(String portPin) {
        this.portPin = portPin;
    }

    @Column(name="cancelled_date")
    public Date getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    @Column(name="response")
    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Column(name="vehicle_type")
    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    @Column(name="reason_code")
    public Integer getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        this.reasonCode = reasonCode;
    }

    @Column(name="reason_remarks")
    public String getReasonRemarks() {
        return reasonRemarks;
    }

    public void setReasonRemarks(String reasonRemarks) {
        this.reasonRemarks = reasonRemarks;
    }

    @Column(name="trans_mode")
    public Integer getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(Integer transactionMode) {
        this.transactionMode = transactionMode;
    }

    @Column(name="remaining_distance")
    public Integer getRemainingDistance() {
        return remainingDistance;
    }

    public void setRemainingDistance(Integer remainingDistance) {
        this.remainingDistance = remainingDistance;
    }

    @Column(name="extn_reason_code")
    public Integer getExtensionReasonCode() {
        return extensionReasonCode;
    }

    public void setExtensionReasonCode(Integer extensionReasonCode) {
        this.extensionReasonCode = extensionReasonCode;
    }

    @Column(name="extn_reason_remarks")
    public String getExtensionReasonRemarks() {
        return extensionReasonRemarks;
    }

    public void setExtensionReasonRemarks(String extensionReasonRemarks) {
        this.extensionReasonRemarks = extensionReasonRemarks;
    }

    @ManyToOne
    @JoinColumn(name="from_state", foreignKey=@ForeignKey(name="FK_EBT_STATE"))
    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @ManyToOne
    @JoinColumn(name="from_place", foreignKey=@ForeignKey(name="FK_EBT_CITY"))
    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }


    @Column(name="from_pincode")
    public String getFromPincode() {
        return fromPincode;
    }

    public void setFromPincode(String fromPincode) {
        this.fromPincode = fromPincode;
    }

    @Column(name="trans_doc_date")
    public Date getTransDocDate() {
        return transDocDate;
    }


    public void setTransDocDate(Date transDocDate) {
        this.transDocDate = transDocDate;
    }

    @Column(name="address_line1")
    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    @Column(name="address_line2")
    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    @Column(name="address_line3")
    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    @Column(name="trans_distance")
    public Integer getTransDistance() {
        return transDistance;
    }

    public void setTransDistance(Integer transDistance) {
        this.transDistance = transDistance;
    }

    @Column(name="no_of_item")
    public Integer getNoOfItems() {
        return noOfItems;
    }

    public void setNoOfItems(Integer noOfItems) {
        this.noOfItems = noOfItems;
    }
    @Column(name="hsn_code")
    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    @Column(name="is_current")
    public Integer getIsCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(Integer isCurrent) {
        this.isCurrent = isCurrent;
    }

    @Column(name="cancelled_reason")
    public String getCancelledReason() {
        return cancelledReason;
    }

    public void setCancelledReason(String cancelledReason) {
        this.cancelledReason = cancelledReason;
    }

    @Column(name="cancell_remarks")
    public String getCancelledRemarks() {
        return cancelledRemarks;
    }

    public void setCancelledRemarks(String cancelledRemarks) {
        this.cancelledRemarks = cancelledRemarks;
    }

    @Column(name="port_state_id")
    public Integer getPortStateId() {
        return portStateId;
    }

    public void setPortStateId(Integer portStateId) {
        this.portStateId = portStateId;
    }
}
