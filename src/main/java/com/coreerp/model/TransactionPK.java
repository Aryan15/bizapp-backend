package com.coreerp.model;

import java.io.Serializable;


public class TransactionPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected String id;

	protected Integer idVersion;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idVersion == null) ? 0 : idVersion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransactionPK other = (TransactionPK) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idVersion == null) {
			if (other.idVersion != null)
				return false;
		} else if (!idVersion.equals(other.idVersion))
			return false;
		return true;
	}
	
	public TransactionPK(){}
	
	public TransactionPK(String id, Integer idVersion) {
		super();
		this.id = id;
		this.idVersion = idVersion;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getIdVersion() {
		return idVersion;
	}

	public void setIdVersion(Integer idVersion) {
		this.idVersion = idVersion;
	}

	@Override
	public String toString() {
		return "TransactionPK [id=" + id + ", idVersion=" + idVersion + "]";
	}
	
	
}
