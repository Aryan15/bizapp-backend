package com.coreerp.model;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ma_acct_posting_rule")
public class AccountPostingRule extends AuditModelAbstractEntity{
	
	private TransactionType transactionType;
	
	private Account debitAccount;
	
	private Account creditAccount;

	@ManyToOne
	@JoinColumn(name="transaction_type_id", foreignKey=@ForeignKey(name="FK_APRL_TRTYPE"))
	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
	
	@ManyToOne
	@JoinColumn(name="debit_acct_id", foreignKey=@ForeignKey(name="FK_APRL_ACCTDR"))
	public Account getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(Account debitAccount) {
		this.debitAccount = debitAccount;
	}

	@ManyToOne
	@JoinColumn(name="credit_acct_id", foreignKey=@ForeignKey(name="FK_APRL_ACCTCR"))
	public Account getCreditAccount() {
		return creditAccount;
	}

	public void setCreditAccount(Account creditAccount) {
		this.creditAccount = creditAccount;
	}
	
	

}
