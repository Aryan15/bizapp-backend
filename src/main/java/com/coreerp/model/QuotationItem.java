package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_quotation_item")
public class QuotationItem extends AbstractTransactionModel{

	
	private QuotationHeader quotationHeader;
	
	private Material material;

	private Integer slNo;
	
	private Double quantity;
	
	private Double price;
	
	private Double amount;
	
	private String status;
	
	private Double taxPercentage;
	
	private Double taxAmount;
	
	private Double amountAfterTax;
	
	private Double discountPercentage;
	
	private Double discountAmount;
	
	private Double amountAfterDiscount;
	
	private UnitOfMeasurement unitOfMeasurement;
	
	private Tax tax;
	
	private Integer inclusiveTax;
	
	private Double taxRate;
	
	private String remarks;
	
	private Double sgstTaxRate;
	
	private Double sgstTaxAmount;
	
	private Double cgstTaxRate;
	
	private Double cgstTaxAmount;
	
	private Double igstTaxRate;
	
	private Double igstTaxAmount;
	
	private String specification;


	@ManyToOne
	@JoinColumn(name="quotation_header_id", foreignKey=@ForeignKey(name="FK_QOTI_QOTH"))
	@NotNull
	public QuotationHeader getQuotationHeader() {
		return quotationHeader;
	}

	public void setQuotationHeader(QuotationHeader quotationHeader) {
		this.quotationHeader = quotationHeader;
	}

	@ManyToOne
	@JoinColumn(name="material_id", foreignKey=@ForeignKey(name="FK_QOTI_MATERIAL"))
	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	@Column(name="sl_no")
	public Integer getSlNo() {
		return slNo;
	}

	/**
	 * @param slNo the slNo to set
	 */
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}


	@Column(name="price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name="amt")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name="tax_percent")
	public Double getTaxPercentage() {
		return taxPercentage;
	}

	public void setTaxPercentage(Double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}

	@Column(name="tax_amt")
	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	@Column(name="amt_after_tax")
	public Double getAmountAfterTax() {
		return amountAfterTax;
	}

	public void setAmountAfterTax(Double amountAfterTax) {
		this.amountAfterTax = amountAfterTax;
	}

	@Column(name="discount_percent")
	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	@Column(name="discount_amt")
	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	@Column(name="amt_after_discount")
	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}

	public void setAmountAfterDiscount(Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}

	@ManyToOne
	@JoinColumn(name="uom_id", foreignKey=@ForeignKey(name="FK_QOTI_UOM"))
	public UnitOfMeasurement getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public void setUnitOfMeasurement(UnitOfMeasurement unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}

	@ManyToOne
	@JoinColumn(name="tax_id", foreignKey=@ForeignKey(name="FK_QOTI_TAX"))
	public Tax getTax() {
		return tax;
	}

	public void setTax(Tax tax) {
		this.tax = tax;
	}

	

	@Column(name="tax_rate")
	public Double getTaxRate() {
		return taxRate;
	}

	@Column(name="inclusive_tax")
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}

	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	@Column(name="remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name="sgst_tax_rate")
	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}

	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	@Column(name="sgst_tax_amt")
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}

	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}

	@Column(name="cgst_tax_rate")
	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}

	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}

	@Column(name="cgst_tax_amt")
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}

	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}

	@Column(name="igst_tax_rate")
	public Double getIgstTaxRate() {
		return igstTaxRate;
	}

	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}

	@Column(name="igst_tax_amt")
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}

	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	@Column(name="quantity")
	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "QuotationItem [id=" + getId() + ", material=" + material
				+ ", quantity=" + quantity + ", price=" + price + ", amount=" + amount + ", status=" + status
				+ ", taxPercentage=" + taxPercentage + ", taxAmount=" + taxAmount + ", amountAfterTax=" + amountAfterTax
				+ ", discountPercentage=" + discountPercentage + ", discountAmount=" + discountAmount
				+ ", amountAfterDiscount=" + amountAfterDiscount + ", unitOfMeasurement=" + unitOfMeasurement + ", tax="
				+ tax + ", inclusiveOfTax=" + inclusiveTax + ", taxRate=" + taxRate + ", remarks=" + remarks
				+ ", sgstTaxRate=" + sgstTaxRate + ", sgstTaxAmount=" + sgstTaxAmount + ", cgstTaxRate=" + cgstTaxRate
				+ ", cgstTaxAmount=" + cgstTaxAmount + ", igstTaxRate=" + igstTaxRate + ", igstTaxAmount="
				+ igstTaxAmount + "]";
	}

	@Column(name="specification")
	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	

	

}
