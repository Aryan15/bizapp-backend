package com.coreerp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="ma_company")
public class Company extends AuditModelAbstractEntity{

	private String name;
	
	private String address;
	
	private City city;
	
	private State state;
	
	private Country country;
	
	private GstRegistrationType gstRegistrationType;
	
	private String gstNumber;
	
	private List<Material> materials;
//	private CompanyType companyType;
	
	private String deleted;
	
	private String pinCode;
	
	private String primaryTelephone;
	
	private String secondaryTelephone;
	
	private String tinNumber;
	
	private Date tinDate;
	
	private String faxNumber;
	
	private String contactPersonName;
	
	private String primaryMobile;
	
	private String secondaryMobile;
	
	private String email;
	
	private String website;
	
	private String contactPersonNumber;
	
	private Status status;
	
	private String panNumber;
	
	private String panDate;
	
	private String companyLogoPath;
	
	private List<Address> addresses;
	
	private String tagLine;
	
	private String ceritificateImagePath;

	private String signatureImagePath;

	private String msmeNumber;

	private String cinNumber;

	private String subject;

	private String iecCode;
	private Long companyCurrencyId;

	private Long creditLimit;
	private String insuranceType;
	private String insuranceRef;
	private String financePerson;
	private String financeEmail;
	@Column(name="name")
	@NotNull
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@ManyToOne
	@JoinColumn(name="city_id", foreignKey=@ForeignKey(name="FK_COMP_CITY"))
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@ManyToOne
	@JoinColumn(name="state_id", foreignKey=@ForeignKey(name="FK_COMP_STATE"))
	@NotNull
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@ManyToOne
	@JoinColumn(name="country_id", foreignKey=@ForeignKey(name="FK_COMP_COUNTRY"))
	@NotNull
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	
	@ManyToOne
	@JoinColumn(name="gst_reg_type_id", foreignKey=@ForeignKey(name="FK_COMP_GSTRT"))
	//@NotNull
	public GstRegistrationType getGstRegistrationType() {
		return gstRegistrationType;
	}

	public void setGstRegistrationType(GstRegistrationType gstRegistrationType) {
		this.gstRegistrationType = gstRegistrationType;
	}

	@Column(name="gst_number")
	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

//	@ManyToOne
//	@JoinColumn(name="company_type_id")
//	public CompanyType getCompanyType() {
//		return companyType;
//	}

//	public void setCompanyType(CompanyType companyType) {
//		this.companyType = companyType;
//	}

	@Column(name="deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	
	@OneToMany(mappedBy="company")
	@JsonIgnore
	public List<Material> getMaterials() {
		return materials;
	}

	public void setMaterials(List<Material> materials) {
		this.materials = materials;
	}

	@Column(name="pincode")
	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	
	@Column(name="primary_telephone")
	public String getPrimaryTelephone() {
		return primaryTelephone;
	}

	public void setPrimaryTelephone(String primaryTelephone) {
		this.primaryTelephone = primaryTelephone;
	}

	@Column(name="secondary_telephone")
	public String getSecondaryTelephone() {
		return secondaryTelephone;
	}

	public void setSecondaryTelephone(String secondaryTelephone) {
		this.secondaryTelephone = secondaryTelephone;
	}

	@Column(name="tin_no")
	public String getTinNumber() {
		return tinNumber;
	}

	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}

	@Column(name="tin_date")
	public Date getTinDate() {
		return tinDate;
	}

	public void setTinDate(Date tinDate) {
		this.tinDate = tinDate;
	}

	@Column(name="fax_no")
	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	@Column(name="contact_person")
	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	@Column(name="primary_mobile")
	public String getPrimaryMobile() {
		return primaryMobile;
	}

	public void setPrimaryMobile(String primaryMobile) {
		this.primaryMobile = primaryMobile;
	}

	@Column(name="secondary_mobile")
	public String getSecondaryMobile() {
		return secondaryMobile;
	}

	public void setSecondaryMobile(String secondaryMobile) {
		this.secondaryMobile = secondaryMobile;
	}

	@Column(name="email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="website")
	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Column(name="contact_person_number")
	public String getContactPersonNumber() {
		return contactPersonNumber;
	}

	public void setContactPersonNumber(String contactPersonNumber) {
		this.contactPersonNumber = contactPersonNumber;
	}

	@ManyToOne
	@JoinColumn(name="status_id", foreignKey=@ForeignKey(name="FK_COMP_STAT"))
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Column(name="pan_number")
	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	@Column(name="pan_date")
	public String getPanDate() {
		return panDate;
	}

	public void setPanDate(String panDate) {
		this.panDate = panDate;
	}

	@Column(name="company_logo")
	public String getCompanyLogoPath() {
		return companyLogoPath;
	}

	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}

	@OneToMany()
	@JoinTable(name = "ma_company_address"
			, joinColumns = @JoinColumn(name = "company_id")
			, inverseJoinColumns = @JoinColumn(name = "address_id")
			, foreignKey = @ForeignKey(name = "FK_COMP_ADDRESS")
    		, inverseForeignKey = @ForeignKey(name = "FK_ADDRESS_COMP")
			, uniqueConstraints=@UniqueConstraint(name="UK_COMP_ADDRESS",columnNames={"address_id"})
	)
	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	@Column(name = "certificate_image_path")
	public String getCeritificateImagePath() {
		return ceritificateImagePath;
	}

	public void setCeritificateImagePath(String ceritificateImagePath) {
		this.ceritificateImagePath = ceritificateImagePath;
	}

	@Column(name = "tag_line")
	public String getTagLine() {
		return tagLine;
	}

	public void setTagLine(String tagLine) {
		this.tagLine = tagLine;
	}


	@Column(name = "signature_image_path")
	public String getSignatureImagePath() {
		return signatureImagePath;
	}

	public void setSignatureImagePath(String signatureImagePath) {
		this.signatureImagePath = signatureImagePath;
	}


	@Column(name="msme_number")
	public String getMsmeNumber() {
		return msmeNumber;
	}

	public void setMsmeNumber(String msmeNumber) {
		this.msmeNumber = msmeNumber;
	}

	@Column(name="cin_number")
	public String getCinNumber() {
		return cinNumber;
	}

	public void setCinNumber(String cinNumber) {
		this.cinNumber = cinNumber;
	}


	@Column(name="subject")
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name="iec_code")
	public String getIecCode() {
		return iecCode;
	}


	public void setIecCode(String iecCode) {
		this.iecCode = iecCode;
	}

	@Column(name="company_currency_id")
	public Long getCompanyCurrencyId() {
		return companyCurrencyId;
	}

	public void setCompanyCurrencyId(Long companyCurrencyId) {
		this.companyCurrencyId = companyCurrencyId;
	}

	@Column(name="company_credit_limit")
	public Long getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(Long creditLimit) {
		this.creditLimit = creditLimit;
	}

	@Column(name="insurance_type")
	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	@Column(name="insurance_ref")
	public String getInsuranceRef() {
		return insuranceRef;
	}

	public void setInsuranceRef(String insuranceRef) {
		this.insuranceRef = insuranceRef;
	}
	@Column(name="finance_person")
	public String getFinancePerson() {
		return financePerson;
	}

	public void setFinancePerson(String financePerson) {
		this.financePerson = financePerson;
	}
	@Column(name="finance_email")
	public String getFinanceEmail() {
		return financeEmail;
	}

	public void setFinanceEmail(String financeEmail) {
		this.financeEmail = financeEmail;
	}
}
