package com.coreerp.model;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="user_tenant_relation", uniqueConstraints = {
	      @UniqueConstraint(name="UQ_UTR_USERNAME", columnNames={"username"})})
public class UserTenantRelation {

    private Integer id;
    private String username;
    private String tenant;
    private String email;
	private Date otpDate;	
	private Long numberOfOTP;
	private String name;
	private Date registrationDate;
	private Date createdDateTime;
	private String createdBy;
	private Date updatedDateTime;
	private String updatedBy;
	private Integer isActive;
	private Integer companyId;
	private Integer isPrimary;
	private Integer isPasswordResetRequired;
	private String companyName;
	private Party party;



	public UserTenantRelation() {
    }

    public UserTenantRelation(Integer id, String username, String tenant) {
        this.id = id;
        this.username = username;
        this.tenant = tenant;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name="username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name="tenant")
    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }
    
    @Column(name="otp_date")
    @Basic
    @Temporal(TemporalType.DATE)
	public Date getOtpDate() {
		return otpDate;
	}

	public void setOtpDate(Date otpDate) {
		this.otpDate = otpDate;
	}

	@Column(name="otp_number")
	public Long getNumberOfOTP() {
		return numberOfOTP != null ? numberOfOTP : 0;
	}

	public void setNumberOfOTP(Long numberOfOTP) {
		this.numberOfOTP = numberOfOTP;
	}
	
	@Column(name="email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="reg_date")
	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Column(name="created_date_time", updatable = false)
	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	@Column(name="created_by", updatable = false)	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="updated_date_time")
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "UserTenantRelation [id=" + id + ", username=" + username + ", tenant=" + tenant + ", email=" + email
				+ ", otpDate=" + otpDate + ", numberOfOTP=" + numberOfOTP + ", name=" + name + "]";
	}

	@Column(name="is_active")
	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	@Column(name="company_id")
	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	@Column(name="is_primary")
	public Integer getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Integer isPrimary) {
		this.isPrimary = isPrimary;
	}

	@Column(name="is_password_reset_required")
	public Integer getIsPasswordResetRequired() {
		return isPasswordResetRequired;
	}

	public void setIsPasswordResetRequired(Integer isPasswordResetRequired) {
		this.isPasswordResetRequired = isPasswordResetRequired;
	}

	@Column(name="company_name")
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@ManyToOne
	@JoinColumn(name = "party_id", foreignKey=@ForeignKey(name="FK_UTR_PARTY"))
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}
}
