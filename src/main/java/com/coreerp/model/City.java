package com.coreerp.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="ma_city")
@Where(clause = "deleted='N'")
public class City extends AuditModelAbstractEntity {

	private String name;
	
	private State state;
	
	private List<Area> areas;
	
	private String deleted="N";
	

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne
	@JoinColumn(name="state_id", foreignKey=@ForeignKey(name="FK_CITY_STATE"))
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@OneToMany(mappedBy="city", cascade=CascadeType.ALL)	
	@JsonIgnore
	public List<Area> getAreas() {
		return areas;
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}
	
	@Column(name="deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}
	
	
}
