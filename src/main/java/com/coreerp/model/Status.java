package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

@Entity
@Table(name="cd_status")
@Where(clause="deleted='N'")
public class Status extends AuditModelAbstractEntity {

	private String name;
	private TransactionType transactionType;
	private String deleted;
	
	@Column(name="name")
	@NotNull
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

	@ManyToOne
	@JoinColumn(name="transaction_type_id", foreignKey=@ForeignKey(name="FK_ST_TT"))
	public TransactionType getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
	@Column(name="deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "Status{" +
				"name='" + name + '\'' +
				", transactionType=" + transactionType +
				", deleted='" + deleted + '\'' +
				", id="+ getId() +
				'}';
	}
}
