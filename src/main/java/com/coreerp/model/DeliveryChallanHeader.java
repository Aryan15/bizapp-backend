package com.coreerp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_delivery_challan_header")
public class DeliveryChallanHeader extends AbstractTransactionModel{

	
	private String deliveryChallanNumber;
	
	private Long deliveryChallanId;
	
	private Date deliveryChallanDate;
	
	private String internalReferenceNumber;
	
	private Date internalReferenceDate;	
	
	private String purchaseOrderNumber;
	
	private Date purchaseOrderDate;
	
	private TransactionType deliveryChallanType;
	
	private Party party;
	
	private String officeAddress;
	
	private String deliveryAddress;
	
	private Date dispatchDate;
	
	private String inspectedBy;
	
	private Date inspectionDate;
	
	private String modeOfDispatch;
	
	private String vehicleNumber;
	
	private String deliveryNote;
	
	private Double totalQuantity;
	
	private Status status;
	
	//private String deliveryChallanStatus;
	
	private FinancialYear financialYear;
	
	private Company company;
	
	private String numberOfPackages;
	
	private String deliveryTerms;
	
	private String termsAndConditions;
	
	private String paymentTerms;
	
	private String dispatchTime;
	
	private Tax tax;
	
	private Long labourChargesOnly;
	
	private String inDeliveryChallanNumber;
	
	private Date inDeliveryChallanDate;
	
	private String returnableDeliveryChallan;
	
	private String nonReturnableDeliveryChallan;
	
	private String notForSale;
	
	private String forSale;
	
	private String returnForJobWork;
	
	private String modeOfDispatchStatus;
	
	private String vehicleNumberStatus;
	
	private String numberOfPackagesStatus;
	
	private String personName;
	
	private Double sgstTaxRate;
	
	private Double sgstTaxAmount;
	
	private Double cgstTaxRate;
	
	private Double cgstTaxAmount;
	
	private Double igstTaxRate;
	
	private Double igstTaxAmount;
	
	private Double taxAmount;
	
	private Double price;
	
	private Double amount;
	
	private String eWayBill;
	
	private Double balanceQuantity;
	
    private Double discountPercentage;
	    
	private Double discountAmount;
	    
    private Double amountAfterDiscount;  
    
    private String remarks;
    
    private Integer inclusiveTax;
    
    private String billToAddress;
    private String shipToAddress;
    private String gstNumber;
    
    private String companyName; 
    private Long companygGstRegistrationTypeId; //companyGstRegistrationnTypeId
    private String companyPinCode; //companyPinCode .. etc
    private Long companyStateId;
    private String companyStateName;
    private Long companyCountryId;
    private String companyPrimaryMobile;
    private String companySecondaryMobile;
    private String companyContactPersonNumber;
    private String companyContactPersonName;
    private String companyPrimaryTelephone;
    private String companySecondaryTelephone;
    private String companyWebsite;
    private String companyEmail;
    private String companyFaxNumber;
    private String companyAddress;
    private String companyTagLine;
    private String companyGstNumber;
    private String companyPanNumber;
    private String companyPanDate;
    private String companyCeritificateImagePath;
    private String companyLogoPath;
    
    private String partyName;
    private String partyContactPersonNumber;
    private String partyPinCode;
    private Long partyAreaId;
    private Long partyCityId;
    private Long partyStateId;
    private Long partyCountryId;
    private String partyPrimaryTelephone;
    private String partySecondaryTelephone;
    private String partyPrimaryMobile;
    private String partySecondaryMobile;
    private String partyEmail;
    private String partyWebsite;
    private String partyContactPersonName;
//    private String partyBillToAddress;
//    private String partyShipAddress;
    private String partyDueDaysLimit;
    private String partyGstRegistrationTypeId;
//    private String partyGstNumber;
    private String partyPanNumber;
    private String isIgst;
	private String jwNoteId;


	@Column(name = "remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	private List<DeliveryChallanItem> deliveryChallanItems;
	
	
	
	@Column(name="dc_number")
	public String getDeliveryChallanNumber() {
		return deliveryChallanNumber;
	}

	public void setDeliveryChallanNumber(String deliveryChallanNumber) {
		this.deliveryChallanNumber = deliveryChallanNumber;
	}
	
	@Column(name="dc_id")	
	public Long getDeliveryChallanId() {
		return deliveryChallanId;
	}

	public void setDeliveryChallanId(Long deliveryChallanId) {
		this.deliveryChallanId = deliveryChallanId;
	}
	
	@Column(name="dc_date")
	@NotNull
	public Date getDeliveryChallanDate() {
		return deliveryChallanDate;
	}

	public void setDeliveryChallanDate(Date deliveryChallanDate) {
		this.deliveryChallanDate = deliveryChallanDate;
	}

	@Column(name="int_ref_number")
	public String getInternalReferenceNumber() {
		return internalReferenceNumber;
	}

	public void setInternalReferenceNumber(String internalReferenceNumber) {
		this.internalReferenceNumber = internalReferenceNumber;
	}
	
	
	@Column(name="int_ref_date")
	public Date getInternalReferenceDate() {
		return internalReferenceDate;
	}

	public void setInternalReferenceDate(Date internalReferenceDate) {
		this.internalReferenceDate = internalReferenceDate;
	}



	@Column(name="po_number")
	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	@Column(name="po_date")
	public Date getPurchaseOrderDate() {
		return purchaseOrderDate;
	}

	public void setPurchaseOrderDate(Date purchaseOrderDate) {
		this.purchaseOrderDate = purchaseOrderDate;
	}
	
	@ManyToOne
	@JoinColumn(name="dc_type", foreignKey=@ForeignKey(name="FK_DCH_TT"))
	public TransactionType getDeliveryChallanType() {
		return deliveryChallanType;
	}

	public void setDeliveryChallanType(TransactionType deliveryChallanType) {
		this.deliveryChallanType = deliveryChallanType;
	}
	
	@ManyToOne
	@JoinColumn(name="party_id", foreignKey=@ForeignKey(name="FK_DCH_PARTY"))
	@NotNull
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@Column(name="office_address")
	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}

	@Column(name="delivery_address")
	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	@Column(name="dispatch_date")
	public Date getDispatchDate() {
		return dispatchDate;
	}

	public void setDispatchDate(Date dispatchDate) {
		this.dispatchDate = dispatchDate;
	}

	@Column(name="inspected_by")
	public String getInspectedBy() {
		return inspectedBy;
	}

	public void setInspectedBy(String inspectedBy) {
		this.inspectedBy = inspectedBy;
	}

	@Column(name="inspection_date")
	public Date getInspectionDate() {
		return inspectionDate;
	}

	public void setInspectionDate(Date inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	@Column(name="mode_of_dispatch")
	public String getModeOfDispatch() {
		return modeOfDispatch;
	}

	public void setModeOfDispatch(String modeOfDispatch) {
		this.modeOfDispatch = modeOfDispatch;
	}

	@Column(name="vehicle_number")
	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	@Column(name="delivery_note")
	public String getDeliveryNote() {
		return deliveryNote;
	}

	public void setDeliveryNote(String deliveryNote) {
		this.deliveryNote = deliveryNote;
	}

	@Column(name="total_quantity")
	public Double getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	@ManyToOne
	@JoinColumn(name="status_id",foreignKey=@ForeignKey(name="FK_DCH_ST"))
	@NotNull
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}



	@ManyToOne
	@JoinColumn(name="financial_year_id", foreignKey=@ForeignKey(name="FK_DCH_FY"))
	@NotNull
	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	@ManyToOne
	@JoinColumn(name="company_id", foreignKey=@ForeignKey(name="FK_DCH_COMPANY"))
	@NotNull
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Column(name="number_of_packages")
	public String getNumberOfPackages() {
		return numberOfPackages;
	}

	public void setNumberOfPackages(String numberOfPackages) {
		this.numberOfPackages = numberOfPackages;
	}

	@Column(name="delivery_terms")
	public String getDeliveryTerms() {
		return deliveryTerms;
	}

	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}

	@Column(name="terms_conditions")
	public String getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	@Column(name="payment_terms")
	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	@Column(name="dispatch_time")
	public String getDispatchTime() {
		return dispatchTime;
	}

	public void setDispatchTime(String dispatchTime) {
		this.dispatchTime = dispatchTime;
	}
	

	@ManyToOne	
	@JoinColumn(name="tax_id", foreignKey=@ForeignKey(name="FK_DCH_TAX"))
	public Tax getTax() {
		return tax;
	}

	public void setTax(Tax tax) {
		this.tax = tax;
	}

	@Column(name="labour_charges_only")
	public Long getLabourChargesOnly() {
		return labourChargesOnly;
	}

	public void setLabourChargesOnly(Long labourChargesOnly) {
		this.labourChargesOnly = labourChargesOnly;
	}

	@Column(name="in_dc_number")
	public String getInDeliveryChallanNumber() {
		return inDeliveryChallanNumber;
	}

	public void setInDeliveryChallanNumber(String inDeliveryChallanNumber) {
		this.inDeliveryChallanNumber = inDeliveryChallanNumber;
	}

	@Column(name="in_dc_date")
	public Date getInDeliveryChallanDate() {
		return inDeliveryChallanDate;
	}

	public void setInDeliveryChallanDate(Date inDeliveryChallanDate) {
		this.inDeliveryChallanDate = inDeliveryChallanDate;
	}

	@Column(name="returnable_dc")
	public String getReturnableDeliveryChallan() {
		return returnableDeliveryChallan;
	}

	public void setReturnableDeliveryChallan(String returnableDeliveryChallan) {
		this.returnableDeliveryChallan = returnableDeliveryChallan;
	}

	@Column(name="non_returnable_dc")
	public String getNonReturnableDeliveryChallan() {
		return nonReturnableDeliveryChallan;
	}

	public void setNonReturnableDeliveryChallan(String nonReturnableDeliveryChallan) {
		this.nonReturnableDeliveryChallan = nonReturnableDeliveryChallan;
	}

	@Column(name="not_for_sale")
	public String getNotForSale() {
		return notForSale;
	}

	public void setNotForSale(String notForSale) {
		this.notForSale = notForSale;
	}

	@Column(name="for_sale")
	public String getForSale() {
		return forSale;
	}

	public void setForSale(String forSale) {
		this.forSale = forSale;
	}

	@Column(name="return_for_jobwork")
	public String getReturnForJobWork() {
		return returnForJobWork;
	}

	public void setReturnForJobWork(String returnForJobWork) {
		this.returnForJobWork = returnForJobWork;
	}

	@Column(name="mode_of_dispatch_status")
	public String getModeOfDispatchStatus() {
		return modeOfDispatchStatus;
	}

	public void setModeOfDispatchStatus(String modeOfDispatchStatus) {
		this.modeOfDispatchStatus = modeOfDispatchStatus;
	}

	@Column(name="vehicle_no_status")
	public String getVehicleNumberStatus() {
		return vehicleNumberStatus;
	}

	public void setVehicleNumberStatus(String vehicleNumberStatus) {
		this.vehicleNumberStatus = vehicleNumberStatus;
	}

	@Column(name="no_of_packages_status")
	public String getNumberOfPackagesStatus() {
		return numberOfPackagesStatus;
	}

	public void setNumberOfPackagesStatus(String numberOfPackagesStatus) {
		this.numberOfPackagesStatus = numberOfPackagesStatus;
	}

	@Column(name="person_name")
	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	@Column(name="sgst_tax_rate")
	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}

	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	@Column(name="sgst_tax_amt")
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}

	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}

	@Column(name="cgst_tax_rate")
	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}

	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}

	@Column(name="cgst_tax_amount")
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}

	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}

	@Column(name="igst_tax_rate")
	public Double getIgstTaxRate() {
		return igstTaxRate;
	}

	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}

	@Column(name="igst_tax_amt")
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}

	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	@Column(name="tax_amt")
	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}


	@OneToMany(mappedBy="deliveryChallanHeader", cascade=CascadeType.ALL)
	@NotNull
	@OrderBy("slNo ASC")
	public List<DeliveryChallanItem> getDeliveryChallanItems() {
		return deliveryChallanItems;
	}

	public void setDeliveryChallanItems(List<DeliveryChallanItem> deliveryChallanItems) {
		this.deliveryChallanItems = deliveryChallanItems;
	}

	@Column(name="price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name="amt")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name="e_way_bill")
	public String geteWayBill() {
		return eWayBill;
	}

	public void seteWayBill(String eWayBill) {
		this.eWayBill = eWayBill;
	}

	@Column(name="bal_qty")
	public Double getBalanceQuantity() {
		return balanceQuantity;
	}

	public void setBalanceQuantity(Double balanceQuantity) {
		this.balanceQuantity = balanceQuantity;
	}

	@Column(name="disc_per")
	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	@Column(name="disc_amt")
	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	@Column(name="amt_after_disc")
	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}

	public void setAmountAfterDiscount(Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}

	@Column(name="inclusive_tax")
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}

	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	@Override
	public String toString() {
		return "DeliveryChallanHeader [deliveryChallanNumber=" + deliveryChallanNumber + ", deliveryChallanId="
				+ deliveryChallanId + ", deliveryChallanDate=" + deliveryChallanDate + ", gstNumber=" + gstNumber + ",billToAddress=" + billToAddress + ", shipToAddress=" + shipToAddress + "]";
	}

	@Column(name="bill_to_address")
	public String getBillToAddress() {
		return billToAddress;
	}

	public void setBillToAddress(String billToAddress) {
		this.billToAddress = billToAddress;
	}

	@Column(name="ship_address")
	public String getShipToAddress() {
		return shipToAddress;
	}

	public void setShipToAddress(String shipToAddress) {
		this.shipToAddress = shipToAddress;
	}

	@Column(name="gst_number")
	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	//............................
	
	
		@Column(name="company_name")
		public String getCompanyName() {
			return companyName;
		}



		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}
		
		@Column(name="comp_gst_registration_type_id")
		public Long getCompanygGstRegistrationTypeId() {
			return companygGstRegistrationTypeId;
		}



		public void setCompanygGstRegistrationTypeId(Long companygGstRegistrationTypeId) {
			this.companygGstRegistrationTypeId = companygGstRegistrationTypeId;
		}


		@Column(name="comp_pincode")
		public String getCompanyPinCode() {
			return companyPinCode;
		}



		public void setCompanyPinCode(String companyPinCode) {
			this.companyPinCode = companyPinCode;
		}


		@Column(name="comp_state_id")
		public Long getCompanyStateId() {
			return companyStateId;
		}



		public void setCompanyStateId(Long companyStateId) {
			this.companyStateId = companyStateId;
		}
		
		@Column(name="comp_state_name")
		public String getCompanyStateName() {
			return companyStateName;
		}

		public void setCompanyStateName(String companyStateName) {
			this.companyStateName = companyStateName;
		}


		@Column(name="comp_country_id")
		public Long getCompanyCountryId() {
			return companyCountryId;
		}



		public void setCompanyCountryId(Long companyCountryId) {
			this.companyCountryId = companyCountryId;
		}


		@Column(name="comp_primary_mobile")
		public String getCompanyPrimaryMobile() {
			return companyPrimaryMobile;
		}



		public void setCompanyPrimaryMobile(String companyPrimaryMobile) {
			this.companyPrimaryMobile = companyPrimaryMobile;
		}


		@Column(name="comp_secondary_mobile")
		public String getCompanySecondaryMobile() {
			return companySecondaryMobile;
		}



		public void setCompanySecondaryMobile(String companySecondaryMobile) {
			this.companySecondaryMobile = companySecondaryMobile;
		}


		@Column(name="comp_contact_person_number")
		public String getCompanyContactPersonNumber() {
			return companyContactPersonNumber;
		}



		public void setCompanyContactPersonNumber(String companyContactPersonNumber) {
			this.companyContactPersonNumber = companyContactPersonNumber;
		}


		@Column(name="comp_contact_person_name")
		public String getCompanyContactPersonName() {
			return companyContactPersonName;
		}



		public void setCompanyContactPersonName(String companyContactPersonName) {
			this.companyContactPersonName = companyContactPersonName;
		}


		@Column(name="comp_primary_telephone")
		public String getCompanyPrimaryTelephone() {
			return companyPrimaryTelephone;
		}



		public void setCompanyPrimaryTelephone(String companyPrimaryTelephone) {
			this.companyPrimaryTelephone = companyPrimaryTelephone;
		}


		@Column(name="comp_secondary_telephone")
		public String getCompanySecondaryTelephone() {
			return companySecondaryTelephone;
		}



		public void setCompanySecondaryTelephone(String companySecondaryTelephone) {
			this.companySecondaryTelephone = companySecondaryTelephone;
		}


		@Column(name="comp_website")
		public String getCompanyWebsite() {
			return companyWebsite;
		}



		public void setCompanyWebsite(String companyWebsite) {
			this.companyWebsite = companyWebsite;
		}


		@Column(name="comp_email")
		public String getCompanyEmail() {
			return companyEmail;
		}



		public void setCompanyEmail(String companyEmail) {
			this.companyEmail = companyEmail;
		}


		@Column(name="comp_fax_number")
		public String getCompanyFaxNumber() {
			return companyFaxNumber;
		}



		public void setCompanyFaxNumber(String companyFaxNumber) {
			this.companyFaxNumber = companyFaxNumber;
		}


		@Column(name="comp_address")
		public String getCompanyAddress() {
			return companyAddress;
		}



		public void setCompanyAddress(String companyAddress) {
			this.companyAddress = companyAddress;
		}

	   @Column(name="comp_tagline")
	   public String getCompanyTagLine() {
		return companyTagLine;
	   }

	   public void setCompanyTagLine(String companyTagLine) {
		this.companyTagLine = companyTagLine;
	   }




		@Column(name="comp_gst_number")
		public String getCompanyGstNumber() {
			return companyGstNumber;
		}



		public void setCompanyGstNumber(String companyGstNumber) {
			this.companyGstNumber = companyGstNumber;
		}




	   @Column(name="comp_pan_number")
	   public String getCompanyPanNumber() {
		return companyPanNumber;
	   }

	   public void setCompanyPanNumber(String companyPanNumber) {
		this.companyPanNumber = companyPanNumber;
	   }

		@Column(name="comp_pan_date")
		public String getCompanyPanDate() {
			return companyPanDate;
		}



		public void setCompanyPanDate(String companyPanDate) {
			this.companyPanDate = companyPanDate;
		}


		@Column(name="comp_certificate_image_path")
		public String getCompanyCeritificateImagePath() {
			return companyCeritificateImagePath;
		}



		public void setCompanyCeritificateImagePath(String companyCeritificateImagePath) {
			this.companyCeritificateImagePath = companyCeritificateImagePath;
		}


		@Column(name="comp_logo_path")
		public String getCompanyLogoPath() {
			return companyLogoPath;
		}



		public void setCompanyLogoPath(String companyLogoPath) {
			this.companyLogoPath = companyLogoPath;
		}
		
		@Column(name="party_name")
		public String getPartyName() {
			return partyName;
		}



		public void setPartyName(String partyName) {
			this.partyName = partyName;
		}


		@Column(name="party_contact_person_number")
		public String getPartyContactPersonNumber() {
			return partyContactPersonNumber;
		}



		public void setPartyContactPersonNumber(String partyContactPersonNumber) {
			this.partyContactPersonNumber = partyContactPersonNumber;
		}


		@Column(name="party_pin_code")
		public String getPartyPinCode() {
			return partyPinCode;
		}



		public void setPartyPinCode(String partyPinCode) {
			this.partyPinCode = partyPinCode;
		}


		@Column(name="party_area_id")
		public Long getPartyAreaId() {
			return partyAreaId;
		}



		public void setPartyAreaId(Long partyAreaId) {
			this.partyAreaId = partyAreaId;
		}


		@Column(name="party_city_id")
		public Long getPartyCityId() {
			return partyCityId;
		}



		public void setPartyCityId(Long partyCityId) {
			this.partyCityId = partyCityId;
		}


		@Column(name="party_state_id")
		public Long getPartyStateId() {
			return partyStateId;
		}



		public void setPartyStateId(Long partyStateId) {
			this.partyStateId = partyStateId;
		}


		@Column(name="party_country_id")
		public Long getPartyCountryId() {
			return partyCountryId;
		}



		public void setPartyCountryId(Long partyCountryId) {
			this.partyCountryId = partyCountryId;
		}


		@Column(name="party_primary_telephone")
		public String getPartyPrimaryTelephone() {
			return partyPrimaryTelephone;
		}



		public void setPartyPrimaryTelephone(String partyPrimaryTelephone) {
			this.partyPrimaryTelephone = partyPrimaryTelephone;
		}


		@Column(name="party_secondary_telephone")
		public String getPartySecondaryTelephone() {
			return partySecondaryTelephone;
		}


		public void setPartySecondaryTelephone(String partySecondaryTelephone) {
			this.partySecondaryTelephone = partySecondaryTelephone;
		}


		@Column(name="party_primary_mobile_number")
		public String getPartyPrimaryMobile() {
			return partyPrimaryMobile;
		}



		public void setPartyPrimaryMobile(String partyPrimaryMobile) {
			this.partyPrimaryMobile = partyPrimaryMobile;
		}


		@Column(name="party_secondary_mobile")
		public String getPartySecondaryMobile() {
			return partySecondaryMobile;
		}



		public void setPartySecondaryMobile(String partySecondaryMobile) {
			this.partySecondaryMobile = partySecondaryMobile;
		}


		@Column(name="party_email")
		public String getPartyEmail() {
			return partyEmail;
		}



		public void setPartyEmail(String partyEmail) {
			this.partyEmail = partyEmail;
		}


		@Column(name="party_website")
		public String getPartyWebsite() {
			return partyWebsite;
		}



		public void setPartyWebsite(String partyWebsite) {
			this.partyWebsite = partyWebsite;
		}


		@Column(name="party_contact_person_name")
		public String getPartyContactPersonName() {
			return partyContactPersonName;
		}



		public void setPartyContactPersonName(String partyContactPersonName) {
			this.partyContactPersonName = partyContactPersonName;
		}


//		@Column(name="party_bill_to_address")
//		public String getPartyBillToAddress() {
//			return partyBillToAddress;
//		}
	//
	//
	//
//		public void setPartyBillToAddress(String partyBillToAddress) {
//			this.partyBillToAddress = partyBillToAddress;
//		}
	//
	//
//		@Column(name="party_ship_to_address")
//		public String getPartyShipAddress() {
//			return partyShipAddress;
//		}
	//
	//
	//
//		public void setPartyShipAddress(String partyShipAddress) {
//			this.partyShipAddress = partyShipAddress;
//		}


		@Column(name="party_due_days_limit")
		public String getPartyDueDaysLimit() {
			return partyDueDaysLimit;
		}



		public void setPartyDueDaysLimit(String partyDueDaysLimit) {
			this.partyDueDaysLimit = partyDueDaysLimit;
		}


		@Column(name="party_gst_registration_type_id")
		public String getPartyGstRegistrationTypeId() {
			return partyGstRegistrationTypeId;
		}



		public void setPartyGstRegistrationTypeId(String partyGstRegistrationTypeId) {
			this.partyGstRegistrationTypeId = partyGstRegistrationTypeId;
		}


//		@Column(name="party_gst_number")
//		public String getPartyGstNumber() {
//			return partyGstNumber;
//		}
	//
	//
	//
//		public void setPartyGstNumber(String partyGstNumber) {
//			this.partyGstNumber = partyGstNumber;
//		}


		@Column(name="party_pan_number")
		public String getPartyPanNumber() {
			return partyPanNumber;
		}



		public void setPartyPanNumber(String partyPanNumber) {
			this.partyPanNumber = partyPanNumber;
		}


		@Column(name="is_igst")
		public String getIsIgst() {
			return isIgst;
		}



		public void setIsIgst(String isIgst) {
			this.isIgst = isIgst;
		}

	@Column(name="jw_note_Id")
	public String getJwNoteId() {
		return jwNoteId;
	}

	public void setJwNoteId(String jwNoteId) {
		this.jwNoteId = jwNoteId;
	}
}
