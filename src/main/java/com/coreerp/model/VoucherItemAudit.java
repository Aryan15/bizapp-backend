package com.coreerp.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name="tr_voucher_item_audit")
public class VoucherItemAudit {

    private String id;

    private Date createdDateTime;

    private String createdBy;

    private Date updatedDateTime;

    private String updatedBy;

    private String voucherHeaderId;

    private String description;

    private Double amount;

    private Long expenseHeaderId;

    @Id
    @Column(name="id", length=255)
    @Size(min=1)
    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }
    @Column(name="created_date_time",nullable = false, updatable = false)
    public Date getCreatedDateTime() {
        return createdDateTime;
    }
    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Column(name="created_by",nullable = false, updatable = false)
    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name="updated_date_time")
    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }
    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    @Column(name="updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }


    @Column(name="voucher_header_id")
    public String getVoucherHeaderId() {
        return voucherHeaderId;
    }

    public void setVoucherHeaderId(String voucherHeaderId) {
        this.voucherHeaderId = voucherHeaderId;
    }

    @Column(name="description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name="amount")
    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Column(name="expense_header_id")
    public Long getExpenseHeaderId() {
        return expenseHeaderId;
    }

    public void setExpenseHeaderId(Long  expenseHeaderId) {
        this.expenseHeaderId = expenseHeaderId;
    }


}
