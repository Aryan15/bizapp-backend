package com.coreerp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_payable_receivable_header")
public class PayableReceivableHeader extends AbstractTransactionModel{

	private String payReferenceNumber;
	
	private Long payReferenceId;
	
	private Date payReferenceDate;
	
	private Party party;
	
	private PaymentMethod paymentMethod;
	
	private CardType cardType;
	
	private String paymentDocumentNumber;
	
	private String bankName;
	
	private Date paymentDocumentDate;
	
	private String paymentNote;
	
	private String clearingMode;
	
	private String paymentStatus;
	
	private Status status;
	
	private FinancialYear financialYear;
	
	private Long companyId;
	
	private TransactionType transactionType;
	
	private Double paymentAmount;
	
	private Integer isPost;
	
	private List<PayableReceivableItem> payableReceivableItems;
	
	private String creditDebitNumber;
	
	private String creditDebitId;
	
	
    private String companyName; 
    private Long companygGstRegistrationTypeId; //companyGstRegistrationnTypeId
    private String companyPinCode; //companyPinCode .. etc
    private Long companyStateId;
    private String companyStateName;
    private Long companyCountryId;
    private String companyPrimaryMobile;
    private String companySecondaryMobile;
    private String companyContactPersonNumber;
    private String companyContactPersonName;
    private String companyPrimaryTelephone;
    private String companySecondaryTelephone;
    private String companyWebsite;
    private String companyEmail;
    private String companyFaxNumber;
    private String companyAddress;
    private String companyTagLine;
    private String companyGstNumber;
    private String companyPanNumber;
    private String companyPanDate;
    private String companyCeritificateImagePath;
    private String companyLogoPath;
    
    private String partyName;
    private String partyContactPersonNumber;
    private String partyPinCode;
    private Long partyAreaId;
    private Long partyCityId;
    private Long partyStateId;
    private Long partyCountryId;
    private String partyPrimaryTelephone;
    private String partySecondaryTelephone;
    private String partyPrimaryMobile;
    private String partySecondaryMobile;
    private String partyEmail;
    private String partyWebsite;
    private String partyContactPersonName;
    private String partyBillToAddress;
    private String partyShipAddress;
    private String partyDueDaysLimit;
    private String partyGstRegistrationTypeId;
    private String partyGstNumber;
    private String partyPanNumber;
    private String isIgst;

	@Column(name="pay_ref_num")
	public String getPayReferenceNumber() {
		return payReferenceNumber;
	}

	public void setPayReferenceNumber(String payReferenceNumber) {
		this.payReferenceNumber = payReferenceNumber;
	}

	@Column(name="pay_ref_id")
	public Long getPayReferenceId() {
		return payReferenceId;
	}

	public void setPayReferenceId(Long payReferenceId) {
		this.payReferenceId = payReferenceId;
	}
	@Column(name="pay_ref_date")
	public Date getPayReferenceDate() {
		return payReferenceDate;
	}

	public void setPayReferenceDate(Date payReferenceDate) {
		this.payReferenceDate = payReferenceDate;
	}
//	@Column(name="user_id")
//	public Long getUserId() {
//		return userId;
//	}
//
//	public void setUserId(Long userId) {
//		this.userId = userId;
//	}
	
	@ManyToOne
	@JoinColumn(name="party_id", foreignKey=@ForeignKey(name="FK_PRH_PARTY"))
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}


	@ManyToOne
	@JoinColumn(name="payment_method_id", foreignKey=@ForeignKey(name="FK_PRH_PMETH"))
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	@ManyToOne
	@JoinColumn(name="card_type_id", foreignKey=@ForeignKey(name="FK_PRH_CTYPE"))
	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}
	


	@Column(name="pay_doc_num")
	public String getPaymentDocumentNumber() {
		return paymentDocumentNumber;
	}



	public void setPaymentDocumentNumber(String paymentDocumentNumber) {
		this.paymentDocumentNumber = paymentDocumentNumber;
	}

	@Column(name="bank_name")
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Column(name="pay_doc_date")
	public Date getPaymentDocumentDate() {
		return paymentDocumentDate;
	}

	public void setPaymentDocumentDate(Date paymentDocumentDate) {
		this.paymentDocumentDate = paymentDocumentDate;
	}

	@Column(name="payment_note")
	public String getPaymentNote() {
		return paymentNote;
	}

	public void setPaymentNote(String paymentNote) {
		this.paymentNote = paymentNote;
	}

	@Column(name="clearing_mode")
	public String getClearingMode() {
		return clearingMode;
	}

	public void setClearingMode(String clearingMode) {
		this.clearingMode = clearingMode;
	}

	@Column(name="payment_status")
	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	@ManyToOne
	@JoinColumn(name="status_id",foreignKey=@ForeignKey(name="FK_PRH_ST"))
	@NotNull
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@ManyToOne
	@JoinColumn(name="financial_year_id", foreignKey=@ForeignKey(name="FK_PRH_FY"))
	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	@Column(name="company_id")
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
	@ManyToOne
	@JoinColumn(name="transaction_type", foreignKey=@ForeignKey(name="FK_PRH_TT"))
	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	@Column(name="payment_amount")
	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	@Column(name="is_post")
	public Integer getIsPost() {
		return isPost;
	}

	public void setIsPost(Integer isPost) {
		this.isPost = isPost;
	}

	@OneToMany(mappedBy = "payableReceivableHeader", cascade = CascadeType.ALL)
	@NotNull
	@OrderBy("slNo ASC")
	public List<PayableReceivableItem> getPayableReceivableItems() {
		return payableReceivableItems;
	}

	public void setPayableReceivableItems(List<PayableReceivableItem> payableReceivableItems) {
		this.payableReceivableItems = payableReceivableItems;
	}

	@Override
	public String toString() {
		return "PayableReceivableHeader [payReferenceNumber=" + payReferenceNumber + ", payReferenceId="
				+ payReferenceId + ", payReferenceDate=" + payReferenceDate + ", party=" + party + ", paymentMethod="
				+ paymentMethod + ", cardType=" + cardType + ", paymentDocumentNumber=" + paymentDocumentNumber
				+ ", bankName=" + bankName + ", paymentDocumentDate=" + paymentDocumentDate + ", paymentNote="
				+ paymentNote + ", clearingMode=" + clearingMode + ", paymentStatus=" + paymentStatus + ", status="
				+ status + ", financialYear=" + financialYear + ", companyId=" + companyId + ", transactionType="
				+ transactionType + ", paymentAmount=" + paymentAmount + ", isPost=" + isPost
				+ ", payableReceivableItems=" + payableReceivableItems + "]";
	}

	@Column(name="credit_debit_number")
	public String getCreditDebitNumber() {
		return creditDebitNumber;
	}

	public void setCreditDebitNumber(String creditDebitNumber) {
		this.creditDebitNumber = creditDebitNumber;
	}

	@Column(name="credit_debit_id")
	public String getCreditDebitId() {
		return creditDebitId;
	}

	public void setCreditDebitId(String creditDebitId) {
		this.creditDebitId = creditDebitId;
	}

	//............................
	
	
		@Column(name="company_name")
		public String getCompanyName() {
			return companyName;
		}



		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}
		
		@Column(name="comp_gst_registration_type_id")
		public Long getCompanygGstRegistrationTypeId() {
			return companygGstRegistrationTypeId;
		}



		public void setCompanygGstRegistrationTypeId(Long companygGstRegistrationTypeId) {
			this.companygGstRegistrationTypeId = companygGstRegistrationTypeId;
		}


		@Column(name="comp_pincode")
		public String getCompanyPinCode() {
			return companyPinCode;
		}



		public void setCompanyPinCode(String companyPinCode) {
			this.companyPinCode = companyPinCode;
		}


		@Column(name="comp_state_id")
		public Long getCompanyStateId() {
			return companyStateId;
		}



		public void setCompanyStateId(Long companyStateId) {
			this.companyStateId = companyStateId;
		}

		@Column(name="comp_state_name")
		public String getCompanyStateName() {
			return companyStateName;
		}

		public void setCompanyStateName(String companyStateName) {
			this.companyStateName = companyStateName;
		}

		@Column(name="comp_country_id")
		public Long getCompanyCountryId() {
			return companyCountryId;
		}



		public void setCompanyCountryId(Long companyCountryId) {
			this.companyCountryId = companyCountryId;
		}


		@Column(name="comp_primary_mobile")
		public String getCompanyPrimaryMobile() {
			return companyPrimaryMobile;
		}



		public void setCompanyPrimaryMobile(String companyPrimaryMobile) {
			this.companyPrimaryMobile = companyPrimaryMobile;
		}


		@Column(name="comp_secondary_mobile")
		public String getCompanySecondaryMobile() {
			return companySecondaryMobile;
		}



		public void setCompanySecondaryMobile(String companySecondaryMobile) {
			this.companySecondaryMobile = companySecondaryMobile;
		}


		@Column(name="comp_contact_person_number")
		public String getCompanyContactPersonNumber() {
			return companyContactPersonNumber;
		}



		public void setCompanyContactPersonNumber(String companyContactPersonNumber) {
			this.companyContactPersonNumber = companyContactPersonNumber;
		}


		@Column(name="comp_contact_person_name")
		public String getCompanyContactPersonName() {
			return companyContactPersonName;
		}



		public void setCompanyContactPersonName(String companyContactPersonName) {
			this.companyContactPersonName = companyContactPersonName;
		}


		@Column(name="comp_primary_telephone")
		public String getCompanyPrimaryTelephone() {
			return companyPrimaryTelephone;
		}



		public void setCompanyPrimaryTelephone(String companyPrimaryTelephone) {
			this.companyPrimaryTelephone = companyPrimaryTelephone;
		}


		@Column(name="comp_secondary_telephone")
		public String getCompanySecondaryTelephone() {
			return companySecondaryTelephone;
		}



		public void setCompanySecondaryTelephone(String companySecondaryTelephone) {
			this.companySecondaryTelephone = companySecondaryTelephone;
		}


		@Column(name="comp_website")
		public String getCompanyWebsite() {
			return companyWebsite;
		}



		public void setCompanyWebsite(String companyWebsite) {
			this.companyWebsite = companyWebsite;
		}


		@Column(name="comp_email")
		public String getCompanyEmail() {
			return companyEmail;
		}



		public void setCompanyEmail(String companyEmail) {
			this.companyEmail = companyEmail;
		}


		@Column(name="comp_fax_number")
		public String getCompanyFaxNumber() {
			return companyFaxNumber;
		}



		public void setCompanyFaxNumber(String companyFaxNumber) {
			this.companyFaxNumber = companyFaxNumber;
		}


		@Column(name="comp_address")
		public String getCompanyAddress() {
			return companyAddress;
		}



		public void setCompanyAddress(String companyAddress) {
			this.companyAddress = companyAddress;
		}

	    @Column(name="comp_tagline")
	    public String getCompanyTagLine() {
		return companyTagLine;
	   }

	   public void setCompanyTagLine(String companyTagLine) {
		this.companyTagLine = companyTagLine;
	   }





		@Column(name="comp_gst_number")
		public String getCompanyGstNumber() {
			return companyGstNumber;
		}



		public void setCompanyGstNumber(String companyGstNumber) {
			this.companyGstNumber = companyGstNumber;
		}

	    @Column(name="comp_pan_number")
	    public String getCompanyPanNumber() {
		return companyPanNumber;
	    }

	    public void setCompanyPanNumber(String companyPanNumber) {
		this.companyPanNumber = companyPanNumber;
	    }



		@Column(name="comp_pan_date")
		public String getCompanyPanDate() {
			return companyPanDate;
		}



		public void setCompanyPanDate(String companyPanDate) {
			this.companyPanDate = companyPanDate;
		}


		@Column(name="comp_certificate_image_path")
		public String getCompanyCeritificateImagePath() {
			return companyCeritificateImagePath;
		}



		public void setCompanyCeritificateImagePath(String companyCeritificateImagePath) {
			this.companyCeritificateImagePath = companyCeritificateImagePath;
		}


		@Column(name="comp_logo_path")
		public String getCompanyLogoPath() {
			return companyLogoPath;
		}



		public void setCompanyLogoPath(String companyLogoPath) {
			this.companyLogoPath = companyLogoPath;
		}
		
		@Column(name="party_name")
		public String getPartyName() {
			return partyName;
		}



		public void setPartyName(String partyName) {
			this.partyName = partyName;
		}


		@Column(name="party_contact_person_number")
		public String getPartyContactPersonNumber() {
			return partyContactPersonNumber;
		}



		public void setPartyContactPersonNumber(String partyContactPersonNumber) {
			this.partyContactPersonNumber = partyContactPersonNumber;
		}


		@Column(name="party_pin_code")
		public String getPartyPinCode() {
			return partyPinCode;
		}



		public void setPartyPinCode(String partyPinCode) {
			this.partyPinCode = partyPinCode;
		}


		@Column(name="party_area_id")
		public Long getPartyAreaId() {
			return partyAreaId;
		}



		public void setPartyAreaId(Long partyAreaId) {
			this.partyAreaId = partyAreaId;
		}


		@Column(name="party_city_id")
		public Long getPartyCityId() {
			return partyCityId;
		}



		public void setPartyCityId(Long partyCityId) {
			this.partyCityId = partyCityId;
		}


		@Column(name="party_state_id")
		public Long getPartyStateId() {
			return partyStateId;
		}



		public void setPartyStateId(Long partyStateId) {
			this.partyStateId = partyStateId;
		}


		@Column(name="party_country_id")
		public Long getPartyCountryId() {
			return partyCountryId;
		}



		public void setPartyCountryId(Long partyCountryId) {
			this.partyCountryId = partyCountryId;
		}


		@Column(name="party_primary_telephone")
		public String getPartyPrimaryTelephone() {
			return partyPrimaryTelephone;
		}



		public void setPartyPrimaryTelephone(String partyPrimaryTelephone) {
			this.partyPrimaryTelephone = partyPrimaryTelephone;
		}


		@Column(name="party_secondary_telephone")
		public String getPartySecondaryTelephone() {
			return partySecondaryTelephone;
		}


		public void setPartySecondaryTelephone(String partySecondaryTelephone) {
			this.partySecondaryTelephone = partySecondaryTelephone;
		}


		@Column(name="party_primary_mobile_number")
		public String getPartyPrimaryMobile() {
			return partyPrimaryMobile;
		}



		public void setPartyPrimaryMobile(String partyPrimaryMobile) {
			this.partyPrimaryMobile = partyPrimaryMobile;
		}


		@Column(name="party_secondary_mobile")
		public String getPartySecondaryMobile() {
			return partySecondaryMobile;
		}



		public void setPartySecondaryMobile(String partySecondaryMobile) {
			this.partySecondaryMobile = partySecondaryMobile;
		}


		@Column(name="party_email")
		public String getPartyEmail() {
			return partyEmail;
		}



		public void setPartyEmail(String partyEmail) {
			this.partyEmail = partyEmail;
		}


		@Column(name="party_website")
		public String getPartyWebsite() {
			return partyWebsite;
		}



		public void setPartyWebsite(String partyWebsite) {
			this.partyWebsite = partyWebsite;
		}


		@Column(name="party_contact_person_name")
		public String getPartyContactPersonName() {
			return partyContactPersonName;
		}



		public void setPartyContactPersonName(String partyContactPersonName) {
			this.partyContactPersonName = partyContactPersonName;
		}


		@Column(name="party_due_days_limit")
		public String getPartyDueDaysLimit() {
			return partyDueDaysLimit;
		}



		public void setPartyDueDaysLimit(String partyDueDaysLimit) {
			this.partyDueDaysLimit = partyDueDaysLimit;
		}


		@Column(name="party_gst_registration_type_id")
		public String getPartyGstRegistrationTypeId() {
			return partyGstRegistrationTypeId;
		}



		public void setPartyGstRegistrationTypeId(String partyGstRegistrationTypeId) {
			this.partyGstRegistrationTypeId = partyGstRegistrationTypeId;
		}


		@Column(name="party_pan_number")
		public String getPartyPanNumber() {
			return partyPanNumber;
		}



		public void setPartyPanNumber(String partyPanNumber) {
			this.partyPanNumber = partyPanNumber;
		}


		@Column(name="is_igst")
		public String getIsIgst() {
			return isIgst;
		}



		public void setIsIgst(String isIgst) {
			this.isIgst = isIgst;
		}

		@Column(name="party_bill_to_address")
		public String getPartyBillToAddress() {
			return partyBillToAddress;
		}

		public void setPartyBillToAddress(String partyBillToAddress) {
			this.partyBillToAddress = partyBillToAddress;
		}

		@Column(name="party_ship_to_address")
		public String getPartyShipAddress() {
			return partyShipAddress;
		}

		public void setPartyShipAddress(String partyShipAddress) {
			this.partyShipAddress = partyShipAddress;
		}

		@Column(name="party_gst_number")
		public String getPartyGstNumber() {
			return partyGstNumber;
		}

		public void setPartyGstNumber(String partyGstNumber) {
			this.partyGstNumber = partyGstNumber;
		}
	
		
	
	
}
