package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

@Entity
@Table(name="ma_address")
@Where(clause = "deleted='N'")
public class Address extends AuditModelAbstractEntity{
	

	private String address;
	
	private String contactPersonName;

	private String contactPersonNumber;

	private String contactPersonDesignation;

	private String deleted = "N";

	@Column(name ="address", length=5000)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name ="contact_person_name", length=60)
	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	@Column(name ="contact_person_number")
	public String getContactPersonNumber() {
		return contactPersonNumber;
	}

	public void setContactPersonNumber(String contactPersonNumber) {
		this.contactPersonNumber = contactPersonNumber;
	}

	@Column(name ="contact_person_designation")
	public String getContactPersonDesignation() {
		return contactPersonDesignation;
	}

	public void setContactPersonDesignation(String contactPersonDesignation) {
		this.contactPersonDesignation = contactPersonDesignation;
	}
	
	@Column(name ="deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}
	
	@Override
	public String toString() {
		return "Address [address=" + address + ", contactPersonName=" + contactPersonName + ", contactPersonNumber="
				+ contactPersonNumber + ", contactPersonDesignation=" + contactPersonDesignation + ", deleted="
				+ deleted + "]";
	}
	
	
}
