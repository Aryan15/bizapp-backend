package com.coreerp.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="ma_expense", uniqueConstraints = {
	      @UniqueConstraint(name="UQ_EXPENSE_NAME", columnNames={"name"})})
@Where(clause = "deleted='N'")
public class ExpenseHeader extends AuditModelAbstractEntity {
	
	private String deleted="N";
	private String name;

	@Column(name="deleted")
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		if(deleted != null){
		this.deleted = deleted;
		}
	}
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
