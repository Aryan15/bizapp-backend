package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "ma_role")
public class Role extends AuditModelAbstractEntity {



	private String role;
	private RoleType roleType;
	
	@Column(name="role")
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	@ManyToOne
	@JoinColumn(name="role_type_id", foreignKey=@ForeignKey(name="FK_ROLE_TYPE"))
	@NotNull
	public RoleType getRoleType() {
		return roleType;
	}
	public void setRoleType(RoleType roleType) {
		this.roleType = roleType;
	}
	
}
