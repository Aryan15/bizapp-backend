package com.coreerp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_stock_trace")
public class StockTrace extends AbstractTransactionModel{
	
	private Date businessDate;
	
	private Date transactionDate;
	
	private Material material;
	
	private TransactionType transactionType;
	
	private String transactionHeaderId;
	
	private String transactionId;
	
	private Integer transactionIdSequence;
	
	private String transactionNote;
	
	private Party party;
	
	private Double quantityIn;
	
	private Double quantityOut;
	
	private String reverse;
	
	private String remarks;
	
	private String transactionNumber;

	@Column(name="bus_date")
	@NotNull
	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	@Column(name="trans_date")
	@NotNull
	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	@ManyToOne
	@JoinColumn(name="material_id", foreignKey=@ForeignKey(name="FK_STTR_MAT"))
	@NotNull
	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	@ManyToOne
	@JoinColumn(name="trans_type_id", foreignKey=@ForeignKey(name="FK_STTR_TT"))
	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	
	@Column(name="trans_header_id")
	@NotNull
	public String getTransactionHeaderId() {
		return transactionHeaderId;
	}

	public void setTransactionHeaderId(String transactionHeaderId) {
		this.transactionHeaderId = transactionHeaderId;
	}

	@Column(name="trans_id")
	@NotNull
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Column(name="trans_id_seq")
	public Integer getTransactionIdSequence() {
		return transactionIdSequence;
	}

	public void setTransactionIdSequence(Integer transactionIdSequence) {
		this.transactionIdSequence = transactionIdSequence;
	}

	@Column(name="trans_note")
	public String getTransactionNote() {
		return transactionNote;
	}

	public void setTransactionNote(String transactionNote) {
		this.transactionNote = transactionNote;
	}

	@ManyToOne
	@JoinColumn(name="party_id",foreignKey=@ForeignKey(name="FK_STTR_PARTY"))
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@Column(name="qty_in")
	@NotNull
	public Double getQuantityIn() {
		return quantityIn;
	}

	public void setQuantityIn(Double quantityIn) {
		this.quantityIn = quantityIn;
	}

	@Column(name="qty_out")
	@NotNull
	public Double getQuantityOut() {
		return quantityOut;
	}

	public void setQuantityOut(Double quantityOut) {
		this.quantityOut = quantityOut;
	}

	@Column(name="reverse")
	public String getReverse() {
		return reverse;
	}

	public void setReverse(String reverse) {
		this.reverse = reverse;
	}

	@Column(name="remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	
	@Column(name="trans_number")
	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	@Override
	public String toString() {
		return "StockTrace [businessDate=" + businessDate + ", transactionDate=" + transactionDate + ", material="
				+ material + ", transactionType=" + transactionType + ", transactionHeaderId=" + transactionHeaderId
				+ ", transactionId=" + transactionId + ", transactionIdSequence=" + transactionIdSequence
				+ ", transactionNote=" + transactionNote + ", party=" + party + ", quantityIn=" + quantityIn
				+ ", quantityOut=" + quantityOut + ", reverse=" + reverse + ", remarks=" + remarks + "]";
	}
	
	
	

}
