package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;



@Entity
@Table(name="tr_status_change")
public class TransactionStatusChange extends AuditModelAbstractEntity  {
	
	private TransactionType transactionType;
	
	private String transactionId;
	
	private String transactionNumber;
	
	private Status status;
	
	private String latest;
	
	private TransactionType sourceTransactionType;
	
	private String sourceTransactionId;
	
	private String sourceTransactionNumber;
	
	private String sourceOperation;
	
	private Party party;

	@ManyToOne
	@JoinColumn(name="tran_type", foreignKey=@ForeignKey(name="FK_TSC_TT"))
	@NotNull
	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
	
	@ManyToOne
	@JoinColumn(name="party_id", foreignKey=@ForeignKey(name="FK_TSC_PARTY"))
	@NotNull
	public Party getParty() {
		return party;
	}

	public void setParty(Party party2) {
		this.party = party2;
	}

	@Column(name="tran_id")
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@ManyToOne
	@JoinColumn(name="status_id", foreignKey=@ForeignKey(name="FK_TSC_ST"))
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Column(name="latest")
	public String getLatest() {
		return latest;
	}

	public void setLatest(String latest) {
		this.latest = latest;
	}

	@ManyToOne
	@JoinColumn(name="src_tran_type", foreignKey=@ForeignKey(name="FK_TSC_TTS"))
	public TransactionType getSourceTransactionType() {
		return sourceTransactionType;
	}

	public void setSourceTransactionType(TransactionType sourceTransactionType) {
		this.sourceTransactionType = sourceTransactionType;
	}

	@Column(name="src_tran_id")
	public String getSourceTransactionId() {
		return sourceTransactionId;
	}

	public void setSourceTransactionId(String sourceTransactionId) {
		this.sourceTransactionId = sourceTransactionId;
	}

	@Column(name="src_oper")
	public String getSourceOperation() {
		return sourceOperation;
	}

	public void setSourceOperation(String sourceOperation) {
		this.sourceOperation = sourceOperation;
	}
	
	@Column(name="tran_number")
	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	@Column(name="src_tran_number")
	public String getSourceTransactionNumber() {
		return sourceTransactionNumber;
	}

	public void setSourceTransactionNumber(String sourceTransactionNumber) {
		this.sourceTransactionNumber = sourceTransactionNumber;
	}
	
	

}
