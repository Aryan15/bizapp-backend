package com.coreerp.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="tr_pettycash_header")
public class PettyCashHeader extends AbstractTransactionModel{

    private String pettyCashNumber;
    private Long peetyCashId;
    private Date pettyCashDate;
    private TransactionType pettyCashTypeId;

    private Double totalIncomingAmount;
    private Double totalOutgoingAmount;
    private  Long isDayClosed;
    private String comments;
    private String approvedBy;
    private  Date approvedDate;
    private FinancialYear financialYearId;
    private Company  companyId;
    private Status status;
    private Double totalAmount;
    private Double bankAmount;
    private Double cashAmount;
    private Double balanceAmount;
    private Long isLatest;
    private List<PettyCashItem> pettyCashItems;

    @Column(name="petty_cash_number")
    public String getPettyCashNumber() {
        return pettyCashNumber;
    }

    public void setPettyCashNumber(String pettyCashNumber) {
        this.pettyCashNumber = pettyCashNumber;
    }

    @Column(name="petty_cash_id")
    
    public Long getPeetyCashId() {
        return peetyCashId;
    }

    public void setPeetyCashId(Long peetyCashId) {
        this.peetyCashId = peetyCashId;
    }




    @Column(name="petty_cash_date")
    public Date getPettyCashDate() {
        return pettyCashDate;
    }

    public void setPettyCashDate(Date pettyCashDate) {
        this.pettyCashDate = pettyCashDate;
    }

    @Column(name="total_incoming_amount")
    public Double getTotalIncomingAmount() {
        return totalIncomingAmount;
    }

    public void setTotalIncomingAmount(Double totalIncomingAmount) {
        this.totalIncomingAmount = totalIncomingAmount;
    }

    @Column(name="total_outgoing_amount")
    public Double getTotalOutgoingAmount() {
        return totalOutgoingAmount;
    }

    public void setTotalOutgoingAmount(Double totalOutgoingAmount) {
        this.totalOutgoingAmount = totalOutgoingAmount;
    }


    @Column(name="is_day_closed")
    public Long getIsDayClosed() {
        return isDayClosed;
    }

    public void setIsDayClosed(Long isDayClosed) {
        this.isDayClosed = isDayClosed;
    }

    @Column(name="comments")
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Column(name="approved_by")
    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }



    @Column(name="approved_date")
    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }



    @ManyToOne
    @JoinColumn(name="financial_year_id", foreignKey=@ForeignKey(name="FK_PETTY_FY"))
    @NotNull
    public FinancialYear getFinancialYearId() {
        return financialYearId;
    }

    public void setFinancialYearId(FinancialYear financialYearId) {
        this.financialYearId = financialYearId;
    }




    @ManyToOne
    @JoinColumn(name="company_id", foreignKey=@ForeignKey(name="FK_PETTY_COMPANY"))
    @NotNull
    public Company getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Company companyId) {
        this.companyId = companyId;
    }


    @ManyToOne
    @JoinColumn(name="status_id",foreignKey=@ForeignKey(name="FK_PETTY_STATUS"))
    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }




    @Column(name="total_amount")
    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Column(name="bank_amount")
    public Double getBankAmount() {
        return bankAmount;
    }

    public void setBankAmount(Double bankAmount) {
        this.bankAmount = bankAmount;
    }

    @Column(name="cash_amount")
    public Double getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(Double cashAmount) {
        this.cashAmount = cashAmount;
    }

    @Column(name="balance_amount")
    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    @ManyToOne
    @JoinColumn(name="petty_cash_type_id", foreignKey=@ForeignKey(name="FK_PETTY_TT"))
    @NotNull
    public TransactionType getPettyCashTypeId() {
        return pettyCashTypeId;
    }

    public void setPettyCashTypeId(TransactionType pettyCashTypeId) {
        this.pettyCashTypeId = pettyCashTypeId;
    }


    @OneToMany(mappedBy="pettyCashHeader",cascade=CascadeType.ALL)
    public List<PettyCashItem> getPettyCashItems() {
        return pettyCashItems;
    }

    public void setPettyCashItems(List<PettyCashItem> pettyCashItems) {
        this.pettyCashItems = pettyCashItems;
    }

    @Column(name="is_latest")
    public Long getIsLatest() {
        return isLatest;
    }

    public void setIsLatest(Long isLatest) {
        this.isLatest = isLatest;
    }
}
