package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_voucher_item")
public class VoucherItem extends AbstractTransactionModel{
	
	private VoucherHeader voucherHeader;
	
	private String description;
	
	private Double amount;
	
	private ExpenseHeader expenseHeader;

	@ManyToOne
	@JoinColumn(name="voucher_header_id", foreignKey=@ForeignKey(name="FK_VOTI_VOTH"))
	@NotNull
	public VoucherHeader getVoucherHeader() {
		return voucherHeader;
	}

	public void setVoucherHeader(VoucherHeader voucherHeader) {
		this.voucherHeader = voucherHeader;
	}

	@Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="amount")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@ManyToOne
	@JoinColumn(name="expense_header_id", foreignKey=@ForeignKey(name="FK_VOTI_ETH"))
	@NotNull
	public ExpenseHeader getExpenseHeader() {
		return expenseHeader;
	}

	public void setExpenseHeader(ExpenseHeader expenseHeader) {
		this.expenseHeader = expenseHeader;
	}

	
	
	

}
