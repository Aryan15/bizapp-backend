package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

@MappedSuperclass
public abstract class AbstractTransactionModel extends AuditModel{
	
	private String id;


	@Id
	@GenericGenerator(name="id_generator", strategy="com.coreerp.idgen.TransactionIdGenerator")
	@GeneratedValue(generator="id_generator")
	@Column(name="id", length=255)
	@Size(min=1)
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}



}
