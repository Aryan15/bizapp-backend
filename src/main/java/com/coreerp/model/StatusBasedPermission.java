package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

@Entity
@Table(name="fw_status_based_permission")
@Where(clause = "deleted='N'")
public class StatusBasedPermission extends AuditModelAbstractEntity {

    private Status status;
    private String deleted= "N";
	private Integer permissionToCreate;
	private Integer permissionToUpdate;
	private Integer permissionToDelete;
	private Integer permissionToCancel;
	private Integer permissionToApprove;
	private Integer permissionToPrint;
	private Integer permissionToDraft;
	
	@ManyToOne
	@JoinColumn(name="status_id", foreignKey=@ForeignKey(name="FK_SBP_STAT"))
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	@Column(name="deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}
	
	@Column(name="permission_to_create")
	public Integer getPermissionToCreate() {
		return permissionToCreate;
	}
	public void setPermissionToCreate(Integer permissionToCreate) {
		this.permissionToCreate = permissionToCreate;
	}
	
	@Column(name="permission_to_update")
	public Integer getPermissionToUpdate() {
		return permissionToUpdate;
	}
	public void setPermissionToUpdate(Integer permissionToUpdate) {
		this.permissionToUpdate = permissionToUpdate;
	}
	
	@Column(name="permission_to_delete")
	public Integer getPermissionToDelete() {
		return permissionToDelete;
	}
	public void setPermissionToDelete(Integer permissionToDelete) {
		this.permissionToDelete = permissionToDelete;
	}
	
	@Column(name="permission_to_cancel")
	public Integer getPermissionToCancel() {
		return permissionToCancel;
	}
	public void setPermissionToCancel(Integer permissionToCancel) {
		this.permissionToCancel = permissionToCancel;
	}
	
	@Column(name="permission_to_approve")
	public Integer getPermissionToApprove() {
		return permissionToApprove;
	}
	public void setPermissionToApprove(Integer permissionToApprove) {
		this.permissionToApprove = permissionToApprove;
	}
	
	@Column(name="permission_to_print")
	public Integer getPermissionToPrint() {
		return permissionToPrint;
	}
	public void setPermissionToPrint(Integer permissionToPrint) {
		this.permissionToPrint = permissionToPrint;
	}
	
	@Column(name="permission_to_draft")
	public Integer getPermissionToDraft() {
		return permissionToDraft;
	}
	public void setPermissionToDraft(Integer permissionToDraft) {
		this.permissionToDraft = permissionToDraft;
	}
	
	
	
	
}
