package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name="conf_number_range")
@Where(clause = "deleted='N'")
public class NumberRangeConfiguration extends AuditModelAbstractEntity{

	private String deleted="N";
	
	private TransactionType transactionType;
	
	private String prefix;
	
	private String postfix;
	
	private Integer startNumber;
	
	private Integer autoNumber;
	
	private Integer autoNumberReset;
	
	private Integer termsAndConditionCheck;
	
	private String delimiter;
	
	private Integer financialYearCheck;
	
	private PrintTemplate printTemplate;
	
	private Long printTemplateTopSize;

	private Integer isZeroPrefix;

	private String printHeaderText;

	private Integer allowShipingAddress;



	@Column(name="deleted")
	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}

	@ManyToOne
	@JoinColumn(name="trans_type_id", foreignKey=@ForeignKey(name="FK_NRC_TT"))
	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	@Column(name="prefix")
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	
	@Column(name="postfix")
	public String getPostfix() {
		return postfix;
	}

	public void setPostfix(String postfix) {
		this.postfix = postfix;
	}

	@Column(name="start_number")
	public Integer getStartNumber() {
		return startNumber;
	}

	public void setStartNumber(Integer startNumber) {
		this.startNumber = startNumber;
	}

	@Column(name="auto_number")
	public Integer getAutoNumber() {
		return autoNumber;
	}

	public void setAutoNumber(Integer autoNumber) {
		this.autoNumber = autoNumber;
	}

	@Column(name="auto_number_reset")
	public Integer getAutoNumberReset() {
		return autoNumberReset;
	}

	public void setAutoNumberReset(Integer autoNumberReset) {
		this.autoNumberReset = autoNumberReset;
	}

	@Column(name="terms_and_condition_check")
	public Integer getTermsAndConditionCheck() {
		return termsAndConditionCheck;
	}

	public void setTermsAndConditionCheck(Integer termsAndConditionCheck) {
		this.termsAndConditionCheck = termsAndConditionCheck;
	}

	@Column(name="delimiter")
	public String getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	@Column(name="financial_Year_Check")
	public Integer getFinancialYearCheck() {
		return financialYearCheck;
	}

	public void setFinancialYearCheck(Integer financialYearCheck) {
		this.financialYearCheck = financialYearCheck;
	}

	@Override
	public String toString() {
		return "NumberRangeConfiguration [deleted=" + deleted + ", transactionType=" + transactionType + ", prefix="
				+ prefix + ", postfix=" + postfix + ", startNumber=" + startNumber + ", autoNumber=" + autoNumber
				+ ", autoNumberReset=" + autoNumberReset + ", termsAndConditionCheck=" + termsAndConditionCheck
				+ ", delimiter=" + delimiter + ", financialYearCheck=" + financialYearCheck + ", printTemplateTopSize=" + printTemplateTopSize + ", printHeaderText=" +printHeaderText+ ", allowShipingAddress=" +allowShipingAddress+ "]";
	}


	@ManyToOne
	@JoinColumn(name="print_template_id", foreignKey=@ForeignKey(name="FK_NRC_PT"))
	public PrintTemplate getPrintTemplate() {
		return printTemplate;
	}

	public void setPrintTemplate(PrintTemplate printTemplate) {
		this.printTemplate = printTemplate;
	}

	@Column(name="print_template_top_height")
	public Long getPrintTemplateTopSize() {
		return printTemplateTopSize;
	}

	public void setPrintTemplateTopSize(Long printTemplateTopSize) {
		this.printTemplateTopSize = printTemplateTopSize;
	}

	@Column(name = "is_zero_prefix")
	public Integer getIsZeroPrefix() {
		return isZeroPrefix;
	}

	public void setIsZeroPrefix(Integer isZeroPrefix) {
		this.isZeroPrefix = isZeroPrefix;
	}

	@Column(name = "print_header_text")
	public String getPrintHeaderText() {
		return printHeaderText;
	}

	public void setPrintHeaderText(String printHeaderText) {
		this.printHeaderText = printHeaderText;
	}

	@Column(name = "allow_shiping_address")
	public Integer getAllowShipingAddress() {
		return allowShipingAddress;
	}

	public void setAllowShipingAddress(Integer allowShipingAddress) {
		this.allowShipingAddress = allowShipingAddress;
	}
}
