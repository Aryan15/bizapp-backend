package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

import com.coreerp.dto.ActivityDTO;

@Entity
@Table(name="fw_activity")
@Where(clause = "deleted='N'")
public class Activity extends AuditModelAbstractEntity implements Comparable<Activity>{
	
	private Menu menu;
	
	private String name;
	
	private String pageUrl;
	
	private String urlParams;
	
	private String imagePath;
	
	private String description;
	
	private String deleted;
	
	private Integer activityOrder;
	
	private Company company;
	
	private Menu subMenu;

	@ManyToOne
	@JoinColumn(name="menu_id",  foreignKey=@ForeignKey(name="FK_ACT_MENU"))
	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="page_url")
	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	@Column(name="url_params")
	public String getUrlParams() {
		return urlParams;
	}

	public void setUrlParams(String urlParams) {
		this.urlParams = urlParams;
	}

	@Column(name="image_path")
	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}

	@Column(name="activity_order")
	public Integer getActivityOrder() {
		return activityOrder;
	}

	public void setActivityOrder(Integer activityOrder) {
		this.activityOrder = activityOrder;
	}

	@ManyToOne
	@JoinColumn(name="company_id", foreignKey=@ForeignKey(name="FK_ACT_COMP"))
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@OneToOne
	@JoinColumn(name="sub_menu_id", foreignKey=@ForeignKey(name="FK_ACT_SMENU"))	
	public Menu getSubMenu() {
		return subMenu;
	}

	public void setSubMenu(Menu subMenu) {
		this.subMenu = subMenu;
	}

	@Override
	public int compareTo(Activity o) {
		if(activityOrder == o.getActivityOrder()){
			return 0;
		}
		else if(activityOrder > o.getActivityOrder()){
			return 1;
		}else
			return -1;
	}
	
	

}
