package com.coreerp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_purchase_order_item")
public class PurchaseOrderItem  extends AbstractTransactionModel{

	private PurchaseOrderHeader purchaseOrderHeader;
	
	private Material material;
    private Integer slNo;
    private Double quantity;
    private Double dcBalanceQuantity;
    private Double price;
    private Double amount;
    //private QuotationHeader quotationHeader ;
    private QuotationItem quotationItem;
    private Double basicAmount;
    private Double taxPercent;
    private Double taxAmount;
    private Double amountAfterTax;
    private Double discountPercentage;
    private Double discountAmount;
    private Double amountAfterDiscount;    
    private UnitOfMeasurement unitOfMeasurement;
    private Tax tax;
    private Integer inclusiveTax;
    private Double taxRate;
    private String remarks;
    private String outDeliveryChallanNumber;
    private String batchCode;
    private Date outDeliveryChallanDate;    
    private String inspectionNumber;
    private String inDeliveryChallanNumber;
    private Date inDeliveryChallanDate;
    private String descriptionOfPackages;
    private String averageContentPerPackage;
    private Double transportationAmount;
    private Double cessPercentage;
    private Double cessAmount;
    private Double igstTaxPercentage;
    private Double igstTaxAmount;
    private Double sgstTaxPercentage;
    private Double sgstTaxAmount;
    private Double cgstTaxPercentage;
    private Double cgstTaxAmount;
    private Double invoiceBalanceQuantity;
    private String processId;
    private String processName;
    private String specification;

	
	@ManyToOne
	@JoinColumn(name = "po_header_id", foreignKey=@ForeignKey(name="FK_POI_POH"))	
	@NotNull
	public PurchaseOrderHeader getPurchaseOrderHeader() {
		return purchaseOrderHeader;
	}
	public void setPurchaseOrderHeader(PurchaseOrderHeader purchaseOrderHeader) {
		this.purchaseOrderHeader = purchaseOrderHeader;
	}
	
	
	@Column(name="sl_no")
	public Integer getSlNo() {
		return slNo;
	}


	
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	@Column(name="quantity")
	public Double getQuantity() {
		return quantity;
	}


	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	


	
	@Column(name="price")
	public Double getPrice() {
		return price;
	}


	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name="amount")
	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name="disc_per")
	public Double getDiscountPercentage() {
		return discountPercentage;
	}


	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	@Column(name="disc_amt")
	public Double getDiscountAmount() {
		return discountAmount;
	}


	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	@Column(name="amt_after_disc")
	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}


	public void setAmountAfterDiscount(Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}

	@Column(name="transportation_amt")
	public Double getTransportationAmount() {
		return transportationAmount;
	}


	public void setTransportationAmount(Double transportationAmount) {
		this.transportationAmount = transportationAmount;
	}

	@Column(name="cess_per")
	public Double getCessPercentage() {
		return cessPercentage;
	}


	public void setCessPercentage(Double cessPercentage) {
		this.cessPercentage = cessPercentage;
	}

	@Column(name="cess_amt")
	public Double getCessAmount() {
		return cessAmount;
	}


	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}

	@Column(name="igst_tax_per")
	public Double getIgstTaxPercentage() {
		return igstTaxPercentage;
	}


	public void setIgstTaxPercentage(Double igstTaxPercentage) {
		this.igstTaxPercentage = igstTaxPercentage;
	}

	@Column(name="igst_tax_amt")
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}


	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	@Column(name="sgst_tax_per")
	public Double getSgstTaxPercentage() {
		return sgstTaxPercentage;
	}


	public void setSgstTaxPercentage(Double sgstTaxPercentage) {
		this.sgstTaxPercentage = sgstTaxPercentage;
	}

	@Column(name="sgst_tax_amt")
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}


	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}

	@Column(name="cgst_tax_per")
	public Double getCgstTaxPercentage() {
		return cgstTaxPercentage;
	}


	public void setCgstTaxPercentage(Double cgstTaxPercentage) {
		this.cgstTaxPercentage = cgstTaxPercentage;
	}

	@Column(name="cgst_tax_amt")
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}


	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}

	@Column(name="amt_after_tax")
	public Double getAmountAfterTax() {
		return amountAfterTax;
	}


	public void setAmountAfterTax(Double amountAfterTax) {
		this.amountAfterTax = amountAfterTax;
	}

	@Column(name="remarks")
	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}




	@ManyToOne
	@JoinColumn(name="material_id", foreignKey=@ForeignKey(name="FK_POI_MATERIAL"))
	@NotNull
	public Material getMaterial() {
		return material;
	}


	public void setMaterial(Material material) {
		this.material = material;
	}



	
//    @ManyToOne
//    @JoinColumn(name = "quotation_header_id", foreignKey=@ForeignKey(name="FK_POI_QOTH"))
//	public QuotationHeader getQuotationHeader() {
//		return quotationHeader;
//	}
//
//
//	public void setQuotationHeader(QuotationHeader quotationHeader) {
//		this.quotationHeader = quotationHeader;
//	}

	
	
    @OneToOne
    @JoinColumn(name = "quotation_item_id", foreignKey=@ForeignKey(name="FK_POI_QOTI"))
	public QuotationItem getQuotationItem() {
		return quotationItem;
	}
	public void setQuotationItem(QuotationItem quotationItem) {
		this.quotationItem = quotationItem;
	}
	@Column(name="basic_amt")
	public Double getBasicAmount() {
		return basicAmount;
	}


	public void setBasicAmount(Double basicAmount) {
		this.basicAmount = basicAmount;
	}

	@Column(name="tax_percent")
	public Double getTaxPercent() {
		return taxPercent;
	}


	public void setTaxPercent(Double taxPercent) {
		this.taxPercent = taxPercent;
	}

	@Column(name="tax_amt")
	public Double getTaxAmount() {
		return taxAmount;
	}


	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	@ManyToOne
	@JoinColumn(name="uom_id", foreignKey=@ForeignKey(name="FK_POI_UOM"))
	public UnitOfMeasurement getUnitOfMeasurement() {
		return unitOfMeasurement;
	}


	public void setUnitOfMeasurement(UnitOfMeasurement unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}

	@ManyToOne
	@JoinColumn(name="tax_id", foreignKey=@ForeignKey(name="FK_POI_TAX"))
	//@NotNull
	public Tax getTax() {
		return tax;
	}


	public void setTax(Tax tax) {
		this.tax = tax;
	}

	@Column(name="inclusive_tax")
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}


	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	@Column(name="tax_rate")
	public Double getTaxRate() {
		return taxRate;
	}


	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	@Column(name="dc_number")
	public String getOutDeliveryChallanNumber() {
		return outDeliveryChallanNumber;
	}


	public void setOutDeliveryChallanNumber(String outDeliveryChallanNumber) {
		this.outDeliveryChallanNumber = outDeliveryChallanNumber;
	}

	@Column(name="batch_code")
	public String getBatchCode() {
		return batchCode;
	}


	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	@Column(name="out_dc_date")
	public Date getOutDeliveryChallanDate() {
		return outDeliveryChallanDate;
	}


	public void setOutDeliveryChallanDate(Date outDeliveryChallanDate) {
		this.outDeliveryChallanDate = outDeliveryChallanDate;
	}

	@Column(name="inspection_number")
	public String getInspectionNumber() {
		return inspectionNumber;
	}


	public void setInspectionNumber(String inspectionNumber) {
		this.inspectionNumber = inspectionNumber;
	}

	@Column(name="in_dc_number")
	public String getInDeliveryChallanNumber() {
		return inDeliveryChallanNumber;
	}


	public void setInDeliveryChallanNumber(String inDeliveryChallanNumber) {
		this.inDeliveryChallanNumber = inDeliveryChallanNumber;
	}

	@Column(name="in_dc_date")
	public Date getInDeliveryChallanDate() {
		return inDeliveryChallanDate;
	}


	public void setInDeliveryChallanDate(Date inDeliveryChallanDate) {
		this.inDeliveryChallanDate = inDeliveryChallanDate;
	}

	@Column(name="description_of_packages")
	public String getDescriptionOfPackages() {
		return descriptionOfPackages;
	}


	public void setDescriptionOfPackages(String descriptionOfPackages) {
		this.descriptionOfPackages = descriptionOfPackages;
	}

	@Column(name="average_content_per_package")
	public String getAverageContentPerPackage() {
		return averageContentPerPackage;
	}


	public void setAverageContentPerPackage(String averageContentPerPackage) {
		this.averageContentPerPackage = averageContentPerPackage;
	}
	

	@Column(name="dc_balance_quantity")	
	public Double getDcBalanceQuantity() {
		return dcBalanceQuantity;
	}
	
	public void setDcBalanceQuantity(Double dcBalanceQuantity) {
		this.dcBalanceQuantity = dcBalanceQuantity;
	}
	
	@Column(name="invoice_balance_quantity")
	public Double getInvoiceBalanceQuantity() {
		return invoiceBalanceQuantity;
	}
	
	
	public void setInvoiceBalanceQuantity(Double invoiceBalanceQuantity) {
		this.invoiceBalanceQuantity = invoiceBalanceQuantity;
	}
	
	


	
	
	@Override
	public String toString() {
		return "PurchaseOrderItem [material=" + material + ", slNo=" + slNo + ", quantity=" + quantity
				+ ", balanceQuantity=" + dcBalanceQuantity + ", price=" + price + ", amount=" + amount + ", basicAmount="
				+ basicAmount + ", taxPercent=" + taxPercent + ", taxAmount=" + taxAmount + ", amountAfterTax="
				+ amountAfterTax + ", discountPercentage=" + discountPercentage + ", discountAmount=" + discountAmount
				+ ", amountAfterDiscount=" + amountAfterDiscount + ", unitOfMeasurement=" + unitOfMeasurement + ", tax="
				+ tax + ", inclusiveTax=" + inclusiveTax + ", taxRate=" + taxRate + ", remarks=" + remarks
				+ ", outDeliveryChallanNumber=" + outDeliveryChallanNumber + ", batchCode=" + batchCode
				+ ", outDeliveryChallanDate=" + outDeliveryChallanDate + ", inspectionNumber=" + inspectionNumber
				+ ", inDeliveryChallanNumber=" + inDeliveryChallanNumber + ", inDeliveryChallanDate="
				+ inDeliveryChallanDate + ", descriptionOfPackages=" + descriptionOfPackages
				+ ", averageContentPerPackage=" + averageContentPerPackage + ", transportationAmount="
				+ transportationAmount + ", cessPercentage=" + cessPercentage + ", cessAmount=" + cessAmount
				+ ", igstTaxPercentage=" + igstTaxPercentage + ", igstTaxAmount=" + igstTaxAmount
				+ ", sgstTaxPercentage=" + sgstTaxPercentage + ", sgstTaxAmount=" + sgstTaxAmount
				+ ", cgstTaxPercentage=" + cgstTaxPercentage + ", cgstTaxAmount=" + cgstTaxAmount + "]";
	}
	
	@Column(name="process_id")
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	
	@Column(name="process_name")
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	@Column(name="specification")
	public String getSpecification() {
		return specification;
	}
	public void setSpecification(String specification) {
		this.specification = specification;
	}
	
}
