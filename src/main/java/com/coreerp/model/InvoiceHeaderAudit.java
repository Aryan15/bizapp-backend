package com.coreerp.model;

import com.coreerp.utils.TransactionDataException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="tr_invoice_header_audit")
public class InvoiceHeaderAudit {

    private String id;

    private Date createdDateTime;

    private String createdBy;

    private Date updatedDateTime;

    private String updatedBy;

    private Date invoiceDate;
    private String invoiceNumber ;
    private Long invId;
    private Date purchaseOrderDate ;
    private Date internalReferenceDate;
    private String internalReferenceNumber;
    private Long partyId ;
    private Double subTotal;
    private Double discountAmount;
    private Double discountPercent;
    private Double netAmount ;
    private Long taxId;
    private Double roundOffAmount ;
    private Double grandTotal;
    private Long financialYearId;
    private Long companyId;
    private Long invoiceTypeId;
    private String paymentTerms;
    private String deliveryTerms;
    private String termsAndConditions;
    private String modeOfDispatch ;
    private String vehicleNumber ;
    private String numOfPackages;
    private String documentThrough ;
    private Double taxAmount;
    private String purchaseOrderNumber ;
    private Status status;
    private Double advanceAmount;
    private Date paymentDueDate ;
    private Double dueAmount;
    private String partyAddress ;
    private String deliveryChallanNumber ;
    private Date deliveryChallanDate ;
    private String grnNumber ;
    private Date grnDate ;
    private String proformaInvoiceNumber;
    private Date proformaInvoiceDate;
    private String sourceInvoiceNumber;
    private Date sourceInvoiceDate;
    private Long labourChargesOnly  ;
    private String billToAddress ;
    private String shipToAddress ;
    private String notForSale;
    private String timeOfInvoice;
    private String vendorCode;
    private Double transportationCharges;
    private Double cessAmount;
    private Double cessPercent;
    private Double sgstTaxRate;
    private Double sgstTaxAmount;
    private Double cgstTaxRate;
    private Double cgstTaxAmount;
    private Double igstTaxRate;
    private Double igstTaxAmount;
    private String eWayBillNumber ;
    private Double totalTaxableAmount ;
    private Long isReverseCharge;
    private String remarks;
    private Integer inclusiveTax;
    private String deliveryChallanDateString;

    private String gstNumber;

    private Integer isReused;

    private String reusedHeaderId;
    private Long materialNoteType;


    @Id
    @Column(name="id", length=255)
    @Size(min=1)
    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }
    @Column(name="created_date_time",nullable = false, updatable = false)
    public Date getCreatedDateTime() {
        return createdDateTime;
    }
    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Column(name="created_by",nullable = false, updatable = false)
    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name="updated_date_time")
    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }
    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    @Column(name="updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }


    @Column(name ="remarks")
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }



    @Column(name="inv_date")
    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @Column(name="inv_number")
    public String getInvoiceNumber() {
        return invoiceNumber;
    }


    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }




    @Column(name="inv_id")
    @NotNull
    public Long getInvId() {
        return invId;
    }


    public void setInvId(Long invId) {
        this.invId = invId;
    }


    @Column(name="payment_due_date")
    public Date getPaymentDueDate() {
        return paymentDueDate;
    }


    public void setPaymentDueDate(Date paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }


    @Column(name="due_amount")
    public Double getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(Double dueAmount) throws TransactionDataException {

        // if(this.grandTotal != null && dueAmount > this.grandTotal){
        // 	throw new TransactionDataException("Due Amount being set greater than grand total", null);
        // }
        // else{
        // 	this.dueAmount = dueAmount;
        // }

        this.dueAmount = dueAmount;
    }

    @Column(name="e_way_bill_number")
    public String geteWayBillNumber() {
        return eWayBillNumber;
    }


    public void seteWayBillNumber(String eWayBillNumber) {
        this.eWayBillNumber = eWayBillNumber;
    }


    @Column(name="total_taxable_amount")
    public Double getTotalTaxableAmount() {
        return totalTaxableAmount;
    }


    public void setTotalTaxableAmount(Double totalTaxableAmount) {
        this.totalTaxableAmount = totalTaxableAmount;
    }



    @Column(name="grand_total")
    public Double getGrandTotal() {
        return grandTotal;
    }


    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    @Column(name="bill_to_address")
    public String getBillToAddress() {
        return billToAddress;
    }


    public void setBillToAddress(String billToAddress) {
        this.billToAddress = billToAddress;
    }

    @Column(name="labour_charges_only")
    public Long getLabourChargesOnly() {
        return labourChargesOnly;
    }


    public void setLabourChargesOnly(Long labourChargesOnly) {
        this.labourChargesOnly = labourChargesOnly;
    }

    @Column(name="sub_total")
    public Double getSubTotal() {
        return subTotal;
    }


    public void setSubTotal(Double subTotal ){
        this.subTotal = subTotal;
    }

    @Column(name="payment_terms", length=500)
    public String getPaymentTerms() {
        return paymentTerms;
    }


    public void setPaymentTerms(String paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    @Column(name="delivery_terms", length=500)
    public String getDeliveryTerms() {
        return deliveryTerms;
    }


    public void setDeliveryTerms(String deliveryTerms) {
        this.deliveryTerms = deliveryTerms;
    }

    @Column(name="terms_conditions", length =2500)
    public String getTermsAndConditions() {
        return termsAndConditions;
    }


    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    @Column(name="mode_of_dispatch")
    public String getModeOfDispatch() {
        return modeOfDispatch;
    }


    public void setModeOfDispatch(String modeOfDispatch) {
        this.modeOfDispatch = modeOfDispatch;
    }

    @Column(name="vehicle_number")
    public String getVehicleNumber() {
        return vehicleNumber;
    }


    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }


    @Column(name="number_of_packages")
    public String getNumOfPackages() {
        return numOfPackages;
    }


    public void setNumOfPackages(String numOfPackages) {
        this.numOfPackages = numOfPackages;
    }

    @Column(name="doc_through", length=2500)
    public String getDocumentThrough() {
        return documentThrough;
    }


    public void setDocumentThrough(String documentThrough) {
        this.documentThrough = documentThrough;
    }

    @Column(name="ship_address", length=1500)
    public String getShipToAddress() {
        return shipToAddress;
    }


    public void setShipToAddress(String shipToAddress) {
        this.shipToAddress = shipToAddress;
    }



    @Column(name="net_amount")
    public Double getNetAmount() {
        return netAmount;
    }


    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    @Column(name="transportation_amt")
    public Double getTransportationCharges() {
        return transportationCharges;
    }


    public void setTransportationCharges(Double transportationCharges) {
        this.transportationCharges = transportationCharges;
    }

    @Column(name="advance_amount")
    public Double getAdvanceAmount() {
        return advanceAmount;
    }


    public void setAdvanceAmount(Double advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    @Column(name="round_off_amount")
    public Double getRoundOffAmount() {
        return roundOffAmount;
    }


    public void setRoundOffAmount(Double roundOffAmount) {
        this.roundOffAmount = roundOffAmount;
    }




    @Column(name="po_date")
    public Date getPurchaseOrderDate() {
        return purchaseOrderDate;
    }


    public void setPurchaseOrderDate(Date purchaseOrderDate) {
        this.purchaseOrderDate = purchaseOrderDate;
    }

    @Column(name="po_number")
    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }


    public void setPurchaseOrderNumber(String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }


    @Column(name="proforma_invoice_number")
    public String getProformaInvoiceNumber() {
        return proformaInvoiceNumber;
    }

    public void setProformaInvoiceNumber(String proformaInvoiceNumber) {
        this.proformaInvoiceNumber = proformaInvoiceNumber;
    }

    @Column(name="proforma_invoice_date")
    public Date getProformaInvoiceDate() {
        return proformaInvoiceDate;
    }

    public void setProformaInvoiceDate(Date proformaInvoiceDate) {
        this.proformaInvoiceDate = proformaInvoiceDate;
    }

    @Column(name="dc_date")
    public Date getDeliveryChallanDate() {
        return deliveryChallanDate;
    }


    public void setDeliveryChallanDate(Date deliveryChallanDate) {
        this.deliveryChallanDate = deliveryChallanDate;
    }


    @Column(name="dc_number")
    public String getDeliveryChallanNumber() {
        return deliveryChallanNumber;
    }


    public void setDeliveryChallanNumber(String deliveryChallanNumber) {
        this.deliveryChallanNumber = deliveryChallanNumber;
    }

    @Column(name="int_ref_date")
    public Date getInternalReferenceDate() {
        return internalReferenceDate;
    }


    public void setInternalReferenceDate(Date internalReferenceDate) {
        this.internalReferenceDate = internalReferenceDate;
    }

    @Column(name="int_ref_number")
    public String getInternalReferenceNumber() {
        return internalReferenceNumber;
    }


    public void setInternalReferenceNumber(String internalReferenceNumber) {
        this.internalReferenceNumber = internalReferenceNumber;
    }


    @Column(name="party_id")
    public Long getPartyId() {
        return partyId;
    }


    public void setPartyId(Long partyId) {
        this.partyId = partyId;
    }

    @Column(name="discount_amount")
    public Double getDiscountAmount() {
        return discountAmount;
    }


    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    @Column(name="discount_percent")
    public Double getDiscountPercent() {
        return discountPercent;
    }


    public void setDiscountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
    }


    @Column(name="tax_id")
    public Long getTaxId() {
        return taxId;
    }


    public void setTaxId(Long taxId) {
        this.taxId = taxId;
    }


    @Column(name="financial_year_id")
    public Long getFinancialYearId() {
        return financialYearId;
    }


    public void setFinancialYearId(Long financialYearId) {
        this.financialYearId = financialYearId;
    }


    @Column(name="company_id")
    public Long getCompanyId() {
        return companyId;
    }


    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Column(name="invoice_type")
    public Long getInvoiceTypeId() {
        return invoiceTypeId;
    }


    public void setInvoiceTypeId(Long invoiceTypeId) {
        this.invoiceTypeId = invoiceTypeId;
    }



    @Column(name="tax_amount")
    public Double getTaxAmount() {
        return taxAmount;
    }


    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    @ManyToOne
    @JoinColumn(name="status_id",foreignKey=@ForeignKey(name="FK_INVH_ST"))
    @NotNull
    public Status getStatus() {
        return status;
    }


    public void setStatus(Status status) {
        this.status = status;
    }


//	public void setDueAmount(Double dueAmount) {
//		this.dueAmount = dueAmount;
//	}


    @Column(name="party_address")
    public String getPartyAddress() {
        return partyAddress;
    }


    public void setPartyAddress(String partyAddress) {
        this.partyAddress = partyAddress;
    }


    @Column(name="not_for_sale")
    public String getNotForSale() {
        return notForSale;
    }


    public void setNotForSale(String notForSale) {
        this.notForSale = notForSale;
    }

    @Column(name="time_of_invoice")
    public String getTimeOfInvoice() {
        return timeOfInvoice;
    }


    public void setTimeOfInvoice(String timeOfInvoice) {
        this.timeOfInvoice = timeOfInvoice;
    }

    @Column(name="vendor_code")
    public String getVendorCode() {
        return vendorCode;
    }


    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    @Column(name="cess_amt")
    public Double getCessAmount() {
        return cessAmount;
    }


    public void setCessAmount(Double cessAmount) {
        this.cessAmount = cessAmount;
    }

    @Column(name="cess_percent")
    public Double getCessPercent() {
        return cessPercent;
    }


    public void setCessPercent(Double cessPercent) {
        this.cessPercent = cessPercent;
    }

    @Column(name="sgst_tax_rate")
    public Double getSgstTaxRate() {
        return sgstTaxRate;
    }


    public void setSgstTaxRate(Double sgstTaxRate) {
        this.sgstTaxRate = sgstTaxRate;
    }

    @Column(name="sgst_tax_amount")
    public Double getSgstTaxAmount() {
        return sgstTaxAmount;
    }


    public void setSgstTaxAmount(Double sgstTaxAmount) {
        this.sgstTaxAmount = sgstTaxAmount;
    }

    @Column(name="cgst_tax_rate")
    public Double getCgstTaxRate() {
        return cgstTaxRate;
    }


    public void setCgstTaxRate(Double cgstTaxRate) {
        this.cgstTaxRate = cgstTaxRate;
    }

    @Column(name="cgst_tax_amount")
    public Double getCgstTaxAmount() {
        return cgstTaxAmount;
    }


    public void setCgstTaxAmount(Double cgstTaxAmount) {
        this.cgstTaxAmount = cgstTaxAmount;
    }

    @Column(name="igst_tax_rate")
    public Double getIgstTaxRate() {
        return igstTaxRate;
    }


    public void setIgstTaxRate(Double igstTaxRate) {
        this.igstTaxRate = igstTaxRate;
    }

    @Column(name="igst_tax_amount")
    public Double getIgstTaxAmount() {
        return igstTaxAmount;
    }


    public void setIgstTaxAmount(Double igstTaxAmount) {
        this.igstTaxAmount = igstTaxAmount;
    }



    @Column(name="source_invoice_number")
    public String getSourceInvoiceNumber() {
        return sourceInvoiceNumber;
    }

    public void setSourceInvoiceNumber(String sourceInvoiceNumber) {
        this.sourceInvoiceNumber = sourceInvoiceNumber;
    }

    @Column(name="source_invoice_date")
    public Date getSourceInvoiceDate() {
        return sourceInvoiceDate;
    }

    public void setSourceInvoiceDate(Date sourceInvoiceDate) {
        this.sourceInvoiceDate = sourceInvoiceDate;
    }

    @Column(name="is_revserse_charge")
    public Long getIsReverseCharge() {
        return isReverseCharge;
    }

    public void setIsReverseCharge(Long isReverseCharge) {
        this.isReverseCharge = isReverseCharge;
    }
    @Column(name="grn_number")
    public String getGrnNumber() {
        return grnNumber;
    }

    public void setGrnNumber(String grnNumber) {
        this.grnNumber = grnNumber;
    }
    @Column(name="grn_date")
    public Date getGrnDate() {
        return grnDate;
    }

    public void setGrnDate(Date grnDate) {
        this.grnDate = grnDate;
    }

    @Column(name="inclusive_tax")
    public Integer getInclusiveTax() {
        return inclusiveTax;
    }

    public void setInclusiveTax(Integer inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
    }

    @Column(name="dc_date_str")
    public String getDeliveryChallanDateString() {
        return deliveryChallanDateString;
    }

    public void setDeliveryChallanDateString(String deliveryChallanDateString) {
        this.deliveryChallanDateString = deliveryChallanDateString;
    }

    @Column(name="gst_number")
    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }


    @Column(name="is_reused")
    public Integer getIsReused() {
        return isReused;
    }

    public void setIsReused(Integer isReused) {
        this.isReused = isReused;
    }

    @Column(name="reused_header")
    public String getReusedHeaderId() {
        return reusedHeaderId;
    }

    public void setReusedHeaderId(String reusedHeaderId) {
        this.reusedHeaderId = reusedHeaderId;
    }

    @Column(name="material_note_type")
    public Long getMaterialNoteType() {
        return materialNoteType;
    }

    public void setMaterialNoteType(Long materialNoteType) {
        this.materialNoteType = materialNoteType;
    }
}
