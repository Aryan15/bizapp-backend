package com.coreerp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_grn_header")
public class GRNHeader extends AbstractTransactionModel {

	private Long grnId;
	private String grnNumber;
	private Date grnDate;
	private PurchaseOrderHeader purchaseOrderHeader;
	private String comments;
	private Integer verbal;//is_verbal
	private Status grnStatus;
	private Company company;
	private User qualityCheckedBy;
	private User stockCheckedBy;
	private FinancialYear financialYear;
	private Party supplier;
    private String supplierAddress;
    private String gstNumber;
    
    private String companyName; 
    private Long companygGstRegistrationTypeId; //companyGstRegistrationnTypeId
    private String companyPinCode; //companyPinCode .. etc
    private Long companyStateId;
    private String companyStateName;
    private Long companyCountryId;
    private String companyPrimaryMobile;
    private String companySecondaryMobile;
    private String companyContactPersonNumber;
    private String companyContactPersonName;
    private String companyPrimaryTelephone;
    private String companySecondaryTelephone;
    private String companyWebsite;
    private String companyEmail;
    private String companyFaxNumber;
    private String companyAddress;
    private String companyTagLine;
    private String companyGstNumber;
    private String companyPanNumber;
    private String companyPanDate;
    private String companyCeritificateImagePath;
    private String companyLogoPath;
    
    private String partyName;
    private String partyContactPersonNumber;
    private String partyPinCode;
    private Long partyAreaId;
    private Long partyCityId;
    private Long partyStateId;
    private Long partyCountryId;
    private String partyPrimaryTelephone;
    private String partySecondaryTelephone;
    private String partyPrimaryMobile;
    private String partySecondaryMobile;
    private String partyEmail;
    private String partyWebsite;
    private String partyContactPersonName;
//    private String partyBillToAddress;
//    private String partyShipAddress;
    private String partyDueDaysLimit;
    private String partyGstRegistrationTypeId;
//    private String partyGstNumber;
    private String partyPanNumber;
    private String isIgst;
	
    
    @Column(name="supplier_address")
	public String getSupplierAddress() {
		return supplierAddress;
	}
	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}
	
	@Column(name="gst_number")
	public String getGstNumber() {
		return gstNumber;
	}
	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}
	
	
	private DeliveryChallanHeader deliveryChallanHeader;
	//private NoteHeader debitNote; not required
	private String termsAndConditionNote;
	private TermsAndCondition termsAndConditon;
	//private Terms
    private List<InvoiceHeader> invoiceHeaders;
    //private String invoiceNumbers;
    //private String status;
    private TransactionType grnType;    
    private List<GRNItem> grnItems;
    private Tax tax;
   
	@Column(name="grn_id")  
	public Long getGrnId() {
		return grnId;
	}
	public void setGrnId(Long grnId) {
		this.grnId = grnId;
	}
	
	@Column(name="grn_no")
	public String getGrnNumber() {
		return grnNumber;
	}
	public void setGrnNumber(String grnNumber) {
		this.grnNumber = grnNumber;
	}
	@Column(name="grn_date")
	public Date getGrnDate() {
		return grnDate;
	}
	public void setGrnDate(Date grnDate) {
		this.grnDate = grnDate;
	}
	
	@ManyToOne
	@JoinColumn(name="po_id",foreignKey=@ForeignKey(name="FK_GRNH_POH")) 
	public PurchaseOrderHeader getPurchaseOrderHeader() {
		return purchaseOrderHeader;
	}
	public void setPurchaseOrderHeader(PurchaseOrderHeader purchaseOrderHeader) {
		this.purchaseOrderHeader = purchaseOrderHeader;
	}
	
	@Column(name="comments")
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	@Column(name="is_verbal")
	public Integer getVerbal() {
		return verbal;
	}
	public void setVerbal(Integer verbal) {
		this.verbal = verbal;
	}
	
	@ManyToOne
	@JoinColumn(name="status_id",foreignKey=@ForeignKey(name="FK_GRNH_ST"))
	@NotNull
	public Status getGrnStatus() {
		return grnStatus;
	}
	public void setGrnStatus(Status grnStatus) {
		this.grnStatus = grnStatus;
	}
	
	@ManyToOne
	@JoinColumn(name="company_id",foreignKey=@ForeignKey(name="FK_GRNH_COMP"))
	@NotNull
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	@ManyToOne
	@JoinColumn(name="quality_checked_by",foreignKey=@ForeignKey(name="FK_GRNH_USERQ"))
	@NotNull
	public User getQualityCheckedBy() {
		return qualityCheckedBy;
	}
	public void setQualityCheckedBy(User qualityCheckedBy) {
		this.qualityCheckedBy = qualityCheckedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="stock_checked_by",foreignKey=@ForeignKey(name="FK_GRNH_USERST"))
	@NotNull
	public User getStockCheckedBy() {
		return stockCheckedBy;
	}
	public void setStockCheckedBy(User stockCheckedBy) {
		this.stockCheckedBy = stockCheckedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="financial_year_id",foreignKey=@ForeignKey(name="FK_GRNH_FY"))
	@NotNull
	public FinancialYear getFinancialYear() {
		return financialYear;
	}
	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}
	
	@ManyToOne
	@JoinColumn(name="supplier_id",foreignKey=@ForeignKey(name="FK_GRNH_SUP"))
	@NotNull
	public Party getSupplier() {
		return supplier;
	}
	public void setSupplier(Party supplier) {
		this.supplier = supplier;
	}
	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="dc_header_id",foreignKey=@ForeignKey(name="FK_GRNH_DCH"))
	public DeliveryChallanHeader getDeliveryChallanHeader() {
		return deliveryChallanHeader;
	}
	
	public void setDeliveryChallanHeader(DeliveryChallanHeader deliveryChallanHeader) {
		this.deliveryChallanHeader = deliveryChallanHeader;
	}
	
	@Column(name="terms_condition")
	public String getTermsAndConditionNote() {
		return termsAndConditionNote;
	}
	public void setTermsAndConditionNote(String termsAndConditionNote) {
		this.termsAndConditionNote = termsAndConditionNote;
	}
	
	@ManyToOne
	@JoinColumn(name="terms_condition_id",foreignKey=@ForeignKey(name="FK_GRNH_TC"))
	public TermsAndCondition getTermsAndConditon() {
		return termsAndConditon;
	}
	public void setTermsAndConditon(TermsAndCondition termsAndConditon) {
		this.termsAndConditon = termsAndConditon;
	}
	
	@OneToMany
	@JoinTable(
			name="tr_grn_invoice_header_map",
			joinColumns=@JoinColumn(name="grn_id", foreignKey=@ForeignKey(name="FK_GRNH_INVH")),
			inverseJoinColumns=@JoinColumn(name="inv_id", foreignKey=@ForeignKey(name="FK_INVH_GRNH"))
			)
	public List<InvoiceHeader> getInvoiceHeaders() {
		return invoiceHeaders;
	}
	public void setInvoiceHeaders(List<InvoiceHeader> invoiceHeaders) {
		this.invoiceHeaders = invoiceHeaders;
	}
	
	@ManyToOne
	@JoinColumn(name="grn_type_id", foreignKey=@ForeignKey(name="FK_GRNH_TT"))
	public TransactionType getGrnType() {
		return grnType;
	}
	public void setGrnType(TransactionType grnType) {
		this.grnType = grnType;
	}
	@OneToMany(mappedBy="grnHeader",cascade=CascadeType.ALL)
	@OrderBy("slNo ASC")
	public List<GRNItem> getGrnItems() {
		return grnItems;
	}
	public void setGrnItems(List<GRNItem> grnItems) {
		this.grnItems = grnItems;
	}
	
	@ManyToOne
	@JoinColumn(name="tax_id", foreignKey=@ForeignKey(name="FK_GRNH_TAX"))
	public Tax getTax() {
		return tax;
	}
	public void setTax(Tax tax) {
		this.tax = tax;
	}
	
	//............................
	
	
	@Column(name="company_name")
	public String getCompanyName() {
		return companyName;
	}



	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	@Column(name="comp_gst_registration_type_id")
	public Long getCompanygGstRegistrationTypeId() {
		return companygGstRegistrationTypeId;
	}



	public void setCompanygGstRegistrationTypeId(Long companygGstRegistrationTypeId) {
		this.companygGstRegistrationTypeId = companygGstRegistrationTypeId;
	}


	@Column(name="comp_pincode")
	public String getCompanyPinCode() {
		return companyPinCode;
	}



	public void setCompanyPinCode(String companyPinCode) {
		this.companyPinCode = companyPinCode;
	}


	@Column(name="comp_state_id")
	public Long getCompanyStateId() {
		return companyStateId;
	}



	public void setCompanyStateId(Long companyStateId) {
		this.companyStateId = companyStateId;
	}

	@Column(name="comp_state_name")
	public String getCompanyStateName() {
		return companyStateName;
	}

	public void setCompanyStateName(String companyStateName) {
		this.companyStateName = companyStateName;
	}

	@Column(name="comp_country_id")
	public Long getCompanyCountryId() {
		return companyCountryId;
	}



	public void setCompanyCountryId(Long companyCountryId) {
		this.companyCountryId = companyCountryId;
	}


	@Column(name="comp_primary_mobile")
	public String getCompanyPrimaryMobile() {
		return companyPrimaryMobile;
	}



	public void setCompanyPrimaryMobile(String companyPrimaryMobile) {
		this.companyPrimaryMobile = companyPrimaryMobile;
	}


	@Column(name="comp_secondary_mobile")
	public String getCompanySecondaryMobile() {
		return companySecondaryMobile;
	}



	public void setCompanySecondaryMobile(String companySecondaryMobile) {
		this.companySecondaryMobile = companySecondaryMobile;
	}


	@Column(name="comp_contact_person_number")
	public String getCompanyContactPersonNumber() {
		return companyContactPersonNumber;
	}



	public void setCompanyContactPersonNumber(String companyContactPersonNumber) {
		this.companyContactPersonNumber = companyContactPersonNumber;
	}


	@Column(name="comp_contact_person_name")
	public String getCompanyContactPersonName() {
		return companyContactPersonName;
	}



	public void setCompanyContactPersonName(String companyContactPersonName) {
		this.companyContactPersonName = companyContactPersonName;
	}


	@Column(name="comp_primary_telephone")
	public String getCompanyPrimaryTelephone() {
		return companyPrimaryTelephone;
	}



	public void setCompanyPrimaryTelephone(String companyPrimaryTelephone) {
		this.companyPrimaryTelephone = companyPrimaryTelephone;
	}


	@Column(name="comp_secondary_telephone")
	public String getCompanySecondaryTelephone() {
		return companySecondaryTelephone;
	}



	public void setCompanySecondaryTelephone(String companySecondaryTelephone) {
		this.companySecondaryTelephone = companySecondaryTelephone;
	}


	@Column(name="comp_website")
	public String getCompanyWebsite() {
		return companyWebsite;
	}



	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}


	@Column(name="comp_email")
	public String getCompanyEmail() {
		return companyEmail;
	}



	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}


	@Column(name="comp_fax_number")
	public String getCompanyFaxNumber() {
		return companyFaxNumber;
	}



	public void setCompanyFaxNumber(String companyFaxNumber) {
		this.companyFaxNumber = companyFaxNumber;
	}


	@Column(name="comp_address")
	public String getCompanyAddress() {
		return companyAddress;
	}



	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	@Column(name="comp_tagline")
	public String getCompanyTagLine() {
		return companyTagLine;
	}

	public void setCompanyTagLine(String companyTagLine) {
		this.companyTagLine = companyTagLine;
	}




	@Column(name="comp_gst_number")
	public String getCompanyGstNumber() {
		return companyGstNumber;
	}



	public void setCompanyGstNumber(String companyGstNumber) {
		this.companyGstNumber = companyGstNumber;
	}



	@Column(name="comp_pan_number")

	public String getCompanyPanNumber() {
		return companyPanNumber;
	}

	public void setCompanyPanNumber(String companyPanNumber) {
		this.companyPanNumber = companyPanNumber;
	}


	@Column(name="comp_pan_date")
	public String getCompanyPanDate() {
		return companyPanDate;
	}



	public void setCompanyPanDate(String companyPanDate) {
		this.companyPanDate = companyPanDate;
	}


	@Column(name="comp_certificate_image_path")
	public String getCompanyCeritificateImagePath() {
		return companyCeritificateImagePath;
	}



	public void setCompanyCeritificateImagePath(String companyCeritificateImagePath) {
		this.companyCeritificateImagePath = companyCeritificateImagePath;
	}


	@Column(name="comp_logo_path")
	public String getCompanyLogoPath() {
		return companyLogoPath;
	}



	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}
	
	@Column(name="party_name")
	public String getPartyName() {
		return partyName;
	}



	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}


	@Column(name="party_contact_person_number")
	public String getPartyContactPersonNumber() {
		return partyContactPersonNumber;
	}



	public void setPartyContactPersonNumber(String partyContactPersonNumber) {
		this.partyContactPersonNumber = partyContactPersonNumber;
	}


	@Column(name="party_pin_code")
	public String getPartyPinCode() {
		return partyPinCode;
	}



	public void setPartyPinCode(String partyPinCode) {
		this.partyPinCode = partyPinCode;
	}


	@Column(name="party_area_id")
	public Long getPartyAreaId() {
		return partyAreaId;
	}



	public void setPartyAreaId(Long partyAreaId) {
		this.partyAreaId = partyAreaId;
	}


	@Column(name="party_city_id")
	public Long getPartyCityId() {
		return partyCityId;
	}



	public void setPartyCityId(Long partyCityId) {
		this.partyCityId = partyCityId;
	}


	@Column(name="party_state_id")
	public Long getPartyStateId() {
		return partyStateId;
	}



	public void setPartyStateId(Long partyStateId) {
		this.partyStateId = partyStateId;
	}


	@Column(name="party_country_id")
	public Long getPartyCountryId() {
		return partyCountryId;
	}



	public void setPartyCountryId(Long partyCountryId) {
		this.partyCountryId = partyCountryId;
	}


	@Column(name="party_primary_telephone")
	public String getPartyPrimaryTelephone() {
		return partyPrimaryTelephone;
	}



	public void setPartyPrimaryTelephone(String partyPrimaryTelephone) {
		this.partyPrimaryTelephone = partyPrimaryTelephone;
	}


	@Column(name="party_secondary_telephone")
	public String getPartySecondaryTelephone() {
		return partySecondaryTelephone;
	}


	public void setPartySecondaryTelephone(String partySecondaryTelephone) {
		this.partySecondaryTelephone = partySecondaryTelephone;
	}


	@Column(name="party_primary_mobile_number")
	public String getPartyPrimaryMobile() {
		return partyPrimaryMobile;
	}



	public void setPartyPrimaryMobile(String partyPrimaryMobile) {
		this.partyPrimaryMobile = partyPrimaryMobile;
	}


	@Column(name="party_secondary_mobile")
	public String getPartySecondaryMobile() {
		return partySecondaryMobile;
	}



	public void setPartySecondaryMobile(String partySecondaryMobile) {
		this.partySecondaryMobile = partySecondaryMobile;
	}


	@Column(name="party_email")
	public String getPartyEmail() {
		return partyEmail;
	}



	public void setPartyEmail(String partyEmail) {
		this.partyEmail = partyEmail;
	}


	@Column(name="party_website")
	public String getPartyWebsite() {
		return partyWebsite;
	}



	public void setPartyWebsite(String partyWebsite) {
		this.partyWebsite = partyWebsite;
	}


	@Column(name="party_contact_person_name")
	public String getPartyContactPersonName() {
		return partyContactPersonName;
	}



	public void setPartyContactPersonName(String partyContactPersonName) {
		this.partyContactPersonName = partyContactPersonName;
	}


//	@Column(name="party_bill_to_address")
//	public String getPartyBillToAddress() {
//		return partyBillToAddress;
//	}
//
//
//
//	public void setPartyBillToAddress(String partyBillToAddress) {
//		this.partyBillToAddress = partyBillToAddress;
//	}
//
//
//	@Column(name="party_ship_to_address")
//	public String getPartyShipAddress() {
//		return partyShipAddress;
//	}
//
//
//
//	public void setPartyShipAddress(String partyShipAddress) {
//		this.partyShipAddress = partyShipAddress;
//	}


	@Column(name="party_due_days_limit")
	public String getPartyDueDaysLimit() {
		return partyDueDaysLimit;
	}



	public void setPartyDueDaysLimit(String partyDueDaysLimit) {
		this.partyDueDaysLimit = partyDueDaysLimit;
	}


	@Column(name="party_gst_registration_type_id")
	public String getPartyGstRegistrationTypeId() {
		return partyGstRegistrationTypeId;
	}



	public void setPartyGstRegistrationTypeId(String partyGstRegistrationTypeId) {
		this.partyGstRegistrationTypeId = partyGstRegistrationTypeId;
	}


//	@Column(name="party_gst_number")
//	public String getPartyGstNumber() {
//		return partyGstNumber;
//	}
//
//
//
//	public void setPartyGstNumber(String partyGstNumber) {
//		this.partyGstNumber = partyGstNumber;
//	}


	@Column(name="party_pan_number")
	public String getPartyPanNumber() {
		return partyPanNumber;
	}



	public void setPartyPanNumber(String partyPanNumber) {
		this.partyPanNumber = partyPanNumber;
	}


	@Column(name="is_igst")
	public String getIsIgst() {
		return isIgst;
	}



	public void setIsIgst(String isIgst) {
		this.isIgst = isIgst;
	}
   
}
