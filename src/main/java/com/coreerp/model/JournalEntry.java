package com.coreerp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tr_journal_entry")
public class JournalEntry extends AuditModel {
	
	private Long id;

	private Date businessDate;
	
	
	private Date transactionDate;
	
	private TransactionType transactionType;
		
	private String transactionId;
	
	private Integer transactionIdSequence;
	
	private Party party;
	
	private Double amount;
	
	private Account debitAccount;
	
	private Account creditAccount;
		
	private String isReversal;
	
	private String remarks;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="business_date")
	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	@Column(name="transaction_date")
	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	@ManyToOne
	@JoinColumn(name="transaction_type_id", foreignKey=@ForeignKey(name="FK_JE_TRANTYPE"))
	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	@Column(name="transaction_id")
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	
	@Column(name="transaction_id_seq")
	public Integer getTransactionIdSequence() {
		return transactionIdSequence;
	}

	public void setTransactionIdSequence(Integer transactionIdSequence) {
		this.transactionIdSequence = transactionIdSequence;
	}

	@ManyToOne
	@JoinColumn(name="party_id", foreignKey=@ForeignKey(name="FK_JE_PARTY"))
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@Column(name="amt")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	@ManyToOne
	@JoinColumn(name="dr_acct_id", foreignKey=@ForeignKey(name="FK_JE_DRACCT"))
	public Account getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(Account debitAccount) {
		this.debitAccount = debitAccount;
	}
	
	@ManyToOne
	@JoinColumn(name="cr_acct_id", foreignKey=@ForeignKey(name="FK_JE_CRACCT"))
	public Account getCreditAccount() {
		return creditAccount;
	}

	public void setCreditAccount(Account creditAccount) {
		this.creditAccount = creditAccount;
	}

	@Column(name="is_reversal")
	public String getIsReversal() {
		return isReversal;
	}

	public void setIsReversal(String isReversal) {
		this.isReversal = isReversal;
	}

	@Column(name="remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}
