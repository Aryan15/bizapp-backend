package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name="ma_print_copy")
@Where(clause = "deleted='N'")
public class PrintCopy extends AuditModelAbstractEntity  {

	private String deleted="N";
	private String name;
	
	@Column(name="deleted")
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
