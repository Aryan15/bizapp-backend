package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;

import com.coreerp.controller.MaterialController;

@Entity
@Table(name="ma_material", uniqueConstraints = {
	      @UniqueConstraint(name="UQ_MATERIAL_PARTNUMBER", columnNames={"number"})})
public class Material extends AuditModelAbstractEntity{

	final static Logger log = LogManager.getLogger(Material.class);
	
	private String deleted="N";
	private String name;
	private String partNumber;
	private SupplyType supplyType;
	private MaterialType materialType;
	private UnitOfMeasurement unitOfMeasurement;
	private Double price;
	private String specification;
	private Double stock;
	private Company company;
	private Double buyingPrice;
	private Tax tax;
	private Double mrp;
	private Double discountPercentage;
	private Party party;
	private String hsnCode;
	private Double cessPercentage;
	private String imagePath;
	private Double openingStock;
	private Double minimumStock;
	private Integer isContainer;
	private String outName;
	private String outPartNumber;

	@Column(name="deleted")
	@NotNull
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		if(deleted != null){
			this.deleted = deleted;
		}
	}
	
	@Column(name="name")
	@NotNull
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	@Column(name="number")
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	
	@ManyToOne
	@JoinColumn(name="supply_type_id", foreignKey=@ForeignKey(name="FK_MAT_SUPTYPE"))
	@NotNull
	public SupplyType getSupplyType() {
		return supplyType;
	}
	public void setSupplyType(SupplyType supplyType) {
		this.supplyType = supplyType;
	}
	@ManyToOne
	@JoinColumn(name="material_type_id", foreignKey=@ForeignKey(name="FK_MAT_MATTYPE"))
	//@NotNull
	public MaterialType getMaterialType() {
		return materialType;
	}
	public void setMaterialType(MaterialType materialType) {
		this.materialType = materialType;
	}
	@ManyToOne
	@JoinColumn(name="uom_id", foreignKey=@ForeignKey(name="FK_MAT_UOM"))
//	@NotNull
	public UnitOfMeasurement getUnitOfMeasurement() {
		return unitOfMeasurement;
	}
	public void setUnitOfMeasurement(UnitOfMeasurement unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}
	@Column(name="price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@Column(name="specification")
	public String getSpecification() {
		return specification;
	}
	public void setSpecification(String specification) {
		this.specification = specification;
	}
	@Column(name="stock")
	public Double getStock() {
//		if(openingStock != null && openingStock > 0){
//			return openingStock + stock;
//		}
//		else{
//			return stock;	
//		}
		//log.info("openingStock "+openingStock );
		//log.info("stock "+stock );
		//return openingStock != null  ? (openingStock + (stock != null ? stock : 0)) : (0 + (stock != null ? stock : 0));
		return stock;
		
	}
	public void setStock(Double stock) {
		this.stock = stock;
	}
	@ManyToOne
	@JoinColumn(name="company_id", foreignKey=@ForeignKey(name="FK_MAT_COMP"))
	@NotNull
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	@Column(name="buying_price")
	public Double getBuyingPrice() {
		return buyingPrice;
	}
	public void setBuyingPrice(Double buyingPrice) {
		this.buyingPrice = buyingPrice;
	}
	@ManyToOne
	@JoinColumn(name="tax_id", foreignKey=@ForeignKey(name="FK_MAT_TAX"))
//	@NotNull
	public Tax getTax() {
		return tax;
	}
	public void setTax(Tax tax) {
		this.tax = tax;
	}
	@Column(name="mrp")
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	@Column(name="discount_percentage")
	public Double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	@ManyToOne
	@JoinColumn(name="party_id", foreignKey=@ForeignKey(name="FK_MAT_PARTY"))
	public Party getParty() {
		return party;
	}
	public void setParty(Party party) {
		this.party = party;
	}
	@Column(name="hsn_code")
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	@Column(name="cess_percentage")
	public Double getCessPercentage() {
		return cessPercentage;
	}
	public void setCessPercentage(Double cessPercentage) {
		this.cessPercentage = cessPercentage;
	}
	
	@Column(name="image_path", length=1000)
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	@Override
	public String toString() {
		return "Material [deleted=" + deleted + ", name=" + name + ", partNumber=" + partNumber + ", supplyType="
				+ supplyType + ", materialType=" + materialType + ", unitOfMeasurement=" + unitOfMeasurement
				+ ", price=" + price + ", specification=" + specification + ", stock=" + stock 
				+ ", buyingPrice=" + buyingPrice + ", tax=" + tax + ", mrp=" + mrp + ", discountPercentage="
				+ discountPercentage + ", hsnCode=" + hsnCode + ", cessPercentage="
				+ cessPercentage + ", imagePath=" + imagePath + "]";
	}
	@Column(name="opening_stock")
	public Double getOpeningStock() {
		return openingStock;
	}
	public void setOpeningStock(Double openingStock) {
		this.openingStock = openingStock;
	}
	@Column(name="minimum_stock")
	public Double getMinimumStock() {
		return minimumStock;
	}
	public void setMinimumStock(Double minimumStock) {
		this.minimumStock = minimumStock;
	}

	@Column(name="out_name")
	public String getOutName() {
		return outName;
	}

	public void setOutName(String outName) {
		this.outName = outName;
	}

	@Column(name="out_number")
	public String getOutPartNumber() {
		return outPartNumber;
	}

	public void setOutPartNumber(String outPartNumber) {
		this.outPartNumber = outPartNumber;
	}

	@Column(name="is_container")
	public Integer getIsContainer() {
		return isContainer;
	}

	public void setIsContainer(Integer isContainer) {
		this.isContainer = isContainer;
	}
}
