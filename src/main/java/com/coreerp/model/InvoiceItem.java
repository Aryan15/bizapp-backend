package com.coreerp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_invoice_item")
public class InvoiceItem extends AbstractTransactionModel{

	private InvoiceHeader invoiceHeader;
	private Material material;
    private Integer slNo;
    private Double quantity;
    private Double price;
    private Double amount;
    //private PurchaseOrderHeader purchaseOrderHeader;
    private PurchaseOrderItem purchaseOrderItem;
    //private DeliveryChallanHeader deliveryChallanHeader ;
    private DeliveryChallanItem deliveryChallanItem;
    //private GRNHeader grnHeader ;
    private GRNItem grnItem;
    //private InvoiceHeader proformaInvoiceHeader;
    private InvoiceItem proformaInvoiceItem;
    //private InvoiceHeader sourceInvoiceHeader;
    private InvoiceItem sourceInvoiceItem;    
    private Double basicAmount;
    private Double taxPercent;
    private Double taxAmount;
    private Double amountAfterTax;
    private Double discountPercentage;
    private Double discountAmount;
    private Double amountAfterDiscount;    
    private UnitOfMeasurement unitOfMeasurement;
    private Tax tax;
    private Integer inclusiveTax;
    private Double taxRate;
    private String remarks;
    private String outDeliveryChallanNumber;
    private String batchCode;
    private Date outDeliveryChallanDate;    
    private String inspectionNumber;
    private String inDeliveryChallanNumber;
    private Date inDeliveryChallanDate;
    private String descriptionOfPackages;
    private String averageContentPerPackage;
    private Double processQuantity;
    private Double transportationAmount;
    private Double cessPercentage;
    private Double cessAmount;
    private Double igstTaxPercentage;
    private Double igstTaxAmount;
    private Double sgstTaxPercentage;
    private Double sgstTaxAmount;
    private Double cgstTaxPercentage;
    private Double cgstTaxAmount;
    private Double noteBalanceQuantity;
    private String specification;
    private String processId;
    private String processName;
	@Column(name="sl_no")
	public Integer getSlNo() {
		return slNo;
	}


	
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	@Column(name="quantity")
	public Double getQuantity() {
		return quantity;
	}


	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	@Column(name="price")
	public Double getPrice() {
		return price;
	}


	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name="amount")
	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name="disc_per")
	public Double getDiscountPercentage() {
		return discountPercentage;
	}


	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	@Column(name="disc_amt")
	public Double getDiscountAmount() {
		return discountAmount;
	}


	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	@Column(name="amt_after_disc")
	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}


	public void setAmountAfterDiscount(Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}

	@Column(name="transportation_amt")
	public Double getTransportationAmount() {
		return transportationAmount;
	}


	public void setTransportationAmount(Double transportationAmount) {
		this.transportationAmount = transportationAmount;
	}

	@Column(name="cess_per")
	public Double getCessPercentage() {
		return cessPercentage;
	}


	public void setCessPercentage(Double cessPercentage) {
		this.cessPercentage = cessPercentage;
	}

	@Column(name="cess_amt")
	public Double getCessAmount() {
		return cessAmount;
	}


	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}

	@Column(name="igst_tax_per")
	public Double getIgstTaxPercentage() {
		return igstTaxPercentage;
	}


	public void setIgstTaxPercentage(Double igstTaxPercentage) {
		this.igstTaxPercentage = igstTaxPercentage;
	}

	@Column(name="igst_tax_amt")
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}


	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	@Column(name="sgst_tax_per")
	public Double getSgstTaxPercentage() {
		return sgstTaxPercentage;
	}


	public void setSgstTaxPercentage(Double sgstTaxPercentage) {
		this.sgstTaxPercentage = sgstTaxPercentage;
	}

	@Column(name="sgst_tax_amt")
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}


	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}

	@Column(name="cgst_tax_per")
	public Double getCgstTaxPercentage() {
		return cgstTaxPercentage;
	}


	public void setCgstTaxPercentage(Double cgstTaxPercentage) {
		this.cgstTaxPercentage = cgstTaxPercentage;
	}

	@Column(name="cgst_tax_amt")
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}


	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}

	@Column(name="amt_after_tax")
	public Double getAmountAfterTax() {
		return amountAfterTax;
	}


	public void setAmountAfterTax(Double amountAfterTax) {
		this.amountAfterTax = amountAfterTax;
	}

	@Column(name="remarks")
	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "invoice_header_id", foreignKey=@ForeignKey(name="FK_INVI_INVH"))
    @NotNull
    public InvoiceHeader getInvoiceHeader() {
        return invoiceHeader;
    }


	public void setInvoiceHeader(InvoiceHeader invoiceHeader) {
		this.invoiceHeader = invoiceHeader;
	}

	@ManyToOne
	@JoinColumn(name="material_id", foreignKey=@ForeignKey(name="FK_INVI_MATERIAL"))
	@NotNull
	public Material getMaterial() {
		return material;
	}


	public void setMaterial(Material material) {
		this.material = material;
	}

//	@ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "po_header_id", foreignKey=@ForeignKey(name="FK_INVI_POH"))
//	public PurchaseOrderHeader getPurchaseOrderHeader() {
//		return purchaseOrderHeader;
//	}
//
//
//	public void setPurchaseOrderHeader(PurchaseOrderHeader purchaseOrderHeader) {
//		this.purchaseOrderHeader = purchaseOrderHeader;
//	}


	
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "dc_header_id", foreignKey=@ForeignKey(name="FK_INVI_DCH"))
//	public DeliveryChallanHeader getDeliveryChallanHeader() {
//		return deliveryChallanHeader;
//	}
//
//
//	public void setDeliveryChallanHeader(DeliveryChallanHeader deliveryChallanHeader) {
//		this.deliveryChallanHeader = deliveryChallanHeader;
//	}


	@Column(name="basic_amt")
	public Double getBasicAmount() {
		return basicAmount;
	}


	public void setBasicAmount(Double basicAmount) {
		this.basicAmount = basicAmount;
	}

	@Column(name="tax_percent")
	public Double getTaxPercent() {
		return taxPercent;
	}


	public void setTaxPercent(Double taxPercent) {
		this.taxPercent = taxPercent;
	}

	@Column(name="tax_amt")
	public Double getTaxAmount() {
		return taxAmount;
	}


	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	@ManyToOne
	@JoinColumn(name="uom_id", foreignKey=@ForeignKey(name="FK_INVI_UOM"))
	//@NotNull
	public UnitOfMeasurement getUnitOfMeasurement() {
		return unitOfMeasurement;
	}


	public void setUnitOfMeasurement(UnitOfMeasurement unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}

	@ManyToOne
	@JoinColumn(name="tax_id", foreignKey=@ForeignKey(name="FK_INVI_TAX"))
	public Tax getTax() {
		return tax;
	}


	public void setTax(Tax tax) {
		this.tax = tax;
	}

	@Column(name="inclusive_tax")
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}


	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	@Column(name="tax_rate")
	public Double getTaxRate() {
		return taxRate;
	}


	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	@Column(name="dc_number")
	public String getOutDeliveryChallanNumber() {
		return outDeliveryChallanNumber;
	}


	public void setOutDeliveryChallanNumber(String outDeliveryChallanNumber) {
		this.outDeliveryChallanNumber = outDeliveryChallanNumber;
	}

	@Column(name="batch_code")
	public String getBatchCode() {
		return batchCode;
	}


	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	@Column(name="out_dc_date")
	public Date getOutDeliveryChallanDate() {
		return outDeliveryChallanDate;
	}


	public void setOutDeliveryChallanDate(Date outDeliveryChallanDate) {
		this.outDeliveryChallanDate = outDeliveryChallanDate;
	}

	@Column(name="inspection_number")
	public String getInspectionNumber() {
		return inspectionNumber;
	}


	public void setInspectionNumber(String inspectionNumber) {
		this.inspectionNumber = inspectionNumber;
	}

	@Column(name="in_dc_number")
	public String getInDeliveryChallanNumber() {
		return inDeliveryChallanNumber;
	}


	public void setInDeliveryChallanNumber(String inDeliveryChallanNumber) {
		this.inDeliveryChallanNumber = inDeliveryChallanNumber;
	}

	@Column(name="in_dc_date")
	public Date getInDeliveryChallanDate() {
		return inDeliveryChallanDate;
	}


	public void setInDeliveryChallanDate(Date inDeliveryChallanDate) {
		this.inDeliveryChallanDate = inDeliveryChallanDate;
	}

	@Column(name="description_of_packages")
	public String getDescriptionOfPackages() {
		return descriptionOfPackages;
	}


	public void setDescriptionOfPackages(String descriptionOfPackages) {
		this.descriptionOfPackages = descriptionOfPackages;
	}

	@Column(name="average_content_per_package")
	public String getAverageContentPerPackage() {
		return averageContentPerPackage;
	}


	public void setAverageContentPerPackage(String averageContentPerPackage) {
		this.averageContentPerPackage = averageContentPerPackage;
	}

	@Column(name="process_qty")
	public Double getProcessQuantity() {
		return processQuantity;
	}


	public void setProcessQuantity(Double processQuantity) {
		this.processQuantity = processQuantity;
	}


    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "po_item_id", foreignKey=@ForeignKey(name="FK_INVI_POI"))
	public PurchaseOrderItem getPurchaseOrderItem() {
		return purchaseOrderItem;
	}



	public void setPurchaseOrderItem(PurchaseOrderItem purchaseOrderItem) {
		this.purchaseOrderItem = purchaseOrderItem;
	}


//	@ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "grn_header_id", foreignKey=@ForeignKey(name="FK_INVI_GRNH"))
//    public GRNHeader getGrnHeader() {
//		return grnHeader;
//	}
//
//
//
//	public void setGrnHeader(GRNHeader grnHeader) {
//		this.grnHeader = grnHeader;
//	}


	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "grn_item_id", foreignKey=@ForeignKey(name="FK_INVI_GRNI"))
	public GRNItem getGrnItem() {
		return grnItem;
	}



	public void setGrnItem(GRNItem grnItem) {
		this.grnItem = grnItem;
	}



	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "dc_item_id", foreignKey=@ForeignKey(name="FK_INVI_DCI"))
	public DeliveryChallanItem getDeliveryChallanItem() {
		return deliveryChallanItem;
	}



	public void setDeliveryChallanItem(DeliveryChallanItem deliveryChallanItem) {
		this.deliveryChallanItem = deliveryChallanItem;
	}


//	  @ManyToOne(fetch = FetchType.LAZY)
//	  @JoinColumn(name = "proforma_invoice_header_id", foreignKey=@ForeignKey(name="FK_INVI_PROH"))
//	public InvoiceHeader getProformaInvoiceHeader() {
//		return proformaInvoiceHeader;
//	}
//
//
//
//	public void setProformaInvoiceHeader(InvoiceHeader proformaInvoiceHeader) {
//		this.proformaInvoiceHeader = proformaInvoiceHeader;
//	}


	  @OneToOne(fetch=FetchType.LAZY)
	  @JoinColumn(name = "proforma_invoice_item_id", foreignKey=@ForeignKey(name="FK_INVI_PROI"))
	public InvoiceItem getProformaInvoiceItem() {
		return proformaInvoiceItem;
	}



	public void setProformaInvoiceItem(InvoiceItem proformaInvoiceItem) {
		this.proformaInvoiceItem = proformaInvoiceItem;
	}


//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "source_invoice_header_id", foreignKey=@ForeignKey(name="FK_INVI_SRCH"))
//	public InvoiceHeader getSourceInvoiceHeader() {
//		return sourceInvoiceHeader;
//	}
//
//
//
//	public void setSourceInvoiceHeader(InvoiceHeader sourceInvoiceHeader) {
//		this.sourceInvoiceHeader = sourceInvoiceHeader;
//	}


	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "source_invoice_item_id", foreignKey=@ForeignKey(name="FK_INVI_SRCI"))
	public InvoiceItem getSourceInvoiceItem() {
		return sourceInvoiceItem;
	}



	public void setSourceInvoiceItem(InvoiceItem sourceInvoiceItem) {
		this.sourceInvoiceItem = sourceInvoiceItem;
	}


	@Column(name="note_balance_quantity")
	public Double getNoteBalanceQuantity() {
		return noteBalanceQuantity;
	}



	public void setNoteBalanceQuantity(Double noteBalanceQuantity) {
		this.noteBalanceQuantity = noteBalanceQuantity;
	}


	@Column(name="specification")
	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	@Column(name="process_id")
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	
	@Column(name="process_name")
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}	
	
}
