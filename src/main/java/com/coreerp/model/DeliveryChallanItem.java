package com.coreerp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_delivery_challan_item")
public class DeliveryChallanItem extends AbstractTransactionModel{

	private DeliveryChallanHeader deliveryChallanHeader;
	
	private Material material;

	private Integer slNo;
	
	private Double quantity;
	
	private String remarks;
	
	private String status;
	
	private Double price;
	
	private Double amount;
	
	private UnitOfMeasurement unitOfMeasurement;
	
	private Tax tax;
	
	private Double taxRate;
	
	private String deliveryChallanType;
	
	private String deliveryChallanItemStatus;
	
	//private String process;
	
	private Double invoiceBalanceQuantity;
	
	private String batchCode;
	
	private String purchaseOrderNumber;
	
	private String batchCodeId;
	
	private String inDeliveryChallanNumber;
	
	private Date inDeliveryChallanDate;
	
	private Double sgstTaxRate;
	
	private Double sgstTaxAmount;
	
	private Double cgstTaxRate;
	
	private Double cgstTaxAmount;
	
	private Double igstTaxRate;
	
	private Double igstTaxAmount;
	
	private Double taxAmount;

	//private PurchaseOrderHeader purchaseOrderHeader;
	
	private PurchaseOrderItem purchaseOrderItem;

	private Integer inclusiveTax;

    private Double discountPercentage;
    
    private Double discountAmount;
    
    private Double amountAfterDiscount;   
    
    private Double grnBalanceQuantity;
    
    //private DeliveryChallanHeader sourceDeliveryChallanHeader ;
    
    private DeliveryChallanItem sourceDeliveryChallanItem;
    
    private Double incomingQuantity;    
    
    private Double dcBalanceQuantity;
    
    private String specification;
    
    private String processId;
    private String processName;
	private String jwNoteItemId;
	@ManyToOne
	@JoinColumn(name="dc_header_id", foreignKey=@ForeignKey(name="FK_DCI_DCH"))
	@NotNull
	public DeliveryChallanHeader getDeliveryChallanHeader() {
		return deliveryChallanHeader;
	}

	public void setDeliveryChallanHeader(DeliveryChallanHeader deliveryChallanHeader) {
		this.deliveryChallanHeader = deliveryChallanHeader;
	}

	@ManyToOne
	@JoinColumn(name="material_id", foreignKey=@ForeignKey(name="FK_DCI_MATERIAL"))
	@NotNull
	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}



	@Column(name="quantity")
	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	@Column(name="remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	@Column(name="price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name="amt")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@ManyToOne
	@JoinColumn(name="uom_id", foreignKey=@ForeignKey(name="FK_DCI_UOM"))
	//@NotNull
	public UnitOfMeasurement getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public void setUnitOfMeasurement(UnitOfMeasurement unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}
	
	@ManyToOne
	@JoinColumn(name="tax_id", foreignKey=@ForeignKey(name="FK_DCI_TAX"))
	//@NotNull
	public Tax getTax() {
		return tax;
	}

	public void setTax(Tax tax) {
		this.tax = tax;
	}

	@Column(name="tax_rate")
	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	@Column(name="dc_type")
	public String getDeliveryChallanType() {
		return deliveryChallanType;
	}

	public void setDeliveryChallanType(String deliveryChallanType) {
		this.deliveryChallanType = deliveryChallanType;
	}

	@Column(name="dc_item_status")
	public String getDeliveryChallanItemStatus() {
		return deliveryChallanItemStatus;
	}

	public void setDeliveryChallanItemStatus(String deliveryChallanItemStatus) {
		this.deliveryChallanItemStatus = deliveryChallanItemStatus;
	}

//	@Column(name="process")
//	public String getProcess() {
//		return process;
//	}
//
//	public void setProcess(String process) {
//		this.process = process;
//	}



	@Column(name="batch_code")
	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	@Column(name="po_no")
	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	@Column(name="batch_code_id")
	public String getBatchCodeId() {
		return batchCodeId;
	}

	public void setBatchCodeId(String batchCodeId) {
		this.batchCodeId = batchCodeId;
	}

	@Column(name="in_dc_number")
	public String getInDeliveryChallanNumber() {
		return inDeliveryChallanNumber;
	}

	public void setInDeliveryChallanNumber(String inDeliveryChallanNumber) {
		this.inDeliveryChallanNumber = inDeliveryChallanNumber;
	}

	@Column(name="in_dc_date")
	public Date getInDeliveryChallanDate() {
		return inDeliveryChallanDate;
	}

	public void setInDeliveryChallanDate(Date inDeliveryChallanDate) {
		this.inDeliveryChallanDate = inDeliveryChallanDate;
	}

	@Column(name="sgst_tax_rate")
	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}

	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	@Column(name="sgst_tax_amt")
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}

	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}

	@Column(name="cgst_tax_rate")
	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}

	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}

	@Column(name="cgst_tax_amt")
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}

	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}

	@Column(name="igst_tax_rate")
	public Double getIgstTaxRate() {
		return igstTaxRate;
	}

	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}

	@Column(name="igst_tax_amt")
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}

	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	@Column(name="tax_amt")
	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

//	@ManyToOne
//    @JoinColumn(name = "po_header_id", foreignKey=@ForeignKey(name="FK_DCI_POH"))
//	public PurchaseOrderHeader getPurchaseOrderHeader() {
//		return purchaseOrderHeader;
//	}
//
//	public void setPurchaseOrderHeader(PurchaseOrderHeader purchaseOrderHeader) {
//		this.purchaseOrderHeader = purchaseOrderHeader;
//	}

	@OneToOne
    @JoinColumn(name = "po_item_id", foreignKey=@ForeignKey(name="FK_DCI_POI"))
	public PurchaseOrderItem getPurchaseOrderItem() {
		return purchaseOrderItem;
	}

	public void setPurchaseOrderItem(PurchaseOrderItem purchaseOrderItem) {
		this.purchaseOrderItem = purchaseOrderItem;
	}

	@Column(name="inclusive_tax")
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}

	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	@Column(name="disc_per")
	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	@Column(name="disc_amt")
	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	@Column(name="amt_after_disc")
	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}

	public void setAmountAfterDiscount(Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}

	@Column(name="invoice_balance_quantity")
	@Min(0)
	public Double getInvoiceBalanceQuantity() {
		return invoiceBalanceQuantity;
	}

	public void setInvoiceBalanceQuantity(Double invoiceBalanceQuantity) {
		this.invoiceBalanceQuantity = invoiceBalanceQuantity;
	}

	@Column(name="grn_balance_quantity")
	@Min(0)
	public Double getGrnBalanceQuantity() {
		return grnBalanceQuantity;
	}

	public void setGrnBalanceQuantity(Double grnBalanceQuantity) {
		this.grnBalanceQuantity = grnBalanceQuantity;
	}

	
	
	
//	@ManyToOne
//    @JoinColumn(name = "src_dc_header_id", foreignKey=@ForeignKey(name="FK_DCI_DCHIN"))
//	public DeliveryChallanHeader getSourceDeliveryChallanHeader() {
//		return sourceDeliveryChallanHeader;
//	}
//
//	public void setSourceDeliveryChallanHeader(DeliveryChallanHeader sourceDeliveryChallanHeader) {
//		this.sourceDeliveryChallanHeader = sourceDeliveryChallanHeader;
//	}

	@OneToOne
    @JoinColumn(name = "src_dc_item_id", foreignKey=@ForeignKey(name="FK_DCI_DCIIN"))
	public DeliveryChallanItem getSourceDeliveryChallanItem() {
		return sourceDeliveryChallanItem;
	}

	public void setSourceDeliveryChallanItem(DeliveryChallanItem sourceDeliveryChallanItem) {
		this.sourceDeliveryChallanItem = sourceDeliveryChallanItem;
	}
	
	

	@Column(name="dc_balance_quantity")
	public Double getDcBalanceQuantity() {
		return dcBalanceQuantity;
	}

	public void setDcBalanceQuantity(Double dcBalanceQuantity) {
		this.dcBalanceQuantity = dcBalanceQuantity;
	}

	@Column(name="incoming_quantity")
	public Double getIncomingQuantity() {
		return incomingQuantity;
	}


	

	public void setIncomingQuantity(Double incomingQuantity) {
		this.incomingQuantity = incomingQuantity;
	}


	@Override
	public String toString() {
		return "DeliveryChallanItem [material=" + material + ", quantity=" + quantity + ", remarks=" + remarks
				+ ", status=" + status + ", price=" + price + ", amount=" + amount + ", unitOfMeasurement="
				+ unitOfMeasurement + ", tax=" + tax + ", taxRate=" + taxRate + ", deliveryChallanType="
				+ deliveryChallanType + ", deliveryChallanItemStatus=" + deliveryChallanItemStatus + ", invoiceBalanceQuantity=" + invoiceBalanceQuantity + ", batchCode=" + batchCode
				+ ", purchaseOrderNumber=" + purchaseOrderNumber + ", batchCodeId=" + batchCodeId
				+ ", inDeliveryChallanNumber=" + inDeliveryChallanNumber + ", inDeliveryChallanDate="
				+ inDeliveryChallanDate + ", sgstTaxRate=" + sgstTaxRate + ", sgstTaxAmount=" + sgstTaxAmount
				+ ", cgstTaxRate=" + cgstTaxRate + ", cgstTaxAmount=" + cgstTaxAmount + ", igstTaxRate=" + igstTaxRate
				+ ", igstTaxAmount=" + igstTaxAmount + ", taxAmount=" + taxAmount + ", purchaseOrderHeader="
				+ purchaseOrderItem + ", inclusiveTax=" + inclusiveTax
				+ ", discountPercentage=" + discountPercentage + ", discountAmount=" + discountAmount
				+ ", amountAfterDiscount=" + amountAfterDiscount + ", grnBalanceQuantity=" + grnBalanceQuantity + "]";
	}

	@Column(name="sl_no")
	public Integer getSlNo() {
		return slNo;
	}


	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	
	@Column(name="specification")
	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}
	
	@Column(name="process_id")
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	
	@Column(name="process_name")
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	@Column(name="jw_note_item_id")
	public String getJwNoteItemId() {
		return jwNoteItemId;
	}

	public void setJwNoteItemId(String jwNoteItemId) {
		this.jwNoteItemId = jwNoteItemId;
	}
}
