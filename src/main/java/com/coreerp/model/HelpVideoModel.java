package com.coreerp.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="conf_help_videos")
public class HelpVideoModel extends AuditModelAbstractEntity {
    private String header;

    private String description;
    private Integer order;
    private Long activityId;

    private Long transactionTypeId;

    private String videoLink;


    @Column(name="header")
    public String getHeader() {
        return header;
    }


    public void setHeader(String header) {
        this.header = header;
    }
    @Column(name="description")
    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name="orders")
    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Column(name="activity_id")
    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    @Column(name="transaction_type_id")
    public Long getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(Long transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    @Column(name="video_link")
    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }
}
