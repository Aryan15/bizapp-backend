package com.coreerp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tr_payable_receivable_item")
public class PayableReceivableItem extends AbstractTransactionModel {

	private PayableReceivableHeader payableReceivableHeader;
	private InvoiceHeader invoiceHeader;
	private Date invoiceDate;
	private Date paymentDueDate;
	private Double invoiceAmount;
	private Double paidAmount;
	private Double dueAmount;
	private Double payingAmount;
	private String invoiceStatus;
	private String paymentItemStatus;
	private Double tdsPercentage;
	private Double tdsAmount;
	private Double amountAfterTds;
	private Double debitAmount;
	private InvoiceHeader creditDebitHeader;
	private Integer slNo;
	private Double taxableAmount;
	private Double totalTaxAmount;
	private Double roundOff;
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "payable_receivable_header_id", foreignKey=@ForeignKey(name="FK_PRI_PRH"))
    @NotNull
	public PayableReceivableHeader getPayableReceivableHeader() {
		return payableReceivableHeader;
	}
	public void setPayableReceivableHeader(PayableReceivableHeader payableReceivableHeader) {
		this.payableReceivableHeader = payableReceivableHeader;
	}
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "invoice_header_id", foreignKey=@ForeignKey(name="FK_PRI_INVH"))
    @NotNull
	public InvoiceHeader getInvoiceHeader() {
		return invoiceHeader;
	}
	public void setInvoiceHeader(InvoiceHeader invoiceHeader) {
		this.invoiceHeader = invoiceHeader;
	}
	
	@Column(name="inv_date")
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	
	@Column(name="payment_due_date")
	public Date getPaymentDueDate() {
		return paymentDueDate;
	}
	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	
	@Column(name="inv_amt")
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	
	@Column(name="paid_amt")
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	
	@Column(name="due_amt")
	public Double getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}
	
	@Column(name="paying_amt")
	public Double getPayingAmount() {
		return payingAmount;
	}
	public void setPayingAmount(Double payingAmount) {
		this.payingAmount = payingAmount;
	}
	
	@Column(name="inv_status")
	public String getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	
	@Column(name="payment_item_status")
	public String getPaymentItemStatus() {
		return paymentItemStatus;
	}
	public void setPaymentItemStatus(String paymentItemStatus) {
		this.paymentItemStatus = paymentItemStatus;
	}
	
	@Column(name="tds_per")
	public Double getTdsPercentage() {
		return tdsPercentage;
	}
	public void setTdsPercentage(Double tdsPercentage) {
		this.tdsPercentage = tdsPercentage;
	}
	
	@Column(name="tds_amt")
	public Double getTdsAmount() {
		return tdsAmount;
	}
	public void setTdsAmount(Double tdsAmount) {
		this.tdsAmount = tdsAmount;
	}
	
	@Column(name="amt_after_tds")
	public Double getAmountAfterTds() {
		return amountAfterTds;
	}
	public void setAmountAfterTds(Double amountAfterTds) {
		this.amountAfterTds = amountAfterTds;
	}
	
	@Column(name="debit_amt")
	public Double getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}
	@Override
	public String toString() {
		return "PayableReceivableItem [invoiceDate=" + invoiceDate + ", paymentDueDate=" + paymentDueDate
				+ ", invoiceAmount=" + invoiceAmount + ", paidAmount=" + paidAmount + ", dueAmount=" + dueAmount
				+ ", payingAmount=" + payingAmount + ", invoiceStatus=" + invoiceStatus + ", paymentItemStatus="
				+ paymentItemStatus + ", tdsPercentage=" + tdsPercentage + ", tdsAmount=" + tdsAmount
				+ ", amountAfterTds=" + amountAfterTds + ", debitAmount=" + debitAmount + ", taxableAmount=" + taxableAmount + "]";
	}
	
	@ManyToOne
    @JoinColumn(name = "credit_debit_header_id", foreignKey=@ForeignKey(name="FK_PRI_CRDH"))
	public InvoiceHeader getCreditDebitHeader() {
		return creditDebitHeader;
	}
	public void setCreditDebitHeader(InvoiceHeader creditDebitHeader) {
		this.creditDebitHeader = creditDebitHeader;
	}
	

	@Column(name="sl_no")
	public Integer getSlNo() {
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	@Column(name="taxable_amount")
	public Double getTaxableAmount() {
		return taxableAmount;
	}

	public void setTaxableAmount(Double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}


	@Column(name="total_tax_amount")
	public Double getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public void setTotalTaxAmount(Double totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}

	@Column(name="round_off")
	public Double getRoundOff() {
		return roundOff;
	}

	public void setRoundOff(Double roundOff) {
		this.roundOff = roundOff;
	}
}
