package com.coreerp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Date;


@Entity
@Table(name="tr_pettycash_header_audit")
public class PettyCashHeaderAudit {
    private String id;

    private Date createdDateTime;

    private String createdBy;

    private Date updatedDateTime;

    private String updatedBy;
    private String pettyCashNumber;
    private Long peetyCashId;
    private Date pettyCashDate;
    private Long pettyCashTypeId;

    private Double totalIncomingAmount;
    private Double totalOutgoingAmount;
    private  Long isDayClosed;
    private String comments;
    private String approvedBy;
    private  Date approvedDate;
    private Long financialYearId;
    private Long  companyId;
    private Long statusId;
    private Double totalAmount;
    private Double bankAmount;
    private Double cashAmount;
    private Double balanceAmount;
    private Long isLatest;

    @Id
    @Column(name="id", length=255)
    @Size(min=1)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name="created_date_time",nullable = false, updatable = false)
    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Column(name="created_by",nullable = false, updatable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name="updated_date_time")
    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    @Column(name="updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }


    @Column(name="petty_cash_number")
    public String getPettyCashNumber() {
        return pettyCashNumber;
    }

    public void setPettyCashNumber(String pettyCashNumber) {
        this.pettyCashNumber = pettyCashNumber;
    }

    @Column(name="petty_cash_id")
    public Long getPeetyCashId() {
        return peetyCashId;
    }

    public void setPeetyCashId(Long peetyCashId) {
        this.peetyCashId = peetyCashId;
    }

    @Column(name="petty_cash_date")
    public Date getPettyCashDate() {
        return pettyCashDate;
    }

    public void setPettyCashDate(Date pettyCashDate) {
        this.pettyCashDate = pettyCashDate;
    }

    @Column(name="total_incoming_amount")
    public Double getTotalIncomingAmount() {
        return totalIncomingAmount;
    }

    public void setTotalIncomingAmount(Double totalIncomingAmount) {
        this.totalIncomingAmount = totalIncomingAmount;
    }

    @Column(name="total_outgoing_amount")
    public Double getTotalOutgoingAmount() {
        return totalOutgoingAmount;
    }

    public void setTotalOutgoingAmount(Double totalOutgoingAmount) {
        this.totalOutgoingAmount = totalOutgoingAmount;
    }

    @Column(name="is_day_closed")
    public Long getIsDayClosed() {
        return isDayClosed;
    }

    public void setIsDayClosed(Long isDayClosed) {
        this.isDayClosed = isDayClosed;
    }

    @Column(name="comments")
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
    @Column(name="approved_by")
    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }




    @Column(name="approved_date")
    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }


    @Column(name="status_id")
    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    @Column(name="total_amount")
    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Column(name="bank_amount")
    public Double getBankAmount() {
        return bankAmount;
    }

    public void setBankAmount(Double bankAmount) {
        this.bankAmount = bankAmount;
    }

    @Column(name="cash_amount")
    public Double getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(Double cashAmount) {
        this.cashAmount = cashAmount;
    }

    @Column(name="balance_amount")
    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    @Column(name="petty_cash_type_id")
    public Long getPettyCashTypeId() {
        return pettyCashTypeId;
    }

    public void setPettyCashTypeId(Long pettyCashTypeId) {
        this.pettyCashTypeId = pettyCashTypeId;
    }

    @Column(name="financial_year_id")
    public Long getFinancialYearId() {
        return financialYearId;
    }

    public void setFinancialYearId(Long financialYearId) {
        this.financialYearId = financialYearId;
    }

    @Column(name="company_id")
    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Column(name="is_latest")
    public Long getIsLatest() {
        return isLatest;
    }

    public void setIsLatest(Long isLatest) {
        this.isLatest = isLatest;
    }
}
