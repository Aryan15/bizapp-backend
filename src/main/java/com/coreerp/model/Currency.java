package com.coreerp.model;


import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="cd_currency")
@Where(clause = "deleted='N'")
public class Currency extends AuditModelAbstractEntity{


    private String currencyName;

    private String deleted = "N";

    private String shortName;
    private String currencySymbol;
    private String currencyFraction;
    private String currencyDecimal;




    @Column(name="currency_name")
    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }
    @Column(name="deleted")
    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }
    @Column(name="short_name")
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Column(name="currency_symbol")
    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    @Column(name="currency_fraction")
    public String getCurrencyFraction() {
        return currencyFraction;
    }

    public void setCurrencyFraction(String currencyFraction) {
        this.currencyFraction = currencyFraction;
    }
    @Column(name="currency_decimal")
    public String getCurrencyDecimal() {
        return currencyDecimal;
    }

    public void setCurrencyDecimal(String currencyDecimal) {
        this.currencyDecimal = currencyDecimal;
    }
}
