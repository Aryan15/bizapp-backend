package com.coreerp.model;


import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="ma_category", uniqueConstraints = {
        @UniqueConstraint(name="UQ_CATEGORY_NAME", columnNames={"name"})})
@Where(clause = "deleted='N'")
public class Category extends AuditModelAbstractEntity{
    private String deleted="N";
    private String name;

    @Column(name="deleted")
    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        if (deleted != null) {
            this.deleted = deleted;
        }
    }
    @Column(name="name")
    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }
}
