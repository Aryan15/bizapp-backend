package com.coreerp.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="ma_state")
@Where(clause = "deleted='N'")
public class State extends AuditModelAbstractEntity{
	
	private String name;
	
	private String stateCode;
	
	
	private Country country;
	
	private List<City> cities;

	private String deleted = "N";


	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="state_code")
	@NotNull
	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	@ManyToOne
	@JoinColumn(name="country_id", foreignKey=@ForeignKey(name="FK_STATE_COUNTRY"))
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	@OneToMany(mappedBy="state")	
	@JsonIgnore
	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}
	
	@Column(name="deleted")
	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		if(deleted != null)
			this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "State [name=" + name + ", stateCode=" + stateCode +  ", deleted=" + deleted + "]";
	}

	
	
}
