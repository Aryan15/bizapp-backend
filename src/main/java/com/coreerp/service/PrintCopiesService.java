package com.coreerp.service;
import java.util.List;

import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.PrintCopy;

public interface PrintCopiesService {

	public PrintCopy getById(Long id);
	public PrintCopy save(PrintCopy printCopies);
	public List<PrintCopy> getAll();
	public TransactionResponseDTO delete(Long id);

}
