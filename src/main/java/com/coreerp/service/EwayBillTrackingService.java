package com.coreerp.service;

import com.coreerp.dto.EwayBillInvoiceResponseDTO;
import com.coreerp.model.EwayBillTracking;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.Material;
import com.coreerp.model.SupplyType;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

public interface EwayBillTrackingService {
    public EwayBillTracking saveEwayBillTracking(EwayBillTracking ewayBillTracking, EwayBillInvoiceResponseDTO ewayBillInvoiceResponseDTO);
    public EwayBillTracking updateEwayBillTracking(EwayBillTracking ewayBillTracking, EwayBillInvoiceResponseDTO ewayBillInvoiceResponseDTO);
    public EwayBillTracking updateEwayBillTrackingAfterCancel(EwayBillTracking ewayBillTracking);
    public List<EwayBillTracking> getByInvoiceHeader(InvoiceHeader invoiceHeader);
    public List<EwayBillTracking> getEwayBillDetailsByInvoiceHeaderAndStatus(String invoiceHeaderId);
    public EwayBillTracking getByEWayBillNo(String eWayBillNum);
    public EwayBillTracking getActiveEwayBillByInvoiceHeaderAndStatus(String invoiceHeaderId, ArrayList<Integer> status);
    public EwayBillTracking getActiveEwayBillByInvoiceHeader(String invoiceHeaderId);


}
