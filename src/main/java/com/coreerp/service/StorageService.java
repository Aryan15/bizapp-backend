package com.coreerp.service;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {
    void init();

    void store(MultipartFile file, String tenantId);

    Stream<Path> loadAll();

    Path load(String filename, String tenantId);

    Resource loadAsResource(String filename, String tenantId);
    
    Boolean deleteFile(String filepath);

    void deleteAll();
    String loadAsBase64(String filename, String tenantId);

    String getPresignedUrl(String bucketName, String fileName);
}
