package com.coreerp.service;

import com.coreerp.model.AccountPostingRule;
import com.coreerp.model.TransactionType;

public interface AccountPostingRuleService {
	
	public AccountPostingRule getByTransactionType(TransactionType transactionType);

}
