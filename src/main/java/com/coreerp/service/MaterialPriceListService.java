package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.MaterialPriceListDTO;
import com.coreerp.dto.PartyMaterialPriceListDTO;
import com.coreerp.dto.PartyReportDTO;
import com.coreerp.model.MaterialPriceList;

public interface MaterialPriceListService {

	public MaterialPriceListDTO save(MaterialPriceListDTO materialPriceListDTO);
	
	
	public MaterialPriceListDTO getById(Long materialPriceListId);
	
	public MaterialPriceList getModelById(Long materialPriceListId);

	public void deleteById(Long id);

	public List<MaterialPriceListDTO> saveList(List<MaterialPriceListDTO> materialPriceListDTOList);


	public List<MaterialPriceListDTO> getForParty(Long partyId);
	
	public Boolean checkForPartyAndMaterial(Long partyId, Long materialId);

	public PartyMaterialPriceListDTO getPagePriceListReport(Long partyTypeId, Long partyId, Integer pageNumber, Integer pageSize, String sortDirection, String sortColumn,Long allowMaterialId);


}
