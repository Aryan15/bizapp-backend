package com.coreerp.service;

import com.coreerp.dto.PettyCashHeaderDTO;
import com.coreerp.dto.PettyCashWrapperDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.Category;
import com.coreerp.model.PettyCashHeader;
import com.coreerp.model.Tag;
import com.coreerp.model.TransactionType;


public interface PettyCashService {

    public Category saveCategory(Category  categoryIn);

    public Tag saveTag(Tag tagIn);

  public Long countByPettyCashAndTransactionType(String pettyCashNumber, TransactionType id);

    public PettyCashHeaderDTO savePettyCash(PettyCashWrapperDTO PeetyCashWrapperDTO);

   public TransactionResponseDTO deleteItem(String id);

    public TransactionResponseDTO delete(String id);


    public Long getMaxPettyCashNumberByTransactionType(Long transactionTypeId);

  public  PettyCashHeader  getPettyCashByHeaderId(String id);

  public  PettyCashHeaderDTO approvePettyCash(String headerId);
}
