package com.coreerp.service;

import com.coreerp.model.Status;

public interface TransactionStatusService<T> {

	public Status getStatus(T transactionComponent);
}
