package com.coreerp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.dto.VoucherHeaderDTO;
import com.coreerp.dto.VoucherWrapperDTO;
import com.coreerp.model.TransactionType;
import com.coreerp.model.VoucherHeader;
import com.coreerp.model.VoucherItem;
public interface VoucherService {
	
	public Page<VoucherHeader> getAllVoucherHeader(Pageable pageable);
	
	public void saveVoucherHeader(VoucherHeader voucherHeader);
	
	public void deleteVoucherHeader(String id);

	//public Page<InvoiceHeader> getAllInvoiceHeadersByHeaderId(TransactionPK invoiceHeaderPK, Pageable pageable);
	
	public Page<VoucherItem> getAllVoucherItems(Pageable pageable);
	
	public void saveVoucherItem(VoucherItem voucherItem);
		

	public void deleteVoucherItem(String id);
	
	public List<VoucherItem> getAllVoucherItemsByHeaderId(String voucherHeaderId, Pageable pageable);

	public VoucherHeader getVoucherHeaderByHeaderId(String voucherHeaderId);

	public List<VoucherHeaderDTO> findAllVouchers();

	public VoucherHeaderDTO getVouchersByHeaderId(String voucherHeaderId);

	public VoucherHeaderDTO saveVoucher(VoucherWrapperDTO voucherWrapperDTOIn);
	
	public VoucherHeader save(VoucherHeader voucherHeaderIn);
	
//	public ExpenseHeader saveExpense(ExpenseHeader expenseHeaderIn);
	
	public List<VoucherHeaderDTO> findVoucherByVoucherNumber(String voucherNumber);
	

	public Long getMaxVoucherNumber(String prefix);
	
	public VoucherItem getItemById(String voucherItemId);
	
	public TransactionResponseDTO delete(String id);
	
	public TransactionResponseDTO deleteItem(String id);

	public Long getMaxVoucherNumberByTransactionType(Long id);
	
	public VoucherHeader getModelById(String id);
	
	public VoucherItem getItemModelById(String id);
	
	public Long countByVoucherNumberAndTransactionType(String voucherNumber, TransactionType id);
	
//	public ExpenseHeader deleteExpenseById(Long id) throws MySQLIntegrityConstraintViolationException;

}
