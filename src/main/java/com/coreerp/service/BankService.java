package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.BankDTO;
import com.coreerp.model.Bank;

public interface BankService {

	
	public BankDTO save(BankDTO bankDTO);
	
	public BankDTO getById(Long id);
	
	public List<BankDTO> getAll();
	
	public Bank getModelById(Long id);
	
	public Bank deleteBankById (Long id);
}
