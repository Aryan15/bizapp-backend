package com.coreerp.service;

import com.coreerp.model.CardType;

public interface CardTypeService {
	
	public CardType getById(Long id);

}
