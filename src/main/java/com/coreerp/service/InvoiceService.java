package com.coreerp.service;

import java.util.Date;
import java.util.List;

import com.coreerp.dto.*;
import com.coreerp.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface InvoiceService {

	public Page<InvoiceHeader> getAllInvoiceHeader(Pageable pageable);
	
	public void saveInvoiceHeader(InvoiceHeader invoiceHeader);
	
	public Iterable<InvoiceHeader> findAllInvoiceHeaders();

	public void deleteInvoiceHeader(String id);

	//public Page<InvoiceHeader> getAllInvoiceHeadersByHeaderId(TransactionPK invoiceHeaderPK, Pageable pageable);
	
	public Page<InvoiceItem> getAllInvoiceItems(Pageable pageable);
	
	public void saveInvoiceItem(InvoiceItem invoiceItem);
		
	public Iterable<InvoiceItem> findAllInvoiceItems();

	public void deleteInvoiceItem(String id);
	
	public List<InvoiceItem> getAllInvoiceItemsByHeaderId(String invoiceHeaderId, Pageable pageable);

	public InvoiceHeader getInvoiceHeaderByHeaderId(String invoiceHeaderId);

	public List<InvoiceDTO> findAllInvoices();
	
	public List<InvoiceDTO> findAllCustomerInvoices();
	
	public List<InvoiceDTO> findAllSupplierInvoices();

	public InvoiceDTO getInvoicesByHeaderId(String invoiceHeaderId);

	public InvoiceDTO saveInvoice(InvoiceWrapperDTO invoiceWrapperIn);
	
	public InvoiceHeader save(InvoiceHeader invoiceHeaderIn);
	
	public InvoiceDTO findInvoiceByInvoiceNumber(String invoiceNumber);

	public Long getMaxInvoiceNumber(String prefix);
	
	public Long getMaxInvoiceNumberByTransactionType(Long transactionTypeId);


	public List<InvoiceDTO> getInvoicesForPartyAndTransactionType(Long partyId, Long transactionTypeId);
	
	public InvoiceItem getItemById(String itemId);

	public List<InvoiceDTO> getInvoicesForPartyAndTransactionTypeWithNonZeroDueAmount(Long partyId,
			Long transactionTypeId);

	public TransactionResponseDTO delete(String id,Integer reuseId);
	
	public TransactionResponseDTO deleteItem(String id);
	
	public InvoiceHeader getModelById(String id);
	
	public List<InvoiceDTO> getByPartyAndTransactionTypeAndStatusNotIn(Party party, String transactionType, List<Status> statuses);
	
	public List<InvoiceDTO> getByPartyAndTransactionTypeAndStatusNotIn(Party party, TransactionType transactionType, List<Status> statuses);

	public InvoiceHeader getByInvoiceNumberAndTransactionType(String invNumber, TransactionType invType);
	
	public Long getTotalCountByInvoiceNumber();
	
	public Long countByInvoiceNumberAndTransactionType(String invNumber, TransactionType invType, Long partyId);

	//related to auto transaction
	
    public void generateAutoTransaction(EmailSetting emailSetting,String email,String userName,String name,String password);

	public boolean  saveAutoTransactionLog(String invoiceHeaderId,Date currentDate,String generatedInvoiceId,Integer transactionCount);

   public AutoTransactionGenerationDTO saveAutoTransactionGeneration(AutoTransactionGenerationDTO autoTransactionGenerationDTO);


   public List<InvoiceDTO> getAllInvoiceForCustomer(Long partyId,Long transactionType);


	public TransactionResponseDTO deleteTransaction(Long id);

	public AutoTransactionGenerationDTO stopTransaction(AutoTransactionGenerationDTO autoTransactionGenerationDTO);


	public Long checkInvoiceInAutoTranaction(String id);

	public AutoTransactionGenerationDTO getAutoTransction(String id);

	public TransactionResponseDTO getAutoTransctionLog(String id);

	public InvoiceDTO reuseInvoiec(InvoiceWrapperDTO invoiceWrapperIn);


	public void weeklyTransactionReport(EmailSetting emailSetting,String email,String userName,String name,String password);

	public PartyDueAmountDTO  dueAmount(String partyName);

	public String sendDueAmountEmail(String partyName);

}
