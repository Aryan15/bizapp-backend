package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.CompanyEmailDTO;
import com.coreerp.dto.GlobalSettingDTO;
import com.coreerp.dto.SettingsWrapperDTO;
import com.coreerp.model.GlobalSetting;

public interface GlobalSettingService {

	public GlobalSettingDTO save(GlobalSettingDTO globalSettingDTO);
	
	public GlobalSetting getGlobalSettingById(Long id);
	
	public GlobalSettingDTO getGlobalSettingDTOById(Long id);
	
	public List<GlobalSettingDTO> getAllGlobalSettings();

	public SettingsWrapperDTO getAllGlobalSettingsList();

	public CompanyEmailDTO saveCompanyEmail(CompanyEmailDTO companyEmailDTO);
	
}
