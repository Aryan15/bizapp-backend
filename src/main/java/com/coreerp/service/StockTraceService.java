package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.StockTraceDTO;

import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.GRNHeader;
import com.coreerp.model.GRNItem;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.Material;
import com.coreerp.model.StockTrace;


public interface StockTraceService {

	public StockTraceDTO save(StockTraceDTO stockTraceDTO);
	
	public StockTrace getModelById(String id);
	
	public StockTraceDTO getById(String id);
	
	public List<StockTrace> getByTransactionHeaderId(String transactionHeaderId);
	
	public StockTrace getByTransactionId(String transactionId);
	
	public StockTrace saveMaterialCreationStockTrace(Material material);
	
	public StockTrace saveGrnCreationStockTrace(GRNHeader grnHeader, GRNHeader grnHeaderPre);
	
	public StockTrace saveCustomerInvoiceCreationStockTrace(InvoiceHeader invoiceHeader, InvoiceHeader invoiceHeaderPre);
	
	public StockTrace saveSupplierInvoiceCreationStockTrace(InvoiceHeader invoiceHeader, InvoiceHeader invoiceHeaderPre);
	
	public StockTrace postReversalMaterialStockTrace(Material material);
	
	public StockTrace postReversalGrnStockTrace(GRNHeader grnHeader);
	
	public StockTrace postReversalGrnItemStockTrace(GRNItem grnItem);
	
	public StockTrace postReversalCustomerInvoiceStockTrace(InvoiceHeader invoiceHeader);
	
	public StockTrace postReversalSupplierInvoiceStockTrace(InvoiceHeader invoiceHeader);
	
	public StockTrace saveJWDCCreationStockTrace(DeliveryChallanHeader dcHeader, DeliveryChallanHeader dcHeaderPre);
	
	public StockTrace postReversalJWDCStockTrace(DeliveryChallanHeader dcHeader);
	
	public StockTrace saveDcCreationStockTrace(DeliveryChallanHeader dcHeader, DeliveryChallanHeader dcHeaderPre);
	
	public StockTrace postReversalDcStockTrace(DeliveryChallanHeader dcHeader);

	public StockTrace saveCreditNoteCreationStockTrace(InvoiceHeader invoiceHeader, InvoiceHeader invoiceHeaderPre);
	
	public StockTrace postReversalCreditNoteStockTrace(InvoiceHeader invoiceHeader);
	
	public Boolean deleteFirstMaterialInStockTrace(Material material,Long id);
	
}
