package com.coreerp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.coreerp.model.QuotationHeader;


public interface QuotationHeaderService {


	
	public Page<QuotationHeader> getAllQuotationHeader(Pageable pageable);
	
	public void save(QuotationHeader quotationHeader);
	
	public QuotationHeader findByCreatedBy(String createdBy);
	
	public Iterable<QuotationHeader> findAll();

	public void delete(String id);
	
	public QuotationHeader getById(String id);
	
	
	
}
