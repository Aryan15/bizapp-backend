package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.QuotationDTO;
import com.coreerp.dto.QuotationItemDTO;
import com.coreerp.dto.QuotationWrapperDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.Party;
import com.coreerp.model.QuotationHeader;
import com.coreerp.model.QuotationItem;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;

public interface QuotationService {

	public QuotationDTO save(QuotationWrapperDTO quotationWrapperDTO);
	
	public QuotationItemDTO saveItem(QuotationItemDTO quotationItemDTO);
	
	public List<QuotationDTO> getAll();
	
	public List<QuotationDTO> getByQuotationNumber(String quotationNumber);
	
	public List<QuotationDTO> getByParty(Party party);
	
	//public QuotationHeader updateQuotation(QuotationHeader quotationHeader, String newStatus);

	public QuotationDTO getById(String id);
	
	public QuotationHeader getModelById(String id);
	
	public QuotationItem getItemModelById(String id);

	public List<QuotationDTO> getByPartyAndStatusNotIn(Party party, List<Status> statuses);

	public Long getMaxQuotationNumber(String prefix);

	public TransactionResponseDTO delete(String id);

	public List<QuotationDTO> getByQuotationNumberLike(String searchString);

	public List<QuotationDTO> getByQuotationDate(String searchString);

	public TransactionResponseDTO deleteItem(String id);

	public Long countByQuotationNumberAndTransactionType(String quotationNumber, TransactionType id);

	public Long getMaxQuotationNumberByTransactionType(Long transactionTypeId);
}
