package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.StatusDTO;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;



public interface StatusService {

public Status saveState(StatusDTO statusDTO);
	
	public List<StatusDTO> getAllStatus();
	
	public Status getStatusById(Long id);
	
	public Status getStatusByName(String name);
	
	public Status getStatusByTransactionTypeAndName(TransactionType transactionType, String name);
	
}
