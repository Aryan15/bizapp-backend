package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.CardTypeDTO;
import com.coreerp.dto.PaymentMethodDTO;
import com.coreerp.model.PaymentMethod;

public interface PaymentMethodService {

	public PaymentMethod getById(Long id);
	
	public List<PaymentMethodDTO> getAll();
	
	public List<CardTypeDTO> getAllCardType();
}
