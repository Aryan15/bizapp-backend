package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.CountryDTO;
import com.coreerp.model.Country;

public interface CountryService {
	
	public Country saveCountry(CountryDTO countryDTO);

	public List<CountryDTO> getAllCountries();
	
	public Country getCountryById(Long id);
	
	public Country deleteCountryById(Long id);

}
