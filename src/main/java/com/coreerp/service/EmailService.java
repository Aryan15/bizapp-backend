package com.coreerp.service;

import com.coreerp.dto.EmailDTO;


import java.util.List;

public interface EmailService {

    public EmailDTO getAllEmailSettings();
    public EmailDTO save(EmailDTO emailDTO);


}


