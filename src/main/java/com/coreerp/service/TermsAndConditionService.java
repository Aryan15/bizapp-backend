package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.TermsAndConditionDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.TermsAndCondition;
import com.coreerp.model.TransactionType;

public interface TermsAndConditionService {

	public TermsAndCondition getTermsAndConditionById(Long id);
	
	public List<TermsAndConditionDTO> getAll();
	
	public TermsAndConditionDTO save(TermsAndConditionDTO dto);
	
	public TransactionResponseDTO delete(Long id);
	
	public List<TermsAndConditionDTO> getByTransactionTypeAndDefault(TransactionType transactionType, Integer defaultTermsAndCondition);
}
