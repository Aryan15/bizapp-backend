package com.coreerp.service;

import com.coreerp.dto.RoleDTO;

public interface RoleService {
	
	public RoleDTO save(RoleDTO role);

}
