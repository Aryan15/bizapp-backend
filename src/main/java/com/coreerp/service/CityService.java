package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.CityDTO;
import com.coreerp.model.City;

public interface CityService {

	public CityDTO saveCity(CityDTO cityDTO);
	
	public List<CityDTO> getAllCities();
	
	public List<CityDTO> getAllCitiesForState(Long stateId);
	
	public City getCityById(Long id);
	
	public City deleteCityById(Long id);
}
