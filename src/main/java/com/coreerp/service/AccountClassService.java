package com.coreerp.service;

import com.coreerp.dto.AccountClassDTO;
import com.coreerp.model.AccountClass;

public interface AccountClassService {

	public AccountClassDTO save(AccountClassDTO accountDTO);
	
	public AccountClassDTO getById(Long id);
	
	public AccountClass getModelById(Long id);
}
