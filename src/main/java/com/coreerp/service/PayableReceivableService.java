package com.coreerp.service;

import com.coreerp.dto.PayableReceivableHeaderDTO;
import com.coreerp.dto.PayableReceivableWrapperDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.PayableReceivableHeader;

public interface PayableReceivableService {
	
	public PayableReceivableHeaderDTO save(PayableReceivableWrapperDTO payableReceivableWrapperDTO);
	
	public PayableReceivableHeaderDTO getById(String id);
	
	public PayableReceivableHeader getModelById(String id);

	public Long getMaxPRNumber(String prefix);

	public Long getMaxPRNumberByTransactionType(Long id);
	
	public TransactionResponseDTO delete(String id);
	
	public TransactionResponseDTO deleteItem(String id);

	public Long getCountByPartyAndStatusIn(Long partyId, Long transactionTypeId);
}
