package com.coreerp.service;

import java.util.List;

import com.coreerp.model.GRNHeader;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.Material;
import com.coreerp.model.StockTrace;
import com.coreerp.model.TransactionType;

public interface StockTraceBuilder {

	public StockTrace getReversalStockTrace(TransactionType transactionType, String transactionId);
	
	public StockTrace makeMaterialCreationStockTrace(Material material);
	
	public List<StockTrace> makeGrnCreationStockTrace(GRNHeader grnHeader);
	
	public List<StockTrace> makeCustomerInvoiceCreationStockTrace(InvoiceHeader invoiceHeader);
	
}
