package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.PartyDTO;
import com.coreerp.dto.RegistrationDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.Party;
import com.coreerp.model.PartyType;
import com.coreerp.model.UserTenantRelation;

public interface PartyService {

	public PartyDTO saveParty(PartyDTO partyDTO);

	public UserTenantRelation savePartyByRegistration(RegistrationDTO registrationDTO, UserTenantRelation userTenantRelation);
	
	public List<PartyDTO> getAllParties();
	
	public PartyDTO getPartyById(Long id);
	
	public Party getPartyObjectById(Long id);
	
	public List<PartyDTO> getPartiesByPartyType(PartyType partyType);
	
	public List<PartyDTO> getPartiesByPartyTypeAndNameLike(List<PartyType> partyTypes, String name);
	
	public List<Party> getPartyModelsByPartyType(PartyType partyType); 
	
	public TransactionResponseDTO deleteParty(Long id);

	public List<Party> getPartyByName(String searchString);
	
	public List<PartyDTO> getPartyDTOByName(String searchString);

	public List<Party> getPartiesByName(String searchString);
	
	public Boolean checkPartyStatus(Long id);

	public Long getMaxPartyNumberByTransactionType(String transactionTypeName);
	public List<Party> getPartyByPartyCode(String searchString);

	public TransactionResponseDTO getPartyCount();
	public Long countByPartyCodeAndPartyType(String partyCode,PartyType partyTypeId);


	
}
