package com.coreerp.service;

import com.coreerp.dto.CompanyDTO;
import com.coreerp.dto.PartyDTO;
import com.coreerp.dto.UserDTO;
import com.coreerp.dto.UserTenantRelationDTO;
import com.coreerp.model.Company;
import com.coreerp.model.Party;
import com.coreerp.model.User;
import com.coreerp.model.UserTenantRelation;

public interface UserRegistrationService {

	
	public UserTenantRelation createUserTenantRelationEntry(UserTenantRelation userTenantRelation);
	public void deleteUserTenantRelationEntry(UserTenantRelation userTenantRelation);
	
	public Boolean createNewDataBase(UserTenantRelation userTenantRelation);
	
	public Boolean createTenantObjects(UserTenantRelation userTenantRelation);
	public Boolean createJobworkEntries(UserTenantRelation userTenantRelation);
	
	public Company createCompanyAndUser(CompanyDTO companyDTO, UserDTO user, UserTenantRelationDTO userTenantRelation);
	
	
	//public Boolean createUserInTenant(Company company, UserDTO user, UserTenantRelation userTenantRelation);
	
	public Boolean sendConformationEmail(User user);
	
	public Boolean activateUser(User user);


}
