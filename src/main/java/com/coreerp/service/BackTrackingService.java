package com.coreerp.service;

public interface BackTrackingService<T> {

	public T updateTransactionStatus(T transactionComponent, T transactionComponentPre);
}
