package com.coreerp.service;

import com.coreerp.dto.EwayBillInvoiceResponseDTO;
import com.coreerp.dto.EwayBillTrackingDTO;
import com.coreerp.model.EwayBillTracking;

import java.util.List;

public interface EwayBillGenerationService {

    public List<EwayBillInvoiceResponseDTO> generateEwayBill(EwayBillTrackingDTO ewayBillTrackingDTO);
    public List<EwayBillInvoiceResponseDTO> cancelEwayBill(EwayBillTrackingDTO ewayBillTrackingDTO);
    public List<EwayBillInvoiceResponseDTO> ewayBillVehicleUpdate(EwayBillTrackingDTO ewayBillTrackingDTO);
    public List<EwayBillInvoiceResponseDTO> extendValidityOfEwayBill(EwayBillTrackingDTO ewayBillTrackingDTO);
    public List<EwayBillInvoiceResponseDTO> getEwayBillDetailsByNumber(String eWayBillNum);
    public List<EwayBillInvoiceResponseDTO> ewayBillTransporterIdUpdate(EwayBillTrackingDTO ewayBillTrackingDTO);

}
