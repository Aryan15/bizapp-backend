package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.TransactionTypeDTO;
import com.coreerp.model.TransactionType;

public interface TransactionTypeService {

	public List<TransactionTypeDTO> gtAll();
	
	public TransactionTypeDTO save(TransactionTypeDTO transactionTypeDTO);
	
	public TransactionTypeDTO getById(Long id);
	
	public TransactionType getModelById(Long id);
	
	public TransactionType getByName(String name);
	
	public Boolean allowStockIncreaseOption();
	
	public Boolean allowStockDecreaseOption();

	public  Integer getJwTransactionType();

//	public String getBuyOrSell(Long id);
}
