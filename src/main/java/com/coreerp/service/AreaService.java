package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.AreaDTO;
import com.coreerp.model.Area;

public interface AreaService {

	public AreaDTO saveArea(AreaDTO areaDTO);
	public List<AreaDTO> getAllAreas();
	public List<AreaDTO> getAreasByCity(Long cityId);
	public Area getAreaById(Long id);
	public Area deleteAreaById(Long id);
}
