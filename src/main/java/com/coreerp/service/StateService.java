package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.StateDTO;
import com.coreerp.model.Country;
import com.coreerp.model.State;

public interface StateService {
	
	public StateDTO saveState(StateDTO stateDTO);
	
	public List<StateDTO> getAllStates();
	
	public List<StateDTO> getAllStatesForCountry(Long countryId);
	
	public State getStateById(Long id);
	
	public State deleteStateById(Long id);
	

}
