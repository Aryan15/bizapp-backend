package com.coreerp.service;

import com.coreerp.model.ExpenseHeader;

public interface ExpenseService {

	public ExpenseHeader saveExpense(ExpenseHeader expenseHeaderIn);
	public ExpenseHeader deleteExpenseById(Long id);
}
