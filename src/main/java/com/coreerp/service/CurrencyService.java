package com.coreerp.service;

import com.coreerp.dto.CurrencyDTO;
import com.coreerp.model.Currency;

import java.util.List;

public interface CurrencyService

{
    public List<CurrencyDTO> getAllCurrency();

    public CurrencyDTO getCurrencyById(Long id);






}
