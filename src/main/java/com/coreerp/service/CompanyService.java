package com.coreerp.service;

import com.coreerp.dto.CompanyDTO;
import com.coreerp.model.Company;

import javax.servlet.http.HttpServletRequest;

public interface CompanyService {

	public CompanyDTO saveCompany(CompanyDTO companyDTO);
	public Company saveCompanyDTO(CompanyDTO companyDTO);
	public CompanyDTO getCompanyById(Long id);
	public Company getModelById(Long id);
	public Company saveModel(Company company);
	public void delete(Company companyPre);
	public String getCompanyLogo(Long id, HttpServletRequest request);
	public String getCompanySecondImage(Long id, HttpServletRequest request);
	public String getCompanySignatureImage(Long id, HttpServletRequest request);
	public String getTenantFromRequest(HttpServletRequest request);
}
