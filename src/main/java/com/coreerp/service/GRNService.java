package com.coreerp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.coreerp.dto.GRNHeaderDTO;
import com.coreerp.dto.GRNWrapperDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.GRNHeader;
import com.coreerp.model.GRNItem;
import com.coreerp.model.Party;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.model.Status;


public interface GRNService {

	public Page<GRNHeader> getAllGRNHeader(Pageable pageable);
	
	public void saveGRNHeader(GRNHeader grnHeader);
	
	public Iterable<GRNHeader> findAllGRNHeaders();

	public void deleteGRNHeader(String id);

	//public Page<InvoiceHeader> getAllInvoiceHeadersByHeaderId(TransactionPK invoiceHeaderPK, Pageable pageable);
	
	public Page<GRNItem> getAllGRNItems(Pageable pageable);
	
	public void saveGRNItem(GRNItem grnItem);
		
	public Iterable<GRNItem> findAllGRNItems();

	public void deleteGRNItem(String id);
	
	public List<GRNItem> getAllGRNItemsByHeaderId(String grnHeaderId, Pageable pageable);

	public GRNHeader getGRNHeaderByHeaderId(String grnHeaderId);

	public List<GRNHeaderDTO> findAllGRNs();

	public GRNHeaderDTO getGRNsByHeaderId(String grnHeaderId);

	public GRNHeaderDTO saveGRN(GRNWrapperDTO grnWrapperDTOIn);
	
	public GRNHeader save(GRNHeader grnHeaderIn);
	
	public List<GRNHeaderDTO> findGRNByGRNNumber(String grnNumber);
	
	public Long countGRNByGRNNumber(String grnNumber, Long partyId,String id);
	
	public List<GRNHeaderDTO> getByPartyAndStatusNotIn(Party party, List<Status> statuses);

	public Long getMaxGRNNumber(String prefix);
	
	public GRNItem getItemById(String grnItemId);
	
	public List<GRNHeaderDTO> findAllGrnForAParty(Long partyId);
	
	public TransactionResponseDTO delete(String id);
	
	public TransactionResponseDTO deleteItem(String id);

	public Long getMaxGRNNumberByTransactionType(Long id);
	
	public GRNHeader getModelById(String id);
	
	public GRNItem getItemModelById(String id);

}
