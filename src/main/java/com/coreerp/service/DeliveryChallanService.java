package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.DeliveryChallanDTO;
import com.coreerp.dto.DeliveryChallanHeaderWrapperDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.DeliveryChallanItem;
import com.coreerp.model.Party;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;

public interface DeliveryChallanService {
	
	public DeliveryChallanDTO save(DeliveryChallanHeaderWrapperDTO deliveryChallanHeaderWrapperDTO);
	
	public List<DeliveryChallanDTO> getByParty(Long partyId);
	
	public List<DeliveryChallanDTO> getAll();
	
	public DeliveryChallanDTO getById(String id);
	
	public DeliveryChallanHeader getModelById(String id);
	
	public DeliveryChallanItem getItemModelById(String id);

	public Long getMaxDCNumber(String prefix);
	
	public List<DeliveryChallanDTO> getByPartyAndTransactionTypeWithNonZeroBalanceQuantity(Long partyId, Long transactionTypeId);

	public List<DeliveryChallanDTO> getByDCNumber(String dcNumber);

	public List<DeliveryChallanDTO> getByPartyAndStatusNotIn(Party party, List<Status> statuses);

	public List<DeliveryChallanDTO> getByPartyAndTransactionTypeWithNonZeroBalanceQuantityAndStatusNotIn(Long partyId,
			String transactionTypeName, List<Status> statuses, TransactionType requestingTransactionType);

	public TransactionResponseDTO deleteItem(String id);
	
	public TransactionResponseDTO delete(String id);

	public Long getMaxDCNumberByTransactionType(Long transactionTypeId);

	public Long countByDeliveryChallanNumberAndTransactionType(String dcNumber, TransactionType dcType, Long partyId);
	public  DeliveryChallanHeader findByNoteId(String noteId);
}
