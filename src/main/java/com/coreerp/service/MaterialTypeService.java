package com.coreerp.service;

import java.util.List;

import com.coreerp.model.MaterialType;

public interface MaterialTypeService {

	public List<MaterialType> getAll();
	
	public MaterialType getById(Long id);
	
	public MaterialType getByName(String name);
}
