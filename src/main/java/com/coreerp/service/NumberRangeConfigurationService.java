package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.NumberRangeConfigurationDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.NumberRangeConfiguration;
import com.coreerp.model.TransactionType;

public interface NumberRangeConfigurationService {

	public List<NumberRangeConfigurationDTO> getAll();
	
	//public NumberRangeConfiguration getByName(String name);
	
	public NumberRangeConfiguration getByTransactionType(TransactionType transactionType);
	
	public NumberRangeConfigurationDTO getDTOByTransactionType(TransactionType transactionType);
	
	public List<NumberRangeConfigurationDTO> saveAll(List<NumberRangeConfigurationDTO> dtoList);
	
	public List<NumberRangeConfigurationDTO> updatePrintTemplete(List<NumberRangeConfigurationDTO> dtoList);
	public Boolean updatePrintTopMargin(NumberRangeConfigurationDTO dto);

	public TransactionResponseDTO delete(Long id);
	
	
}
