package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.TaxDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.Tax;

public interface TaxService {

	public Tax getById(Long id);
	
	public TaxDTO saveTax(TaxDTO taxDTO);

	public List<TaxDTO> getAll();

	public TransactionResponseDTO delete(Long id);
}
