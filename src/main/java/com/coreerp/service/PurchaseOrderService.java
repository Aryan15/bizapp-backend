package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.PurchaseOrderDTO;
import com.coreerp.dto.PurchaseOrderItemDTO;
import com.coreerp.dto.PurchaseOrderWrapperDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.Party;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.model.PurchaseOrderItem;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;

public interface PurchaseOrderService {
	
	public PurchaseOrderDTO save(PurchaseOrderWrapperDTO purchaseOrderWrapperDTO);
	
	public List<PurchaseOrderDTO> getByParty(Party partyIn);
	
	public List<PurchaseOrderDTO> getAll();
	
	public PurchaseOrderItemDTO saveItem(PurchaseOrderItemDTO purchaseOrderItemDTOIn);
	
	public PurchaseOrderDTO getById(String id);

	//public PurchaseOrderHeader updatePO(PurchaseOrderHeader purchaseOrderHeader, String newPoStatus);

	//public PurchaseOrderDTO cancel(String id);

	public TransactionResponseDTO delete(String id);

	public List<PurchaseOrderDTO> getByPartyAndStatusNotIn(Party party, TransactionType poType, List<Status> statuses, TransactionType requestingTransactionType);

	public List<PurchaseOrderDTO> getByPurchaseOrderNumber(String poNumber);
//	public PurchaseOrderDTO getByPoNumber(String poNumber);
	
	public PurchaseOrderHeader getModelById(String id);
	
	public PurchaseOrderItem getItemModelById(String id);

	public Long getMaxPONumber(String prefix);

	//public List<PurchaseOrderItemDTO> getItemsForInvoice(String headerId);
	
	public TransactionResponseDTO deleteItem(String id);

	public Long getMaxPONumberByTransactionType(Long id);


	PurchaseOrderHeader getpoNumberByPurchaseOrderNumberAndTransactionType(String poNumber, TransactionType poType);
	
	public Long  countByPurchaseOrderNumberAndTransactionType(String poNumber, TransactionType poType, Long partyId);

	public TransactionResponseDTO closePo(String poId,Long id);


}
