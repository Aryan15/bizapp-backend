package com.coreerp.service;

import java.util.List;

import com.coreerp.model.PartyType;

public interface PartyTypeService {
	
	public PartyType savePartyType(PartyType partyType);
	
	public List<PartyType> getAllPartyType();
	
	
	public PartyType getPartyTypeById(Long id);

	public PartyType getPartyTypeByName(String string);

}
