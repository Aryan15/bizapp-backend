package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.MaterialDTO;
import com.coreerp.dto.MaterialRecentDTO;
import com.coreerp.dto.MaterialStockCheckDTO;
import com.coreerp.dto.MaterialStockCheckDTOWrapper;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.Material;
import com.coreerp.model.Party;
import com.coreerp.model.SupplyType;

public interface MaterialService {
	
	public MaterialDTO save(MaterialDTO materialDTO);
	
	public Material getMaterialById(Long id);
	public MaterialDTO getMaterialDTOById(Long id);
	
	public List<MaterialDTO>  getMaterialDTOByMaterialTypeId(Long MaterialTypeId);

	public List<MaterialDTO>  getMaterialDTOByMaterialTypeIdJobwork(Long MaterialTypeId, String name);
	public List<MaterialDTO> getAllMaterials();
	
	public List<MaterialDTO> searchMaterialByNamePattern(String name);
	
	public List<MaterialDTO> getMaterialByParty(Party party);

	public TransactionResponseDTO deleteMaterial(Long id);

	public Material getMaterialByPartNumber(String partNumber);

	public List<MaterialDTO> getMaterialByPartyAndSupplyTypeAndNameLike(Long partyId, Long supplyTypeId, String name, String buyOrSell);

	public Boolean updateMaterialHsnAndSpec(Long materialId, String hsnCode, String spec);

	public List<MaterialDTO> getMaterialsByName(String name,Long id);
	
	public List<MaterialStockCheckDTO> validateMinimumStock(MaterialStockCheckDTOWrapper inMaterials);

	public Integer materialExistsinTransactions(Long materialId);
}
