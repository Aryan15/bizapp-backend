package com.coreerp.service;

import com.coreerp.dto.JournalEntryDTO;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.JournalEntry;
import com.coreerp.model.PayableReceivableHeader;
import com.coreerp.model.TransactionType;

public interface JournalEntryService {
	
	public JournalEntryDTO save(JournalEntryDTO journalEntryDTO);
	
	public JournalEntry saveModel(JournalEntry journalEntry);
	
	public JournalEntryDTO findById(String id);
	
	public JournalEntry findModelById(String id);
	
	public JournalEntry findByTransactionTypeAndTransactionId(TransactionType transactionType, String transactionId);
	
	public JournalEntry saveInvoiceJournalEntry(InvoiceHeader invoiceHeader);
	
	public JournalEntry savePayableReceivableJournalEntry(PayableReceivableHeader payableReceivableHeader);

	public JournalEntry postReversalInvoiceJournalEntry(InvoiceHeader invoiceHeader);
	
	public JournalEntry postReversalPayableReceivableJournalEntry(PayableReceivableHeader payableReceivableHeader);

}
