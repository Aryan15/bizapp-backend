package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.UnitOfMeasurement;

public interface UnitOfMeasurementService {
	
	public UnitOfMeasurement getById(Long id);
	
	public UnitOfMeasurement save(UnitOfMeasurement unitOfMeasurement);

	public List<UnitOfMeasurement> getAll();

	public TransactionResponseDTO delete(Long id);

}
