package com.coreerp.service;

import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.JournalEntry;
import com.coreerp.model.PayableReceivableItem;

public interface JournalEntryBuilder {

	
	public JournalEntry getReversalEntry(String transactionId);
	
	public JournalEntry makeCustomerInvoiceJournalEntry(InvoiceHeader invoiceHeader);
	
	public JournalEntry makeSupplierPayableJournalEntry(PayableReceivableItem payableReceivableItem);
	
	public JournalEntry makeCustomerReceivableJournalEntry(PayableReceivableItem payableReceivableItem);
	
	public JournalEntry makeIncomingDeliveryChallanJournalEntry(DeliveryChallanHeader deliveryChallanHeader);
	
	
	
	
}
