package com.coreerp.service;

import com.coreerp.dto.AccountDTO;
import com.coreerp.model.Account;


public interface AccountService {
	
	public AccountDTO save(AccountDTO accountDTO);
	
	public AccountDTO getById(Long id);
	
	public Account getModelById(Long id);

}
