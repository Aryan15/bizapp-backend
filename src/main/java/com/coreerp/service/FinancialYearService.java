package com.coreerp.service;

import java.util.List;

import com.coreerp.dto.FinancialYearDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.FinancialYear;

public interface FinancialYearService {

	public FinancialYear getById(Long id);
	
	public FinancialYear save(FinancialYear finanncialYear);
	
	public FinancialYear findByIsActive(Number isActive);
	
	public List<FinancialYearDTO> getAllFinancialYears();
	
	public List<FinancialYearDTO> saveAll(List<FinancialYearDTO> dtoList);
	
	public TransactionResponseDTO delete(Long id);
}
