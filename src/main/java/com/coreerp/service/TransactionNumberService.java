package com.coreerp.service;

import java.util.Map;

import com.coreerp.dto.TransactionNumber;
import com.coreerp.model.TransactionType;

public interface TransactionNumberService {

	public Map<String, Long> getNextTransactionNumber(TransactionType transactionType);
	
	public TransactionNumber  getTransactionNumber(TransactionType transactionType);


}
