package com.coreerp.service;

import java.util.Date;
import java.util.List;

import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.dto.UserDTO;
import com.coreerp.dto.UserPasswordDTO;
import com.coreerp.model.HelpVideoModel;
import com.coreerp.model.User;
import org.springframework.data.domain.Page;

public interface UserService {

	public User findByUsername(String username);
	public User findByEmail(String email);
	public UserDTO saveUser(UserDTO userDTO);
	public User getUserById(Long id);
	public List<UserDTO> getAllUsers();
	public TransactionResponseDTO deleteUser(Long id);
	public User save(User userModel);
	public Boolean resetPasswordEmployee(UserDTO userDTO) ;
	public Boolean resetPassword(UserDTO userDTO) ;
	public Boolean disableLogin(UserDTO userDTO);	
	public UserDTO saveEmployee(UserDTO user);
	public List<HelpVideoModel> getHelpVideos(Long activityId);

}
