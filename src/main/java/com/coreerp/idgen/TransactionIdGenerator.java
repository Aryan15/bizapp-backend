package com.coreerp.idgen;

import java.io.Serializable;
import java.util.UUID;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import com.coreerp.model.AbstractTransactionModel;

public class TransactionIdGenerator implements IdentifierGenerator {

	public Serializable generate(SharedSessionContractImplementor arg0, Object obj) throws HibernateException {
		
	    //if ((((QuotationHeader) obj).getId()) == null) {
		if((((AbstractTransactionModel)obj).getId()) == null) {
	        UUID uuid = java.util.UUID.randomUUID();
	        //System.out.println("TransactionIdGenerator.generate: "+uuid);
	        return uuid.toString();
	    } else {
	        //return ((QuotationHeader) obj).getId();
	    	return ((AbstractTransactionModel) obj).getId();

	    }
	    

	}

	
}
