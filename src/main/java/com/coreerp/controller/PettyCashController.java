package com.coreerp.controller;

import com.coreerp.dao.CategoryRepository;
import com.coreerp.dao.PettyCashRepository;
import com.coreerp.dao.TagRepository;
import com.coreerp.dto.*;
import com.coreerp.mapper.PettyCashHeaderMapper;
import com.coreerp.model.*;
import com.coreerp.reports.service.RecentPettyCashService;
import com.coreerp.service.PettyCashService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TransactionTypeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PettyCashController {
    final static Logger log = LogManager.getLogger(PettyCashController.class);

    @Autowired
    PettyCashService pettyCashService;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    TransactionTypeService transactionTypeService;

    @Autowired
    RecentPettyCashService recentPettyCashService;

    @Autowired
    PettyCashHeaderMapper pettyCashHeaderMapper;

    @Autowired
    StatusService statusService;

    @Autowired
    PettyCashRepository pettyCashRepository;

    @PostMapping("/category")
    public ResponseEntity<List<Category>> saveCategory(@RequestBody List<Category> categoryList){

        log.info("in expensSave: "+categoryList);

        List<Category> outCategory = new ArrayList<Category>();

        for(Category category : categoryList){
            if(!category.getName().isEmpty()){
                Category categoryOut = pettyCashService.saveCategory(category);
                outCategory.add(category);

            }

        }

        return new ResponseEntity<List<Category>>(outCategory, HttpStatus.OK);

    }



    @PostMapping("/tag")
    @Transactional
    public ResponseEntity<List<Tag>> saveTgas(@RequestBody List<Tag> tagList){



        List<Tag> outTag = new ArrayList<Tag>();

        for(Tag tag : tagList){
            if(!tag.getName().isEmpty()){
                Tag tagOut = pettyCashService.saveTag(tag);
                outTag.add(tag);

            }

        }

        return new ResponseEntity<List<Tag>>(outTag, HttpStatus.OK);

    }

    @GetMapping("/allCategory")
    public ResponseEntity<List<Category>> getAllCategory() {

        log.info("In getAllProcesses");
        List<Category> categoriesList = categoryRepository.findAll();

        log.info("categoriesList.size(): "+ categoriesList.size());

        return new ResponseEntity<List<Category>>(categoriesList, HttpStatus.OK);
    }




    @GetMapping("/allTags")
    public ResponseEntity<List<Tag>> getAllTags() {

        log.info("In getAllProcesses");
        List<Tag> tagList = tagRepository.findAll();

        log.info("categoriesList.size(): "+ tagList.size());

        return new ResponseEntity<List<Tag>>(tagList, HttpStatus.OK);
    }


    @GetMapping("/checkPetty-CashNumber/{petTypeId}")
    public ResponseEntity<TransactionResponseDTO> checkPettyCashNumber(@PathVariable("petTypeId") Long petTypeId
            , @RequestParam(value = "pettyCashNumber", required=false) String pettyCashNumber){

        TransactionType  petType = transactionTypeService.getModelById(petTypeId);

        Long count = pettyCashService.countByPettyCashAndTransactionType(pettyCashNumber, petType);

        TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();

        if (count > 0){

            transactionResponseDTO.setResponseStatus(0);
            transactionResponseDTO.setResponseString("PettyCash  "+pettyCashNumber+" already taken");
        }else{
            transactionResponseDTO.setResponseStatus(1);
            transactionResponseDTO.setResponseString("PettyCash available");
        }

        return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);

    }


    @PostMapping("/pettyCash")
    @Transactional
    public ResponseEntity<PettyCashHeaderDTO> save(@RequestBody PettyCashWrapperDTO pettyCashWrapperDTO){
        log.info("pettyCashWrapperDTO"+pettyCashWrapperDTO.getPettyCashHeader().getPettyCashItems());

        PettyCashHeaderDTO  pettyCashHeaderDTOOut = pettyCashService.savePettyCash(pettyCashWrapperDTO);
        log.info("Saved successfully");
        return new ResponseEntity<PettyCashHeaderDTO>(pettyCashHeaderDTOOut, HttpStatus.OK);
    }


    @DeleteMapping("/pettyCash/{id}")
    @Transactional
    public ResponseEntity<TransactionResponseDTO> deleteById(@PathVariable("id") String id){


        TransactionResponseDTO deleteStatus = pettyCashService.delete(id);
        log.info("returning delete message: "+deleteStatus);
        return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);


    }

    @GetMapping("/recent/pettyCash/{pettyCashTypeId}/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
    public ResponseEntity<RecentPettyCashDTO> getRecentVoucher(
            @PathVariable("pettyCashTypeId") Long PettyCashTypeId
            , @PathVariable("sortColumn") String sortColumn
            , @PathVariable("sortDirection") String sortDirection
            , @PathVariable("pageNumber") Integer pageNumber
            , @PathVariable("pageSize") Integer pageSize
            , @RequestParam(value = "searchText", required=false) String searchText	){
        log.info("in recent");
        log.info("VoucherTypeId: "+PettyCashTypeId);
        log.info("searchText: "+searchText);
        log.info("searchText.isEmpty(): "+searchText.isEmpty());
        log.info("searchText.length(): "+searchText.length());
        log.info("searchText == null : "+searchText == null ? "NULL" : "NOT NULL");
        log.info("searchText.trim(): "+searchText.trim());

        log.info("sortColumn: "+sortColumn);
        log.info("sort direction: "+sortDirection);
        log.info("pageNumber: " +pageNumber);
        log.info("pageSize: "+pageSize);

        Long totalCount = 0l;
        List<PettyCashHeader> pettyCashHeaders = new ArrayList<PettyCashHeader>();

        RecentPettyCashDTO recentPettyCashDTO = new RecentPettyCashDTO();

        TransactionType PettyCashType = transactionTypeService.getModelById(PettyCashTypeId);

        if(searchText.isEmpty() || searchText.equals("null"))
        {
            log.info("empty search text");
            Page<PettyCashHeader> recentPettyCashes = recentPettyCashService.getPage(PettyCashType, sortColumn, sortDirection, pageNumber, pageSize);
            pettyCashHeaders.addAll(recentPettyCashes.getContent());
            log.info("recentPetty sizeGGGGGGGG: "+pettyCashHeaders.size());
            totalCount = recentPettyCashes.getTotalElements();

        }else {

            Page<PettyCashHeader> recentPettyCashes = recentPettyCashService.getPageByPettyCashNumber(PettyCashType, searchText, sortColumn, sortDirection, pageNumber, pageSize);
            pettyCashHeaders.addAll(recentPettyCashes.getContent());

            totalCount= recentPettyCashes.getTotalElements();



          //  recentPettyCashes = recentPettyCashService.getPageByPaidTo(PettyCashType, searchText, sortColumn, sortDirection, pageNumber, pageSize);

            pettyCashHeaders.addAll(recentPettyCashes.getContent());
            totalCount+= recentPettyCashes.getTotalElements();


            try {
                Date searchDate = new SimpleDateFormat("dd/MM/yyyy").parse(searchText);

                log.info("parsed date: "+searchDate);

                recentPettyCashes = recentPettyCashService.getPageByPettyCashDate(PettyCashType, searchDate, sortColumn, sortDirection, pageNumber, pageSize);

                pettyCashHeaders.addAll(recentPettyCashes.getContent());
                totalCount+= recentPettyCashes.getTotalElements();
                log.info("totalCount after date: "+totalCount);

            } catch (ParseException e) {
                log.info("incorrect date format: "+searchText+" expected dd/MM/yyyy");
            }

            //searchDate.s

            log.info("non empty search text");
        }

        recentPettyCashDTO.setPettyCashHeaders(pettyCashHeaderMapper.modelToDTOList(pettyCashHeaders));
        recentPettyCashDTO.setTotalCount(totalCount);


        return new ResponseEntity<RecentPettyCashDTO>(recentPettyCashDTO, HttpStatus.OK);

    }


    @PostMapping("/approve")
    public  ResponseEntity<PettyCashHeaderDTO> getPettyCash(@RequestBody String pettyCashId){
       PettyCashHeaderDTO pettyCashHeaderDTO =pettyCashService.approvePettyCash(pettyCashId);

        return new ResponseEntity<PettyCashHeaderDTO>(pettyCashHeaderDTO, HttpStatus.OK);

    }

    @GetMapping("/latest")
    public PettyCashHeaderDTO getPettyCashLatest(){


        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        Integer month = Calendar.getInstance().get(Calendar.MONTH);
        month=month+1;
        System.out.println(year+"year");
        System.out.println(month+"month");
        String yearMonth = year.toString()+"-"+ month.toString();


        PettyCashHeader pettyCashHeader = pettyCashRepository.findByMonthAndIsLatest(yearMonth, 1l);


        if(pettyCashHeader==null){
           pettyCashHeader =new PettyCashHeader();
        }
        PettyCashHeaderDTO pettyCashHeaderDTO = new PettyCashHeaderDTO();
        if(pettyCashHeader!=null && pettyCashHeader.getId()!=null) {

            pettyCashHeaderDTO = pettyCashHeaderMapper.modelToDTOMap(pettyCashHeader);
        }


      return pettyCashHeaderDTO;
    }
}
