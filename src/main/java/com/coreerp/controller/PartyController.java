package com.coreerp.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


import com.coreerp.dto.PartyMaterialPriceListDTO;
import com.coreerp.model.StockTraceReport;
import com.coreerp.reports.model.PartyPriceListReport;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Page;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.PartyRepository;
import com.coreerp.dto.MaterialPriceListDTO;
import com.coreerp.dto.PartyBankMapDTO;
import com.coreerp.dto.PartyDTO;
import com.coreerp.dto.PartyRecentDTO;
import com.coreerp.dto.PartyReportDTO;
import com.coreerp.dto.PartyWithPriceListDTO;
import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.PartyMapper;
import com.coreerp.model.Party;
import com.coreerp.model.PartyType;
import com.coreerp.reports.service.PartyBankMapService;
import com.coreerp.reports.service.PartyReportService;
import com.coreerp.service.MaterialPriceListService;
import com.coreerp.service.PartyService;
import com.coreerp.service.PartyTypeService;

@RestController
@RequestMapping("/api")
public class PartyController {

	final static Logger log = LogManager.getLogger(PartyController.class);
	@Autowired
	PartyTypeService partyTypeService;
	@Autowired
	PartyService partyService;
	
	@Autowired
	MaterialPriceListService materialPriceListService;
	
	@Autowired
	PartyBankMapService partyBankMapService;
	
	@Autowired
	PartyMapper partyMapper;
	
	@Autowired
	PartyReportService partyReportService;
	
	@Autowired
	PartyRepository partyRepository;
	
	@GetMapping("/customer")
	public ResponseEntity<List<PartyDTO>> getCustomerList(){
		
		log.info("in getCustomerList()");
		
		PartyType partyType = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
		List<PartyDTO> partyDTOList = partyService.getPartiesByPartyType(partyType);
		
		partyType = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
		
		partyDTOList.addAll(partyService.getPartiesByPartyType(partyType));
		
		log.info("Pre sort");
//		partyDTOList.forEach(p -> {
//			log.info("name: "+ p.getName());
//		});
		
		
		Collections.sort(partyDTOList);
		
		log.info("post sort");
//		partyDTOList.forEach(p -> {
//			log.info("name: "+ p.getName());
//		});
		
		
		return new ResponseEntity<List<PartyDTO>>(partyDTOList, HttpStatus.OK);
	}


	@GetMapping("/parties-by-name/{type}")
	public ResponseEntity<List<PartyDTO>> getCustomerListByName(@PathVariable("type") String type,
																@RequestParam(value="searchText", required=false) String searchText){
		
		log.info("in getCustomerListByName(): "+type+":"+searchText);
		List<PartyType> partyTypes = new ArrayList<PartyType>();

		if(type.equals(ApplicationConstants.PARTY_TYPE_CUSTOMER)){
			partyTypes.add(partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER));
		}else if(type.equals(ApplicationConstants.PARTY_TYPE_SUPPLIER)){
			partyTypes.add(partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER));
		}else if(type.equals(ApplicationConstants.PARTY_TYPE_ALL)) {
			partyTypes.add(partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER));
			partyTypes.add(partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER));
		}
		partyTypes.add(partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH));
		List<PartyDTO> partyDTOList = partyService.getPartiesByPartyTypeAndNameLike(partyTypes, searchText);
		
		//partyType = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
		
		//partyDTOList.addAll(partyService.getPartiesByPartyType(partyType));
		
		//log.info("Pre sort");
//		partyDTOList.forEach(p -> {
//			log.info("name: "+ p.getName());
//		});
		
		
		//Collections.sort(partyDTOList);
		
		//partyDTOList.forEach(p -> {
//			log.info("name: "+ p.getName());
//		});
		
		
		return new ResponseEntity<List<PartyDTO>>(partyDTOList, HttpStatus.OK);
	}
	
	@GetMapping("/customer/{id}")
	public ResponseEntity<PartyDTO> getCustomerById(@PathVariable("id") Long id){
		
		PartyDTO partyDTO = partyService.getPartyById(id);
		
		return new ResponseEntity<PartyDTO>(partyDTO, HttpStatus.OK);
	}
	
	@PostMapping("/customer")
	@Transactional
	public ResponseEntity<PartyDTO> saveCustomer(@RequestBody PartyDTO partyDTO){
		
		PartyType partyType = partyTypeService.getPartyTypeByName("Customer");
		partyDTO.setPartyTypeId(partyType.getId());
		return new ResponseEntity<PartyDTO>(partyService.saveParty(partyDTO),HttpStatus.OK);
	}
	
	
	@GetMapping("/supplier")
	public ResponseEntity<List<PartyDTO>> getSupplierList(){
		
		PartyType partyType = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);
		List<PartyDTO> partyDTOList = partyService.getPartiesByPartyType(partyType);
		
		partyType = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
		
		partyDTOList.addAll(partyService.getPartiesByPartyType(partyType));
		Collections.sort(partyDTOList);
		return new ResponseEntity<List<PartyDTO>>(partyDTOList, HttpStatus.OK);
	}
	
	
	@GetMapping("/supplier/{id}")
	public ResponseEntity<PartyDTO> getSupplierById(@PathVariable("id") Long id){
		
		PartyDTO partyDTO = partyService.getPartyById(id);
		
		return new ResponseEntity<PartyDTO>(partyDTO, HttpStatus.OK);
	}
	
	
	@PostMapping("/supplier")
	@Transactional
	public ResponseEntity<PartyDTO> saveSupplier(@RequestBody PartyDTO partyDTO){
		
		PartyType partyType = partyTypeService.getPartyTypeByName("Supplier");
		partyDTO.setPartyTypeId(partyType.getId());
		return new ResponseEntity<PartyDTO>(partyService.saveParty(partyDTO),HttpStatus.OK);
	}
	
	@PostMapping("/party")
	@Transactional
	public ResponseEntity<PartyWithPriceListDTO> savePartyWithPriceList(@RequestBody PartyWithPriceListDTO partyWithPriceListDTO){
		
		PartyDTO partyDTO = partyWithPriceListDTO.getPartyDTO();
	
		log.info("partyDTO: "+partyDTO);
		
		PartyDTO partyDTOOut =  partyService.saveParty(partyDTO);
		
		if(partyDTOOut == null ) {
			return new ResponseEntity<PartyWithPriceListDTO>(new PartyWithPriceListDTO(), HttpStatus.OK);
		}
		
		List<MaterialPriceListDTO> materialPriceListDTOList = partyWithPriceListDTO.getMaterialPriceListDTOList();
		
		//update price list with saved party id
		
		materialPriceListDTOList.forEach(dto -> {
			dto.setPartyId(partyDTOOut.getId());
		});
		
		
		partyWithPriceListDTO.getMaterialPriceListDeletedIds().forEach(id -> {
			materialPriceListService.deleteById(id);
		});
		
		
		List<MaterialPriceListDTO> materialPriceListDTOListOut = materialPriceListService.saveList(materialPriceListDTOList);
		
		//Save bank mappings
		List<PartyBankMapDTO> partyBankMapDTOList = partyWithPriceListDTO.getPartyBankMapDTOList();
		
		partyBankMapDTOList.forEach(dto -> {
			dto.setPartyId(partyDTOOut.getId());
		});
				
		
		partyWithPriceListDTO.getPartyBankMapDeletedIds().forEach(id -> {
			partyBankMapService.deleteById(id);
		});
		
		List<PartyBankMapDTO> partyBankMapDTOListOut = partyBankMapService.saveList(partyBankMapDTOList);
		
		PartyWithPriceListDTO partyWithPriceListDTOOut = new PartyWithPriceListDTO();
		
		partyWithPriceListDTOOut.setPartyDTO(partyDTOOut);
		partyWithPriceListDTOOut.setMaterialPriceListDTOList(materialPriceListDTOListOut);
		partyWithPriceListDTOOut.setPartyBankMapDTOList(partyBankMapDTOListOut);
		
		return new ResponseEntity<PartyWithPriceListDTO>(partyWithPriceListDTOOut, HttpStatus.OK);
		
		
	}
	
	@GetMapping("/party/{id}")
	public ResponseEntity<PartyWithPriceListDTO> getPartyWithPriceListDTO(@PathVariable("id") Long id){
		
		PartyWithPriceListDTO dtoOut = new PartyWithPriceListDTO();
		dtoOut.setPartyDTO(partyService.getPartyById(id));
		dtoOut.setMaterialPriceListDTOList(materialPriceListService.getForParty(id));
		
		return new ResponseEntity<PartyWithPriceListDTO>(dtoOut, HttpStatus.OK);
	}
	
	@GetMapping("/party/price-list/{partyId}")
	public ResponseEntity<List<MaterialPriceListDTO>> getMaterialPriceListsForParty(@PathVariable("partyId") Long partyId){
		
		log.info("getMaterialPriceListsForParty, incoming party id: "+partyId);
		List<MaterialPriceListDTO> materialPriceList = materialPriceListService.getForParty(partyId);
		log.info("returning materialPriceList: "+materialPriceList.size());
		return new ResponseEntity<List<MaterialPriceListDTO>>(materialPriceList, HttpStatus.OK);
	}
	
	@DeleteMapping("/party/{id}")
	public ResponseEntity<TransactionResponseDTO> deleteParty(@PathVariable("id") Long id){
		
		log.info("in deleteParty incoming id: "+id);
		
		return new ResponseEntity<TransactionResponseDTO>(partyService.deleteParty(id), HttpStatus.OK);
	}

	@GetMapping("/parties")
	public ResponseEntity<List<PartyWithPriceListDTO>> getPartyWithPriceListDTOList(){
		
		List<PartyWithPriceListDTO> dtoOut = new ArrayList<PartyWithPriceListDTO>();
		
		partyService.getAllParties().forEach(party -> {
			
			PartyWithPriceListDTO ppDTO = new PartyWithPriceListDTO();
			
			ppDTO.setPartyDTO(party);
			ppDTO.setMaterialPriceListDTOList(materialPriceListService.getForParty(party.getId()));
			
			dtoOut.add(ppDTO);
			
		});
		
		
		
		return new ResponseEntity<List<PartyWithPriceListDTO>>(dtoOut, HttpStatus.OK);
	}
	
	@GetMapping("/party-type")
	public ResponseEntity<List<PartyType>> getAllPartyTypes(){
		
		return new ResponseEntity<List<PartyType>>(partyTypeService.getAllPartyType(), HttpStatus.OK);
	}
	
	@GetMapping("/party-name/{partyName}")
	public ResponseEntity<List<PartyDTO>> getByPartyName(@PathVariable("partyName") String partyName){
		return new ResponseEntity<List<PartyDTO>>( partyService.getPartyDTOByName(partyName), HttpStatus.OK); 
	}
	
	
	@PostMapping("/party/report/{partyTypeId}/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<PartyReportDTO> getPartyReport(
												  @PathVariable("partyTypeId") Long partyTypeId
												, @PathVariable("sortColumn") String sortColumn
												, @PathVariable("sortDirection") String sortDirection
												, @PathVariable("pageNumber") Integer pageNumber
												, @PathVariable("pageSize") Integer pageSize
												, @RequestBody TransactionReportRequestDTO partyReportRequest){
		log.info("partyTypeId: "+partyTypeId );		
		log.info("sortColumn: "+sortColumn);
		log.info("sort direction: "+sortDirection);
		log.info("pageNumber: " +pageNumber);
		log.info("pageSize: "+pageSize);
		log.info("partyReportRequest: "+ partyReportRequest);
		log.info("partyReportRequest: "+ partyReportRequest.getCreatedBy());
		
		Long partyCount = 0l;

		
		
		
		PartyType partyType = partyTypeService.getPartyTypeById(partyTypeId);
		PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
		
		List<PartyType> partyTypeList = new ArrayList<PartyType>();
		
		partyTypeList.add(partyType);
		partyTypeList.add(partyTypeBoth);
		
		List<Long> partyTypeIds = partyTypeList.stream()
				.map(pt -> pt.getId())
				.collect(Collectors.toList());
		
		//If no search text get all parties
		//If there is a search text, get parties by name  
		
		Page<Party> partyPage = partyReportService.getPageReport(partyTypeIds, partyReportRequest.getCreatedBy(), sortColumn, sortDirection, pageNumber, pageSize);
		
		List<Party> parties = partyPage.getContent();
		
		
		log.info("parties size: "+parties.size());
		partyCount = partyPage.getTotalElements();
		parties.forEach(p -> {
			if(p.getEmail().contains("notnull@notnull.com")){
				p.setEmail(" ");
			}

				});
		
		

		
		log.info("returning parties size: "+ partyCount);
		
		PartyReportDTO partyReportDTO = new PartyReportDTO();
		
		partyReportDTO.setParties(partyMapper.modelToDTOList(parties));
		partyReportDTO.setTotalCount(partyCount);
		
		return new ResponseEntity<PartyReportDTO>(partyReportDTO, HttpStatus.OK);
		
	}
	
	
	@GetMapping("/parties/recent/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<PartyRecentDTO> getRecentPartyReport(
												 @PathVariable("sortColumn") String sortColumn
												, @PathVariable("sortDirection") String sortDirection
												, @PathVariable("pageNumber") Integer pageNumber
												, @PathVariable("pageSize") Integer pageSize
												, @RequestParam(value="searchText", required=false) String searchText												){
	
		log.info("searchText: "+searchText );		
		log.info("sortColumn: "+sortColumn);
		log.info("sort direction: "+sortDirection);
		log.info("pageNumber: " +pageNumber);
		log.info("pageSize: "+pageSize);
		
		PartyRecentDTO partyRecentDTO = new PartyRecentDTO();
		
		PartyType partyTypeCustomer = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
		PartyType partyTypeSupplier = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);
		PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
		
		List<PartyType> partyTypeList = new ArrayList<PartyType>();
		
		partyTypeList.add(partyTypeCustomer);
		partyTypeList.add(partyTypeSupplier);
		partyTypeList.add(partyTypeBoth);
		
		List<PartyWithPriceListDTO> partiesWithPriceList = new ArrayList<PartyWithPriceListDTO>();
		Long partyCount = 0l;
		
		if(searchText.isEmpty() || searchText.equals("null"))	
		{
			//LisPartyWithPriceListDTO partyWithPriceListDTO = new PartyWithPriceListDTO();
			
			
//			parties.addAll(partyReportService.getPage(partyTypeList, sortColumn, sortDirection, pageNumber, pageSize));
//			log.info("materials size: "+parties.size());
			
			log.info("null search text");
			
			
			partyReportService.getPage(partyTypeList, sortColumn, sortDirection, pageNumber, pageSize).forEach(party -> {
				
				PartyWithPriceListDTO ppDTO = new PartyWithPriceListDTO();
				
				ppDTO.setPartyDTO(partyMapper.modelToDTOMap(party));
				ppDTO.setMaterialPriceListDTOList(materialPriceListService.getForParty(party.getId()));
				ppDTO.setPartyBankMapDTOList(partyBankMapService.getForParty(party.getId()));
				
				partiesWithPriceList.add(ppDTO);
				
				
				
			});
			
			partyCount = partyReportService.getTotalCountForPartyTypes(partyTypeList);
			
		}else {
			
			log.info("non empty search text");
			
			partyReportService.getPageByName(partyTypeList, searchText, sortColumn, sortDirection, pageNumber, pageSize).forEach(party -> {
				PartyWithPriceListDTO ppDTO = new PartyWithPriceListDTO();
				
				ppDTO.setPartyDTO(partyMapper.modelToDTOMap(party));
				ppDTO.setMaterialPriceListDTOList(materialPriceListService.getForParty(party.getId()));
				ppDTO.setPartyBankMapDTOList(partyBankMapService.getForParty(party.getId()));
				
				partiesWithPriceList.add(ppDTO);
			});
			
			
			partyCount = partyReportService.getTotalCountByName(partyTypeList, searchText);
			
		}
		
		partyRecentDTO.setPartyWithPriceLists(partiesWithPriceList);
		partyRecentDTO.setTotalCount(partyCount);
		
		log.info("returning parties count: "+partiesWithPriceList.size() );
		log.info("length: "+partyCount);
		
		return new ResponseEntity<PartyRecentDTO>(partyRecentDTO, HttpStatus.OK);
	
		
	
	}
	
	@GetMapping("/party/check-email/{email}")
	public ResponseEntity<TransactionResponseDTO> checkEmail(@PathVariable("email") String email){
		

		Party party = partyRepository.findByEmail(email);
		log.info("in email"+party);
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (party != null){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("email "+email+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("email available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}
	
	@GetMapping("/party/check-number/{number}")
	public ResponseEntity<TransactionResponseDTO> checkNumber(@PathVariable("number") String number){
		

		Party party = partyRepository.findByPrimaryMobile(number);
		log.info("in email"+party);
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (party != null){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString(" Phone Number "+number+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("email available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}
	
	
	@GetMapping("/party/check-gst/{gstNumber}")
	public ResponseEntity<TransactionResponseDTO> checkGSTNumber(@PathVariable("gstNumber") String gstNumber){
		

		List<Party> parties = partyRepository.findByGstNumber(gstNumber);
		log.info("in gstNumber"+parties.size());
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (parties != null && parties.size() > 0){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString(" GST Number "+ gstNumber +" should be unique");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("gstNumber available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}

    @GetMapping("/get-partyCount")
    public ResponseEntity<TransactionResponseDTO> get(){


        return new ResponseEntity<TransactionResponseDTO>(partyService.getPartyCount(), HttpStatus.OK);
    }


	@GetMapping("/get-partyNumber/{partyTypeId}")
	public ResponseEntity<TransactionResponseDTO> checkPartyNumber(
			@PathVariable("partyTypeId") Long partyTypeId
			, @RequestParam(value = "partyCode", required = false) String partyCode) {


		PartyType partyType = partyTypeService.getPartyTypeById(partyTypeId);
		Long count = partyService.countByPartyCodeAndPartyType(partyCode, partyType);

		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();

		if (count > 0) {

			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("Party Code  already taken");
		} else {
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("Party Code available");
		}

		return new ResponseEntity<TransactionResponseDTO>(transactionResponseDTO, HttpStatus.OK);

	}




	@PostMapping("/partys/material-price-report/{partyTypeId}/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<PartyMaterialPriceListDTO> getPartyReports(
            @PathVariable("partyTypeId") Long partyTypeId
            , @PathVariable("sortColumn") String sortColumn
            , @PathVariable("sortDirection") String sortDirection
            , @PathVariable("pageNumber") Integer pageNumber
            , @PathVariable("pageSize") Integer pageSize
            , @RequestBody TransactionReportRequestDTO partyReportRequest){
		log.info("partyTypeId: "+partyTypeId );
		log.info("sortColumn: "+sortColumn);
		log.info("sort direction: "+sortDirection);
		log.info("pageNumber: " +pageNumber);
		log.info("pageSize: "+pageSize);
		log.info("partyReportRequest: "+ partyReportRequest);
		log.info("partyReportRequest: "+ partyReportRequest.getPartyId());
		log.info("partyReportRequest: "+ partyReportRequest.getPartyId());


		if (sortColumn.equals("partyName")) {
			sortColumn = "party.name";
		}

		if (sortColumn.equals("materialName")) {
			sortColumn = "mat.name";
		}





		Long partyId = null;
		if(partyReportRequest.getPartyId()!=null){
			partyId=partyReportRequest.getPartyId();
		}


		PartyType partyType = partyTypeService.getPartyTypeById(partyTypeId);


		PartyMaterialPriceListDTO priceListData = materialPriceListService.getPagePriceListReport(partyType.getId(),partyId, pageNumber, pageSize, sortDirection, sortColumn,partyReportRequest.getMaterialId());













		return new ResponseEntity<PartyMaterialPriceListDTO>(priceListData, HttpStatus.OK);

	}

	@GetMapping("/partys/{partyName}")
	public ResponseEntity <List<PartyDTO>> getPartyByName(@PathVariable("partyName") String partyName){

		List<PartyDTO> partyDTOList= partyService.getPartyDTOByName(partyName);

		return new ResponseEntity<List<PartyDTO>>(partyDTOList, HttpStatus.OK);
	}


}


