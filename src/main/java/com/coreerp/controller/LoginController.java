package com.coreerp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import com.coreerp.dao.*;
import com.coreerp.dto.*;
import com.coreerp.mapper.PartyMapper;
import com.coreerp.model.*;
import com.coreerp.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.coreerp.ApplicationConstants;
import com.coreerp.jwt.JwtAuthenticationRequest;
import com.coreerp.jwt.JwtAuthenticationResponse;
import com.coreerp.jwt.JwtTokenUtil;
import com.coreerp.multitenancy.TenantContext;
import com.coreerp.multitenancy.TenantNameFetcher;
import com.coreerp.reports.dao.UserTenantRelationDTORepository;
import com.coreerp.serviceimpl.DeactivatedUserService;
import com.coreerp.utils.EmailService;
import com.coreerp.utils.OtpService;

@Controller
public class LoginController {
	
	final static Logger log = LogManager.getLogger(LoginController.class);
	
	@Autowired
	private UserService userService;

	@Autowired
	UserRegistrationService userRegistrationService;
	
	@Autowired
	UserTenantRelationRepository userTenantRelationRepository;
	
	@Autowired
	AboutCompanyRepository aboutRepository;

	@Autowired
	HelpVideoRepository helpVideoRepository;
	
    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    private TenantNameFetcher tenantResolver;
    
    @Autowired
    BCryptPasswordEncoder passwordEncoder;
    
	@Autowired
	CityService cityService;
	
	@Autowired
	CountryService countryService;
	
	@Autowired
	AreaService areaService;
	
	
	@Autowired
	StateService stateService;
	
	@Autowired
	OtpService otpService;
	
	@Autowired
	EmailSettingRepository emailSettingRepository;
	
	@Autowired
	EmailService emailService;
    
	@Autowired
	private Environment environment;
	
	@Autowired
	DeactivatedUserService deactivatedUserService;

	@Autowired
	PartyService partyService;

	@RequestMapping(value={"/","/login"}, method=RequestMethod.GET)
	public String login(){
		return "login.page";
	}
	

	
	@RequestMapping(value="/admin/home", method = RequestMethod.GET)
	public String home(Model model){
		//Model model = new Model();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findByEmail(auth.getName());
		model.addAttribute("userName", "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
		model.addAttribute("adminMessage","Content Available Only for Users with Admin Role");
		//model.setViewName("admin/home");
		return "home.page";
	}

	
    @PostMapping(value="/auth")
    public ResponseEntity<?> createAuthenticationToken(
            @RequestBody JwtAuthenticationRequest authenticationRequest,
            HttpServletResponse response) throws AuthenticationException {
        //Resolve the user's tenantId
    	log.info("in createAuthenticationToken: "+authenticationRequest);
    	UserTenantRelationDTO utr = new UserTenantRelationDTO();
        try {
        	log.info("authenticationRequest.getUsername(): "+authenticationRequest.getUsername());
            tenantResolver.setUsername(authenticationRequest.getUsername());
            ExecutorService es = Executors.newSingleThreadExecutor();
            Future<UserTenantRelationDTO> utrFuture = es.submit(tenantResolver);
            utr = utrFuture.get();
            es.shutdown();
            //handle utr == null, user is not found
            if(utr == null || ( utr != null && utr.getIsActive().equals(0))){
            	log.info("No such user "+authenticationRequest.getUsername()+" exists: ");
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            //Got the tenant, now switch to the context
            log.info("utr.getTenant(): "+utr.getTenant());
            TenantContext.setCurrentTenant(utr.getTenant());
        } catch (Exception e) {
        	log.info("exception: "+e);
            e.printStackTrace();
        }

        // Check for password reset required
		if(utr.getIsPasswordResetRequired() != null && utr.getIsPasswordResetRequired() > 0){
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
        // Perform the authentication
        try {
        	log.info("authenticationRequest.getUsername(): "+authenticationRequest.getUsername());
        	log.info("authenticationRequest.getPassword(): "+authenticationRequest.getPassword());
        	log.info("TenantContext: "+TenantContext.getCurrentTenant());
            final Authentication authentication = authenticationManager.authenticate(            		
                    new UsernamePasswordAuthenticationToken(
                            authenticationRequest.getUsername(),
                            authenticationRequest.getPassword()
                            //passwordEncoder.encode(authenticationRequest.getPassword())
                    )
            );
            log.info("authentication: "+authentication);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (Exception e) {
        	log.info("Unauthorized: "+e);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        //Generate JWT for user and send it as a Secured & HttpOnly cookie
        final UserDetails users = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final User user = userService.findByUsername(users.getUsername());
        
     // get a calendar instance, which defaults to "now"
        Calendar calendar = Calendar.getInstance();
        
        // get a date to represent "today"
        Date today = calendar.getTime();
        System.out.println("today:    " + today);
     
        // add one day to the date/calendar
        calendar.add(Calendar.YEAR, 1);
        
        //Date subscriptionEndDate = calendar.getTime(); 
        //Get plan end date of user
        Date subscriptionEndDate = utr.getToDate(); //userTenantRelationRepository.getExpiryDateOfCompany(user.getCompany().getId().intValue());
        if(subscriptionEndDate == null) {
        	subscriptionEndDate = calendar.getTime(); 
        }
        log.info("sending subscriptionEndDate: "+subscriptionEndDate);
        final String token = jwtTokenUtil.generate(user, subscriptionEndDate);
        Cookie cookie = new Cookie(tokenHeader, token);
        cookie.setHttpOnly(true);
        cookie.setSecure(true);
        cookie.setPath("/");
        response.addCookie(cookie);
        
        log.info("user: "+user);
        log.info("token: "+token);

        return new ResponseEntity<>(new JwtAuthenticationResponse(token, user), HttpStatus.OK);
    }
    
	@GetMapping("/auth/userregistration/area/{cityId}")
	public ResponseEntity<List<AreaDTO>> getAllAreas(@PathVariable Long cityId){
		
		log.info("In getAllAreas");
		List<AreaDTO> areaDTOList=areaService.getAreasByCity(cityId);
		log.info("areaDTOList.size(): "+areaDTOList.size());
		return new ResponseEntity<List<AreaDTO>>(areaDTOList,HttpStatus.OK);
	}
	
//	@GetMapping("/auth/userregistration/city")
//	public ResponseEntity<List<CityDTO>> citiesGet(){
//		
//		List<CityDTO> citiesOut = cityService.getAllCities();
//		
//		log.info("returning citiesOut: "+citiesOut);
//		
//		return new ResponseEntity<List<CityDTO>>(citiesOut, HttpStatus.OK);
//		
//	}
//	
	@GetMapping("/auth/userregistration/city/{stateId}")
	public ResponseEntity<List<CityDTO>> citiesGetForState(@PathVariable Long stateId){
		
		List<CityDTO> citiesOut = cityService.getAllCitiesForState(stateId);
		
		return new ResponseEntity<List<CityDTO>>(citiesOut, HttpStatus.OK);
		
	}
	
	@GetMapping("/auth/userregistration/country")
	public ResponseEntity<List<CountryDTO>> getAllCountries() {
		
		log.info("In getAllCountries");
		List<CountryDTO> countryDTOList = countryService.getAllCountries();
		
		log.info("countryDTOList.size(): "+ countryDTOList.size());
		
		return new ResponseEntity<List<CountryDTO>>(countryDTOList, HttpStatus.OK);
	}
    
//	@GetMapping("/auth/userregistration/state")
//	public ResponseEntity<List<StateDTO>> statesGet(){
//		
//		List<StateDTO> statesOut = stateService.getAllStates();
//		
//		return new ResponseEntity<List<StateDTO>>(statesOut, HttpStatus.OK);
//		
//	}
	
	@GetMapping("/auth/check-username/{username}")
	public ResponseEntity<TransactionResponseDTO> checkUserName(@PathVariable("username") String username){
		
		UserTenantRelation userTenantRelation = userTenantRelationRepository.findByUsername(username);
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (userTenantRelation != null){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("Username "+username+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("Username available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}
	
	@GetMapping("/auth/check-email/{email}")
	public ResponseEntity<TransactionResponseDTO> checkEmail(@PathVariable("email") String email){
		
		UserTenantRelation userTenantRelation = userTenantRelationRepository.findByEmail(email);
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (userTenantRelation != null){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("Email "+email+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("Email available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}

	@GetMapping("/auth/userregistration/state/{countryId}")
	public ResponseEntity<List<StateDTO>> statesGetForCountry(@PathVariable Long countryId){
		
		List<StateDTO> statesOut = stateService.getAllStatesForCountry(countryId);
		
		return new ResponseEntity<List<StateDTO>>(statesOut, HttpStatus.OK);
		
	}
	
	@PostMapping("/auth/userregistration")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> registerUser(@RequestBody RegistrationDTO registrationDTO){
		
		log.info("in registerUser");
		
		log.info("TenantContext: "+TenantContext.getCurrentTenant());
		
		UserTenantRelation userTenantRelation = new UserTenantRelation(); 
		

		//UserTenantRelation userTenantRelation = new UserTenantRelation(); 
		
		userTenantRelation.setTenant(registrationDTO.getCompanyDTO().getName());
		userTenantRelation.setUsername(registrationDTO.getUserDTO().getUsername());
		userTenantRelation.setName(registrationDTO.getUserDTO().getName());
		userTenantRelation.setEmail(registrationDTO.getUserDTO().getEmail());
		userTenantRelation.setCreatedBy("system");
		userTenantRelation.setCreatedDateTime(new Date());
		userTenantRelation.setRegistrationDate(new Date());
		userTenantRelation.setUpdatedBy("system");
		userTenantRelation.setUpdatedDateTime(new Date());
		userTenantRelation.setIsActive(1); // Will be set to 1 as part of activation process
		userTenantRelation.setIsPrimary(1);
		userTenantRelation.setCompanyName(registrationDTO.getCompanyDTO().getName());
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();

 		UserTenantRelation userTenantRelationCreated = userRegistrationService.createUserTenantRelationEntry(userTenantRelation);
		userTenantRelationCreated = partyService.savePartyByRegistration(registrationDTO , userTenantRelationCreated);
		log.info("in userTenantRelationCreated",userTenantRelationCreated);


		if( userTenantRelationCreated == null){
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("1. Failure");
		}else{
			//create new database
			
			if(!userRegistrationService.createNewDataBase(userTenantRelationCreated)){
				transactionResponseDTO.setResponseStatus(0);
				transactionResponseDTO.setResponseString("2. Failure");
				userRegistrationService.deleteUserTenantRelationEntry(userTenantRelationCreated);
			}else {
				//createTenantObjects
				if(!userRegistrationService.createTenantObjects(userTenantRelationCreated)){
					transactionResponseDTO.setResponseStatus(0);
					transactionResponseDTO.setResponseString("3. Failure");
					userRegistrationService.deleteUserTenantRelationEntry(userTenantRelationCreated);
				}else {

					//Enable JW if applicable
					if(registrationDTO.getEnableJobwork() > 0) {
						if (!userRegistrationService.createJobworkEntries(userTenantRelation)) {
							transactionResponseDTO.setResponseStatus(0);
							transactionResponseDTO.setResponseString("4. Failure");
							userRegistrationService.deleteUserTenantRelationEntry(userTenantRelationCreated);
						} else {
							transactionResponseDTO.setResponseStatus(1);
							transactionResponseDTO.setResponseString("6. Success");
						}
					}else {
						transactionResponseDTO.setResponseStatus(1);
						transactionResponseDTO.setResponseString("6. Success");
					}
				}
			}
		}
		
		
		
		
		
		return new ResponseEntity<TransactionResponseDTO>(transactionResponseDTO, HttpStatus.OK);
	}
	
	
	@PostMapping("/auth/userregistration/create-company-user")
	public ResponseEntity<TransactionResponseDTO> createCompanyAndUserPostRegistration(@RequestBody RegistrationDTO registrationDTO){
		
		UserTenantRelationDTO userTenantRelationCreated = null;
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		//Resolve the user's tenantId
    	log.info("in createCompanyAndUserPostRegistration: ");
        try {
        	log.info("authenticationRequest.getUsername(): "+registrationDTO.getUserDTO().getUsername());
            tenantResolver.setUsername(registrationDTO.getUserDTO().getUsername());
            ExecutorService es = Executors.newSingleThreadExecutor();
            Future<UserTenantRelationDTO> utrFuture = es.submit(tenantResolver);
            userTenantRelationCreated = utrFuture.get();
            es.shutdown();
            //handle utr == null, user is not found
            if(userTenantRelationCreated == null){
            	log.info("No such user "+registrationDTO.getUserDTO().getUsername()+" exists: ");
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            //Got the tenant, now switch to the context
            log.info("utr.getTenant(): "+userTenantRelationCreated.getTenant());
            TenantContext.setCurrentTenant(userTenantRelationCreated.getTenant());
        } catch (Exception e) {
        	log.info("exception: "+e);
            e.printStackTrace();
        }
        
		
		//
		
		//create Company
		Company company = null;
		try {
        	log.info("userTenantRelationCreated.getUsername(): "+userTenantRelationCreated.getUsername());
            tenantResolver.setUsername(userTenantRelationCreated.getUsername());
            ExecutorService es = Executors.newSingleThreadExecutor();
            Future<UserTenantRelationDTO> utrFuture = es.submit(tenantResolver);
            UserTenantRelationDTO utr = utrFuture.get();
            es.shutdown();
            
            
            //handle utr == null, user is not found
            if(utr == null){
            	log.info("No such user "+userTenantRelationCreated.getUsername()+" exists: ");

            }else{
            	//Got the tenant, now switch to the context
	            log.info("utr.getTenant(): "+utr.getTenant());
	            TenantContext.setCurrentTenant(utr.getTenant());
            	company = userRegistrationService.createCompanyAndUser(registrationDTO.getCompanyDTO(), registrationDTO.getUserDTO(), userTenantRelationCreated);
            }
            
        } catch (Exception e) {
        	log.info("exception: "+e);
            e.printStackTrace();
        }									
		if(company == null){
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("7. Failure");
		}else {
			
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("8. Success");
			
			}
		
		return new ResponseEntity<TransactionResponseDTO>(transactionResponseDTO, HttpStatus.OK);
		
	}

	@GetMapping("/auth/generate-otp/{username}")
	public ResponseEntity<TransactionResponseDTO> generateOtp(@PathVariable("username") String username){
		
		TransactionResponseDTO response = new TransactionResponseDTO();
		response.setResponseStatus(1);
		response.setResponseString("OTP Sent");
		
		UserTenantRelation userTenantRelation = userTenantRelationRepository.findByUsernameAndIsActive(username, 1);
		
//		EmailSetting emailSetting = emailSettingRepository.getOne(1);
		
		log.info("got tenant: "+userTenantRelation);
		
		if(userTenantRelation != null ) {
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			Date today = null;
			try {
				today = sdf.parse(sdf.format(new Date()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
//&& (userTenantRelation.getOtpDate() == null || ( userTenantRelation.getOtpDate() != null && userTenantRelation.getOtpDate().equals(today)))
			if((userTenantRelation.getNumberOfOTP() != null && userTenantRelation.getNumberOfOTP() >= ApplicationConstants.OTP_PER_DAY) &&  (userTenantRelation.getOtpDate() != null && userTenantRelation.getOtpDate().equals(today) )) {
				response.setResponseStatus(0);
				response.setResponseString("You have exceeded reset attempts for today!");
			}else {
				if(userTenantRelation.getNumberOfOTP() == 3) {
					userTenantRelation.setNumberOfOTP((long)1);
				}else {
					userTenantRelation.setNumberOfOTP(userTenantRelation.getNumberOfOTP() + 1);
				}
				
				//Generate OTP
				
				int otp = otpService.generateOTP(username);
				log.info("OTP : "+otp+" AND username = "+username);
				
				//Send OTP in the email
				EmailSetting emailSetting = emailSettingRepository.getOne(1);
				
				EmailObject emailObject = new EmailObject();
				
				emailObject.setOtp(otp+"");
				emailObject.setName(userTenantRelation.getName() != null ? userTenantRelation.getName() : userTenantRelation.getUsername());
				String templateName = environment.getProperty("email.forgotpassword.template.name");
				String subjecta = environment.getProperty("email.forgotpassword.subject");
						
				if(emailSetting != null && emailSetting.getIsEmailEnabled() != 0 && userTenantRelation.getEmail() != null ) {
					
					emailService.sendEmail(emailSetting, emailObject, emailSetting.getEmailFrom(), templateName, userTenantRelation.getEmail(), subjecta, null, null, null);
				}else {
					response.setResponseStatus(0);
					response.setResponseString("Could not send email");
				}
				//Update user tenant with OTP sent info
				userTenantRelation.setOtpDate(new Date());
				userTenantRelationRepository.save(userTenantRelation);
			}
		}else {
			response.setResponseStatus(0);
			response.setResponseString("Invalid Username");
			return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
		}
		
		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
		
	}
	
	@GetMapping("/auth/validate-otp/{otpnum}/{username}")
	public ResponseEntity<TransactionResponseDTO>  validateOtp(@PathVariable("otpnum") int otpnum, @PathVariable("username") String username){
		log.info("in validateOTP: "+otpnum);
		final String SUCCESS = "Entered Otp is valid";
		final String FAIL = "Entered Otp is NOT valid. Please Retry!";
		TransactionResponseDTO response = new TransactionResponseDTO();
		
		log.info(" Otp Number : "+otpnum);
		//Validate the Otp 
		if(otpnum >= 0){
			log.info("1");
			int serverOtp = otpService.getOtp(username);
			log.info("server otp----"+serverOtp);
			if(serverOtp > 0){
				log.info("2");
				if(otpnum == serverOtp){
					log.info("3");
					otpService.clearOTP(username);
		
					//return SUCCESS;
					response.setResponseStatus(1);
					response.setResponseString(SUCCESS);
					log.info("4");
				}
				else {
					//return FAIL;
					log.info("5");
					response.setResponseStatus(0);
					response.setResponseString(FAIL);
				}
		}else {
		//return FAIL;
			log.info("6");
			response.setResponseStatus(0);
			response.setResponseString(FAIL);
		}}else {
		//return FAIL;
			log.info("10");
			response.setResponseStatus(0);
			response.setResponseString(FAIL);
		}
	
		log.info("7");
		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	}
		
	@PostMapping("/auth/reset-password")
	public ResponseEntity<TransactionResponseDTO> resetPassword(@RequestBody UserDTO userDTO){
		TransactionResponseDTO response = new TransactionResponseDTO();
		
		try {
        	log.info("userDTO.getUsername(): "+userDTO.getUsername());
            tenantResolver.setUsername(userDTO.getUsername());
            ExecutorService es = Executors.newSingleThreadExecutor();
            Future<UserTenantRelationDTO> utrFuture = es.submit(tenantResolver);
            UserTenantRelationDTO utr = utrFuture.get();
            es.shutdown();
            //handle utr == null, user is not found
            if(utr == null){
            	log.info("No such user "+userDTO.getUsername()+" exists: ");
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            //Got the tenant, now switch to the context
            log.info("utr.getTenant(): "+utr.getTenant());
            TenantContext.setCurrentTenant(utr.getTenant());
        } catch (Exception e) {
        	log.info("exception: "+e);
            e.printStackTrace();
        }
		
		if(userService.resetPassword(userDTO)) {
			response.setResponseStatus(1);
			response.setResponseString("Password updated!");
		}else {
			response.setResponseStatus(0);
			response.setResponseString("Couldn't update password!");
		}
		
		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	}

	@GetMapping("/auth/about-company")
	public ResponseEntity<AboutCompany> getAboutCompany(){
		
		log.info("In about Company");
		AboutCompany aboutCompany=aboutRepository.getOne((long)1);
		log.info("Got company: " +aboutCompany);
		return new ResponseEntity<AboutCompany>(aboutCompany,HttpStatus.OK);
	}

	
	@PostMapping("/auth/deactivated-user")
	public ResponseEntity<String> addDeactivatedUser(@RequestBody DeactivatedUser deactivatedUser){
		log.info("In addDeactivatedUser");
		List<DeactivatedUser> userList = new ArrayList<>();
		
		userList.add(deactivatedUser);
		
		deactivatedUserService.addToList(userList);
		log.info("out addDeactivatedUser"+deactivatedUserService.getAllDeactivatedUsers());
		return new ResponseEntity<String>("success", HttpStatus.OK);
	}
	
	@GetMapping("/auth/deactivated-user")
	public ResponseEntity<List<DeactivatedUser>> getDeactivatedUserList(){
		
		List<DeactivatedUser> userList = deactivatedUserService.getAllDeactivatedUsers();
		
		return new ResponseEntity<List<DeactivatedUser>>(userList, HttpStatus.OK);
	}
	
	@DeleteMapping("/auth/deactivated-user/{id}")
	public ResponseEntity<String> deleteDeactivatedUser(@PathVariable("id") Integer id){
		log.info("in deleteDeactivatedUser"+deactivatedUserService.getAllDeactivatedUsers());
		DeactivatedUser deactivatedUser = deactivatedUserService.getAllDeactivatedUsers()
				.stream()
				.filter(user -> user.getId() == id)
				.collect(Collectors.toList())
				.get(0);
		log.info("out deleteDeactivatedUser"+deactivatedUserService.getAllDeactivatedUsers());
		deactivatedUserService.deleteFromList(deactivatedUser);
		
		return new ResponseEntity<String>("success", HttpStatus.OK);
	}


	@GetMapping("/auth/password-reset-complete/{username}/{resetValue}")
	public ResponseEntity<TransactionResponseDTO> passwordResetComplete(
			@PathVariable("username") String username,
			@PathVariable("resetValue") Integer resetValue
	){

		TransactionResponseDTO response = new TransactionResponseDTO();
		response.setResponseStatus(1);
		response.setResponseString("Complete");

		UserTenantRelation userTenantRelation = userTenantRelationRepository.findByUsernameAndIsActive(username, 1);


		log.info("got tenant: "+userTenantRelation);

		if(userTenantRelation != null ) {
			userTenantRelation.setIsPasswordResetRequired(resetValue);
			userTenantRelationRepository.save(userTenantRelation);

		}else {
			response.setResponseStatus(0);
			response.setResponseString("INCOMPLETE");
		}

		return new ResponseEntity<>(response, HttpStatus.OK);

	}


	@GetMapping("/auth/get-videos/{activityId}")
	public ResponseEntity<List<HelpVideoModel>> getAllHelpVideos(@PathVariable("activityId") String activityId){
		Long activity=0l;

       if(activityId.equalsIgnoreCase("null")) {
          activity=null;

       }
    else{
          activity = Long.parseLong(activityId);
       }
		List<HelpVideoModel> helpVideoModelList = userService.getHelpVideos(activity);

		return new ResponseEntity<List<HelpVideoModel>>(helpVideoModelList, HttpStatus.OK);
	}





}


