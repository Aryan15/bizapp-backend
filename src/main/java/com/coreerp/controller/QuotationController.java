package com.coreerp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.dto.*;
import com.coreerp.model.*;

import com.coreerp.reports.service.QuotationReportPageableService;
import com.coreerp.service.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.ApplicationConstants;
import com.coreerp.mapper.PartyMapper;
import com.coreerp.mapper.QuotationHeaderMapper;
import com.coreerp.reports.service.RecentQuotationService;
import com.coreerp.utils.DateUtils;
import com.coreerp.dto.QuotationReportDTO;

@RestController
@RequestMapping("/api")
public class QuotationController {

	final static Logger log = LogManager.getLogger(QuotationController.class);
	
	@Autowired
	QuotationService quotationService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	PartyMapper partyMapper;
	
	@Autowired
	StatusService statusService;


	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	QuotationHeaderMapper quotationHeaderMapper;
	
	@Autowired
	RecentQuotationService recentQuotationService;
	
	@Autowired
	FinancialYearService financialYearService;

	@Autowired
	PartyTypeService partyTypeService;

	@Autowired
	QuotationReportPageableService quotationReportPageableService;

//	@Autowired
//	TransactionNumberService transactionNumberService;
	
	@GetMapping("/quotation")
	public ResponseEntity<List<QuotationDTO>> getAllQuotations(){
		
		List<QuotationDTO> quotationDTOList = quotationService.getAll();
		
		return new ResponseEntity<List<QuotationDTO>>(quotationDTOList, HttpStatus.OK);
	}

//	@GetMapping("/quotation/quotation-number")
//	public ResponseEntity<TransactionResponseDTO> getQuotationNumber(){
//		
//		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
//		
//		TransactionType transactionType = transactionTypeService.getByName(ApplicationConstants.QUOTATION_TYPE_CUSTOMER);
//		
//		transactionResponseDTO.setResponseString(transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(transactionType.getId())).getTransactionNumber());
//		
//		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
//	}
	
	@GetMapping("/quotation/{id}")
	public ResponseEntity<QuotationDTO> getById(@PathVariable("id") String id){
		
		return new ResponseEntity<QuotationDTO>(quotationService.getById(id), HttpStatus.OK);
	}
	
	@GetMapping("/quotation/quotation-no/{quotationNumber}")
	public ResponseEntity<List<QuotationDTO>> getByNumber(@PathVariable("quotationNumber") String quotationNumber){
		List<QuotationDTO> quotationDTOList = quotationService.getByQuotationNumber(quotationNumber); //getAll();
		
		return new ResponseEntity<List<QuotationDTO>>(quotationDTOList, HttpStatus.OK);
	}
	
	
	@PostMapping("/quotation")
	@Transactional
	public ResponseEntity<QuotationDTO> save(@RequestBody QuotationWrapperDTO quotationWrapperDTO){
		

		return new ResponseEntity<QuotationDTO> (quotationService.save(quotationWrapperDTO), HttpStatus.OK);
		
	}
	
	@DeleteMapping("/quotation/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteById(@PathVariable("id") String id){
		TransactionResponseDTO deleteStatus = quotationService.delete(id);
		log.info("returning delete message: "+deleteStatus);
		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
	}
	
	
	@GetMapping("/quotation/party/{id}")
	public ResponseEntity<List<QuotationDTO>> getByParty(@PathVariable("id") Long id){
		Party party = partyMapper.dtoToModelMap(partyService.getPartyById(id));
		
		List<Status> statuses = new ArrayList<Status>(); //Arrays.asList(ApplicationConstants.PURCHASE_ORDER_STATUS_CANCELLED,ApplicationConstants.PURCHASE_ORDER_STATUS_COMPLETED,ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED);
		
		TransactionType transactionType = transactionTypeService.getByName(ApplicationConstants.QUOTATION_TYPE_CUSTOMER);
		
		//statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType,ApplicationConstants.PURCHASE_ORDER_STATUS_CANCELLED));
		statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType,ApplicationConstants.QUOTATION_STATUS_COMPLETED));
		//statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType,ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED));
		
		log.info("statuses: "+statuses.size());
		
		List<QuotationDTO> quotationListOut = quotationService.getByPartyAndStatusNotIn(party,statuses);//purchaseOrderService.getByParty(party);
		log.info("Returning quotation list");
		log.info(quotationListOut);
		return new ResponseEntity<List<QuotationDTO>>(quotationListOut, HttpStatus.OK);
	}
	
	@GetMapping("/quotation/search-qno-date-party/{searchString}")
	public ResponseEntity<List<QuotationDTO>> getByQuotationNoDateParty(@PathVariable("searchString") String searchString){

		// Get quotations by party
		List<Party> partyList = partyService.getPartyByName(searchString);
		
		List<Status> statuses = new ArrayList<Status>(); //Arrays.asList(ApplicationConstants.PURCHASE_ORDER_STATUS_CANCELLED,ApplicationConstants.PURCHASE_ORDER_STATUS_COMPLETED,ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED);
		
		TransactionType transactionType = transactionTypeService.getByName(ApplicationConstants.QUOTATION_TYPE_CUSTOMER);
		
		//statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType,ApplicationConstants.PURCHASE_ORDER_STATUS_CANCELLED));
		statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType,ApplicationConstants.PURCHASE_ORDER_STATUS_DC_GENERATED));
		statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType,ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED));

		List<QuotationDTO> quotationListOut = new ArrayList<QuotationDTO>();
		
		partyList.forEach(party -> {
		
			List<QuotationDTO> quotationList = quotationService.getByPartyAndStatusNotIn(party,statuses);
			quotationListOut.addAll(quotationList);
		});

		
		// Get quotations by quotation number
		
		List<QuotationDTO> quotationListQnumber = quotationService.getByQuotationNumberLike(searchString);
		
		quotationListOut.addAll(quotationListQnumber);
		
		
		// Get quotations by date
		
		List<QuotationDTO> quotationListDate = quotationService.getByQuotationDate(searchString);
		
		quotationListOut.addAll(quotationListDate);
		
		log.info("Returning quotation list");
		log.info(quotationListOut);
		return new ResponseEntity<List<QuotationDTO>>(quotationListOut, HttpStatus.OK);
	}

	/*
	 * @DeleteMapping("/quotation/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteById(@PathVariable("id") String id){
		TransactionResponseDTO deleteStatus = quotationService.delete(id);
		log.info("returning delete message: "+deleteStatus);
		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
	}
	 
	 */
	
//	@DeleteMapping("/quotation/delete-item/{id}")
//	@Transactional
//	public ResponseEntity<TransactionResponseDTO> deleteItemById(@PathVariable("id") String id){
//		
//		TransactionResponseDTO deleteStatus = quotationService.deleteItem(id);
//		log.info("returning delete message: "+deleteStatus);
//		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
//		
//	}
	
	@GetMapping("/quotation/recent/{quotationTypeId}/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<RecentQuotationDTO> getRecentQuotation(
												  @PathVariable("quotationTypeId") Long quotationTypeId
												, @PathVariable("sortColumn") String sortColumn
												, @PathVariable("sortDirection") String sortDirection
												, @PathVariable("pageNumber") Integer pageNumber
												, @PathVariable("pageSize") Integer pageSize
												, @RequestParam(value = "searchText", required=false) String searchText												){
		log.info("searchText: "+searchText);
		log.info("searchText.isEmpty(): "+searchText.isEmpty());
		log.info("searchText.length(): "+searchText.length());
		log.info("searchText == null : "+searchText == null ? "NULL" : "NOT NULL");
		log.info("searchText.trim(): "+searchText.trim());
		
		log.info("sortColumn: "+sortColumn);
		log.info("sort direction: "+sortDirection);
		log.info("pageNumber: " +pageNumber);
		log.info("pageSize: "+pageSize);
		
		Long totalCount = 0l;
		List<QuotationHeader> quotationHeaders = new ArrayList<QuotationHeader>();
		
		RecentQuotationDTO recentQuotationDTO = new RecentQuotationDTO();
		
		TransactionType quotationType = transactionTypeService.getModelById(quotationTypeId);
		
		FinancialYear financialYear = financialYearService.findByIsActive(1);
		

			if (searchText.isEmpty() || searchText.equals("null")) {
				searchText=null;
			}

		Page<QuotationHeader> recentQo = recentQuotationService.getQuotationPageList(quotationType, financialYear, searchText, sortColumn, sortDirection, pageNumber, pageSize);
		quotationHeaders.addAll(recentQo.getContent());
		totalCount = recentQo.getTotalElements();
		recentQuotationDTO.setQuotationHeaders(quotationHeaderMapper.modelToDTOList(quotationHeaders));
	recentQuotationDTO.getQuotationHeaders().forEach(partycode -> {
		log.info(partycode.getPartyName()+"--------------------------"+partycode.getPartyCode());
				});
		recentQuotationDTO.setTotalCount(totalCount);
		
		
		return new ResponseEntity<RecentQuotationDTO>(recentQuotationDTO, HttpStatus.OK);
		
	}
	
	@GetMapping("/check-quotationNumber/{qtnTypeId}")
	public ResponseEntity<TransactionResponseDTO> checkQuotationNumber(@PathVariable("qtnTypeId") Long qtnTypeId
			, @RequestParam(value = "quotationNumber", required=false) String quotationNumber){
		
		TransactionType qtnType = transactionTypeService.getModelById(qtnTypeId);
		
		Long count = quotationService.countByQuotationNumberAndTransactionType(quotationNumber, qtnType);
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (count > 0){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("quotationNumber  "+quotationNumber+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("quotationNumber available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}

	@PostMapping("/quotation/report/{quotationTypeId}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
	public ResponseEntity<QuotationReportDTO> getQuotationReport(
			@PathVariable("quotationTypeId") Long quotationTypeId,
			@PathVariable("pageNumber") Integer pageNumber,
			@PathVariable("pageSize") Integer pageSize,
			@PathVariable("sortDir") String sortDir,
			@PathVariable("sortColumn") String sortColumn,
			@RequestBody TransactionReportRequestDTO quotationReportRequestDto){
		log.info("incoming request 1: "+quotationReportRequestDto);
		log.info("incoming request from date: "+quotationReportRequestDto.getTransactionFromDate());
		log.info("incoming request to date: "+quotationReportRequestDto.getTransactionToDate());
		log.info("incoming request mat id: "+quotationReportRequestDto.getMaterialId());
		log.info("incoming request party id: "+quotationReportRequestDto.getPartyId());
		log.info("incoming request fin id: "+quotationReportRequestDto.getFinancialYearId());
		log.info("incoming request party type: "+quotationReportRequestDto.getPartyTypeIds());

		List<Long> partyTypeIds = null;

		if(quotationReportRequestDto.getPartyTypeIds().isEmpty()){
			PartyType partyTypeCustomer = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);

			PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);

			List<PartyType> partyTypeList = new ArrayList<PartyType>();

			partyTypeList.add(partyTypeCustomer);
			//partyTypeList.add(partyTypeSupplier);
			partyTypeList.add(partyTypeBoth);

			//List<Long> partyTypeIds = new ArrayList<Long>();

			partyTypeIds = partyTypeList.stream()
					.map(partyType -> partyType.getId())
					.collect(Collectors.toList());

		}
		else{

			partyTypeIds = quotationReportRequestDto.getPartyTypeIds();

		}
		log.info("party type ids: "+partyTypeIds);

		log.info("sortColumn: "+sortColumn);
		if(sortColumn.equals("partyName"))
			sortColumn = "party.name";

		TransactionType quotationType = transactionTypeService.getModelById(quotationTypeId);

		QuotationReportDTO quotationReportDto =  quotationReportPageableService.getPagedQuotationReport(quotationType.getId(), quotationReportRequestDto.getFinancialYearId(), quotationReportRequestDto.getTransactionFromDate(), quotationReportRequestDto.getTransactionToDate(), quotationReportRequestDto.getPartyId(), partyTypeIds, quotationReportRequestDto.getCreatedBy(), pageNumber, pageSize, sortDir, sortColumn);

        log.info(quotationReportDto+"quotationReportDto");

		return new ResponseEntity<QuotationReportDTO>(quotationReportDto, HttpStatus.OK);
	}

	
}
	