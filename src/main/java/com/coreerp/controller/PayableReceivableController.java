package com.coreerp.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.dao.*;
import com.coreerp.dto.PurchaseOrderDTO;
import com.coreerp.model.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.ApplicationConstants;
import com.coreerp.dto.CardTypeDTO;
import com.coreerp.dto.PayableReceivableHeaderDTO;
import com.coreerp.dto.PayableReceivableReportDTO;
import com.coreerp.dto.PayableReceivableWrapperDTO;
import com.coreerp.dto.PaymentMethodDTO;
import com.coreerp.dto.RecentPayableReceivableDTO;
import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.PayableReceivableHeaderMapper;
import com.coreerp.reports.model.BalanceReport;
import com.coreerp.reports.model.PayableReceivableReport;
import com.coreerp.reports.service.InvoiceReportPageableService;
import com.coreerp.reports.service.PayableReceivablePageableService;
import com.coreerp.reports.service.RecentPayableReceivableService;
import com.coreerp.service.PartyService;
import com.coreerp.service.PartyTypeService;
import com.coreerp.service.PayableReceivableService;
import com.coreerp.service.PaymentMethodService;
import com.coreerp.service.TransactionTypeService;
import com.coreerp.serviceimpl.PayableReceivableBackTrackingService;
import com.coreerp.service.FinancialYearService;

@RestController
@RequestMapping("/api")
public class PayableReceivableController {

	final static Logger log = LogManager.getLogger(PayableReceivableController.class);
	
	@Autowired
	PayableReceivableService payableReceivableService;
	
	@Autowired
	PartyTypeService partyTypeService;
	
	@Autowired
	InvoiceReportPageableService invoiceReportPageableService;
	
	@Autowired
	PayableReceivablePageableService payableReceivablePageableService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	RecentPayableReceivableService recentPayableReceivableService;
	
	@Autowired
	PayableReceivableHeaderMapper payableReceivableHeaderMapper;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	PaymentMethodService paymentMethodService;

	@Autowired
	FinancialYearService financialYearService;

	@Autowired
	StatusRepository statusRepository;

	@Autowired
	PayableReceivableItemRepository payableReceivableItemRepository;
	
	@GetMapping("/pay-receive/{id}")
	public ResponseEntity<PayableReceivableHeaderDTO> get(@PathVariable("id") String id){
		
		return new ResponseEntity<PayableReceivableHeaderDTO>(payableReceivableService.getById(id), HttpStatus.OK);
		
	}
	
	@PostMapping("pay-receive")
	@Transactional
	public ResponseEntity<PayableReceivableHeaderDTO> save(@RequestBody PayableReceivableWrapperDTO payableReceivableWrapperDTO){
	
		
		return new ResponseEntity<PayableReceivableHeaderDTO>(payableReceivableService.save(payableReceivableWrapperDTO), HttpStatus.OK);
	}
	
	

	@GetMapping("/pay-receive/recent/{invoiceTypeId}/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<RecentPayableReceivableDTO> getRecentPR(
												  @PathVariable("invoiceTypeId") Long invoiceTypeId
												, @PathVariable("sortColumn") String sortColumn
												, @PathVariable("sortDirection") String sortDirection
												, @PathVariable("pageNumber") Integer pageNumber
												, @PathVariable("pageSize") Integer pageSize
												, @RequestParam(value = "searchText", required=false) String searchText												){
	
		Long totalCount = 0l;
		List<PayableReceivableHeader> payableReceivableHeaders = new ArrayList<PayableReceivableHeader>();
		
		RecentPayableReceivableDTO recentPayableReceivableDTO = new RecentPayableReceivableDTO();
		
		TransactionType transactionType = transactionTypeService.getModelById(invoiceTypeId);

		FinancialYear financialYear = financialYearService.findByIsActive(1);

		if (searchText.isEmpty() || searchText.equals("null")) {
			searchText=null;
		}
		Page<PayableReceivableHeader> recentPay = recentPayableReceivableService.getPayRecPageList(transactionType, financialYear, searchText, sortColumn, sortDirection, pageNumber, pageSize);
		payableReceivableHeaders.addAll(recentPay.getContent());
		totalCount = recentPay.getTotalElements();


		recentPayableReceivableDTO.setPayableReceivableHeaders(payableReceivableHeaderMapper.modelToDTOList(payableReceivableHeaders));
		recentPayableReceivableDTO.setTotalCount(totalCount);
        recentPayableReceivableDTO.getPayableReceivableHeaders().forEach(pay ->{
            if(pay.getStatusName()==ApplicationConstants.PR_STATUS_DRAFT){
                pay.getPayableReceivableItems().forEach(payItem ->{
               List<PayableReceivableItem>  payableReceivableItems=payableReceivableItemRepository.getItem(payItem.getInvoiceHeaderId());
                    payableReceivableItems.forEach(payableItme ->{
                        Double tdsAmount=0.0;
                        tdsAmount= tdsAmount+payableItme.getTdsAmount();
                        payItem.setPreTdsAmount(tdsAmount);

                    });
                });


            }


        });
		
		return new ResponseEntity<RecentPayableReceivableDTO>(recentPayableReceivableDTO, HttpStatus.OK);
	}
	
	
	
	@PostMapping("/pay-receive/balance-report/{invoiceTypeId}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
	public ResponseEntity<PayableReceivableReportDTO> getBalanceReport(
			@PathVariable("invoiceTypeId") Long invoiceTypeId,
			@PathVariable("pageNumber") Integer pageNumber,
			@PathVariable("pageSize") Integer pageSize,
			@PathVariable("sortDir") String sortDir,
			@PathVariable("sortColumn") String sortColumn,
			@RequestBody TransactionReportRequestDTO transactionReportRequestDTO){
		log.info("incoming request: "+transactionReportRequestDTO);
		log.info("incoming request: "+transactionReportRequestDTO.getTransactionFromDate());
		log.info("incoming request: "+transactionReportRequestDTO.getTransactionToDate());
		log.info("incoming request: "+transactionReportRequestDTO.getMaterialId());
		log.info("incoming request: "+transactionReportRequestDTO.getPartyId());
		log.info("incoming request: "+transactionReportRequestDTO.getFinancialYearId());
		log.info("incoming request: "+transactionReportRequestDTO.getPartyTypeIds());
		log.info("incoming request: "+transactionReportRequestDTO.getStatusName());
		List<Long> partyTypeIds = null;
		
		if(transactionReportRequestDTO.getPartyTypeIds().isEmpty()){
			PartyType partyTypeCustomer = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
			PartyType partyTypeSupplier = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);
			PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
			
			List<PartyType> partyTypeList = new ArrayList<PartyType>();
			
			partyTypeList.add(partyTypeCustomer);
			partyTypeList.add(partyTypeSupplier);
			partyTypeList.add(partyTypeBoth);
			
			//List<Long> partyTypeIds = new ArrayList<Long>();
			
			partyTypeIds = partyTypeList.stream()
							.map(partyType -> partyType.getId())
							.collect(Collectors.toList());
			
		}
		else{
			
			partyTypeIds = transactionReportRequestDTO.getPartyTypeIds();
			
		}
		log.info("party type ids: "+partyTypeIds);
		
		log.info("sortColumn: "+sortColumn);

		if(sortColumn.equals("partyName"))
			sortColumn = "party.name";
		
		TransactionType invoiceType = transactionTypeService.getModelById(invoiceTypeId);
		Status status=new Status();
		if(transactionReportRequestDTO.getStatusName()!=null) {
			 status = statusRepository.findByTransactionTypeAndName(invoiceType, transactionReportRequestDTO.getStatusName());
		}
		
		//List<BalanceReport> invoices = invoiceReportPageableService.getPagedBalanceReport(invoiceType.getId(), transactionReportRequestDTO.getFinancialYearId(), transactionReportRequestDTO.getTransactionFromDate(), transactionReportRequestDTO.getTransactionToDate(), transactionReportRequestDTO.getPartyId(), partyTypeIds, pageNumber, pageSize, sortDir, sortColumn);

		//Long totalCount = invoiceReportPageableService.getBalanceReportCount(invoiceType.getId(),transactionReportRequestDTO.getFinancialYearId(), transactionReportRequestDTO.getTransactionFromDate(), transactionReportRequestDTO.getTransactionToDate(), transactionReportRequestDTO.getPartyId(), partyTypeIds);


		PayableReceivableReportDTO balanceReportDTO = invoiceReportPageableService.getPagedBalanceReport(invoiceType.getId(), transactionReportRequestDTO.getFinancialYearId(), transactionReportRequestDTO.getTransactionFromDate(), transactionReportRequestDTO.getTransactionToDate(), transactionReportRequestDTO.getPartyId(), partyTypeIds, transactionReportRequestDTO.getCreatedBy(),status.getId(), pageNumber, pageSize, sortDir, sortColumn);
		
		
//		balanceReportDTO.setBalanceReportModels(invoices);
//		balanceReportDTO.setTotalCount(totalCount);
		
		return new ResponseEntity<PayableReceivableReportDTO>(balanceReportDTO, HttpStatus.OK);
	}
	
	@PostMapping("/pay-receive/report/{transactionTypeId}/{inTypeId}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
	public ResponseEntity<PayableReceivableReportDTO> getInvoiceReport(
			@PathVariable("transactionTypeId") Long transactionTypeId,
			@PathVariable("inTypeId") Long inTypeId,
			@PathVariable("pageNumber") Integer pageNumber,
			@PathVariable("pageSize") Integer pageSize,
			@PathVariable("sortDir") String sortDir,
			@PathVariable("sortColumn") String sortColumn,
			@RequestBody TransactionReportRequestDTO transactioReportRequestDTO){
		log.info("incoming request: "+transactioReportRequestDTO);
		log.info("incoming request: "+transactioReportRequestDTO.getTransactionFromDate());
		log.info("incoming request: "+transactioReportRequestDTO.getTransactionToDate());
		log.info("incoming request: "+transactioReportRequestDTO.getMaterialId());
		log.info("incoming request: "+transactioReportRequestDTO.getPartyId());
		log.info("incoming request: "+transactioReportRequestDTO.getFinancialYearId());
		log.info("incoming request: "+transactioReportRequestDTO.getPartyTypeIds());
		
		
		
		Page<PayableReceivableReport> payableReceivableReportPage = payableReceivablePageableService.getReport(transactionTypeId, inTypeId, pageNumber, pageSize, sortDir, sortColumn, transactioReportRequestDTO);
		
		List<PayableReceivableReport> payableReceivableReport = payableReceivableReportPage.getContent();
		
		Long totalCount = payableReceivableReportPage.getTotalElements();
		
		
		PayableReceivableReportDTO payableReceivableReportDTO = new PayableReceivableReportDTO();
		
		
		payableReceivableReportDTO.setPayableReceivableReportItems(payableReceivableReport);
		payableReceivableReportDTO.setTotalCount(totalCount);
		
		return new ResponseEntity<PayableReceivableReportDTO>(payableReceivableReportDTO, HttpStatus.OK);
	}
	
	
	@PostMapping("/pay-receive/report-download/{transactionTypeId}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
	public ResponseEntity<InputStreamResource> downloadInvoiceReport(
			@PathVariable("transactionTypeId") Long transactionTypeId,
			@PathVariable("pageNumber") Integer pageNumber,
			@PathVariable("pageSize") Integer pageSize,
			@PathVariable("sortDir") String sortDir,
			@PathVariable("sortColumn") String sortColumn,
			@RequestBody TransactionReportRequestDTO transactioReportRequestDTO) throws IOException{
		
//		
//		Page<PayableReceivableReport> payableReceivableReportPage = payableReceivablePageableService.getReport(transactionTypeId,  pageNumber, pageSize, sortDir, sortColumn, transactioReportRequestDTO); 
//		
//		List<PayableReceivableReport> payableReceivableReport = payableReceivableReportPage.getContent();
//		
//		log.info("received "+payableReceivableReportPage.getTotalElements());
//		
//		ByteArrayInputStream in = payableReceivablePageableService.payableReceivableToExcel(payableReceivableReport);
//		
		
		ByteArrayInputStream in = payableReceivablePageableService.nestedExcel();
		
		log.info("sending : "+in.available());
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=prreport.xlsx");
    
        return ResponseEntity
                  .ok()
                  .headers(headers)
                  .body(new InputStreamResource(in));
    
	}
	
	
	@DeleteMapping("/pay-receive/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteById(@PathVariable("id") String id){
		
		TransactionResponseDTO deleteStatus = payableReceivableService.delete(id);
		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
		
	}
	
	
//	@DeleteMapping("/pay-receive/delete-item/{id}")
//	@Transactional
//	public ResponseEntity<TransactionResponseDTO> deleteItemById(@PathVariable("id") String id){
//		
//		TransactionResponseDTO deleteStatus = payableReceivableService.deleteItem(id);
//		log.info("returning delete message: "+deleteStatus);
//		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
//		
//	}
	
	
	@GetMapping("/payment-methods")
	public ResponseEntity<List<PaymentMethodDTO>> getAll(){
		
		log.info("in getAll");
		List<PaymentMethodDTO> paymentMethods= paymentMethodService.getAll();
		log.info("returning paymentMethods: "+paymentMethods);
		return new ResponseEntity<List<PaymentMethodDTO>>(paymentMethods, HttpStatus.OK);
	}
	
	@GetMapping("/card-type")
	public ResponseEntity<List<CardTypeDTO>> getAllCardTypes(){
		
		log.info("in getAll");
		List<CardTypeDTO> cardType= paymentMethodService.getAllCardType();
		log.info("returning paymentMethods: "+cardType);
		return new ResponseEntity<List<CardTypeDTO>>(cardType, HttpStatus.OK);
	}
	
	@GetMapping("/check-draft-for-party/{partyId}/{transactionTypeId}")
	public ResponseEntity<TransactionResponseDTO> checkDraftForParty(@PathVariable("partyId") Long partyId, @PathVariable("transactionTypeId") Long transactionTypeId){
		
		Long draftCount = payableReceivableService.getCountByPartyAndStatusIn(partyId, transactionTypeId);
		
		TransactionResponseDTO retStatus = new TransactionResponseDTO();
		
		if(draftCount > 0) {
			
			retStatus.setResponseStatus(0);
			retStatus.setResponseString("Draft for party already exists");
		}else {
			retStatus.setResponseStatus(1);
			retStatus.setResponseString("No Draft exists for party");
		}
		
		return new ResponseEntity<TransactionResponseDTO>(retStatus, HttpStatus.OK) ;
	}

	@GetMapping("/pay/{id}")
	public ResponseEntity<PayableReceivableHeaderDTO> getById(@PathVariable("id") String id){

		return new ResponseEntity<PayableReceivableHeaderDTO>(payableReceivableService.getById(id), HttpStatus.OK);
	}




}
