package com.coreerp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.model.MaterialType;
import com.coreerp.service.MaterialTypeService;

@RestController
@RequestMapping("/api")
public class MaterialTypeController {

	@Autowired
	MaterialTypeService materialTypeService;
	
	
	@GetMapping("/material-type")
	public ResponseEntity<List<MaterialType>> getAll(){
		return new ResponseEntity<List<MaterialType>>(materialTypeService.getAll(),HttpStatus.OK);
	}
}
