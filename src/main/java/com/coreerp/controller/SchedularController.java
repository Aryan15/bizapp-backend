package com.coreerp.controller;

import com.coreerp.serviceimpl.RecurringInvoiceServiceScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class SchedularController {


    @Autowired
    RecurringInvoiceServiceScheduler recurringInvoiceServiceScheduler;


    @PostMapping(value="/transaction_schedular")
    public ResponseEntity<Boolean> createTransaction() throws Exception {

        try {
            recurringInvoiceServiceScheduler.startRecurringInvoice();
        }

        catch(Exception e){

            return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);


        }

        return new ResponseEntity<>(true, HttpStatus.OK);

    }




}
