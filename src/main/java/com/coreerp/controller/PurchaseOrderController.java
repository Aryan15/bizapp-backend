package com.coreerp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.dao.StatusRepository;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.PurchaseOrderHeaderRepository;
import com.coreerp.dto.PurchaseOrderDTO;
import com.coreerp.dto.PurchaseOrderReportDTO;
import com.coreerp.dto.PurchaseOrderWrapperDTO;
import com.coreerp.dto.RecentPurchaseOrderDTO;
import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.PartyMapper;
import com.coreerp.mapper.PurchaseOrderHeaderMapper;
import com.coreerp.model.FinancialYear;
import com.coreerp.model.Party;
import com.coreerp.model.PartyType;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.model.Status;
import com.coreerp.model.TransactionType;
import com.coreerp.reports.model.PurchaseOrderReport;
import com.coreerp.reports.service.PurchaseOrderReportPageableService;
import com.coreerp.reports.service.RecentPurchaseOrderService;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.PartyService;
import com.coreerp.service.PartyTypeService;
import com.coreerp.service.PurchaseOrderService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TransactionTypeService;

@RestController
@RequestMapping("/api")
public class PurchaseOrderController {

	@Autowired
	PurchaseOrderHeaderRepository purchaseOrderHeaderRepository;
	
	final static Logger log = LogManager.getLogger(PurchaseOrderController.class);
	
	@Autowired
	PurchaseOrderService purchaseOrderService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	PartyTypeService partyTypeService;
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	PurchaseOrderReportPageableService purchaseOrderReportPageableService;
	
	@Autowired
	PartyMapper partyMapper;
	
	@Autowired
	RecentPurchaseOrderService recentPurchaseOrderService;
	
	@Autowired
	PurchaseOrderHeaderMapper purchaseOrderHeaderMapper;
	
	@Autowired
	FinancialYearService financialYearService;

	@Autowired
	StatusRepository statusRepository;
	
	@GetMapping("/po")
	public ResponseEntity<List<PurchaseOrderDTO>> getAllPo(){
		List<PurchaseOrderDTO> poListOut = purchaseOrderService.getAll();
	
		return new ResponseEntity<List<PurchaseOrderDTO>>(poListOut, HttpStatus.OK);
	}
	
	@GetMapping("/po/recent/{quotationTypeId}/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<RecentPurchaseOrderDTO> getRecentPo(
			  @PathVariable("quotationTypeId") Long quotationTypeId
			, @PathVariable("sortColumn") String sortColumn
			, @PathVariable("sortDirection") String sortDirection
			, @PathVariable("pageNumber") Integer pageNumber
			, @PathVariable("pageSize") Integer pageSize
			, @RequestParam(value = "searchText", required=false) String searchText){
		
		
		//log.info("searchText: "+searchText);
		//log.info("searchText.isEmpty(): "+searchText.isEmpty());
		//log.info("searchText.length(): "+searchText.length());
		//log.info("searchText == null : "+searchText == null ? "NULL" : "NOT NULL");
		//log.info("searchText.trim(): "+searchText.trim());
		
		//log.info("sortColumn: "+sortColumn);
		//log.info("sort direction: "+sortDirection);
		//log.info("pageNumber: " +pageNumber);
		//log.info("pageSize: "+pageSize);


		Long totalCount = 0l;
		List<PurchaseOrderHeader> purchaseOrderHeaders = new ArrayList<PurchaseOrderHeader>();
		
		RecentPurchaseOrderDTO recentPoDTO = new RecentPurchaseOrderDTO();
		
		TransactionType quotationType = transactionTypeService.getModelById(quotationTypeId);
		
		FinancialYear financialYear = financialYearService.findByIsActive(1);

		if (searchText.isEmpty() || searchText.equals("null")) {
			searchText=null;
		}
		Page<PurchaseOrderHeader> recentPo = recentPurchaseOrderService.getPageList(quotationType, financialYear, searchText, sortColumn, sortDirection, pageNumber, pageSize);
		purchaseOrderHeaders.addAll(recentPo.getContent());
		totalCount = recentPo.getTotalElements();
		recentPoDTO.setPurchaseOrderHeaders(purchaseOrderHeaderMapper.modelToRecentDTOList(purchaseOrderHeaders));
		recentPoDTO.setTotalCount(totalCount);
		return new ResponseEntity<RecentPurchaseOrderDTO>(recentPoDTO, HttpStatus.OK);
		
	}
	
	
	@PostMapping("/po/report/{poTypeId}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
	public ResponseEntity<PurchaseOrderReportDTO> getPOReport(
			@PathVariable("poTypeId") Long poTypeId,
			@PathVariable("pageNumber") Integer pageNumber,
			@PathVariable("pageSize") Integer pageSize,
			@PathVariable("sortDir") String sortDir,
			@PathVariable("sortColumn") String sortColumn,
			@RequestBody TransactionReportRequestDTO poReportRequestDTO){
		//log.info("incoming request: "+poReportRequestDTO);
		//log.info("incoming request: "+poReportRequestDTO.getTransactionFromDate());
		//log.info("incoming request: "+poReportRequestDTO.getTransactionToDate());
		//log.info("incoming request: "+poReportRequestDTO.getMaterialId());
		//log.info("incoming request: "+poReportRequestDTO.getPartyId());
		//log.info("incoming request: "+poReportRequestDTO.getFinancialYearId());
		//log.info("incoming request: "+poReportRequestDTO.getPartyTypeIds());
		
		List<Long> partyTypeIds = null;
		
		if(poReportRequestDTO.getPartyTypeIds().isEmpty()){
			PartyType partyTypeCustomer = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
			PartyType partyTypeSupplier = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);
			PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
			
			List<PartyType> partyTypeList = new ArrayList<PartyType>();
			
			partyTypeList.add(partyTypeCustomer);
			partyTypeList.add(partyTypeSupplier);
			partyTypeList.add(partyTypeBoth);
			
			//List<Long> partyTypeIds = new ArrayList<Long>();
			
			partyTypeIds = partyTypeList.stream()
							.map(partyType -> partyType.getId())
							.collect(Collectors.toList());
			
		}
		else{
			
			partyTypeIds = poReportRequestDTO.getPartyTypeIds();
			
		}
		//log.info("party type ids: "+partyTypeIds);
		
		//log.info("sortColumn: "+sortColumn);
		if(sortColumn.equals("customerName"))
			sortColumn = "party.name";
		
		TransactionType poType = transactionTypeService.getModelById(poTypeId);
		
		List<PurchaseOrderReport> poReport = purchaseOrderReportPageableService.getPagedPurchaseOrderReport(poType.getId(), poReportRequestDTO.getFinancialYearId(), poReportRequestDTO.getTransactionFromDate(), poReportRequestDTO.getTransactionToDate(), poReportRequestDTO.getPartyId(),  poReportRequestDTO.getMaterialId(),partyTypeIds, pageNumber, pageSize, sortDir, sortColumn);
		
		Long totalCount = purchaseOrderReportPageableService.getPurchaseOrderReportCount(poType.getId(),poReportRequestDTO.getFinancialYearId(), poReportRequestDTO.getTransactionFromDate(), poReportRequestDTO.getTransactionToDate(), poReportRequestDTO.getPartyId(), poReportRequestDTO.getMaterialId(), partyTypeIds);
		
		
		PurchaseOrderReportDTO purchaseOrderReportDTO = new PurchaseOrderReportDTO();
		
		
		purchaseOrderReportDTO.setPurchaseOrderReports(poReport);
		purchaseOrderReportDTO.setTotalCount(totalCount);
		
		return new ResponseEntity<PurchaseOrderReportDTO>(purchaseOrderReportDTO, HttpStatus.OK);
	}
	
	@GetMapping("/po/party/{id}/{poType}/{t-type}")
	public ResponseEntity<List<PurchaseOrderDTO>> getByParty(@PathVariable("id") Long id, @PathVariable("poType") String poType, @PathVariable("t-type") String Ttype){
		Party party = partyMapper.dtoToModelMap(partyService.getPartyById(id));
		
		TransactionType transactionType = transactionTypeService.getByName(poType);
		TransactionType typeOfTransaction = transactionTypeService.getByName(Ttype);
		log.info(transactionType.getId()+"---------------"+typeOfTransaction.getName());
//		List<Status> statuses = Arrays.asList(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DC_GENERATED), statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DC_GENERATED), statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED));
		List<Status> statuses = new ArrayList<Status>(); 
				
		if(transactionType.getName().equals(ApplicationConstants.PURCHASE_ORDER_INCOMING_JOBWORK)){

			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_INCOMING_JW_DC_CREATED));
			//statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_INCOMING_JW_DC_PARTIAL));
		}else if(transactionType.getName().equals(ApplicationConstants.PURCHASE_ORDER_OUTGOING_JOBWORK)){
			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_OUTGOING_JW_DC_CREATED));

			//statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_INCOMING_JW_DC_PARTIAL));
		}
		else{

			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED));
			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_INVOICED));
			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DC_GENERATED));
		}

		if(typeOfTransaction.getName().equals(ApplicationConstants.INVOICE_TYPE_INCOMING_JW_INVOICE) ){

			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_INCOMING_JW_DC_CREATED));
			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_INCOMING_JW_DC_PARTIAL));
			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_INVOICED));
		}
		if(typeOfTransaction.getName().equals(ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE) ){

			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_OUTGOING_JW_DC_CREATED));
			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_OUTGOING_JW_DC_PARTIAL));
			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_INVOICED));
		}
		if(typeOfTransaction.getName().equals(ApplicationConstants.INCOMING_JOBWORK_IN_DC) ){
			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_INVOICED));
			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_INVOICED));
		}
		if(typeOfTransaction.getName().equals(ApplicationConstants.OUTGOING_JOBWORK_OUT_DC) ){
			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_INVOICED));
			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_INVOICED));
		}

//		statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_DC));
		
		//log.info("statuses.size(): "+statuses.size());
		
		//log.info("transactionType  in po "+typeOfTransaction.getName());
		//log.info("transactionType "+transactionType.getName());
		if(typeOfTransaction.getName().equals(ApplicationConstants.INVOICE_TYPE_CUSTOMER)||typeOfTransaction.getName().equals(ApplicationConstants.INVOICE_TYPE_SUPPLIER)||typeOfTransaction.getName().equals(ApplicationConstants.INVOICE_TYPE_PROFORMA))
		{

			//log.info("1");
			statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_PARTIAL_DC));
		}
		
		List<PurchaseOrderDTO> poListOut = purchaseOrderService.getByPartyAndStatusNotIn(party, transactionType,statuses, typeOfTransaction);//purchaseOrderService.getByParty(party);
		//log.info("Returning po list");
		//log.info(poListOut);
		return new ResponseEntity<List<PurchaseOrderDTO>>(poListOut, HttpStatus.OK);
	}
	
	
	@GetMapping("/po/{id}")
	public ResponseEntity<PurchaseOrderDTO> getById(@PathVariable("id") String id){
		
		return new ResponseEntity<PurchaseOrderDTO>(purchaseOrderService.getById(id), HttpStatus.OK);
	}
	
	@PostMapping("/po")
	@Transactional
	public ResponseEntity<PurchaseOrderDTO> save(@RequestBody PurchaseOrderWrapperDTO purchaseOrderWrapperDTO){


		return new ResponseEntity<PurchaseOrderDTO> (purchaseOrderService.save(purchaseOrderWrapperDTO), HttpStatus.OK);
		
	}
	
//	@PostMapping("/po/cancel/{id}")
//	public ResponseEntity<PurchaseOrderDTO> cancel(@PathVariable("id") String id){
//		log.info("cancelling po id: "+id);
//		
//		PurchaseOrderDTO purchaseOrderDTO = purchaseOrderService.cancel(id);
//				
//		log.info("purchaseOrderDTO: "+purchaseOrderDTO);
//		log.info("Done... returning");
//		return new ResponseEntity<PurchaseOrderDTO> (purchaseOrderDTO, HttpStatus.OK);
//		
//	}

	
//	@PostMapping("/po/delete/{id}")
//	public ResponseEntity<PurchaseOrderDTO> delete(@PathVariable("id") String id){
//		log.info("Deleting po id: "+id);
//		
//		PurchaseOrderDTO purchaseOrderDTO = purchaseOrderService.delete(id);
//				
//		log.info("purchaseOrderDTO: "+purchaseOrderDTO);
//		log.info("Done... returning");
//		return new ResponseEntity<PurchaseOrderDTO> (purchaseOrderDTO, HttpStatus.OK);
//		
//	}
	
	@GetMapping("/po/po-no/{poNumber}")
	public ResponseEntity<List<PurchaseOrderDTO>>  getPurchaseOrderByPurchaseOrderNumber(@PathVariable("poNumber") String poNumber){
		//log.info("in getPurchaseOrderByPurchaseOrderNumber: "+poNumber);
		
		return new ResponseEntity<List<PurchaseOrderDTO>> (purchaseOrderService.getByPurchaseOrderNumber(poNumber), HttpStatus.OK);
	}
	
//	@GetMapping("/po/items-for-invoice/{id}")
//	public List<PurchaseOrderItemDTO> getPurchaseOrderItemsForInvoice(@PathVariable("headerId") String headerId){
//		log.info("in getPurchaseOrderItemsForInvoice: "+headerId);
//		return purchaseOrderService.getItemsForInvoice(headerId);
//	}
	
	
	/*
	@DeleteMapping("/quotation/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteById(@PathVariable("id") String id){
		TransactionResponseDTO deleteStatus = quotationService.delete(id);
		log.info("returning delete message: "+deleteStatus);
		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
	}
	 */
	 
	
	@DeleteMapping("/po/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteById(@PathVariable("id") String id){
		
		TransactionResponseDTO deleteStatus = purchaseOrderService.delete(id);
		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
		
	}
	
	
	@DeleteMapping("/po/delete-item/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteItemById(@PathVariable("id") String id){
		
		TransactionResponseDTO deleteStatus = purchaseOrderService.deleteItem(id);
		//log.info("returning delete message: "+deleteStatus);
		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
		
	}
	
	@GetMapping("/check-poNumber/{partyId}/{poTypeId}")
	public ResponseEntity<TransactionResponseDTO> checkpoNumber(
			@PathVariable("poTypeId") Long poTypeId
			, @PathVariable("partyId") Long partyId
			, @RequestParam(value = "poNumber", required=false) String poNumber){
		
		
		TransactionType poType = transactionTypeService.getModelById(poTypeId);
//		log.info("poType: "+poType);
		
		Long count = purchaseOrderService.countByPurchaseOrderNumberAndTransactionType(poNumber, poType, partyId );
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (count > 0){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("poNumber  "+poNumber+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("poNumber available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}

	@PostMapping("/po/balance-report/{poTypeId}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
	public ResponseEntity<PurchaseOrderReportDTO> getBalanceReport(
			@PathVariable("poTypeId") Long poTypeId,
			@PathVariable("pageNumber") Integer pageNumber,
			@PathVariable("pageSize") Integer pageSize,
			@PathVariable("sortDir") String sortDir,
			@PathVariable("sortColumn") String sortColumn,
			@RequestBody TransactionReportRequestDTO transactionReportRequestDTO) {
		log.info("incoming request: " + transactionReportRequestDTO);
		log.info("incoming request: " + transactionReportRequestDTO.getTransactionFromDate());
		log.info("incoming request: " + transactionReportRequestDTO.getTransactionToDate());
		log.info("incoming request: " + transactionReportRequestDTO.getMaterialId());
		log.info("incoming request: " + transactionReportRequestDTO.getPartyId());
		log.info("incoming request: " + transactionReportRequestDTO.getFinancialYearId());
		log.info("incoming request: " + transactionReportRequestDTO.getPartyTypeIds());
		log.info("incoming request: " + transactionReportRequestDTO.getStatusName());
		List<Long> partyTypeIds = null;

		if (transactionReportRequestDTO.getPartyTypeIds().isEmpty()) {
			PartyType partyTypeCustomer = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
			PartyType partyTypeSupplier = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);
			PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);

			List<PartyType> partyTypeList = new ArrayList<PartyType>();

			partyTypeList.add(partyTypeCustomer);
			partyTypeList.add(partyTypeSupplier);
			partyTypeList.add(partyTypeBoth);

			//List<Long> partyTypeIds = new ArrayList<Long>();

			partyTypeIds = partyTypeList.stream()
					.map(partyType -> partyType.getId())
					.collect(Collectors.toList());

		} else {

			partyTypeIds = transactionReportRequestDTO.getPartyTypeIds();

		}
		log.info("party type ids: " + partyTypeIds);

		log.info("sortColumn: " + sortColumn);
		if (sortColumn.equals("partyName")) {
			sortColumn = "party.name";
		}

		if (sortColumn.equals("materialName")) {

			sortColumn = "mat.name";
		}

		TransactionType poType = transactionTypeService.getModelById(poTypeId);
		Status status = new Status();
		if (transactionReportRequestDTO.getStatusName() != null) {
			status = statusRepository.findByTransactionTypeAndName(poType, transactionReportRequestDTO.getStatusName());
		}


		PurchaseOrderReportDTO poBalanceReport = purchaseOrderReportPageableService.getPagedBalanceReport(poType.getId(), transactionReportRequestDTO.getFinancialYearId(), transactionReportRequestDTO.getTransactionFromDate(), transactionReportRequestDTO.getTransactionToDate(), transactionReportRequestDTO.getPartyId(), partyTypeIds, transactionReportRequestDTO.getCreatedBy(), status.getId(), pageNumber, pageSize, sortDir, sortColumn);

		return new ResponseEntity<PurchaseOrderReportDTO>(poBalanceReport, HttpStatus.OK);
	}

	@GetMapping("/po/close-po/{poId}/{statusId}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> closePo(@PathVariable("poId") String id,@PathVariable("statusId") Long statusId){

		TransactionResponseDTO deleteStatus = purchaseOrderService.closePo(id,statusId);
		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);

	}

}
