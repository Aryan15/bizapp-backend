package com.coreerp.controller;

import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.jwt.JwtAuthenticationRequest;
import com.coreerp.jwt.JwtAuthenticationResponse;
import com.coreerp.tally.service.TallyVoucherService;
import com.coreerp.tallyexternal.model.VoucherEnvelopeEx;
import org.apache.http.HttpHeaders;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/tally")
public class TallyExternalApiController {

    final static Logger log = LogManager.getLogger(TallyExternalApiController.class);



    @Value("${server.port}")
    private String serverPort;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    @Autowired
    TallyVoucherService tallyVoucherService;




    @GetMapping(value="/purchase-invoices",produces= MediaType.APPLICATION_XML_VALUE)
    public String getPurchaseInvoices (
            @RequestHeader Map<String, String> headers) {

        String username = headers.get("username");
        String password = headers.get("password");
        log.info("Tally export request from : "+username);

        JwtAuthenticationRequest jwtAuthenticationRequest = new JwtAuthenticationRequest();
        jwtAuthenticationRequest.setUsername(username);
        jwtAuthenticationRequest.setPassword(password);


        String apiUrl = "http://localhost:"+serverPort+contextPath;
        WebClient webClient = WebClient.builder().baseUrl(apiUrl).build();


        Mono<JwtAuthenticationResponse> token = webClient.post()
                .uri("/auth")
                .body(BodyInserters.fromObject(jwtAuthenticationRequest))
                .retrieve()
                .bodyToMono(JwtAuthenticationResponse.class)
                .onErrorMap(e -> new Exception("Error While getting Token", e));

        String tokenString = token.block().getToken();
        log.info("valid user with token "+tokenString);

        TransactionReportRequestDTO transactionReportRequestDTO = new TransactionReportRequestDTO();
        //transactionReportRequestDTO.setPartyId(2l);

        Mono<String> envelopeMono = webClient.post().uri("/api/tally-voucher-external-xml/{invoiceTypeId}",5l)
                .headers(headerss -> headerss.setBearerAuth(tokenString))
                .body(BodyInserters.fromObject(transactionReportRequestDTO))
                .retrieve()
                .bodyToMono(String.class)
                .onErrorMap(e -> new Exception("Error While getting voucher", e));

        String env = envelopeMono.block();
        log.info("got envelope: "+ env);

        return env;


    }




    @GetMapping(value="/salse-invoices",produces= MediaType.APPLICATION_XML_VALUE)
    public VoucherEnvelopeEx getSalesInvoices (
            @RequestHeader Map<String, String> headers) {

        String username = headers.get("username");
        String password = headers.get("password");
        log.info("Tally export request from : "+username);

        JwtAuthenticationRequest jwtAuthenticationRequest = new JwtAuthenticationRequest();
        jwtAuthenticationRequest.setUsername(username);
        jwtAuthenticationRequest.setPassword(password);


        String apiUrl = "http://localhost:"+serverPort+contextPath;
        WebClient webClient = WebClient.builder().baseUrl(apiUrl).build();


        Mono<JwtAuthenticationResponse> token = webClient.post()
                .uri("/auth")
                .body(BodyInserters.fromObject(jwtAuthenticationRequest))
                .retrieve()
                .bodyToMono(JwtAuthenticationResponse.class)
                .onErrorMap(e -> new Exception("Error While getting Token", e));

        String tokenString = token.block().getToken();
        log.info("valid user with token "+tokenString);

        TransactionReportRequestDTO transactionReportRequestDTO = new TransactionReportRequestDTO();
       transactionReportRequestDTO.setPartyId(1l);

        Mono<VoucherEnvelopeEx> envelopeMono = webClient.post().uri("/api/tally-voucher-external-xml/{invoiceTypeId}",1l)
                .headers(headerss -> headerss.setBearerAuth(tokenString))
                .body(BodyInserters.fromObject(transactionReportRequestDTO))
                .retrieve()
                .bodyToMono(VoucherEnvelopeEx.class)
                .onErrorMap(e -> new Exception("Error While getting voucher", e));

        VoucherEnvelopeEx env = envelopeMono.block();
        log.info("got envelope: "+ env);

        return env;


    }

}
