package com.coreerp.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dto.NumberRangeConfigurationDTO;
import com.coreerp.dto.PrintTemplateWrapperDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.TransactionType;
import com.coreerp.service.NumberRangeConfigurationService;
import com.coreerp.service.TransactionTypeService;

@RestController
@RequestMapping("/api")
public class NumberRangeConfigController {

	final static Logger log = LogManager.getLogger(NumberRangeConfigController.class);
	
	@Autowired
	NumberRangeConfigurationService numberRangeConfigurationService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@GetMapping("/num-range-config")
	public ResponseEntity<List<NumberRangeConfigurationDTO>> getAll(){
		
		return new ResponseEntity<List<NumberRangeConfigurationDTO>>(numberRangeConfigurationService.getAll(), HttpStatus.OK);
		
	}
	
	@GetMapping("/num-range-config/transactionType/{transactionTypeId}")
	public ResponseEntity<NumberRangeConfigurationDTO> getByTransactionTypeId(@PathVariable("transactionTypeId") Long transactionTypeId){
		
		TransactionType transactionType = transactionTypeService.getModelById(transactionTypeId);
		
		return new ResponseEntity<NumberRangeConfigurationDTO>(numberRangeConfigurationService.getDTOByTransactionType(transactionType), HttpStatus.OK);
	}
	
	@PostMapping("/num-range-config")
	public ResponseEntity<List<NumberRangeConfigurationDTO>> saveAll(List<NumberRangeConfigurationDTO> inList){
		
		return new ResponseEntity<List<NumberRangeConfigurationDTO>>(numberRangeConfigurationService.saveAll(inList), HttpStatus.OK);
		
	}	
	
	@PostMapping("/print-template")
	public ResponseEntity<List<NumberRangeConfigurationDTO>> savePrintTemplate(@RequestBody PrintTemplateWrapperDTO wrap ){
		
		List<NumberRangeConfigurationDTO> inList = wrap.getTemplates();
		
		log.info("inside savePrintTemplate");
		return new ResponseEntity<List<NumberRangeConfigurationDTO>>(numberRangeConfigurationService.updatePrintTemplete(inList), HttpStatus.OK);
		
	}	
	
	
	@PostMapping("/print-topmargin")
	public ResponseEntity<Boolean> savePrintTopMargin(@RequestBody NumberRangeConfigurationDTO dto ){
		
		
		
		log.info("inside savePrintTemplate");
		return new ResponseEntity<Boolean>(numberRangeConfigurationService.updatePrintTopMargin(dto), HttpStatus.OK);
		
	}	
	
	@DeleteMapping("/num-range-config/{id}")
	public ResponseEntity<TransactionResponseDTO> delete(@PathVariable("id") Long id){
		
		TransactionResponseDTO del = numberRangeConfigurationService.delete(id);
		
		return new ResponseEntity<TransactionResponseDTO>(del, HttpStatus.OK);
	}
}
