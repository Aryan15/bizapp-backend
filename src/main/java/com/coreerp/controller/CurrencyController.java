package com.coreerp.controller;


import com.coreerp.dto.CountryDTO;
import com.coreerp.dto.CurrencyDTO;
import com.coreerp.dto.PartyDTO;
import com.coreerp.service.CurrencyService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CurrencyController {
    final static Logger log = LogManager.getLogger(CurrencyController.class);
    @Autowired
    CurrencyService currencyService;


    @GetMapping("/currency")
    public ResponseEntity<List<CurrencyDTO>> getAllCountries() {

        log.info("In getAllCurrency");
        List<CurrencyDTO> currencyDTOList = currencyService.getAllCurrency();

        log.info("currencyDTOList.size(): "+ currencyDTOList.size());

        return new ResponseEntity<List<CurrencyDTO>>(currencyDTOList, HttpStatus.OK);
    }

    @GetMapping("/currency/{id}")
    public ResponseEntity<CurrencyDTO> getCurrencyById(@PathVariable("id") Long id){

        CurrencyDTO currencyDTOObj = currencyService.getCurrencyById(id);
        log.info("currencyDTOObj bizapp =  "+currencyDTOObj.getCurrencySymbol() );
        return new ResponseEntity<CurrencyDTO>(currencyDTOObj, HttpStatus.OK);
    }

}
