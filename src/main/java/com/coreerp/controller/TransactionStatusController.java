package com.coreerp.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import com.coreerp.dto.InvoiceItemDTO;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.TransactionReffernceModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dao.StatusRepository;
import com.coreerp.dao.TransactionStatusChangeRepository;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dto.StatusDTO;
import com.coreerp.dto.TransactionStatusChangeDTO;
import com.coreerp.mapper.StatusMapper;
import com.coreerp.mapper.TransactionStatusChangeMapper;
import com.coreerp.model.TransactionStatusChange;
import com.coreerp.model.TransactionType;
import com.coreerp.service.PartyService;
import com.coreerp.service.TransactionTypeService;


@RestController
@RequestMapping("/api")
public class TransactionStatusController {
	
	final static Logger log = LogManager.getLogger(TransactionStatusController.class);
	
	@Autowired
	StatusRepository statusRepository;
	
	@Autowired
	TransactionStatusChangeRepository transactionStatusChangeRepository;
	
	@Autowired
	TransactionStatusChangeMapper transactionStatusChangeMapper;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	StatusMapper statusMapper;
	
	@Autowired
	TransactionTypeRepository transactionTypeRepository ; 
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@GetMapping("/tran-status")
	public ResponseEntity<List<StatusDTO>> getAll(){
		
		return new ResponseEntity<List<StatusDTO>>(statusMapper.modelToDTOList(statusRepository.findAll()), HttpStatus.OK);
	}
	
	@GetMapping("/status-change/source-ref/{id}/{transactionTypeId}")
	public ResponseEntity<Set<TransactionStatusChangeDTO>> getStatusChangeRef(@PathVariable("id") String id,@PathVariable("transactionTypeId") Long transactionTypeId){

		log.info(transactionTypeId+"in getStatusChangeRef"+id);
		List<TransactionStatusChange> statusChangeList=new ArrayList<TransactionStatusChange>();

		if(transactionTypeId == 9 || transactionTypeId==10){
			TransactionStatusChange statusChange = transactionStatusChangeRepository.findByLatestSourceTransactionsId(id);
			if(statusChange.getId()!=null){

				 statusChangeList = transactionStatusChangeRepository.findByLatestSourceTransactionId(statusChange.getTransactionId());//findBySourceTransactionId(id);
				if (statusChangeList.size() > 0 && statusChangeList != null) {

					for (Integer i = 1; i <= statusChangeList.size(); i++) {

						TransactionStatusChange status = statusChangeList.get(i - 1);


						TransactionStatusChange statusChange1 = transactionStatusChangeRepository.findByLatestSourceTransactionsId(status.getTransactionId());


						if (statusChange1 != null) {
							statusChangeList.add(statusChange1);
							TransactionStatusChange statusChange2 = transactionStatusChangeRepository.findByLatestSourceTransactionsId(statusChange1.getTransactionId());
							if (statusChange2 != null) {
								statusChangeList.add(statusChange2);
								TransactionStatusChange statusChange3 = transactionStatusChangeRepository.findByLatestSourceTransactionsId(statusChange2.getTransactionId());
								if (statusChange3 != null) {
									statusChangeList.add(statusChange3);
								}
							}
						}


					}

				}

                statusChangeList.add(statusChange);

			}





		}
		else {

			 statusChangeList = transactionStatusChangeRepository.findByLatestSourceTransactionId(id);//findBySourceTransactionId(id);
			if (statusChangeList.size() > 0 && statusChangeList != null) {

				for (Integer i = 1; i <= statusChangeList.size(); i++) {

					TransactionStatusChange status = statusChangeList.get(i - 1);


					TransactionStatusChange statusChange = transactionStatusChangeRepository.findByLatestSourceTransactionsId(status.getTransactionId());


					if (statusChange != null) {
						statusChangeList.add(statusChange);
						TransactionStatusChange statusChange1 = transactionStatusChangeRepository.findByLatestSourceTransactionsId(statusChange.getTransactionId());
						if (statusChange1 != null) {
							statusChangeList.add(statusChange1);
						}
					}


				}
				;
			}
		}



		List<TransactionStatusChangeDTO> statusChangeDTOList = transactionStatusChangeMapper.modelToDTOList(statusChangeList);
		
		Set<TransactionStatusChangeDTO> statusChangeListUnique = new HashSet<TransactionStatusChangeDTO>(statusChangeDTOList);

		log.info("got list: "+ statusChangeList.size());
		log.info("got unique list: "+statusChangeListUnique.size());
		return new ResponseEntity<Set<TransactionStatusChangeDTO>>(statusChangeListUnique, HttpStatus.OK);
	}

	
	@GetMapping("/status-change/link-ref/{id}")
	public ResponseEntity<Set<TransactionStatusChangeDTO>> getStatusChangeLinkRef(@PathVariable("id") String id){

		log.info("in getStatusChangeLinkRef"+id);
		List<TransactionReffernceModel> statusChangeList = transactionStatusChangeRepository.findByTransactionIdAndSourceTransactionIdIsNotNulls(id);
		if(statusChangeList.size()>0 && statusChangeList != null) {

			for(Integer i =1; i<=statusChangeList.size();i++){

                TransactionReffernceModel status =statusChangeList.get(i-1);

                //statusChangeList.add(status);
				List<TransactionReffernceModel> statusChangeList1 =transactionStatusChangeRepository.findByTransactionIdAndSourceTransactionIdIsNotNulls(status.getSourceTransactionId());


				if (statusChangeList1.size()>0 && statusChangeList1 != null) {
					for (Integer J = 1; J <= statusChangeList1.size(); J++) {
                        TransactionReffernceModel status1 = statusChangeList1.get(J - 1);
						statusChangeList.add(status1);
						List<TransactionReffernceModel> statusChangeList2 = transactionStatusChangeRepository.findByTransactionIdAndSourceTransactionIdIsNotNulls(status1.getSourceTransactionId());


						if (statusChangeList2.size() > 0 && statusChangeList2 != null) {
							for (Integer k = 1; k <= statusChangeList2.size(); k++) {
                                TransactionReffernceModel status2 = statusChangeList2.get(k - 1);
								statusChangeList.add(status2);

							}
						}
					}

				}


			};
		}
        List<TransactionStatusChangeDTO> statusChange=new ArrayList<TransactionStatusChangeDTO>();

        statusChangeList.forEach(status ->{
            TransactionStatusChangeDTO transactionStatusChange =new TransactionStatusChangeDTO();
            transactionStatusChange.setTransactionTypeId(status.getTransactionTypeId());
            transactionStatusChange.setTransactionTypeName(status.getTransactionTypeName());
            transactionStatusChange.setTransactionId(status.getTransactionId());
            transactionStatusChange.setTransactionNumber(status.getTransactionNumber());
            transactionStatusChange.setStatusId(status.getStatusId());
			transactionStatusChange.setStatusName(status.getStatusName());
            transactionStatusChange.setLatest(status.getLatest());
            transactionStatusChange.setSourceTransactionTypeId(status.getSourceTransactionTypeId());
			transactionStatusChange.setSourceTransactionTypeName(status.getSourceTransactionTypeName());
            transactionStatusChange.setSourceTransactionId(status.getSourceTransactionId());
            transactionStatusChange.setSourceTransactionNumber(status.getSourceTransactionNumber());
            transactionStatusChange.setSourceOperation(status.getSourceOperation());



            statusChange.add(transactionStatusChange);
        });
		//List<TransactionStatusChangeDTO> statusChangeDTOList = transactionStatusChangeMapper.modelToDTOList(statusChange);
		
		Set<TransactionStatusChangeDTO> statusChangeListUnique = new HashSet<TransactionStatusChangeDTO>(statusChange);
		log.info("got list: "+statusChangeList.size());
		log.info("got unique list: "+statusChangeListUnique.size());
		return new ResponseEntity<Set<TransactionStatusChangeDTO>>(statusChangeListUnique, HttpStatus.OK);
	}

	@PostMapping("/status-change/exists")
	public ResponseEntity<Boolean> getExistsForTransactionTypes(@RequestBody List<Long> tTypeIds){
		
		List<TransactionType> tTypes = transactionTypeRepository.findByIdIn(tTypeIds);
		
		return new ResponseEntity<Boolean>(transactionStatusChangeRepository.existsByTransactionTypeIn(tTypes), HttpStatus.OK);
		
	}
	
	@GetMapping("/allow-stock-increase")
	public ResponseEntity<Boolean> allowStockIncrease(){
		return new ResponseEntity<Boolean>(transactionTypeService.allowStockIncreaseOption(), HttpStatus.OK);
	}
	
	@GetMapping("/allow-stock-decrease")
	public ResponseEntity<Boolean> allowStockDecrease(){
		return new ResponseEntity<Boolean>(transactionTypeService.allowStockDecreaseOption(), HttpStatus.OK);
	}
	
	@GetMapping("/party-status/{id}")
	public ResponseEntity<Boolean> getPartyStatus(@PathVariable("id") Long id){
		return new ResponseEntity<Boolean>(partyService.checkPartyStatus(id), HttpStatus.OK);

	}

	@PostMapping("/check-higher-count")
	public ResponseEntity<Integer> checkHigherCount(@RequestBody List<String> tranNumbers){
		log.info("tranNumbers: "+tranNumbers);
		return new ResponseEntity<>(transactionStatusChangeRepository.countByTransactionNumber(tranNumbers), HttpStatus.OK);
	}
}
