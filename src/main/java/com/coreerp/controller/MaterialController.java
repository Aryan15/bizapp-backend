package com.coreerp.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.MaterialTypeRepository;
import com.coreerp.dao.SupplyTypeRepository;
import com.coreerp.dto.MaterialDTO;
import com.coreerp.dto.MaterialRecentDTO;
import com.coreerp.dto.MaterialReportDTO;
import com.coreerp.dto.MaterialStockCheckDTO;
import com.coreerp.dto.MaterialStockCheckDTOWrapper;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.MaterialMapper;
import com.coreerp.model.Material;
import com.coreerp.model.MaterialType;
import com.coreerp.model.Party;
import com.coreerp.model.SupplyType;
import com.coreerp.model.TransactionType;
import com.coreerp.reports.service.MaterialReportService;
import com.coreerp.service.MaterialService;
import com.coreerp.service.MaterialTypeService;
import com.coreerp.service.PartyService;

@RestController
@RequestMapping("/api")
public class MaterialController {

	final static Logger log = LogManager.getLogger(MaterialController.class);
	
	@Autowired
	MaterialService materialService;
	
	@Autowired
	MaterialTypeService materialTypeService;
	
	@Autowired
	MaterialReportService materialReportService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	MaterialMapper materialMapper;
	
	@Autowired
	SupplyTypeRepository supplyTypeRepository;
	
	@GetMapping("/materials")
	public ResponseEntity<List<MaterialDTO>> getAll(){
		
		log.info("in getAll");
		List<MaterialDTO> materials = materialService.getAllMaterials();
		
		Collections.sort(materials);
		
		log.info("Returning totatl "+materials.size()+" materials");
		return new ResponseEntity<List<MaterialDTO>>(materials, HttpStatus.OK);
		
		
	}
	
	@GetMapping("/material/{id}")
	public ResponseEntity<MaterialDTO> getMaterial(@PathVariable("id") Long id){
		log.info("in get material....id: "+id);
		
		MaterialDTO materialOut = materialService.getMaterialDTOById(id);
		
		return new ResponseEntity<MaterialDTO>(materialOut, HttpStatus.OK);
		
	}
	
	@GetMapping("/material/search-name/{name}")
	public ResponseEntity<List<MaterialDTO>> getMaterialForNamePattern(@PathVariable("name") String name){
		
		log.info("in getMaterialForNamePattern: "+name);
		
		List<MaterialDTO> dtoList = materialService.searchMaterialByNamePattern(name);
		
		return new ResponseEntity<List<MaterialDTO>> (dtoList, HttpStatus.OK);
	}
	
	@GetMapping("/material/search-name/{supplyTypeId}/{partyId}/{buyOrSell}")
	public ResponseEntity<List<MaterialDTO>> getMaterialForNameAndPartyAndSupplyType(
			@PathVariable("supplyTypeId") Long supplyTypeId, 
			@PathVariable("partyId") Long partyId, 
			@PathVariable("buyOrSell") String buyOrSell,
		    @RequestParam(value="searchText", required=false) String searchText){
		
		log.info("searchText: "+searchText);

		log.info("in getMaterialForNameAndPartyAndSupplyType: "+searchText+":"+supplyTypeId+":"+partyId);

		//Party party = partyService.getPartyObjectById(partyId);

		//SupplyType supplyType = supplyTypeRepository.getOne(supplyTypeId);
		
		List<MaterialDTO> dtoList = materialService.getMaterialByPartyAndSupplyTypeAndNameLike(partyId, supplyTypeId, searchText, buyOrSell);
		
		return new ResponseEntity<List<MaterialDTO>> (dtoList, HttpStatus.OK);
	}

	@GetMapping("/material/material-type/{id}")
	public ResponseEntity<List<MaterialDTO>> getMaterialByMaterialTypeId(@PathVariable("id") Long id){
		log.info("in get material type....id: "+id);
		
		List<MaterialDTO> materialsOut = materialService.getMaterialDTOByMaterialTypeId(id);
		return new ResponseEntity<List<MaterialDTO>>(materialsOut, HttpStatus.OK);
		
	}
	
	
	@GetMapping("/material/party/{id}")
	public ResponseEntity<List<MaterialDTO>> getMaterialsForParty(@PathVariable("id") Long id){
		log.info("in get material for party id: "+id);
		
		Party party = partyService.getPartyObjectById(id);
		
		
		List<MaterialDTO> materialOut = materialService.getMaterialByParty(party);
		
		return new ResponseEntity<List<MaterialDTO>>(materialOut, HttpStatus.OK);
		
	}
	
//	@GetMapping("/material/party/{id}")
//	public ResponseEntity<List<MaterialDTO>> getMaterialByPartyId(@PathVariable("id") Long id){
//		log.info("in get material type....id: "+id);
//		
//		List<MaterialDTO> materialsOut = materialService.getMaterialByPartyId(id);		
//		return new ResponseEntity<List<MaterialDTO>>(materialsOut, HttpStatus.OK);
//		
//	}
//	
	
	@PostMapping("/material")
	@Transactional
	public ResponseEntity<MaterialDTO> saveMaterial(@RequestBody MaterialDTO materialDTO){

		log.info("material stock"+ materialDTO.getStock());
		log.info("material opening stock"+ materialDTO.getOpeningStock());
		
		MaterialDTO materialOut = materialService.save(materialDTO);
		
		return new ResponseEntity<MaterialDTO>(materialOut, HttpStatus.OK);
	}
	
	@DeleteMapping("/material/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteMaterial(@PathVariable("id") Long id){
		
		log.info("in deleteMaterial incoming id: "+id);
		
		TransactionResponseDTO response = new TransactionResponseDTO();
		
		try{
			response = materialService.deleteMaterial(id);
		}catch(Exception e){
			response.setResponseStatus(0);
			response.setResponseString("Material is being used elsewhere cannot delete");
		}
		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	}
	
	
	@GetMapping("/material/report/{supplyTypeId}/{searchText}/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<MaterialReportDTO> getMaterialReport(
												@PathVariable("supplyTypeId") Long supplyTypeId
												, @PathVariable("searchText") String searchText
												, @PathVariable("sortColumn") String sortColumn
												, @PathVariable("sortDirection") String sortDirection
												, @PathVariable("pageNumber") Integer pageNumber
												, @PathVariable("pageSize") Integer pageSize){
		log.info("supplyTypeId: "+supplyTypeId);
		log.info("searchText: "+searchText);
		log.info("searchText.isEmpty(): "+searchText.isEmpty());
		log.info("searchText.length(): "+searchText.length());
		log.info("searchText == null : "+searchText == null ? "NULL" : "NOT NULL");
		log.info("searchText.trim(): "+searchText.trim());
		
		log.info("sortColumn: "+sortColumn);
		log.info("sort direction: "+sortDirection);
		log.info("pageNumber: " +pageNumber);
		log.info("pageSize: "+pageSize);
		
		Long materialCount = 0l;

		List<Material> materials = new ArrayList<Material>(); 
		SupplyType supplyType = supplyTypeRepository.getOne(supplyTypeId);
		Page<Material> materialPage = null;
		//If no search text get all materials
		//If there is a search text, get materials by name + get materials by part number 
		if(searchText.isEmpty() || searchText.equals("null"))	
		{
			log.info("empty search text");
			materialPage = materialReportService.getPageNonJobwork(supplyType, sortColumn, sortDirection, pageNumber, pageSize);
			materials.addAll(materialPage.getContent());
			log.info("materials size: "+materials.size());
			materialCount = materialPage.getTotalElements();
			
		}
		else {
			log.info("non empty search text");
			materialPage = materialReportService.getPageByNameNonJobwork(supplyType, searchText, sortColumn, sortDirection, pageNumber, pageSize);
			materials.addAll(materialPage.getContent());
			materialCount = materialPage.getTotalElements();

			materialPage = materialReportService.getPageByPartNumberNonJobwork(supplyType, searchText, sortColumn, sortDirection, pageNumber, pageSize);
			materials.addAll(materialPage.getContent());
			materialCount += materialPage.getTotalElements() ;
			log.info("materials size: "+materials.size());
		}
		

		
		log.info("returning materials size: "+ materialCount);
		
		MaterialReportDTO materialReportDTO = new MaterialReportDTO();
		
		materialReportDTO.setMaterials(materialMapper.modelToDTOList(materials));
		materialReportDTO.setTotalCount(materialCount);
		
		return new ResponseEntity<MaterialReportDTO>(materialReportDTO, HttpStatus.OK);
		
	}
	
	
	@GetMapping("/material/recent/{supplyTypeId}/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<MaterialRecentDTO> getRecentMaterialReport(
			@PathVariable("supplyTypeId") Long supplyTypeId
			, @PathVariable("sortColumn") String sortColumn
			, @PathVariable("sortDirection") String sortDirection
			, @PathVariable("pageNumber") Integer pageNumber
			, @PathVariable("pageSize") Integer pageSize
			, @RequestParam(value="searchText", required=false) String searchText){
		
		MaterialRecentDTO materialRecentDTO = new MaterialRecentDTO();
		
		List<MaterialDTO> materials = new ArrayList<MaterialDTO>();
		Long materialCount = 0l;
		

		log.info("supplyTypeId: "+supplyTypeId );	
		log.info("searchText: "+searchText );		
		log.info("sortColumn: "+sortColumn);
		log.info("sort direction: "+sortDirection);
		log.info("pageNumber: " +pageNumber);
		log.info("pageSize: "+pageSize);
//		MaterialType matType =  materialTypeService.getById(supplyTypeId);

		SupplyType supplyType = supplyTypeRepository.getOne(supplyTypeId);
		Page<Material> materialPage = null;
		if(searchText.isEmpty() || searchText.equals("null"))	
		{
			log.info("empty search text");
			List<Material> materialModels = new ArrayList<Material>();
			materialPage = materialReportService.getPage(supplyType,sortColumn, sortDirection, pageNumber, pageSize);
			materialModels.addAll(materialPage.getContent());
			log.info("materials size: "+materialModels.size());
			materialCount = materialPage.getTotalElements();
			
			materials = materialMapper.modelToDTOList(materialModels);
		}else {
			log.info("non empty search text");
			List<Material> materialModels = new ArrayList<Material>();
			materialPage = materialReportService.getPageByName(supplyType,searchText, sortColumn, sortDirection, pageNumber, pageSize);
			materialModels.addAll(materialPage.getContent());
			materialCount = materialPage.getTotalElements();
			log.info("1 total count: "+materialCount);

			materialPage = materialReportService.getPageByPartNumber(supplyType, searchText, sortColumn, sortDirection, pageNumber, pageSize);
			materialModels.addAll(materialPage.getContent());
			materialCount += materialPage.getTotalElements();
			log.info("2 total count: "+materialCount);
			materials = materialMapper.modelToDTOList(materialModels);
			log.info("materials size: "+materials.size());
		}
		
		log.info("before return total count: "+materialCount);
		log.info("before return materials size: "+materials.size());
		
		materialRecentDTO.setMaterials(materials);
		materialRecentDTO.setTotalCount(materialCount);
		
		return new ResponseEntity<MaterialRecentDTO>(materialRecentDTO, HttpStatus.OK); 
		
		
		
	}
	
	@GetMapping("/check-part-number")
	public ResponseEntity<TransactionResponseDTO> checkPartNumber(
			@RequestParam(value="partNumber", required=false) String partNumber){
		
		Material material = materialService.getMaterialByPartNumber(partNumber);
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (material != null){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("partNumber "+partNumber+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("partNumber available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}

	@GetMapping("/check-name")
	public ResponseEntity<List<MaterialDTO>> checkMaterialName(
			@RequestParam(value="name", required=false) String name, @RequestParam(value="id", required=false) Long  id ){
		
		log.info("name and id: "+name+","+id);
		
		List<MaterialDTO> materials = materialService.getMaterialsByName(name,id);

		return new ResponseEntity<List<MaterialDTO>> (materials, HttpStatus.OK);

	}

	@GetMapping("/jobwork-materials")
	public ResponseEntity<List<MaterialDTO>> getJobworkMaterials(
			@RequestParam(value="searchText", required=false) String searchText){
		log.info("in getJobworkMaterials() ");
		
		log.info("searchText: "+searchText);

		
		MaterialType jwMaterialType = materialTypeService.getByName(ApplicationConstants.MATERIAL_TYPE_JOBWORK);
		log.info("jwMaterialType 1: " +jwMaterialType);
		List<MaterialDTO> materialsOut = new ArrayList<MaterialDTO>();
		if(jwMaterialType != null)
		{
			log.info("jwMaterialType 2: " +jwMaterialType.getName());
			Long id = jwMaterialType.getId();
			
			materialsOut = materialService.getMaterialDTOByMaterialTypeIdJobwork(id, searchText);
			return new ResponseEntity<List<MaterialDTO>>(materialsOut, HttpStatus.OK);
		}
		else {
			return new ResponseEntity<List<MaterialDTO>>(materialsOut, HttpStatus.OK);
		}
	}
	
	@PostMapping("/min-stock-validate")
	public ResponseEntity<MaterialStockCheckDTOWrapper> validateMinimumStock(@RequestBody MaterialStockCheckDTOWrapper inMaterials)
	{
	
		List<MaterialStockCheckDTO> materialsOut = materialService.validateMinimumStock(inMaterials);
		MaterialStockCheckDTOWrapper materialStockCheckDTOWrapper = new MaterialStockCheckDTOWrapper();
		
		materialStockCheckDTOWrapper.setMaterialStockCheckDTOs(materialsOut);
		return new ResponseEntity<MaterialStockCheckDTOWrapper>(materialStockCheckDTOWrapper, HttpStatus.OK);
	}

	@GetMapping("/exists-in-transactions/{materialId}")
	public ResponseEntity<Integer> materialExistsinTransactions(@PathVariable("materialId") Long materialId){

		return new ResponseEntity<Integer>(materialService.materialExistsinTransactions(materialId), HttpStatus.OK);
	}
}


