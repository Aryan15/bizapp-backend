package com.coreerp.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Path;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import com.amazonaws.services.s3.internal.Mimetypes;
import org.apache.logging.log4j.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.coreerp.dto.CompanyDTO;
import com.coreerp.dto.ImageMetadata;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.jwt.JwtTokenUtil;
import com.coreerp.model.Company;
import com.coreerp.service.CompanyService;
import com.coreerp.service.StorageService;

@RestController
@RequestMapping("/api")
public class FileUploadController {
	
	final static Logger log = LogManager.getLogger(FileUploadController.class);
	
	
	@Autowired
	CompanyService companyService;
	
    @Value("${jwt.header}")
    private String tokenHeader;
    
	   private final StorageService storageService;
	   
//	   @Autowired
//	    private ServletContext servletContext;
	   
	    @Autowired
	    JwtTokenUtil jwtTokenUtil;

	    @Autowired
	    public FileUploadController(@Qualifier("AwsS3StorageService") StorageService storageService) {
	        this.storageService = storageService;
	    }

	    @GetMapping("/files")
	    public List<String> listUploadedFiles() throws IOException {

	    	List<String> outFileList = storageService.loadAll()
	        			.map( path -> path.getFileName().toString())
	        			.collect(Collectors.toList());

	        return outFileList;
	    }
	    
	    
	    @GetMapping("/files/company-base64/{filename:.+}")
	    public ResponseEntity<TransactionResponseDTO> serveAsBase64(@PathVariable String filename, HttpServletRequest request) {

	    	String authToken = request.getHeader(this.tokenHeader);
	    	String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);
	    	String encodedString = "";
	    	
	    	TransactionResponseDTO retValue = new TransactionResponseDTO();
	    	log.info("serveAsBase64 filename: "+filename);
	    	if(filename != null && filename.length() > 0) {
				encodedString = storageService.loadAsBase64(filename, tenantId);
		    	
		    	
		    	retValue.setResponseString(encodedString);
	    	}
	    	return new ResponseEntity<TransactionResponseDTO>(retValue, HttpStatus.OK);
	    }

	    @GetMapping("/files/company/{filename:.+}")
	    @ResponseBody
	    public ResponseEntity<Resource> serveFile(@PathVariable String filename, HttpServletRequest request) {

	    	String authToken = request.getHeader(this.tokenHeader);
	    	String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);

	        Resource file = storageService.loadAsResource(filename, tenantId);

	        log.info("file path"+file);
	        
	        String mimeType = Mimetypes.getInstance().getMimetype(filename);
	        
	        log.info("file mimeType: "+mimeType);
	        
	        if (file != null)
	        {
	        return ResponseEntity.ok()
	        		.contentType(mimeType.equals("image/png")? MediaType.IMAGE_PNG : MediaType.IMAGE_JPEG)
	        		.header(HttpHeaders.CONTENT_DISPOSITION,
	                "filename=\"" + file.getFilename() + "\"").body(file);
	        }
	        else
	        {
	        	return null;
	        }
	    }
	    
	    @GetMapping("/files/company/image-metadata/{filename:.+}/{companyId}")
	    @ResponseBody
	    public ResponseEntity<ImageMetadata> serveImageMetadata(@PathVariable String filename, @PathVariable Long companyId,  HttpServletRequest request) {

	    	log.info("serveImageMetadata filename: "+filename);
	    	log.info("serveImageMetadata companyId: "+companyId);
	    	String authToken = request.getHeader(this.tokenHeader);
	    	String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);
			Resource file=null;
			if(!filename.equalsIgnoreCase("null")) {
				file = storageService.loadAsResource(filename, tenantId);
			}

	        ImageMetadata returnImageMetadata = new ImageMetadata();
	        
	        if(file == null){
	        	return new ResponseEntity<ImageMetadata>(returnImageMetadata, HttpStatus.OK);
	        }
	        
	        try {
				BufferedImage image = ImageIO.read(file.getInputStream());
				returnImageMetadata.setWidth(image.getWidth());
				returnImageMetadata.setHeight(image.getHeight());
				returnImageMetadata.setName(filename);
				
			} catch (IOException e) {
				
				e.printStackTrace();
			}
	        
	        log.info("Before Return:" +returnImageMetadata);
	        return new ResponseEntity<ImageMetadata>(returnImageMetadata, HttpStatus.OK);
	        
	    }
	    

	    @PostMapping("/files/company")
	    public ResponseEntity<TransactionResponseDTO> handleFileUpload(@RequestParam("image") MultipartFile file
	    		, HttpServletRequest request) {
	    	
	    	log.info("incoming file: "+file.getName());
	    	
	    	String authToken = request.getHeader(this.tokenHeader);
	    	String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);
	    	log.info("any roken here? :"+tenantId);
	    	
	    	TransactionResponseDTO response = new TransactionResponseDTO();
	    	try{
	        storageService.store(file, tenantId);
	        log.info("saved successfully: "+file.getOriginalFilename());
	        
	        //Update logo path in the company
	        Company comp = companyService.getModelById(1l);

	        comp.setCompanyLogoPath(file.getOriginalFilename());

	        companyService.saveModel(comp);	        
	    	}catch(Exception e){
	    		response.setResponseString("Error uploading file");
	    		throw e;
	    	}
	    	
	    	response.setResponseString("Success");

	        return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	    }

	    @PostMapping("/files/company-cert-image")
	    public ResponseEntity<TransactionResponseDTO> certificateImageUpload(@RequestParam("image") MultipartFile file
	    		, HttpServletRequest request) {
	    	
	    	log.info("incoming file: "+file.getName());
	    	
	    	String authToken = request.getHeader(this.tokenHeader);
	    	String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);
	    	log.info("any roken here? :"+tenantId);
	    	
	    	TransactionResponseDTO response = new TransactionResponseDTO();
	    	try{
	        storageService.store(file, tenantId);
	        log.info("saved successfully: "+file.getOriginalFilename());
	        
	        //Update logo path in the company
	        Company comp = companyService.getModelById(1l);
	        
	        comp.setCeritificateImagePath(file.getOriginalFilename());
	        
	        companyService.saveModel(comp);	        
	    	}catch(Exception e){
	    		response.setResponseString("Error uploading file");
	    		throw e;
	    	}
	    	
	    	response.setResponseString("Success");

	        return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	    }



	@PostMapping("/files/signature-image")
	public ResponseEntity<TransactionResponseDTO> signatureImageUpload(@RequestParam("image") MultipartFile file
			, HttpServletRequest request) {

		log.info("incoming file: "+file.getName());

		String authToken = request.getHeader(this.tokenHeader);
		String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);
		log.info("any roken here? :"+tenantId);

		TransactionResponseDTO response = new TransactionResponseDTO();
		try{
			storageService.store(file, tenantId);
			log.info("saved successfully: "+file.getOriginalFilename());

			//Update logo path in the company
			Company comp = companyService.getModelById(1l);

			comp.setSignatureImagePath(file.getOriginalFilename());

			companyService.saveModel(comp);
		}catch(Exception e){
			response.setResponseString("Error uploading file");
			throw e;
		}

		response.setResponseString("Success");

		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	}

	    
	    @DeleteMapping("/files-delete/company/{filename:.+}/{companyId}")
	    public ResponseEntity<TransactionResponseDTO> deleteFile(
	    		@PathVariable("companyId") Long companyId,
	    		@PathVariable("filename") String filename
	    		, HttpServletRequest request
	    		){
	    	
	    	TransactionResponseDTO response = new TransactionResponseDTO();
	    	Boolean isDeleted=false;
	    	
	    	String authToken = request.getHeader(this.tokenHeader);
	    	String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);
	    	log.info("tenant :"+tenantId);

			String filePath = "company/" + tenantId + "/" + filename;
	    	log.info("filePath: "+filePath.toString());
	    	isDeleted=storageService.deleteFile(filePath);

	    	if(isDeleted)
	    	{
	    		//Update logo path in the company
		        Company comp = companyService.getModelById(1l);
		        
		        comp.setCompanyLogoPath(null);
		        
		        companyService.saveModel(comp);	        
		    	
	    	}
	    	if(isDeleted)
	    	{
	    		response.setResponseStatus(1);
	    		response.setResponseString("Deleted Successfully");
	    	}
	    	else
	    	{
	    		response.setResponseStatus(0);
	    		response.setResponseString("error");
	    	}
	    	return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	    	
	    	
	    }
	    
	    @DeleteMapping("/files-delete/company-cert/{filename:.+}/{companyId}")
	    public ResponseEntity<TransactionResponseDTO> deleteCertImage(
	    		@PathVariable("companyId") Long companyId,
	    		@PathVariable("filename") String filename
	    		, HttpServletRequest request
	    		){
	    	
	    	TransactionResponseDTO response = new TransactionResponseDTO();
	    	Boolean isDeleted=false;
	    	
	    	String authToken = request.getHeader(this.tokenHeader);
	    	String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);
	    	log.info("tenant :"+tenantId);
	    	
	    	String filePath = "company/" + tenantId + "/" + filename;
	    	log.info("filePath: "+filePath);
	    	isDeleted=storageService.deleteFile(filePath);
	    	//String thumbnailFilePath=storageService.loadThumbnail(filename, agentId).toString();
//	    	if(isDeleted)
//	    	{
//	    		isDeleted=storageService.deleteFile(thumbnailFilePath);
//	    	}
	    	if(isDeleted)
	    	{
	    		//Update logo path in the company
		        Company comp = companyService.getModelById(1l);
		        
		        comp.setCeritificateImagePath(null);
		        
		        companyService.saveModel(comp);	        
		    	
	    	}
	    	if(isDeleted)
	    	{
	    		response.setResponseStatus(1);
	    		response.setResponseString("Deleted Successfully");
	    	}
	    	else
	    	{
	    		response.setResponseStatus(0);
	    		response.setResponseString("error");
	    	}
	    	return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	    	
	    	
	    }



	@DeleteMapping("/files-delete/signature-image/{filename:.+}/{companyId}")
	public ResponseEntity<TransactionResponseDTO> deleteSignatureImage(
			@PathVariable("companyId") Long companyId,
			@PathVariable("filename") String filename
			, HttpServletRequest request
	){

		TransactionResponseDTO response = new TransactionResponseDTO();
		Boolean isDeleted=false;

		String authToken = request.getHeader(this.tokenHeader);
		String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);
		log.info("tenant :"+tenantId);

		String filePath = "company/" + tenantId + "/" + filename;
		log.info("filePath: "+filePath.toString());
		isDeleted=storageService.deleteFile(filePath);
		//String thumbnailFilePath=storageService.loadThumbnail(filename, agentId).toString();
//	    	if(isDeleted)
//	    	{
//	    		isDeleted=storageService.deleteFile(thumbnailFilePath);
//	    	}
		if(isDeleted)
		{
			//Update logo path in the company
			Company comp = companyService.getModelById(1l);

			comp.setSignatureImagePath(null);

			companyService.saveModel(comp);

		}
		if(isDeleted)
		{
			response.setResponseStatus(1);
			response.setResponseString("Deleted Successfully");
		}
		else
		{
			response.setResponseStatus(0);
			response.setResponseString("error");
		}
		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);


	}
	    
}
