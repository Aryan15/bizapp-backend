package com.coreerp.controller;

import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.model.TransactionType;
import com.coreerp.tallyexternal.model.VoucherEnvelopeEx;
import com.coreerp.tallyexternal.service.TallyVoucherServiceEx;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api")
public class TallyExternalController {

    @Autowired
    TransactionTypeRepository transactionTypeRepository;
   @Autowired
    TallyVoucherServiceEx tallyVoucherServiceEx;

    final static Logger log = LogManager.getLogger(TallyExternalController.class);
    @PostMapping(value="/tally-voucher-external-xml/{invoiceTypeId}")
    public ResponseEntity<String> getVoucherXml(@PathVariable("invoiceTypeId") Long invoiceTypeId,
                                                           @RequestBody TransactionReportRequestDTO reportRequestDTO){
        log.info("getVoucherXml "+reportRequestDTO+"----------------"+invoiceTypeId);
       TransactionType invoiceType = transactionTypeRepository.getOne(invoiceTypeId);
        String envelope = tallyVoucherServiceEx.getVoucherEnvelope(invoiceType,reportRequestDTO.getPartyId(),reportRequestDTO.getTransactionFromDate(), reportRequestDTO.getTransactionToDate());
        log.info("returning envelop: "+ envelope);
        return new ResponseEntity<>(envelope, HttpStatus.OK);
    }


}
