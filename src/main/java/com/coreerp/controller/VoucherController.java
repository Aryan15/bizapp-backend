package com.coreerp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.ApplicationConstants;
import com.coreerp.dto.PayableReceivableReportDTO;
import com.coreerp.dto.RecentVoucherDTO;
import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.dto.VoucherHeaderDTO;
import com.coreerp.dto.VoucherReportDTO;
import com.coreerp.dto.VoucherWrapperDTO;
import com.coreerp.mapper.VoucherHeaderMapper;
import com.coreerp.model.PartyType;
import com.coreerp.model.TransactionType;
import com.coreerp.model.VoucherHeader;
import com.coreerp.reports.model.PayableReceivableReport;
import com.coreerp.reports.model.VoucherReportModel;
import com.coreerp.reports.service.RecentVoucherService;
import com.coreerp.reports.service.VoucherPageableService;
import com.coreerp.service.TransactionTypeService;
import com.coreerp.service.VoucherService;

@RestController
@RequestMapping("/api")
public class VoucherController {
	

	final static Logger log=LogManager.getLogger(VoucherController.class);
 

	@Autowired
	TransactionTypeService transactionTypeService;
	
	
	@Autowired
	VoucherService voucherService;
	
	@Autowired
	VoucherHeaderMapper VoucherHeaderMapper;
	
	@Autowired
	RecentVoucherService recentVoucherService;
	
	@Autowired
	VoucherPageableService voucherPageableService;
	
	@GetMapping("/voucher/{id}")
	public VoucherHeaderDTO getVoucher(@PathVariable("id") String VoucherId){
		log.info("in getVoucher: "+VoucherId);

		
		return voucherService.getVouchersByHeaderId(VoucherId);
	}
	
	@GetMapping("/voucher")
	public ResponseEntity<List<VoucherHeaderDTO>> getAllVouchers(){
		log.info("In getAllVouchers");
		List<VoucherHeaderDTO> VoucherHeaders =  voucherService.findAllVouchers();
		log.info("length: "+VoucherHeaders.size());
		log.info("item length: "+ (VoucherHeaders.size() > 0 ? VoucherHeaders.get(0).getVoucherItems().size() : 0));
		return new ResponseEntity<List<VoucherHeaderDTO>>(VoucherHeaders, HttpStatus.OK);
		
	}
	
	
	@GetMapping("/voucher/voucher-number/{voucherNumber}")
	public ResponseEntity<List<VoucherHeaderDTO>> getVoucherForVoucherNumber(@PathVariable("VoucherNumber") String VoucherNumber){
		log.info("Voucher number: "+ VoucherNumber);
		return new ResponseEntity<List<VoucherHeaderDTO>>(voucherService.findVoucherByVoucherNumber(VoucherNumber), HttpStatus.OK);
	}
	
	@PostMapping("/voucher")
	@Transactional
	public ResponseEntity<VoucherHeaderDTO> save(@RequestBody VoucherWrapperDTO VoucherWrapperDTOIn){
		

		VoucherHeaderDTO VoucherDTOOut = voucherService.saveVoucher(VoucherWrapperDTOIn);
		log.info("Saved successfully");
		return new ResponseEntity<VoucherHeaderDTO>(VoucherDTOOut, HttpStatus.OK);
	}

	@DeleteMapping("/voucher/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteById(@PathVariable("id") String id){
		TransactionResponseDTO deleteStatus = voucherService.delete(id);
		log.info("returning delete message: "+deleteStatus);
		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
	}

//	//to delete expense
//	@DeleteMapping("/expense/{ids}")
//	public ResponseEntity<TransactionResponseDTO> delete(@PathVariable List<Long> ids){
//		
//		log.info("delete: "+ids);
//		TransactionResponseDTO response = new TransactionResponseDTO();
//		
//		response.setResponseStatus(1);
//		response.setResponseString("Deleted Successfully");
//		
//		try{
//			ids.forEach(id -> {
//				try{
//				voucherService.deleteExpenseById(id);
//				} catch(MySQLIntegrityConstraintViolationException e){
//					log.info("Error deleteing 1.."+e.getMessage());
//					response.setResponseStatus(0);
//					response.setResponseString("Cannot delete. bank already used");
//				}
//			});
//			
//			log.info("DELETED");
//			
//		}
//		
//		catch(Exception e){
//			log.info("Error deleteing.."+e.getMessage());
//			response.setResponseStatus(0);
//			response.setResponseString(e.getMessage());
//		}
//		
//		log.info("returning: "+response);
//		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
//	}
	
	@GetMapping("/voucher/recent/{voucherTypeId}/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<RecentVoucherDTO> getRecentVoucher(
												  @PathVariable("voucherTypeId") Long VoucherTypeId
												, @PathVariable("sortColumn") String sortColumn
												, @PathVariable("sortDirection") String sortDirection
												, @PathVariable("pageNumber") Integer pageNumber
												, @PathVariable("pageSize") Integer pageSize
												, @RequestParam(value = "searchText", required=false) String searchText												){
		log.info("in recent");
		log.info("VoucherTypeId: "+VoucherTypeId);
		log.info("searchText: "+searchText);
		log.info("searchText.isEmpty(): "+searchText.isEmpty());
		log.info("searchText.length(): "+searchText.length());
		log.info("searchText == null : "+searchText == null ? "NULL" : "NOT NULL");
		log.info("searchText.trim(): "+searchText.trim());
		
		log.info("sortColumn: "+sortColumn);
		log.info("sort direction: "+sortDirection);
		log.info("pageNumber: " +pageNumber);
		log.info("pageSize: "+pageSize);
		
		Long totalCount = 0l;
		List<VoucherHeader> VoucherHeaders = new ArrayList<VoucherHeader>();
		
		RecentVoucherDTO recentVoucherDTO = new RecentVoucherDTO();
		
		TransactionType VoucherType = transactionTypeService.getModelById(VoucherTypeId);
		
		if(searchText.isEmpty() || searchText.equals("null"))	
		{
			log.info("empty search text");
			Page<VoucherHeader> recentVouchers = recentVoucherService.getPage(VoucherType, sortColumn, sortDirection, pageNumber, pageSize); 
			VoucherHeaders.addAll(recentVouchers.getContent());
			log.info("recentVoucher size: "+VoucherHeaders.size());
			totalCount = recentVouchers.getTotalElements();
			
		}else {
			
			Page<VoucherHeader> recentVouchers = recentVoucherService.getPageByVoucherNumber(VoucherType, searchText, sortColumn, sortDirection, pageNumber, pageSize); 
			VoucherHeaders.addAll(recentVouchers.getContent());
			
			totalCount= recentVouchers.getTotalElements();
			
//			searchText = searchText.replace("-", "/");
//			
//			recentVouchers = recentVoucherService.getPageByVoucherNumber(VoucherType, searchText, sortColumn, sortDirection, pageNumber, pageSize);
//			
//			VoucherHeaders.addAll(recentVouchers.getContent());
//			totalCount+= recentVouchers.getTotalElements();
			
			
			recentVouchers = recentVoucherService.getPageByPaidTo(VoucherType, searchText, sortColumn, sortDirection, pageNumber, pageSize);
			
			VoucherHeaders.addAll(recentVouchers.getContent());
			totalCount+= recentVouchers.getTotalElements();
			
			
			try {
				Date searchDate = new SimpleDateFormat("dd/MM/yyyy").parse(searchText);
				
				log.info("parsed date: "+searchDate);
				
				recentVouchers = recentVoucherService.getPageByVoucherDate(VoucherType, searchDate, sortColumn, sortDirection, pageNumber, pageSize);
				
				VoucherHeaders.addAll(recentVouchers.getContent());
				totalCount+= recentVouchers.getTotalElements();
				log.info("totalCount after date: "+totalCount);
				
			} catch (ParseException e) {
				log.info("incorrect date format: "+searchText+" expected dd/MM/yyyy");
			}
			
			//searchDate.s
			
			log.info("non empty search text");
		}
		
		recentVoucherDTO.setVoucherHeaders(VoucherHeaderMapper.modelToDTOList(VoucherHeaders));
		recentVoucherDTO.setTotalCount(totalCount);
		
		
		return new ResponseEntity<RecentVoucherDTO>(recentVoucherDTO, HttpStatus.OK);
		
	}

	
//	@PostMapping("/expense")
//	@Transactional
//	public ResponseEntity<List<ExpenseHeader>> saveExpense(@RequestBody List<ExpenseHeader> expenseHeaderList){
//		
//		log.info("in expensSave: "+expenseHeaderList);
//		List<ExpenseHeader> outExpenses = new ArrayList<ExpenseHeader>();
//		
//		for(ExpenseHeader expenseHeader : expenseHeaderList){
//			if(!expenseHeader.getName().isEmpty()){
//				ExpenseHeader expense = voucherService.saveExpense(expenseHeader);
//				outExpenses.add(expenseHeader);	
//				
//			}
//
//		}
//		
//		return new ResponseEntity<List<ExpenseHeader>>(outExpenses, HttpStatus.OK);
//		/*
//		 * ExpenseHeader expenseHeaderOut = voucherService.saveExpense(ExpenseHeaderin);
//		 * log.info("Saved successfully"); return new
//		 * ResponseEntity<ExpenseHeader>(expenseHeaderOut, HttpStatus.OK);
//		 */
//	}
	
	@GetMapping("/check-voucherNumber/{invTypeId}")
	public ResponseEntity<TransactionResponseDTO> checkVoucherNumber(@PathVariable("invTypeId") Long invTypeId
			, @RequestParam(value = "voucherNumber", required=false) String voucherNumber){
		
		TransactionType invType = transactionTypeService.getModelById(invTypeId);
		
		Long count = voucherService.countByVoucherNumberAndTransactionType(voucherNumber, invType);
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (count > 0){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("voucherNumber  "+voucherNumber+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("voucherNumber available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}
	
	
	
	
	@PostMapping("/voucher/report/{transactionTypeId}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
	public ResponseEntity<VoucherReportDTO> getInvoiceReport(
			@PathVariable("transactionTypeId") Long transactionTypeId,
			@PathVariable("pageNumber") Integer pageNumber,
			@PathVariable("pageSize") Integer pageSize,
			@PathVariable("sortDir") String sortDir,
			@PathVariable("sortColumn") String sortColumn,
			@RequestBody TransactionReportRequestDTO transactioReportRequestDTO){
		log.info("incoming request: "+transactioReportRequestDTO);
		log.info("incoming request: "+transactioReportRequestDTO.getTransactionFromDate());
		log.info("incoming request: "+transactioReportRequestDTO.getTransactionToDate());
		log.info("incoming request: "+transactioReportRequestDTO.getMaterialId());
		log.info("incoming request: "+transactioReportRequestDTO.getPartyId());
		log.info("incoming request: "+transactioReportRequestDTO.getFinancialYearId());
		log.info("incoming request: "+transactioReportRequestDTO.getPartyTypeIds());
		
		List<Long> partyTypeIds = null;
		
		
		log.info("party type ids: "+partyTypeIds);
		
		log.info("sortColumn: "+sortColumn);
		if(sortColumn.equals("partyName"))
			sortColumn = "party.name";
		
		//TransactionType transactionType = transactionTypeService.getModelById(transactionTypeId);
		
		Page<VoucherReportModel> voucherReportPage = voucherPageableService.getPagedVoucherReport(transactionTypeId, transactioReportRequestDTO.getFinancialYearId(), transactioReportRequestDTO.getTransactionFromDate(), transactioReportRequestDTO.getTransactionToDate(), transactioReportRequestDTO.getMaterialId(), pageNumber, pageSize, sortDir, sortColumn);
		
		List<VoucherReportModel> voucherReport = voucherReportPage.getContent();
		
		Long totalCount = voucherReportPage.getTotalElements();
		
		
		VoucherReportDTO voucherReportDTO = new VoucherReportDTO();
		
		
		voucherReportDTO.setVoucherReportItems(voucherReport);
		voucherReportDTO.setTotalCount(totalCount);
		
		return new ResponseEntity<VoucherReportDTO>(voucherReportDTO, HttpStatus.OK);
	}
	
	
}
