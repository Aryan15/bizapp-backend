package com.coreerp.controller;


import com.coreerp.dto.InvoiceDTO;
import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.model.*;
import com.coreerp.multitenancy.TenantContext;
import com.coreerp.service.*;
import com.coreerp.serviceimpl.JasperReportServiceImpl;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class JasperReportController {

    private static Logger logger = LogManager.getLogger(JasperReportController.class.getName());

    @Value("${spring.datasource.username}")
    private String dbUsername;

    @Value("${spring.datasource.password}")
    private String dbPassword;

    @Value("${report.path}")
    private String reportPath;

    @Autowired
    PrintCopiesService printCopiesService;

    @Autowired
    CompanyService companyService;

    @Autowired
    JasperReportServiceImpl jasperReportService;

    @Autowired
    GlobalSettingService globalSettingService;


    @GetMapping("/jasper-print/{signature}/{allowShipingAddress}")
    public ResponseEntity<InputStreamResource> getReport(
            @PathVariable("signature") Long signature
            ,@PathVariable("allowShipingAddress") Long allowShipingAddress
            ,@RequestParam(value = "id", required=false) String id
            , @RequestParam(value = "reportName", required=false) String reportName
            , @RequestParam(value = "copyText", required=false) String copyText
            , @RequestParam(value = "amountInWords", required = false) String amountInWords
            , @RequestParam(value ="printHeaderText", required = false)String printHeaderText
            , HttpServletRequest request) throws JRException, SQLException {

        logger.info("In getReport: "+id);
        logger.info("reportName: "+reportName);
        logger.info("copyText: "+copyText);
        logger.info("amountInWords: "+amountInWords);
        logger.info("printHeaderText: "+printHeaderText);
        logger.info("allowShipingAddress: "+allowShipingAddress);

        Integer allowPartyCode=0;
        GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);
        if(globalSetting.getAllowPartyCode()!=null){
            allowPartyCode=globalSetting.getAllowPartyCode();
        }

        ByteArrayInputStream in = jasperReportService.generateReport(reportName, id, copyText, amountInWords,printHeaderText,signature,allowPartyCode,allowShipingAddress,request);



        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename="+reportName+".pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));



    }


    @PostMapping("/jasper-report/{tTypeId}/{counterTTypeId}/{jasperReportName}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
    public ResponseEntity<InputStreamResource> getReport(
            @PathVariable("tTypeId") Long tTypeId,
            @PathVariable("counterTTypeId") Long counterTTypeId,
            @PathVariable("jasperReportName") String jasperReportName,
            @PathVariable("pageNumber") Integer pageNumber,
            @PathVariable("pageSize") Integer pageSize,
            @PathVariable("sortDir") String sortDir,
            @PathVariable("sortColumn") String sortColumn,
            @RequestBody TransactionReportRequestDTO reportRequestDTO,
            HttpServletRequest request) throws JRException, SQLException {

        logger.info("In getReportDC: ");

        ByteArrayInputStream in = jasperReportService.getReport(tTypeId,
                counterTTypeId,
                jasperReportName,
                pageNumber,
                pageSize,
                sortDir,
                sortColumn,
                reportRequestDTO,
                "PDF");


        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=prreport.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));



    }


    @PostMapping("/jasper-report-excel/{tTypeId}/{counterTTypeId}/{jasperReportName}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
    public ResponseEntity<InputStreamResource> getReportExcel(
            @PathVariable("tTypeId") Long tTypeId,
            @PathVariable("counterTTypeId") Long counterTTypeId,
            @PathVariable("jasperReportName") String jasperReportName,
            @PathVariable("pageNumber") Integer pageNumber,
            @PathVariable("pageSize") Integer pageSize,
            @PathVariable("sortDir") String sortDir,
            @PathVariable("sortColumn") String sortColumn,
            @RequestBody TransactionReportRequestDTO reportRequestDTO,
            HttpServletRequest request) throws JRException, SQLException {

        logger.info("In getReportDC: ");

        ByteArrayInputStream in = jasperReportService.getReport(tTypeId,
                counterTTypeId,
                jasperReportName,
                pageNumber,
                pageSize,
                sortDir,
                sortColumn,
                reportRequestDTO,
                "EXCEL");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=prreport.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));



    }

    @GetMapping("/jasper-report-excel-form")
    public ResponseEntity<InputStreamResource> getExcelReport(
            @RequestParam(value = "id", required=false) String id
            , @RequestParam(value = "reportName", required=false) String reportName
            , @RequestParam(value = "copyText", required=false) String copyText
            , @RequestParam(value = "amountInWords", required = false) String amountInWords
            , @RequestParam(value = "printHeaderText", required = false) String printHeaderText
            , HttpServletRequest request) throws JRException, SQLException {

        logger.info("In getReport: "+id);



        ByteArrayInputStream in = jasperReportService.generateExcelReport(reportName, id, copyText, amountInWords,printHeaderText,request);



        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename="+reportName+".xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));



    }


    @GetMapping("/jasper-report-doc-form")
    public ResponseEntity<InputStreamResource> getWordReport(
            @RequestParam(value = "id", required=false) String id
            , @RequestParam(value = "reportName", required=false) String reportName
            , @RequestParam(value = "copyText", required=false) String copyText
            , @RequestParam(value = "amountInWords", required = false) String amountInWords
            , @RequestParam(value = "printHeaderText", required = false) String printHeaderText
            , HttpServletRequest request) throws JRException, SQLException {

        logger.info("In getReport: "+id);



        ByteArrayInputStream in = jasperReportService.generateWordReport(reportName, id, copyText, amountInWords,printHeaderText,request);



        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename="+reportName+".docx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));



    }














//    @GetMapping("/dc-jasper-sub-report")
//    public ResponseEntity<InputStreamResource> getSubreport(
//            HttpServletRequest request) throws JRException, SQLException, IOException {
//
//        logger.info("In getReportDC: ");
//
//
//        String reportName = "sub-report.jrxml";
//        String reportJName = "sub-report.jasper";
//        File xmlFile = new File(reportPath+reportName);
//        File jasperFile = new File(reportPath+reportJName);
//
////        JasperReport jasperReport
////                = JasperCompileManager.compileReport(xmlFile.getAbsolutePath());
//
//        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(jasperFile.getAbsolutePath());
//
//
//        Map<String, Object> parameters = new HashMap<>();
//        //parameters.put("v_limit", vLimit);
//        //parameters.put("v_offset", vLimit * vPage);
//        logger.info("Calling Jasper");
//        String url = "jdbc:mysql://localhost:3306/"+ TenantContext.getCurrentTenant()+"?useSSL=false";
//        Connection c = DriverManager.getConnection(url,dbUsername,dbPassword);
//        JasperPrint jasperPrint = JasperFillManager.fillReport(
//                jasperReport, parameters, c);
//
//
//
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//
//
//        JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
//
//
//        ByteArrayInputStream in = new ByteArrayInputStream(outputStream.toByteArray());
//
//        logger.info("Got report: "+in.available());
//
//        if (c != null) {
//            try {
//                //c.rollback();
//                c.close();
//                logger.info("connection closed");
//            } catch (SQLException e) {
//                //nothing to do
//                logger.info("Sql exception "+e);
//            }
//        }
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Content-Disposition", "attachment; filename=prreport.pdf");
//
//        return ResponseEntity
//                .ok()
//                .headers(headers)
//                .body(new InputStreamResource(in));
//
//
//
//    }
}
