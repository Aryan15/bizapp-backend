package com.coreerp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dto.AreaDTO;
import com.coreerp.dto.CityDTO;
import com.coreerp.dto.StateDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.service.AreaService;
import com.coreerp.service.CityService;
import com.coreerp.service.StateService;

@RestController
@RequestMapping("/api")
public class AreaController {

	final static Logger log = LogManager.getLogger(AreaController.class);
      
	@Autowired
	AreaService areaService;
	
	
	@Autowired
	StateService stateService;
	
	@Autowired
	CityService cityService;
	
	
	@PostMapping("/states")	
	@Transactional
	public ResponseEntity<List<StateDTO>> statesSave(@RequestBody List<StateDTO> stateDTOList){
		
		log.info("Incoming states");
//		stateDTOList.forEach(state -> {
//			log.info("sate: "+state);
//		});
		
		List<StateDTO> statesOut = new ArrayList<StateDTO>();
		
		stateDTOList.forEach(state -> {
			statesOut.add(stateService.saveState(state));
		});
		
		return new ResponseEntity<List<StateDTO>>(statesOut, HttpStatus.OK);
		
	}
	
	@GetMapping("/state")
	public ResponseEntity<List<StateDTO>> statesGet(){
		
		List<StateDTO> statesOut = stateService.getAllStates();
		
		return new ResponseEntity<List<StateDTO>>(statesOut, HttpStatus.OK);
		
	}
	
	@DeleteMapping("/state/{ids}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteState(@PathVariable List<Long> ids){
		
		log.info("deleteState: "+ids);
		TransactionResponseDTO response = new TransactionResponseDTO();
		
		response.setResponseStatus(1);
		response.setResponseString("Deleted Successfully");
		
		try{
			ids.forEach(id -> {
				log.info("deleting: "+id);
				stateService.deleteStateById(id);	
			});
			
		}catch(Exception e){
			log.info("Error deleteing.."+e.getMessage());
			response.setResponseStatus(1);
			response.setResponseString(e.getMessage());
		}
		log.info("returning.."+response);
		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	}
	

	@GetMapping("/state/{countryId}")
	public ResponseEntity<List<StateDTO>> statesGetForCountry(@PathVariable Long countryId){
		
		List<StateDTO> statesOut = stateService.getAllStatesForCountry(countryId);
		
		return new ResponseEntity<List<StateDTO>>(statesOut, HttpStatus.OK);
		
	}

	@PostMapping("/cities")	
	@Transactional
	public ResponseEntity<List<CityDTO>> citiesSave(@RequestBody List<CityDTO> cityDTOList){
		
		List<CityDTO> citiesOut = new ArrayList<CityDTO>();
		
		cityDTOList.forEach(city -> {
			citiesOut.add(cityService.saveCity(city));
		});
		
		return new ResponseEntity<List<CityDTO>>(citiesOut, HttpStatus.OK);
		
	}
	
	

	@DeleteMapping("/city/{ids}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteCity(@PathVariable List<Long> ids){
		
		log.info("deleteCity: "+ids);
		TransactionResponseDTO response = new TransactionResponseDTO();
		
		response.setResponseStatus(1);
		response.setResponseString("Deleted Successfully");
		
		try{
			ids.forEach(id -> {
				log.info("deleting: "+id);
				cityService.deleteCityById(id);	
			});
			
		}catch(Exception e){
			log.info("Error deleteing.."+e.getMessage());
			response.setResponseStatus(1);
			response.setResponseString(e.getMessage());
		}
		log.info("returning.."+response);
		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	}
	
	@GetMapping("/city")
	public ResponseEntity<List<CityDTO>> citiesGet(){
		
		List<CityDTO> citiesOut = cityService.getAllCities();
		
//		log.info("returning citiesOut: "+citiesOut);
		
		return new ResponseEntity<List<CityDTO>>(citiesOut, HttpStatus.OK);
		
	}
	@GetMapping("/city/{stateId}")
	public ResponseEntity<List<CityDTO>> citiesGetForState(@PathVariable Long stateId){
		
		List<CityDTO> citiesOut = cityService.getAllCitiesForState(stateId);
		
		return new ResponseEntity<List<CityDTO>>(citiesOut, HttpStatus.OK);
		
	}	
	
//	@PostMapping("/areas")
//	public ResponseEntity<List<AreaDTO>> areaSave(@RequestBody List<AreaDTO> areaDTO){
//		
//		List<AreaDTO> areaDTOList = new ArrayList<AreaDTO>();
//		
//		areaDTO.forEach(area -> {
//			AreaDTO areaSave=areaService.saveArea(area);
//			areaDTOList.add(areaSave);
//			
//		});
//		//log.info("In areaSave: name="+areaDTO.getName());
//	   
//	   log.info("Area has been saved");
//	   
//	   return new ResponseEntity<List<AreaDTO>>(areaDTOList,HttpStatus.OK);
//	}
	
	@PostMapping("/areas")
	@Transactional
	public ResponseEntity<List<AreaDTO>> areasSave(@RequestBody List<AreaDTO> areaDTOList){
		
		log.info("in areasSave: "+areaDTOList);
		List<AreaDTO> outAreas=new ArrayList<AreaDTO>();
		
		for(AreaDTO areaDTO :areaDTOList){
			if(!areaDTO.getName().isEmpty()){
				AreaDTO area=areaService.saveArea(areaDTO);
				outAreas.add(area);
			}
		}
		
		return new ResponseEntity<List<AreaDTO>>(outAreas,HttpStatus.OK);
	}
	
	
	@DeleteMapping("/area/{ids}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteArea(@PathVariable List<Long> ids){
		
		log.info("deleteCity: "+ids);
		TransactionResponseDTO response = new TransactionResponseDTO();
		
		response.setResponseStatus(1);
		response.setResponseString("Deleted Successfully");
		
		try{
			ids.forEach(id -> {
				log.info("deleting: "+id);
				areaService.deleteAreaById(id);	
			});
			
		}catch(Exception e){
			log.info("Error deleteing.."+e.getMessage());
			response.setResponseStatus(1);
			response.setResponseString(e.getMessage());
		}
		log.info("returning.."+response);
		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	}

	@GetMapping("/area")
	public ResponseEntity<List<AreaDTO>> getAllAreas(){
		
//		log.info("In getAllAreas");
		List<AreaDTO> areaDTOList=areaService.getAllAreas();
//		log.info("areaDTOList.size(): "+areaDTOList.size());
		return new ResponseEntity<List<AreaDTO>>(areaDTOList,HttpStatus.OK);
	}


}
