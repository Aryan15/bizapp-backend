package com.coreerp.controller;

import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.UnitOfMeasurement;
import com.coreerp.service.UnitOfMeasurementService;

@RestController
@RequestMapping("/api")
public class UnitOfMeasurementController {
	
	final static Logger log = LogManager.getLogger(UnitOfMeasurementController.class);
	
	@Autowired
	UnitOfMeasurementService unitOfMeasurementService;
	
	@GetMapping("/uoms")
	public ResponseEntity<List<UnitOfMeasurement>> getAll(){
		
		log.info("in getAll");
		List<UnitOfMeasurement> uoms= unitOfMeasurementService.getAll();
		log.info("returning uoms: "+uoms);
		return new ResponseEntity<List<UnitOfMeasurement>>(uoms, HttpStatus.OK);
	}
	
	@DeleteMapping("/uoms/{id}")
	public ResponseEntity<TransactionResponseDTO> delete(@PathVariable("id") Long id){
		
		TransactionResponseDTO del = unitOfMeasurementService.delete(id);
		
		return new ResponseEntity<TransactionResponseDTO> (del, HttpStatus.OK);
		
	}

}
