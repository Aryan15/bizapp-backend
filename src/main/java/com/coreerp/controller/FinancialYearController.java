package com.coreerp.controller;

import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dao.FinancialYearRepository;
import com.coreerp.dto.FinancialYearDTO;
import com.coreerp.dto.FinancialYearsWrapperDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.FinancialYear;
import com.coreerp.service.FinancialYearService;

@RestController
@RequestMapping("/api")
public class FinancialYearController {
	
	final static Logger log = LogManager.getLogger(FinancialYearController.class);

	@Autowired
	FinancialYearService financialYearService;
	
	@Autowired
	FinancialYearRepository financialYearRepository;
	

	
	@GetMapping("/financial-year")
	public ResponseEntity<FinancialYear> get(){
		
		log.info("in getAll");
		FinancialYear financialYear= financialYearService.findByIsActive(1);
		log.info("returning financialYear: "+financialYear.getId());
		return new ResponseEntity<FinancialYear>(financialYear, HttpStatus.OK);
	}

	
	@GetMapping("/financial-years")
	public ResponseEntity<List<FinancialYear>> getAll(){
		
		log.info("in getAll");
		List<FinancialYear> financialYears= financialYearRepository.findAll();
		//log.info("returning financialYear: "+financialYear);
		return new ResponseEntity<List<FinancialYear>>(financialYears, HttpStatus.OK);
	}
	
	@GetMapping("/financial-years-all")
	public ResponseEntity<List<FinancialYearDTO>> getAllFinancialYear(){
		
		log.info("in getAllFinancialYear");
		List<FinancialYearDTO> financialYears= financialYearService.getAllFinancialYears();
		log.info("returning financialYear: "+financialYears);
		return new ResponseEntity<List<FinancialYearDTO>>(financialYears, HttpStatus.OK);
	}
	
	@PostMapping("/financial-year")
	@Transactional
	public ResponseEntity<FinancialYearsWrapperDTO> save(@RequestBody FinancialYearsWrapperDTO dto ){
		log.info("in save: ");
		FinancialYearsWrapperDTO out  = new FinancialYearsWrapperDTO();
		List<FinancialYearDTO> financialYears = financialYearService.saveAll(dto.getFinancialYearList());
		log.info("list financialYear: "+financialYears);
		out.setFinancialYearList(financialYears);
		log.info("after set financialYear: "+financialYears);
		return new ResponseEntity<FinancialYearsWrapperDTO>(out, HttpStatus.OK);
		//return null;
	}
	
	@DeleteMapping("/financial-year/{id}")
	public ResponseEntity<TransactionResponseDTO> delete(@PathVariable("id") Long id){
		log.info("in delete: "+id);
		TransactionResponseDTO del = financialYearService.delete(id);
		
		return new ResponseEntity<TransactionResponseDTO>(del, HttpStatus.OK);
	}

}
