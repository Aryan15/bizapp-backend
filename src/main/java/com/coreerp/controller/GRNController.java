package com.coreerp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.ApplicationConstants;
import com.coreerp.dto.GRNHeaderDTO;
import com.coreerp.dto.GRNWrapperDTO;
import com.coreerp.dto.RecentGrnDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.GRNHeaderMapper;
import com.coreerp.mapper.PartyMapper;
import com.coreerp.reports.service.RecentGRNService;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.GRNService;
import com.coreerp.service.PartyService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TransactionTypeService;
import com.coreerp.utils.DateUtils;

@RestController
@RequestMapping("/api")
public class GRNController {

	final static Logger log=LogManager.getLogger(GRNController.class);
  
	@Autowired
	GRNService grnService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	RecentGRNService recentGRNService;
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	PartyMapper partyMapper;
	
	@Autowired
	GRNHeaderMapper grnHeaderMapper;
	
	@Autowired
	FinancialYearService financialYearService;
	
	@GetMapping("/grn/{id}")
	public GRNHeaderDTO getGRN(@PathVariable("id") String grnId){
		log.info("in getGRN: "+grnId+"-----");

		
		return grnService.getGRNsByHeaderId(grnId);
	}
	
	@GetMapping("/grn")
	public ResponseEntity<List<GRNHeaderDTO>> getAllGRNs(){
		log.info("In getAllGRNs");
		List<GRNHeaderDTO> grnHeaders =  grnService.findAllGRNs();
		log.info("length: "+grnHeaders.size());
		log.info("item length: "+ (grnHeaders.size() > 0 ? grnHeaders.get(0).getGrnItems().size() : 0));
		return new ResponseEntity<List<GRNHeaderDTO>>(grnHeaders, HttpStatus.OK);
		
	}
	
	@GetMapping("/grn/party/{id}/{grnType}/{t-type}")
	public ResponseEntity<List<GRNHeaderDTO>> getGrnsForAParty(@PathVariable("id") Long id, @PathVariable("grnType") String grnType, @PathVariable("t-type") String Ttype){
		Party party = partyMapper.dtoToModelMap(partyService.getPartyById(id));
		
		TransactionType transactionType = transactionTypeService.getByName(grnType);
		TransactionType typeOfTransaction = transactionTypeService.getByName(Ttype);
		
		List<Status> statuses = Arrays.asList( statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.GRN_STATUS_INVOICED), statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.GRN_STATUS_DELETED));
		
		
		
		List<GRNHeaderDTO> grnList = grnService.getByPartyAndStatusNotIn(party,statuses);//getByParty(partyId);
		
		return new ResponseEntity<List<GRNHeaderDTO>>(grnList,HttpStatus.OK );
		
	}
	
	@GetMapping("/grn/grn-number/{grnNumber}")
	public ResponseEntity<List<GRNHeaderDTO>> getGrnForGrnNumber(@PathVariable("grnNumber") String grnNumber){
		log.info("grn number: "+ grnNumber);
		return new ResponseEntity<List<GRNHeaderDTO>>(grnService.findGRNByGRNNumber(grnNumber), HttpStatus.OK);
	}
	
	
	@GetMapping("/check-grnNumber/{partyId}")
	public ResponseEntity<TransactionResponseDTO> checkpoNumber(
			 @PathVariable("partyId") Long partyId
			,@RequestParam(value = "grnNumber", required=false) String grnNumber,@RequestParam(value="id", required=false)String id){
		
		log.info("partyId :"+ partyId);
		log.info("id :"+ id);
		Long count = grnService.countGRNByGRNNumber(grnNumber, partyId,id);
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		log.info("count :"+ count);
		if (count > 0){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("GRN Number  "+grnNumber+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("GRN Number available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}
	
	@PostMapping("/grn")
	@Transactional
	public ResponseEntity<GRNHeaderDTO> save(@RequestBody GRNWrapperDTO grnWrapperDTOIn){
		
//		GRNHeaderDTO grnDTOIn = grnWrapperDTOIn.getGrnHeader();
//		
//		if(grnWrapperDTOIn.getItemToBeRemove().size() > 0){
//			grnWrapperDTOIn.getItemToBeRemove().forEach(id -> {
//				grnService.deleteItem(id);
//			});
//		}
//		
//		log.info("incoming id: "+grnDTOIn.getId());
//		log.info("Incoming  DTO: "+grnDTOIn);
//		log.info("company id: "+ grnDTOIn.getCompanyId());
//		log.info("Incoming DTO Item: "+grnDTOIn.getGrnItems().size());
		
		GRNHeaderDTO grnDTOOut = grnService.saveGRN(grnWrapperDTOIn);
		log.info("Saved successfully");
		return new ResponseEntity<GRNHeaderDTO>(grnDTOOut, HttpStatus.OK);
	}

	@DeleteMapping("/grn/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteById(@PathVariable("id") String id){
		TransactionResponseDTO deleteStatus = grnService.delete(id);
		log.info("returning delete message: "+deleteStatus);
		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
	}
	
//	@DeleteMapping("/grn/delete-item/{id}")
//	@Transactional
//	public ResponseEntity<TransactionResponseDTO> deleteItemById(@PathVariable("id") String id){
//		
//		TransactionResponseDTO deleteStatus = grnService.deleteItem(id);
//		log.info("returning delete message: "+deleteStatus);
//		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
//		
//	}
	
	
	@GetMapping("/grn/recent/{grnTypeId}/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<RecentGrnDTO> getRecentGrn(
												  @PathVariable("grnTypeId") Long grnTypeId
												, @PathVariable("sortColumn") String sortColumn
												, @PathVariable("sortDirection") String sortDirection
												, @PathVariable("pageNumber") Integer pageNumber
												, @PathVariable("pageSize") Integer pageSize
												, @RequestParam(value = "searchText", required=false) String searchText												){
		log.info("grnTypeId: "+grnTypeId);
		log.info("searchText: "+searchText);
		log.info("searchText.isEmpty(): "+searchText.isEmpty());
		log.info("searchText.length(): "+searchText.length());
		log.info("searchText == null : "+searchText == null ? "NULL" : "NOT NULL");
		log.info("searchText.trim(): "+searchText.trim());
		
		log.info("sortColumn: "+sortColumn);
		log.info("sort direction: "+sortDirection);
		log.info("pageNumber: " +pageNumber);
		log.info("pageSize: "+pageSize);
		
		Long totalCount = 0l;
		List<GRNHeader> grnHeaders = new ArrayList<GRNHeader>();
		
		RecentGrnDTO recentGrnDTO = new RecentGrnDTO();
		
		TransactionType grnType = transactionTypeService.getModelById(grnTypeId);
		
		FinancialYear financialYear = financialYearService.findByIsActive(1);

		if (searchText.isEmpty() || searchText.equals("null")) {
			searchText=null;
		}
		Page<GRNHeader> recentGrn = recentGRNService.getGrnPageList(grnType, financialYear, searchText, sortColumn, sortDirection, pageNumber, pageSize);
		grnHeaders.addAll(recentGrn.getContent());
		totalCount = recentGrn.getTotalElements();


		recentGrnDTO.setGrnHeaders(grnHeaderMapper.modelToRecentDTOList(grnHeaders));
		recentGrnDTO.setTotalCount(totalCount);
		
		
		return new ResponseEntity<RecentGrnDTO>(recentGrnDTO, HttpStatus.OK);
		
	}
}
