package com.coreerp.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dto.B2BGSTReportDTO;
import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.reports.model.B2BGSTReport;
import com.coreerp.reports.service.GSTReportPageableService;

@RestController
@RequestMapping("/api")
public class GSTReportController {
	
	final static Logger log = LogManager.getLogger(GSTReportController.class);
	
	@Autowired
	GSTReportPageableService gstReportPageableService;
	
	
	
	@PostMapping("/gst/b2b-report/{invoiceTypeId}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
	public ResponseEntity<B2BGSTReportDTO> getB2BReport(
			@PathVariable("invoiceTypeId") Long invoiceTypeId,
			@PathVariable("pageNumber") Integer pageNumber,
			@PathVariable("pageSize") Integer pageSize,
			@PathVariable("sortDir") String sortDir,
			@PathVariable("sortColumn") String sortColumn,
			@RequestBody TransactionReportRequestDTO invoiceReportRequestDTO){
		log.info("incoming request: "+invoiceReportRequestDTO);
		log.info("incoming request: "+invoiceReportRequestDTO.getTransactionFromDate());
		log.info("incoming request: "+invoiceReportRequestDTO.getTransactionToDate());
		log.info("incoming request: "+invoiceReportRequestDTO.getFinancialYearId());
		
		
		
		log.info("sortColumn: "+sortColumn);
		if(sortColumn.equals("partyName"))
			sortColumn = "party.name";
		
		Page<B2BGSTReport> reportPage =gstReportPageableService.getPagedB2BReport(invoiceReportRequestDTO.getFinancialYearId(), invoiceReportRequestDTO.getTransactionFromDate(), invoiceReportRequestDTO.getTransactionToDate(), invoiceTypeId, pageNumber, pageSize, sortDir, sortColumn); 
		
		List<B2BGSTReport> b2bReport = reportPage.getContent();

		
		//Long totalCount = invoiceItemReportRepository.getReportTotalCount(invoiceReportRequestDTO.getFinancialYearId(), invoiceReportRequestDTO.getTransactionFromDate(), invoiceReportRequestDTO.getTransactionToDate(), invoiceReportRequestDTO.getPartyId(), partyTypeIds);
		
		Long totalCount = reportPage.getTotalElements();
		
		
		log.info("total count: "+totalCount);
		
		B2BGSTReportDTO B2BGSTReportDTO = new B2BGSTReportDTO();
		
		B2BGSTReportDTO.setB2bGSTReports(b2bReport);
		B2BGSTReportDTO.setTotalCount(totalCount);
		
		return new ResponseEntity<B2BGSTReportDTO>(B2BGSTReportDTO, HttpStatus.OK);
	}

}
