package com.coreerp.controller;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dao.StockTraceReportRepository;
import com.coreerp.model.StockTraceReport;

@RestController
@RequestMapping("/api")
public class StockTraceReportController {
	
	final static Logger log = LogManager.getLogger(StockTraceReportController.class);
	
	@Autowired
	StockTraceReportRepository stockTraceReportRepository;
	
	
	@PostMapping("/stock-trace")
	public ResponseEntity<List<StockTraceReport>> getStockTrace(@RequestBody StockTraceRequestDTO stockTraceRequestDTO){
		
		log.info("incoming request: "+stockTraceRequestDTO.getFromDate());
		log.info("incoming request: "+stockTraceRequestDTO.getToDate());
		log.info("incoming request: "+stockTraceRequestDTO.getMaterialId());
		
		List<StockTraceReport> stockTraceReport = stockTraceReportRepository.getReportForMaterial(stockTraceRequestDTO.getFromDate(), stockTraceRequestDTO.getToDate(), stockTraceRequestDTO.getMaterialId());
		
		return new ResponseEntity<List<StockTraceReport>>(stockTraceReport, HttpStatus.OK);
	}

}

class StockTraceRequestDTO{
	private Date fromDate;
	private Date toDate;
	private Long materialId;
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Long getMaterialId() {
		return materialId;
	}
	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}
	
}