package com.coreerp.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.coreerp.dao.TransactionStatusChangeRepository;
import com.coreerp.dto.CompanyDTO;
import com.coreerp.dto.CompanyWithBankListDTO;
import com.coreerp.dto.PartyBankMapDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.reports.service.PartyBankMapService;
import com.coreerp.service.CompanyService;

@RestController
@RequestMapping("/api")
public class CompanyController {
	
	final static Logger log = LogManager.getLogger(CompanyController.class);
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	RestTemplate restTemplate ;
	
	@Autowired
	PartyBankMapService partyBankMapService;
	
	@Autowired
	TransactionStatusChangeRepository transactionStatusChangeRepository;
	
//	@Autowired
//	StorageService storageService;


	
	
//	@PostMapping("/company")
//	public ResponseEntity<CompanyDTO> save(@RequestBody CompanyDTO companyDTO){
//		
//		log.info("incoming dto: "+companyDTO);
//		
//		return new ResponseEntity<CompanyDTO>(companyService.saveCompany(companyDTO), HttpStatus.OK);
//		
//	}

	@PostMapping("/company")
	public ResponseEntity<CompanyWithBankListDTO> save(@RequestBody CompanyWithBankListDTO companyWithBankListDTO){


		CompanyDTO companyDTO = companyWithBankListDTO.getCompanyDTO();
		//companyDTO.getCompanyCurrencyId();
		CompanyDTO companyDTOOut = companyService.saveCompany(companyDTO);
		
		List<PartyBankMapDTO> partyBankMapDTOList = companyWithBankListDTO.getPartyBankMapDTOList();
		
		partyBankMapDTOList.forEach(p -> {

			p.setCompanyId(companyDTOOut.getId());
		});
		
		List<Long> partyBankMapListDeletedIds = companyWithBankListDTO.getPartyBankMapListDeletedIds();
		
		
		partyBankMapListDeletedIds.forEach(id -> {
			partyBankMapService.deleteById(id);
		});
		
		
		List<PartyBankMapDTO> partyBankMapDTOListOut = partyBankMapService.saveList(partyBankMapDTOList);
		
		
		CompanyWithBankListDTO companyWithBankListDTOOut = new CompanyWithBankListDTO();

		CompanyDTO companyDTOOut2 = companyService.getCompanyById(companyDTO.getId());
		companyWithBankListDTOOut.setCompanyDTO(companyDTOOut2);
		companyWithBankListDTOOut.setPartyBankMapDTOList(partyBankMapDTOListOut);
		
		return new ResponseEntity<CompanyWithBankListDTO>(companyWithBankListDTOOut, HttpStatus.OK);
				
		
	}
	
	@GetMapping("/company/{id}")
	public ResponseEntity<CompanyDTO> get(@PathVariable("id") Long id){
		
		log.info("incoming id: "+id);

		return new ResponseEntity<CompanyDTO>(companyService.getCompanyById(id), HttpStatus.OK);
	}
	
	@GetMapping("/company-with-bank/{id}")
	public ResponseEntity<CompanyWithBankListDTO> getCompanyWithBank(@PathVariable("id") Long id){
		
		log.info("incoming id: "+id);
		
		CompanyDTO companyDTO = companyService.getCompanyById(id);
		
		CompanyWithBankListDTO companyWithBankListDTO = new CompanyWithBankListDTO();
		
		companyWithBankListDTO.setCompanyDTO(companyDTO);
		companyWithBankListDTO.setPartyBankMapDTOList(partyBankMapService.getForCompany(companyDTO.getId()));
		
		return new ResponseEntity<CompanyWithBankListDTO>(companyWithBankListDTO, HttpStatus.OK);
	}
	
	@GetMapping("/company/transaction-count")
	public ResponseEntity<TransactionResponseDTO> getcompanyTransCount(){
		
		TransactionResponseDTO out = new TransactionResponseDTO();
		
		Long transCount = transactionStatusChangeRepository.count();
		
		if(transCount > 0) {
			out.setResponseStatus(0);
			out.setResponseString("Transactions already created. Cannot change state");
		}else {
			out.setResponseStatus(1);
			out.setResponseString("No Transactions created. Can change state");
		}
		
		return new ResponseEntity<TransactionResponseDTO>(out, HttpStatus.OK);
	}
	
	@GetMapping("/company/logo/{id}")
	public ResponseEntity<TransactionResponseDTO> getcompanyLogoName(@PathVariable("id") Long id){
		
		log.info("incloming company id: "+id);
		
		String companyLogoName = companyService.getCompanyById(id).getCompanyLogoPath();
		TransactionResponseDTO out = new TransactionResponseDTO();

		
		if(companyLogoName != null){
			
			out.setResponseString(companyLogoName);
			out.setResponseStatus(1);
		}else{
			out.setResponseString(null);
			out.setResponseStatus(0);
		}
		

		log.info("outgoing company logo file name: "+companyLogoName);
		
		return new ResponseEntity<TransactionResponseDTO>(out, HttpStatus.OK);
		
	}
	
	
	@GetMapping("/company/cert-image/{id}")
	public ResponseEntity<TransactionResponseDTO> getcompanyCertImageName(@PathVariable("id") Long id){
		
		log.info("incloming company id: "+id);
		
		String companyLogoName = companyService.getCompanyById(id).getCeritificateImagePath();
		TransactionResponseDTO out = new TransactionResponseDTO();

		
		if(companyLogoName != null){
			
			out.setResponseString(companyLogoName);
			out.setResponseStatus(1);
		}else{
			out.setResponseString(null);
			out.setResponseStatus(0);
		}
		

		log.info("outgoing company logo file name: "+companyLogoName);
		
		return new ResponseEntity<TransactionResponseDTO>(out, HttpStatus.OK);
		
	}


	@GetMapping("/company/signature-image/{id}")
	public ResponseEntity<TransactionResponseDTO> getSignatureImageName(@PathVariable("id") Long id){

		log.info("incloming company id: "+id);

		String signatureImagePath = companyService.getCompanyById(id).getSignatureImagePath();
		TransactionResponseDTO out = new TransactionResponseDTO();


		if(signatureImagePath != null){

			out.setResponseString(signatureImagePath);
			out.setResponseStatus(1);
		}else{
			out.setResponseString(null);
			out.setResponseStatus(0);
		}


		log.info("outgoing company logo file name: "+signatureImagePath);

		return new ResponseEntity<TransactionResponseDTO>(out, HttpStatus.OK);

	}



//	@PostMapping("/company/logo")
//	public ResponseEntity<TransactionResponseDTO> updateLogoPath(@RequestParam("image") MultipartFile file){
//		
//		
////		//log.info("uri: "+test);
////		log.info("path: "+request.getContextPath()+":"+request.getLocalAddr()+":"+request.getLocalPort()+":"+request.getRequestURL());
////		log.info(request.getScheme());
////		log.info(request.getServerName());
////		log.info(request.getServerPort());
////		log.info(request.getServletContext());
////		
////		String fileEndPointUrl = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/api/files";
////		
////		log.info("fileEndPointUrl: "+fileEndPointUrl);
////		
////		ResponseEntity<TransactionResponseDTO> response = restTemplate.postForEntity(fileEndPointUrl, file,
////				TransactionResponseDTO.class);
//		
//    	log.info("incoming file: "+file.getName());
//    	
//    	String authToken = request.getHeader(this.tokenHeader);
//    	String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);
//    	log.info("any roken here? :"+tenantId);
//    	
//    	TransactionResponseDTO response = new TransactionResponseDTO();
//    	try{
//        storageService.store(file, tenantId);
//        log.info("saved successfully: "+file.getOriginalFilename());
//        
//        //Update logo path in the company
//        Company comp = companyService.getModelById(1l);
//        
//        comp.setCompanyLogoPath(file.getOriginalFilename());
//        
//        companyService.saveModel(comp);
//        
//    	}catch(Exception e){
//    		response.setResponseString("Error uploading file");
//    		throw e;
//    	}
//    	
//    	response.setResponseString("Success");
//
//        return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);		
//	}
//log.info(request.getServletContext());
}
