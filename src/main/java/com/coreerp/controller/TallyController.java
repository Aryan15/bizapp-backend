package com.coreerp.controller;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.PartyTypeRepository;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dto.TallyExportTrackerReport;
import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.model.PartyType;
import com.coreerp.model.TransactionType;
import com.coreerp.tally.model.voucher.Envelope;
import com.coreerp.tally.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api")
public class TallyController {

    final static Logger log = LogManager.getLogger(TallyController.class);

    @Autowired
    TallyVoucherService tallyVoucherService;

    @Autowired
    CustomerService customerService;

    @Autowired
    TransactionTypeRepository transactionTypeRepository;

    @Autowired
    UomService uomService;

    @Autowired
    GstEnvelopeService gstEnvelopeService;

    @Autowired
    TallyExportTrackerService tallyExportTrackerService;

    @Autowired
    PartyTypeRepository partyTypeRepository;

    @Autowired
    StockItemService stockItemService;

    @Autowired
   BankServiceses bankServiceses;

    @Autowired
    MasterService masterService;

    @PostMapping("/tally-voucher/{invoiceTypeId}")
    public ResponseEntity<InputStreamResource> getVoucherXmlStream(@PathVariable("invoiceTypeId") Long invoiceTypeId,
                                                             @RequestBody TransactionReportRequestDTO reportRequestDTO){
        log.info("'getVoucherXml "+reportRequestDTO);
        TransactionType invoiceType = transactionTypeRepository.getOne(invoiceTypeId);
        return new ResponseEntity<>(tallyVoucherService.getVoucherXml(invoiceType,reportRequestDTO.getPartyId(),reportRequestDTO.getTransactionFromDate(), reportRequestDTO.getTransactionToDate()), HttpStatus.OK);
    }


  @GetMapping("/tally-master/{partyTypeId}")
    public ResponseEntity<InputStreamResource> getPartyXml(@PathVariable("partyTypeId") Long partyTypeId){

        return new ResponseEntity<>(masterService.getGstXML(partyTypeId), HttpStatus.OK);
    }

//    @GetMapping("/tally-uom")
//    public ResponseEntity<InputStreamResource> getUomXml(){
//
//        return new ResponseEntity<>(uomService.getUomXML(), HttpStatus.OK);
//    }

    /*@GetMapping("/tally-gst")
    public ResponseEntity<InputStreamResource> getGstXml(){

        return new ResponseEntity<>(gstEnvelopeService.getGstXML() , HttpStatus.OK);
    }
*/
  /*@GetMapping("/tally-stockItem")
    public ResponseEntity<InputStreamResource> getStockItemXml(){

        return new ResponseEntity<>(stockItemService.getStockItemXml() , HttpStatus.OK);
    }*/

//    @GetMapping("/tally-bank")
//    public ResponseEntity<InputStreamResource> getBankXML(){
//
//        return new ResponseEntity<>(bankServiceses.getBankXML() , HttpStatus.OK);
//    }


    @PostMapping("/tally-tracker/{invoiceTypeId}")
    public ResponseEntity<Boolean> makeTallyTrackerEntries(@PathVariable("invoiceTypeId") Long invoiceTypeId,
                                                             @RequestBody TransactionReportRequestDTO reportRequestDTO){

        TransactionType transactionType = transactionTypeRepository.getOne(invoiceTypeId);
        PartyType partyType = null;
        if(transactionType.getName().equals(ApplicationConstants.INVOICE_TYPE_CUSTOMER)){
            partyType = partyTypeRepository.findByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
        }else{
            partyType = partyTypeRepository.findByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);
        }
        Boolean retValue = tallyExportTrackerService.makeEntry(reportRequestDTO.getPartyId(),reportRequestDTO.getTransactionFromDate(), reportRequestDTO.getTransactionToDate(), invoiceTypeId, partyType.getId());

        return new ResponseEntity<>(retValue, HttpStatus.OK);
    }


    @PostMapping("/tally-tracker-report/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
    public ResponseEntity<TallyExportTrackerReport> getTrackerItemReport(
            @PathVariable("pageNumber") Integer pageNumber,
            @PathVariable("pageSize") Integer pageSize,
            @PathVariable("sortDir") String sortDir,
            @PathVariable("sortColumn") String sortColumn,
            @RequestBody TransactionReportRequestDTO invoiceReportRequestDTO){

        return new ResponseEntity<TallyExportTrackerReport>(tallyExportTrackerService.getPagedTallyTrackerReport(pageNumber, pageSize, sortDir, sortColumn), HttpStatus.OK);
    }
}
