package com.coreerp.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dto.CountryDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.Country;
import com.coreerp.service.CountryService;


@RestController
@RequestMapping("/api")
public class CountryController {

	final static Logger log = LogManager.getLogger(CountryController.class);
	
	@Autowired
	CountryService countryService;
	
	@PostMapping("/country")
	public ResponseEntity<Country> countrySave(@RequestBody CountryDTO countryDTO){
		
		log.info("In countrySave: name = "+countryDTO.getName());
		Country country = countryService.saveCountry(countryDTO);
		log.info("Country has been saved");
		
		//get the saved country to return correct id
		
		//Country country = countryService.getCountryById
		
		return new ResponseEntity<Country>(country, HttpStatus.OK);
	}
	
	@DeleteMapping("/country/{ids}")
	public ResponseEntity<TransactionResponseDTO> delete(@PathVariable List<Long> ids){
		
		TransactionResponseDTO response = new TransactionResponseDTO();
		
		response.setResponseStatus(1);
		response.setResponseString("Deleted Successfully");
		
		try{
			ids.forEach(id -> {
				countryService.deleteCountryById(id);	
			});
			
		}catch(Exception e){
			log.info("Error deleteing.."+e.getMessage());
			response.setResponseStatus(1);
			response.setResponseString(e.getMessage());
		}
		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	}
	
	@PostMapping("/countries")
	public ResponseEntity<List<Country>> countriesSave(@RequestBody List<CountryDTO> countryDTOList){
		
		log.info("in countriesSave: "+countryDTOList);
		List<Country> outCountries = new ArrayList<Country>();
		
		for(CountryDTO countryDTO : countryDTOList){
			if(!countryDTO.getName().isEmpty()){
				Country country = countryService.saveCountry(countryDTO);
				outCountries.add(country);				
			}

		}
		
		return new ResponseEntity<List<Country>>(outCountries, HttpStatus.OK);
		
	}
	
	@GetMapping("/country")
	public ResponseEntity<List<CountryDTO>> getAllCountries() {
		
		log.info("In getAllCountries");
		List<CountryDTO> countryDTOList = countryService.getAllCountries();
		
		log.info("countryDTOList.size(): "+ countryDTOList.size());
		
		return new ResponseEntity<List<CountryDTO>>(countryDTOList, HttpStatus.OK);
	}
}
