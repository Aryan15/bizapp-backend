package com.coreerp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.dao.*;
import com.coreerp.dto.*;
import com.coreerp.mapper.AutoTransactionMapper;

import com.coreerp.model.*;
import com.coreerp.reports.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.ApplicationConstants;
import com.coreerp.mapper.InvoiceHeaderMapper;
import com.coreerp.mapper.PartyMapper;
import com.coreerp.reports.model.InvoiceItemReportModel;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.GlobalSettingService;
import com.coreerp.service.InvoiceService;
import com.coreerp.service.PartyService;
import com.coreerp.service.PartyTypeService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TransactionTypeService;
import com.coreerp.utils.DateUtils;

@RestController
@RequestMapping("/api")
public class InvoiceController {
	
	final static Logger log = LogManager.getLogger(InvoiceController.class);
	
	@Autowired
	InvoiceService invoiceService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	PartyTypeService partyTypeService;
	
	@Autowired
	RecentInvoiceService recentInvoiceService;
	
	@Autowired
	InvoiceHeaderMapper invoiceHeaderMapper;
	
//	@Autowired
//	InvoiceReportRepository invoiceReportRepository;
	
//	@Autowired	
//	InvoiceItemReportRepository invoiceItemReportRepository;
	
	@Autowired
	InvoiceReportPageableService invoiceReportPageableService;
	
	@Autowired
	InvoiceItemReportPageableService invoiceItemReportPageableService;
	
	@Autowired
	InvoiceTaxReportPageableService invoiceTaxReportPageableService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	PartyMapper partyMapper;
	
	@Autowired
	StatusService statusService;
	

	@Autowired
	FinancialYearService financialYearService;
	
	@Autowired
	GlobalSettingService globalSettingService;

	@Autowired
	AutoTransactionMapper autoTransactionMapper;

	@Autowired
	RecentAutoTransactionService recentAutoTransactionService;

	@Autowired
	PayableReceivableItemRepository payableReceivableItemRepository;

	@Autowired
	AutoTransactionRepository autoTransactionRepository;

	
	@GetMapping("/invoice/{id}")
	public InvoiceDTO getInvoice(@PathVariable("id") String invoiceId){
		log.info("in getInvoice: "+invoiceId);

		
		return invoiceService.getInvoicesByHeaderId(invoiceId);
	}

	@GetMapping("/invoice/inv-no/{invoiceNumber}")
	public ResponseEntity<InvoiceDTO> getInvoiceByInvoiceNumber(@PathVariable("invoiceNumber") String invoiceNumber){
		log.info("in getInvoiceByInvoiceNumber: "+invoiceNumber);
		
		InvoiceDTO ret = invoiceService.findInvoiceByInvoiceNumber(invoiceNumber);
		
		log.info("Got invoice: "+ret);
		
		return new ResponseEntity<InvoiceDTO>(ret, HttpStatus.OK);
	}
	
	@GetMapping("/invoice")
	public ResponseEntity<List<InvoiceDTO>> getAllInvoices(){
		log.info("In getAllInvoices");
		List<InvoiceDTO> invoiceHeaders =  invoiceService.findAllInvoices();
		log.info("invoiceHeaders.....................>>>>>>>>" +invoiceHeaders);
		log.info("length: "+invoiceHeaders.size());
		log.info("length: "+invoiceHeaders.size());
		log.info("item length: "+ (invoiceHeaders.size() > 0 ? invoiceHeaders.get(0).getInvoiceItems().size() : 0));
		return new ResponseEntity<List<InvoiceDTO>>(invoiceHeaders, HttpStatus.OK);
		
	}

	@GetMapping("/invoice/customer")
	public ResponseEntity<List<InvoiceDTO>> getAllCustomerInvoices(){
		log.info("In getAllCustomerInvoices");
		List<InvoiceDTO> invoiceHeaders =  invoiceService.findAllCustomerInvoices();
		log.info("length: "+invoiceHeaders.size());
		log.info("item length: "+ (invoiceHeaders.size() > 0 ? invoiceHeaders.get(0).getInvoiceItems().size() : 0));
		return new ResponseEntity<List<InvoiceDTO>>(invoiceHeaders, HttpStatus.OK);
		
	}
	
	@GetMapping("/invoice/supplier")
	public ResponseEntity<List<InvoiceDTO>> getAllSupplierInvoices(){
		log.info("In getAllSupplierInvoices");
		List<InvoiceDTO> invoiceHeaders =  invoiceService.findAllSupplierInvoices();
		log.info("length: "+invoiceHeaders.size());
		log.info("item length: "+ (invoiceHeaders.size() > 0 ? invoiceHeaders.get(0).getInvoiceItems().size() : 0));
		return new ResponseEntity<List<InvoiceDTO>>(invoiceHeaders, HttpStatus.OK);
		
	}

	@GetMapping("/invoice/invoice-party-ttype/{partyId}/{transactionTypeId}")
	public ResponseEntity<List<InvoiceDTO>> getInvoicesForPartyAndTransactionType(
			@PathVariable("partyId") Long partyId, @PathVariable("transactionTypeId") Long transactionTypeId) {

		//log.info("in getInvoicesForPartyAndTransactionType");
		//log.info("partyId: " + partyId + " transactionTypeId: " + transactionTypeId);
		List<InvoiceDTO> invoiceHeaders = invoiceService.getInvoicesForPartyAndTransactionTypeWithNonZeroDueAmount(partyId, transactionTypeId);
		if (invoiceHeaders != null) {

			invoiceHeaders.forEach(inv -> {
				List<PayableReceivableItem> payableReceivableItem = payableReceivableItemRepository.findByInvoiceHeader(invoiceHeaderMapper.dtoToModelMap(inv));
				if (payableReceivableItem.size() != 0) {
					payableReceivableItem.forEach(pay -> {
						inv.setPreTdsPer(pay.getTdsPercentage());
						inv.setPreTdsAmount(pay.getTdsAmount());

					});


				}

				if(inv.getTaxAmount() == null || inv.getTaxAmount() == 0.0){
					if(inv.getCgstTaxAmount()==null || inv.getCgstTaxAmount()==0.0) {
						inv.setTaxAmount( inv.getIgstTaxAmount());
					}
					else{
						inv.setTaxAmount( inv.getCgstTaxAmount()+inv.getSgstTaxAmount());
					}


				}
				if (inv.getTaxAmount() == null || inv.getTaxAmount() == 0.0 ) {


						Double taxAmount = inv.getInvoiceItems()
								.stream()
								.mapToDouble(item -> item.getIgstTaxAmount() + item.getCgstTaxAmount() + item.getSgstTaxAmount())
								.sum();
                           //log.info(i);

						inv.setTaxAmount(taxAmount);

				}
				if (inv.getTcsAmount() != null && inv.getTcsAmount() != 0.0) {
					inv.setTaxAmount(inv.getTaxAmount() + inv.getTcsAmount());
				}



			});
		}
		log.info("invoiceHeaders: " + invoiceHeaders);
		return new ResponseEntity<List<InvoiceDTO>>(invoiceHeaders, HttpStatus.OK);
	}
	
	@GetMapping("/invoice/get-notes/{partyId}/{transactionTypeId}")
	public ResponseEntity<List<InvoiceDTO>> getOpenNotesForPartyAndTransactionType(
			@PathVariable("partyId") Long partyId, @PathVariable("transactionTypeId") Long transactionTypeId){
		
		log.info("in getInvoicesForPartyAndTransactionType");
		log.info("partyId: "+partyId+" transactionTypeId: "+transactionTypeId);
		
		Party party = partyService.getPartyObjectById(partyId);
		TransactionType tType = transactionTypeService.getModelById(transactionTypeId);
		
		List<Status> statuses = new ArrayList<Status>();
		
		statuses.add(statusService.getStatusByTransactionTypeAndName(tType, ApplicationConstants.INVOICE_STATUS_NOTE_COMPLETED));
		
		List<InvoiceDTO> invoiceHeaders = invoiceService.getByPartyAndTransactionTypeAndStatusNotIn(party, tType, statuses);
		
//		log.info("invoiceHeaders: "+invoiceHeaders);
		return new ResponseEntity<List<InvoiceDTO>>(invoiceHeaders, HttpStatus.OK);
	}

	@PostMapping("/invoice")
	@Transactional
	public ResponseEntity<InvoiceDTO> save(@RequestBody InvoiceWrapperDTO invoiceWrapperIn){
		log.info("in save: "+invoiceWrapperIn.getInvoiceHeader().getMaterialNoteType());
		//log.info("incoming id: "+invoiceDTOIn.getId());
		//log.info("Incoming  DTO: "+invoiceDTOIn);
		//log.info("Incoming DTO Item: "+invoiceDTOIn.getInvoiceItems().size());
		InvoiceDTO invoiceDTOOut=new InvoiceDTO();

		if(invoiceWrapperIn.getInvoiceHeader().getIsReused()!=null && invoiceWrapperIn.getInvoiceHeader().getIsReused() == 1 && invoiceWrapperIn.getInvoiceHeader().getId()==null) {

				invoiceDTOOut = invoiceService.reuseInvoiec(invoiceWrapperIn);
			}

       else {
			invoiceDTOOut = invoiceService.saveInvoice(invoiceWrapperIn);
		}
		log.info("Saved successfully");
		return new ResponseEntity<InvoiceDTO>(invoiceDTOOut, HttpStatus.OK);
	}
	
	@DeleteMapping("/invoice/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteById(@PathVariable("id") String id){
        Integer reuseValue=0;
		TransactionResponseDTO deleteStatus = invoiceService.delete(id,reuseValue);
		log.info("returning delete message: "+deleteStatus);
		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
	}
	
	
	
	@GetMapping("/invoice/recent/{invoiceTypeId}/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<RecentInvoiceDTO> getRecentInvoice(
												  @PathVariable("invoiceTypeId") Long invoiceTypeId												
												, @PathVariable("sortColumn") String sortColumn
												, @PathVariable("sortDirection") String sortDirection
												, @PathVariable("pageNumber") Integer pageNumber
												, @PathVariable("pageSize") Integer pageSize
												, @RequestParam(value = "searchText", required=false) String searchText){
		log.info("invoiceTypeId: "+invoiceTypeId);
		log.info("searchText: "+searchText);
		log.info("searchText.isEmpty(): "+searchText.isEmpty());
		log.info("searchText.length(): "+searchText.length());
		log.info("searchText == null : "+searchText == null ? "NULL" : "NOT NULL");
		log.info("searchText.trim(): "+searchText.trim());
		
		log.info("sortColumn: "+sortColumn);
		log.info("sort direction: "+sortDirection);
		log.info("pageNumber: " +pageNumber);
		log.info("pageSize: "+pageSize);
		
		Long totalCount = 0l;
		List<InvoiceHeader> invoiceHeaders = new ArrayList<InvoiceHeader>();
		
		RecentInvoiceDTO recentInvoiceDTO = new RecentInvoiceDTO();
		
		TransactionType invoiceType = transactionTypeService.getModelById(invoiceTypeId);
		
		FinancialYear financialYear = financialYearService.findByIsActive(1);

		if (searchText.isEmpty() || searchText.equals("null")) {
			searchText=null;
		}
		Page<InvoiceHeader> recentInvoice = recentInvoiceService.getInvoicePageList(invoiceType, financialYear, searchText, sortColumn, sortDirection, pageNumber, pageSize);
		invoiceHeaders.addAll(recentInvoice.getContent());
		totalCount = recentInvoice.getTotalElements();

		recentInvoiceDTO.setInvoiceHeaders(invoiceHeaderMapper.modelToRecentDTOList(invoiceHeaders));
		recentInvoiceDTO.setTotalCount(totalCount);
		

		return new ResponseEntity<RecentInvoiceDTO>(recentInvoiceDTO, HttpStatus.OK);
		
	}
	
//	@DeleteMapping("/invoice/delete-item/{id}")
//	@Transactional
//	public ResponseEntity<TransactionResponseDTO> deleteItemById(@PathVariable("id") String id){
//		
//		TransactionResponseDTO deleteStatus = invoiceService.deleteItem(id);
//		log.info("returning delete message: "+deleteStatus);
//		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
//		
//	}
	
	
	@PostMapping("/invoice/report/{invoiceTypeId}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
	public ResponseEntity<InvoiceReportDTO> getInvoiceReport(
			@PathVariable("invoiceTypeId") Long invoiceTypeId,
			@PathVariable("pageNumber") Integer pageNumber,
			@PathVariable("pageSize") Integer pageSize,
			@PathVariable("sortDir") String sortDir,
			@PathVariable("sortColumn") String sortColumn,
			@RequestBody TransactionReportRequestDTO invoiceReportRequestDTO){
		log.info("incoming request 1: "+invoiceReportRequestDTO);
		log.info("incoming request from date: "+invoiceReportRequestDTO.getTransactionFromDate());
		log.info("incoming request to date: "+invoiceReportRequestDTO.getTransactionToDate());
		log.info("incoming request mat id: "+invoiceReportRequestDTO.getMaterialId());
		log.info("incoming request party id: "+invoiceReportRequestDTO.getPartyId());
		log.info("incoming request fin id: "+invoiceReportRequestDTO.getFinancialYearId());
		log.info("incoming request party type: "+invoiceReportRequestDTO.getPartyTypeIds());
		
		List<Long> partyTypeIds = null;
		
		if(invoiceReportRequestDTO.getPartyTypeIds().isEmpty()){
			PartyType partyTypeCustomer = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
			PartyType partyTypeSupplier = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);
			PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
			
			List<PartyType> partyTypeList = new ArrayList<PartyType>();
			
			partyTypeList.add(partyTypeCustomer);
			partyTypeList.add(partyTypeSupplier);
			partyTypeList.add(partyTypeBoth);
			
			//List<Long> partyTypeIds = new ArrayList<Long>();
			
			partyTypeIds = partyTypeList.stream()
							.map(partyType -> partyType.getId())
							.collect(Collectors.toList());
			
		}
		else{
			
			partyTypeIds = invoiceReportRequestDTO.getPartyTypeIds();
			
		}
		log.info("party type ids: "+partyTypeIds);
		
		log.info("sortColumn: "+sortColumn);
		if(sortColumn.equals("partyName"))
			sortColumn = "party.name";
		
		TransactionType invoiceType = transactionTypeService.getModelById(invoiceTypeId);
		log.info(invoiceType.getName());
		
		InvoiceReportDTO invoiceReportDTO =  invoiceReportPageableService.getPagedInvoiceReport(invoiceType.getId(), invoiceReportRequestDTO.getFinancialYearId(), invoiceReportRequestDTO.getTransactionFromDate(), invoiceReportRequestDTO.getTransactionToDate(), invoiceReportRequestDTO.getPartyId(), partyTypeIds, invoiceReportRequestDTO.getCreatedBy(), pageNumber, pageSize, sortDir, sortColumn);
		
//		Long totalCount = invoiceReportPageableService.getInvoiceReportCount(invoiceType.getId(),invoiceReportRequestDTO.getFinancialYearId(), invoiceReportRequestDTO.getTransactionFromDate(), invoiceReportRequestDTO.getTransactionToDate(), invoiceReportRequestDTO.getPartyId(), partyTypeIds);
		
		
//		InvoiceReportDTO invoiceReportDTO = new InvoiceReportDTO();
		
		
//		invoiceReportDTO.setInvoiceReports(invoiceReport);
//		invoiceReportDTO.setTotalCount(totalCount);
		
		return new ResponseEntity<InvoiceReportDTO>(invoiceReportDTO, HttpStatus.OK);
	}
	
	@PostMapping("/invoice/item-report/{invoiceTypeId}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
	public ResponseEntity<InvoiceItemReportDTO> getInvoiceItemReport(
			@PathVariable("invoiceTypeId") Long invoiceTypeId,
			@PathVariable("pageNumber") Integer pageNumber,
			@PathVariable("pageSize") Integer pageSize,
			@PathVariable("sortDir") String sortDir,
			@PathVariable("sortColumn") String sortColumn,
			@RequestBody TransactionReportRequestDTO invoiceReportRequestDTO){
		log.info("incoming request: "+invoiceReportRequestDTO);
		log.info("incoming request from: "+invoiceReportRequestDTO.getTransactionFromDate());
		log.info("incoming request to: "+invoiceReportRequestDTO.getTransactionToDate());
		log.info("incoming request mat: "+invoiceReportRequestDTO.getMaterialId());
		log.info("incoming request party: "+invoiceReportRequestDTO.getPartyId());
		log.info("incoming request fin: "+invoiceReportRequestDTO.getFinancialYearId());
		log.info("incoming request party type: "+invoiceReportRequestDTO.getPartyTypeIds());
		log.info("incoming request created By: "+invoiceReportRequestDTO.getCreatedBy());
		
		List<Long> partyTypeIds = null;
		
		if(invoiceReportRequestDTO.getPartyTypeIds().isEmpty()){
			PartyType partyTypeCustomer = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
			PartyType partyTypeSupplier = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);
			PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
			
			List<PartyType> partyTypeList = new ArrayList<PartyType>();
			
			partyTypeList.add(partyTypeCustomer);
			partyTypeList.add(partyTypeSupplier);
			partyTypeList.add(partyTypeBoth);
			
			//List<Long> partyTypeIds = new ArrayList<Long>();
			
			partyTypeIds = partyTypeList.stream()
							.map(partyType -> partyType.getId())
							.collect(Collectors.toList());
			
		}
		else{
			
			partyTypeIds = invoiceReportRequestDTO.getPartyTypeIds();
			
		}
		log.info("party type ids: "+partyTypeIds);
		
		log.info("sortColumn: "+sortColumn);
		if(sortColumn.equals("partyName"))
			sortColumn = "party.name";
		
		Integer isHeaderLevel = globalSettingService.getGlobalSettingById(1l).getItemLevelTax().compareTo(1) == 0 ? 0 : 1;


		log.info("isHeaderLevel: "+isHeaderLevel);
		
		Page<InvoiceItemReportModel> invoiceItemReportPage = invoiceItemReportPageableService.getPagedInvoiceItemReport(invoiceTypeId,invoiceReportRequestDTO.getFinancialYearId(), invoiceReportRequestDTO.getTransactionFromDate(), invoiceReportRequestDTO.getTransactionToDate(), invoiceReportRequestDTO.getPartyId(), partyTypeIds, invoiceReportRequestDTO.getMaterialId(), isHeaderLevel, invoiceReportRequestDTO.getCreatedBy(), pageNumber, pageSize, sortDir, sortColumn); 
		

		List<InvoiceItemReportModel> invoiceItemReport = invoiceItemReportPage.getContent();
		invoiceItemReport.forEach(data ->{
			log.info(data.getGrandTotal()+" = grandtotal"+data.getInvoiceHeaderId()+"= id ");
		});
		log.info(invoiceItemReport.toString()+"= value");
//		log.info("any items? "+ invoiceItemReport.size());
//		invoiceItemReport.forEach(rep -> {
//			log.info("report : "+rep);
//		});
		
		//Long totalCount = invoiceItemReportRepository.getReportTotalCount(invoiceReportRequestDTO.getFinancialYearId(), invoiceReportRequestDTO.getTransactionFromDate(), invoiceReportRequestDTO.getTransactionToDate(), invoiceReportRequestDTO.getPartyId(), partyTypeIds);

		Long totalCount = invoiceItemReportPage.getTotalElements();

		
		log.info("total count: "+totalCount);
		
		InvoiceItemReportDTO invoiceItemReportDTO = new InvoiceItemReportDTO();
		
		invoiceItemReportDTO.setInvoiceReportItems(invoiceItemReport);
		invoiceItemReportDTO.setTotalCount(totalCount);
		
		return new ResponseEntity<InvoiceItemReportDTO>(invoiceItemReportDTO, HttpStatus.OK);
	}

	@PostMapping("/invoice/tax-report/{invoiceTypeId}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
	public ResponseEntity<InvoiceTaxReportDTO> getInvoiceTaxReport(
			@PathVariable("invoiceTypeId") Long invoiceTypeId,
			@PathVariable("pageNumber") Integer pageNumber,
			@PathVariable("pageSize") Integer pageSize,
			@PathVariable("sortDir") String sortDir,
			@PathVariable("sortColumn") String sortColumn,
			@RequestBody TransactionReportRequestDTO invoiceReportRequestDTO){
		log.info("incoming request: "+invoiceReportRequestDTO);
		log.info("incoming request from dt: "+invoiceReportRequestDTO.getTransactionFromDate());
		log.info("incoming request to dt: "+invoiceReportRequestDTO.getTransactionToDate());
		log.info("incoming request mat id: "+invoiceReportRequestDTO.getMaterialId());
		log.info("incoming request party: "+invoiceReportRequestDTO.getPartyId());
		log.info("incoming request fin : "+invoiceReportRequestDTO.getFinancialYearId());
		log.info("incoming request party type: "+invoiceReportRequestDTO.getPartyTypeIds());
		
		List<Long> partyTypeIds = null;
		
		if(invoiceReportRequestDTO.getPartyTypeIds().isEmpty()){
			PartyType partyTypeCustomer = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
			PartyType partyTypeSupplier = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);
			PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
			
			List<PartyType> partyTypeList = new ArrayList<PartyType>();
			
			partyTypeList.add(partyTypeCustomer);
			partyTypeList.add(partyTypeSupplier);
			partyTypeList.add(partyTypeBoth);
			
			//List<Long> partyTypeIds = new ArrayList<Long>();
			
			partyTypeIds = partyTypeList.stream()
							.map(partyType -> partyType.getId())
							.collect(Collectors.toList());
			
		}
		else{
			
			partyTypeIds = invoiceReportRequestDTO.getPartyTypeIds();
			
		}
		log.info("party type ids: "+partyTypeIds);
		
		log.info("sortColumn: "+sortColumn);
		if(sortColumn.equals("partyName"))
			sortColumn = "party.name";
		
		TransactionType invoiceType = transactionTypeService.getModelById(invoiceTypeId);
		
		//List<InvoiceTaxReport> invoiceTaxReport = invoiceTaxReportPageableService.getPagedInvoiceTaxReport(invoiceType.getId(), invoiceReportRequestDTO.getFinancialYearId(), invoiceReportRequestDTO.getTransactionFromDate(), invoiceReportRequestDTO.getTransactionToDate(), invoiceReportRequestDTO.getPartyId(),invoiceReportRequestDTO.getMaterialId(), partyTypeIds, pageNumber, pageSize, sortDir, sortColumn);
		
		//Long totalCount = invoiceTaxReportPageableService.getInvoiceTaxReportCount(invoiceType.getId(),invoiceReportRequestDTO.getFinancialYearId(), invoiceReportRequestDTO.getTransactionFromDate(), invoiceReportRequestDTO.getTransactionToDate(), invoiceReportRequestDTO.getPartyId(), invoiceReportRequestDTO.getMaterialId(),partyTypeIds);
		
		
		InvoiceTaxReportDTO invoiceTaxReportDTO = invoiceTaxReportPageableService.getPagedInvoiceTaxReport(invoiceType.getId(), invoiceReportRequestDTO.getFinancialYearId(), invoiceReportRequestDTO.getTransactionFromDate(), invoiceReportRequestDTO.getTransactionToDate(), invoiceReportRequestDTO.getPartyId(),invoiceReportRequestDTO.getMaterialId(), partyTypeIds, pageNumber, pageSize, sortDir, sortColumn);
		
		
		//invoiceTaxReportDTO.setInvoiceTaxReports(invoiceTaxReport);
		//invoiceTaxReportDTO.setTotalCount(totalCount);
		
		return new ResponseEntity<InvoiceTaxReportDTO>(invoiceTaxReportDTO, HttpStatus.OK);
	}
	
	
	@GetMapping("/invoice/party/{id}/{invoiceType}")
	public ResponseEntity<List<InvoiceDTO>> getByParty(@PathVariable("id") Long id, @PathVariable("invoiceType") String invoiceType){
		
		log.info("invoiceType"+invoiceType);
		Party party = partyMapper.dtoToModelMap(partyService.getPartyById(id));
		
		TransactionType transactionType = transactionTypeService.getByName(invoiceType);
		
//		List<Status> statuses = Arrays.asList(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DC_GENERATED), statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DC_GENERATED), statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED));
		List<Status> statuses = new ArrayList<Status>(); 
				
		//statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PURCHASE_ORDER_STATUS_DELETED));
		statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.PROFORMA_INVOICE_STATUS_INVOICED));

		
		List<InvoiceDTO> invoiceListOut = invoiceService.getByPartyAndTransactionTypeAndStatusNotIn(party,invoiceType,statuses);
		log.info("Returning invoice list");
		log.info(invoiceListOut);
		return new ResponseEntity<List<InvoiceDTO>>(invoiceListOut, HttpStatus.OK);
	}
	
	@GetMapping("/invoice-note/party/{id}/{invoiceType}")
	public ResponseEntity<List<InvoiceDTO>> getByPartyForNote(@PathVariable("id") Long id, @PathVariable("invoiceType") String invoiceType){
		
		log.info("invoiceType getByPartyForNote"+invoiceType);
		Party party = partyMapper.dtoToModelMap(partyService.getPartyById(id));
		
		TransactionType transactionType = transactionTypeService.getByName(invoiceType);
		
		List<Status> statuses = new ArrayList<Status>(); 
				
		statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.INVOICE_STATUS_CANCELLED));
		statuses.add(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.INVOICE_STATUS_NOTE_CREATED));

		log.info("party,invoiceType,statuses "+party+invoiceType+statuses);
		List<InvoiceDTO> invoiceListOut = invoiceService.getByPartyAndTransactionTypeAndStatusNotIn(party,invoiceType,statuses);
		log.info("Returning invoice list");
		log.info(invoiceListOut);
		return new ResponseEntity<List<InvoiceDTO>>(invoiceListOut, HttpStatus.OK);
	}

	@GetMapping("/check-invNumber/{invTypeId}/{partyId}")
	public ResponseEntity<TransactionResponseDTO> checkpoNumber(
			@PathVariable("invTypeId") Long invTypeId
			, @PathVariable("partyId") Long partyId
			, @RequestParam(value = "invNumber", required=false) String invNumber){
		
		TransactionType invType = transactionTypeService.getModelById(invTypeId);
		
		Long count = invoiceService.countByInvoiceNumberAndTransactionType(invNumber, invType, partyId);
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (count > 0){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("invNumber  "+invNumber+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("invNumber available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}
	
	
	@GetMapping("/invoice/count")
	public Long getInvoiceCount(){
		Long invoiceCount = invoiceService.getTotalCountByInvoiceNumber();
		return invoiceCount;
		
		
	}
	
	
	@PostMapping("/check-cancelled-deleted-invoices")
	public ResponseEntity<List<String>> checkCancelledOrDeletedInvoices(@RequestBody List<String> invoiceIds){
		
		List<String> returnInvoiceIds = new ArrayList<String>();
		
		invoiceIds.forEach(invId -> {
			if(invId != null) {
				InvoiceHeader invoice = invoiceService.getModelById(invId);
				if(invoice == null) {
					returnInvoiceIds.add(invId);
				}
				else if(invoice.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {
					returnInvoiceIds.add(invId);
				}
			}
		});
		
		
		return new ResponseEntity<List<String>> (returnInvoiceIds, HttpStatus.OK);
		
	}

	@PostMapping("/auto-transaction")
	@Transactional
	public  ResponseEntity<AutoTransactionListDTO> saveAutoTransaction(@RequestBody AutoTransactionListDTO autoTransactionListDTO){

		AutoTransactionGenerationDTO autoTransactionGenerationDTO=autoTransactionListDTO.getAutoTransactionGenerationDTO();

		AutoTransactionGenerationDTO autoTransactionGenerationOutDTO= invoiceService.saveAutoTransactionGeneration(autoTransactionGenerationDTO);
        AutoTransactionListDTO autoTransactionListOutDTO = new AutoTransactionListDTO();

		autoTransactionListOutDTO.setAutoTransactionGenerationDTO(autoTransactionGenerationOutDTO);
		return new ResponseEntity<AutoTransactionListDTO>(autoTransactionListOutDTO, HttpStatus.OK);
	}



    @GetMapping("/invoice-list/{partyId}/{transactionTypeId}")
    ResponseEntity<List<InvoiceDTO>> getAllInvoice(@PathVariable("partyId") Long partyId, @PathVariable("transactionTypeId") Long transactionTypeId){


        Long partyIds=partyId;
        Long transactionType = transactionTypeId;
        List<InvoiceDTO> invoiceListOut = invoiceService.getAllInvoiceForCustomer(partyId,transactionType);


     return    new ResponseEntity<List<InvoiceDTO>>(invoiceListOut, HttpStatus.OK);

    }



	@DeleteMapping("/delete-transaction/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteTransaction(@PathVariable("id") Long id){

		TransactionResponseDTO deleteStatus = invoiceService.deleteTransaction(id);
		log.info("returning delete message: "+deleteStatus);
		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
	}





	@GetMapping("/recent-transaction/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<RecentAutoTransactionDTO> getRecentAutoTransaction(
			@PathVariable("sortColumn") String sortColumn
			, @PathVariable("sortDirection") String sortDirection
			, @PathVariable("pageNumber") Integer pageNumber
			, @PathVariable("pageSize") Integer pageSize
			, @RequestParam(value = "searchText", required=false) String searchText	) {
		log.info("in recent");
		//log.info("VoucherTypeId: "+PettyCashTypeId);
		log.info("searchText: " + searchText);
		log.info("searchText.isEmpty(): " + searchText.isEmpty());
		log.info("searchText.length(): " + searchText.length());
		log.info("searchText == null : " + searchText == null ? "NULL" : "NOT NULL");
		log.info("searchText.trim(): " + searchText.trim());

		log.info("sortColumn: " + sortColumn);
		log.info("sort direction: " + sortDirection);
		log.info("pageNumber: " + pageNumber);
		log.info("pageSize: " + pageSize);

		Long totalCount = 0l;
		List<AutoTransactionGeneration> autoTransactionGenerations = new ArrayList<AutoTransactionGeneration>();

		RecentAutoTransactionDTO recentAutoTransactionDTO = new RecentAutoTransactionDTO();

		//TransactionType PettyCashType = transactionTypeService.getModelById(PettyCashTypeId);

		if (searchText.isEmpty() || searchText.equals("null")) {
			log.info("empty search text");
			Page<AutoTransactionGeneration> recentAutoTransactionGenerations = recentAutoTransactionService.getPage(sortColumn, sortDirection, pageNumber, pageSize);
			autoTransactionGenerations.addAll(recentAutoTransactionGenerations.getContent());
			log.info(recentAutoTransactionGenerations.getContent()+"recentPetty size " + autoTransactionGenerations.size());
			totalCount = recentAutoTransactionGenerations.getTotalElements();

		} else {

			Page<AutoTransactionGeneration> recentAutoTransactionGenerations = recentAutoTransactionService.getPageByInvoiceNumber(searchText, sortColumn, sortDirection, pageNumber, pageSize);
			autoTransactionGenerations.addAll(recentAutoTransactionGenerations.getContent());

			totalCount = recentAutoTransactionGenerations.getTotalElements();


			//  recentPettyCashes = recentPettyCashService.getPageByPaidTo(PettyCashType, searchText, sortColumn, sortDirection, pageNumber, pageSize);

		//	autoTransactionGenerations.addAll(recentAutoTransactionGenerations.getContent());
			//totalCount += recentAutoTransactionGenerations.getTotalElements();


			try {
				Date searchDate = new SimpleDateFormat("dd/MM/yyyy").parse(searchText);

				log.info("parsed date: " + searchDate);

				recentAutoTransactionGenerations = recentAutoTransactionService.getPageByStartDate(searchDate, sortColumn, sortDirection, pageNumber, pageSize);

				autoTransactionGenerations.addAll(recentAutoTransactionGenerations.getContent());
				totalCount += recentAutoTransactionGenerations.getTotalElements();
				log.info("totalCount after date: " + totalCount);

			} catch (ParseException e) {
				log.info("incorrect date format: " + searchText + " expected dd/MM/yyyy");
			}

			//searchDate.s

			log.info("non empty search text");
		}

		recentAutoTransactionDTO.setAutoTransactionGenerationDTOS(autoTransactionMapper.modelToDTOList(autoTransactionGenerations));
		recentAutoTransactionDTO.setTotalCount(totalCount);


		return new ResponseEntity<RecentAutoTransactionDTO>(recentAutoTransactionDTO, HttpStatus.OK);
	}



	@PostMapping("/stop-transaction")
	public  ResponseEntity<AutoTransactionGenerationDTO> stopAutoTransaction(@RequestBody AutoTransactionGenerationDTO autoTransactionGenerationDTO){
		AutoTransactionGenerationDTO autoTransactionGenerationDTOOut =invoiceService.stopTransaction(autoTransactionGenerationDTO);

		return new ResponseEntity<AutoTransactionGenerationDTO>(autoTransactionGenerationDTOOut, HttpStatus.OK);

	}



	@GetMapping("/get-auto/{id}")
	public AutoTransactionGenerationDTO getAutoTransaction(@PathVariable("id") String invoiceId){
		log.info("in getInvoice: "+invoiceId);
	AutoTransactionGenerationDTO autoTransactionGenerationDTO=invoiceService.getAutoTransction(invoiceId);

		return autoTransactionGenerationDTO;
	}


	@GetMapping("/get-auto-log/{id}")
	public ResponseEntity<TransactionResponseDTO> getAutoTransactionLog(@PathVariable("id") String invoiceId){
		log.info("in getInvoice: "+invoiceId);
		TransactionResponseDTO transactionResponseDTO=invoiceService.getAutoTransctionLog(invoiceId);

		return new ResponseEntity<TransactionResponseDTO>(transactionResponseDTO, HttpStatus.OK);
	}

	@GetMapping("/get-auto-cancel/{id}")
	public ResponseEntity<TransactionResponseDTO> getAutoTransactionforInvoice(@PathVariable("id") String invoiceId){
		log.info("in getInvoice: "+invoiceId);
		TransactionResponseDTO transactionResponseDTO  =new TransactionResponseDTO();
		String invoice=invoiceId;
		AutoTransactionGeneration autoTransactionGeneration =autoTransactionRepository.getAutoGeneration(invoice,1);
		if(autoTransactionGeneration!=null && autoTransactionGeneration.getId()!=null){
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("Cannot Delete Invoice!! It has been used in Recuring Invoice IF you want to delete Please Stop the Recuring Invoice ");


		}
		else{
			transactionResponseDTO.setResponseStatus(0);

		}









		return new ResponseEntity<TransactionResponseDTO>(transactionResponseDTO, HttpStatus.OK);
	}


	@GetMapping("/get-due-amount/{party}")
	public PartyDueAmountDTO getDueAmount(@PathVariable("party") String name){
		PartyDueAmountDTO invoiceDueAmount = invoiceService.dueAmount(name);
		return invoiceDueAmount;


	}


	@GetMapping("/send-email/{party}")
	public String sendEmail(@PathVariable("party") String name){
		String  send= invoiceService.sendDueAmountEmail(name);
		return send;
	}



}
