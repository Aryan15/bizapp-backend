package com.coreerp.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.PartyType;
import com.coreerp.service.TransactionTypeService;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.FinancialYearRepository;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.reports.dao.DashboardTransactionSummaryRepository;
import com.coreerp.reports.model.DashboardTransactionSummary;



@RestController
@RequestMapping("/api")
public class DashboardController {

	final static Logger log=LogManager.getLogger(DashboardController.class);
	
	@Autowired
	DashboardTransactionSummaryRepository dashboardTransactionSummaryRepository;
	
	@Autowired
	FinancialYearRepository financialYearRepository;
	
	@Autowired
	TransactionTypeRepository transactionTypeRepository;

	@Autowired
	TransactionTypeService transactionTypeService;
	
	@GetMapping("/invoice-payments-for-month/{monthCount}")
	public ResponseEntity<List<DashboardTransactionSummary>> getInvoiceWithPayments(@PathVariable Long monthCount){
		
		log.info("getInvoiceWithPayments: {monthCount}"+monthCount);
		
		LocalDate now = LocalDate.now();
		LocalDate monthsBehind = now.minusMonths(monthCount);
		
		LocalDate firstDateMonthsBehind = LocalDate.of(monthsBehind.getYear(), monthsBehind.getMonth(), 1);
		
		log.info("now: "+now);
		log.info("monthsBehind: "+monthsBehind);
		log.info("firstDateMonthsBehind: "+firstDateMonthsBehind);
		
		
		Date toDate = java.sql.Date.valueOf(now);
		Date fromDate = java.sql.Date.valueOf(firstDateMonthsBehind);
		
		log.info("toDate: "+toDate);
		log.info("fromDate: "+fromDate);
		
		Long finacialYearId = null; //financialYearRepository.findByIsActive(1).getId();
		Long jobworkInvoiceType=0l;
		Long subcontratInvoiceType=0l;

		Integer isJobworkAvailable = transactionTypeService.getJwTransactionType();
		Long customerInvoiceType = transactionTypeRepository.findByName(ApplicationConstants.INVOICE_TYPE_CUSTOMER).getId();
		Long purchaseInvoiceType = transactionTypeRepository.findByName(ApplicationConstants.INVOICE_TYPE_SUPPLIER).getId();
		if (isJobworkAvailable>0) {
			jobworkInvoiceType = transactionTypeRepository.findByName(ApplicationConstants.INVOICE_TYPE_INCOMING_JW_INVOICE).getId();
			subcontratInvoiceType = transactionTypeRepository.findByName(ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE).getId();
		}
		List<Long> transactionTypeIds = Arrays.asList(customerInvoiceType, purchaseInvoiceType,jobworkInvoiceType,subcontratInvoiceType);
		
		String canceledStatus = ApplicationConstants.INVOICE_STATUS_CANCELLED;
		
		List<String> statuses = Arrays.asList(canceledStatus);
		
		List<DashboardTransactionSummary> dashboardTransactionSummaryList =dashboardTransactionSummaryRepository.getInvoiceAndPendingPayment(finacialYearId, fromDate, toDate, statuses, transactionTypeIds);

		log.info("dashboardTransactionSummaryList="+dashboardTransactionSummaryList.toString());
		return new ResponseEntity<List<DashboardTransactionSummary>>(dashboardTransactionSummaryList, HttpStatus.OK);
		
	}
	
	
	@GetMapping("/pay-receive-for-month/{monthCount}/{sourceType}")
	public ResponseEntity<List<DashboardTransactionSummary>> getPayReceive(@PathVariable Long monthCount,@PathVariable String sourceType){
		
		log.info("getInvoiceWithPayments: {monthCount}"+monthCount);
		
		LocalDate now = LocalDate.now();
		LocalDate monthsBehind = now.minusMonths(monthCount);
		
		LocalDate firstDateMonthsBehind = LocalDate.of(monthsBehind.getYear(), monthsBehind.getMonth(), 1);
		
		log.info("now: "+now);
		log.info("monthsBehind: "+monthsBehind);
		log.info("firstDateMonthsBehind: "+firstDateMonthsBehind);
		
		
		Date toDate = java.sql.Date.valueOf(now);
		Date fromDate = java.sql.Date.valueOf(firstDateMonthsBehind);
		
		log.info("toDate: "+toDate);
		log.info("fromDate: "+fromDate);
		
		Long finacialYearId = null; //financialYearRepository.findByIsActive(1).getId();
		
		Long customerReceiptsTypeId = transactionTypeRepository.findByName(ApplicationConstants.PR_CUSTOMER_RECEIVABLE).getId();
		Long supplierPaymentsTypeId = transactionTypeRepository.findByName(ApplicationConstants.PR_SUPPLIER_PAYABLE).getId();
		Integer isJobworkAvailable = transactionTypeService.getJwTransactionType();
		List<Long> transactionTypeIds = Arrays.asList(customerReceiptsTypeId, supplierPaymentsTypeId);
		Long salesId=0l;
		Long purchaseId =0l;


		if(sourceType.equalsIgnoreCase(ApplicationConstants.PR_SOURCE_SP) ){
             salesId = transactionTypeRepository.findByName(ApplicationConstants.INVOICE_TYPE_CUSTOMER).getId();
             purchaseId = transactionTypeRepository.findByName(ApplicationConstants.INVOICE_TYPE_SUPPLIER).getId();

        }

		if(isJobworkAvailable>0) {
		 if (sourceType.equalsIgnoreCase(ApplicationConstants.PR_SOURCE_JS)) {
				salesId = transactionTypeRepository.findByName(ApplicationConstants.INVOICE_TYPE_INCOMING_JW_INVOICE).getId();
				purchaseId = transactionTypeRepository.findByName(ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE).getId();

			}
		}

        List< Long > sourceTransactionIds = Arrays.asList(salesId,purchaseId);

		String canceledStatus = ApplicationConstants.PR_STATUS_CANCELLED;
		String draftStatus = ApplicationConstants.PR_STATUS_DRAFT;
		
		List<String> statuses = Arrays.asList(canceledStatus, draftStatus);
		
		List<DashboardTransactionSummary> dashboardTransactionSummaryList =dashboardTransactionSummaryRepository.getPayReceive(finacialYearId, fromDate, toDate, statuses, transactionTypeIds,sourceTransactionIds);
		log.info("getPayReceive="+dashboardTransactionSummaryList.toString());
		return new ResponseEntity<List<DashboardTransactionSummary>>(dashboardTransactionSummaryList, HttpStatus.OK);
		
	}
	
	@GetMapping("/post-dated-cheques")
	public ResponseEntity<List<DashboardTransactionSummary>> getPostDatedCheques(){
		
		log.info("getPostDatedCheques");
		
		
		
		Long finacialYearId = null; //financialYearRepository.findByIsActive(1).getId();
		
		Long supplierPayableType = transactionTypeRepository.findByName(ApplicationConstants.PR_SUPPLIER_PAYABLE).getId();
		Long customerReceivableType = transactionTypeRepository.findByName(ApplicationConstants.PR_CUSTOMER_RECEIVABLE).getId();
		
		
		List<Long> transactionTypeIds = Arrays.asList(supplierPayableType, customerReceivableType);
		
		String status = ApplicationConstants.TRANSACTION_COMPLETED;
		String paymentMethodName = ApplicationConstants.PAYMENT_METHOD_CHECQUE_OR_DD;
		List<DashboardTransactionSummary> dashboardTransactionSummaryList =dashboardTransactionSummaryRepository.getPostDatedCheques(finacialYearId, transactionTypeIds, status, paymentMethodName);
		log.info("dashboardTransactionSummaryList="+dashboardTransactionSummaryList.toString());
		return new ResponseEntity<List<DashboardTransactionSummary>>(dashboardTransactionSummaryList, HttpStatus.OK);
		
	}
	
	
	@GetMapping("/open-quotations/{monthCount}")
	public ResponseEntity<List<DashboardTransactionSummary>> getOpenQuotations(@PathVariable Long monthCount){
		
		log.info("getOpenQuotations");
		
		
		
		Long finacialYearId = null; // financialYearRepository.findByIsActive(1).getId();
		
		Long quotationType = transactionTypeRepository.findByName(ApplicationConstants.QUOTATION_TYPE_CUSTOMER).getId();
		
		LocalDate now = LocalDate.now();
		LocalDate monthsBehind = now.minusMonths(monthCount);
		
		LocalDate firstDateMonthsBehind = LocalDate.of(monthsBehind.getYear(), monthsBehind.getMonth(), 1);
		
		log.info("now: "+now);
		log.info("monthsBehind: "+monthsBehind);
		log.info("firstDateMonthsBehind: "+firstDateMonthsBehind);
		
		
		Date toDate = java.sql.Date.valueOf(now);
		Date fromDate = java.sql.Date.valueOf(firstDateMonthsBehind);
		
		log.info("toDate: "+toDate);
		log.info("fromDate: "+fromDate);
		
		List<Long> transactionTypeIds = Arrays.asList(quotationType);
		
		String newStatus = ApplicationConstants.QUOTATION_STATUS_NEW;
		String openStatus = ApplicationConstants.QUOTATION_STATUS_OPEN;
		List<String> statuses = Arrays.asList(newStatus, openStatus);
		
		List<DashboardTransactionSummary> dashboardTransactionSummaryList 
		=dashboardTransactionSummaryRepository.getOpenQuotations(finacialYearId, transactionTypeIds, statuses, fromDate, toDate);
		log.info("dashboardTransactionSummaryList="+dashboardTransactionSummaryList.toString());
		return new ResponseEntity<List<DashboardTransactionSummary>>(dashboardTransactionSummaryList, HttpStatus.OK);
		
	}
	
	
	@GetMapping("/open-po/{monthCount}")
	public ResponseEntity<List<DashboardTransactionSummary>> getOpenPurchaseOrders(@PathVariable Long monthCount){
		
		log.info("getOpenPurchaseOrders");
		
		
		
		Long finacialYearId = null; // financialYearRepository.findByIsActive(1).getId();
		
		Long purchaseOrderType = transactionTypeRepository.findByName(ApplicationConstants.PURCHASE_ORDER_TYPE_CUSTOMER).getId();
		
		LocalDate now = LocalDate.now();
		LocalDate monthsBehind = now.minusMonths(monthCount);
		
		LocalDate firstDateMonthsBehind = LocalDate.of(monthsBehind.getYear(), monthsBehind.getMonth(), 1);
		
		log.info("now: "+now);
		log.info("monthsBehind: "+monthsBehind);
		log.info("firstDateMonthsBehind: "+firstDateMonthsBehind);
		
		
		Date toDate = java.sql.Date.valueOf(now);
		Date fromDate = java.sql.Date.valueOf(firstDateMonthsBehind);
		
		log.info("toDate: "+toDate);
		log.info("fromDate: "+fromDate);
		
		List<Long> transactionTypeIds = Arrays.asList(purchaseOrderType);
		
		String newStatus = ApplicationConstants.PURCHASE_ORDER_STATUS_NEW;
		
		List<String> statuses = Arrays.asList(newStatus);
		
		List<DashboardTransactionSummary> dashboardTransactionSummaryList =dashboardTransactionSummaryRepository.getOpenPurchaseOrders(finacialYearId, transactionTypeIds, statuses, fromDate, toDate);
		log.info("dashboardTransactionSummaryList="+dashboardTransactionSummaryList.toString());
		return new ResponseEntity<List<DashboardTransactionSummary>>(dashboardTransactionSummaryList, HttpStatus.OK);
		
	}
}
