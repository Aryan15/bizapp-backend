package com.coreerp.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dao.ProcessRepository;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.Process;

@RestController
@RequestMapping("/api")
public class ProcessController {

	final static Logger log = LogManager.getLogger(ProcessController.class);
	
	
	@Autowired
	ProcessRepository processRepository;
	
	@PostMapping("/process")
	public ResponseEntity<Process> save(@RequestBody Process process){
		
		log.info("incoming model: "+process);
		
		return new ResponseEntity<Process>(processRepository.save(process), HttpStatus.OK);
		
	}
	
	
	
	@GetMapping("/process/{id}")
	public ResponseEntity<Process> getOne(@PathVariable("id") Long id){
		
		log.info("incoming id: "+id);
		
		return new ResponseEntity<Process>(processRepository.getOne(id), HttpStatus.OK);
		
	}
	
	@GetMapping("/processes")
	public ResponseEntity<List<Process>> getAllProcesses() {
		
		log.info("In getAllProcesses");
		List<Process> processList = processRepository.findAll();
		
		log.info("processList.size(): "+ processList.size());
		
		return new ResponseEntity<List<Process>>(processList, HttpStatus.OK);
	}
	
	@PostMapping("/processes")
	public ResponseEntity<List<Process>> processSave(@RequestBody List<Process> processList){
		
		log.info("in processSave: "+processList);
		List<Process> outProcesses = new ArrayList<Process>();
		
		for(Process process : processList){
			if(!process.getName().isEmpty()){
				process.setDescription(process.getName());
				Process p = processRepository.save(process);
				outProcesses.add(p);				
			}

		}
		
		return new ResponseEntity<List<Process>>(outProcesses, HttpStatus.OK);
		
	}
	
	
	@DeleteMapping("/process/{ids}")
	public ResponseEntity<TransactionResponseDTO> delete(@PathVariable List<Long> ids){
		
		TransactionResponseDTO response = new TransactionResponseDTO();
		
		response.setResponseStatus(1);
		response.setResponseString("Deleted Successfully");
		
		try{
			ids.forEach(id -> {
				processRepository.deleteById(id);
			});
			
		}catch(Exception e){
			log.info("Error deleteing.."+e.getMessage());
			response.setResponseStatus(1);
			response.setResponseString(e.getMessage());
		}
		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	}
	
}
