package com.coreerp.controller;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.coreerp.service.CityService;

@RestController
@RequestMapping("/api")
public class CityController {

	final static Logger log=LogManager.getLogger(CityController.class);
	
	@Autowired
	CityService cityService;
	
//	@PostMapping("/city")
//	public ResponseEntity<City> citySave(@RequestBody CityDTO cityDTO){
//		log.info("In citySave: name="+cityDTO.getName());
//		City city=cityService.saveCity(cityDTO);
//		log.info("City has been saved");
//		
//		return new ResponseEntity<City>(city,HttpStatus.OK);
//	}
	
//	@PostMapping("/cities")
//	public ResponseEntity<List<City>> citiesSave(@RequestBody List<CityDTO> cityDTOList){
//		
//		log.info("in citiesSave: "+cityDTOList);
//		List<City> outCities=new ArrayList<City>();
//		
//		for(CityDTO cityDTO:cityDTOList){
//			if(!cityDTO.getName().isEmpty()){
//				City city=cityService.saveCity(cityDTO);
//				outCities.add(city);
//			}
//		}
//		
//		return new ResponseEntity<List<City>>(outCities,HttpStatus.OK);
//	}
//	
//	@GetMapping("/city")
//	public ResponseEntity<List<CityDTO>>getAllCities(){
//		
//		log.info("In getAllCities");
//		List<CityDTO> cityDTOList=cityService.getAllCities();
//		log.info("cityDTOList.size():"+cityDTOList.size());
//		return new ResponseEntity<List<CityDTO>>(cityDTOList,HttpStatus.OK);
//	}
}
