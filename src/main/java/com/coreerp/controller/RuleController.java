package com.coreerp.controller;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.kie.api.event.rule.AgendaEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.ApplicationConstants;
import com.coreerp.TrackingAgendaEventListener;
import com.coreerp.dto.TransactionStatusRule;


@RestController
public class RuleController {

	final static Logger log = LogManager.getLogger(RuleController.class);
	
	@Autowired
    private KieContainer kieContainer;
    
	
    //private KieSession kieSession ;
    
	@GetMapping("/auth/rule")
	public ResponseEntity<TransactionStatusRule> testRule(){
		
		KieSession kieSession = kieContainer.newKieSession("rulesSession");
		
		TransactionStatusRule transactionStatusRule = new TransactionStatusRule();
		
		transactionStatusRule.setQuantity(5.0);
		transactionStatusRule.setTransactionType(ApplicationConstants.TRANSACTION_SUPPLIER_PURCHASE_ORDER);
		
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
		
		kieSession.addEventListener(agendaEventListener);

		
        kieSession.insert(transactionStatusRule);

        int ruleCount = kieSession.fireAllRules();
        
        log.info("rules count: "+ruleCount);
        

        return new ResponseEntity<TransactionStatusRule>(transactionStatusRule, HttpStatus.OK);
	}
	
}
