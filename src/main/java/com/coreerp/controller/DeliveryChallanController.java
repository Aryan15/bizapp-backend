package com.coreerp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.model.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.ApplicationConstants;
import com.coreerp.dto.DeliveryChallanDTO;
import com.coreerp.dto.DeliveryChallanHeaderWrapperDTO;
import com.coreerp.dto.DeliveryChallanItemReportDTO;
import com.coreerp.dto.RecentDeliveryChallanDTO;
import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.DeliveryChallanHeaderMapper;
import com.coreerp.mapper.PartyMapper;
import com.coreerp.reports.model.DeliveryChallanItemModel;
import com.coreerp.reports.service.DeliveryChallanItemReportPageableService;
import com.coreerp.reports.service.RecentDeliveryChallanService;
import com.coreerp.service.DeliveryChallanService;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.PartyService;
import com.coreerp.service.PartyTypeService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TransactionTypeService;

@RestController
@RequestMapping("/api")
public class DeliveryChallanController {

	final static Logger log = LogManager.getLogger(DeliveryChallanController.class);
	
	@Autowired 
	DeliveryChallanService deliveryChallanService;
	
	@Autowired
	DeliveryChallanItemReportPageableService deliveryChallanItemReportPageableService;
	
	@Autowired
	PartyTypeService partyTypeService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	PartyMapper partyMapper;
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	RecentDeliveryChallanService recentDeliveryChallanService;
	
	@Autowired
	DeliveryChallanHeaderMapper deliveryChallanHeaderMapper;
	
	@Autowired
	FinancialYearService financialYearService;
	
	@GetMapping("/dc/{id}")
	public ResponseEntity<DeliveryChallanDTO> getDc(@PathVariable("id") String id){
		
		DeliveryChallanDTO deliveryChallanDTO = deliveryChallanService.getById(id);
		return new ResponseEntity<DeliveryChallanDTO>(deliveryChallanDTO, HttpStatus.OK);
	}
	
	@GetMapping("/dc")
	public ResponseEntity<List<DeliveryChallanDTO>> getAll(){
		
		List<DeliveryChallanDTO> deliveryChallanList = deliveryChallanService.getAll();
		
		return new ResponseEntity<List<DeliveryChallanDTO>>(deliveryChallanList,HttpStatus.OK );
		
	}
	
	@GetMapping("/dc/party/{partyId}/{dcType}")
	public ResponseEntity<List<DeliveryChallanDTO>> getDCByParty(@PathVariable("partyId") Long partyId,
							@PathVariable("dcType") String dcType){
		
		
		Party party = partyMapper.dtoToModelMap(partyService.getPartyById(partyId));
		
		TransactionType transactionType = transactionTypeService.getByName(dcType);
		
		List<Status> statuses = Arrays.asList( statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_INVOICED), statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_DELETED));
		
		
		
		List<DeliveryChallanDTO> deliveryChallanList = deliveryChallanService.getByPartyAndStatusNotIn(party,statuses);//getByParty(partyId);
		
		return new ResponseEntity<List<DeliveryChallanDTO>>(deliveryChallanList,HttpStatus.OK );
	}

	@GetMapping("/dc/party-ttype/{partyId}/{transactionTypeName}/{t-type}")
	public ResponseEntity<List<DeliveryChallanDTO>> getDCByPartyAndTransactionTypeWithNonZeroBalanceQuantity(
																		@PathVariable("partyId") Long partyId,
																		@PathVariable("transactionTypeName") String transactionTypeName,
																		 @PathVariable("t-type") String Ttype){
		
		TransactionType transactionType = transactionTypeService.getByName(transactionTypeName);

		TransactionType typeOfTransaction = transactionTypeService.getByName(Ttype);
		log.info("partyId: "+partyId+" transactionTypeName: "+transactionTypeName + " typeOfTransaction: "+ typeOfTransaction.getName());


		List<Status> statuses = Arrays.asList( statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_INVOICED)
				,statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_CANCELLED));

         if(typeOfTransaction.getName().equals(ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE))
		 {
			 statuses =Arrays.asList(statusService.getStatusByTransactionTypeAndName(transactionType,ApplicationConstants.DC_STATUS_INVOICED));
		 }

		
		if(typeOfTransaction.getName().equals(ApplicationConstants.GRN_TYPE_SUPPLIER))
		{
			statuses = Arrays.asList(
					statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_GRN_GENEREATED)
					,statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_DELETED)
					,statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_INVOICED)
					,statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_PARTIALLY_INVOICED)
			);
		}
		
		if(typeOfTransaction.getName().equals(ApplicationConstants.INCOMING_JOBWORK_OUT_DC))
		{
			statuses = Arrays.asList(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_OUTDC_GENEREATED),statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_DELETED));
		}
		
		if(typeOfTransaction.getName().equals(ApplicationConstants.OUTGOING_JOBWORK_IN_DC))
		{
			statuses = Arrays.asList(statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_INDC_GENEREATED),statusService.getStatusByTransactionTypeAndName(transactionType, ApplicationConstants.DC_STATUS_DELETED));
		}
		

		
		List<DeliveryChallanDTO> deliveryChallanList = deliveryChallanService.getByPartyAndTransactionTypeWithNonZeroBalanceQuantityAndStatusNotIn(partyId, transactionTypeName, statuses, typeOfTransaction);
		
		log.info("returning deliveryChallanList: "+deliveryChallanList);
		log.info("returning deliveryChallanList: "+deliveryChallanList);

		return new ResponseEntity<List<DeliveryChallanDTO>>(deliveryChallanList,HttpStatus.OK );
	}
	
	@PostMapping("/dc")
	@Transactional
	public ResponseEntity<DeliveryChallanDTO> save(@RequestBody DeliveryChallanHeaderWrapperDTO deliveryChallanHeaderWrapperDTO ){
		
		
		
		
		return new ResponseEntity<DeliveryChallanDTO>(deliveryChallanService.save(deliveryChallanHeaderWrapperDTO),HttpStatus.OK);
	}
	
	@GetMapping("/dc/dc-no/{dcNumber}")
	public ResponseEntity<List<DeliveryChallanDTO>> getByDCNumber(@PathVariable("dcNumber") String dcNumber){
		
		return new ResponseEntity<List<DeliveryChallanDTO>>(deliveryChallanService.getByDCNumber(dcNumber), HttpStatus.OK);
	}

	
	@GetMapping("/dc/recent/{deliveryChallanTypeId}/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<RecentDeliveryChallanDTO> getRecentDC(
												  @PathVariable("deliveryChallanTypeId") Long deliveryChallanTypeId
												, @PathVariable("sortColumn") String sortColumn
												, @PathVariable("sortDirection") String sortDirection
												, @PathVariable("pageNumber") Integer pageNumber
												, @PathVariable("pageSize") Integer pageSize
												, @RequestParam(value = "searchText", required=false) String searchText												){
		log.info("searchText: "+searchText);
		log.info("searchText.isEmpty(): "+searchText.isEmpty());
		log.info("searchText.length(): "+searchText.length());
		log.info("searchText == null : "+searchText == null ? "NULL" : "NOT NULL");
		log.info("searchText.trim(): "+searchText.trim());
		
		log.info("sortColumn: "+sortColumn);
		log.info("sort direction: "+sortDirection);
		log.info("pageNumber: " +pageNumber);
		log.info("pageSize: "+pageSize);
		Long totalCount = 0l;
		List<DeliveryChallanHeader> deliveryChallanHeaders = new ArrayList<DeliveryChallanHeader>();
		
		RecentDeliveryChallanDTO recentDeliveryChallanDTO = new RecentDeliveryChallanDTO();
		
		TransactionType dcType = transactionTypeService.getModelById(deliveryChallanTypeId);

		FinancialYear financialYear = financialYearService.findByIsActive(1);

		if (searchText.isEmpty() || searchText.equals("null")) {
			searchText=null;
		}
		Page<DeliveryChallanHeader> recentDc = recentDeliveryChallanService.getDcPageList(dcType, financialYear, searchText, sortColumn, sortDirection, pageNumber, pageSize);
		deliveryChallanHeaders.addAll(recentDc.getContent());
		totalCount = recentDc.getTotalElements();

		recentDeliveryChallanDTO.setDeliveryChallanHeaders(deliveryChallanHeaderMapper.modelToDTOList(deliveryChallanHeaders));
		recentDeliveryChallanDTO.setTotalCount(totalCount);
		
		
		return new ResponseEntity<RecentDeliveryChallanDTO>(recentDeliveryChallanDTO, HttpStatus.OK);
		
	}
	
	@PostMapping("/dc/item-report/{deliveryChallanTypeId}/{pageNumber}/{pageSize}/{sortDir}/{sortColumn}")
	public ResponseEntity<DeliveryChallanItemReportDTO> getDeliveryChallanItemReport(
			@PathVariable("deliveryChallanTypeId") Long deliveryChallanTypeId,
			@PathVariable("pageNumber") Integer pageNumber,
			@PathVariable("pageSize") Integer pageSize,
			@PathVariable("sortDir") String sortDir,
			@PathVariable("sortColumn") String sortColumn,
			@RequestBody TransactionReportRequestDTO transactionReportRequestDTO){
		log.info("incoming request: "+transactionReportRequestDTO);
		log.info("incoming request: "+transactionReportRequestDTO.getTransactionFromDate());
		log.info("incoming request: "+transactionReportRequestDTO.getTransactionToDate());
		log.info("incoming request: "+transactionReportRequestDTO.getMaterialId());
		log.info("incoming request: "+transactionReportRequestDTO.getPartyId());
		log.info("incoming request: "+transactionReportRequestDTO.getFinancialYearId());
		log.info("incoming request: "+transactionReportRequestDTO.getPartyTypeIds());
		
		List<Long> partyTypeIds = null;
		
		if(transactionReportRequestDTO.getPartyTypeIds().isEmpty()){
			PartyType partyTypeCustomer = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
			PartyType partyTypeSupplier = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);
			PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
			
			List<PartyType> partyTypeList = new ArrayList<PartyType>();
			
			partyTypeList.add(partyTypeCustomer);
			partyTypeList.add(partyTypeSupplier);
			partyTypeList.add(partyTypeBoth);
			
			//List<Long> partyTypeIds = new ArrayList<Long>();
			
			partyTypeIds = partyTypeList.stream()
							.map(partyType -> partyType.getId())
							.collect(Collectors.toList());
			
		}
		else{
			
			partyTypeIds = transactionReportRequestDTO.getPartyTypeIds();
			
		}
		log.info("party type ids: "+partyTypeIds);
		
		log.info("sortColumn: "+sortColumn);
		if(sortColumn.equals("partyName"))
			sortColumn = "party.name";
		
		List<DeliveryChallanItemModel> deliveryChallanItemReport = deliveryChallanItemReportPageableService.getPagedDeliveryChallanItemReport(deliveryChallanTypeId,transactionReportRequestDTO.getFinancialYearId(), transactionReportRequestDTO.getTransactionFromDate(), transactionReportRequestDTO.getTransactionToDate(), transactionReportRequestDTO.getPartyId(),transactionReportRequestDTO.getMaterialId(), partyTypeIds, pageNumber, pageSize, sortDir, sortColumn);
		
		log.info("any items? "+ deliveryChallanItemReport.size());
		deliveryChallanItemReport.forEach(rep -> {
			log.info("report : "+rep);
		});
		
		//Long totalCount = invoiceItemReportRepository.getReportTotalCount(invoiceReportRequestDTO.getFinancialYearId(), invoiceReportRequestDTO.getTransactionFromDate(), invoiceReportRequestDTO.getTransactionToDate(), invoiceReportRequestDTO.getPartyId(), partyTypeIds);
		
		Long totalCount = deliveryChallanItemReportPageableService.getDeliveryChallanItemReportCount(deliveryChallanTypeId,transactionReportRequestDTO.getFinancialYearId(), transactionReportRequestDTO.getTransactionFromDate(), transactionReportRequestDTO.getTransactionToDate(), transactionReportRequestDTO.getPartyId(), partyTypeIds, transactionReportRequestDTO.getMaterialId());
		
		
		log.info("total count: "+totalCount);
		
		DeliveryChallanItemReportDTO deliveryChallanItemReportDTO = new DeliveryChallanItemReportDTO();
		
		deliveryChallanItemReportDTO.setDeliveryChallanReportItems(deliveryChallanItemReport);
		deliveryChallanItemReportDTO.setTotalCount(totalCount);
		
		return new ResponseEntity<DeliveryChallanItemReportDTO>(deliveryChallanItemReportDTO, HttpStatus.OK);
	}
	
//	@DeleteMapping("/dc/delete-item/{id}")
//	@Transactional
//	public ResponseEntity<TransactionResponseDTO> deleteItemById(@PathVariable("id") String id){
//		
//		TransactionResponseDTO deleteStatus = deliveryChallanService.deleteItem(id);
//		log.info("returning delete message: "+deleteStatus);
//		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
//		
//	}
	
	@DeleteMapping("/dc/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteById(@PathVariable("id") String id){
		TransactionResponseDTO deleteStatus = deliveryChallanService.delete(id);
		log.info("returning delete message: "+deleteStatus);
		return new ResponseEntity<TransactionResponseDTO>(deleteStatus, HttpStatus.OK);
		
	}
	
	@GetMapping("/check-dcNumber/{partyId}/{dcTypeId}")
	public ResponseEntity<TransactionResponseDTO> checkpoNumber(
			@PathVariable("dcTypeId") Long dcTypeId
			, @PathVariable("partyId") Long partyId
			, @RequestParam(value = "dcNumber", required=false) String dcNumber){
		
		TransactionType dcType = transactionTypeService.getModelById(dcTypeId);
		
		Long count = deliveryChallanService.countByDeliveryChallanNumberAndTransactionType(dcNumber, dcType, partyId);
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (count > 0){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("dcNumber  "+dcNumber+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("dcNumber available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}
	
}
