package com.coreerp.controller;

import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dto.MaterialPriceListDTO;
import com.coreerp.service.MaterialPriceListService;

@RestController
@RequestMapping("/api")
public class MaterialPriceListController {
	
	final static Logger log = LogManager.getLogger(MaterialPriceListController.class);
	
	@Autowired
	MaterialPriceListService materialPriceListService;

	
	@PostMapping("/material-price-list")
	public ResponseEntity<List<MaterialPriceListDTO>> save(@RequestBody List<MaterialPriceListDTO> materialPriceListDTOList){
		
		log.info("incoming dto: "+materialPriceListDTOList);
		
		return new ResponseEntity<List<MaterialPriceListDTO>>(materialPriceListService.saveList(materialPriceListDTOList), HttpStatus.OK);
		
	}
	
	@GetMapping("/material-price-list/{partyId}")
	public ResponseEntity<List<MaterialPriceListDTO>> getForParty(@PathVariable("partyId") Long partyId){
		
		log.info("incoming party id: "+partyId);
		
		return new ResponseEntity<List<MaterialPriceListDTO>>(materialPriceListService.getForParty(partyId), HttpStatus.OK);
		
	}
}
