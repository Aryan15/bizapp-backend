package com.coreerp.controller;


import com.coreerp.dto.SettingsWrapperDTO;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dto.GlobalSettingDTO;
import com.coreerp.service.GlobalSettingService;

@RestController
@RequestMapping("/api")
public class GlobalSettingController {

	final static Logger log= LogManager.getLogger(GlobalSettingController.class);
	
	@Autowired
	GlobalSettingService globalSettingService;

	
	
//	@PostMapping("/global-setting")
//	public ResponseEntity<GlobalSettingDTO> globalSettingSave(@RequestBody GlobalSettingDTO globalSettingDTO){
//		//log.info("In areaSave: name="+GlobalSettingDTO.getName());
//		GlobalSettingDTO globalSetting=globalSettingService.save(globalSettingDTO);
//	   log.info("Area has been saved");
//	   
//	   return new ResponseEntity<GlobalSettingDTO>(globalSetting,HttpStatus.OK);
//	}
	
//	@PostMapping("/global-settings")
//	public ResponseEntity<List<GlobalSettingDTO>> globalSettingsSave(@RequestBody List<GlobalSettingDTO> globalSettingDTOList){
//		
//		log.info("in globalSettingsSave: "+globalSettingDTOList);
//		List<GlobalSettingDTO> outGlobalSetting=new ArrayList<GlobalSettingDTO>();
//		
//		for(GlobalSettingDTO globalSettingDTO :globalSettingDTOList){
//			/*if(!areaDTO.getName().isEmpty()){*/
//			GlobalSettingDTO globalSetting=globalSettingService.save(globalSettingDTO);
//			outGlobalSetting.add(globalSetting);
//			/*}*/
//		}
//		
//		return new ResponseEntity<List<GlobalSettingDTO>>(outGlobalSetting,HttpStatus.OK);
//	}
	
	@GetMapping("/global-setting")
	public ResponseEntity<GlobalSettingDTO> getAll(){
		
		log.info("in getAll");
		GlobalSettingDTO globalSettingsDTO=globalSettingService.getAllGlobalSettings().get(0);// getAllGlobalSettings();
		return new ResponseEntity<GlobalSettingDTO>(globalSettingsDTO, HttpStatus.OK);
	}

	@GetMapping("/settings")
	public ResponseEntity<SettingsWrapperDTO> getAllSettings(){

		log.info("in getAll");
		SettingsWrapperDTO settingsWrapperDTO=globalSettingService.getAllGlobalSettingsList();// getAllGlobalSettings();
		return new ResponseEntity<SettingsWrapperDTO>(settingsWrapperDTO, HttpStatus.OK);
	}


}
