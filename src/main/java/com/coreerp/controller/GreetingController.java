package com.coreerp.controller;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.coreerp.dto.QuotationDTO;

@Controller
public class GreetingController {
	
	final static Logger log = LogManager.getLogger(GreetingController.class);
	
	@RequestMapping(value="/quotation", method=RequestMethod.GET)
	public String quotationPage(Model model){
		log.info("in quotationPage");
		
		QuotationDTO quotationDTO = new QuotationDTO();
		model.addAttribute("quotation", quotationDTO);
		return "quotation.page";
	}

}
