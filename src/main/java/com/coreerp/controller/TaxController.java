package com.coreerp.controller;

import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dao.TaxTypeRepository;
import com.coreerp.dto.TaxDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.TaxType;
import com.coreerp.service.TaxService;

@RestController
@RequestMapping("/api")
public class TaxController {
	
	final static Logger log = LogManager.getLogger(TaxController.class);
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	TaxTypeRepository taxTypeRepository;
	
	@GetMapping("/taxes")
	public ResponseEntity<List<TaxDTO>> getAll(){
		
		log.info("in getAll");
		List<TaxDTO> taxes= taxService.getAll();
		log.info("returning taxes: "+taxes);
		return new ResponseEntity<List<TaxDTO>>(taxes, HttpStatus.OK);
	}
	
	
	@GetMapping("/tax-types")
	public ResponseEntity<List<TaxType>> getAllTaxTypes(){
		
		List<TaxType> taxTypes = taxTypeRepository.findAll();
		
		return new ResponseEntity<List<TaxType>>(taxTypes, HttpStatus.OK);
	}
	
	@DeleteMapping("/taxes/{id}")
	public ResponseEntity<TransactionResponseDTO> delete(@PathVariable("id") Long id){
		
		return new ResponseEntity<TransactionResponseDTO>(taxService.delete(id), HttpStatus.OK);
	}

}
