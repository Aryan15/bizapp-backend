package com.coreerp.controller;


 import com.coreerp.ApplicationConstants;
 import com.coreerp.dto.*;
 import com.coreerp.mapper.EwayBillTrackingMapper;
import com.coreerp.model.EwayBillTracking;
import com.coreerp.model.InvoiceHeader;
 import com.coreerp.model.Material;
 import com.coreerp.reports.model.EWayBillReport;
 import com.coreerp.reports.model.EwayBillRecentReport;
 import com.coreerp.reports.service.RecentEWayBillService;
import com.coreerp.service.EwayBillGenerationService;
import com.coreerp.service.EwayBillTrackingService;
import com.coreerp.service.InvoiceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class EwayBillGenerationController {
    final static Logger log = LogManager.getLogger(EwayBillGenerationController.class);

    @Autowired
    EwayBillGenerationService ewayBillGenerationService;

    @Autowired
    EwayBillTrackingService ewayBillTrackingService;

    @Autowired
    EwayBillTrackingMapper ewayBillTrackingMapper;



    @Autowired
    RecentEWayBillService recentEWayBillService;

    @PostMapping("/eway-bill-generation")
    public ResponseEntity<List<EwayBillInvoiceResponseDTO>> generateEWayBill(@RequestBody EwayBillTrackingDTO ewayBillTrackingDTO){

        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTOList1 = ewayBillGenerationService.generateEwayBill(ewayBillTrackingDTO);
        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTOList= ewayBillInvoiceResponseDTOList1;


        return new ResponseEntity<List<EwayBillInvoiceResponseDTO>> (ewayBillInvoiceResponseDTOList, HttpStatus.OK);

    }

    @PostMapping("/cancel-eway-bill")
    public ResponseEntity<List<EwayBillInvoiceResponseDTO>> cancelEWayBill(@RequestBody EwayBillTrackingDTO ewayBillTrackingDTO){

        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTOList=ewayBillGenerationService.cancelEwayBill(ewayBillTrackingDTO);

        return new ResponseEntity<List<EwayBillInvoiceResponseDTO>> (ewayBillInvoiceResponseDTOList, HttpStatus.OK);

    }

    @PostMapping("/eway-bill-vehicle-update")
    public ResponseEntity<List<EwayBillInvoiceResponseDTO>> EWayBillVehicleUpdate(@RequestBody EwayBillTrackingDTO ewayBillTrackingDTO){

        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTOList=ewayBillGenerationService.ewayBillVehicleUpdate(ewayBillTrackingDTO);

        return new ResponseEntity<List<EwayBillInvoiceResponseDTO>> (ewayBillInvoiceResponseDTOList, HttpStatus.OK);

    }
    @PostMapping("/eway-bill-transporterId-update")
    public ResponseEntity<List<EwayBillInvoiceResponseDTO>> EWayBillTransporterIDUpdate(@RequestBody EwayBillTrackingDTO ewayBillTrackingDTO){

        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTOList=ewayBillGenerationService.ewayBillTransporterIdUpdate(ewayBillTrackingDTO);

        return new ResponseEntity<List<EwayBillInvoiceResponseDTO>> (ewayBillInvoiceResponseDTOList, HttpStatus.OK);

    }


    @PostMapping("/eway-bill-extend-validity")
    public ResponseEntity<List<EwayBillInvoiceResponseDTO>> EWayBillExtendValidity(@RequestBody EwayBillTrackingDTO ewayBillTrackingDTO){

        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTOList=ewayBillGenerationService.extendValidityOfEwayBill(ewayBillTrackingDTO);

        return new ResponseEntity<List<EwayBillInvoiceResponseDTO>> (ewayBillInvoiceResponseDTOList, HttpStatus.OK);

    }

    @PostMapping("/eway-bill-number")
    public ResponseEntity<List<EwayBillInvoiceResponseDTO>> getEWayBillByNumber(@RequestBody String eWayBillNum){

        List<EwayBillInvoiceResponseDTO> ewayBillInvoiceResponseDTOList=ewayBillGenerationService.getEwayBillDetailsByNumber(eWayBillNum);

        return new ResponseEntity<List<EwayBillInvoiceResponseDTO>> (ewayBillInvoiceResponseDTOList, HttpStatus.OK);

    }
    @GetMapping("/eway-bill/{eWayBillNum}")
    public ResponseEntity<EwayBillTrackingDTO> getEwayBillDetailsByEwayNum(@PathVariable("eWayBillNum") String eWayBillNo){
        EwayBillTracking ewayBillTracking = ewayBillTrackingService.getByEWayBillNo(eWayBillNo);
        EwayBillTrackingDTO ewayBillTrackingDTO = ewayBillTrackingMapper.modelToDTOMap(ewayBillTracking);
        log.info("getEwayBillDetailsByEwayNum  :"+ewayBillTrackingDTO);

        return new ResponseEntity<EwayBillTrackingDTO>(ewayBillTrackingDTO , HttpStatus.OK);
    }

    @GetMapping("/eway-bill/invoice/{id}")
    public ResponseEntity<EwayBillTrackingDTO> getCurrentEwayBillByInvoiceId(@PathVariable("id") String invoiceId){
        ArrayList<Integer> statusId = new ArrayList<Integer>();
        statusId.add(ApplicationConstants.EWAY_BILL_ACTIVE);
        EwayBillTracking ewayBillTracking = ewayBillTrackingService.getActiveEwayBillByInvoiceHeader(invoiceId);
        EwayBillTrackingDTO ewayBillTrackingDTO = ewayBillTrackingMapper.modelToDTOMap(ewayBillTracking);
       // log.info("getEwayBillDetailsByInvId  :"+ewayBillTrackingDTO);

        return new ResponseEntity<EwayBillTrackingDTO>(ewayBillTrackingDTO , HttpStatus.OK);
    }

    @GetMapping("/eway-bills/invoice/{id}")
    public ResponseEntity<List<EwayBillTrackingDTO>> getEwayBillDetailsByInvoiceId(@PathVariable("id") String invoiceId){

        List<EwayBillTracking> ewayBillTrackingList = ewayBillTrackingService.getEwayBillDetailsByInvoiceHeaderAndStatus(invoiceId);
        List<EwayBillTrackingDTO> ewayBillTrackingDTOList= ewayBillTrackingMapper.modelToDTOList(ewayBillTrackingList);

        //log.info("getEwayBillDetails  :"+ewayBillTrackingDTOList);

        return new ResponseEntity<List<EwayBillTrackingDTO>>(ewayBillTrackingDTOList , HttpStatus.OK);
    }

    @GetMapping("/eway-bill/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
    public ResponseEntity<EWayBillRecentDTO> getEwayBillRecentDetails( @PathVariable("sortColumn") String sortColumn
            , @PathVariable("sortDirection") String sortDirection
            , @PathVariable("pageNumber") Integer pageNumber
            , @PathVariable("pageSize") Integer pageSize
            , @RequestParam(value="searchText", required=false) String searchText){
        log.info("sortDirection: "+sortDirection);
        log.info("sortColumn: "+sortColumn);

         Long totalCount = 0l;
        Page<EwayBillRecentReport> ewayBillReportPage = null;

        EWayBillRecentDTO eWayBillRecentDTO = new EWayBillRecentDTO();
        List<EwayBillRecentReport> ewayBillReportModels = new ArrayList<EwayBillRecentReport>();


        if (searchText.isEmpty() || searchText.equals("null")) {
            searchText=null;
        }
        ewayBillReportPage = recentEWayBillService.getPage(searchText, sortColumn, sortDirection, pageNumber, pageSize);
        ewayBillReportModels.addAll(ewayBillReportPage.getContent());
        totalCount = ewayBillReportPage.getTotalElements();
        log.info("1 total count: "+totalCount);


        eWayBillRecentDTO.seteWayBillRecentReports(ewayBillReportModels);
        eWayBillRecentDTO.setTotalCount(totalCount);
        log.info("ewayBillReportModels  :"+ewayBillReportModels);

        return new ResponseEntity<EWayBillRecentDTO>(eWayBillRecentDTO , HttpStatus.OK);
    }

    @PostMapping("/eway-bill-report/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
    public ResponseEntity<EWayBillReportDTO> getEwayBillDetails( @PathVariable("sortColumn") String sortColumn
            , @PathVariable("sortDirection") String sortDirection
            , @PathVariable("pageNumber") Integer pageNumber
            , @PathVariable("pageSize") Integer pageSize
            ,@RequestBody TransactionReportRequestDTO invoiceReportRequestDTO){
        log.info("sortDirection: "+sortDirection);
        log.info("sortColumn: "+sortColumn);

        if(sortColumn.equals("invoiceDate"))
            sortColumn = "inh.invoiceDate";
        else if(sortColumn.equals("invoiceNumber"))
            sortColumn = "inh.invoiceNumber";
        else if(sortColumn.equals("eWayBillValidity"))
            sortColumn = "validUpto";


        Long totalCount = 0l;
        Page<EWayBillReport> ewayBillReportPage = null;

        EWayBillReportDTO eWayBillReportDTO = new EWayBillReportDTO();
        List<EWayBillReport> ewayBillReportModels = new ArrayList<EWayBillReport>();

        ewayBillReportPage = recentEWayBillService.getReport(invoiceReportRequestDTO.getTransactionFromDate(), invoiceReportRequestDTO.getTransactionToDate(), invoiceReportRequestDTO.getPartyId(), sortColumn, sortDirection, pageNumber, pageSize);

        ewayBillReportModels.addAll(ewayBillReportPage.getContent());
        totalCount = ewayBillReportPage.getTotalElements();
        log.info("1 total count: "+totalCount);

        ewayBillReportModels.forEach(rep -> {
            log.info("rep : "+rep.getHsnCode());
        });

        eWayBillReportDTO.setEwayBillReports(ewayBillReportModels);
        eWayBillReportDTO.setTotalCount(totalCount);
        log.info("eWayBillReportDTO  :"+ewayBillReportModels);

        return new ResponseEntity<EWayBillReportDTO>(eWayBillReportDTO , HttpStatus.OK);
    }
}
