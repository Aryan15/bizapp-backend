package com.coreerp.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dto.BankDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.service.BankService;

@RestController
@RequestMapping("/api")
public class BankController {
	
	final static Logger log = LogManager.getLogger(BankController.class);
	
	@Autowired
	BankService bankService;
	
	
	@PostMapping("/bank")
	public ResponseEntity<BankDTO> save(@RequestBody BankDTO bankDTO){
		
		log.info("incoming dto: "+bankDTO);
		
		return new ResponseEntity<BankDTO>(bankService.save(bankDTO), HttpStatus.OK);
		
	}
	
	@GetMapping("/bank/{id}")
	public ResponseEntity<BankDTO> getOne(@PathVariable("id") Long id){
		
		log.info("incoming id: "+id);
		
		return new ResponseEntity<BankDTO>(bankService.getById(id), HttpStatus.OK);
		
	}
	
	@GetMapping("/banks")
	public ResponseEntity<List<BankDTO>> getAllBanks() {
		
		log.info("In getAllBanks");
		List<BankDTO> bankDTOList = bankService.getAll();
		
		log.info("bankDTOList.size(): "+ bankDTOList.size());
		
		return new ResponseEntity<List<BankDTO>>(bankDTOList, HttpStatus.OK);
	}
	
	@PostMapping("/banks")
	@Transactional
	public ResponseEntity<List<BankDTO>> banksSave(@RequestBody List<BankDTO> bankDTOList){
		
		log.info("in banksSave: "+bankDTOList);
		List<BankDTO> outBanks = new ArrayList<BankDTO>();
		
		for(BankDTO bankDTO : bankDTOList){
			if(!bankDTO.getBankname().isEmpty()){
				BankDTO bank = bankService.save(bankDTO);
				outBanks.add(bank);				
			}

		}
		
		return new ResponseEntity<List<BankDTO>>(outBanks, HttpStatus.OK);
		
	}
	
	
	@DeleteMapping("/bank/{ids}")
	public ResponseEntity<TransactionResponseDTO> delete(@PathVariable List<Long> ids){
		
		log.info("delete: "+ids);
		TransactionResponseDTO response = new TransactionResponseDTO();
		
		response.setResponseStatus(1);
		response.setResponseString("Deleted Successfully");
		
		try{
			ids.forEach(id -> {
				try{
				bankService.deleteBankById(id);
				} catch(Exception e){
					log.info("Error deleteing 1.."+e.getMessage());
					response.setResponseStatus(0);
					response.setResponseString("Cannot delete. bank already used");
				}
			});
			
			log.info("DELETED");
			
		}
		
		catch(Exception e){
			log.info("Error deleteing.."+e.getMessage());
			response.setResponseStatus(0);
			response.setResponseString(e.getMessage());
		}
		
		log.info("returning: "+response);
		return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
	}
	

}
