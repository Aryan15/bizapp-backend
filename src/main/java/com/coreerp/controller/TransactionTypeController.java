package com.coreerp.controller;

import java.util.List;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.dto.TransactionTypeDTO;
import com.coreerp.model.TransactionType;
import com.coreerp.service.TransactionNumberService;
import com.coreerp.service.TransactionTypeService;

@RestController
@RequestMapping("/api")
public class TransactionTypeController {

	final static Logger log = LogManager.getLogger(TransactionTypeController.class);

	@Autowired
	TransactionTypeService transactionTypeService;

	@Autowired
	TransactionNumberService transactionNumberService;

	@GetMapping("transaction-type")
	public ResponseEntity<List<TransactionTypeDTO>> getAll() {

		return new ResponseEntity<List<TransactionTypeDTO>>(transactionTypeService.gtAll(), HttpStatus.OK);
	}

	@GetMapping("transaction-type/{id}")
	public ResponseEntity<TransactionTypeDTO> getById(@PathVariable("id") Long id) {
		return new ResponseEntity<TransactionTypeDTO>(transactionTypeService.getById(id), HttpStatus.OK);
	}


	@GetMapping("/transaction-number/{transactionTypeName}")
	public ResponseEntity<TransactionResponseDTO> getInvoiceNumber(@PathVariable("transactionTypeName") String transactionTypeName) {

		//TransactionType transactionType = transactionTypeService.getByName(transactionTypeName);

		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();

		TransactionType transactionType = transactionTypeService.getByName(transactionTypeName);

		transactionResponseDTO.setResponseString(transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(transactionType.getId())).getTransactionNumber());

		return new ResponseEntity<TransactionResponseDTO>(transactionResponseDTO, HttpStatus.OK);
	}

	@GetMapping("/jw-trnsaction")
	public ResponseEntity<TransactionResponseDTO> getJwTransactionType() {

		Integer response = transactionTypeService.getJwTransactionType();
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		if (response>0) {

			transactionResponseDTO.setResponseStatus(1);
		} else {
			transactionResponseDTO.setResponseStatus(0);
		}


		return new ResponseEntity<TransactionResponseDTO>(transactionResponseDTO, HttpStatus.OK);

	}
}