package com.coreerp.controller;

import java.util.ArrayList;
import java.util.List;

import com.coreerp.dao.PrintTemplateRepository;
import com.coreerp.dto.CompanyEmailDTO;
import com.coreerp.dto.EmailDTO;
import com.coreerp.model.*;
import com.coreerp.service.EmailService;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.RoleRepository;
import com.coreerp.dao.StockUpdateOnRepository;
import com.coreerp.dto.GlobalSettingDTO;
import com.coreerp.dto.NumberRangeConfigurationDTO;
import com.coreerp.dto.PrintCopyDTO;
import com.coreerp.dto.SettingsWrapperDTO;
import com.coreerp.dto.StockUpdateOnDTO;
import com.coreerp.dto.TaxDTO;
import com.coreerp.dto.TermsAndConditionDTO;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.mapper.StockUpdateOnMapper;
import com.coreerp.service.GlobalSettingService;
import com.coreerp.service.NumberRangeConfigurationService;
import com.coreerp.service.PartyTypeService;
import com.coreerp.service.PrintCopiesService;
import com.coreerp.service.TaxService;
import com.coreerp.service.TermsAndConditionService;
import com.coreerp.service.TransactionTypeService;
import com.coreerp.service.UnitOfMeasurementService;

@RestController
@RequestMapping("/api")
public class SettingsWrapperController {

	final static Logger log= LogManager.getLogger(SettingsWrapperController.class);
	
	@Autowired
	GlobalSettingService globalSettingService;
	
	@Autowired
	NumberRangeConfigurationService numberRangeConfigurationService;
	
//	@Autowired
//	TermsAndConditionService termsAndConditionService;
//	
	@Autowired
	TaxService taxService;
	
	@Autowired
	UnitOfMeasurementService unitOfMeasurementService;
	
	@Autowired
	TermsAndConditionService termsAndConditionService;
	
	@Autowired
	RoleRepository roleRepository;

	
	@Autowired
	StockUpdateOnRepository stockUpdateOnRepository;
	
	@Autowired
	StockUpdateOnMapper stockUpdateOnMapper;
	
	@Autowired
	TransactionTypeService transactionTypeService ;
	
	@Autowired
	PartyTypeService partyTypeService;
	
	@Autowired
	PrintCopiesService printCopiesService;

	@Autowired
	PrintTemplateRepository printTemplateRepository;

	@Autowired
	EmailService emailService;
	
	@GetMapping("/settings-wrapper")
	public ResponseEntity<SettingsWrapperDTO> getSettings(){
		
		SettingsWrapperDTO out  = new SettingsWrapperDTO();
		
		GlobalSettingDTO globalSettingDTO = globalSettingService.getAllGlobalSettings().get(0);
		
		List<NumberRangeConfigurationDTO> numberRangeConfigurations = numberRangeConfigurationService.getAll();
		
		List<TaxDTO> taxes = taxService.getAll();
		
		List<UnitOfMeasurement> unitOfMeasurements = unitOfMeasurementService.getAll();
		
		List<TermsAndConditionDTO> termsAndConditions = termsAndConditionService.getAll();
		
		List<Role> roles = roleRepository.findAll();

		//EmailDTO emailDTO = emailService.getAllEmailSettings();
		
		out.setGlobalSetting(globalSettingDTO);
		out.setNumberRangeConfigurations(numberRangeConfigurations);
		out.setTaxes(taxes);
		out.setUnitOfMeasurements(unitOfMeasurements);
		out.setTermsAndConditions(termsAndConditions);
		out.setRoles(roles);
		//out.setEmail(emailDTO);
		
		return new ResponseEntity<SettingsWrapperDTO>(out, HttpStatus.OK);
		
	}

	@PostMapping("/settings-wrapper")
	@Transactional
	public ResponseEntity<SettingsWrapperDTO> saveSettings(@RequestBody SettingsWrapperDTO dto ){
		
		log.info("In save tax "+dto.getTaxes().size());
		
		SettingsWrapperDTO out  = new SettingsWrapperDTO();
		
		GlobalSettingDTO globalSettingDTO = globalSettingService.save(dto.getGlobalSetting());

		//CompanyEmailDTO companyEmailDTO = globalSettingService.saveCompanyEmail(dto.getEmailDTO());
		
		List<NumberRangeConfigurationDTO> numberRangeConfigurations = numberRangeConfigurationService.saveAll(dto.getNumberRangeConfigurations());
		
		List<TaxDTO> taxes = new ArrayList<TaxDTO>();
		
		dto.getTaxes().forEach(tax -> {
			log.info("tax: "+tax.getName()+" : "+tax.getRate());
			taxes.add(taxService.saveTax(tax));
		});
		
		
		List<UnitOfMeasurement> unitOfMeasurements = new ArrayList<UnitOfMeasurement>();
		
		dto.getUnitOfMeasurements().forEach(uom -> {
			unitOfMeasurements.add(unitOfMeasurementService.save(uom));	
		});
				
		List<TermsAndConditionDTO> termsAndConditions = new  ArrayList<TermsAndConditionDTO>();
		
		dto.getTermsAndConditions().forEach(tnc -> {
			termsAndConditions.add(termsAndConditionService.save(tnc));
		});
		
		List<Role> roles = new ArrayList<Role>(); 
				
		dto.getRoles().forEach(role -> {
			roles.add(roleRepository.save(role));
		});

	 //EmailDTO emailDTO = emailService.save(dto.getEmail());
		
		out.setGlobalSetting(globalSettingDTO);
		out.setNumberRangeConfigurations(numberRangeConfigurations);
		out.setTaxes(taxes);
		out.setUnitOfMeasurements(unitOfMeasurements);
		out.setTermsAndConditions(termsAndConditions);
		out.setRoles(roles);
		//out.setEmail(emailDTO);
		
		return new ResponseEntity<SettingsWrapperDTO>(out, HttpStatus.OK);
		
	}
	
	
	
	@PostMapping("/print-copy")
	@Transactional
	public ResponseEntity<PrintCopyDTO> savePrintCopy(@RequestBody PrintCopyDTO printCopyDTO ){

		printCopyDTO.getPrintCopiesList().forEach(pc -> {
			log.info("printCopies : "+pc.getName());
			printCopiesService.save(pc);
		});

		return new ResponseEntity<PrintCopyDTO>(printCopyDTO, HttpStatus.OK);

	}
	
	@GetMapping("/pc")
	public ResponseEntity<List<PrintCopy>> getAllPrintCopies(){
		List<PrintCopy> pc = printCopiesService.getAll();
		return new ResponseEntity<List<PrintCopy>>(pc, HttpStatus.OK);
	}

	@GetMapping("/pt")
	public ResponseEntity<List<PrintTemplate>> getAllPrintTemplates(){
		List<PrintTemplate> pts = printTemplateRepository.findAll();

		return new ResponseEntity<>(pts, HttpStatus.OK);
	}
	@DeleteMapping("/print-copy-delete/{id}")
	public ResponseEntity<TransactionResponseDTO> deletePrintCopy(@PathVariable("id") Long id){
		log.info("in delete: "+id);
		TransactionResponseDTO del = printCopiesService.delete(id);
		
		return new ResponseEntity<TransactionResponseDTO>(del, HttpStatus.OK);
	}
	
	
	@GetMapping("/stock-increase-on")
	public ResponseEntity<List<StockUpdateOnDTO>> getAllStockUpdateOn(){
		
		List<PartyType> partyTypes = new ArrayList<PartyType>();
		
		PartyType partyTypeCustomer = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);
		PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
		
		
		partyTypes.add(partyTypeCustomer);
		partyTypes.add(partyTypeBoth);
		
		List<StockUpdateOnDTO> stockUpdateOnList = stockUpdateOnMapper.modelToDTOList(stockUpdateOnRepository.findByPartyTypeIn(partyTypes));
		
		return new ResponseEntity<List<StockUpdateOnDTO>>(stockUpdateOnList, HttpStatus.OK);
	}

	@GetMapping("/stock-decrease-on")
	public ResponseEntity<List<StockUpdateOnDTO>> getAllStockDecreaseOn(){
		
		List<PartyType> partyTypes = new ArrayList<PartyType>();
		
		PartyType partyTypeCustomer = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
		PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
		
		
		partyTypes.add(partyTypeCustomer);
		partyTypes.add(partyTypeBoth);
		
		List<StockUpdateOnDTO> stockUpdateOnList = stockUpdateOnMapper.modelToDTOList(stockUpdateOnRepository.findByPartyTypeIn(partyTypes));
		
		return new ResponseEntity<List<StockUpdateOnDTO>>(stockUpdateOnList, HttpStatus.OK);
	}
	
	@GetMapping("/terms-and-conditions")
	public ResponseEntity<List<TermsAndConditionDTO>> getAllTnCs(){
		
		List<TermsAndConditionDTO> tncList = termsAndConditionService.getAll();
		
		return new ResponseEntity<List<TermsAndConditionDTO>>(tncList, HttpStatus.OK);
		
	}
	
	@DeleteMapping("/terms-and-conditions/{id}")
	public ResponseEntity<TransactionResponseDTO> delete(@PathVariable("id") Long id){
		
		return new ResponseEntity<TransactionResponseDTO>(termsAndConditionService.delete(id), HttpStatus.OK);
	}

	@GetMapping("/terms-and-conditions/{transactionTypeId}")
	public ResponseEntity<TermsAndConditionDTO> getDefaultTnCByTransactionType(@PathVariable("transactionTypeId") Long transactionTypeId){
		
		TransactionType transactionType = transactionTypeService.getModelById(transactionTypeId);
		
		List<TermsAndConditionDTO> tncList = termsAndConditionService.getByTransactionTypeAndDefault(transactionType, 1);
		
		
		TermsAndConditionDTO tnc = new TermsAndConditionDTO();//termsAndConditionService.getByTransactionTypeAndDefault(transactionType, 1).get(0);
		
		if(tncList.size() > 0){
			tnc = tncList.get(0);
		}
		
		log.info("Returning tnc: "+tnc);
		
		return new ResponseEntity<TermsAndConditionDTO>(tnc, HttpStatus.OK);
		
	}
	
	@GetMapping("/terms-and-conditions-all/{transactionTypeId}")
	public ResponseEntity<List<TermsAndConditionDTO>> getTnCByTransactionType(@PathVariable("transactionTypeId") Long transactionTypeId){
		
		TransactionType transactionType = transactionTypeService.getModelById(transactionTypeId);
		
		List<TermsAndConditionDTO> tncList = termsAndConditionService.getByTransactionTypeAndDefault(transactionType, null);
		
		log.info("Returning tnc: "+tncList.size());
		
		return new ResponseEntity<List<TermsAndConditionDTO>>(tncList, HttpStatus.OK);
		
	}

	@GetMapping("/get-email")
	public ResponseEntity<EmailDTO> getEmail(){
		EmailDTO emailDTO = emailService.getAllEmailSettings();

		return new ResponseEntity<EmailDTO>(emailDTO, HttpStatus.OK);
	}


	@PostMapping("/email-save")
	@Transactional
	public ResponseEntity<EmailDTO> saveEmail(@RequestBody EmailDTO emailDTO ){

	  EmailDTO emailDTO1=emailService.save(emailDTO);

		return new ResponseEntity<EmailDTO>(emailDTO1, HttpStatus.OK);

	}




}
