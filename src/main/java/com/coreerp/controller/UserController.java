package com.coreerp.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import com.coreerp.dto.*;
import com.coreerp.model.*;
import com.coreerp.reports.model.InvoiceItemReportModel;
import com.coreerp.reports.model.VoucherReportModel;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.ActivityRepository;
import com.coreerp.dao.ActivityRoleBindingRepository;
import com.coreerp.dao.MenuRepository;
import com.coreerp.dao.RoleRepository;
import com.coreerp.dao.RoleTypeRepository;
import com.coreerp.dao.StatusBasedPermissionRepository;
import com.coreerp.dao.UserTenantRelationRepository;
import com.coreerp.jwt.JwtTokenUtil;
import com.coreerp.mapper.ActivityRoleBindingMapper;
import com.coreerp.mapper.CompanyMapper;
import com.coreerp.mapper.RoleMapper;
import com.coreerp.mapper.StatusBasedPermissionMapper;
import com.coreerp.mapper.UserMapper;
import com.coreerp.reports.service.UserReportService;
import com.coreerp.service.RoleService;
import com.coreerp.service.StatusService;
import com.coreerp.service.UserService;

@RestController
@RequestMapping("/api")
public class UserController {

	final static Logger log = LogManager.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserReportService userReportService;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	RoleTypeRepository roleTypeRepository;
	
	@Autowired
	MenuRepository menuRepository;
	
	@Autowired
	ActivityRepository activityRepository;
	
	@Autowired
	CompanyMapper companyMapper;
	
	@Autowired
	ActivityRoleBindingRepository activityRoleBindingRepository;
	
	@Autowired
	StatusBasedPermissionRepository statusBasedPermissionRepository;
	
	@Autowired
	StatusBasedPermissionMapper statusBasedPermissionMapper;
	
	@Autowired
	UserMapper userMapper;
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	ActivityRoleBindingMapper activityRoleBindingMapper;
	
	@Autowired
	UserTenantRelationRepository userTenantRelationRepository;
	
	@Autowired
	RoleMapper roleMapper;
	
	@Autowired
    JwtTokenUtil jwtTokenUtil;
	
    @Value("${jwt.header}")
    private String tokenHeader;
    
	
	@GetMapping("/users")
	public ResponseEntity<List<UserDTO>> getAllUsers(){
		return new ResponseEntity<List<UserDTO>>(userService.getAllUsers(), HttpStatus.OK);
	}
	
	@GetMapping("/user/recent/{sortColumn}/{sortDirection}/{pageNumber}/{pageSize}")
	public ResponseEntity<RecentUserDTO> getRecentUsers(
			  @PathVariable("sortColumn") String sortColumn
			, @PathVariable("sortDirection") String sortDirection
			, @PathVariable("pageNumber") Integer pageNumber
			, @PathVariable("pageSize") Integer pageSize
			, @RequestParam(value = "searchText", required=false) String searchText			
			){
		
		log.info("in getRecentUsers");
		
		log.info("searchText: "+searchText);
		log.info("searchText.isEmpty(): "+searchText.isEmpty());
		log.info("searchText.length(): "+searchText.length());
		log.info("searchText == null : "+searchText == null ? "NULL" : "NOT NULL");
		log.info("searchText.trim(): "+searchText.trim());
		
		log.info("sortColumn: "+sortColumn);
		log.info("sort direction: "+sortDirection);
		log.info("pageNumber: " +pageNumber);
		log.info("pageSize: "+pageSize);
		
		
		RecentUserDTO recentUserDTO = new RecentUserDTO();
		
		Long userCount = 0l;
		
		List<User> users = new ArrayList<User>();
		
		if(searchText.isEmpty() || searchText.equals("null"))	
		{
			log.info("empty search string");
			Page<User> userPage = userReportService.getPage(sortColumn, sortDirection, pageNumber, pageSize); 
			users.addAll(userPage.getContent());
			userCount = userPage.getTotalElements();
			
			//log.info("got users: "+users.size());
			//log.info("user count: "+ userCount);
			
		}else{
			
			log.info("NON empty search string");
			Page<User> userPage = userReportService.getPageByName(searchText, sortColumn, sortDirection, pageNumber, pageSize); 
			users.addAll(userPage.getContent());
			userCount = userPage.getTotalElements();
			
			userPage = userReportService.getPageByLastName(searchText, sortColumn, sortDirection, pageNumber, pageSize);
			
			users.addAll(userPage.getContent());
			userCount += userPage.getTotalElements();
			
			//log.info("got users: "+users.size());
			log.info("user count: "+ userCount);
			
		}
		
		recentUserDTO.setUsers(userMapper.modelToDTOList(users));
		recentUserDTO.setTotalCount(userCount);
		
		//log.info("recentUserDTO: "+recentUserDTO.getUsers().size());
		
		return new ResponseEntity<RecentUserDTO>(recentUserDTO, HttpStatus.OK);
	}
	
	@GetMapping("/user-roles")
	public ResponseEntity<List<RoleDTO>> getAllRoles(){
		
		RoleType roleType = roleTypeRepository.findByName(ApplicationConstants.ROLE_TYPE_SYSTEM);
		
		return new ResponseEntity<List<RoleDTO>>(roleMapper.modelToDTOList(roleRepository.findByRoleTypeNotIn(roleType)), HttpStatus.OK);
	}
	
	@GetMapping("/role-type")
	public ResponseEntity<List<RoleType>> getAllRoleTypes(){
		
		return new ResponseEntity<List<RoleType>>(roleTypeRepository.findByNameNotIn(new ArrayList<String>(Arrays.asList(ApplicationConstants.ROLE_TYPE_SYSTEM))), HttpStatus.OK);
	}
	
	@GetMapping("/check-username/{username}")
	public ResponseEntity<TransactionResponseDTO> checkUserName(@PathVariable("username") String username){
		
		UserTenantRelation userTenantRelation = userTenantRelationRepository.findByUsername(username);
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (userTenantRelation != null){
			
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("Username "+username+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("Username available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}
	
	@GetMapping("/check-email/{email}")
	public ResponseEntity<TransactionResponseDTO> checkEmail(@PathVariable("email") String email){
		
		log.info("Check email: "+email);
		User user = userService.findByEmail(email);
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (user != null){
			log.info("Email taken");
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("email  "+email+" already taken");
		}else{
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("email available");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}

	
	@Transactional
	@PostMapping("/user-role-mapping/{roleId}")
	public ResponseEntity<TransactionResponseDTO> saveUserRoleMapping(@RequestBody UserRoleWrapperDTO userRoleWrapperDTO, @PathVariable("roleId") Long roleId){
		
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		log.info("in saveUserRoleMapping: "+ userRoleWrapperDTO);
		
		transactionResponseDTO.setResponseStatus(1);
		transactionResponseDTO.setResponseString("Done");
		
		try{
			log.info("1");
//		userRoleWrapperDTO.getUserRoleMenus().forEach(userRoleMenu -> {
//			log.info("2: "+userRoleMenu.getMenuName());
//			
//			
//		});
			//Delete All enntries for roleId
			Role role = roleRepository.getOne(roleId);
			List<ActivityRoleBinding> arbDelete = activityRoleBindingRepository.findByRole(role);
			
			arbDelete.forEach(arbd -> {
				activityRoleBindingRepository.delete(arbd);
			});
			saveUserRoleMenuNew(userRoleWrapperDTO.getUserRoleMenus(), role);
			
		}catch(Exception e){
			log.info("4 ");
			log.error("Exception: "+e);
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("Error: "+e.getMessage());
		}
		log.info("Done. "+transactionResponseDTO);
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
	}
	
	
	private void saveUserRoleMenuNew(List<UserRoleMenuDTO> userRoleMenus, Role role){
		

		
		//Insert Fresh entries for passed role
		
		userRoleMenus.stream()
		.filter(urm -> urm.getIsRoleAssigned() == 1)
		.forEach(urm -> {
			urm.getActivities().stream()
			.filter(act -> act.getIsRoleAssigned() == 1)
			.forEach(act -> {
				//log.info("name: "+act.getActivityName());
				ActivityRoleBinding arbNew = new ActivityRoleBinding();
				Activity activity = activityRepository.getOne(act.getActivityId());
				arbNew.setActivity(activity);
				arbNew.setPermissionToApprove(act.getPermissionToApprove());
				arbNew.setPermissionToCancel(act.getPermissionToCancel());
				arbNew.setPermissionToCreate(act.getPermissionToCreate());
				arbNew.setPermissionToDelete(act.getPermissionToDelete());
				arbNew.setPermissionToPrint(act.getPermissionToPrint());
				arbNew.setPermissionToUpdate(act.getPermissionToUpdate());
				arbNew.setRole(role);
				arbNew.setDeleted("N");
				activityRoleBindingRepository.save(arbNew);
				log.info("act.getSubmenus(): "+act.getSubmenus().size());				
				if(act.getSubmenus() != null){
//					act.getSubmenus().forEach(sm -> {
//						log.info("sm: "+sm.getIsRoleAssigned());
//						log.info("sm: "+sm.getActivities().size());
//					});
					saveUserRoleMenuNew(act.getSubmenus(), role);
//					act.getSubmenus().stream()
//					.filter(sm -> sm.getIsRoleAssigned() == 1)
//					.forEach(sm -> {
//						sm.getActivities().stream()
//						.filter(ac -> act.getIsRoleAssigned() == 1)
//						.forEach(ac -> {
//							ActivityRoleBinding arbNew2 = new ActivityRoleBinding();
//							Activity activity2 = activityRepository.getOne(ac.getActivityId());
//							arbNew2.setActivity(activity2);
//							arbNew2.setPermissionToApprove(ac.getPermissionToApprove());
//							arbNew2.setPermissionToCancel(ac.getPermissionToCancel());
//							arbNew2.setPermissionToCreate(ac.getPermissionToCreate());
//							arbNew2.setPermissionToDelete(ac.getPermissionToDelete());
//							arbNew2.setPermissionToPrint(ac.getPermissionToPrint());
//							arbNew2.setPermissionToUpdate(ac.getPermissionToUpdate());
//							arbNew2.setRole(role);
//							arbNew2.setDeleted("N");
//							activityRoleBindingRepository.save(arbNew2);
//						});
//						;
//					});;
				}
			});;
		});;
		
	}
//	private void saveUserRoleMenu(UserRoleMenuDTO userRoleMenu){
//		try{
//			log.info("5 ");
//		userRoleMenu.getActivities().forEach(act -> {
//			log.info("6 "+act.getActivityName()+"/"+act.getRoleId()+"/"+act.getActivityId());
//			//log.info("userRoleActivityDTO: "+act);
//			Role role = act.getRoleId() != null ? roleRepository.getOne(act.getRoleId()) : null;
//			Activity activity = activityRepository.getOne(act.getActivityId());
//			ActivityRoleBinding arb = role != null ? activityRoleBindingRepository.findByRoleAndActivity(role, activity) : null;
//			
//			//ActivityRoleBinding arb = activityRoleBindingRepository.getOne(act.getArbId());
//			log.info("7 ");
//			log.info("got arb: "+arb);
//			if(arb != null){
//				log.info("8 ");
//				arb.setPermissionToApprove(act.getPermissionToApprove());
//				arb.setPermissionToCancel(act.getPermissionToCancel());
//				arb.setPermissionToCreate(act.getPermissionToCreate());
//				arb.setPermissionToDelete(act.getPermissionToDelete());
//				arb.setPermissionToPrint(act.getPermissionToPrint());
//				arb.setPermissionToUpdate(act.getPermissionToUpdate());
//				if(act.getIsRoleAssigned() == 0){
//					arb.setDeleted("Y");
//				}				
//				activityRoleBindingRepository.save(arb);
//				log.info("9 ");
//			}
//			else{
//				log.info("10 ");
//				ActivityRoleBinding arbNew = new ActivityRoleBinding();
//				arbNew.setActivity(activity);
//				arbNew.setPermissionToApprove(act.getPermissionToApprove());
//				arbNew.setPermissionToCancel(act.getPermissionToCancel());
//				arbNew.setPermissionToCreate(act.getPermissionToCreate());
//				arbNew.setPermissionToDelete(act.getPermissionToDelete());
//				arbNew.setPermissionToPrint(act.getPermissionToPrint());
//				arbNew.setPermissionToUpdate(act.getPermissionToUpdate());
//				arbNew.setRole(role);
//				arbNew.setDeleted("N");
//				activityRoleBindingRepository.save(arbNew);
//			}
//			if(act.getSubmenus()!= null){
//				log.info("11 ");
//				act.getSubmenus().forEach(subMenu -> {
//					saveUserRoleMenu(subMenu);
//				});
//				
//			}
//			
//		});
//		}catch(Exception e){
//			log.info("12 ");
//			log.error("Exception: "+e);
//			//return false;
//			throw e;
//		}
//		//return true;
//	}
	
	@DeleteMapping("/user-roles/{idList}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteRole(@PathVariable("idList") List<Long> idList){
		
		TransactionResponseDTO ret = new TransactionResponseDTO();
		
		ret.setResponseStatus(1);
		ret.setResponseString("Deleted successfully");
		try{
			idList.forEach(id -> {
				roleRepository.deleteById(id);
			});
			
		}catch(Exception e){
			ret.setResponseStatus(0);
			ret.setResponseString("Error deleting Role"+e.getMessage());
		}
		
		return new ResponseEntity<TransactionResponseDTO>(ret, HttpStatus.OK);
		
	}
	
	@PostMapping("/user-roles")
	@Transactional
	public ResponseEntity<List<RoleDTO>> saveUserRole(@RequestBody List<RoleDTO> roleList){
		
		List<RoleDTO> outRoles = new ArrayList<RoleDTO>();
		
		roleList.forEach(role -> {
			if(role.getRole() != null){
				RoleDTO roleDTO = roleService.save(role);
				outRoles.add(roleDTO);
			}
		});
		
		return new ResponseEntity<List<RoleDTO>>(outRoles, HttpStatus.OK);
	}
	
	@PostMapping("/user")
	public ResponseEntity<UserDTO> save(@RequestBody UserDTO userDTO){
		
		log.info("inside function......" + "userId :"+ userDTO.getId() +" , "+ "userPassword :"+userDTO.getPassword());
		
		return new ResponseEntity<UserDTO>(userService.saveEmployee(userDTO), HttpStatus.OK);
		
	}
	
	@PostMapping("/password-change")
	public ResponseEntity<Boolean> passwordReset(@RequestBody UserDTO userDTO){
		
		return new ResponseEntity<Boolean>(userService.resetPasswordEmployee(userDTO), HttpStatus.OK);
	}
	
	@PostMapping("/disable-login")
	public ResponseEntity<Boolean> disableLogin(@RequestBody UserDTO userDTO){
		
		return new ResponseEntity<Boolean>(userService.disableLogin(userDTO), HttpStatus.OK);
	}
	
	@PostMapping("/new-employee")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> createNewEmployee(@RequestBody UserDTO userDTO
			,HttpServletRequest request){
		
		log.info("in createNewEmployee: "+ userDTO.getUsername()+","+userDTO.getPassword());
	
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		String authToken = request.getHeader(this.tokenHeader);
    	String tenantId = jwtTokenUtil.getTenantIdFromToken(authToken);
    	
    	String user = SecurityContextHolder.getContext().getAuthentication().getName();


		UserTenantRelation userTenantRelation = new UserTenantRelation();

		UserTenantRelation utrPrev = userTenantRelationRepository.findFirstByTenant(tenantId);

		userTenantRelation.setTenant(tenantId);
		userTenantRelation.setUsername(userDTO.getUsername());
		userTenantRelation.setEmail(userDTO.getEmail());
		userTenantRelation.setName(userDTO.getName());
		userTenantRelation.setCreatedBy(user);
		userTenantRelation.setCreatedDateTime(new Date());
		userTenantRelation.setUpdatedBy(user);
		userTenantRelation.setUpdatedDateTime(new Date());
		userTenantRelation.setIsActive(1);
		userTenantRelation.setCompanyName(utrPrev.getCompanyName());
		userTenantRelation.setCompanyId(utrPrev.getCompanyId());
		transactionResponseDTO.setResponseStatus(1);
		transactionResponseDTO.setResponseString("New Tenant Entry created");
		
		try{
			userTenantRelationRepository.save(userTenantRelation);
		}catch(Exception e){
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("Error creating Tenant Entry.."+e.getMessage());
		}
		
		log.info("returning transactionResponseDTO: "+transactionResponseDTO.getResponseString()+","+transactionResponseDTO.getResponseStatus());
		
		return new ResponseEntity<TransactionResponseDTO>(transactionResponseDTO, HttpStatus.OK);
		
	}
	
	@DeleteMapping("/user/{id}")
	@Transactional
	public ResponseEntity<TransactionResponseDTO> deleteUser(@PathVariable("id") Long id){
		
		log.info("in deleteUser incoming id: "+id);
		
		return new ResponseEntity<TransactionResponseDTO>(userService.deleteUser(id), HttpStatus.OK);
	}
	


	@GetMapping("/menus/{userName:.+}")
	public ResponseEntity<List<MenuDTO>> getMenuForUser(@PathVariable("userName") String userName){
		
		log.info("in getMenuForUser: "+userName);
		
		List<MenuDTO> menuList = new ArrayList<MenuDTO>();
		
		List<MenuDTO> returnMenuList = new ArrayList<MenuDTO>();
		
		menuRepository.findAll()
		.stream()
		.filter(menu -> menu.getDeleted().equals("N"))
		.forEach(menu -> {
			
			//menuList.add(mapMenuDTO(menu, userName));
//			log.info("menu.getName(): "+ menu.getName() +":"+ menu.getActivities().size());
			if(menu.getActivities().size() > 0)
				menuList.add(mapMenuDTOForUser(menu, userName));
			
		});
		
		
		returnMenuList = menuList.stream()
		.filter(m -> m.getActivities().size() > 0)
		.collect(Collectors.toList());
		
		log.info("returning menuList");
//		returnMenuList.forEach(m -> {
//			log.info(m);
//			log.info(m.getActivities().size());
//		});
		
		
		return new ResponseEntity<List<MenuDTO>>(returnMenuList, HttpStatus.OK);
	}
	
	@GetMapping("/menus-all") //getJustMenu
	public ResponseEntity<List<MenuDTO>> getAllMenu(){
		
		log.info("in getAllMenu: ");
		
		List<MenuDTO> menuList = new ArrayList<MenuDTO>();
		
		
		menuRepository.findAll()
		.stream()
		.filter(menu -> menu.getDeleted().equals("N"))
		.forEach(menu -> {
			
			//menuList.add(mapMenuDTO(menu, userName));
//			log.info("Menu: "+menu.getName()+":"+menu.getDeleted());
			menuList.add(getJustMenu(menu));
			
		});
		
		return new ResponseEntity<List<MenuDTO>>(menuList, HttpStatus.OK);
	}
	@GetMapping("/menus-for-role/{roleId}")
	public ResponseEntity<List<MenuDTO>> getMenuForRole(@PathVariable("roleId") Long roleId){
		
		log.info("in getMenuForRole: "+roleId);
		
		List<MenuDTO> menuList = new ArrayList<MenuDTO>();
		Role role = roleRepository.getOne(roleId);
		
		menuRepository.findAll()
		.stream()
		.filter(menu -> menu.getDeleted().equals("N"))
		.forEach(menu -> {
			
			//menuList.add(mapMenuDTO(menu, userName));
			menuList.add(mapMenuDTOForRole(menu, role));
			
		});
		
		return new ResponseEntity<List<MenuDTO>>(menuList, HttpStatus.OK);
	}
	

	
	@GetMapping("/status-permission/{statusId}")
	public ResponseEntity<StatusBasedPermissionDTO> getStatusBasedPermission(@PathVariable("statusId") Long statusId){
		
		Status status = statusService.getStatusById(statusId);
		return new ResponseEntity<StatusBasedPermissionDTO> (statusBasedPermissionMapper.modelToDTOMap(statusBasedPermissionRepository.findByStatus(status)), HttpStatus.OK);
		
	}
	
	@GetMapping("/activity-role-binding/{roleId}")
	public ResponseEntity<List<ActivityRoleBindingDTO>> getactivityRoleBindingForRole(@PathVariable("roleId") Long roleId){
		
		//User user = userService.findByUsername(username);
		
		//UserDTO userDTO = userMapper.modelToDTOMap(user);
		
		Role role = roleRepository.getOne(roleId);
		
		List<ActivityRoleBinding> modelList = activityRoleBindingRepository.findByRole(role);
		
		List<ActivityRoleBindingDTO> dtoList = new ArrayList<ActivityRoleBindingDTO>();
		
		modelList
		.stream()
		.filter(model -> model.getActivity() != null)
		.filter(model -> model.getActivity().getDeleted().equals("N"))
		.filter(model -> model.getActivity().getMenu().getDeleted().equals("N"))
		.forEach(model -> {
			ActivityRoleBindingDTO dto = new ActivityRoleBindingDTO();
			//dto.setCompany(model.getCompany());
			dto.setActivityId(model.getActivity().getId());
			dto.setDeleted(model.getDeleted());
			dto.setId(model.getId());
			dto.setPermissionToApprove(model.getPermissionToApprove());
			dto.setPermissionToCancel(model.getPermissionToCancel());
			dto.setPermissionToCreate(model.getPermissionToCreate());
			dto.setPermissionToDelete(model.getPermissionToDelete());
			dto.setPermissionToPrint(model.getPermissionToPrint());
			dto.setPermissionToUpdate(model.getPermissionToUpdate());
			dto.setRoleId(model.getRole().getId());
			dto.setUserId(null);
			
			dtoList.add(dto);
			
			//log.info("model.getActivity().getMenu().getDeleted(): "+model.getActivity().getMenu().getName()+":"+model.getActivity().getMenu().getDeleted());
			
		});
		
		log.info("returning ActivityRoleBindingDTO" );
//		dtoList.forEach(dto -> {
//			log.info("dto: "+dto);
//		});
		return new ResponseEntity<List<ActivityRoleBindingDTO>>(dtoList, HttpStatus.OK);
		
	}
	

	
	private MenuDTO mapMenuDTOForRole(Menu menu, Role role){
		
		//log.info("in mapMenuDTOForRole: "+role);
		MenuDTO menuDTO = new MenuDTO();
		//User user = userService.findByUsername(userName);
		//Set<Role> userRoles = user.getRoles();
		List<ActivityRoleBinding> activityRoleBindings = new ArrayList<ActivityRoleBinding>();
		
		
			
		activityRoleBindings.addAll(activityRoleBindingRepository.findByRole(role));
		
		
		List<Activity> allActivities = activityRoleBindings.stream()
					.map(arb -> arb.getActivity())
					.filter(act -> act.getDeleted().equals("N"))
					.collect(Collectors.toList());
		
		List<Activity> distinctActivitiesForRoles = allActivities.stream().distinct().collect(Collectors.toList());
		
		
//		if(menu.getMenuLevel() == 1){
		menuDTO.setId(menu.getId());
		menuDTO.setName(menu.getName());
		//menuDTO.setCompany(menu.getCompany());
		menuDTO.setMenuOrder(menu.getMenuOrder());
		menuDTO.setMenuLevel(menu.getMenuLevel());
		menuDTO.setActivities(menu.getActivities().stream()
								.filter(activity -> distinctActivitiesForRoles.stream().anyMatch(dact -> dact.getId() == activity.getId()))
								.map(activity -> {
									ActivityDTO activityDTO = new ActivityDTO();
									activityDTO.setActivityOrder(activity.getActivityOrder());
									activityDTO.setActivityRoleBindings(activityRoleBindingMapper.modelToDTOList(activityRoleBindings.stream().filter(arb -> arb.getActivity().getId() == activity.getId()).collect(Collectors.toList()))); // TO D
									//activityDTO.setCompany(companyMapper.modelToDTOMap(activity.getCompany()));
									activityDTO.setDeleted(activity.getDeleted());
									activityDTO.setDescription(activity.getDescription());
									activityDTO.setId(activity.getId());
									activityDTO.setImagePath(activity.getImagePath());
									activityDTO.setName(activity.getName());
									activityDTO.setPageUrl(activity.getPageUrl());
									activityDTO.setUrlParams(activity.getUrlParams());
									activityDTO.setSubMenu((activity.getSubMenu()!=null && activity.getSubMenu().getDeleted().equals("N"))? mapMenuDTOForRole(activity.getSubMenu(), role): null);
									return activityDTO;
								})
								.collect(Collectors.toList()));	
//		}
		//log.info("Returning menuDTO: "+menuDTO);
		return menuDTO;
	}
	
	
	private MenuDTO getJustMenu(Menu menu){
		
		
		MenuDTO menuDTO = new MenuDTO();
		
			
		
		
//		if(menu.getMenuLevel() == 1){
		menuDTO.setId(menu.getId());
		menuDTO.setName(menu.getName());
		//menuDTO.setCompany(menu.getCompany());
		menuDTO.setMenuOrder(menu.getMenuOrder());
		menuDTO.setMenuLevel(menu.getMenuLevel());
		menuDTO.setActivities(menu.getActivities().stream()	
								.filter(activity -> activity.getDeleted().equals("N"))
								.map(activity -> {
									ActivityDTO activityDTO = new ActivityDTO();
									activityDTO.setActivityOrder(activity.getActivityOrder());
									activityDTO.setActivityRoleBindings(null); 
									activityDTO.setDeleted(activity.getDeleted());
									activityDTO.setDescription(activity.getDescription());
									activityDTO.setId(activity.getId());
									activityDTO.setImagePath(activity.getImagePath());
									activityDTO.setName(activity.getName());
									activityDTO.setPageUrl(activity.getPageUrl());
									activityDTO.setUrlParams(activity.getUrlParams());
									activityDTO.setSubMenu((activity.getSubMenu()!=null && activity.getSubMenu().getDeleted().equals("N")) ? getJustMenu(activity.getSubMenu()): null);
									return activityDTO;
								})
								.collect(Collectors.toList()));	
//		}
		log.info("getJustMenu Returning menuDTO: "+menuDTO);
		return menuDTO;
	}

	private MenuDTO mapMenuDTOForUser(Menu menu, String userName){
		
		//log.info("in mapMenuDTOForUser: "+userName);
		MenuDTO menuDTO = new MenuDTO();
		User user = userService.findByUsername(userName);
		
		//log.info("Got User: "+user.getName());
		Set<Role> userRoles = user.getRoles();
		List<ActivityRoleBinding> activityRoleBindings = new ArrayList<ActivityRoleBinding>();
		
		userRoles.forEach(userRole -> {
			
			activityRoleBindings.addAll(activityRoleBindingRepository.findByRole(userRole));
		});
		
		List<Activity> allActivities = activityRoleBindings.stream()
					.map(arb -> arb.getActivity())
					.filter(act -> act != null)
					.filter(act -> act.getDeleted().equals("N"))					
					.collect(Collectors.toList());
		
		//log.info("allActivities.size(): "+allActivities.size());
		
		List<Activity> distinctActivitiesForRoles = allActivities.stream().distinct().collect(Collectors.toList());
		
		//log.info("distinctActivitiesForRoles.size(): "+distinctActivitiesForRoles.size());
		
//		if(menu.getMenuLevel() == 1){
		menuDTO.setId(menu.getId());
		menuDTO.setName(menu.getName());
		//menuDTO.setCompany(menu.getCompany());
		menuDTO.setMenuOrder(menu.getMenuOrder());
		menuDTO.setMenuLevel(menu.getMenuLevel());
		menuDTO.setActivities(menu.getActivities().stream()
								.filter(activity -> activity.getDeleted().equals("N"))
								.sorted()
								.filter(activity -> distinctActivitiesForRoles.stream().anyMatch(dact -> dact.getId() == activity.getId()))				
								.map(activity -> {
									ActivityDTO activityDTO = new ActivityDTO();
									activityDTO.setActivityOrder(activity.getActivityOrder());
									activityDTO.setActivityRoleBindings(activityRoleBindingMapper.modelToDTOList(activityRoleBindings.stream().filter(arb -> (arb.getActivity() != null ? arb.getActivity().getId() : null) == activity.getId()).collect(Collectors.toList()))); // TO D
									//activityDTO.setCompany(companyMapper.modelToDTOMap(activity.getCompany()));
									activityDTO.setDeleted(activity.getDeleted());
									activityDTO.setDescription(activity.getDescription());
									activityDTO.setId(activity.getId());
									activityDTO.setImagePath(activity.getImagePath());
									activityDTO.setName(activity.getName());
									activityDTO.setPageUrl(activity.getPageUrl());
									activityDTO.setUrlParams(activity.getUrlParams());
									activityDTO.setSubMenu((activity.getSubMenu()!=null && activity.getSubMenu().getDeleted().equals("N")) ? mapMenuDTOForUser(activity.getSubMenu(), userName): null);
//									//activityDTO.setSubMenu((activity.getSubMenu()!=null && activity.getSubMenu().getDeleted().equals("N"))  ? (activityDTO.getSubMenu().getActivities() !=null && activityDTO.getSubMenu().getActivities().size() > 0 ? activityDTO.getSubMenu() : null) : null);
									return activityDTO;
								})
								.collect(Collectors.toList()));	
//		}
		
		List<ActivityDTO> menuActs = menuDTO.getActivities();

		
		for(int i =0; i < menuDTO.getActivities().size(); i++){
			if(menuDTO.getActivities().get(i).getActivityRoleBindings().size() == 0){
				menuActs.remove(i);
			}
		}
		
		menuDTO.setActivities(menuActs);
		//log.info("Returning menuDTO: "+menuDTO);
		return menuDTO;
	}
	
	@GetMapping("/theme/{userId}/{theme}")
	public ResponseEntity<TransactionResponseDTO> saveTheme(@PathVariable("userId") Long userId,
			@PathVariable("theme") String theme){
		
		User user = userService.getUserById(userId);
		user.setUserThemeName(theme);
		User returnUser = userService.save(user);
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
		
		if (returnUser != null){
			
			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("theme saved");
		}else{
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("error");
		}
		
		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);
		
	}


	@GetMapping("/language/{userId}/{language}")
	public ResponseEntity<TransactionResponseDTO> saveLanguge(@PathVariable("userId") Long userId,
															@PathVariable("language") String language){
		User user = userService.getUserById(userId);
		user.setUserLanguage(language);
		User returnUser = userService.save(user);
		TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();

		if (returnUser != null){

			transactionResponseDTO.setResponseStatus(1);
			transactionResponseDTO.setResponseString("laguage saved");
		}else{
			transactionResponseDTO.setResponseStatus(0);
			transactionResponseDTO.setResponseString("error");
		}

		return new ResponseEntity<TransactionResponseDTO> (transactionResponseDTO, HttpStatus.OK);

	}



	
}
