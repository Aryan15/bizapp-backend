package com.coreerp.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coreerp.dao.ExpenseRepository;
import com.coreerp.dao.InvoiceItemRepository;
import com.coreerp.dao.VoucherItemRepository;
import com.coreerp.dto.TransactionResponseDTO;
import com.coreerp.model.ExpenseHeader;
import com.coreerp.service.ExpenseService;

@RestController
@RequestMapping("/api")
public class ExpenseController {

final static Logger log = LogManager.getLogger(ProcessController.class);


@Autowired
ExpenseRepository expenseRepository;

@Autowired
ExpenseService expenseService;


@Autowired
VoucherItemRepository voucherItemRepository;

//@PostMapping("/expenseHeader")
//public ResponseEntity<ExpenseHeader> save(@RequestBody ExpenseHeader  expenseHeader){
//	
//	log.info("incoming model: "+expenseHeader);
//	
//	return new ResponseEntity<ExpenseHeader>(expenseRepository.save(expenseHeader), HttpStatus.OK);
//	
//}



@GetMapping("/expenseHeader/{id}")
public ResponseEntity<ExpenseHeader> getOne(@PathVariable("id") Long id){
	
	log.info("incoming id: "+id);
	
	return new ResponseEntity<ExpenseHeader>(expenseRepository.getOne(id), HttpStatus.OK);
	
}

@GetMapping("/expenses")
public ResponseEntity<List<ExpenseHeader>> getAllExpenses() {
	
	log.info("In getAllProcesses");
	List<ExpenseHeader> expensesList = expenseRepository.findAll();
	
	log.info("expensesList.size(): "+ expensesList.size());
	
	return new ResponseEntity<List<ExpenseHeader>>(expensesList, HttpStatus.OK);
}

@PostMapping("/expenses")
public ResponseEntity<List<ExpenseHeader>> expensesSave(@RequestBody List<ExpenseHeader> expensesList){
	
	log.info("in expensesSave: "+expensesList);
	List<ExpenseHeader> outExpenses = new ArrayList<ExpenseHeader>();
	
	for(ExpenseHeader expenseHeader : expensesList){
		if(!expenseHeader.getName().isEmpty()){
			ExpenseHeader p = expenseRepository.save(expenseHeader);
			expensesList.add(p);				
		}

	}
	
	return new ResponseEntity<List<ExpenseHeader>>(outExpenses, HttpStatus.OK);
	
}


@DeleteMapping("/expenseHeader/{ids}")
public ResponseEntity<TransactionResponseDTO> delete(@PathVariable List<Long> ids){
	
	TransactionResponseDTO response = new TransactionResponseDTO();
	
	response.setResponseStatus(1);
	response.setResponseString("Deleted Successfully");
	
	try{
		ids.forEach(id -> {
			expenseRepository.deleteById(id);
		});
		
	}catch(Exception e){
		log.info("Error deleteing.."+e.getMessage());
		response.setResponseStatus(0);
		response.setResponseString(e.getMessage());
	}
	return new ResponseEntity<TransactionResponseDTO>(response, HttpStatus.OK);
}

@PostMapping("/expenseHeader")
@Transactional
public ResponseEntity<List<ExpenseHeader>> saveExpense(@RequestBody List<ExpenseHeader> expenseHeaderList){
	
	log.info("in expensSave: "+expenseHeaderList);
	System.out.println("expenseHeaderList.................!!!!!!"+ expenseHeaderList);
	List<ExpenseHeader> outExpenses = new ArrayList<ExpenseHeader>();
	
	for(ExpenseHeader expenseHeader : expenseHeaderList){
		if(!expenseHeader.getName().isEmpty()){
			ExpenseHeader expense = expenseService.saveExpense(expenseHeader);
			outExpenses.add(expenseHeader);	
			
		}

	}
	
	return new ResponseEntity<List<ExpenseHeader>>(outExpenses, HttpStatus.OK);
	/*
	 * ExpenseHeader expenseHeaderOut = voucherService.saveExpense(ExpenseHeaderin);
	 * log.info("Saved successfully"); return new
	 * ResponseEntity<ExpenseHeader>(expenseHeaderOut, HttpStatus.OK);
	 */
}

@GetMapping("/expense-status/{ids}")
public ResponseEntity<Boolean> expenseStatusCheck(@PathVariable List<Long> ids){
	
	Boolean returnValue = false;
	
	List<ExpenseHeader> expenseHeaders = expenseRepository.findByIdIn(ids);

	returnValue = voucherItemRepository.existsByExpenseHeaderIn(expenseHeaders);

	return new ResponseEntity<Boolean>(returnValue, HttpStatus.OK);
}

}

