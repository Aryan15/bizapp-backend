package com.coreerp.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.ServletContext;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.coreerp.dao.EmailSettingRepository;
import com.coreerp.dto.EmailObject;
import com.coreerp.dto.UserDTO;
import com.coreerp.model.EmailSetting;
import com.coreerp.model.UserTenantRelation;


import freemarker.template.Configuration;

@Service
public class EmailService {
    final static Logger log = LogManager.getLogger(EmailService.class);

    @Autowired
    private Environment environment;

    @Autowired
    Configuration freemarkerConfiguration;


    @Autowired
    ServletContext servletContext;

    @Autowired
    EmailSettingRepository emailSettingRepository;


    private JavaMailSender getMailSender(EmailSetting emailSetting) {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//        EmailSetting emailSetting = emailSettingRepository.getOne(1);
        if (null == emailSetting)
            return mailSender;

        String emailHost = emailSetting.getEmailHost();
        Integer emailPort = emailSetting.getEmailPort();
        String emailUsername = emailSetting.getEmailUsername();
        String emapPassword = EncryptionUtil.decrypt(emailSetting.getEmailPassword());

        String mailSmtpStarttlsEnable = emailSetting.getEmailSmtpStarttlsEnable() + "";
        String mailSmtpAuth = emailSetting.getEmailSmtpAuth() + "";
        String mailTransportProtocol = emailSetting.getEmailTransportProtocol();
        String mailDebug = emailSetting.getEmailDebug() + "";

        log.info("emailHost: " + emailHost);
        // Using gmail.
        mailSender.setHost(emailHost);//"smtp.gmail.com");
        mailSender.setPort(emailPort); //587);
        mailSender.setUsername(emailUsername); //"ravikumar.bg@welkinsofttech.com");
        mailSender.setPassword(emapPassword);

        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.smtp.starttls.enable", mailSmtpStarttlsEnable);
        javaMailProperties.put("mail.smtp.auth", mailSmtpAuth);
        javaMailProperties.put("mail.transport.protocol", mailTransportProtocol);
        javaMailProperties.put("mail.debug", mailDebug);


        mailSender.setJavaMailProperties(javaMailProperties);
        return mailSender;
    }


    public String geFreeMarkerTemplateContent(Map<String, Object> model, String templateName) {
        StringBuffer content = new StringBuffer();

        try {
            content.append(FreeMarkerTemplateUtils.processTemplateIntoString(
                    freemarkerConfiguration.getTemplate(templateName), model));
            return content.toString();
        } catch (Exception e) {
            System.out.println("Exception occured while processing fmtemplate:" + e.getMessage());
        }
        return "";
    }


    @Async("processExecutor")
    public void sendEmail(EmailSetting emailSetting, EmailObject object, String emailFroma, String templateNamea, String emailToa, String subjecta,
                          String messagea, List<CommonsMultipartFile> attachFiles, HashMap<String, String> uploadedFileNamesa) {


        log.info("Begin sendEmail");


        final String emailFrom = emailFroma;//environment.getProperty("emailFrom");
        final String templateName = templateNamea; //environment.getProperty("emailTemplateName");

        final String emailTo = emailToa;
        final String subject = subjecta;
        final HashMap<String, String> uploadedFileNames = uploadedFileNamesa;


        log.info("emailFrom: " + emailFrom);


        getMailSender(emailSetting).send(new MimeMessagePreparator() {

            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
                String UPLOADED_FOLDER = environment.getProperty("file.upload.folder");
                messageHelper.setFrom(emailFrom);
                messageHelper.setTo(emailTo);
                messageHelper.setSubject(subject);
                //messageHelper.setText(message);

                Map<String, Object> model = new HashMap<String, Object>();


                model.put("user", object);

                log.info("object: " + object);


                String text = geFreeMarkerTemplateContent(model, templateName);//Use Freemarker or Velocity
                log.info("Template content : " + text);

                // use the true flag to indicate you need a multipart message
                messageHelper.setText(text, true);

                //	messageHelper.addAttachment("COUSTOMER Invoice", (File) attachFiles);

                // determines if there is an upload file, attach it to the e-mail

                if (null != uploadedFileNames) {
                    log.info("Attaching files to email");
                    if (!(uploadedFileNames.isEmpty())) {

                        log.info("number of files to attach: " + uploadedFileNames.size());
                        for (HashMap.Entry<String, String> entry : uploadedFileNames.entrySet()) {

                            String attachName = entry.getValue();
                            log.info("attachName: " + attachName);

                            String absoluteFilePath = servletContext.getRealPath(UPLOADED_FOLDER);


                            String fullFileName = absoluteFilePath + attachName;

                            log.info("fullFileName: " + fullFileName);

                            log.info("Full file path: " + fullFileName);

                            FileSystemResource file = new FileSystemResource(fullFileName);
                            //getInputStream(attachFiles);

                            messageHelper.addAttachment(attachName, file);


                        }
                    }
                } else {
                    log.info("attachment is null");
                }
            }

        });

    }


    @Async("processExecutor")
    public void sendEmailSingleFileAttach(EmailSetting emailSetting,
                                          Object object,
                                          String emailFroma,
                                          String emailContenta,
                                          String emailToa,
                                          String subjecta,

                                          ByteArrayDataSource source,
                                          String attachFileName,
                                           int check) {



        //log.info("Begin sendEmail" + source.getContentType());



        final String emailFrom = emailFroma;//environment.getProperty("emailFrom");
        final String emailContent = emailContenta; //environment.getProperty("emailTemplateName");

        final String emailTo = emailToa;
        final String subject = subjecta;

        //final String message = messageto;
        getMailSender(emailSetting).send(new MimeMessagePreparator() {

            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage,
                        MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                        StandardCharsets.UTF_8.name());
                String UPLOADED_FOLDER = environment.getProperty("file.upload.folder");
                messageHelper.setFrom(emailFrom);
                messageHelper.setTo(emailTo);
                messageHelper.setSubject(subject);
                //messageHelper.setText(message);

                Map<String, Object> model = new HashMap<String, Object>();


                model.put("user", object);

                log.info("object: " + object);


                String text = geFreeMarkerTemplateContent(model, emailContent);//Use Freemarker or Velocity
                //log.info("Template content : "+text);

                // use the true flag to indicate you need a multipart message
                messageHelper.setText(emailContent, true);

                if(check==0) {
                      messageHelper.addAttachment(attachFileName, source);
                }

               // messageHelper.addAttachment(attachFileName, source);

                // determines if there is an upload file, attach it to the e-mail


            }



        });




    }


}
