package com.coreerp.utils;

public class TransactionDataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TransactionDataException(String message, Throwable cause){
		super(message, cause);
		
	}

}
