package com.coreerp.utils;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.internal.Mimetypes;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import com.coreerp.serviceimpl.AwsS3StorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.Base64;

@Service
public class AWSUtil {

    private static Logger log = LogManager.getLogger(AWSUtil.class.getName());

    private AmazonS3 s3client;

    @PostConstruct
    public void init() {
        s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.AP_SOUTH_1).build();

    }

    public void uploadFileTos3bucket(String bucketName, String fileName, File file) {

        PutObjectResult result = s3client.putObject(new PutObjectRequest(bucketName, fileName, file)
                .withCannedAcl(CannedAccessControlList.Private));

        log.info("uploadFileTos3bucket result: "+result);

    }

    public byte[] loadAsBytes(String bucketName, String companyPrefix, String filename, String tenantId) {
        byte[] content = null;
        String keyName = companyPrefix + "/" + tenantId + "/" + filename;
        log.info("Downloading an object with key= " + keyName);
        final S3Object s3Object = s3client.getObject(bucketName, keyName);
        final S3ObjectInputStream stream = s3Object.getObjectContent();
        try {
            content = IOUtils.toByteArray(stream);
            log.info("File downloaded successfully.");
            s3Object.close();
        } catch(final IOException ex) {
            log.error("IO Error Message= " + ex.getMessage());
        }
        return content;
    }



    private String encodeBase64URL(BufferedImage imgBuf, String mimeType) throws IOException {
        String base64;
        String prefix = mimeType != null && mimeType.equals("image/png")? "data:image/png;base64," : "data:image/jpeg;base64,";

        if (imgBuf == null) {
            base64 = null;
        } else {
//            Base64 encoder = new Base64();
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            ImageIO.write(imgBuf, "PNG", out);

            byte[] bytes = out.toByteArray();
            base64 = prefix + Base64.getEncoder().encodeToString(bytes);
        }

        return base64;
    }




    public Boolean deleteFile(String bucketName, String fileName) {
        s3client.deleteObject(new DeleteObjectRequest(bucketName, fileName));
        return true;
    }

    public String getPresignedUrl(String bucketName, String fileName){
        java.util.Date expiration = new java.util.Date();
        long expTimeMillis = expiration.getTime();
        expTimeMillis += 1000 * 60 * 60;
        expiration.setTime(expTimeMillis);

        GeneratePresignedUrlRequest generatePresignedUrlRequest =
                new GeneratePresignedUrlRequest(bucketName, fileName)
                        .withMethod(HttpMethod.GET)
                        .withExpiration(expiration);
        URL url = s3client.generatePresignedUrl(generatePresignedUrlRequest);

        log.info("Pre-Signed URL: " + url.toString());
        return url.toString();
    }

    public void store(String bucketName, String prefix, MultipartFile multipartFile, String tenantId) {



        String fileName = prefix + "/" + tenantId + "/" + StringUtils.cleanPath(multipartFile.getOriginalFilename());
        log.info("inside store: "+fileName);
        File file = null;
        try {
            file = convertMultiPartToFile(multipartFile);
            uploadFileTos3bucket(bucketName, fileName, file);
            log.info("upload complete");
            file.delete();
        } catch (IOException e) {
            log.error("error: "+e);
            log.error(e.getStackTrace());
        }

    }

    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    public void downloadObjectAsFile(String directory, String bucketName, String fileName, String justFileName){

        FileOutputStream fos = null;
        S3ObjectInputStream s3is = null;
        try {
            S3Object o = s3client.getObject(bucketName, fileName);
            s3is = o.getObjectContent();

            File targetDirectory = new File(directory);
            if(!targetDirectory.exists()){
                targetDirectory.mkdir();
            }

            File file = new File(directory+ File.separator +justFileName);
            if(!file.exists()){
                file.createNewFile();
            }
            fos = new FileOutputStream(file);
            byte[] read_buf = new byte[1024];
            int read_len = 0;
            while ((read_len = s3is.read(read_buf)) > 0) {
                fos.write(read_buf, 0, read_len);
            }
//            fos.flush();
//            s3is.close();
//            fos.close();
        } catch (AmazonServiceException e) {
            log.error(e.getErrorMessage());
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            try {
                fos.flush();
                s3is.close();
                fos.close();
            }catch (IOException ioe){
                log.error(ioe.getMessage());
            }
        }
    }

    public Resource loadAsResource(String bucketName, String prefix,  String filename, String tenantId) {
        byte[] content = null;
        String keyName = prefix + "/" + tenantId + "/" + filename;
        log.info("Downloading an object with key= " + keyName);
        final S3Object s3Object = s3client.getObject(bucketName, keyName);
        final S3ObjectInputStream stream = s3Object.getObjectContent();
        try {
            content = IOUtils.toByteArray(stream);
            log.info("File downloaded successfully.");
//            s3Object.close();
        } catch(final IOException ex) {
            log.error("IO Error Message= " + ex.getMessage());
        } finally {
            try {
                s3Object.close();
            }catch (IOException ioe){
                log.error("IO Error Message= " + ioe.getMessage());
            }
        }
        return new ByteArrayResource(content);
    }

    public String loadAsBase64( String bucketName, String prefix, String filename, String tenantId) {
        byte[] content = null;
        String keyName = prefix + "/" + tenantId + "/" + filename;
        log.info("Downloading an object with key= " + keyName);
        final S3Object s3Object = s3client.getObject(bucketName, keyName);
        //final S3ObjectInputStream stream = s3Object.getObjectContent();
        BufferedImage imgBuf;
        String base64 = null;
        try {
            imgBuf = ImageIO.read(s3Object.getObjectContent());
            String mimeType = Mimetypes.getInstance().getMimetype(filename);
            log.info("mimeType: "+mimeType);
            base64 = encodeBase64URL(imgBuf, mimeType);
        }catch (IOException e){
            log.error("IOException: "+e);
        }finally {
            try {
                s3Object.close();
            }catch (IOException ioe){
                log.error("IO Error Message= " + ioe.getMessage());
            }
        }
        return base64;
    }

    public boolean doesObjectExists(String bucketName, String fileName){

        return s3client.doesObjectExist(bucketName, fileName);

    }
}
