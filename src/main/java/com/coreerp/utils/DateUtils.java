package com.coreerp.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	
	public static Date addHoursToJavaUtilDate(Date date, int hours) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.add(Calendar.HOUR_OF_DAY, hours);
	    return calendar.getTime();
	}




	public static String amountInWord(Integer amount) {
		String amtInWords="";
		if (amount <= 0)
		{
			amtInWords="";
		}
		else
		{

			amtInWords+=pw((amount / 1000000000), " Hundred");
			amtInWords+=pw((amount / 10000000) % 100, " Crore");
			amtInWords+=pw(((amount / 100000) % 100), " Lakh");
			amtInWords+=pw(((amount / 1000) % 100), " Thousand");
			amtInWords+=pw(((amount/ 100) % 10), " Hundred");
			amtInWords+=pw((amount % 100), " ");
		}
		return amtInWords;

	}

	public static String pw(int n, String ch)
	{
		String str="";
		String one[] = { " ", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine", " Ten",
				" Eleven", " Twelve", " Thirteen", " Fourteen", "Fifteen", " Sixteen", " Seventeen", " Eighteen",
				" Nineteen" };

		String ten[] = { " ", " ", " Twenty", " Thirty", " Forty", " Fifty", " Sixty", "Seventy", " Eighty", " Ninety" };

		if (n > 19)
		{
			str+=ten[n / 10] + "" + one[n % 10];
		}
		else
		{
			str+=one[n];
		}
		if (n > 0) {
			str+=ch;
		}
		return str;
	}



}
