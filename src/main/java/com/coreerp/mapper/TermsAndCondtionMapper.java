package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.TermsAndConditionDTO;
import com.coreerp.model.TermsAndCondition;

@Component
public class TermsAndCondtionMapper implements AbstractMapper<TermsAndConditionDTO, TermsAndCondition> {

	@Autowired
	ModelMapper modelMapper;
	
	@Override
	public TermsAndConditionDTO modelToDTOMap(TermsAndCondition model) {
		
		return model != null ? modelMapper.map(model, TermsAndConditionDTO.class) : null;
	}

	@Override
	public TermsAndCondition dtoToModelMap(TermsAndConditionDTO dto) {
		return modelMapper.map(dto, TermsAndCondition.class);
	}

	@Override
	public List<TermsAndConditionDTO> modelToDTOList(List<TermsAndCondition> modelList) {
		return modelList.stream()
				.map(material -> modelToDTOMap(material))
				.collect(Collectors.toList());
	}

	@Override
	public List<TermsAndCondition> dtoToModelList(List<TermsAndConditionDTO> dtoList) {
		return dtoList.stream()
				.map(materialDTO -> dtoToModelMap(materialDTO))
				.collect(Collectors.toList());
	}

}
