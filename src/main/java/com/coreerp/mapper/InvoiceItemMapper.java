package com.coreerp.mapper;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.model.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.InvoiceItemDTO;
import com.coreerp.service.DeliveryChallanService;
import com.coreerp.service.GRNService;
import com.coreerp.service.InvoiceService;
import com.coreerp.service.MaterialService;
import com.coreerp.service.PurchaseOrderService;
import com.coreerp.service.TaxService;
import com.coreerp.service.UnitOfMeasurementService;

@Component
public class InvoiceItemMapper implements AbstractMapper<InvoiceItemDTO, InvoiceItem> {

	final static Logger log = LogManager.getLogger(InvoiceItemMapper.class);
	
	@Autowired
	DeliveryChallanService deliveryChallanService;
	
	@Autowired
	DeliveryChallanHeaderMapper deliveryChallanHeaderMapper;
	
	@Autowired
	PurchaseOrderService purchaseOrderService;
	
	@Autowired
	MaterialService materialService;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	UnitOfMeasurementService unitOfMeasurementService;
	
	@Autowired
	PurchaseOrderHeaderMapper purchaseOrderHeaderMapper;
	
	@Autowired
	InvoiceService invoiceService;
	
	@Autowired
	GRNService grnService;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public InvoiceItemDTO modelToDTOMap(InvoiceItem model) {
		
		InvoiceItemDTO invoiceItemDTOOut = new InvoiceItemDTO();//modelMapper.map(model, InvoiceItemDTO.class);//new InvoiceItemDTO ();
		
		invoiceItemDTOOut.setAmount(model.getAmount());
		invoiceItemDTOOut.setAmountAfterDiscount(model.getAmountAfterDiscount());
		invoiceItemDTOOut.setAmountAfterTax(model.getAmountAfterTax());
		invoiceItemDTOOut.setCessAmount(model.getCessAmount());
		invoiceItemDTOOut.setCessPercentage(model.getCessPercentage());
		invoiceItemDTOOut.setCgstTaxAmount(model.getCgstTaxAmount());
		invoiceItemDTOOut.setCgstTaxPercentage(model.getCgstTaxPercentage());
		invoiceItemDTOOut.setDeliveryChallanHeaderId(model.getDeliveryChallanItem() != null ? model.getDeliveryChallanItem().getDeliveryChallanHeader().getId() : null);
		invoiceItemDTOOut.setDeliveryChallanItemId(model.getDeliveryChallanItem() != null ? model.getDeliveryChallanItem().getId() : null);
		invoiceItemDTOOut.setProformaInvoiceHeaderId(model.getProformaInvoiceItem() != null ? model.getProformaInvoiceItem().getInvoiceHeader().getId() : null);
		invoiceItemDTOOut.setProformaInvoiceItemId(model.getProformaInvoiceItem() != null ? model.getProformaInvoiceItem().getId() : null);
		invoiceItemDTOOut.setSourceInvoiceHeaderId(model.getSourceInvoiceItem() != null ? model.getSourceInvoiceItem().getInvoiceHeader().getId() : null);
		invoiceItemDTOOut.setSourceInvoiceItemId(model.getSourceInvoiceItem() != null ? model.getSourceInvoiceItem().getId() : null);
		invoiceItemDTOOut.setDiscountAmount(model.getDiscountAmount());
		invoiceItemDTOOut.setDiscountPercentage(model.getDiscountPercentage());
		invoiceItemDTOOut.setHeaderId( model.getInvoiceHeader().getId() );
		invoiceItemDTOOut.setId(model.getId());
		invoiceItemDTOOut.setIgstTaxAmount(model.getIgstTaxAmount());
		invoiceItemDTOOut.setIgstTaxPercentage(model.getIgstTaxPercentage());
		invoiceItemDTOOut.setMaterialId(model.getMaterial().getId());
		invoiceItemDTOOut.setPrice(model.getPrice());
		invoiceItemDTOOut.setPurchaseOrderHeaderId(model.getPurchaseOrderItem() != null ? model.getPurchaseOrderItem().getPurchaseOrderHeader().getId() : null);
		invoiceItemDTOOut.setPurchaseOrderItemId(model.getPurchaseOrderItem() != null ? model.getPurchaseOrderItem().getId() : null);
		invoiceItemDTOOut.setGrnHeaderId(model.getGrnItem() != null ? model.getGrnItem().getGrnHeader().getId() : null);
		invoiceItemDTOOut.setGrnItemId(model.getGrnItem() != null ? model.getGrnItem().getId() : null);
		invoiceItemDTOOut.setQuantity(model.getQuantity());
		invoiceItemDTOOut.setRemarks(model.getRemarks());
		invoiceItemDTOOut.setSgstTaxAmount(model.getSgstTaxAmount());
		invoiceItemDTOOut.setSgstTaxPercentage(model.getSgstTaxPercentage());
		invoiceItemDTOOut.setSlNo(model.getSlNo());
		invoiceItemDTOOut.setTaxId(model.getTax() != null ? model.getTax().getId(): null);
		invoiceItemDTOOut.setTransportationAmount(model.getTransportationAmount());
		invoiceItemDTOOut.setUnitOfMeasurementId(model.getUnitOfMeasurement() != null ? model.getUnitOfMeasurement().getId() : null);
		invoiceItemDTOOut.setPartNumber(model.getMaterial().getPartNumber());
		invoiceItemDTOOut.setPartName(model.getMaterial().getName());
		invoiceItemDTOOut.setHsnOrSac(model.getMaterial().getHsnCode());
		invoiceItemDTOOut.setUom(model.getUnitOfMeasurement() != null ? model.getUnitOfMeasurement().getName() : null);
		invoiceItemDTOOut.setInclusiveTax(model.getInclusiveTax());
		invoiceItemDTOOut.setNoteBalanceQuantity(model.getNoteBalanceQuantity());
		invoiceItemDTOOut.setSpecification(model.getSpecification());
		invoiceItemDTOOut.setProcessId(model.getProcessId());
		invoiceItemDTOOut.setProcessName(model.getProcessName());
		invoiceItemDTOOut.setIsContainer(model.getMaterial().getIsContainer());
		invoiceItemDTOOut.setOutName(model.getMaterial().getOutName());
		invoiceItemDTOOut.setOutPartNumber(model.getMaterial().getOutPartNumber());
		return invoiceItemDTOOut;
	}

	@Override
	public InvoiceItem dtoToModelMap(InvoiceItemDTO dto) {
		String deliveryChallanItemId=dto.getDeliveryChallanItemId();
		DeliveryChallanItem  dcItem = null;
		String inDCNumber = null;
		Date inDCDate = null;
		if(deliveryChallanItemId!= null){
			dcItem =	deliveryChallanService.getItemModelById(deliveryChallanItemId);
			if(dcItem.getSourceDeliveryChallanItem()!=null) {
				DeliveryChallanHeader inDcHeader = dcItem.getSourceDeliveryChallanItem().getDeliveryChallanHeader();
				inDCNumber = inDcHeader.getDeliveryChallanNumber();
				inDCDate = inDcHeader.getDeliveryChallanDate();
			}
		}


		log.info("in dtoToModelMap: "+dto.getId());
		InvoiceItem invoiceItemOut = new InvoiceItem();//modelMapper.map(dto, InvoiceItem.class);
		
		invoiceItemOut.setAmount(dto.getAmount());
		invoiceItemOut.setAmountAfterDiscount(dto.getAmountAfterDiscount());
		invoiceItemOut.setAmountAfterTax(dto.getAmountAfterTax());
		invoiceItemOut.setAverageContentPerPackage(null);
		invoiceItemOut.setBasicAmount(null);
		invoiceItemOut.setBatchCode(null);
		invoiceItemOut.setCessAmount(dto.getCessAmount());
		invoiceItemOut.setCessPercentage(dto.getCessPercentage());
		invoiceItemOut.setCgstTaxAmount(dto.getCgstTaxAmount());
		invoiceItemOut.setCgstTaxPercentage(dto.getCgstTaxPercentage());
//		invoiceItemOut.setDeliveryChallanHeader(dto.getDeliveryChallanHeaderId()!= null ? deliveryChallanService.getModelById(dto.getDeliveryChallanHeaderId()):null);
		invoiceItemOut.setDeliveryChallanItem(dcItem);
//		invoiceItemOut.setGrnHeader(dto.getGrnHeaderId()!= null ? grnService.getModelById(dto.getGrnHeaderId()):null);
		invoiceItemOut.setGrnItem(dto.getGrnItemId() != null ? grnService.getItemModelById(dto.getGrnItemId()) : null);
//		invoiceItemOut.setProformaInvoiceHeader(dto.getProformaInvoiceHeaderId() != null ? invoiceService.getModelById(dto.getProformaInvoiceHeaderId()) : null);
		invoiceItemOut.setProformaInvoiceItem(dto.getProformaInvoiceItemId() != null ? invoiceService.getItemById(dto.getProformaInvoiceItemId())  : null);
//		invoiceItemOut.setSourceInvoiceHeader(dto.getSourceInvoiceHeaderId() != null ? invoiceService.getModelById(dto.getSourceInvoiceHeaderId()) : null);
		invoiceItemOut.setSourceInvoiceItem(dto.getSourceInvoiceItemId() != null ? invoiceService.getItemById(dto.getSourceInvoiceItemId()) : null);
		invoiceItemOut.setDescriptionOfPackages(null);
		invoiceItemOut.setDiscountAmount(dto.getDiscountAmount());
		invoiceItemOut.setDiscountPercentage(dto.getDiscountPercentage());
		invoiceItemOut.setId(dto.getId());
		invoiceItemOut.setIgstTaxAmount(dto.getIgstTaxAmount());
		invoiceItemOut.setIgstTaxPercentage(dto.getIgstTaxPercentage());
		invoiceItemOut.setInclusiveTax(null);
		invoiceItemOut.setInDeliveryChallanDate(inDCDate);
		invoiceItemOut.setInDeliveryChallanNumber(inDCNumber);
		invoiceItemOut.setInspectionNumber(null);
		//invoiceItemOut.setInvoiceHeader(dto.getHeaderId());
		invoiceItemOut.setMaterial(materialService.getMaterialById(dto.getMaterialId()));
		invoiceItemOut.setOutDeliveryChallanDate(null);
		invoiceItemOut.setOutDeliveryChallanNumber(null);
		invoiceItemOut.setPrice(dto.getPrice());
		invoiceItemOut.setProcessQuantity(null);
//		invoiceItemOut.setPurchaseOrderHeader(dto.getPurchaseOrderHeaderId()!=null ? purchaseOrderService.getModelById(dto.getPurchaseOrderHeaderId()) : null);
		invoiceItemOut.setPurchaseOrderItem(dto.getPurchaseOrderItemId()!=null ? purchaseOrderService.getItemModelById(dto.getPurchaseOrderItemId()) : null);
//		invoiceItemOut.setGrnHeader(dto.getGrnHeaderId()!=null ? grnService.getModelById(dto.getGrnHeaderId()) : null);
		invoiceItemOut.setGrnItem(dto.getGrnItemId()!=null ? grnService.getItemModelById(dto.getGrnItemId()) : null);
		invoiceItemOut.setQuantity(dto.getQuantity());
		invoiceItemOut.setRemarks(dto.getRemarks());
		invoiceItemOut.setSgstTaxAmount(dto.getSgstTaxAmount());
		invoiceItemOut.setSgstTaxPercentage(dto.getSgstTaxPercentage());
		invoiceItemOut.setSlNo(dto.getSlNo());
		invoiceItemOut.setTax(dto.getTaxId() != null ?taxService.getById(dto.getTaxId()): null);
		invoiceItemOut.setTaxAmount((dto.getCgstTaxAmount()!= null ? dto.getCgstTaxAmount() : 0) + (dto.getSgstTaxAmount() != null ? dto.getSgstTaxAmount() :0) + (dto.getIgstTaxAmount() != null ? dto.getIgstTaxAmount() : 0));
		invoiceItemOut.setTaxRate(null);
		invoiceItemOut.setTransportationAmount(dto.getTransportationAmount());
		invoiceItemOut.setUnitOfMeasurement(dto.getUnitOfMeasurementId() != null ? unitOfMeasurementService.getById(dto.getUnitOfMeasurementId()) : null);
		invoiceItemOut.setInclusiveTax(dto.getInclusiveTax());
		invoiceItemOut.setNoteBalanceQuantity(dto.getNoteBalanceQuantity() != null ? dto.getNoteBalanceQuantity() : dto.getQuantity());
		invoiceItemOut.setSpecification(dto.getSpecification());
		invoiceItemOut.setProcessId(dto.getProcessId());
		invoiceItemOut.setProcessName(dto.getProcessName());
		log.info("invoiceItemOut.id: "+invoiceItemOut.getId());
		
		return invoiceItemOut;
	}

	@Override
	public List<InvoiceItemDTO> modelToDTOList(List<InvoiceItem> modelList) {
		
		List<InvoiceItemDTO> invoiceItemDTOList = modelList.stream().map(invoiceItem -> {
			InvoiceItemDTO invoiceItemDTO = modelToDTOMap(invoiceItem);
			return invoiceItemDTO;
		}).collect(Collectors.toList());
				
		
		return invoiceItemDTOList;
	}

	@Override
	public List<InvoiceItem> dtoToModelList(List<InvoiceItemDTO> dtoList) {
		//log.info("dtoToModelList.. incoiming item size: "+dtoList.size());
		List<InvoiceItem> invoiceItemListOut = dtoList.stream().map(invoiceItemDTO -> {
			InvoiceItem invoiceItem = dtoToModelMap(invoiceItemDTO);
			return invoiceItem;
		}).collect(Collectors.toList());
		
		//log.info("dtoToModelList.. outgoing item size: "+invoiceItemListOut.size());
		//log.info("dtoToModelList.. outgoing item id: "+invoiceItemListOut.get(0).getId());
		
		return invoiceItemListOut;
	}



	public InvoiceItemAudit modelToAuditMap(InvoiceItem model) {
		modelMapper.getConfiguration().setAmbiguityIgnored(true);
		InvoiceItemAudit invoiceAudit = modelMapper.map(model, InvoiceItemAudit.class );//modelToDTOWithoutItemMap(model);
		modelMapper.getConfiguration().setAmbiguityIgnored(false);
		return invoiceAudit;
	}


	public List<InvoiceItemAudit> modelToAuditList(List<InvoiceItem> modelList) {

		List<InvoiceItemAudit> invoiceAuditListOut = modelList.stream()
				.map(
						invoiceItem -> modelToAuditMap(invoiceItem))
				.collect(Collectors.toList());

		return invoiceAuditListOut;
	}
}
