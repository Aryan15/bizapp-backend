package com.coreerp.mapper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dao.EmployeeTypeRepository;
import com.coreerp.dao.NamePrefixRepository;
import com.coreerp.dao.PersonGenderRepository;
import com.coreerp.dao.RoleRepository;
import com.coreerp.dao.SecurityQuestionRepository;
import com.coreerp.dto.UserDTO;
import com.coreerp.model.Role;
import com.coreerp.model.User;
import com.coreerp.service.AreaService;
import com.coreerp.service.CityService;
import com.coreerp.service.CompanyService;
import com.coreerp.service.CountryService;
import com.coreerp.service.StateService;

@Component
public class   UserMapper implements AbstractMapper<UserDTO, User> {

//	@Autowired
//	ModelMapper modelMapper;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	CityService cityService;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	CountryService countryService;
	
	@Autowired
	EmployeeTypeRepository employeeTypeRepository;
	
	@Autowired
	PersonGenderRepository personGenderRepository;
	
	@Autowired
	NamePrefixRepository NamePrefixRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	SecurityQuestionRepository securityQuestionRepository;
	
	@Autowired
	StateService stateService;
	
	@Override
	public UserDTO modelToDTOMap(User model) {
		//return modelMapper.map(model, UserDTO.class);
		UserDTO dto = new UserDTO();
		
		dto.setActive(model.getActive());
		dto.setAreaId(model.getArea() != null ? model.getArea().getId() : null);
		dto.setCityId(model.getCity() != null ? model.getCity().getId() : null);
		dto.setComments(model.getComments());
		dto.setCompanyId(model.getCompany().getId());
		dto.setCountryId(model.getCountry() != null ? model.getCountry().getId() : null);
		dto.setCurrentAddress(model.getCurrentAddress());
		dto.setDateOfBirth(model.getDateOfBirth());
		dto.setDateOfJoin(model.getDateOfJoin());
		dto.setDateOfLeaving(model.getDateOfLeaving());
		dto.setDeleted(model.getDeleted());
		dto.setDisplayName(model.getDisplayName());
		dto.setEmail(model.getEmail());
		dto.setEmployeeId(model.getEmployeeId());
		dto.setEmployeeNumber(model.getEmployeeNumber());
		dto.setEmployeeTypeId(model.getEmployeeType().getId());
		dto.setGenderId(model.getGender() != null ? model.getGender().getId() : null);
		dto.setId(model.getId());
		dto.setIsLogInRequired(model.getIsLogInRequired());
		dto.setLastName(model.getLastName());
		dto.setMobileNumber(model.getMobileNumber());
		dto.setTelephoneNumber(model.getTelephoneNumber());
		dto.setName(model.getName());
		dto.setPanNumber(model.getPanNumber());
		dto.setPassword(model.getPassword());
		dto.setPermanentAddress(model.getPermanentAddress());
		dto.setPinCode(model.getPinCode());
		dto.setPrefixId(model.getNamePrefix() != null ? model.getNamePrefix().getId() : null);
		dto.setRoleIds(model.getRoles().stream().map(role -> role.getId()).collect(Collectors.toList()));
		dto.setUsername(model.getUsername());
		dto.setUserThemeName(model.getUserThemeName());
		dto.setUserLanguage(model.getUserLanguage());
		return dto;
		
	}

	@Override
	public User dtoToModelMap(UserDTO dto) {
		
		User model = new User();
		
		model.setActive(dto.getActive());
		model.setArea(dto.getAreaId() != null ? areaService.getAreaById(dto.getAreaId()) : null);
		model.setCity(dto.getCityId() != null ? cityService.getCityById(dto.getCityId()) : null);
		model.setComments(dto.getComments());
		model.setCompany(companyService.getModelById(dto.getCompanyId()));
		model.setCountry(dto.getCountryId() != null ? countryService.getCountryById(dto.getCountryId()) : null);
		model.setCurrentAddress(dto.getCurrentAddress());
		model.setDateOfBirth(dto.getDateOfBirth());
		model.setDateOfJoin(dto.getDateOfJoin());
		model.setDateOfLeaving(dto.getDateOfLeaving());
		model.setDeleted(dto.getDeleted());
		model.setDisplayName(dto.getDisplayName());
		model.setEmail(dto.getEmail());
		model.setEmployeeId(dto.getEmployeeId());
		model.setEmployeeNumber(dto.getEmployeeNumber());
		model.setEmployeeType(dto.getEmployeeTypeId() != null ? employeeTypeRepository.getOne(dto.getEmployeeTypeId()) : null);
		model.setGender(dto.getGenderId() != null ? personGenderRepository.getOne(dto.getGenderId()) : null);
		model.setId(dto.getId());
		model.setIsLogInRequired(dto.getIsLogInRequired());
		model.setLastName(model.getLastName());
		model.setMobileNumber(dto.getMobileNumber());
		model.setName(dto.getName());
		model.setNamePrefix(dto.getPrefixId() != null ? NamePrefixRepository.getOne(dto.getPrefixId()) : null);
		model.setPanNumber(dto.getPanNumber());
		model.setPassword(dto.getPassword());
		model.setPermanentAddress(dto.getPermanentAddress());
		model.setPinCode(dto.getPinCode());
		model.setRoles(new HashSet<Role>(roleRepository.findAllById(dto.getRoleIds()))); //new HashSet<Role>(roleRepository.findAll(dto.getRoleIds())));
		model.setSecurityAnswer(dto.getSecurityAnswer());
		model.setSecurityQuestion(dto.getSecurityQuestionId() != null ? securityQuestionRepository.getOne(dto.getSecurityQuestionId()) : null);
		model.setState(dto.getStateId() != null ? stateService.getStateById(dto.getStateId()) : null);
		model.setTelephoneNumber(dto.getTelephoneNumber());
		model.setUsername(dto.getUsername());
		model.setUserThemeName(dto.getUserThemeName());
		model.setUserLanguage(dto.getUserLanguage());
		return model;
		
	}
	
//	private Set<Role> getRoles(Set<Role> roles){
//		
//		
//		//role -> roleRepository.getOne(role.id)).collect(Collectors.toSet()
//
//		Set<Role> rolesOut = new HashSet<Role>();
//		
//		for(Role role : roles){
//			rolesOut.add(roleRepository.getOne(role.getId()));
//		}
//		
//		return rolesOut;
////		
////		return roles.stream()
////					.map(role -> roleRepository.getOne(role.id))
////					.collect(Collectors.toSet());
//	}

	@Override
	public List<UserDTO> modelToDTOList(List<User> modelList) {
		return modelList.stream()
					.map(model -> modelToDTOMap(model))
					.collect(Collectors.toList());
	}

	@Override
	public List<User> dtoToModelList(List<UserDTO> dtoList) {
		return dtoList.stream()
					.map(dto -> dtoToModelMap(dto))
					.collect(Collectors.toList());
	}

}
