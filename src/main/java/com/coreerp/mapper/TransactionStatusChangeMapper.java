package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dto.TransactionStatusChangeDTO;
import com.coreerp.model.TransactionStatusChange;

@Component
public class TransactionStatusChangeMapper
		implements AbstractMapper<TransactionStatusChangeDTO, TransactionStatusChange> {

	@Autowired
    private ModelMapper modelMapper;
	
//	@Autowired
//	private TransactionTypeRepository transactionTypeRepository; 
	
	@Override
	public TransactionStatusChangeDTO modelToDTOMap(TransactionStatusChange model) {
		
		TransactionStatusChangeDTO dto = modelMapper.map(model, TransactionStatusChangeDTO.class);
		
		dto.setSourceTransactionTypeName(model.getSourceTransactionType() != null ? model.getSourceTransactionType().getName() : null);
		dto.setStatusName(model.getStatus() != null ? model.getStatus().getName() : null);
		dto.setTransactionTypeName(model.getTransactionType() != null ? model.getTransactionType().getName() : null);
		
		return dto;
	}

	@Override
	public TransactionStatusChange dtoToModelMap(TransactionStatusChangeDTO dto) {
		return modelMapper.map(dto, TransactionStatusChange.class);
	}

	@Override
	public List<TransactionStatusChangeDTO> modelToDTOList(List<TransactionStatusChange> modelList) {
		return modelList.stream()
				.map(material -> modelToDTOMap(material))
				.collect(Collectors.toList());
	}

	@Override
	public List<TransactionStatusChange> dtoToModelList(List<TransactionStatusChangeDTO> dtoList) {
		return dtoList.stream()
				.map(materialDTO -> dtoToModelMap(materialDTO))
				.collect(Collectors.toList());
	}

}
