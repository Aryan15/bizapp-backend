package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.model.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dao.ExpenseRepository;
import com.coreerp.dto.VoucherItemDTO;

@Component
public class VoucherItemMapper implements AbstractMapper<VoucherItemDTO, VoucherItem> {

	final static Logger log=LogManager.getLogger(VoucherItemMapper.class);
	
	
	
	@Autowired
	ExpenseRepository expenseRepository;

	@Autowired
	ModelMapper modelMapper;

	
	@Override
	public VoucherItemDTO modelToDTOMap(VoucherItem model) {
           
		VoucherItemDTO voucherItemDTOOut=new VoucherItemDTO();
		
		voucherItemDTOOut.setId(model.getId());
		voucherItemDTOOut.setVoucherHeaderId(model.getVoucherHeader().getId());
		voucherItemDTOOut.setAmount(model.getAmount());
		voucherItemDTOOut.setDescription(model.getDescription());
		voucherItemDTOOut.setExpenseHeaderId(model.getExpenseHeader() != null ? model.getExpenseHeader().getId() : null);
		voucherItemDTOOut.setExpenseName(model.getExpenseHeader().getName());
        
        
        return voucherItemDTOOut;
	}

	@Override
	public VoucherItem dtoToModelMap(VoucherItemDTO dto) {

		log.info("in dtoToModelMap: "+dto.getId());
		VoucherItem voucherItemOut=new VoucherItem();
		
		voucherItemOut.setId(dto.getId());
		//voucherItemOut.setVoucherHeaderId(dto.getVoucherHeader().getId());
		voucherItemOut.setAmount(dto.getAmount());
		voucherItemOut.setDescription(dto.getDescription());
		voucherItemOut.setExpenseHeader(dto.getExpenseHeaderId()!= null? expenseRepository.getOne(dto.getExpenseHeaderId()):null);
		//voucherItemOut.setExpenseName(dto.getExpenseHeaderId()!= null? expenseRepository.getOne(dto.getId()):null);
        
       log.info("voucherItemOut.id: "+voucherItemOut.getId());
       
       return voucherItemOut; 
	}

	@Override
	public List<VoucherItemDTO> modelToDTOList(List<VoucherItem> modelList) {
     

		return modelList.stream()
				.map(voucherItem -> modelToDTOMap(voucherItem))
				.collect(Collectors.toList());
		
	}

	@Override
	public List<VoucherItem> dtoToModelList(List<VoucherItemDTO> dtoList) {

		log.info("dtoToModelList..incoming item size: "+dtoList.size());

			
			return dtoList.stream()
					.map(voucherItemDTO -> dtoToModelMap(voucherItemDTO))
					.collect(Collectors.toList());
	}


	public VoucherItemAudit modelToAudit(VoucherItem voucher){
		modelMapper.getConfiguration().setAmbiguityIgnored(true);
		VoucherItemAudit out = modelMapper.map(voucher, VoucherItemAudit.class);
		modelMapper.getConfiguration().setAmbiguityIgnored(false);
		return out;
	}

	public List<VoucherItemAudit> modelToAuditList(List<VoucherItem> dtoList){
		return dtoList.stream()
				.map(dto -> modelToAudit(dto))
				.collect(Collectors.toList());
	}

}
