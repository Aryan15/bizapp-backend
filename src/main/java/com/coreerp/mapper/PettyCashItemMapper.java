package com.coreerp.mapper;


import com.coreerp.dao.CategoryRepository;
import com.coreerp.dao.PaymentMethodRepository;
import com.coreerp.dao.TagRepository;
import com.coreerp.dto.PettyCashHeaderDTO;
import com.coreerp.dto.PettyCashItemDTO;
import com.coreerp.model.*;
import com.coreerp.serviceimpl.PettyCashServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PettyCashItemMapper  implements AbstractMapper<PettyCashItemDTO, PettyCashItem> {
    final static Logger log = LogManager.getLogger(PettyCashItemMapper.class);

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    PaymentMethodRepository paymentMethodRepository;

    @Override
    public PettyCashItem dtoToModelMap(PettyCashItemDTO pettyCashItemDTO){
       log.info(pettyCashItemDTO+"hhfhhfhhfhfhhf");
        PettyCashItem pettyCashItem = new PettyCashItem();
        pettyCashItem.setId(pettyCashItemDTO.getId());
       // pettyCashItem.setPettyCashHeader(pettyCashItemDTO.getHeaderId());
        pettyCashItem.setCategory(pettyCashItemDTO.getCategoryId()!=null ?categoryRepository.getOne(pettyCashItemDTO.getCategoryId()):null);

        pettyCashItem.setIncomingAmount(pettyCashItemDTO.getIncomingAmount());
        pettyCashItem.setOutgoingAmount(pettyCashItemDTO.getOutgoingAmount());
        pettyCashItem.setPaymentMethod(pettyCashItemDTO.getPaymentMethodId()!=null ? paymentMethodRepository.getOne(pettyCashItemDTO.getPaymentMethodId()):null);

        pettyCashItem.setRemarks(pettyCashItemDTO.getRemarks());

        pettyCashItem.setTag(pettyCashItemDTO.getTagId()!=null ? tagRepository.getOne(pettyCashItemDTO.getTagId()):null);



        return pettyCashItem;

    }

    @Override

    public PettyCashItemDTO modelToDTOMap (PettyCashItem pettyCashItem){
        PettyCashItemDTO pettyCashItemDTO = new PettyCashItemDTO();
        pettyCashItemDTO.setId(pettyCashItem.getId());
        pettyCashItemDTO.setCategoryId(pettyCashItem.getCategory()!=null ?pettyCashItem.getCategory().getId():null);
       pettyCashItemDTO.setCategoryName(pettyCashItem.getCategory().getName());
        pettyCashItemDTO.setIncomingAmount(pettyCashItem.getIncomingAmount());
        pettyCashItemDTO.setOutgoingAmount(pettyCashItem.getOutgoingAmount());
        pettyCashItemDTO.setPaymentMethodId(pettyCashItem.getPaymentMethod()!=null ?pettyCashItem.getPaymentMethod().getId():null);
        pettyCashItemDTO.setPaymentMethodName(pettyCashItem.getPaymentMethod().getName());
        pettyCashItemDTO.setHeaderId(pettyCashItem.getPettyCashHeader().getId());


        pettyCashItemDTO.setRemarks(pettyCashItem.getRemarks());

        pettyCashItemDTO.setTagId(pettyCashItem.getTag()!=null ?pettyCashItem.getTag().getId():null);
        pettyCashItemDTO.setTagName(pettyCashItem.getTag().getName());
        return pettyCashItemDTO;
    }


    @Override
    public List<PettyCashItemDTO> modelToDTOList(List<PettyCashItem> modelList) {

        List<PettyCashItemDTO> pettyCashItemDTOS =
                modelList.stream()
                        .map(PettyCashItem -> modelToDTOMap(PettyCashItem))
                        .collect(Collectors.toList());

        return pettyCashItemDTOS;
    }

    @Override
    public List<PettyCashItem> dtoToModelList(List<PettyCashItemDTO> dtoList) {

        List<PettyCashItem>  pettyCashItems =
                dtoList.stream()
                        .map( PettyCashItemDTO-> dtoToModelMap(PettyCashItemDTO))
                        .collect(Collectors.toList());

        return pettyCashItems;
    }


    public PettyCashItemAudit modelToAudit(PettyCashItem pettyCashItem){
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PettyCashItemAudit out = modelMapper.map(pettyCashItem, PettyCashItemAudit.class);
        modelMapper.getConfiguration().setAmbiguityIgnored(false);
        return out;
    }

    public List<PettyCashItemAudit> modelToAuditList(List<PettyCashItem> dtoList){
        return dtoList.stream()
                .map(dto -> modelToAudit(dto))
                .collect(Collectors.toList());
    }
}
