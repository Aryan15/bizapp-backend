package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dao.PartyBankMapRepository;
import com.coreerp.dto.CompanyDTO;
import com.coreerp.model.Company;
import com.coreerp.model.PartyBankMap;

@Component
public class CompanyMapper implements AbstractMapper<CompanyDTO, Company> {

	@Autowired
	private PartyBankMapRepository partyBankMapRepository;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public CompanyDTO modelToDTOMap(Company model) {
		
		CompanyDTO companyDTO = modelMapper.map(model, CompanyDTO.class);
		companyDTO.setAddressesListDTO(model.getAddresses ());
		companyDTO.setStateCode(model.getState().getStateCode());
		companyDTO.setStateName(model.getState().getName());
		companyDTO.setTagLine(model.getTagLine());
		
		List<PartyBankMap> partyBankMaps =  partyBankMapRepository.findByCompanyId(model.getId());
		if(partyBankMaps.size() > 0) {
			companyDTO.setBank(partyBankMaps.get(0).getBank().getBankName());
			companyDTO.setBranch(partyBankMaps.get(0).getBranch());
			companyDTO.setAccount(partyBankMaps.get(0).getAccountNumber());
			companyDTO.setIfsc(partyBankMaps.get(0).getIfsc());
			companyDTO.setBankAdCode(partyBankMaps.get(0).getBankAdCode());
			
		}
		return companyDTO;
	}

	@Override
	public Company dtoToModelMap(CompanyDTO dto) {
		Company company = modelMapper.map(dto, Company.class);
		return company;
	}

	@Override
	public List<CompanyDTO> modelToDTOList(List<Company> modelList) {
		return modelList.stream()
				.map(company -> modelToDTOMap(company))
				.collect(Collectors.toList());
	}

	@Override
	public List<Company> dtoToModelList(List<CompanyDTO> dtoList) {
		return dtoList.stream()
				.map(companyDTO -> dtoToModelMap(companyDTO))
				.collect(Collectors.toList());
	}

}
