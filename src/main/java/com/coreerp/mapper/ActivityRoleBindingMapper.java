package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dao.ActivityRepository;
import com.coreerp.dao.RoleRepository;
import com.coreerp.dto.ActivityRoleBindingDTO;
import com.coreerp.model.ActivityRoleBinding;
import com.coreerp.service.CompanyService;
import com.coreerp.service.UserService;

@Component
public class ActivityRoleBindingMapper implements AbstractMapper<ActivityRoleBindingDTO, ActivityRoleBinding> {

	@Autowired
	UserMapper userMapper;
	
	@Autowired 
	ActivityRepository activityRepository;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	RoleRepository roleRepository;
	
	
	@Override
	public ActivityRoleBindingDTO modelToDTOMap(ActivityRoleBinding model) {
		ActivityRoleBindingDTO dto = new ActivityRoleBindingDTO();
		
		dto.setDeleted(model.getDeleted());
		dto.setId(model.getId());
		dto.setActivityId(model.getActivity().getId());
		dto.setCompanyId(model.getCompany() != null ? model.getCompany().getId() : null);
		dto.setPermissionToApprove(model.getPermissionToApprove());
		dto.setPermissionToCancel(model.getPermissionToCancel());
		dto.setPermissionToCreate(model.getPermissionToCreate());
		dto.setPermissionToDelete(model.getPermissionToDelete());
		dto.setPermissionToPrint(model.getPermissionToPrint());
		dto.setPermissionToUpdate(model.getPermissionToUpdate());
		//dto.setRole(model.getRole());
		//dto.setUser(userMapper.modelToDTOMap(model.getUser()));
		dto.setRoleId(model.getRole().getId());
		dto.setUserId(model.getUser() != null ? model.getUser().getId() : null);
		
		return dto;
	}

	@Override
	public ActivityRoleBinding dtoToModelMap(ActivityRoleBindingDTO dto) {

		ActivityRoleBinding model = new ActivityRoleBinding();
		
		//model.setActivity(dto.get);
		//model.setCompany(dto.get);
		model.setActivity(activityRepository.getOne(dto.getActivityId()));
		model.setCompany(companyService.getModelById(dto.getCompanyId()));
		model.setDeleted(dto.getDeleted());
		model.setId(dto.getId());
		model.setPermissionToApprove(dto.getPermissionToApprove());
		model.setPermissionToCancel(dto.getPermissionToCancel());
		model.setPermissionToCreate(dto.getPermissionToCreate());
		model.setPermissionToDelete(dto.getPermissionToDelete());
		model.setPermissionToPrint(dto.getPermissionToPrint());
		model.setPermissionToUpdate(dto.getPermissionToUpdate());
		//model.setRole(dto.getRole());
		//model.setUser(userService.getUserById(dto.getUser().getId()));
		model.setRole(roleRepository.getOne(dto.getRoleId()));
		model.setUser(dto.getUserId()!= null ? userService.getUserById(dto.getUserId()) : null);
		
		return model;
	}

	@Override
	public List<ActivityRoleBindingDTO> modelToDTOList(List<ActivityRoleBinding> modelList) {
		return modelList.stream()
				.map(company -> modelToDTOMap(company))
				.collect(Collectors.toList());
	}

	@Override
	public List<ActivityRoleBinding> dtoToModelList(List<ActivityRoleBindingDTO> dtoList) {
		return dtoList.stream()
				.map(companyDTO -> dtoToModelMap(companyDTO))
				.collect(Collectors.toList());
	}

}
