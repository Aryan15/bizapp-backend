package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.AccountClassDTO;
import com.coreerp.model.AccountClass;

@Component
public class AccountClassMapper implements AbstractMapper<AccountClassDTO, AccountClass> {
	
	@Autowired
	ModelMapper modelMapper;

	@Override
	public AccountClassDTO modelToDTOMap(AccountClass model) {
		

		return modelMapper.map(model, AccountClassDTO.class);
	}

	@Override
	public AccountClass dtoToModelMap(AccountClassDTO dto) {
		return modelMapper.map(dto, AccountClass.class);
	}

	@Override
	public List<AccountClassDTO> modelToDTOList(List<AccountClass> modelList) {
		return modelList.stream()
					.map(model -> modelToDTOMap(model))
					.collect(Collectors.toList());
	}

	@Override
	public List<AccountClass> dtoToModelList(List<AccountClassDTO> dtoList) {
		return dtoList.stream()
				.map(dto -> dtoToModelMap(dto))
				.collect(Collectors.toList());
	}

}
