package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.controller.GlobalSettingController;
import com.coreerp.dao.StockUpdateOnRepository;
import com.coreerp.dto.GlobalSettingDTO;
import com.coreerp.model.GlobalSetting;
import com.coreerp.service.FinancialYearService;

@Component
public class GlobalSettingMapper implements AbstractMapper<GlobalSettingDTO, GlobalSetting> {

	final static Logger log= LogManager.getLogger(GlobalSettingMapper.class);
	
//	@Autowired
//	private ModelMapper modelMapper;
	
	@Autowired
	FinancialYearService financialYearService;
	
	@Autowired
	StockUpdateOnRepository stockUpdateOnRepository;
	
	@Override
	public GlobalSettingDTO modelToDTOMap(GlobalSetting model) {
		log.info("mapping model: "+model);
        GlobalSettingDTO dto = new GlobalSettingDTO();//modelMapper.map(model, GlobalSettingDTO.class);
        if(model != null){
	        dto.setBarCodeStatus(model.getBarCodeStatus());
	        dto.setBrowserPath(model.getBrowserPath());
	        dto.setCgstPercentage(model.getCgstPercentage());
	        dto.setCutomerMateriaBinding(model.getCustomerMaterialBinding());
	        dto.setDeleted(model.getDeleted());
	        dto.setDisableItemGroup(model.getDisableItemGroup());
	        dto.setFinancialYearId(model.getFinancialYear() != null ? model.getFinancialYear().getId() : null);
	        dto.setHelpDocument(model.getHelpDocument());
	        dto.setId(model.getId());
	        dto.setIncludeSignatureStatus(model.getIncludeSignatureStatus());
	        dto.setInclusiveTax(model.getInclusiveTax());
	        dto.setItemLevelDiscount(model.getItemLevelDiscount());
	        dto.setItemLevelTax(model.getItemLevelTax());
	        dto.setPrintStatus(model.getPrintStatus());
	        dto.setPurchasePrice(model.getPurchasePrice());
	        dto.setSalePrice(model.getSalePrice());
	        dto.setSgstPercentage(model.getSgstPercentage());
	        dto.setShowOnlyCustomerMaterialInSales(model.getShowOnlyCustomerMaterialInSales());
	        dto.setSpecialPriceCalculation(model.getSpecialPriceCalculation());
	        dto.setStockDecreaseOnId(model.getStockUpdateDecrease() != null ? model.getStockUpdateDecrease().getId() : null);
	        dto.setStockIncreaseOnId(model.getStockUpdateIncrease() != null ? model.getStockUpdateIncrease().getId() : null);
	        dto.setRoundOffTaxTransaction(model.getRoundOffTaxTransaction());
	        dto.setStockCheckRequired(model.getStockCheckRequired());
			dto.setItemLevelTaxPurchase(model.getItemLevelTaxPurchase() != null ? model.getItemLevelTaxPurchase() : model.getItemLevelTax());
			dto.setItemLevelTaxPurchase(model.getItemLevelTaxPurchase());
			dto.setEnableTally(model.getEnableTally());
			dto.setAllowReuse(model.getAllowReuse());
			dto.setAllowPartyCode(model.getAllowPartyCode());
			dto.setAllowEwayBill(model.getAllowEwayBill());
	        //log.info("why is this null? "+ model.getStockUpdateDecrease());
        }
        log.info("returning dto: "+dto);
        
		return dto;
	}

	@Override
	public GlobalSetting dtoToModelMap(GlobalSettingDTO dto) {
         //GlobalSetting globalSetting=modelMapper.map(dto, GlobalSetting.class);
		
		GlobalSetting model = new GlobalSetting ();
		
		model.setBarCodeStatus(dto.getBarCodeStatus());
		model.setBrowserPath(dto.getBrowserPath());
		model.setCgstPercentage(dto.getCgstPercentage());
		model.setCustomerMaterialBinding(dto.getCutomerMateriaBinding());
		model.setDeleted(dto.getDeleted());
		model.setDisableItemGroup(dto.getDisableItemGroup());
		model.setFinancialYear(dto.getFinancialYearId() != null ? financialYearService.getById(dto.getFinancialYearId()) : null);
		model.setHelpDocument(dto.getHelpDocument());
		model.setId(dto.getId());
		model.setIncludeSignatureStatus(dto.getIncludeSignatureStatus());
		model.setInclusiveTax(dto.getInclusiveTax());
		model.setItemLevelDiscount(dto.getItemLevelDiscount());
		model.setItemLevelTax(dto.getItemLevelTax());
		model.setPrintStatus(dto.getPrintStatus());
		model.setPurchasePrice(dto.getPurchasePrice());
		model.setSalePrice(dto.getSalePrice());
		model.setSgstPercentage(dto.getSgstPercentage());
		model.setShowOnlyCustomerMaterialInSales(dto.getShowOnlyCustomerMaterialInSales());
		model.setSpecialPriceCalculation(dto.getSpecialPriceCalculation());
		model.setStockUpdateDecrease(dto.getStockDecreaseOnId() != null ? stockUpdateOnRepository.getOne(dto.getStockDecreaseOnId()) : null);
		model.setStockUpdateIncrease(dto.getStockIncreaseOnId() != null ? stockUpdateOnRepository.getOne(dto.getStockIncreaseOnId()) : null);
		model.setRoundOffTaxTransaction(dto.getRoundOffTaxTransaction());
		model.setStockCheckRequired(dto.getStockCheckRequired());
		model.setItemLevelTaxPurchase(dto.getItemLevelTaxPurchase());
		model.setItemLevelTaxPurchase(dto.getItemLevelTaxPurchase());
		model.setEnableTally(dto.getEnableTally());
		model.setAllowReuse(dto.getAllowReuse());
		model.setAllowPartyCode(dto.getAllowPartyCode());
		model.setAllowEwayBill(dto.getAllowEwayBill());

		return model;
	}

	@Override
	public List<GlobalSettingDTO> modelToDTOList(List<GlobalSetting> modelList) {
		return modelList.stream()
				.map(globlaSetting -> modelToDTOMap(globlaSetting))
				.collect(Collectors.toList());
	}

	@Override
	public List<GlobalSetting> dtoToModelList(List<GlobalSettingDTO> dtoList) {
		return dtoList.stream()
				.map(globalSettingDTO -> dtoToModelMap(globalSettingDTO))
				.collect(Collectors.toList());
	}

	

}
