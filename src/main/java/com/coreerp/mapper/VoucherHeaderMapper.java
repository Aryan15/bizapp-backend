package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.model.VoucherHeaderAudit;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.VoucherHeaderDTO;
import com.coreerp.model.VoucherHeader;
import com.coreerp.service.CompanyService;
import com.coreerp.service.DeliveryChallanService;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.InvoiceService;
import com.coreerp.service.PartyService;
import com.coreerp.service.PurchaseOrderService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TaxService;
import com.coreerp.service.TermsAndConditionService;
import com.coreerp.service.TransactionNumberService;
import com.coreerp.service.TransactionTypeService;
import com.coreerp.service.UserService;

@Component
public class VoucherHeaderMapper  implements AbstractMapper<VoucherHeaderDTO,VoucherHeader>{

	final static Logger log=LogManager.getLogger(VoucherHeaderMapper.class);
	
	@Autowired
	VoucherItemMapper voucherItemMapper;
	
	
	@Autowired
	CompanyService companyService;
	
	
	@Autowired
	FinancialYearService financialYearService;
	

	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	TransactionNumberService transactionNumberService;

	@Autowired
	ModelMapper modelMapper;
	
	@Override
	public VoucherHeaderDTO modelToDTOMap(VoucherHeader model) {
     
		VoucherHeaderDTO voucherHeaderDTO=new VoucherHeaderDTO();
		voucherHeaderDTO.setId(model.getId());
		voucherHeaderDTO.setAmount(model.getAmount());
		voucherHeaderDTO.setVoucherNumber(model.getVoucherNumber());
		voucherHeaderDTO.setVoucherDate(model.getVoucherDate());
		voucherHeaderDTO.setVoucherTypeId(model.getVoucherType().getId());
		voucherHeaderDTO.setVoucherId(model.getVoucherId());
		voucherHeaderDTO.setPaidTo(model.getPaidTo());
		voucherHeaderDTO.setBankName(model.getBankName());
		voucherHeaderDTO.setChequeDate(model.getChequeDate());
		voucherHeaderDTO.setChequeNumber(model.getChequeNumber());
		voucherHeaderDTO.setCompanyId(model.getCompany().getId());
		voucherHeaderDTO.setFinancialYearId(model.getFinancialYear().getId());
		log.info("Items...");
		//log.info(model.getVoucherItems());
		voucherHeaderDTO.setVoucherItems(voucherItemMapper.modelToDTOList(model.getVoucherItems()));
		log.info("item size: "+voucherHeaderDTO.getVoucherItems().size());
		
		voucherHeaderDTO.setCompanyName(model.getCompanyName());
		voucherHeaderDTO.setCompanygGstRegistrationTypeId(model.getCompanygGstRegistrationTypeId());
		voucherHeaderDTO.setCompanyPinCode(model.getCompanyPinCode());
		voucherHeaderDTO.setCompanyStateId(model.getCompanyStateId());
		voucherHeaderDTO.setCompanyStateName(model.getCompanyStateName());
		voucherHeaderDTO.setCompanyCountryId(model.getCompanyCountryId());
		voucherHeaderDTO.setCompanyPrimaryMobile(model.getCompanyPrimaryMobile());
		voucherHeaderDTO.setCompanySecondaryMobile(model.getCompanySecondaryMobile());
		voucherHeaderDTO.setCompanyContactPersonNumber(model.getCompanyContactPersonNumber());
		voucherHeaderDTO.setCompanyContactPersonName(model.getCompanyContactPersonName());
		voucherHeaderDTO.setCompanyPrimaryTelephone(model.getCompanyPrimaryTelephone());
		voucherHeaderDTO.setCompanySecondaryTelephone(model.getCompanySecondaryTelephone());
		voucherHeaderDTO.setCompanyWebsite(model.getCompanyWebsite());
		voucherHeaderDTO.setCompanyEmail(model.getCompanyEmail());
		voucherHeaderDTO.setCompanyFaxNumber(model.getCompanyFaxNumber());
		voucherHeaderDTO.setCompanyAddress(model.getCompanyAddress());
		voucherHeaderDTO.setCompanytagLine(model.getCompanytagLine());
		voucherHeaderDTO.setCompanyGstNumber(model.getCompanyGstNumber());
		voucherHeaderDTO.setCompapanNumber(model.getCompapanNumber());
		voucherHeaderDTO.setCompanyPanDate(model.getCompanyPanDate());
		voucherHeaderDTO.setCompanyCeritificateImagePath(model.getCompanyCeritificateImagePath());
		voucherHeaderDTO.setCompanyLogoPath(model.getCompanyLogoPath());
		voucherHeaderDTO.setCreatedBy(model.getCreatedBy());
		voucherHeaderDTO.setUpdateBy(model.getUpdatedBy());
		voucherHeaderDTO.setCreatedDate(model.getCreatedDateTime());
		voucherHeaderDTO.setUpdatedDate(model.getUpdatedDateTime());
		
		return voucherHeaderDTO;
	}

	@Override
	public VoucherHeader dtoToModelMap(VoucherHeaderDTO dto) {
		
		VoucherHeader voucherHeaderOut=new VoucherHeader();
		
		//Not setting Id
		voucherHeaderOut.setId(dto.getId());
		voucherHeaderOut.setAmount(dto.getAmount());
		voucherHeaderOut.setVoucherNumber(dto.getVoucherNumber() != null ? dto.getVoucherNumber() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getVoucherTypeId())).getTransactionNumber());
		voucherHeaderOut.setVoucherDate(dto.getVoucherDate());
		voucherHeaderOut.setVoucherType(transactionTypeService.getModelById(dto.getVoucherTypeId()));
		voucherHeaderOut.setVoucherId(dto.getVoucherId() != null ? dto.getVoucherId() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getVoucherTypeId())).getTransactionId());
		voucherHeaderOut.setPaidTo(dto.getPaidTo());
		voucherHeaderOut.setBankName(dto.getBankName());
		voucherHeaderOut.setChequeDate(dto.getChequeDate());
		voucherHeaderOut.setChequeNumber(dto.getChequeNumber());
		voucherHeaderOut.setFinancialYear(dto.getFinancialYearId() !=null ? financialYearService.getById(dto.getFinancialYearId()) :null);
		voucherHeaderOut.setCompany(dto.getCompanyId()!=null ? companyService.getModelById(dto.getCompanyId()):null);
		voucherHeaderOut.setVoucherItems(voucherItemMapper.dtoToModelList(dto.getVoucherItems()));
		
		voucherHeaderOut.setCompanyName(dto.getCompanyName());
		voucherHeaderOut.setCompanygGstRegistrationTypeId(dto.getCompanygGstRegistrationTypeId());
		voucherHeaderOut.setCompanyPinCode(dto.getCompanyPinCode());
		voucherHeaderOut.setCompanyStateId(dto.getCompanyStateId());
		voucherHeaderOut.setCompanyStateName(dto.getCompanyStateName());
		voucherHeaderOut.setCompanyCountryId(dto.getCompanyCountryId());
		voucherHeaderOut.setCompanyPrimaryMobile(dto.getCompanyPrimaryMobile());
		voucherHeaderOut.setCompanySecondaryMobile(dto.getCompanySecondaryMobile());
		voucherHeaderOut.setCompanyContactPersonNumber(dto.getCompanyContactPersonNumber());
		voucherHeaderOut.setCompanyContactPersonName(dto.getCompanyContactPersonName());
		voucherHeaderOut.setCompanyPrimaryTelephone(dto.getCompanyPrimaryTelephone());
		voucherHeaderOut.setCompanySecondaryTelephone(dto.getCompanySecondaryTelephone());
		voucherHeaderOut.setCompanyWebsite(dto.getCompanyWebsite());
		voucherHeaderOut.setCompanyEmail(dto.getCompanyEmail());
		voucherHeaderOut.setCompanyFaxNumber(dto.getCompanyFaxNumber());
		voucherHeaderOut.setCompanyAddress(dto.getCompanyAddress());
		voucherHeaderOut.setCompanytagLine(dto.getCompanytagLine());
		voucherHeaderOut.setCompanyGstNumber(dto.getCompanyGstNumber());
		voucherHeaderOut.setCompapanNumber(dto.getCompapanNumber());
		voucherHeaderOut.setCompanyPanDate(dto.getCompanyPanDate());
		voucherHeaderOut.setCompanyCeritificateImagePath(dto.getCompanyCeritificateImagePath());
		voucherHeaderOut.setCompanyLogoPath(dto.getCompanyLogoPath());
		voucherHeaderOut.setCreatedBy(dto.getCreatedBy());
		voucherHeaderOut.setCreatedDateTime(dto.getCreatedDate());
		
		return voucherHeaderOut;
	}

	@Override
	public List<VoucherHeaderDTO> modelToDTOList(List<VoucherHeader> modelList) {

		return modelList.stream()
				.map(voucherHeader -> modelToDTOMap(voucherHeader))
				.collect(Collectors.toList());
    		  
	}

	@Override
	public List<VoucherHeader> dtoToModelList(List<VoucherHeaderDTO> dtoList) {
	  
		return dtoList.stream()
				.map(voucherDTO -> dtoToModelMap(voucherDTO))
				.collect(Collectors.toList());
	
	}

	public VoucherHeaderAudit modelToAudit(VoucherHeader voucher){
		modelMapper.getConfiguration().setAmbiguityIgnored(true);
		VoucherHeaderAudit out = modelMapper.map(voucher, VoucherHeaderAudit.class);
		modelMapper.getConfiguration().setAmbiguityIgnored(false);
		return out;
	}

	public List<VoucherHeaderAudit> modelToAuditList(List<VoucherHeader> dtoList){
		return dtoList.stream()
				.map(dto -> modelToAudit(dto))
				.collect(Collectors.toList());
	}
	
}

