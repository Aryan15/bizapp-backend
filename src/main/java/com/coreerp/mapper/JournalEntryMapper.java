package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.JournalEntryDTO;
import com.coreerp.model.JournalEntry;
import com.coreerp.service.AccountService;
import com.coreerp.service.PartyService;
import com.coreerp.service.TransactionTypeService;

@Component
public class JournalEntryMapper implements AbstractMapper<JournalEntryDTO, JournalEntry> {

	@Autowired
	AccountService accountService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Override
	public JournalEntryDTO modelToDTOMap(JournalEntry model) {
		JournalEntryDTO dto = new JournalEntryDTO();
		
		dto.setAmount(model.getAmount());
		dto.setBusinessDate(model.getBusinessDate());
		dto.setCreditAccountId(model.getCreditAccount().getId());
		dto.setDebitAccountId(model.getDebitAccount().getId());
		dto.setId(model.getId());
		dto.setIsReversal(model.getIsReversal());
		dto.setPartyId(model.getParty().getId());
		dto.setRemarks(model.getRemarks());
		dto.setTransactionDate(model.getTransactionDate());
		dto.setTransactionId(model.getTransactionId());
		dto.setTransactionIdSequence(model.getTransactionIdSequence());
		dto.setTransactionTypeId(model.getTransactionType().getId());		
		
		return dto;
	}

	@Override
	public JournalEntry dtoToModelMap(JournalEntryDTO dto) {
		JournalEntry model = new JournalEntry();
		
		model.setAmount(dto.getAmount());
		model.setBusinessDate(dto.getBusinessDate());
		model.setCreditAccount(accountService.getModelById(dto.getCreditAccountId()));
		model.setDebitAccount(accountService.getModelById(dto.getDebitAccountId()));
		model.setId(dto.getId());
		model.setIsReversal(dto.getIsReversal());
		model.setParty(partyService.getPartyObjectById(dto.getPartyId()));
		model.setRemarks(dto.getRemarks());
		model.setTransactionDate(dto.getTransactionDate());
		model.setTransactionId(dto.getTransactionId());
		model.setTransactionIdSequence(dto.getTransactionIdSequence());
		model.setTransactionType(transactionTypeService.getModelById(dto.getTransactionTypeId()));	
		
		
		return model;
	}

	@Override
	public List<JournalEntryDTO> modelToDTOList(List<JournalEntry> modelList) {
		
		return modelList.stream()
				.map(model -> modelToDTOMap(model))
				.collect(Collectors.toList());
		
	}

	@Override
	public List<JournalEntry> dtoToModelList(List<JournalEntryDTO> dtoList) {
		return dtoList.stream()
				.map(dto -> dtoToModelMap(dto))
				.collect(Collectors.toList());
	}

}
