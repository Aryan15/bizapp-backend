package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.StatusBasedPermissionDTO;
import com.coreerp.model.StatusBasedPermission;

@Component
public class StatusBasedPermissionMapper implements AbstractMapper<StatusBasedPermissionDTO, StatusBasedPermission> {

	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public StatusBasedPermissionDTO modelToDTOMap(StatusBasedPermission model) {
		return modelMapper.map(model, StatusBasedPermissionDTO.class);
	}

	@Override
	public StatusBasedPermission dtoToModelMap(StatusBasedPermissionDTO dto) {
		return modelMapper.map(dto,  StatusBasedPermission.class);
	}

	@Override
	public List<StatusBasedPermissionDTO> modelToDTOList(List<StatusBasedPermission> modelList) {
		return modelList.stream()
				.map(material -> modelToDTOMap(material))
				.collect(Collectors.toList());
	}

	@Override
	public List<StatusBasedPermission> dtoToModelList(List<StatusBasedPermissionDTO> dtoList) {
		return dtoList.stream()
				.map(materialDTO -> dtoToModelMap(materialDTO))
				.collect(Collectors.toList());
	}

}
