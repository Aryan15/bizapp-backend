package com.coreerp.mapper;


import com.coreerp.dto.EwayBillTrackingDTO;
import com.coreerp.model.EwayBillTracking;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.service.CityService;
import com.coreerp.service.InvoiceService;
import com.coreerp.service.StateService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EwayBillTrackingMapper implements AbstractMapper<EwayBillTrackingDTO, EwayBillTracking> {


    final static Logger log = LogManager.getLogger(EwayBillTrackingMapper.class);

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    private StateService stateService;

    @Autowired
    private CityService cityService;

    @Override
    public EwayBillTrackingDTO modelToDTOMap(EwayBillTracking model) {
        EwayBillTrackingDTO ewayBillTrackingDTO= modelMapper.map(model, EwayBillTrackingDTO.class);
        ewayBillTrackingDTO.setInvoiceHeaderId(model.getInvoiceHeader().getId());
        ewayBillTrackingDTO.setFromState(model.getState() != null ? model.getState().getId(): null);
        ewayBillTrackingDTO.setFromPlace(model.getCity() != null ? model.getCity().getId(): null);
        ewayBillTrackingDTO.setIsCurrent(model.getIsCurrent());
        return ewayBillTrackingDTO;
    }

    @Override
    public EwayBillTracking dtoToModelMap(EwayBillTrackingDTO dto) {
        log.info("EwayBillTrackingDTO :"+dto);

        EwayBillTracking ewayBillTracking = modelMapper.map(dto, EwayBillTracking.class);
        InvoiceHeader invoiceHeader = invoiceService.getModelById(dto.getInvoiceHeaderId());
        ewayBillTracking.setState(dto.getFromState() != null ? stateService.getStateById(dto.getFromState()): null);
        ewayBillTracking.setCity(dto.getFromPlace() != null ? cityService.getCityById(dto.getFromPlace()): null);
        ewayBillTracking.setVehicleNo(dto.getVehicleNo());
        ewayBillTracking.setIsCurrent(dto.getIsCurrent());

        ewayBillTracking.setInvoiceHeader(invoiceHeader);
        return ewayBillTracking;
    }

    @Override
    public List<EwayBillTrackingDTO> modelToDTOList(List<EwayBillTracking> modelList) {
        return modelList.stream()
                .map(eWayBill -> modelToDTOMap(eWayBill))
                .collect(Collectors.toList());
    }

    @Override
    public List<EwayBillTracking> dtoToModelList(List<EwayBillTrackingDTO> dtoList) {

        return dtoList.stream()
                .map(eWayBillDTO -> dtoToModelMap(eWayBillDTO))
                .collect(Collectors.toList());

    }
}
