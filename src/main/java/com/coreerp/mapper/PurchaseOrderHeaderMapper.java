package com.coreerp.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.dto.InvoiceDTO;
import com.coreerp.model.InvoiceHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.PurchaseOrderDTO;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.service.CompanyService;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.PartyService;
import com.coreerp.service.QuotationHeaderService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TaxService;
import com.coreerp.service.TransactionNumberService;
import com.coreerp.service.TransactionTypeService;

@Component
public class PurchaseOrderHeaderMapper implements AbstractMapper<PurchaseOrderDTO, PurchaseOrderHeader> {

	@Autowired
	CompanyService companyService;
	
	@Autowired
	FinancialYearService financialYearService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	QuotationHeaderService quotationHeaderService;
	
	@Autowired
	QuotationHeaderMapper quotationHeaderMapper;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	PurchaseOrderItemMapper purchaseOrderItemMapper;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	TransactionNumberService transactionNumberService;
	
	@Autowired
	StatusService statusService;
	
	@Override
	public PurchaseOrderDTO modelToDTOMap(PurchaseOrderHeader model) {
		PurchaseOrderDTO purchaseOrderDTO = modelToDTOWithoutItemMap(model);
		

		purchaseOrderDTO.setPurchaseOrderItems(purchaseOrderItemMapper.modelToDTOList(model.getPurchaseOrderItems()));

		return purchaseOrderDTO;
	}

	public PurchaseOrderDTO modelToDTOWithoutItemMap(PurchaseOrderHeader model){
		PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO();

		purchaseOrderDTO.setId(model.getId());
		purchaseOrderDTO.setAdvanceAmount(model.getAdvanceAmount());
		purchaseOrderDTO.setCgstTaxAmount(model.getCgstTaxAmount());
		purchaseOrderDTO.setCgstTaxRate(model.getCgstTaxRate());
		purchaseOrderDTO.setCompanyId(model.getCompany().getId());
		purchaseOrderDTO.setDeliveryTerms(model.getDeliveryTerms());
		purchaseOrderDTO.setTotalDiscount(model.getDiscountAmount());
		purchaseOrderDTO.setDiscountPercent(model.getDiscountPercent());
		purchaseOrderDTO.setFinancialYearId(model.getFinancialYear().getId());
		purchaseOrderDTO.setGrandTotal(model.getGrandTotal());
		purchaseOrderDTO.setIgstTaxAmount(model.getIgstTaxAmount());
		purchaseOrderDTO.setIgstTaxRate(model.getIgstTaxRate());
		purchaseOrderDTO.setInternalReferenceDate(model.getInternalReferenceDate());
		purchaseOrderDTO.setInternalReferenceNumber(model.getInternalReferenceNumber());
		purchaseOrderDTO.setIsReverseCharge(model.getIsReverseCharge());
		purchaseOrderDTO.setTotalTaxableAmount(model.getNetAmount());
		purchaseOrderDTO.setPartyId(model.getParty().getId());
		purchaseOrderDTO.setPaymentTerms(model.getPaymentTerms());
		purchaseOrderDTO.setPurchaseOrderDate(model.getPurchaseOrderDate());
		purchaseOrderDTO.setPurchaseOrderId(model.getPurchaseOrderId());
		purchaseOrderDTO.setPurchaseOrderNumber(model.getPurchaseOrderNumber());
		//purchaseOrderDTO.setPurchaseOrderStatus(model.getStatus());
		purchaseOrderDTO.setPurchaseOrderTypeId(model.getPurchaseOrderType().getId());
		purchaseOrderDTO.setQuotationDate(model.getQuotationDate());
		purchaseOrderDTO.setQuotationNumber(model.getQuotationNumber());
		purchaseOrderDTO.setRoundOffAmount(model.getRoundOffAmount());
		purchaseOrderDTO.setSgstTaxAmount(model.getSgstTaxAmount());
		purchaseOrderDTO.setSgstTaxRate(model.getSgstTaxRate());
		purchaseOrderDTO.setStatusId(model.getStatus().getId());
		purchaseOrderDTO.setSubTotalAmount(model.getSubTotalAmount());
		purchaseOrderDTO.setTaxAmount(model.getTaxAmount());
		purchaseOrderDTO.setTaxId(model.getTax() != null ? model.getTax().getId() : null);
		purchaseOrderDTO.setTermsAndConditions(model.getTermsAndConditions());
		purchaseOrderDTO.setTransportationCharges(model.getTransportationCharges());
		purchaseOrderDTO.setPartyName(model.getParty() != null ? model.getParty().getName() : null);
		purchaseOrderDTO.setStatusName(model.getStatus() != null ? model.getStatus().getName() : null);

		purchaseOrderDTO.setPurchaseOrderEndDate(model.getPurchaseOrderEndDate());
		purchaseOrderDTO.setIsOpenEnded(model.getIsOpenEnded());
		purchaseOrderDTO.setRemarks(model.getRemarks());
		purchaseOrderDTO.setInclusiveTax(model.getInclusiveTax());

		purchaseOrderDTO.setAddress(model.getBillToAddress());
		purchaseOrderDTO.setGstNumber(model.getGstNumber());
		purchaseOrderDTO.setShipToAddress(model.getShipToAddress());

		purchaseOrderDTO.setCompanyName(model.getCompanyName());
		purchaseOrderDTO.setCompanygGstRegistrationTypeId(model.getCompanygGstRegistrationTypeId());
		purchaseOrderDTO.setCompanyPinCode(model.getCompanyPinCode());
		purchaseOrderDTO.setCompanyStateId(model.getCompanyStateId());
		purchaseOrderDTO.setCompanyStateName(model.getCompanyStateName());
		purchaseOrderDTO.setCompanyCountryId(model.getCompanyCountryId());
		purchaseOrderDTO.setCompanyPrimaryMobile(model.getCompanyPrimaryMobile());
		purchaseOrderDTO.setCompanySecondaryMobile(model.getCompanySecondaryMobile());
		purchaseOrderDTO.setCompanyContactPersonNumber(model.getCompanyContactPersonNumber());
		purchaseOrderDTO.setCompanyContactPersonName(model.getCompanyContactPersonName());
		purchaseOrderDTO.setCompanyPrimaryTelephone(model.getCompanyPrimaryTelephone());
		purchaseOrderDTO.setCompanySecondaryTelephone(model.getCompanySecondaryTelephone());
		purchaseOrderDTO.setCompanyWebsite(model.getCompanyWebsite());
		purchaseOrderDTO.setCompanyEmail(model.getCompanyEmail());
		purchaseOrderDTO.setCompanyFaxNumber(model.getCompanyFaxNumber());
		purchaseOrderDTO.setCompanyAddress(model.getCompanyAddress());
		purchaseOrderDTO.setCompanyTagLine(model.getCompanyTagLine());
		purchaseOrderDTO.setCompanyGstNumber(model.getCompanyGstNumber());
		purchaseOrderDTO.setCompanyPanNumber(model.getCompanyPanNumber());
		purchaseOrderDTO.setCompanyPanDate(model.getCompanyPanDate());
		purchaseOrderDTO.setCompanyCeritificateImagePath(model.getCompanyCeritificateImagePath());
		purchaseOrderDTO.setCompanyLogoPath(model.getCompanyLogoPath());
		
		purchaseOrderDTO.setPartyName(model.getPartyName());
		purchaseOrderDTO.setPartyContactPersonNumber(model.getPartyContactPersonNumber());
		purchaseOrderDTO.setPartyPinCode(model.getPartyPinCode());
		purchaseOrderDTO.setPartyAreaId(model.getPartyAreaId());
		purchaseOrderDTO.setPartyCityId(model.getPartyCityId());
		purchaseOrderDTO.setPartyStateId(model.getPartyStateId());
		purchaseOrderDTO.setPartyCountryId(model.getPartyCountryId());
		purchaseOrderDTO.setPartyPrimaryTelephone(model.getPartyPrimaryTelephone());
		purchaseOrderDTO.setPartySecondaryTelephone(model.getPartySecondaryTelephone());
		purchaseOrderDTO.setPartyPrimaryMobile(model.getPartyPrimaryMobile());
		purchaseOrderDTO.setPartySecondaryMobile(model.getPartySecondaryMobile());
		purchaseOrderDTO.setPartyEmail(model.getPartyEmail());
		purchaseOrderDTO.setPartyWebsite(model.getPartyWebsite());
		purchaseOrderDTO.setPartyContactPersonName(model.getPartyContactPersonName());
//		purchaseOrderDTO.setPartyBillToAddress(model.getPartyBillToAddress());
//		purchaseOrderDTO.setPartyShipAddress(model.getPartyBillToAddress());
		purchaseOrderDTO.setPartyDueDaysLimit(model.getPartyDueDaysLimit());
		purchaseOrderDTO.setPartyGstRegistrationTypeId(model.getPartyGstRegistrationTypeId());
//		purchaseOrderDTO.setPartyGstNumber(model.getPartyGstNumber());
		purchaseOrderDTO.setPartyPanNumber(model.getPartyPanNumber());
		purchaseOrderDTO.setIsIgst(model.getIsIgst());
		purchaseOrderDTO.setPartyCode(model.getParty().getPartyCode());
		purchaseOrderDTO.setCreatedBy(model.getCreatedBy());
		purchaseOrderDTO.setUpdateBy(model.getUpdatedBy());
		purchaseOrderDTO.setCreatedDate(model.getCreatedDateTime());
		purchaseOrderDTO.setUpdatedDate(model.getUpdatedDateTime());
		purchaseOrderDTO.setClosePo(model.getClosePo());
		purchaseOrderDTO.setPartyCurrencyId(model.getPartyCurrencyId());
		purchaseOrderDTO.setCurrencyName(model.getCurrencyName());

		return purchaseOrderDTO;
	}

	@Override
	public PurchaseOrderHeader dtoToModelMap(PurchaseOrderDTO dto) {
		PurchaseOrderHeader purchaseOrderHeader = new PurchaseOrderHeader();
		
		purchaseOrderHeader.setAdvanceAmount(dto.getAdvanceAmount());
		purchaseOrderHeader.setCgstTaxAmount(dto.getCgstTaxAmount());
		purchaseOrderHeader.setCgstTaxRate(dto.getCgstTaxRate());
		purchaseOrderHeader.setCompany(companyService.getModelById(dto.getCompanyId()));
		purchaseOrderHeader.setDeliveryTerms(dto.getDeliveryTerms());
		purchaseOrderHeader.setDiscountAmount(dto.getTotalDiscount());
		purchaseOrderHeader.setDiscountPercent(dto.getDiscountPercent());
		purchaseOrderHeader.setFinancialYear(financialYearService.getById(dto.getFinancialYearId()));
		purchaseOrderHeader.setGrandTotal(dto.getGrandTotal());
		purchaseOrderHeader.setId(dto.getId());
		purchaseOrderHeader.setIgstTaxAmount(dto.getIgstTaxAmount());
		purchaseOrderHeader.setIgstTaxRate(dto.getIgstTaxRate());
		purchaseOrderHeader.setInternalReferenceDate(dto.getInternalReferenceDate());
		purchaseOrderHeader.setInternalReferenceNumber(dto.getInternalReferenceNumber());
		purchaseOrderHeader.setIsReverseCharge(dto.getIsReverseCharge());
		purchaseOrderHeader.setNetAmount(dto.getTotalTaxableAmount());
		purchaseOrderHeader.setParty(partyService.getPartyObjectById(dto.getPartyId()));
		purchaseOrderHeader.setPaymentTerms(dto.getPaymentTerms());
		purchaseOrderHeader.setPurchaseOrderDate(dto.getPurchaseOrderDate());		
		purchaseOrderHeader.setPurchaseOrderItems(purchaseOrderItemMapper.dtoToModelList(dto.getPurchaseOrderItems()));
		purchaseOrderHeader.setPurchaseOrderNumber(dto.getPurchaseOrderNumber() != null ? dto.getPurchaseOrderNumber() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getPurchaseOrderTypeId())).getTransactionNumber());
		purchaseOrderHeader.setPurchaseOrderId(dto.getPurchaseOrderId() != null ? dto.getPurchaseOrderId() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getPurchaseOrderTypeId())).getTransactionId() );				
		//purchaseOrderHeader.setStatus(dto.getPurchaseOrderStatus());
		purchaseOrderHeader.setPurchaseOrderType(transactionTypeService.getModelById(dto.getPurchaseOrderTypeId()));
		purchaseOrderHeader.setQuotationDate(dto.getQuotationDate());
		purchaseOrderHeader.setQuotationNumber(dto.getQuotationNumber());
		purchaseOrderHeader.setRoundOffAmount(dto.getRoundOffAmount());
		purchaseOrderHeader.setSgstTaxAmount(dto.getSgstTaxAmount());
		purchaseOrderHeader.setSgstTaxRate(dto.getSgstTaxRate());
		purchaseOrderHeader.setStatus(dto.getPurchaseOrderNumber() != null ? (dto.getStatusId() != null ? statusService.getStatusById(dto.getStatusId()) : null ) : null);
		purchaseOrderHeader.setSubTotalAmount(dto.getSubTotalAmount());
		purchaseOrderHeader.setTax(dto.getTaxId() != null ? taxService.getById(dto.getTaxId()) : null);
		purchaseOrderHeader.setTaxAmount(dto.getTaxAmount());
		purchaseOrderHeader.setTermsAndConditions(dto.getTermsAndConditions());
		purchaseOrderHeader.setTransportationCharges(dto.getTransportationCharges());
		purchaseOrderHeader.setPurchaseOrderEndDate(dto.getPurchaseOrderEndDate());
		purchaseOrderHeader.setIsOpenEnded(dto.getIsOpenEnded());
		purchaseOrderHeader.setRemarks(dto.getRemarks());
		purchaseOrderHeader.setInclusiveTax(dto.getInclusiveTax());
		
		purchaseOrderHeader.setBillToAddress(dto.getAddress());
		purchaseOrderHeader.setGstNumber(dto.getGstNumber());
		purchaseOrderHeader.setShipToAddress(dto.getShipToAddress());
		
		
		purchaseOrderHeader.setCompanyName(dto.getCompanyName());
		purchaseOrderHeader.setCompanygGstRegistrationTypeId(dto.getCompanygGstRegistrationTypeId());
		purchaseOrderHeader.setCompanyPinCode(dto.getCompanyPinCode());
		purchaseOrderHeader.setCompanyStateId(dto.getCompanyStateId());
		purchaseOrderHeader.setCompanyStateName(dto.getCompanyStateName());
		purchaseOrderHeader.setCompanyCountryId(dto.getCompanyCountryId());
		purchaseOrderHeader.setCompanyPrimaryMobile(dto.getCompanyPrimaryMobile());
		purchaseOrderHeader.setCompanySecondaryMobile(dto.getCompanySecondaryMobile());
		purchaseOrderHeader.setCompanyContactPersonNumber(dto.getCompanyContactPersonNumber());
		purchaseOrderHeader.setCompanyContactPersonName(dto.getCompanyContactPersonName());
		purchaseOrderHeader.setCompanyPrimaryTelephone(dto.getCompanyPrimaryTelephone());
		purchaseOrderHeader.setCompanySecondaryTelephone(dto.getCompanySecondaryTelephone());
		purchaseOrderHeader.setCompanyWebsite(dto.getCompanyWebsite());
		purchaseOrderHeader.setCompanyEmail(dto.getCompanyEmail());
		purchaseOrderHeader.setCompanyFaxNumber(dto.getCompanyFaxNumber());
		purchaseOrderHeader.setCompanyAddress(dto.getCompanyAddress());
		purchaseOrderHeader.setCompanyTagLine(dto.getCompanyTagLine());
		purchaseOrderHeader.setCompanyGstNumber(dto.getCompanyGstNumber());
		purchaseOrderHeader.setCompanyPanNumber(dto.getCompanyPanNumber());
		purchaseOrderHeader.setCompanyPanDate(dto.getCompanyPanDate());
		purchaseOrderHeader.setCompanyCeritificateImagePath(dto.getCompanyCeritificateImagePath());
		purchaseOrderHeader.setCompanyLogoPath(dto.getCompanyLogoPath());
		
		purchaseOrderHeader.setPartyName(dto.getPartyName());
		purchaseOrderHeader.setPartyContactPersonNumber(dto.getPartyContactPersonNumber());
		purchaseOrderHeader.setPartyPinCode(dto.getPartyPinCode());
		purchaseOrderHeader.setPartyAreaId(dto.getPartyAreaId());
		purchaseOrderHeader.setPartyCityId(dto.getPartyCityId());
		purchaseOrderHeader.setPartyStateId(dto.getPartyStateId());
		purchaseOrderHeader.setPartyCountryId(dto.getPartyCountryId());
		purchaseOrderHeader.setPartyPrimaryTelephone(dto.getPartyPrimaryTelephone());
		purchaseOrderHeader.setPartySecondaryTelephone(dto.getPartySecondaryTelephone());
		purchaseOrderHeader.setPartyPrimaryMobile(dto.getPartyPrimaryMobile());
		purchaseOrderHeader.setPartySecondaryMobile(dto.getPartySecondaryMobile());
		purchaseOrderHeader.setPartyEmail(dto.getPartyEmail());
		purchaseOrderHeader.setPartyWebsite(dto.getPartyWebsite());
		purchaseOrderHeader.setPartyContactPersonName(dto.getPartyContactPersonName());
//		purchaseOrderHeader.setPartyBillToAddress(dto.getPartyBillToAddress());
//		purchaseOrderHeader.setPartyShipAddress(dto.getPartyBillToAddress());
		purchaseOrderHeader.setPartyDueDaysLimit(dto.getPartyDueDaysLimit());
		purchaseOrderHeader.setPartyGstRegistrationTypeId(dto.getPartyGstRegistrationTypeId());
//		purchaseOrderHeader.setPartyGstNumber(dto.getPartyGstNumber());
		purchaseOrderHeader.setPartyPanNumber(dto.getPartyPanNumber());
		purchaseOrderHeader.setIsIgst(dto.getIsIgst());
		purchaseOrderHeader.setCreatedBy(dto.getCreatedBy());
		purchaseOrderHeader.setCreatedDateTime(dto.getCreatedDate());
		purchaseOrderHeader.setClosePo(dto.getClosePo()!=null ? dto.getClosePo() : 0);
		purchaseOrderHeader.setPartyCurrencyId(dto.getPartyCurrencyId());
		purchaseOrderHeader.setCurrencyName(dto.getCurrencyName());
		return purchaseOrderHeader;
	}

	@Override
	public List<PurchaseOrderDTO> modelToDTOList(List<PurchaseOrderHeader> modelList) {
		List<PurchaseOrderDTO> purchaseOrderListOut = new ArrayList<PurchaseOrderDTO>();
		
		
		for(PurchaseOrderHeader purchaseOrderHeader : modelList){
			PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO();
			purchaseOrderDTO = modelToDTOMap(purchaseOrderHeader);
			purchaseOrderListOut.add(purchaseOrderDTO);
		}
		
		return purchaseOrderListOut;
	}

	public List<PurchaseOrderDTO> modelToRecentDTOList(List<PurchaseOrderHeader> modelList) {

		List<PurchaseOrderDTO> poDTOListOut = modelList.stream()
				.map(
						purchaseOrderHeader -> modelToDTOWithoutItemMap(purchaseOrderHeader))
				.collect(Collectors.toList());

		return poDTOListOut;
	}

	@Override
	public List<PurchaseOrderHeader> dtoToModelList(List<PurchaseOrderDTO> dtoList) {
		List<PurchaseOrderHeader> puchaseOrderHeaderListOut = new ArrayList<PurchaseOrderHeader>();
		
		for(PurchaseOrderDTO purchaseOrderDTO : dtoList){
			PurchaseOrderHeader purchaseOrderHeader = new PurchaseOrderHeader();
			purchaseOrderHeader = dtoToModelMap(purchaseOrderDTO);
			puchaseOrderHeaderListOut.add(purchaseOrderHeader);
		}
		return puchaseOrderHeaderListOut;
	}


}
