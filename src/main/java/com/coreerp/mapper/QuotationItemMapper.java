package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.QuotationItemDTO;
import com.coreerp.model.QuotationItem;
import com.coreerp.service.MaterialService;
import com.coreerp.service.TaxService;
import com.coreerp.service.UnitOfMeasurementService;

@Component
public class QuotationItemMapper implements AbstractMapper<QuotationItemDTO, QuotationItem> {

	final static Logger log = LogManager.getLogger(QuotationHeaderMapper.class);
	@Autowired
	MaterialService materialService;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	UnitOfMeasurementService unitOfMeasurementService ; 
	
	@Override
	public QuotationItemDTO modelToDTOMap(QuotationItem model) {
		// TODO Auto-generated method stub
		log.info("---------------------3");
		QuotationItemDTO  itemDTO = new QuotationItemDTO ();
		
		itemDTO.setAmount(model.getAmount());
		itemDTO.setAmountAfterDiscount(model.getAmountAfterDiscount());
		itemDTO.setAmountAfterTax(model.getAmountAfterTax());
		itemDTO.setCgstTaxAmount(model.getCgstTaxAmount());
		itemDTO.setCgstTaxPercentage(model.getCgstTaxRate());
		itemDTO.setDiscountAmount(model.getDiscountAmount());
		itemDTO.setDiscountPercentage(model.getDiscountPercentage());
		itemDTO.setId(model.getId());
		itemDTO.setIgstTaxAmount(model.getIgstTaxAmount());
		itemDTO.setIgstTaxPercentage(model.getIgstTaxRate());
		itemDTO.setInclusiveTax(model.getInclusiveTax());
		itemDTO.setMaterialId(model.getMaterial().getId());
		itemDTO.setPrice(model.getPrice());
		itemDTO.setQuantity(model.getQuantity());
		itemDTO.setQuotationHeaderId(model.getQuotationHeader()!= null ? model.getQuotationHeader().getId() : null);
		itemDTO.setRemarks(model.getRemarks());
		itemDTO.setSgstTaxAmount(model.getSgstTaxAmount());
		itemDTO.setSgstTaxPercentage(model.getSgstTaxRate());
		itemDTO.setSlNo(model.getSlNo());
		itemDTO.setStatus(model.getStatus());
		itemDTO.setTaxAmount(model.getTaxAmount());
		itemDTO.setTaxId(model.getTax()!= null ? model.getTax().getId() : null);
		itemDTO.setTaxPercentage(model.getTaxPercentage());
		itemDTO.setTaxRate(model.getTaxRate());
		itemDTO.setUnitOfMeasurementId(model.getUnitOfMeasurement() != null ? model.getUnitOfMeasurement().getId() : null);
		itemDTO.setPartName(model.getMaterial().getName());
		itemDTO.setPartNumber(model.getMaterial().getPartNumber());
		itemDTO.setHsnOrSac(model.getMaterial().getHsnCode());
		itemDTO.setUom(model.getUnitOfMeasurement() != null ? model.getUnitOfMeasurement().getName() : null);
		itemDTO.setSpecification(model.getSpecification());
		
		return itemDTO;
	}

	@Override
	public QuotationItem dtoToModelMap(QuotationItemDTO dto) {
		log.info("---------------------4");
		QuotationItem item = new QuotationItem();
		
		item.setAmount(dto.getAmount());
		item.setAmountAfterDiscount(dto.getAmountAfterDiscount());
		item.setAmountAfterTax(dto.getAmountAfterTax());
		item.setCgstTaxAmount(dto.getCgstTaxAmount());
		item.setCgstTaxRate(dto.getCgstTaxPercentage());
		item.setDiscountAmount(dto.getDiscountAmount());
		item.setDiscountPercentage(dto.getDiscountPercentage());
		item.setId(dto.getId());
		item.setIgstTaxAmount(dto.getIgstTaxAmount());
		item.setIgstTaxRate(dto.getIgstTaxPercentage());
		item.setInclusiveTax(dto.getInclusiveTax());
		item.setMaterial(materialService.getMaterialById(dto.getMaterialId()));
		item.setPrice(dto.getPrice());
		item.setQuantity(dto.getQuantity());
		//item.setQuotationHeader(dto.getQuotationHeaderId());
		item.setRemarks(dto.getRemarks());
		item.setSgstTaxAmount(dto.getSgstTaxAmount());
		item.setSgstTaxRate(dto.getSgstTaxPercentage());
		item.setSlNo(dto.getSlNo());
		item.setStatus(dto.getStatus());
		item.setTax(dto.getTaxId()== null ? taxService.getById(0l):taxService.getById(dto.getTaxId()));
		//item.setTax(dto.getTaxId() != null ?taxService.getById(dto.getTaxId()):null);
		item.setTaxAmount(dto.getTaxAmount());
		item.setTaxPercentage(dto.getTaxPercentage());
		item.setTaxRate(dto.getTaxRate());
		item.setUnitOfMeasurement(dto.getUnitOfMeasurementId() != null ? unitOfMeasurementService.getById(dto.getUnitOfMeasurementId()) : null);
		item.setSpecification(dto.getSpecification());
		
		return item;
	}

	@Override
	public List<QuotationItemDTO> modelToDTOList(List<QuotationItem> modelList) {
		return modelList.stream()
					.map(item -> modelToDTOMap(item))
					.collect(Collectors.toList());
	}

	@Override
	public List<QuotationItem> dtoToModelList(List<QuotationItemDTO> dtoList) {
		return dtoList.stream()
				.map(item -> dtoToModelMap(item))
				.collect(Collectors.toList());
	}

}
