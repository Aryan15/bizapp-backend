package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.GRNItemDTO;
import com.coreerp.model.GRNItem;
import com.coreerp.service.DeliveryChallanService;
import com.coreerp.service.MaterialService;
import com.coreerp.service.PurchaseOrderService;
import com.coreerp.service.TaxService;
import com.coreerp.service.UnitOfMeasurementService;

@Component
public class GRNItemMapper implements AbstractMapper<GRNItemDTO, GRNItem> {

	final static Logger log=LogManager.getLogger(GRNItemMapper.class);
	
	@Autowired
	MaterialService materialService;
	
	@Autowired
	PurchaseOrderService purchaseOrderService;
	
	@Autowired
	DeliveryChallanService deliveryChallanService;
	
	@Autowired
	UnitOfMeasurementService unitOfMeasurementService;
	
	@Autowired
	TaxService taxService;
	
	@Override
	public GRNItemDTO modelToDTOMap(GRNItem model) {
           
		GRNItemDTO grnItemDTOOut=new GRNItemDTO();
		
		grnItemDTOOut.setId(model.getId());
		grnItemDTOOut.setGrnHeaderId(model.getGrnHeader().getId());
		grnItemDTOOut.setMaterialId(model.getMaterial().getId());
		grnItemDTOOut.setMaterialName(model.getMaterial().getName());
		grnItemDTOOut.setDeliveryChallanQuantity(model.getDeliveryChallanQuantity());
		grnItemDTOOut.setAcceptedQuantity(model.getAcceptedQuantity());
		grnItemDTOOut.setRejectedQuantity(model.getRejectedQuantity());
		grnItemDTOOut.setPurchaseOrderHeaderId(model.getPurchaseOrderHeader() != null ? model.getPurchaseOrderHeader().getId() : null);
        grnItemDTOOut.setInvoiceBalanceQuantity(model.getBalanceQuantity());
        grnItemDTOOut.setRemarks(model.getRemarks());
        grnItemDTOOut.setAmount(model.getAmount());
        grnItemDTOOut.setDeliveryChallanHeaderId(model.getDeliveryChallanHeader().getId());
        grnItemDTOOut.setDeliveryChallanItemId(model.getDeliveryChallanItem().getId());
        grnItemDTOOut.setUnitOfMeasurementId(model.getUnitOfMeasurement() != null ? model.getUnitOfMeasurement().getId() : null);
        grnItemDTOOut.setUnitPrice(model.getUnitPrice());
        grnItemDTOOut.setMaterialSpecification(model.getMaterialSpecification());
        grnItemDTOOut.setTaxId(model.getTax() != null ? model.getTax().getId() : null);
        grnItemDTOOut.setDiscountPercentage(model.getDeliveryChallanItem().getDiscountPercentage());
        grnItemDTOOut.setDiscountAmount(model.getDeliveryChallanItem().getDiscountAmount());
        grnItemDTOOut.setAmountAfterDiscount(model.getDeliveryChallanItem().getAmountAfterDiscount());
        grnItemDTOOut.setSgstTaxAmount(model.getDeliveryChallanItem().getSgstTaxAmount());
        grnItemDTOOut.setSgstTaxPercentage(model.getDeliveryChallanItem().getSgstTaxRate());
        grnItemDTOOut.setCgstTaxAmount(model.getDeliveryChallanItem().getCgstTaxAmount());
        grnItemDTOOut.setCgstTaxPercentage(model.getDeliveryChallanItem().getCgstTaxRate());
        grnItemDTOOut.setIgstTaxAmount(model.getDeliveryChallanItem().getIgstTaxAmount());
        grnItemDTOOut.setIgstTaxPercentage(model.getDeliveryChallanItem().getIgstTaxRate());
        grnItemDTOOut.setTaxAmount(model.getDeliveryChallanItem().getTaxAmount());
        grnItemDTOOut.setPartNumber(model.getMaterial().getPartNumber());
		grnItemDTOOut.setHsnOrSac(model.getMaterial().getHsnCode());
		grnItemDTOOut.setSlNo(model.getSlNo());
       /* grnItemDTOOut.setPartNumber, partname and hsn(model.getDeliveryChallanItem().get);*/
        grnItemDTOOut.setInclusiveTax(model.getDeliveryChallanItem().getInclusiveTax());
        grnItemDTOOut.setUom(model.getDeliveryChallanItem().getUnitOfMeasurement().getName());
        return grnItemDTOOut;
	}

	@Override
	public GRNItem dtoToModelMap(GRNItemDTO dto) {

		log.info("in dtoToModelMap: "+dto.getId());
		GRNItem grnItemOut=new GRNItem();
		
		grnItemOut.setId(dto.getId());
		//grnItemOut.setGrnHeader(grnHeader);
	   grnItemOut.setMaterial(materialService.getMaterialById(dto.getMaterialId()));
	   grnItemOut.setDeliveryChallanQuantity(dto.getDeliveryChallanQuantity());
	   grnItemOut.setAcceptedQuantity(dto.getAcceptedQuantity());
	   grnItemOut.setRejectedQuantity(dto.getRejectedQuantity());
	   grnItemOut.setPurchaseOrderHeader(dto.getPurchaseOrderHeaderId()!=null ? purchaseOrderService.getModelById(dto.getPurchaseOrderHeaderId()):null);
       grnItemOut.setBalanceQuantity(dto.getAcceptedQuantity());
       grnItemOut.setRemarks(dto.getRemarks());
       grnItemOut.setAmount(dto.getAmount());
       grnItemOut.setDeliveryChallanHeader(dto.getDeliveryChallanHeaderId()!=null ? deliveryChallanService.getModelById(dto.getDeliveryChallanHeaderId()):null);
       grnItemOut.setDeliveryChallanItem(dto.getDeliveryChallanItemId()!=null? deliveryChallanService.getItemModelById(dto.getDeliveryChallanItemId()):null);
       grnItemOut.setUnitOfMeasurement(dto.getUnitOfMeasurementId()!=null ? unitOfMeasurementService.getById(dto.getUnitOfMeasurementId()):null );
       grnItemOut.setUnitPrice(dto.getUnitPrice());
       grnItemOut.setMaterialSpecification(dto.getMaterialSpecification());
	   grnItemOut.setTax(dto.getTaxId() != null ? taxService.getById(dto.getTaxId()) : null);
	   grnItemOut.setSlNo(dto.getSlNo());
	   grnItemOut.setMaterialSpecification(dto.getMaterialSpecification());
       log.info("grnItemOut.id: "+grnItemOut.getId());
       
       return grnItemOut; 
	}

	@Override
	public List<GRNItemDTO> modelToDTOList(List<GRNItem> modelList) {
     
//		List<GRNItemDTO> invoiceItemDTOList=modelList.stream().map(grnItem ->{
//			GRNItemDTO grnItemDTO=modelToDTOMap(grnItem);
//			return grnItemDTO;
//		}).collect(Collectors.toList());
	
		
		return modelList.stream()
				.map(grnItem -> modelToDTOMap(grnItem))
				.collect(Collectors.toList());
		
		//return invoiceItemDTOList;
	}

	@Override
	public List<GRNItem> dtoToModelList(List<GRNItemDTO> dtoList) {

		log.info("dtoToModelList..incoming item size: "+dtoList.size());
//		List<GRNItem> grnItemListOut=dtoList.stream().map(grnItemDTO -> {
//			GRNItem grnItem=dtoToModelMap(grnItemDTO);
//			return grnItem;
//		}).collect(Collectors.toList());
//			
//			
//			log.info("dtoToModelList.. outgoing item size: "+grnItemListOut.size());
//			log.info("dtoToModelList.. outgoing item id: "+grnItemListOut.get(0).getId());
//			
//			return grnItemListOut;
			
			
			return dtoList.stream()
					.map(grnItemDTO -> dtoToModelMap(grnItemDTO))
					.collect(Collectors.toList());
	}

}
