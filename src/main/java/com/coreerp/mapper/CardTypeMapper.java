package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.CardTypeDTO;
import com.coreerp.model.CardType;

@Component
public class CardTypeMapper implements AbstractMapper<CardTypeDTO, CardType>{

	@Autowired
	ModelMapper modelMapper;
	
	
	@Override
	public CardTypeDTO modelToDTOMap(CardType model) {

		return modelMapper.map(model, CardTypeDTO.class);
	}
	
	@Override
	public CardType dtoToModelMap(CardTypeDTO dto) {
		return modelMapper.map(dto, CardType.class);
	}
	
	@Override
	public List<CardTypeDTO> modelToDTOList(List<CardType> modelList) {
		return modelList.stream()
				.map(item -> modelToDTOMap(item))
				.collect(Collectors.toList());
	}
	
	@Override
	public List<CardType> dtoToModelList(List<CardTypeDTO> dtoList) {
		return dtoList.stream()
				.map(item -> dtoToModelMap(item))
				.collect(Collectors.toList());
	}
}