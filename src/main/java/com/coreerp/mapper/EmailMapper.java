package com.coreerp.mapper;

import com.coreerp.dto.EmailDTO;

import com.coreerp.model.EmailSetting;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmailMapper implements AbstractMapper<EmailDTO, EmailSetting> {


    @Override
    public EmailDTO modelToDTOMap(EmailSetting model) {
         EmailDTO emailDTO =new EmailDTO();
         emailDTO.setDeleted(model.getDeleted());
        emailDTO.setEmailDebug(model.getEmailDebug());
        emailDTO.setEmailEnabled(model.getIsEmailEnabled());
           emailDTO.setEmailFrom(model.getEmailFrom());
           emailDTO.setEmailHost(model.getEmailHost());
           emailDTO.setEmailPassword(model.getEmailPassword());
           emailDTO.setEmailSmtpAuth(model.getEmailSmtpAuth());
           emailDTO.setEmailSmtpStartTlsEnable(model.getEmailSmtpStarttlsEnable());
           emailDTO.setEmailPort(model.getEmailPort());
           emailDTO.setEmailSubject(model.getEmailSubject());
           emailDTO.setEmailTransportProtocol(model.getEmailTransportProtocol());
           emailDTO.setEmailText(model.getEmailText());
           emailDTO.setId(model.getId());
           emailDTO.setEmailType(model.getEmailType());
           emailDTO.setEmailUserName(model.getEmailUsername());


        return emailDTO;
    }

    @Override
    public EmailSetting dtoToModelMap(EmailDTO dto) {

        EmailSetting emailSetting =new EmailSetting();
        emailSetting.setDeleted(dto.getDeleted());
        emailSetting.setEmailDebug(dto.getEmailDebug());
        emailSetting.setIsEmailEnabled(dto.getEmailEnabled());
        emailSetting.setEmailFrom(dto.getEmailFrom());
        emailSetting.setEmailHost(dto.getEmailHost());
        emailSetting.setEmailPassword(dto.getEmailPassword());
        emailSetting.setEmailSmtpAuth(dto.getEmailSmtpAuth());
        emailSetting.setEmailSmtpStarttlsEnable(dto.getEmailSmtpStartTlsEnable());
        emailSetting.setEmailPort(dto.getEmailPort());
        emailSetting.setEmailSubject(dto.getEmailSubject());
        emailSetting.setEmailTransportProtocol(dto.getEmailTransportProtocol());
        emailSetting.setEmailText(dto.getEmailText());
        emailSetting.setId(dto.getId());
        emailSetting.setEmailType(dto.getEmailType());
        emailSetting.setEmailUsername(dto.getEmailUserName());







        return emailSetting;
    }

    @Override
    public List<EmailDTO> modelToDTOList(List<EmailSetting> modelList) {
        return null;
    }

    @Override
    public List<EmailSetting> dtoToModelList(List<EmailDTO> dtoList) {
        return null;
    }
}
