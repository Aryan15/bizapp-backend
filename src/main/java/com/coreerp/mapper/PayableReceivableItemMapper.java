package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.PayableReceivableItemDTO;
import com.coreerp.model.PayableReceivableItem;
import com.coreerp.service.InvoiceService;
import com.coreerp.service.PayableReceivableService;

@Component
public class PayableReceivableItemMapper implements AbstractMapper<PayableReceivableItemDTO, PayableReceivableItem> {
	
	@Autowired
	PayableReceivableService payableReceivableService;
	
	@Autowired
	InvoiceService invoiceService;

	@Override
	public PayableReceivableItemDTO modelToDTOMap(PayableReceivableItem model) {
		PayableReceivableItemDTO payableReceivableItemDTO = new PayableReceivableItemDTO();
		
		payableReceivableItemDTO.setAmountAfterTds(model.getAmountAfterTds());
		payableReceivableItemDTO.setDebitAmount(model.getDebitAmount());
		payableReceivableItemDTO.setDueAmount(model.getDueAmount());
		payableReceivableItemDTO.setInvoiceAmount(model.getInvoiceAmount());
		payableReceivableItemDTO.setInvoiceDate(model.getInvoiceDate());
		payableReceivableItemDTO.setInvoiceNumber(model.getInvoiceHeader().getInvoiceNumber());
		payableReceivableItemDTO.setInvoiceHeaderId(model.getInvoiceHeader().getId());
		payableReceivableItemDTO.setInvoiceStatus(model.getInvoiceStatus());
		payableReceivableItemDTO.setPaidAmount(model.getPaidAmount());
		payableReceivableItemDTO.setPayableReceivableHeaderId(model.getPayableReceivableHeader().getId());
		payableReceivableItemDTO.setPayingAmount(model.getPayingAmount());
		payableReceivableItemDTO.setPaymentDueDate(model.getPaymentDueDate());
		payableReceivableItemDTO.setPaymentItemStatus(model.getPaymentItemStatus());
		payableReceivableItemDTO.setTdsAmount(model.getTdsAmount());
		payableReceivableItemDTO.setTdsPercentage(model.getTdsPercentage());
		payableReceivableItemDTO.setId(model.getId());
		payableReceivableItemDTO.setSlNo(model.getSlNo());
		payableReceivableItemDTO.setTaxableAmount(model.getTaxableAmount());
		payableReceivableItemDTO.setTaxAmount(model.getTotalTaxAmount());
		payableReceivableItemDTO.setRoundOff(model.getRoundOff());
		//payableReceivableItemDTO.setCreditDebitHeaderId(model.getCreditDebitHeader() != null ? Arrays.asList(model.getCreditDebitHeader().getId().split(",")) : null);
		
		return payableReceivableItemDTO;
	}

	@Override
	public PayableReceivableItem dtoToModelMap(PayableReceivableItemDTO dto) {
		
		PayableReceivableItem payableReceivableItem  = new PayableReceivableItem ();
		
		payableReceivableItem.setAmountAfterTds(dto.getAmountAfterTds());
		payableReceivableItem.setDebitAmount(dto.getDebitAmount());
		payableReceivableItem.setDueAmount(dto.getDueAmount());
		payableReceivableItem.setId(dto.getId());
		payableReceivableItem.setInvoiceAmount(dto.getInvoiceAmount());
		payableReceivableItem.setInvoiceDate(dto.getInvoiceDate());
		payableReceivableItem.setInvoiceHeader(invoiceService.getInvoiceHeaderByHeaderId(dto.getInvoiceHeaderId()));
		payableReceivableItem.setInvoiceStatus(dto.getInvoiceStatus());
		payableReceivableItem.setPaidAmount(dto.getPaidAmount());
		//payableReceivableItem.setPayableReceivableHeader(payableReceivableService.getModelById(dto.getPayableReceivableHeaderId()));
		payableReceivableItem.setPayingAmount(dto.getPayingAmount());
		payableReceivableItem.setPaymentDueDate(dto.getPaymentDueDate());
		payableReceivableItem.setPaymentItemStatus(dto.getPaymentItemStatus());
		payableReceivableItem.setTdsAmount(dto.getTdsAmount());
		payableReceivableItem.setTdsPercentage(dto.getTdsPercentage());
		payableReceivableItem.setSlNo(dto.getSlNo());
		payableReceivableItem.setTaxableAmount(dto.getTaxableAmount());
		payableReceivableItem.setTotalTaxAmount(dto.getTaxAmount());
		payableReceivableItem.setRoundOff(dto.getRoundOff());
		//payableReceivableItem.setCreditDebitHeader(dto.getCreditDebitHeaderId().toString());
		
		
		return payableReceivableItem;
	}

	@Override
	public List<PayableReceivableItemDTO> modelToDTOList(List<PayableReceivableItem> modelList) {
		
		List<PayableReceivableItemDTO> PayableReceivableItemDTOList = modelList.stream()
					.map(item -> modelToDTOMap(item))
					.collect(Collectors.toList());
		
		return PayableReceivableItemDTOList;
	}

	@Override
	public List<PayableReceivableItem> dtoToModelList(List<PayableReceivableItemDTO> dtoList) {
		
		List<PayableReceivableItem> payableReceivableItemList = dtoList.stream()
				.map(dto -> dtoToModelMap(dto))
				.collect(Collectors.toList());
		
		return payableReceivableItemList;
	}

}
