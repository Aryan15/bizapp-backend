package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dao.PartyTypeRepository;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dto.StockUpdateOnDTO;
import com.coreerp.model.StockUpdateOn;

@Component
public class StockUpdateOnMapper implements AbstractMapper<StockUpdateOnDTO,StockUpdateOn> {

	final static Logger log= LogManager.getLogger(StockUpdateOnMapper.class);
	
	@Autowired
	TransactionTypeRepository transactionTypeRepository ; 
	
//	@Autowired
//	UserTypeRepository userTypeRepository;
	
	@Autowired
	PartyTypeRepository partyTypeRepository;
	
	@Override
	public StockUpdateOnDTO modelToDTOMap(StockUpdateOn model) {
		
		log.info("Mapping model: "+model);
		
		StockUpdateOnDTO  dto = new StockUpdateOnDTO ();
		
		dto.setDeleted(model.getDeleted());
		dto.setId(model.getId());
		dto.setTransactionTypeId(model.getTransactionType() != null ? model.getTransactionType().getId() : null);
		//dto.setUserTypeId(model.getUserType() != null ? model.getUserType().getId() : null);
		dto.setPartyTypeId(model.getPartyType() != null ? model.getPartyType().getId() : null);
		dto.setTransactionTypeName(model.getTransactionType() != null ? model.getTransactionType().getName() : null);
		
		log.info("returning dto: "+dto);
		
		return dto;
	}

	@Override
	public StockUpdateOn dtoToModelMap(StockUpdateOnDTO dto) {

		StockUpdateOn model = new StockUpdateOn();
		
		model.setDeleted(dto.getDeleted());
		model.setId(dto.getId());
		model.setTransactionType(dto.getTransactionTypeId() != null ? transactionTypeRepository.getOne(dto.getTransactionTypeId()) : null);
		//model.setUserType(dto.getUserTypeId() != null ? userTypeRepository.getOne(dto.getUserTypeId()) : null);
		model.setPartyType(dto.getPartyTypeId() != null ? partyTypeRepository.getOne(dto.getPartyTypeId()) : null);
		
		return model;
	}

	@Override
	public List<StockUpdateOnDTO> modelToDTOList(List<StockUpdateOn> modelList) {
		return modelList.stream()
				.map(model -> modelToDTOMap(model))
				.collect(Collectors.toList());
	}

	@Override
	public List<StockUpdateOn> dtoToModelList(List<StockUpdateOnDTO> dtoList) {
		return dtoList.stream()
				.map(dto -> dtoToModelMap(dto))
				.collect(Collectors.toList());
	}

}
