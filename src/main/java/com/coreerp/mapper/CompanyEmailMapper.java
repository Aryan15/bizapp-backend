package com.coreerp.mapper;


import com.coreerp.dto.CompanyDTO;
import com.coreerp.dto.CompanyEmailDTO;
import com.coreerp.model.Company;
import com.coreerp.model.CompanyEmailModel;
import com.coreerp.model.EmailSetting;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CompanyEmailMapper implements AbstractMapper<CompanyEmailDTO, CompanyEmailModel>{

    @Override
    public CompanyEmailDTO modelToDTOMap(CompanyEmailModel model) {
        CompanyEmailDTO emailSetting = new CompanyEmailDTO();
        emailSetting.setId(model.getId());
        emailSetting.setCompanyId(model.getCompanyId());
        emailSetting.setEmailUserName(model.getEmailUserName());
        emailSetting.setEmailPassword(model.getEmailPassword());
        emailSetting.setEmailPort(model.getEmailPort());
        emailSetting.setEmailTemplate(model.getEmailTemplate());
        emailSetting.setEmailSubject(model.getEmailSubject());
        emailSetting.setEmailHost(model.getEmailHost());
        emailSetting.setIsEmailEnabled(model.getIsEmailEnabled());
        emailSetting.setEmailSmtpStarttlsEnable(model.getEmailSmtpStarttlsEnable());
        emailSetting.setEmailSmtpAuth(model.getEmailSmtpAuth());
        emailSetting.setEmailTransportProtocol(model.getEmailTransportProtocol());
        emailSetting.setEmailDebug(model.getEmailDebug());
        emailSetting.setEmailFrom(model.getEmailFrom());
        return emailSetting;
    }

    @Override
    public CompanyEmailModel dtoToModelMap(CompanyEmailDTO dto) {
        CompanyEmailModel emailSetting = new CompanyEmailModel();
        emailSetting.setId(dto.getId());
        emailSetting.setCompanyId(dto.getCompanyId());
        emailSetting.setEmailUserName(dto.getEmailUserName());
        emailSetting.setEmailPassword(dto.getEmailPassword());
        emailSetting.setEmailPort(dto.getEmailPort());
        emailSetting.setEmailTemplate(dto.getEmailTemplate());
        emailSetting.setEmailSubject(dto.getEmailSubject());
        emailSetting.setEmailHost(dto.getEmailHost());
        emailSetting.setIsEmailEnabled(dto.getIsEmailEnabled());
        emailSetting.setEmailSmtpStarttlsEnable(dto.getEmailSmtpStarttlsEnable());
        emailSetting.setEmailSmtpAuth(dto.getEmailSmtpAuth());
        emailSetting.setEmailTransportProtocol(dto.getEmailTransportProtocol());
        emailSetting.setEmailDebug(dto.getEmailDebug());
        emailSetting.setEmailFrom(dto.getEmailFrom());
        return emailSetting;
    }

    @Override
    public List<CompanyEmailDTO> modelToDTOList(List<CompanyEmailModel> modelList) {
        return null;
    }

    @Override
    public List<CompanyEmailModel> dtoToModelList(List<CompanyEmailDTO> dtoList) {
        return null;
    }


}
