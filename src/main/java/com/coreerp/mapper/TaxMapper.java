package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.TaxDTO;
import com.coreerp.model.Tax;

@Component
public class TaxMapper implements AbstractMapper<TaxDTO, Tax>{

	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public TaxDTO modelToDTOMap(Tax model) {
		
		TaxDTO taxDTO = modelMapper.map(model, TaxDTO.class) ;
		return taxDTO;
	}

	@Override
	public Tax dtoToModelMap(TaxDTO dto) {
		Tax tax = modelMapper.map(dto, Tax.class);
		return tax;
	}

	@Override
	public List<TaxDTO> modelToDTOList(List<Tax> modelList) {
		return modelList.stream()
				.map(tax -> modelToDTOMap(tax))
				.collect(Collectors.toList());
	}

	@Override
	public List<Tax> dtoToModelList(List<TaxDTO> dtoList) {
		return dtoList.stream()
				.map(taxDTO -> dtoToModelMap(taxDTO))
				.collect(Collectors.toList());
	}

}
