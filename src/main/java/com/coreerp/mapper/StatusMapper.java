package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.StatusDTO;
import com.coreerp.model.Status;

@Component
public class StatusMapper implements AbstractMapper<StatusDTO, Status> {

	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public StatusDTO modelToDTOMap(Status model) {
		return modelMapper.map(model, StatusDTO.class);
	}

	@Override
	public Status dtoToModelMap(StatusDTO dto) {
		return modelMapper.map(dto,  Status.class);
	}

	@Override
	public List<StatusDTO> modelToDTOList(List<Status> modelList) {
		return modelList.stream()
				.map(material -> modelToDTOMap(material))
				.collect(Collectors.toList());
	}

	@Override
	public List<Status> dtoToModelList(List<StatusDTO> dtoList) {
		return dtoList.stream()
				.map(materialDTO -> dtoToModelMap(materialDTO))
				.collect(Collectors.toList());
	}


	


}
