package com.coreerp.mapper;

import com.coreerp.dao.AutoTransactionLogRepository;
import com.coreerp.dao.InvoiceHeaderRepository;
import com.coreerp.dao.PartyRepository;
import com.coreerp.dto.*;
import com.coreerp.model.*;
import com.coreerp.service.*;
import com.coreerp.serviceimpl.*;
import org.apache.logging.log4j.*;
import org.drools.core.util.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.*;
import java.util.*;
import java.util.stream.*;

@Component
public class AutoTransactionMapper  implements  AbstractMapper<AutoTransactionGenerationDTO, AutoTransactionGeneration>{
    final static Logger log = LogManager.getLogger(AutoTransactionMapper.class);

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    AutoTransactionLogRepository autoTransactionLogRepository;

    @Autowired
    InvoiceHeaderRepository invoiceHeaderRepository;

    @Autowired
    FinancialYearService financialYearService;

    @Autowired
    PartyRepository partyRepository;


    @Override
    public AutoTransactionGenerationDTO modelToDTOMap(AutoTransactionGeneration model) {
        List<Date> dates=new ArrayList<>();
        AutoTransactionGenerationDTO autoTransactionGenerationDTO = modelMapper.map(model, AutoTransactionGenerationDTO.class);
       AutoTransactionLog autoTransactionLog= autoTransactionLogRepository.getAutoTransLogDetails(autoTransactionGenerationDTO.getInvoiceHeaderId());

       Party party= partyRepository.getOne(model.getPartyId());
       autoTransactionGenerationDTO.setPartyName(party.getName());
       if(autoTransactionLog!=null) {
           autoTransactionGenerationDTO.setTotalCount(autoTransactionLog.getTransactionCount());
       }
       List<String> list =new ArrayList<>();
        if(model.getSheaduledDates()!=null) {
            list= Stream.of(model.getSheaduledDates().split(","))
                    .collect(Collectors.toList());

        }
        list.forEach(date->{
          //  Date date1=SimpleDateFormat.(date);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date2  = null;
            try {
                if(date.equalsIgnoreCase("")){

                }
                else {
                    date2 = dateFormat.parse(date);
                    dates.add(date2);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }


        });


        autoTransactionGenerationDTO.setSheduledDates(dates);

        return autoTransactionGenerationDTO;
    }

    @Override
    public AutoTransactionGeneration dtoToModelMap(AutoTransactionGenerationDTO dto) {
        String apprd="";
        AutoTransactionGeneration autoTransactionGeneration = modelMapper.map(dto, AutoTransactionGeneration.class);
        InvoiceHeader invoiceHeader =new InvoiceHeader();

        FinancialYear financialYear = financialYearService.findByIsActive(1);
        if(dto.getInvoiceNumber()!=null){
            invoiceHeader = invoiceHeaderRepository.findByInvoiceNumberAndFinancialYear(autoTransactionGeneration.getInvoiceNumber(), financialYear);
            autoTransactionGeneration.setInvoiceHeaderId(invoiceHeader.getId());
        }

        else if (autoTransactionGeneration.getInvoiceNumber() == null) {
            invoiceHeader = invoiceHeaderRepository.findByHeaderId(autoTransactionGeneration.getInvoiceHeaderId());
            autoTransactionGeneration.setInvoiceNumber(invoiceHeader.getInvoiceNumber());
        }
        invoiceHeader = invoiceHeaderRepository.findByHeaderId(autoTransactionGeneration.getInvoiceHeaderId());
        autoTransactionGeneration.setPartyId(invoiceHeader.getParty().getId());
        final String[] dates = {""};
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if(dto.getSheduledDates()!=null) {
            dto.getSheduledDates().forEach(date -> {
                String strDate = dateFormat.format(date);
                dates[0] = dates[0]+ strDate+",";
            });
        }
        autoTransactionGeneration.setSheaduledDates(dates[0]);
        return autoTransactionGeneration;
    }



    @Override
    public List<AutoTransactionGenerationDTO> modelToDTOList(List<AutoTransactionGeneration> modelList) {
        return modelList.stream()
                .map(autoTransactionGeneration -> modelToDTOMap(autoTransactionGeneration))
                .collect(Collectors.toList());
    }

    @Override
    public List<AutoTransactionGeneration> dtoToModelList(List<AutoTransactionGenerationDTO> dtoList) {

        return dtoList.stream()
                .map(autoTransactionGenerationDTO -> dtoToModelMap(autoTransactionGenerationDTO))
                .collect(Collectors.toList());
    }
}
