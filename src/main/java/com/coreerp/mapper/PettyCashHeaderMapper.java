package com.coreerp.mapper;


import com.coreerp.ApplicationConstants;
import com.coreerp.dto.PayableReceivableHeaderDTO;
import com.coreerp.dto.PettyCashHeaderDTO;

import com.coreerp.dto.VoucherHeaderDTO;
import com.coreerp.model.*;
import com.coreerp.service.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PettyCashHeaderMapper implements AbstractMapper<PettyCashHeaderDTO, PettyCashHeader> {
    @Autowired
    PettyCashItemMapper pettyCashItemMapper;


    @Autowired
    CompanyService companyService;


    @Autowired
    FinancialYearService financialYearService;


    @Autowired
    TransactionTypeService transactionTypeService;

    @Autowired
    TransactionNumberService transactionNumberService;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    StatusService statusService;

    @Override
    public   PettyCashHeaderDTO modelToDTOMap(PettyCashHeader pettyCashHeader){
        PettyCashHeaderDTO pettyCashHeaderDTO=  new PettyCashHeaderDTO();
        pettyCashHeaderDTO.setId(pettyCashHeader.getId());
        pettyCashHeaderDTO.setPeetCashId(pettyCashHeader.getPeetyCashId());
        pettyCashHeaderDTO.setPettyCashNumber(pettyCashHeader.getPettyCashNumber());
        pettyCashHeaderDTO.setPettyCashDate(pettyCashHeader.getPettyCashDate());
        pettyCashHeaderDTO.setApprovedBy(pettyCashHeader.getApprovedBy());
        pettyCashHeaderDTO.setApprovedDate(pettyCashHeader.getApprovedDate());
        pettyCashHeaderDTO.setBalanceAmount(pettyCashHeader.getBalanceAmount());
        pettyCashHeaderDTO.setCashAmount(pettyCashHeader.getCashAmount());
        pettyCashHeaderDTO.setBankAmount(pettyCashHeader.getBankAmount());
        pettyCashHeaderDTO.setComments(pettyCashHeader.getComments());
        pettyCashHeaderDTO.setCompanyId(pettyCashHeader.getCompanyId().getId());
        pettyCashHeaderDTO.setFinancialYearId(pettyCashHeader.getFinancialYearId().getId());
        pettyCashHeaderDTO.setIsDayClosed(pettyCashHeader.getIsDayClosed());
        pettyCashHeaderDTO.setStatusId(pettyCashHeader.getStatus().getId());
        pettyCashHeaderDTO.setStatusName(pettyCashHeader.getStatus().getName());
        pettyCashHeaderDTO.setTotalAmount(pettyCashHeader.getTotalAmount());
        pettyCashHeaderDTO.setTotalIncomingAmount(pettyCashHeader.getTotalIncomingAmount());
        pettyCashHeaderDTO.setTotalOutgoingAmount(pettyCashHeader.getTotalOutgoingAmount());
        pettyCashHeaderDTO.setPettyCashTypeId(pettyCashHeader.getPettyCashTypeId().getId());
        pettyCashHeaderDTO.setPettyCashItems(pettyCashItemMapper.modelToDTOList(pettyCashHeader.getPettyCashItems()));
        pettyCashHeaderDTO.setIsLatest(pettyCashHeader.getIsLatest());

        return pettyCashHeaderDTO;

    }

    @Override
    public PettyCashHeader dtoToModelMap(PettyCashHeaderDTO pettyCashHeaderDTO){
        PettyCashHeader pettyCashHeader = new PettyCashHeader();
        pettyCashHeader.setPeetyCashId(pettyCashHeaderDTO.getPeetCashId() != null ? pettyCashHeaderDTO.getPeetCashId() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(pettyCashHeaderDTO.getPettyCashTypeId())).getTransactionId());
        pettyCashHeader.setPettyCashNumber(pettyCashHeaderDTO.getPettyCashNumber() != null ? pettyCashHeaderDTO.getPettyCashNumber() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(pettyCashHeaderDTO.getPettyCashTypeId())).getTransactionNumber());
        pettyCashHeader.setId(pettyCashHeaderDTO.getId());
        pettyCashHeader.setPettyCashDate(pettyCashHeaderDTO.getPettyCashDate());
        pettyCashHeader.setApprovedBy(pettyCashHeaderDTO.getApprovedBy());
        pettyCashHeader.setApprovedDate(pettyCashHeaderDTO.getApprovedDate());
        pettyCashHeader.setBalanceAmount(pettyCashHeaderDTO.getBalanceAmount());
        pettyCashHeader.setBankAmount(pettyCashHeaderDTO.getBankAmount());
        pettyCashHeader.setCashAmount(pettyCashHeaderDTO.getCashAmount());
        pettyCashHeader.setComments(pettyCashHeaderDTO.getComments());
       pettyCashHeader.setCompanyId(pettyCashHeaderDTO.getCompanyId()!=null ? companyService.getModelById(pettyCashHeaderDTO.getCompanyId()):null);
        pettyCashHeader.setFinancialYearId(pettyCashHeaderDTO.getFinancialYearId() !=null ? financialYearService.getById(pettyCashHeaderDTO.getFinancialYearId()) :null);
        pettyCashHeader.setIsDayClosed(pettyCashHeaderDTO.getIsDayClosed());
        pettyCashHeader.setPettyCashDate(pettyCashHeaderDTO.getPettyCashDate());

        pettyCashHeader.setStatus( pettyCashHeaderDTO.getStatusId() == null ? statusService.getStatusById(ApplicationConstants.NEW_STATUS) :statusService.getStatusById( pettyCashHeaderDTO.getStatusId() ) ) ;
        pettyCashHeader.setTotalAmount(pettyCashHeaderDTO.getTotalAmount());
        pettyCashHeader.setTotalIncomingAmount(pettyCashHeaderDTO.getTotalIncomingAmount());
        pettyCashHeader.setTotalOutgoingAmount(pettyCashHeaderDTO.getTotalOutgoingAmount());
        pettyCashHeader.setPettyCashTypeId(transactionTypeService.getModelById(pettyCashHeaderDTO.getPettyCashTypeId()));
        pettyCashHeader.setPettyCashItems(pettyCashItemMapper.dtoToModelList(pettyCashHeaderDTO.getPettyCashItems()));
        pettyCashHeader.setIsLatest(pettyCashHeaderDTO.getIsLatest());


        return pettyCashHeader;
    }







    @Override
    public List<PettyCashHeaderDTO> modelToDTOList(List<PettyCashHeader> modelList) {

        return modelList.stream()
                .map(pettyCashHeader -> modelToDTOMap(pettyCashHeader))
                .collect(Collectors.toList());

    }



    @Override
    public List<PettyCashHeader> dtoToModelList(List<PettyCashHeaderDTO> dtoList) {

        List<PettyCashHeader>  pettyCashHeaderList =
                dtoList.stream()
                        .map(dto -> dtoToModelMap(dto))
                        .collect(Collectors.toList());

        return pettyCashHeaderList;
    }


    public PettyCashHeaderAudit modelToAudit(PettyCashHeader pettyCashHeader){
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PettyCashHeaderAudit out = modelMapper.map(pettyCashHeader,PettyCashHeaderAudit .class);
        modelMapper.getConfiguration().setAmbiguityIgnored(false);
        return out;
    }

    public List<PettyCashHeaderAudit> modelToAuditList(List<PettyCashHeader> dtoList){
        return dtoList.stream()
                .map(dto -> modelToAudit(dto))
                .collect(Collectors.toList());
    }
}
