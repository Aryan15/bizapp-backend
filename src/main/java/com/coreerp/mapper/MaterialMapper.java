package com.coreerp.mapper;

import java.io.Console;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.controller.MaterialController;
import com.coreerp.dao.SupplyTypeRepository;
import com.coreerp.dto.MaterialDTO;
import com.coreerp.model.Material;
import com.coreerp.service.CompanyService;
import com.coreerp.service.MaterialTypeService;
import com.coreerp.service.PartyService;
import com.coreerp.service.TaxService;
import com.coreerp.service.UnitOfMeasurementService;

@Component
public class MaterialMapper implements AbstractMapper<MaterialDTO, Material> {

//	@Autowired
//    private ModelMapper modelMapper;
	
	final static Logger log = LogManager.getLogger(MaterialMapper.class);
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	MaterialTypeService materialTypeService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	SupplyTypeRepository supplyTypeRepository;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	UnitOfMeasurementService uomService;
	
	@Override
	public MaterialDTO modelToDTOMap(Material model) {
		MaterialDTO dto = new MaterialDTO(); //modelMapper.map(model, MaterialDTO.class);
		
		dto.setBuyingPrice(model.getBuyingPrice());
		dto.setCessPercentage(model.getCessPercentage());
		dto.setCompanyId(model.getCompany().getId());
		dto.setDiscountPercentage(model.getDiscountPercentage());
		dto.setHsnCode(model.getHsnCode());
		dto.setId(model.getId());
		dto.setMaterialTypeId(model.getMaterialType() != null ? model.getMaterialType().getId() : null);
		dto.setMrp(model.getMrp());
		dto.setName(model.getName());
		dto.setPartNumber(model.getPartNumber());
		dto.setPartyId(model.getParty() != null ? model.getParty().getId() : null);
		dto.setPrice(model.getPrice());
		dto.setSpecification(model.getSpecification());
//		dto.setStock(model.getOpeningStock() != null ? model.getOpeningStock() + (model.getStock() !=null ? model.getStock() : 0) : 0 + (model.getStock() !=null ? model.getStock() : 0));
		if(model.getMaterialType() != null && model.getMaterialType().getName().equalsIgnoreCase(ApplicationConstants.MATERIAL_TYPE_JOBWORK)) {
			dto.setStock(null);
			dto.setOpeningStock(null);
		}
		else {
			dto.setStock(model.getStock());
			dto.setOpeningStock(model.getOpeningStock());
		}
		dto.setOpeningStock(model.getOpeningStock());
		dto.setSupplyTypeId(model.getSupplyType().getId());
		dto.setTaxId(model.getTax() != null ? model.getTax().getId() : null);
		dto.setUnitOfMeasurementId(model.getUnitOfMeasurement() != null ? model.getUnitOfMeasurement().getId() : null);
		dto.setUnitOfMeasurementName(model.getUnitOfMeasurement() != null ? model.getUnitOfMeasurement().getName() : null);
		dto.setSpecification(model.getSpecification());
		dto.setImagePath(model.getImagePath());
		dto.setMinimumStock(model.getMinimumStock());
		dto.setOutName(model.getOutName());
		dto.setOutPartNumber(model.getOutPartNumber());
		dto.setIsContainer(model.getIsContainer());
		dto.setMaterialTypeName(model.getMaterialType() != null ? model.getMaterialType().getName() : null);
		return dto;
	}

	@Override
	public Material dtoToModelMap(MaterialDTO dto) {
		Material model = new Material();//modelMapper.map(dto, Material.class);
		
		model.setBuyingPrice(dto.getBuyingPrice());
		model.setCessPercentage(dto.getCessPercentage());
		model.setCompany(companyService.getModelById(dto.getCompanyId()));
		model.setDiscountPercentage(dto.getDiscountPercentage());
		model.setHsnCode(dto.getHsnCode());
		model.setId(dto.getId());
		model.setMaterialType(dto.getMaterialTypeId() != null ? materialTypeService.getById(dto.getMaterialTypeId()) : null);
		model.setMrp(dto.getMrp());
		model.setName(dto.getName());
		model.setOpeningStock(dto.getOpeningStock());
		model.setPartNumber(dto.getPartNumber());
		model.setParty(dto.getPartyId() != null ? partyService.getPartyObjectById(dto.getPartyId()) : null);
		model.setPrice(dto.getPrice());
		model.setSpecification(dto.getSpecification());
		model.setStock(dto.getStock());
		
//		model.setStock((dto.getOpeningStock() != null  ? dto.getOpeningStock() : 0) > 0 ? (dto.getStock() != null ? (dto.getStock() - dto.getOpeningStock()) : 0) : dto.getStock());
		model.setSupplyType(supplyTypeRepository.getOne(dto.getSupplyTypeId()));
		model.setTax(dto.getTaxId() != null ? taxService.getById(dto.getTaxId()) : null);
		model.setUnitOfMeasurement(dto.getUnitOfMeasurementId() != null ? uomService.getById(dto.getUnitOfMeasurementId()) : null);
		model.setImagePath(dto.getImagePath());
		model.setOpeningStock(dto.getOpeningStock());
		model.setMinimumStock(dto.getMinimumStock());
		model.setOutName(dto.getOutName());
		model.setOutPartNumber(dto.getOutPartNumber());
		model.setIsContainer(dto.getIsContainer());
		
		log.info("material dto  stock"+ dto.getStock());
		log.info("material dto opening stock"+ dto.getOpeningStock());
		
		
		log.info("material stock"+ model.getStock());
		log.info("material opening stock"+ model.getOpeningStock());
		
		return model;
	}

	@Override
	public List<MaterialDTO> modelToDTOList(List<Material> modelList) {
		return modelList.stream()
						.map(material -> modelToDTOMap(material))
						.collect(Collectors.toList());
	}

	@Override
	public List<Material> dtoToModelList(List<MaterialDTO> dtoList) {
		return dtoList.stream()
						.map(materialDTO -> dtoToModelMap(materialDTO))
						.collect(Collectors.toList());
	}

}
