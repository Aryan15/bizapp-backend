package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.ProcessRepository;
import com.coreerp.dto.DeliveryChallanItemDTO;
import com.coreerp.model.DeliveryChallanItem;
import com.coreerp.service.DeliveryChallanService;
import com.coreerp.service.MaterialService;
import com.coreerp.service.PurchaseOrderService;
import com.coreerp.service.TaxService;
import com.coreerp.service.UnitOfMeasurementService;

@Component
public class DeliveryChallanItemMapper implements AbstractMapper<DeliveryChallanItemDTO, DeliveryChallanItem> {

	final static Logger log = LogManager.getLogger(DeliveryChallanItemMapper.class);

	@Autowired
	DeliveryChallanService deliveryChallanService;
	
	@Autowired
	PurchaseOrderService purchaseOrderService;
	
	@Autowired
	DeliveryChallanHeaderMapper deliveryChallanHeaderMapper; 
	
	@Autowired
	MaterialService materialService;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	UnitOfMeasurementService unitOfMeasurementService;
	
	@Autowired
	ProcessRepository processRepository;
	Double materialPrice=0.0;
	
	@Override
	public DeliveryChallanItemDTO modelToDTOMap(DeliveryChallanItem model) {
		DeliveryChallanItemDTO deliveryChallanItemDTO = new  DeliveryChallanItemDTO();
		log.info("deliveryChallanItemDTO = "+model.getAmount());
		deliveryChallanItemDTO.setAmount(model.getAmount());
		deliveryChallanItemDTO.setBatchCode(model.getBatchCode());
		deliveryChallanItemDTO.setBatchCodeId(model.getBatchCodeId());
		deliveryChallanItemDTO.setCgstTaxAmount(model.getCgstTaxAmount());
		deliveryChallanItemDTO.setCgstTaxPercentage(model.getCgstTaxRate());
		deliveryChallanItemDTO.setDeliveryChallanItemStatus(model.getDeliveryChallanItemStatus());
		deliveryChallanItemDTO.setDeliveryChallanType(model.getDeliveryChallanType());
		deliveryChallanItemDTO.setHeaderId(model.getDeliveryChallanHeader().getId());
		deliveryChallanItemDTO.setId(model.getId());
		deliveryChallanItemDTO.setIgstTaxAmount(model.getIgstTaxAmount());
		deliveryChallanItemDTO.setIgstTaxPercentage(model.getIgstTaxRate());
		deliveryChallanItemDTO.setInDeliveryChallanNumber(model.getInDeliveryChallanNumber());
		deliveryChallanItemDTO.setInDeliveryChallanDate(model.getInDeliveryChallanDate());
		deliveryChallanItemDTO.setMaterialId(model.getMaterial().getId());
		deliveryChallanItemDTO.setMaterialName(materialService.getMaterialById(model.getMaterial().getId()).getName());
		if(model.getDeliveryChallanHeader().getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_CUSTOMER)) {
			materialPrice = (model.getPrice() == null || model.getPrice() == 0.0) ? model.getMaterial().getPrice() : model.getPrice();
			
		}
		if(model.getDeliveryChallanHeader().getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_SUPPLIER)) {
			materialPrice = (model.getPrice() == null || model.getPrice() == 0.0) ? model.getMaterial().getBuyingPrice(): model.getPrice();
			
		}
		if(model.getDeliveryChallanHeader().getDeliveryChallanType().getName().equals(ApplicationConstants.INCOMING_JOBWORK_IN_DC)) {
			materialPrice = (model.getPrice() == null || model.getPrice() == 0.0) ? model.getMaterial().getPrice(): model.getPrice();
			
		}
		if(model.getDeliveryChallanHeader().getDeliveryChallanType().getName().equals(ApplicationConstants.OUTGOING_JOBWORK_OUT_DC)) {
			materialPrice = (model.getPrice() == null || model.getPrice() == 0.0) ? model.getMaterial().getBuyingPrice(): model.getPrice();
			
		}
		
		if(model.getDeliveryChallanHeader().getDeliveryChallanType().getName().equals(ApplicationConstants.INCOMING_JOBWORK_OUT_DC)) {
			materialPrice = (model.getPrice() == null || model.getPrice() == 0.0) ? model.getMaterial().getPrice(): model.getPrice();
			
		}
		if(model.getDeliveryChallanHeader().getDeliveryChallanType().getName().equals(ApplicationConstants.OUTGOING_JOBWORK_IN_DC)) {
			materialPrice = (model.getPrice() == null || model.getPrice() == 0.0) ? model.getMaterial().getBuyingPrice(): model.getPrice();
			
		}
		deliveryChallanItemDTO.setPrice(materialPrice);
		
		/*deliveryChallanItemDTO.setPrice((model.getPrice() == null || model.getPrice() == 0.0) ? (model.getDeliveryChallanHeader().getDeliveryChallanType().getName().equals(ApplicationConstants.DC_TYPE_CUSTOMER) ? model.getMaterial().getPrice() : model.getMaterial().getBuyingPrice() ) : model.getPrice()); */
		deliveryChallanItemDTO.setProcessId(model.getProcessId());
		deliveryChallanItemDTO.setProcessName(model.getProcessName());
		//deliveryChallanItemDTO.setPurchaseOrderHeaders(model.get);
		deliveryChallanItemDTO.setPurchaseOrderHeaderId(model.getPurchaseOrderItem() != null ? model.getPurchaseOrderItem().getPurchaseOrderHeader().getId() : null);
		deliveryChallanItemDTO.setPurchaseOrderNumber(model.getPurchaseOrderNumber());
		deliveryChallanItemDTO.setPurchaseOrderItemId(model.getPurchaseOrderItem() != null ? model.getPurchaseOrderItem().getId() : null);
		deliveryChallanItemDTO.setQuantity(model.getQuantity());
		deliveryChallanItemDTO.setRemarks(model.getRemarks());
		deliveryChallanItemDTO.setSgstTaxAmount(model.getSgstTaxAmount());
		deliveryChallanItemDTO.setSgstTaxPercentage(model.getSgstTaxRate());
		deliveryChallanItemDTO.setSlNo(model.getSlNo());
		//log.info("model.getStatus() deliveryChallanItemDTO ==== "+model.getStatus());
		deliveryChallanItemDTO.setStatus(model.getStatus());
		deliveryChallanItemDTO.setTaxAmount(model.getTaxAmount());
		deliveryChallanItemDTO.setTaxId(model.getTax() != null ? model.getTax().getId() : null);
		deliveryChallanItemDTO.setTaxRate(model.getTaxRate());
//		deliveryChallanItemDTO.setUnitOfMeasurementId(model.getUnitOfMeasurement().getId());
		deliveryChallanItemDTO.setUnitOfMeasurementId(model.getUnitOfMeasurement() != null ? model.getUnitOfMeasurement().getId() : null);
		deliveryChallanItemDTO.setPartNumber(model.getMaterial().getPartNumber());
		deliveryChallanItemDTO.setPartName(model.getMaterial().getName());
		deliveryChallanItemDTO.setHsnOrSac(model.getMaterial().getHsnCode());
		deliveryChallanItemDTO.setUom(model.getUnitOfMeasurement() != null ? model.getUnitOfMeasurement().getName() : null);
		deliveryChallanItemDTO.setInclusiveTax(model.getInclusiveTax());
		deliveryChallanItemDTO.setDiscountAmount(model.getDiscountAmount());
		deliveryChallanItemDTO.setDiscountPercentage(model.getDiscountPercentage());
		deliveryChallanItemDTO.setAmountAfterDiscount(model.getAmountAfterDiscount());
		deliveryChallanItemDTO.setInvoiceBalanceQuantity(model.getInvoiceBalanceQuantity());
		deliveryChallanItemDTO.setGrnBalanceQuantity(model.getGrnBalanceQuantity());
		deliveryChallanItemDTO.setSourceDeliveryChallanHeaderId(model.getSourceDeliveryChallanItem() != null ? model.getSourceDeliveryChallanItem().getDeliveryChallanHeader().getId() : null );
		deliveryChallanItemDTO.setSourceDeliveryChallanItemId(model.getSourceDeliveryChallanItem() != null ? model.getSourceDeliveryChallanItem().getId() : null);
		deliveryChallanItemDTO.setDcBalanceQuantity(model.getDcBalanceQuantity());
		deliveryChallanItemDTO.setIncomingQuantity(model.getIncomingQuantity());
		deliveryChallanItemDTO.setSpecification(model.getSpecification());
		deliveryChallanItemDTO.setSourceDeliveryChallanNumber(model.getSourceDeliveryChallanItem() != null ? model.getSourceDeliveryChallanItem().getDeliveryChallanHeader().getDeliveryChallanNumber() : null);
		deliveryChallanItemDTO.setSourceDeliveryChallanDate(model.getSourceDeliveryChallanItem() != null ? model.getSourceDeliveryChallanItem().getDeliveryChallanHeader().getDeliveryChallanDate() : null);
		deliveryChallanItemDTO.setIsContainer(model.getMaterial().getIsContainer());
		deliveryChallanItemDTO.setOutName(model.getMaterial().getOutName());
		deliveryChallanItemDTO.setOutPartNumber(model.getMaterial().getOutPartNumber());
		deliveryChallanItemDTO.setJwNoteItemId(model.getJwNoteItemId());
		
		return deliveryChallanItemDTO;
	}

	@Override
	public DeliveryChallanItem dtoToModelMap(DeliveryChallanItemDTO dto) {
		
		DeliveryChallanItem deliveryChallanItem = new DeliveryChallanItem();

		deliveryChallanItem.setAmount(dto.getAmount());

		deliveryChallanItem.setBatchCode(dto.getBatchCode());
		deliveryChallanItem.setBatchCodeId(dto.getBatchCodeId());
		deliveryChallanItem.setCgstTaxAmount(dto.getCgstTaxAmount());
		deliveryChallanItem.setCgstTaxRate(dto.getCgstTaxPercentage());
		//deliveryChallanItem.setDeliveryChallanHeader(deliveryChallanHeaderMapper.dtoToModelMap(deliveryChallanService.getById(new TransactionPK(dto.getHeaderId(), dto.getVersion()))));
		deliveryChallanItem.setDeliveryChallanItemStatus(dto.getStatus());
		deliveryChallanItem.setDeliveryChallanType(dto.getDeliveryChallanType());
		deliveryChallanItem.setId(dto.getId());
		deliveryChallanItem.setIgstTaxAmount(dto.getIgstTaxAmount());
		deliveryChallanItem.setIgstTaxRate(dto.getIgstTaxPercentage());
		deliveryChallanItem.setInDeliveryChallanDate(dto.getInDeliveryChallanDate());
		deliveryChallanItem.setInDeliveryChallanNumber(dto.getInDeliveryChallanNumber());
		deliveryChallanItem.setMaterial(materialService.getMaterialById(dto.getMaterialId()));
		deliveryChallanItem.setPrice(dto.getPrice());
		deliveryChallanItem.setProcessId(dto.getProcessId());
		deliveryChallanItem.setProcessName(dto.getProcessName());
		//deliveryChallanItem.setPurchaseOrderHeader(dto.getPurchaseOrderHeaderId() != null ? purchaseOrderService.getModelById(dto.getPurchaseOrderHeaderId()) : null);
		deliveryChallanItem.setPurchaseOrderNumber(dto.getPurchaseOrderNumber());
		deliveryChallanItem.setPurchaseOrderItem(dto.getPurchaseOrderItemId() != null ? purchaseOrderService.getItemModelById(dto.getPurchaseOrderItemId()) : null);
		deliveryChallanItem.setQuantity(dto.getQuantity());
		deliveryChallanItem.setRemarks(dto.getRemarks());
		deliveryChallanItem.setSgstTaxAmount(dto.getSgstTaxAmount());
		deliveryChallanItem.setSgstTaxRate(dto.getSgstTaxPercentage());
		deliveryChallanItem.setSlNo(dto.getSlNo());
		log.info("deliveryChallanItem = "+dto.getStatus());
		deliveryChallanItem.setStatus(dto.getStatus());
		deliveryChallanItem.setTax(dto.getTaxId() != null ?  taxService.getById(dto.getTaxId()) : null);
		deliveryChallanItem.setTaxAmount(dto.getTaxAmount());
		deliveryChallanItem.setTaxRate(dto.getTaxAmount());
		deliveryChallanItem.setTaxRate(dto.getTaxRate());
		//deliveryChallanItem.setUnitOfMeasurement(unitOfMeasurementService.getById(dto.getUnitOfMeasurementId()));
		deliveryChallanItem.setUnitOfMeasurement(dto.getUnitOfMeasurementId() != null ? unitOfMeasurementService.getById(dto.getUnitOfMeasurementId()) : null);
		deliveryChallanItem.setInclusiveTax(dto.getInclusiveTax());
		deliveryChallanItem.setDiscountAmount(dto.getDiscountAmount());
		deliveryChallanItem.setDiscountPercentage(dto.getDiscountPercentage());
		deliveryChallanItem.setAmountAfterDiscount(dto.getAmountAfterDiscount());
		deliveryChallanItem.setInvoiceBalanceQuantity(dto.getInvoiceBalanceQuantity() != null ? dto.getInvoiceBalanceQuantity() : dto.getQuantity());
		deliveryChallanItem.setGrnBalanceQuantity(dto.getGrnBalanceQuantity() != null ? dto.getGrnBalanceQuantity() : dto.getQuantity());
		//deliveryChallanItem.setSourceDeliveryChallanHeader(dto.getSourceDeliveryChallanHeaderId() != null ? deliveryChallanService.getModelById(dto.getSourceDeliveryChallanHeaderId()) : null);
		deliveryChallanItem.setSourceDeliveryChallanItem(dto.getSourceDeliveryChallanItemId() != null ? deliveryChallanService.getItemModelById(dto.getSourceDeliveryChallanItemId()) : null);
		deliveryChallanItem.setIncomingQuantity(dto.getIncomingQuantity());
		deliveryChallanItem.setDcBalanceQuantity(dto.getDcBalanceQuantity() != null ? dto.getDcBalanceQuantity() : dto.getQuantity());
		deliveryChallanItem.setSpecification(dto.getSpecification());
		deliveryChallanItem.setJwNoteItemId(dto.getJwNoteItemId());

		return deliveryChallanItem;
	}

	@Override
	public List<DeliveryChallanItemDTO> modelToDTOList(List<DeliveryChallanItem> modelList) {
		
		
		return modelList.stream()
					.map(deliveryChallanItem -> modelToDTOMap(deliveryChallanItem))
					.collect(Collectors.toList());
	}

	@Override
	public List<DeliveryChallanItem> dtoToModelList(List<DeliveryChallanItemDTO> dtoList) {
		
		
		return dtoList.stream()
				.map(deliveryChallanItemDTO -> dtoToModelMap(deliveryChallanItemDTO))
				.collect(Collectors.toList());
	}

}
