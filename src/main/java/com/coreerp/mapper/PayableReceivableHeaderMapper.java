package com.coreerp.mapper;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.PayableReceivableHeaderDTO;
import com.coreerp.model.PayableReceivableHeader;
import com.coreerp.service.CardTypeService;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.PartyService;
import com.coreerp.service.PaymentMethodService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TransactionNumberService;
import com.coreerp.service.TransactionTypeService;
import com.coreerp.serviceimpl.PayableReceivableBackTrackingService;

@Component
public class PayableReceivableHeaderMapper implements AbstractMapper<PayableReceivableHeaderDTO, PayableReceivableHeader> {

	final static Logger log = LogManager.getLogger(PayableReceivableHeaderMapper.class);
	
//	@Autowired
//	private ModelMapper modelMapper;
	
	@Autowired
	CardTypeService cardTypeService;
	
	@Autowired
	PaymentMethodService paymentMethodService;
	
	@Autowired
	FinancialYearService financialYearService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	PartyService partyService;
	
	
	@Autowired
	PayableReceivableItemMapper payableReceivableItemMapper;
	
	@Autowired
	TransactionNumberService transactionNumberService;
	
	@Autowired
	StatusService statusService;
	
	@Override
	public PayableReceivableHeaderDTO modelToDTOMap(PayableReceivableHeader model) {
		//return modelMapper.map(model, PayableReceivableHeaderDTO.class);
		
		PayableReceivableHeaderDTO dto = new PayableReceivableHeaderDTO ();
		
		dto.setBankName(model.getBankName());
		dto.setCardTypeId(model.getCardType() != null ? model.getCardType().getId() : null);
		dto.setClearingMode(model.getClearingMode());
		dto.setCompanyId(model.getCompanyId());
		dto.setFinancialYearId(model.getFinancialYear().getId());
		dto.setId(model.getId());
		dto.setIsPost(model.getIsPost());
		dto.setPartyId(model.getParty().getId());
		dto.setPayableReceivableItems(payableReceivableItemMapper.modelToDTOList(model.getPayableReceivableItems()));
		dto.setPaymentAmount(model.getPaymentAmount());
		dto.setPaymentDocumentDate(model.getPaymentDocumentDate());
		dto.setPaymentDocumentNumber(model.getPaymentDocumentNumber());
		dto.setPaymentMethodId(model.getPaymentMethod() != null ? model.getPaymentMethod().getId() : null);
		dto.setPaymentNote(model.getPaymentNote());
		dto.setPaymentStatus(model.getPaymentStatus());
		dto.setPayReferenceDate(model.getPayReferenceDate());
		dto.setPayReferenceId(model.getPayReferenceId());
		dto.setPayReferenceNumber(model.getPayReferenceNumber());
		dto.setStatusId(model.getStatus().getId());
		dto.setPartyName(model.getParty().getName());
		dto.setStatusName(model.getStatus().getName());
		dto.setTransactionTypeId(model.getTransactionType().getId());
		dto.setCreditDebitNumber(model.getCreditDebitNumber() != null ? Arrays.asList(model.getCreditDebitNumber().split(",")) : null);
		dto.setCreditDebitId(model.getCreditDebitId() != null ? Arrays.asList(model.getCreditDebitId().split(",")) : null);
		dto.setPaymentMode(model.getPaymentMethod().getName());
		
		dto.setCompanyName(model.getCompanyName());
		dto.setCompanygGstRegistrationTypeId(model.getCompanygGstRegistrationTypeId());
		dto.setCompanyPinCode(model.getCompanyPinCode());
		dto.setCompanyStateId(model.getCompanyStateId());
		dto.setCompanyStateName(model.getCompanyStateName());
		dto.setCompanyCountryId(model.getCompanyCountryId());
		dto.setCompanyPrimaryMobile(model.getCompanyPrimaryMobile());
		dto.setCompanySecondaryMobile(model.getCompanySecondaryMobile());
		dto.setCompanyContactPersonNumber(model.getCompanyContactPersonNumber());
		dto.setCompanyContactPersonName(model.getCompanyContactPersonName());
		dto.setCompanyPrimaryTelephone(model.getCompanyPrimaryTelephone());
		dto.setCompanySecondaryTelephone(model.getCompanySecondaryTelephone());
		dto.setCompanyWebsite(model.getCompanyWebsite());
		dto.setCompanyEmail(model.getCompanyEmail());
		dto.setCompanyFaxNumber(model.getCompanyFaxNumber());
		dto.setCompanyAddress(model.getCompanyAddress());
		dto.setCompanyTagLine(model.getCompanyTagLine());
		dto.setCompanyGstNumber(model.getCompanyGstNumber());
		dto.setCompanyPanNumber(model.getCompanyPanNumber());
		dto.setCompanyPanDate(model.getCompanyPanDate());
		dto.setCompanyCeritificateImagePath(model.getCompanyCeritificateImagePath());
		dto.setCompanyLogoPath(model.getCompanyLogoPath());
		
		dto.setPartyName(model.getPartyName());
		dto.setPartyContactPersonNumber(model.getPartyContactPersonNumber());
		dto.setPartyPinCode(model.getPartyPinCode());
		dto.setPartyAreaId(model.getPartyAreaId());
		dto.setPartyCityId(model.getPartyCityId());
		dto.setPartyStateId(model.getPartyStateId());
		dto.setPartyCountryId(model.getPartyCountryId());
		dto.setPartyPrimaryTelephone(model.getPartyPrimaryTelephone());
		dto.setPartySecondaryTelephone(model.getPartySecondaryTelephone());
		dto.setPartyPrimaryMobile(model.getPartyPrimaryMobile());
		dto.setPartySecondaryMobile(model.getPartySecondaryMobile());
		dto.setPartyEmail(model.getPartyEmail());
		dto.setPartyWebsite(model.getPartyWebsite());
		dto.setPartyContactPersonName(model.getPartyContactPersonName());
		dto.setPartyBillToAddress(model.getPartyBillToAddress());
		dto.setPartyShipAddress(model.getPartyBillToAddress());
		dto.setPartyDueDaysLimit(model.getPartyDueDaysLimit());
		dto.setPartyGstRegistrationTypeId(model.getPartyGstRegistrationTypeId());
		dto.setPartyGstNumber(model.getPartyGstNumber());
		dto.setPartyPanNumber(model.getPartyPanNumber());
		dto.setIsIgst(model.getIsIgst());
		dto.setPartyCode(model.getParty().getPartyCode());
		dto.setCreatedBy(model.getCreatedBy());
		dto.setUpdateBy(model.getUpdatedBy());
		dto.setCreatedDate(model.getCreatedDateTime());
		dto.setUpdatedDate(model.getUpdatedDateTime());
		
		return dto;
		
	}

	@Override
	public PayableReceivableHeader dtoToModelMap(PayableReceivableHeaderDTO dto) {
		
		PayableReceivableHeader  model = new PayableReceivableHeader();
		//return modelMapper.map(dto, PayableReceivableHeader.class);
		
		model.setBankName(dto.getBankName());
		model.setCardType(dto.getCardTypeId() != null ? cardTypeService.getById(dto.getCardTypeId()) : null);
		model.setClearingMode(dto.getClearingMode());
		model.setCompanyId(dto.getCompanyId());
		model.setFinancialYear(financialYearService.getById(dto.getFinancialYearId()));
		model.setId(dto.getId());
		model.setIsPost(dto.getIsPost());
		model.setParty(partyService.getPartyObjectById(dto.getPartyId()));
		model.setPayableReceivableItems(payableReceivableItemMapper.dtoToModelList(dto.getPayableReceivableItems()));
		model.setPaymentAmount(dto.getPaymentAmount());
		model.setPaymentDocumentDate(dto.getPaymentDocumentDate());
		model.setPaymentDocumentNumber(dto.getPaymentDocumentNumber());
		model.setPaymentMethod(paymentMethodService.getById(dto.getPaymentMethodId()));
		model.setPaymentNote(dto.getPaymentNote());
		model.setPaymentStatus(dto.getPaymentStatus());
		model.setPayReferenceDate(dto.getPayReferenceDate());
		model.setPayReferenceId(dto.getPayReferenceId() != null ? dto.getPayReferenceId() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getTransactionTypeId())).getTransactionId());
		model.setPayReferenceNumber(dto.getPayReferenceNumber() != null ? dto.getPayReferenceNumber() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getTransactionTypeId())).getTransactionNumber());
		model.setStatus(dto.getStatusId() != null ? statusService.getStatusById(dto.getStatusId()) : null);
		model.setTransactionType(transactionTypeService.getModelById(dto.getTransactionTypeId()));
		
		model.setCompanyName(dto.getCompanyName());
		model.setCompanygGstRegistrationTypeId(dto.getCompanygGstRegistrationTypeId());
		model.setCompanyPinCode(dto.getCompanyPinCode());
		model.setCompanyStateId(dto.getCompanyStateId());
		model.setCompanyStateName(dto.getCompanyStateName());
		model.setCompanyCountryId(dto.getCompanyCountryId());
		model.setCompanyPrimaryMobile(dto.getCompanyPrimaryMobile());
		model.setCompanySecondaryMobile(dto.getCompanySecondaryMobile());
		model.setCompanyContactPersonNumber(dto.getCompanyContactPersonNumber());
		model.setCompanyContactPersonName(dto.getCompanyContactPersonName());
		model.setCompanyPrimaryTelephone(dto.getCompanyPrimaryTelephone());
		model.setCompanySecondaryTelephone(dto.getCompanySecondaryTelephone());
		model.setCompanyWebsite(dto.getCompanyWebsite());
		model.setCompanyEmail(dto.getCompanyEmail());
		model.setCompanyFaxNumber(dto.getCompanyFaxNumber());
		model.setCompanyAddress(dto.getCompanyAddress());
		model.setCompanyTagLine(dto.getCompanyTagLine());
		model.setCompanyGstNumber(dto.getCompanyGstNumber());
		model.setCompanyPanNumber(dto.getCompanyPanNumber());
		model.setCompanyPanDate(dto.getCompanyPanDate());
		model.setCompanyCeritificateImagePath(dto.getCompanyCeritificateImagePath());
		model.setCompanyLogoPath(dto.getCompanyLogoPath());
		
		model.setPartyName(dto.getPartyName());
		model.setPartyContactPersonNumber(dto.getPartyContactPersonNumber());
		model.setPartyPinCode(dto.getPartyPinCode());
		model.setPartyAreaId(dto.getPartyAreaId());
		model.setPartyCityId(dto.getPartyCityId());
		model.setPartyStateId(dto.getPartyStateId());
		model.setPartyCountryId(dto.getPartyCountryId());
		model.setPartyPrimaryTelephone(dto.getPartyPrimaryTelephone());
		model.setPartySecondaryTelephone(dto.getPartySecondaryTelephone());
		model.setPartyPrimaryMobile(dto.getPartyPrimaryMobile());
		model.setPartySecondaryMobile(dto.getPartySecondaryMobile());
		model.setPartyEmail(dto.getPartyEmail());
		model.setPartyWebsite(dto.getPartyWebsite());
		model.setPartyContactPersonName(dto.getPartyContactPersonName());
		model.setPartyBillToAddress(dto.getPartyBillToAddress());
		model.setPartyShipAddress(dto.getPartyBillToAddress());
		model.setPartyDueDaysLimit(dto.getPartyDueDaysLimit());
		model.setPartyGstRegistrationTypeId(dto.getPartyGstRegistrationTypeId());
		model.setPartyGstNumber(dto.getPartyGstNumber());
		model.setPartyPanNumber(dto.getPartyPanNumber());
		model.setIsIgst(dto.getIsIgst());
		
		if(dto.getCreditDebitNumber() != null ){
			String tempCrDrNumber = dto.getCreditDebitNumber().toString();
			
			log.info("tempCrDrNumber 1: "+tempCrDrNumber);
			
			tempCrDrNumber = tempCrDrNumber.substring(tempCrDrNumber.lastIndexOf('[')+1);
			
			log.info("tempCrDrNumber 2: "+tempCrDrNumber);
			
			tempCrDrNumber = tempCrDrNumber.substring(0, tempCrDrNumber.lastIndexOf(']'));
			
			log.info("tempCrDrNumber 3: "+tempCrDrNumber);
			
			model.setCreditDebitNumber(tempCrDrNumber);
		}
		
		if(dto.getCreditDebitId() != null ){
			String tempCrDrId = dto.getCreditDebitId().toString();
			
			log.info("tempCrDrNumber 1: "+tempCrDrId);
			
			tempCrDrId = tempCrDrId.substring(tempCrDrId.lastIndexOf('[')+1);
			
			log.info("tempCrDrNumber 2: "+tempCrDrId);
			
			tempCrDrId = tempCrDrId.substring(0, tempCrDrId.lastIndexOf(']'));
			
			log.info("tempCrDrNumber 3: "+tempCrDrId);
			
			model.setCreditDebitId(tempCrDrId);
		}

		model.setCreatedBy(dto.getCreatedBy());

		model.setCreatedDateTime(dto.getCreatedDate());
		
		return model ;
	}

	@Override
	public List<PayableReceivableHeaderDTO> modelToDTOList(List<PayableReceivableHeader> modelList) {
		
		List<PayableReceivableHeaderDTO> payableReceivableHeaderDTOListOut = 
		modelList.stream()
				.map(payableReceivableHeader -> modelToDTOMap(payableReceivableHeader))
				.collect(Collectors.toList());
		
		return payableReceivableHeaderDTOListOut;
	}

	@Override
	public List<PayableReceivableHeader> dtoToModelList(List<PayableReceivableHeaderDTO> dtoList) {
		
		List<PayableReceivableHeader> PayableReceivableHeaderList = 
				dtoList.stream()
						.map(dto -> dtoToModelMap(dto))
						.collect(Collectors.toList());
		
		return PayableReceivableHeaderList;
	}

}
