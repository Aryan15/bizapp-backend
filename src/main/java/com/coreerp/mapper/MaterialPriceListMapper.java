package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.MaterialPriceListDTO;
import com.coreerp.model.MaterialPriceList;
import com.coreerp.service.MaterialService;
import com.coreerp.service.PartyService;

@Component
public class MaterialPriceListMapper implements AbstractMapper<MaterialPriceListDTO, MaterialPriceList> {

//	@Autowired
//	ModelMapper modelMapper;
	
	@Autowired
	MaterialService materialService;
	
	@Autowired
	PartyService partyService;
	
	@Override
	public MaterialPriceListDTO modelToDTOMap(MaterialPriceList model) {
		
		MaterialPriceListDTO dto = new MaterialPriceListDTO();//modelMapper.map(model,  MaterialPriceListDTO.class);
		
		dto.setComment(model.getComments());
		dto.setDeleted(model.getDeleted());
		dto.setDiscountPercentage(model.getDiscountPercentage());
		dto.setId(model.getId());
		dto.setMaterialId(model.getMaterial().getId());
		dto.setPartyId(model.getParty().getId());
		dto.setSellingPrice(model.getSellingPrice());
		dto.setMaterialName(model.getMaterial().getName());
		dto.setMaterialTypeName(model.getMaterial().getMaterialType().getName());
		dto.setPartNumber(model.getMaterial().getPartNumber());
		dto.setCurrentBuyingPrice(model.getMaterial().getBuyingPrice());
		dto.setCurrentSellingPrice(model.getMaterial().getPrice());
		
		return dto;
		
	}

	@Override
	public MaterialPriceList dtoToModelMap(MaterialPriceListDTO dto) {
		MaterialPriceList model = new MaterialPriceList(); //modelMapper.map(dto, MaterialPriceList.class);
		
		model.setComments(dto.getComment());
		model.setDiscountPercentage(dto.getDiscountPercentage());
		model.setId(dto.getId());
		model.setMaterial(materialService.getMaterialById(dto.getMaterialId()));
		model.setParty(partyService.getPartyObjectById(dto.getPartyId()));
		model.setSellingPrice(dto.getSellingPrice());
		
		return model;
	}

	@Override
	public List<MaterialPriceListDTO> modelToDTOList(List<MaterialPriceList> modelList) {
		return modelList.stream()
				.map(model -> modelToDTOMap(model))
				.collect(Collectors.toList());
	}

	@Override
	public List<MaterialPriceList> dtoToModelList(List<MaterialPriceListDTO> dtoList) {
		return dtoList.stream()
				.map(dto -> dtoToModelMap(dto))
				.collect(Collectors.toList());
	}

}
