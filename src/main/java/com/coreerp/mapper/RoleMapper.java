package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.RoleDTO;
import com.coreerp.model.Role;

@Component
public class RoleMapper implements AbstractMapper<RoleDTO, Role> {

	@Autowired
	ModelMapper modelMapper;
	
	@Override
	public RoleDTO modelToDTOMap(Role model) {
		
		RoleDTO dto = modelMapper.map(model, RoleDTO.class);
		dto.setRoleTypeName(model.getRoleType().getName());
		return dto;
	}

	@Override
	public Role dtoToModelMap(RoleDTO dto) {
		return modelMapper.map(dto, Role.class);
	}

	@Override
	public List<RoleDTO> modelToDTOList(List<Role> modelList) {
		return modelList.stream()
				.map(model -> modelToDTOMap(model))
				.collect(Collectors.toList());
	}

	@Override
	public List<Role> dtoToModelList(List<RoleDTO> dtoList) {
		return dtoList.stream()
				.map(dto -> dtoToModelMap(dto))
				.collect(Collectors.toList());
	}

}
