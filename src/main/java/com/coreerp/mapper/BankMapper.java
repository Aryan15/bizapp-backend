package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.BankDTO;
import com.coreerp.model.Bank;

@Component
public class BankMapper implements AbstractMapper<BankDTO, Bank>{

	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public BankDTO modelToDTOMap(Bank model) {
		return modelMapper.map(model, BankDTO.class);
	}

	@Override
	public Bank dtoToModelMap(BankDTO dto) {
		return modelMapper.map(dto, Bank.class);
	}

	@Override
	public List<BankDTO> modelToDTOList(List<Bank> modelList) {
		return modelList.stream()
				.map(material -> modelToDTOMap(material))
				.collect(Collectors.toList());
	}

	@Override
	public List<Bank> dtoToModelList(List<BankDTO> dtoList) {
		return dtoList.stream()
				.map(materialDTO -> dtoToModelMap(materialDTO))
				.collect(Collectors.toList());
	}

}
