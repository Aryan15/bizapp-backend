package com.coreerp.mapper;

import java.applet.AppletContext;
import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dto.CurrencyDTO;
import com.coreerp.service.CurrencyService;
import com.coreerp.service.TransactionNumberService;
import com.coreerp.service.TransactionTypeService;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.controller.PartyController;
import com.coreerp.dto.PartyDTO;
import com.coreerp.model.Party;
import com.coreerp.service.StateService;

@Component
public class PartyMapper implements AbstractMapper<PartyDTO, Party> {
	
	final static Logger log = LogManager.getLogger(PartyMapper.class);

	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
	private StateService stateService;

	@Autowired
	private CurrencyService currencyService;



	@Autowired
	TransactionNumberService transactionNumberService;

	@Autowired
	TransactionTypeService transactionTypeService ;


	@Override
	public PartyDTO modelToDTOMap(Party model) {
		//log.info("party model before dto: "+ model);
		PartyDTO partyDTO = modelMapper.map(model, PartyDTO.class);
		//log.info("model.getState().getName(): "+model.getState().getName());
		partyDTO.setStateName(model.getState().getName());
		partyDTO.setAddressesListDTO(model.getAddresses ());
		if(model.getPartyCurrencyId()!=null)
		{
			CurrencyDTO currencyDTO=currencyService.getCurrencyById(model.getPartyCurrencyId());
			//log.info("dto before return: "+partyDTO);
			partyDTO.setPartyCurrencyName(currencyDTO.getCurrencyName());
		}

		log.info("name1"+model.getPartyCurrencyId());
		return partyDTO;
	}

	@Override
	public Party dtoToModelMap(PartyDTO dto) {
		//log.info("party dto before map: "+dto);
		String transactionType;
		Party party = modelMapper.map(dto, Party.class);
		party.setState(stateService.getStateById(dto.getStateId()));
		if(dto.getPartyTypeId()==ApplicationConstants.PARTY_TYPE_CUSTOMER_ID){
			transactionType= ApplicationConstants.CUSTOMER_CODE;
		}
		else{
			transactionType=ApplicationConstants.SUPPLIER_CODE;
		}
		party.setPartyId(dto.getPartyId()!=null ? dto.getPartyId():transactionNumberService.getTransactionNumber(transactionTypeService.getByName(transactionType)).getTransactionId());
		party.setPartyCode(dto.getPartyCode()!=null ? dto.getPartyCode():transactionNumberService.getTransactionNumber(transactionTypeService.getByName(transactionType)).getTransactionNumber());
		party.setPartyCurrencyId(dto.getPartyCurrencyId());
		if(dto.getPartyCurrencyId()!=null){
			CurrencyDTO currencyDTO = currencyService.getCurrencyById(dto.getPartyCurrencyId());
			party.setPartyCurrencyName((currencyDTO.getCurrencyName()));
		}


		//log.info("mapped party: "+party);
		return party;
	}

	@Override
	public List<PartyDTO> modelToDTOList(List<Party> modelList) {
		return modelList.stream()
				.map(material -> modelToDTOMap(material))
				.collect(Collectors.toList());
	}

	@Override
	public List<Party> dtoToModelList(List<PartyDTO> dtoList) {
		return dtoList.stream()
				.map(materialDTO -> dtoToModelMap(materialDTO))
				.collect(Collectors.toList());
	}

}
