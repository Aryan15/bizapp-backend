package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.PaymentMethodDTO;
import com.coreerp.model.PaymentMethod;

@Component
public class PaymentMethodMapper implements AbstractMapper<PaymentMethodDTO, PaymentMethod>{

	@Autowired
	ModelMapper modelMapper;
	
	
	@Override
	public PaymentMethodDTO modelToDTOMap(PaymentMethod model) {

		return modelMapper.map(model, PaymentMethodDTO.class);
	}
	
	@Override
	public PaymentMethod dtoToModelMap(PaymentMethodDTO dto) {
		return modelMapper.map(dto, PaymentMethod.class);
	}
	
	@Override
	public List<PaymentMethodDTO> modelToDTOList(List<PaymentMethod> modelList) {
		return modelList.stream()
				.map(item -> modelToDTOMap(item))
				.collect(Collectors.toList());
	}
	
	@Override
	public List<PaymentMethod> dtoToModelList(List<PaymentMethodDTO> dtoList) {
		return dtoList.stream()
				.map(item -> dtoToModelMap(item))
				.collect(Collectors.toList());
	}
}
