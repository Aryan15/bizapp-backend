package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.StockTraceDTO;
import com.coreerp.model.StockTrace;

@Component
public class StockTraceMapper implements AbstractMapper<StockTraceDTO, StockTrace> {

	
	@Autowired
	ModelMapper modelMapper;
	
	@Override
	public StockTraceDTO modelToDTOMap(StockTrace model) {
		StockTraceDTO dto = modelMapper.map(model, StockTraceDTO.class);
		return dto;
	}

	@Override
	public StockTrace dtoToModelMap(StockTraceDTO dto) {
		
		StockTrace model = modelMapper.map(dto,  StockTrace.class);
		return model;
	}

	@Override
	public List<StockTraceDTO> modelToDTOList(List<StockTrace> modelList) {
		return modelList.stream()
						.map(model -> modelToDTOMap(model))
						.collect(Collectors.toList());
	}

	@Override
	public List<StockTrace> dtoToModelList(List<StockTraceDTO> dtoList) {
		return dtoList.stream()
						.map(dto -> dtoToModelMap(dto))
						.collect(Collectors.toList());
	}

}
