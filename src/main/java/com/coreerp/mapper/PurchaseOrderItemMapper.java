package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dao.ProcessRepository;
import com.coreerp.dto.PurchaseOrderItemDTO;
import com.coreerp.model.PurchaseOrderItem;
import com.coreerp.service.MaterialService;
import com.coreerp.service.QuotationService;
import com.coreerp.service.TaxService;
import com.coreerp.service.UnitOfMeasurementService;

@Component
public class PurchaseOrderItemMapper implements AbstractMapper<PurchaseOrderItemDTO, PurchaseOrderItem>{

	@Autowired
	MaterialService materialService;
	
	@Autowired
	//QuotationHeaderService quotationHeaderService;
	QuotationService quotationService;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	UnitOfMeasurementService unitOfMeasurementService;
	
	@Autowired
	ProcessRepository processRepository;
	
	@Override
	public PurchaseOrderItemDTO modelToDTOMap(PurchaseOrderItem model) {
		PurchaseOrderItemDTO purchaseOrderItemDTO = new PurchaseOrderItemDTO();
		
		purchaseOrderItemDTO.setAmount(model.getAmount());
		purchaseOrderItemDTO.setAmountAfterDiscount(model.getAmountAfterDiscount());
		purchaseOrderItemDTO.setAmountAfterTax(model.getAmountAfterTax());
		purchaseOrderItemDTO.setCessAmount(model.getCessAmount());
		purchaseOrderItemDTO.setCessPercentage(model.getCessPercentage());
		purchaseOrderItemDTO.setCgstTaxAmount(model.getCgstTaxAmount());
		purchaseOrderItemDTO.setCgstTaxPercentage(model.getCgstTaxPercentage());
		purchaseOrderItemDTO.setDiscountAmount(model.getDiscountAmount());
		purchaseOrderItemDTO.setDiscountPercentage(model.getDiscountPercentage());
		purchaseOrderItemDTO.setHeaderId(model.getPurchaseOrderHeader()!= null ? model.getPurchaseOrderHeader().getId() : null);
		purchaseOrderItemDTO.setId(model.getId());
		purchaseOrderItemDTO.setIgstTaxAmount(model.getIgstTaxAmount());
		purchaseOrderItemDTO.setIgstTaxPercentage(model.getIgstTaxPercentage());
		purchaseOrderItemDTO.setMaterialId(model.getMaterial().getId());
		purchaseOrderItemDTO.setPrice(model.getPrice());
		purchaseOrderItemDTO.setQuantity(model.getQuantity());
		purchaseOrderItemDTO.setDcBalanceQuantity(model.getDcBalanceQuantity());
		purchaseOrderItemDTO.setQuotationHeaderId(model.getQuotationItem()!= null ? model.getQuotationItem().getQuotationHeader().getId() : null);
		purchaseOrderItemDTO.setQuotationItemId(model.getQuotationItem()!= null ? model.getQuotationItem().getId() : null);
		purchaseOrderItemDTO.setRemarks(model.getRemarks());
		purchaseOrderItemDTO.setSgstTaxAmount(model.getSgstTaxAmount());
		purchaseOrderItemDTO.setSgstTaxPercentage(model.getSgstTaxPercentage());
		purchaseOrderItemDTO.setSlNo(model.getSlNo());
		purchaseOrderItemDTO.setTaxId(model.getTax() != null ? model.getTax().getId() : null);
		purchaseOrderItemDTO.setTransportationAmount(model.getTransportationAmount());
		purchaseOrderItemDTO.setUnitOfMeasurementId(model.getUnitOfMeasurement() != null ? model.getUnitOfMeasurement().getId() : null);
		purchaseOrderItemDTO.setPartNumber(model.getMaterial().getPartNumber());
		purchaseOrderItemDTO.setHsnOrSac(model.getMaterial().getHsnCode());
		purchaseOrderItemDTO.setPartName(model.getMaterial().getName());
		purchaseOrderItemDTO.setUom(model.getUnitOfMeasurement() != null ? model.getUnitOfMeasurement().getName() : null);
		purchaseOrderItemDTO.setInclusiveTax(model.getInclusiveTax());
		purchaseOrderItemDTO.setDcBalanceQuantity(model.getDcBalanceQuantity());
		purchaseOrderItemDTO.setInvoiceBalanceQuantity(model.getInvoiceBalanceQuantity());
		purchaseOrderItemDTO.setProcessId(model.getProcessId());
		purchaseOrderItemDTO.setProcessName(model.getProcessName());
		purchaseOrderItemDTO.setSpecification(model.getSpecification());
		purchaseOrderItemDTO.setIsContainer(model.getMaterial().getIsContainer());

		return purchaseOrderItemDTO;
	}

	@Override
	public PurchaseOrderItem dtoToModelMap(PurchaseOrderItemDTO dto) {
		
		PurchaseOrderItem purchaseOrderItem = new PurchaseOrderItem();
		
		purchaseOrderItem.setId(dto.getHeaderId());
		purchaseOrderItem.setAmount(dto.getAmount());
		purchaseOrderItem.setAmountAfterDiscount(dto.getAmountAfterDiscount());
		purchaseOrderItem.setAmountAfterTax(dto.getAmountAfterTax());
		purchaseOrderItem.setCessAmount(dto.getCessAmount());
		purchaseOrderItem.setCessPercentage(dto.getCessPercentage());
		purchaseOrderItem.setCgstTaxAmount(dto.getCgstTaxAmount());
		purchaseOrderItem.setCgstTaxPercentage(dto.getCgstTaxPercentage());
		purchaseOrderItem.setDiscountAmount(dto.getDiscountAmount());
		purchaseOrderItem.setDiscountPercentage(dto.getDiscountPercentage());
		purchaseOrderItem.setId(dto.getId());
		purchaseOrderItem.setIgstTaxAmount(dto.getIgstTaxAmount());
		purchaseOrderItem.setIgstTaxPercentage(dto.getIgstTaxPercentage());
		purchaseOrderItem.setMaterial(materialService.getMaterialById(dto.getMaterialId()));
		purchaseOrderItem.setPrice(dto.getPrice());
		purchaseOrderItem.setQuantity(dto.getQuantity());
		purchaseOrderItem.setDcBalanceQuantity(dto.getDcBalanceQuantity() != null ? dto.getDcBalanceQuantity() : dto.getQuantity());
		//purchaseOrderItem.setQuotationHeader(dto.getQuotationHeaderId() != null ? quotationService.getModelById(dto.getQuotationHeaderId()) : null);
		purchaseOrderItem.setQuotationItem(dto.getQuotationItemId() != null ? quotationService.getItemModelById(dto.getQuotationItemId()) : null);
		purchaseOrderItem.setRemarks(dto.getRemarks());
		purchaseOrderItem.setSgstTaxAmount(dto.getSgstTaxAmount());
		purchaseOrderItem.setSgstTaxPercentage(dto.getSgstTaxPercentage());
		purchaseOrderItem.setSlNo(dto.getSlNo());
		purchaseOrderItem.setTax(dto.getTaxId() != null ? taxService.getById(dto.getTaxId()): null);
		purchaseOrderItem.setTransportationAmount(dto.getTransportationAmount());
		purchaseOrderItem.setUnitOfMeasurement(dto.getUnitOfMeasurementId() != null ? unitOfMeasurementService.getById(dto.getUnitOfMeasurementId()) : null);
		purchaseOrderItem.setInclusiveTax(dto.getInclusiveTax());
		purchaseOrderItem.setInvoiceBalanceQuantity(dto.getInvoiceBalanceQuantity() != null ? dto.getInvoiceBalanceQuantity() : dto.getQuantity());
		
		purchaseOrderItem.setProcessId(dto.getProcessId() );
		purchaseOrderItem.setProcessName(dto.getProcessName() );
		purchaseOrderItem.setSpecification(dto.getSpecification());
		
		return purchaseOrderItem;
	}

	@Override
	public List<PurchaseOrderItemDTO> modelToDTOList(List<PurchaseOrderItem> modelList) {

		return modelList.stream()
				.map(purchaseOrderItem -> modelToDTOMap(purchaseOrderItem))
				.collect(Collectors.toList());
	}

	@Override
	public List<PurchaseOrderItem> dtoToModelList(List<PurchaseOrderItemDTO> dtoList) {
		
//		List<PurchaseOrderItem> purchaseOrderItemListOut = new ArrayList<PurchaseOrderItem>();
//		
//		for(PurchaseOrderItemDTO purchaseOrderItemDTO : dtoList){
//			PurchaseOrderItem purchaseOrderItem = new PurchaseOrderItem();
//			purchaseOrderItem = dtoToModelMap(purchaseOrderItemDTO);
//			purchaseOrderItemListOut.add(purchaseOrderItem);
//		}
//		
//		return purchaseOrderItemListOut;
		
		return dtoList.stream()
				.map(poItemDTO -> dtoToModelMap(poItemDTO))
				.collect(Collectors.toList());
	}

}
