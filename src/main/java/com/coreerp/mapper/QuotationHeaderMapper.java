package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.controller.QuotationController;
import com.coreerp.dto.QuotationDTO;
import com.coreerp.model.QuotationHeader;
import com.coreerp.service.CompanyService;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.PartyService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TaxService;
import com.coreerp.service.TransactionNumberService;
import com.coreerp.service.TransactionTypeService;
import com.coreerp.service.UnitOfMeasurementService;

@Component
public class QuotationHeaderMapper implements AbstractMapper<QuotationDTO, QuotationHeader> {

	final static Logger log = LogManager.getLogger(QuotationHeaderMapper.class);
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	FinancialYearService financialYearService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	QuotationItemMapper quotationItemMapper;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	UnitOfMeasurementService unitOfMeasurementService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	TransactionNumberService transactionNumberService;
	
	@Autowired
	StatusService statusService;
	
	@Override
	public QuotationDTO modelToDTOMap(QuotationHeader model) {
		
		QuotationDTO quotationDTOOut = new QuotationDTO();
		log.info("---------------------2");
		quotationDTOOut.setAmount(model.getAmount());
		quotationDTOOut.setCgstTaxAmount(model.getCgstTaxAmount());
		quotationDTOOut.setCgstTaxRate(model.getCgstTaxRate());
		quotationDTOOut.setCompanyId(model.getCompany().getId());
		quotationDTOOut.setDeliveryTerms(model.getDeliveryTerms());
		quotationDTOOut.setTotalDiscount(model.getDiscountAmount());
		quotationDTOOut.setDiscountPercent(model.getDiscountPercent());
		quotationDTOOut.setEnquiryDate(model.getEnquiryDate());
		quotationDTOOut.setFinancialYearId(model.getFinancialYear().getId());
		quotationDTOOut.setGrandTotal(model.getGrandTotal());
		quotationDTOOut.setId(model.getId());
		quotationDTOOut.setIgstTaxAmount(model.getIgstTaxAmount());
		quotationDTOOut.setIgstTaxRate(model.getIgstTaxRate());
		quotationDTOOut.setIsPaymentChecked(model.getIsPaymentChecked());
		quotationDTOOut.setKindAttention(model.getKindAttention());
		quotationDTOOut.setTotalTaxableAmount(model.getNetAmount());
		quotationDTOOut.setPartyId(model.getParty().getId());
		quotationDTOOut.setEnquiryNumber(model.getEnquiryNumber());
		quotationDTOOut.setPaymentTerms(model.getPaymentTerms());
		quotationDTOOut.setQuotationDate(model.getQuotationDate());
		quotationDTOOut.setQuotationNumber(model.getQuotationNumber());
		quotationDTOOut.setQuotationId(model.getQuotationId());
		//quotationDTOOut.setQuotationStatus(model.getStatus());
		quotationDTOOut.setQuotationSubject(model.getQuotationSubject());
		quotationDTOOut.setRoundOffAmount(model.getRoundOffAmount());
		quotationDTOOut.setSgstTaxAmount(model.getSgstTaxAmount());
		quotationDTOOut.setSgstTaxRate(model.getSgstTaxRate());
		quotationDTOOut.setStatusId(model.getStatus().getId());
		quotationDTOOut.setSubTotalAmount(model.getSubTotalAmount());

			quotationDTOOut.setTaxAmount(model.getTaxAmount());
			quotationDTOOut.setTaxId(model.getTax().getId());

		quotationDTOOut.setTermsAndConditions(model.getTermsAndConditions());
		quotationDTOOut.setQuotationTypeId(model.getQuotationType().getId());
		quotationDTOOut.setQuotationItems(quotationItemMapper.modelToDTOList(model.getQuotationItems()));
		quotationDTOOut.setStatusName(statusService.getStatusById(model.getStatus().getId()).getName());
		
		quotationDTOOut.setPartyName(model.getParty().getName());
		quotationDTOOut.setAddress(model.getAddress());
		quotationDTOOut.setGstNumber(model.getGstNumber());
		quotationDTOOut.setShipToAddress(model.getParty().getBillAddress());
		quotationDTOOut.setRemarks(model.getRemarks());
		quotationDTOOut.setInclusiveTax(model.getInclusiveTax());
		
		
		quotationDTOOut.setCompanyName(model.getCompanyName());
		quotationDTOOut.setCompanygGstRegistrationTypeId(model.getCompanygGstRegistrationTypeId());
		quotationDTOOut.setCompanyPinCode(model.getCompanyPinCode());
		quotationDTOOut.setCompanyStateId(model.getCompanyStateId());
		quotationDTOOut.setCompanyStateName(model.getCompanyStateName());
		quotationDTOOut.setCompanyCountryId(model.getCompanyCountryId());
		quotationDTOOut.setCompanyPrimaryMobile(model.getCompanyPrimaryMobile());
		quotationDTOOut.setCompanySecondaryMobile(model.getCompanySecondaryMobile());
		quotationDTOOut.setCompanyContactPersonNumber(model.getCompanyContactPersonNumber());
		quotationDTOOut.setCompanyContactPersonName(model.getCompanyContactPersonName());
		quotationDTOOut.setCompanyPrimaryTelephone(model.getCompanyPrimaryTelephone());
		quotationDTOOut.setCompanySecondaryTelephone(model.getCompanySecondaryTelephone());
		quotationDTOOut.setCompanyWebsite(model.getCompanyWebsite());
		quotationDTOOut.setCompanyEmail(model.getCompanyEmail());
		quotationDTOOut.setCompanyFaxNumber(model.getCompanyFaxNumber());
		quotationDTOOut.setCompanyAddress(model.getCompanyAddress());
		quotationDTOOut.setCompanyTagLine(model.getCompanyTagLine());
		quotationDTOOut.setCompanyGstNumber(model.getCompanyGstNumber());
		quotationDTOOut.setCompanyPanNumber(model.getCompanyPanNumber());
		quotationDTOOut.setCompanyPanDate(model.getCompanyPanDate());
		quotationDTOOut.setCompanyCeritificateImagePath(model.getCompanyCeritificateImagePath());
		quotationDTOOut.setCompanyLogoPath(model.getCompanyLogoPath());
		
		quotationDTOOut.setPartyName(model.getPartyName());
		quotationDTOOut.setPartyContactPersonNumber(model.getPartyContactPersonNumber());
		quotationDTOOut.setPartyPinCode(model.getPartyPinCode());
		quotationDTOOut.setPartyAreaId(model.getPartyAreaId());
		quotationDTOOut.setPartyCityId(model.getPartyCityId());
		quotationDTOOut.setPartyStateId(model.getPartyStateId());
		quotationDTOOut.setPartyCountryId(model.getPartyCountryId());
		quotationDTOOut.setPartyPrimaryTelephone(model.getPartyPrimaryTelephone());
		quotationDTOOut.setPartySecondaryTelephone(model.getPartySecondaryTelephone());
		quotationDTOOut.setPartyPrimaryMobile(model.getPartyPrimaryMobile());
		quotationDTOOut.setPartySecondaryMobile(model.getPartySecondaryMobile());
		quotationDTOOut.setPartyEmail(model.getPartyEmail());
		quotationDTOOut.setPartyWebsite(model.getPartyWebsite());
		quotationDTOOut.setPartyContactPersonName(model.getPartyContactPersonName());
//		purchaseOrderDTO.setPartyBillToAddress(model.getPartyBillToAddress());
//		purchaseOrderDTO.setPartyShipAddress(model.getPartyBillToAddress());
		quotationDTOOut.setPartyDueDaysLimit(model.getPartyDueDaysLimit());
		quotationDTOOut.setPartyGstRegistrationTypeId(model.getPartyGstRegistrationTypeId());
//		purchaseOrderDTO.setPartyGstNumber(model.getPartyGstNumber());
		quotationDTOOut.setPartyPanNumber(model.getPartyPanNumber());
		quotationDTOOut.setIsIgst(model.getIsIgst());
		quotationDTOOut.setPartyCode(model.getParty().getPartyCode());
		quotationDTOOut.setCreatedBy(model.getCreatedBy());
		quotationDTOOut.setUpdateBy(model.getUpdatedBy());
		quotationDTOOut.setCreatedDate(model.getCreatedDateTime());
		quotationDTOOut.setUpdatedDate(model.getUpdatedDateTime());
		quotationDTOOut.setPartyCurrencyId(model.getPartyCurrencyId());
		quotationDTOOut.setCurrencyName((model.getCurrencyName()));

		log.info("name: "+model.getCurrencyName());


		return quotationDTOOut;
	}

	@Override
	public QuotationHeader dtoToModelMap(QuotationDTO dto) {
		log.info("---------------------1");
		QuotationHeader quotationHeaderOut = new QuotationHeader();
		
		quotationHeaderOut.setAmount(dto.getAmount());
		quotationHeaderOut.setCgstTaxAmount(dto.getCgstTaxAmount());
		quotationHeaderOut.setCgstTaxRate(dto.getCgstTaxRate());
		quotationHeaderOut.setCompany(companyService.getModelById(dto.getCompanyId()));
		quotationHeaderOut.setDeliveryTerms(dto.getDeliveryTerms());
		quotationHeaderOut.setDiscountAmount(dto.getTotalDiscount());
		quotationHeaderOut.setDiscountPercent(dto.getDiscountPercent());
		quotationHeaderOut.setEnquiryDate(dto.getEnquiryDate());
		quotationHeaderOut.setEnquiryNumber(dto.getEnquiryNumber());
		quotationHeaderOut.setFinancialYear(financialYearService.getById(dto.getFinancialYearId()));
		quotationHeaderOut.setGrandTotal(dto.getGrandTotal());
		quotationHeaderOut.setId(dto.getId());
		quotationHeaderOut.setIgstTaxAmount(dto.getIgstTaxAmount());
		quotationHeaderOut.setIgstTaxRate(dto.getIgstTaxRate());
		quotationHeaderOut.setIsPaymentChecked(dto.getIsPaymentChecked());
		quotationHeaderOut.setKindAttention(dto.getKindAttention());
		quotationHeaderOut.setNetAmount(dto.getTotalTaxableAmount());
		quotationHeaderOut.setParty(partyService.getPartyObjectById(dto.getPartyId()));
		quotationHeaderOut.setPaymentTerms(dto.getPaymentTerms());
		quotationHeaderOut.setQuotationDate(dto.getQuotationDate());
		quotationHeaderOut.setQuotationId(dto.getQuotationId() != null ? dto.getQuotationId() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getQuotationTypeId())).getTransactionId());
		quotationHeaderOut.setQuotationItems(quotationItemMapper.dtoToModelList(dto.getQuotationItems()));
		quotationHeaderOut.setQuotationNumber(dto.getQuotationNumber() != null ? dto.getQuotationNumber() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getQuotationTypeId())).getTransactionNumber());
		//quotationHeaderOut.setStatus(dto.getQuotationStatus());
		quotationHeaderOut.setQuotationSubject(dto.getQuotationSubject());
		quotationHeaderOut.setQuotationType(transactionTypeService.getModelById(dto.getQuotationTypeId()));
		quotationHeaderOut.setRoundOffAmount(dto.getRoundOffAmount());
		quotationHeaderOut.setSgstTaxAmount(dto.getSgstTaxAmount());
		quotationHeaderOut.setSgstTaxRate(dto.getSgstTaxRate());
		quotationHeaderOut.setStatus(dto.getStatusId() != null ? statusService.getStatusById(dto.getStatusId()) : null);
		quotationHeaderOut.setSubTotalAmount(dto.getSubTotalAmount());

		quotationHeaderOut.setTax(dto.getTaxId()==null?taxService.getById(0l):taxService.getById(dto.getTaxId()));

		//quotationHeaderOut.setTax(dto.getTaxId() != null ? taxService.getById(dto.getTaxId()): null);

		quotationHeaderOut.setTaxAmount(dto.getTaxAmount());
		quotationHeaderOut.setTermsAndConditions(dto.getTermsAndConditions());
		quotationHeaderOut.setRemarks(dto.getRemarks());
		quotationHeaderOut.setInclusiveTax(dto.getInclusiveTax());
		quotationHeaderOut.setAddress(dto.getAddress());
		quotationHeaderOut.setGstNumber(dto.getGstNumber());
		
		quotationHeaderOut.setCompanyName(dto.getCompanyName());
		quotationHeaderOut.setCompanygGstRegistrationTypeId(dto.getCompanygGstRegistrationTypeId());
		quotationHeaderOut.setCompanyPinCode(dto.getCompanyPinCode());
		quotationHeaderOut.setCompanyStateId(dto.getCompanyStateId());
		quotationHeaderOut.setCompanyStateName(dto.getCompanyStateName());
		quotationHeaderOut.setCompanyCountryId(dto.getCompanyCountryId());
		quotationHeaderOut.setCompanyPrimaryMobile(dto.getCompanyPrimaryMobile());
		quotationHeaderOut.setCompanySecondaryMobile(dto.getCompanySecondaryMobile());
		quotationHeaderOut.setCompanyContactPersonNumber(dto.getCompanyContactPersonNumber());
		quotationHeaderOut.setCompanyContactPersonName(dto.getCompanyContactPersonName());
		quotationHeaderOut.setCompanyPrimaryTelephone(dto.getCompanyPrimaryTelephone());
		quotationHeaderOut.setCompanySecondaryTelephone(dto.getCompanySecondaryTelephone());
		quotationHeaderOut.setCompanyWebsite(dto.getCompanyWebsite());
		quotationHeaderOut.setCompanyEmail(dto.getCompanyEmail());
		quotationHeaderOut.setCompanyFaxNumber(dto.getCompanyFaxNumber());
		quotationHeaderOut.setCompanyAddress(dto.getCompanyAddress());
		quotationHeaderOut.setCompanyTagLine(dto.getCompanyTagLine());
		quotationHeaderOut.setCompanyGstNumber(dto.getCompanyGstNumber());
		quotationHeaderOut.setCompanyPanNumber(dto.getCompanyPanNumber());
		quotationHeaderOut.setCompanyPanDate(dto.getCompanyPanDate());
		quotationHeaderOut.setCompanyCeritificateImagePath(dto.getCompanyCeritificateImagePath());
		quotationHeaderOut.setCompanyLogoPath(dto.getCompanyLogoPath());
		
		quotationHeaderOut.setPartyName(dto.getPartyName());
		quotationHeaderOut.setPartyContactPersonNumber(dto.getPartyContactPersonNumber());
		quotationHeaderOut.setPartyPinCode(dto.getPartyPinCode());
		quotationHeaderOut.setPartyAreaId(dto.getPartyAreaId());
		quotationHeaderOut.setPartyCityId(dto.getPartyCityId());
		quotationHeaderOut.setPartyStateId(dto.getPartyStateId());
		quotationHeaderOut.setPartyCountryId(dto.getPartyCountryId());
		quotationHeaderOut.setPartyPrimaryTelephone(dto.getPartyPrimaryTelephone());
		quotationHeaderOut.setPartySecondaryTelephone(dto.getPartySecondaryTelephone());
		quotationHeaderOut.setPartyPrimaryMobile(dto.getPartyPrimaryMobile());
		quotationHeaderOut.setPartySecondaryMobile(dto.getPartySecondaryMobile());
		quotationHeaderOut.setPartyEmail(dto.getPartyEmail());
		quotationHeaderOut.setPartyWebsite(dto.getPartyWebsite());
		quotationHeaderOut.setPartyContactPersonName(dto.getPartyContactPersonName());
//		purchaseOrderHeader.setPartyBillToAddress(dto.getPartyBillToAddress());
//		purchaseOrderHeader.setPartyShipAddress(dto.getPartyBillToAddress());
		quotationHeaderOut.setPartyDueDaysLimit(dto.getPartyDueDaysLimit());
		quotationHeaderOut.setPartyGstRegistrationTypeId(dto.getPartyGstRegistrationTypeId());
//		purchaseOrderHeader.setPartyGstNumber(dto.getPartyGstNumber());
		quotationHeaderOut.setPartyPanNumber(dto.getPartyPanNumber());
		quotationHeaderOut.setIsIgst(dto.getIsIgst());
		quotationHeaderOut.setCreatedBy(dto.getCreatedBy());
		quotationHeaderOut.setCreatedDateTime(dto.getCreatedDate());
		quotationHeaderOut.setPartyCurrencyId(dto.getPartyCurrencyId());
		quotationHeaderOut.setCurrencyName(dto.getCurrencyName());
		log.info("name: "+dto.getCurrencyName());

		log.info("mapped quotationHeaderOut: "+quotationHeaderOut);
		
		return quotationHeaderOut;
	}

	@Override
	public List<QuotationDTO> modelToDTOList(List<QuotationHeader> modelList) {
//		List<QuotationDTO> quotationDTOListOut = new ArrayList<QuotationDTO>();
//		
//		for(QuotationHeader quotationHeader : modelList){
//			QuotationDTO quotationDTO = modelToDTOMap(quotationHeader);
//			quotationDTOListOut.add(quotationDTO);
//		}
//		
//		return quotationDTOListOut;
		
		
		return modelList.stream()
					.map(qHeader -> modelToDTOMap(qHeader))
					.collect(Collectors.toList());
	}

	@Override
	public List<QuotationHeader> dtoToModelList(List<QuotationDTO> dtoList) {
		
//		List<QuotationHeader> quotationHeaderListOut = new ArrayList<QuotationHeader>();
//		
//		for(QuotationDTO quotationDTO : dtoList){
//			QuotationHeader quotationHeader = dtoToModelMap(quotationDTO);
//			quotationHeaderListOut.add(quotationHeader);
//		}
//		return quotationHeaderListOut;
		
		return dtoList.stream()
				.map(qDTO -> dtoToModelMap(qDTO))
				.collect(Collectors.toList());
	}

}
