package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.DeliveryChallanDTO;
import com.coreerp.model.DeliveryChallanHeader;
import com.coreerp.service.CompanyService;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.PartyService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TaxService;
import com.coreerp.service.TransactionNumberService;
import com.coreerp.service.TransactionTypeService;

@Component
public class DeliveryChallanHeaderMapper implements AbstractMapper<DeliveryChallanDTO, DeliveryChallanHeader>{

	final static Logger log = LogManager.getLogger(DeliveryChallanHeaderMapper.class);
	
	@Autowired
	PurchaseOrderHeaderMapper purchaseOrderHeaderMapper;
	
	@Autowired
	DeliveryChallanItemMapper deliveryChallanItemMapper;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	FinancialYearService financialYearService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	TransactionTypeService transactionTypeService;

	@Autowired
	TransactionNumberService transactionNumberService;
	
	@Override
	public DeliveryChallanDTO modelToDTOMap(DeliveryChallanHeader model) {
		DeliveryChallanDTO deliveryChallanDTOOut = new DeliveryChallanDTO ();
		deliveryChallanDTOOut.setAmount(model.getAmount());
		deliveryChallanDTOOut.setBalanceQuantity(model.getBalanceQuantity());
		deliveryChallanDTOOut.setCgstTaxAmount(model.getCgstTaxAmount());
		deliveryChallanDTOOut.setCgstTaxRate(model.getCgstTaxRate());
		deliveryChallanDTOOut.setCompanyId(model.getCompany().getId());
		deliveryChallanDTOOut.setDeliveryAddress(model.getDeliveryAddress());
		deliveryChallanDTOOut.setDeliveryChallanDate(model.getDeliveryChallanDate());
		deliveryChallanDTOOut.setDeliveryChallanId(model.getDeliveryChallanId());
		deliveryChallanDTOOut.setDeliveryChallanItems(deliveryChallanItemMapper.modelToDTOList(model.getDeliveryChallanItems()));
		deliveryChallanDTOOut.setDeliveryChallanNumber(model.getDeliveryChallanNumber());
		deliveryChallanDTOOut.seteWayBill(model.geteWayBill());
		deliveryChallanDTOOut.setDeliveryChallanTypeId(model.getDeliveryChallanType().getId());
		deliveryChallanDTOOut.setDeliveryNote(model.getDeliveryNote());
		deliveryChallanDTOOut.setDeliveryTerms(model.getDeliveryTerms());
		deliveryChallanDTOOut.setDispatchDate(model.getDispatchDate());
		deliveryChallanDTOOut.setDispatchTime(model.getDispatchTime());
		deliveryChallanDTOOut.setFinancialYearId(model.getFinancialYear().getId());
		deliveryChallanDTOOut.setForSale(model.getForSale());
		deliveryChallanDTOOut.setId(model.getId());
		deliveryChallanDTOOut.setIgstTaxAmount(model.getIgstTaxAmount());
		deliveryChallanDTOOut.setIgstTaxRate(model.getIgstTaxRate());
		deliveryChallanDTOOut.setInDeliveryChallanDate(model.getInDeliveryChallanDate());
		deliveryChallanDTOOut.setInDeliveryChallanNumber(model.getInDeliveryChallanNumber());
		deliveryChallanDTOOut.setInspectedBy(model.getInspectedBy());
		deliveryChallanDTOOut.setInspectionDate(model.getInspectionDate());
		deliveryChallanDTOOut.setInternalReferenceDate(model.getInternalReferenceDate());
		deliveryChallanDTOOut.setInternalReferenceNumber(model.getInternalReferenceNumber());
		deliveryChallanDTOOut.setLabourChargesOnly(model.getLabourChargesOnly());
		deliveryChallanDTOOut.setModeOfDispatch(model.getModeOfDispatch());
		deliveryChallanDTOOut.setModeOfDispatchStatus(model.getModeOfDispatchStatus());
		deliveryChallanDTOOut.setNonReturnableDeliveryChallan(model.getNonReturnableDeliveryChallan());
		deliveryChallanDTOOut.setNotForSale(model.getNotForSale());
		deliveryChallanDTOOut.setNumberOfPackages(model.getNumberOfPackages());
		deliveryChallanDTOOut.setNumberOfPackagesStatus(model.getNumberOfPackagesStatus());
		deliveryChallanDTOOut.setOfficeAddress(model.getOfficeAddress());
		deliveryChallanDTOOut.setPartyId(model.getParty().getId());
		deliveryChallanDTOOut.setPaymentTerms(model.getPaymentTerms());
		deliveryChallanDTOOut.setPersonName(model.getPersonName());
		deliveryChallanDTOOut.setPurchaseOrderDate(model.getPurchaseOrderDate());
		deliveryChallanDTOOut.setPurchaseOrderNumber(model.getPurchaseOrderNumber());
		deliveryChallanDTOOut.setPrice(model.getPrice());
		deliveryChallanDTOOut.setReturnableDeliveryChallan(model.getReturnableDeliveryChallan());
		deliveryChallanDTOOut.setReturnForJobWork(model.getReturnForJobWork());
		deliveryChallanDTOOut.setSgstTaxAmount(model.getSgstTaxAmount());
		deliveryChallanDTOOut.setSgstTaxRate(model.getSgstTaxRate());
		deliveryChallanDTOOut.setStatusId(model.getStatus().getId());
		deliveryChallanDTOOut.setTaxAmount(model.getTaxAmount());
		deliveryChallanDTOOut.setTaxId(model.getTax()!= null ? model.getTax().getId() : null);
		deliveryChallanDTOOut.setTermsAndConditions(model.getTermsAndConditions());
		deliveryChallanDTOOut.setTotalQuantity(model.getTotalQuantity());
		deliveryChallanDTOOut.setAddress(model.getParty().getAddress());
		deliveryChallanDTOOut.setVehicleNumber(model.getVehicleNumber());
		deliveryChallanDTOOut.setVehicleNumberStatus(model.getVehicleNumberStatus());
		deliveryChallanDTOOut.setDiscountAmount(model.getDiscountAmount());
		deliveryChallanDTOOut.setDiscountPercentage(model.getDiscountPercentage());
		deliveryChallanDTOOut.setAmountAfterDiscount(model.getAmountAfterDiscount());
		deliveryChallanDTOOut.setStatusName(model.getStatus() != null ? model.getStatus().getName() : null);
		deliveryChallanDTOOut.setPartyName(model.getParty() != null ? model.getParty().getName() : null);
		deliveryChallanDTOOut.setGstNumber(model.getGstNumber());
		deliveryChallanDTOOut.setRemarks(model.getRemarks());
		deliveryChallanDTOOut.setInclusiveTax(model.getInclusiveTax());
		deliveryChallanDTOOut.setBillToAddress((model.getBillToAddress()));
		deliveryChallanDTOOut.setShipToAddress(model.getShipToAddress());
		
		deliveryChallanDTOOut.setCompanyName(model.getCompanyName());
		deliveryChallanDTOOut.setCompanygGstRegistrationTypeId(model.getCompanygGstRegistrationTypeId());
		deliveryChallanDTOOut.setCompanyPinCode(model.getCompanyPinCode());
		deliveryChallanDTOOut.setCompanyStateId(model.getCompanyStateId());
		deliveryChallanDTOOut.setCompanyStateName(model.getCompanyStateName());
		deliveryChallanDTOOut.setCompanyCountryId(model.getCompanyCountryId());
		deliveryChallanDTOOut.setCompanyPrimaryMobile(model.getCompanyPrimaryMobile());
		deliveryChallanDTOOut.setCompanySecondaryMobile(model.getCompanySecondaryMobile());
		deliveryChallanDTOOut.setCompanyContactPersonNumber(model.getCompanyContactPersonNumber());
		deliveryChallanDTOOut.setCompanyContactPersonName(model.getCompanyContactPersonName());
		deliveryChallanDTOOut.setCompanyPrimaryTelephone(model.getCompanyPrimaryTelephone());
		deliveryChallanDTOOut.setCompanySecondaryTelephone(model.getCompanySecondaryTelephone());
		deliveryChallanDTOOut.setCompanyWebsite(model.getCompanyWebsite());
		deliveryChallanDTOOut.setCompanyEmail(model.getCompanyEmail());
		deliveryChallanDTOOut.setCompanyFaxNumber(model.getCompanyFaxNumber());
		deliveryChallanDTOOut.setCompanyAddress(model.getCompanyAddress());
		deliveryChallanDTOOut.setCompanyTagLine(model.getCompanyTagLine());
		deliveryChallanDTOOut.setCompanyGstNumber(model.getCompanyGstNumber());
		deliveryChallanDTOOut.setCompanyPanNumber(model.getCompanyPanNumber());
		deliveryChallanDTOOut.setCompanyPanDate(model.getCompanyPanDate());
		deliveryChallanDTOOut.setCompanyCeritificateImagePath(model.getCompanyCeritificateImagePath());
		deliveryChallanDTOOut.setCompanyLogoPath(model.getCompanyLogoPath());
		
		deliveryChallanDTOOut.setPartyName(model.getPartyName());
		deliveryChallanDTOOut.setPartyContactPersonNumber(model.getPartyContactPersonNumber());
		deliveryChallanDTOOut.setPartyPinCode(model.getPartyPinCode());
		deliveryChallanDTOOut.setPartyAreaId(model.getPartyAreaId());
		deliveryChallanDTOOut.setPartyCityId(model.getPartyCityId());
		deliveryChallanDTOOut.setPartyStateId(model.getPartyStateId());
		deliveryChallanDTOOut.setPartyCountryId(model.getPartyCountryId());
		deliveryChallanDTOOut.setPartyPrimaryTelephone(model.getPartyPrimaryTelephone());
		deliveryChallanDTOOut.setPartySecondaryTelephone(model.getPartySecondaryTelephone());
		deliveryChallanDTOOut.setPartyPrimaryMobile(model.getPartyPrimaryMobile());
		deliveryChallanDTOOut.setPartySecondaryMobile(model.getPartySecondaryMobile());
		deliveryChallanDTOOut.setPartyEmail(model.getPartyEmail());
		deliveryChallanDTOOut.setPartyWebsite(model.getPartyWebsite());
		deliveryChallanDTOOut.setPartyContactPersonName(model.getPartyContactPersonName());
//		deliveryChallanDTOOut.setPartyBillToAddress(model.getPartyBillToAddress());
//		deliveryChallanDTOOut.setPartyShipAddress(model.getPartyBillToAddress());
		deliveryChallanDTOOut.setPartyDueDaysLimit(model.getPartyDueDaysLimit());
		deliveryChallanDTOOut.setPartyGstRegistrationTypeId(model.getPartyGstRegistrationTypeId());
		deliveryChallanDTOOut.setPartyPanNumber(model.getPartyPanNumber());
		deliveryChallanDTOOut.setIsIgst(model.getIsIgst());
		deliveryChallanDTOOut.setPartyCode(model.getParty().getPartyCode());
		deliveryChallanDTOOut.setCreatedBy(model.getCreatedBy());
		deliveryChallanDTOOut.setUpdateBy(model.getUpdatedBy());
		deliveryChallanDTOOut.setCreatedDate(model.getCreatedDateTime());
		deliveryChallanDTOOut.setUpdatedDate(model.getUpdatedDateTime());
		deliveryChallanDTOOut.setJwNoteId(model.getJwNoteId());
		return deliveryChallanDTOOut;
	}

	@Override
	public DeliveryChallanHeader dtoToModelMap(DeliveryChallanDTO dto) {
		DeliveryChallanHeader headerOut = new DeliveryChallanHeader();
		
		headerOut.setCgstTaxAmount(dto.getCgstTaxAmount());
		headerOut.setCgstTaxRate(dto.getCgstTaxRate());
		headerOut.setCompany(companyService.getModelById(dto.getCompanyId()));
		headerOut.setBalanceQuantity(dto.getBalanceQuantity()!=null ? dto.getBalanceQuantity() : dto.getTotalQuantity());
		headerOut.setDeliveryAddress(dto.getDeliveryAddress());
		headerOut.setDeliveryChallanDate(dto.getDeliveryChallanDate());
		headerOut.setDeliveryChallanId(dto.getDeliveryChallanNumber() != null ? dto.getDeliveryChallanId() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getDeliveryChallanTypeId())).getTransactionId());
		headerOut.setDeliveryChallanItems(deliveryChallanItemMapper.dtoToModelList(dto.getDeliveryChallanItems()));
		headerOut.setDeliveryChallanNumber(
				dto.getDeliveryChallanNumber() != null ? dto.getDeliveryChallanNumber() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getDeliveryChallanTypeId())).getTransactionNumber());
		headerOut.setDeliveryChallanType(transactionTypeService.getModelById(dto.getDeliveryChallanTypeId()));
		headerOut.setDeliveryNote(dto.getDeliveryNote());
		headerOut.setDeliveryTerms(dto.getDeliveryTerms());			
		headerOut.setDispatchDate(dto.getDispatchDate());
		headerOut.setDispatchTime(dto.getDispatchTime());
		headerOut.seteWayBill(dto.geteWayBill());
		headerOut.setFinancialYear(financialYearService.getById(dto.getFinancialYearId()));
		headerOut.setForSale(dto.getForSale());
		headerOut.setId(dto.getId());
		headerOut.setIgstTaxAmount(dto.getIgstTaxAmount());
		headerOut.setIgstTaxRate(dto.getIgstTaxRate());
		headerOut.setInDeliveryChallanDate(dto.getInDeliveryChallanDate());
		headerOut.setInDeliveryChallanNumber(dto.getInDeliveryChallanNumber());
		headerOut.setInspectedBy(dto.getInspectedBy());
		headerOut.setInspectionDate(dto.getInspectionDate());
		headerOut.setInternalReferenceDate(dto.getInternalReferenceDate());
		headerOut.setInternalReferenceNumber(dto.getInternalReferenceNumber());
		headerOut.setLabourChargesOnly(dto.getLabourChargesOnly());
		headerOut.setModeOfDispatch(dto.getModeOfDispatch());
		headerOut.setModeOfDispatchStatus(dto.getModeOfDispatchStatus());
		headerOut.setNonReturnableDeliveryChallan(dto.getNonReturnableDeliveryChallan());
		headerOut.setNumberOfPackages(dto.getNumberOfPackages());
		headerOut.setNumberOfPackagesStatus(dto.getNumberOfPackagesStatus());
		headerOut.setOfficeAddress(dto.getOfficeAddress());
		headerOut.setParty(partyService.getPartyObjectById(dto.getPartyId()));
		headerOut.setPaymentTerms(dto.getPaymentTerms());
		headerOut.setPersonName(dto.getPersonName());
		headerOut.setPurchaseOrderDate(dto.getPurchaseOrderDate());
		headerOut.setPurchaseOrderNumber(dto.getPurchaseOrderNumber());
		headerOut.setReturnableDeliveryChallan(dto.getReturnableDeliveryChallan());
		headerOut.setReturnForJobWork(dto.getReturnForJobWork());
		headerOut.setSgstTaxAmount(dto.getSgstTaxAmount());
		headerOut.setSgstTaxRate(dto.getSgstTaxRate());

		headerOut.setStatus(dto.getStatusId() != null ? statusService.getStatusById(dto.getStatusId()) : null);
		headerOut.setTax(dto.getTaxId() != null ? taxService.getById(dto.getTaxId()): null);
		headerOut.setTaxAmount(dto.getTaxAmount());
		headerOut.setTermsAndConditions(dto.getTermsAndConditions());
		headerOut.setTotalQuantity(dto.getTotalQuantity());
		headerOut.setVehicleNumber(dto.getVehicleNumber());
		headerOut.setVehicleNumberStatus(dto.getVehicleNumberStatus());
		headerOut.setPrice(dto.getPrice());

		headerOut.setAmount(dto.getAmount());
		headerOut.setDiscountAmount(dto.getDiscountAmount());
		headerOut.setDiscountPercentage(dto.getDiscountPercentage());
		headerOut.setAmountAfterDiscount(dto.getAmountAfterDiscount());
		headerOut.setRemarks(dto.getRemarks());
		headerOut.setInclusiveTax(dto.getInclusiveTax());
		headerOut.setBillToAddress(dto.getBillToAddress());
		headerOut.setShipToAddress(dto.getShipToAddress());
		headerOut.setGstNumber(dto.getGstNumber());
		
		headerOut.setCompanyName(dto.getCompanyName());
		headerOut.setCompanygGstRegistrationTypeId(dto.getCompanygGstRegistrationTypeId());
		headerOut.setCompanyPinCode(dto.getCompanyPinCode());
		headerOut.setCompanyStateId(dto.getCompanyStateId());
		headerOut.setCompanyStateName(dto.getCompanyStateName());
		headerOut.setCompanyCountryId(dto.getCompanyCountryId());
		headerOut.setCompanyPrimaryMobile(dto.getCompanyPrimaryMobile());
		headerOut.setCompanySecondaryMobile(dto.getCompanySecondaryMobile());
		headerOut.setCompanyContactPersonNumber(dto.getCompanyContactPersonNumber());
		headerOut.setCompanyContactPersonName(dto.getCompanyContactPersonName());
		headerOut.setCompanyPrimaryTelephone(dto.getCompanyPrimaryTelephone());
		headerOut.setCompanySecondaryTelephone(dto.getCompanySecondaryTelephone());
		headerOut.setCompanyWebsite(dto.getCompanyWebsite());
		headerOut.setCompanyEmail(dto.getCompanyEmail());
		headerOut.setCompanyFaxNumber(dto.getCompanyFaxNumber());
		headerOut.setCompanyAddress(dto.getCompanyAddress());
		headerOut.setCompanyTagLine(dto.getCompanyTagLine());
		headerOut.setCompanyGstNumber(dto.getCompanyGstNumber());
		headerOut.setCompanyPanNumber(dto.getCompanyPanNumber());
		headerOut.setCompanyPanDate(dto.getCompanyPanDate());
		headerOut.setCompanyCeritificateImagePath(dto.getCompanyCeritificateImagePath());
		headerOut.setCompanyLogoPath(dto.getCompanyLogoPath());
		
		headerOut.setPartyName(dto.getPartyName());
		headerOut.setPartyContactPersonNumber(dto.getPartyContactPersonNumber());
		headerOut.setPartyPinCode(dto.getPartyPinCode());
		headerOut.setPartyAreaId(dto.getPartyAreaId());
		headerOut.setPartyCityId(dto.getPartyCityId());
		headerOut.setPartyStateId(dto.getPartyStateId());
		headerOut.setPartyCountryId(dto.getPartyCountryId());
		headerOut.setPartyPrimaryTelephone(dto.getPartyPrimaryTelephone());
		headerOut.setPartySecondaryTelephone(dto.getPartySecondaryTelephone());
		headerOut.setPartyPrimaryMobile(dto.getPartyPrimaryMobile());
		headerOut.setPartySecondaryMobile(dto.getPartySecondaryMobile());
		headerOut.setPartyEmail(dto.getPartyEmail());
		headerOut.setPartyWebsite(dto.getPartyWebsite());
		headerOut.setPartyContactPersonName(dto.getPartyContactPersonName());
//		headerOut.setPartyBillToAddress(dto.getPartyBillToAddress());
//		headerOut.setPartyShipAddress(dto.getPartyBillToAddress());
		headerOut.setPartyDueDaysLimit(dto.getPartyDueDaysLimit());
		headerOut.setPartyGstRegistrationTypeId(dto.getPartyGstRegistrationTypeId());
//		headerOut.setPartyGstNumber(dto.getPartyGstNumber());
		headerOut.setPartyPanNumber(dto.getPartyPanNumber());
		headerOut.setIsIgst(dto.getIsIgst());
		headerOut.setCreatedBy(dto.getCreatedBy());
		headerOut.setCreatedDateTime(dto.getCreatedDate());
		headerOut.setJwNoteId(dto.getJwNoteId());
		return headerOut;
	}

	@Override
	public List<DeliveryChallanDTO> modelToDTOList(List<DeliveryChallanHeader> modelList) {
		return modelList.stream()
				.map(dcHeader -> modelToDTOMap(dcHeader))
				.collect(Collectors.toList());
	}

	@Override
	public List<DeliveryChallanHeader> dtoToModelList(List<DeliveryChallanDTO> dtoList) {
		return dtoList.stream()
				.map(dcDTO -> dtoToModelMap(dcDTO))
				.collect(Collectors.toList());
	}

}
