package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.PartyBankMapDTO;
import com.coreerp.model.PartyBankMap;
import com.coreerp.service.BankService;
import com.coreerp.service.CompanyService;
import com.coreerp.service.PartyService;

@Component
public class PartyBankMapper implements AbstractMapper<PartyBankMapDTO, PartyBankMap> {

	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
	private BankService bankService;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private PartyService partyService;
	
	@Override
	public PartyBankMapDTO modelToDTOMap(PartyBankMap model) {
		//PartyBankMapDTO partyBankMapDTO = modelMapper.map(model, PartyBankMapDTO.class);
		
		PartyBankMapDTO partyBankMapDTO = new PartyBankMapDTO();
		
		partyBankMapDTO.setAccountNumber(model.getAccountNumber());
		partyBankMapDTO.setBankId(model.getBank().getId());
		partyBankMapDTO.setBankname(model.getBank().getBankName());
		partyBankMapDTO.setBranch(model.getBranch());
		partyBankMapDTO.setCompanyId(model.getCompany() != null ? model.getCompany().getId() : null);
		partyBankMapDTO.setContactNumber(model.getContactNumber());
		partyBankMapDTO.setDeleted(model.getDeleted());
		partyBankMapDTO.setId(model.getId());
		partyBankMapDTO.setIfsc(model.getIfsc());
		partyBankMapDTO.setBankAdCode(model.getBankAdCode());
		partyBankMapDTO.setOpeningBalance(model.getOpeningBalance());
		partyBankMapDTO.setPartyId(model.getParty() != null ? model.getParty().getId() : null);
		
		return partyBankMapDTO;
	}

	@Override
	public PartyBankMap dtoToModelMap(PartyBankMapDTO dto) {
		PartyBankMap partyBankMap = new PartyBankMap();//modelMapper.map(dto, PartyBankMap.class);
		
		partyBankMap.setAccountNumber(dto.getAccountNumber());
		partyBankMap.setBank(bankService.getModelById(dto.getBankId()));
		partyBankMap.setBranch(dto.getBranch());
		partyBankMap.setCompany(dto.getCompanyId() != null ? companyService.getModelById(dto.getCompanyId()) : null);
		partyBankMap.setContactNumber(dto.getContactNumber());
		partyBankMap.setDeleted(dto.getDeleted());
		partyBankMap.setId(dto.getId());
		partyBankMap.setIfsc(dto.getIfsc());
		partyBankMap.setBankAdCode(dto.getBankAdCode());
		partyBankMap.setOpeningBalance(dto.getOpeningBalance());
		partyBankMap.setParty(dto.getPartyId() != null ? partyService.getPartyObjectById(dto.getPartyId()) : null);
		
		
		return partyBankMap;
	}

	@Override
	public List<PartyBankMapDTO> modelToDTOList(List<PartyBankMap> modelList) {
		return modelList.stream()
				.map(partyBankMap -> modelToDTOMap(partyBankMap))
				.collect(Collectors.toList());
	}

	@Override
	public List<PartyBankMap> dtoToModelList(List<PartyBankMapDTO> dtoList) {
		return dtoList.stream()
				.map(partyBankMapDTO -> dtoToModelMap(partyBankMapDTO))
				.collect(Collectors.toList());
	}

}
