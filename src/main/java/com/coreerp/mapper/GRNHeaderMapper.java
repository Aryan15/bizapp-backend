package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.GRNHeaderDTO;
import com.coreerp.model.GRNHeader;
import com.coreerp.service.CompanyService;
import com.coreerp.service.DeliveryChallanService;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.InvoiceService;
import com.coreerp.service.PartyService;
import com.coreerp.service.PurchaseOrderService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TaxService;
import com.coreerp.service.TermsAndConditionService;
import com.coreerp.service.TransactionNumberService;
import com.coreerp.service.TransactionTypeService;
import com.coreerp.service.UserService;

@Component
public class GRNHeaderMapper  implements AbstractMapper<GRNHeaderDTO,GRNHeader>{

	final static Logger log=LogManager.getLogger(GRNHeaderMapper.class);
	
	@Autowired
	GRNItemMapper grnItemMapper;
	
	@Autowired
	TransactionNumberService transactionNumberService;
	
	@Autowired
	PurchaseOrderService purchaseOrderService;
	
	@Autowired
	InvoiceService invoiceService;
    
	@Autowired
	StatusService statusService;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	FinancialYearService financialYearService;
	
	@Autowired
	DeliveryChallanService deliveryChallanService;
	
	@Autowired
	TermsAndConditionService termsAndConditionService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	TransactionTypeService transactionTypeService;
	
	@Autowired
	TaxService taxService;
	
	@Override
	public GRNHeaderDTO modelToDTOMap(GRNHeader model) {
     
		GRNHeaderDTO grnHeaderDTO = modelToDTOWithoutItemMap(model);
		grnHeaderDTO.setGrnItems(grnItemMapper.modelToDTOList(model.getGrnItems()));
		log.info("item size: "+grnHeaderDTO.getGrnItems().size());
		return grnHeaderDTO;
	}

	public GRNHeaderDTO modelToDTOWithoutItemMap(GRNHeader model) {
		log.info("model: "+model.getDeliveryChallanHeader());

		
		GRNHeaderDTO grnHeaderDTO=new GRNHeaderDTO();
		grnHeaderDTO.setId(model.getId());
		grnHeaderDTO.setGrnId(model.getGrnId());
		grnHeaderDTO.setGrnNumber(model.getGrnNumber());
//		grnHeaderDTO.setQualityCheckedByName(model.getQualityCheckedBy());
//		grnHeaderDTO.setStockCheckedByName(model.getStockCheckedBy());
		grnHeaderDTO.setGrnDate(model.getGrnDate());
		grnHeaderDTO.setGrnTypeId(model.getGrnType().getId());
		grnHeaderDTO.setPurchaseOrderHeaderId(model.getPurchaseOrderHeader() != null ? model.getPurchaseOrderHeader().getId() : null);
		grnHeaderDTO.setComments(model.getComments());
		grnHeaderDTO.setVerbal(model.getVerbal());
		grnHeaderDTO.setGrnStatusId(model.getGrnStatus().getId());
		grnHeaderDTO.setCompanyId(model.getCompany().getId());
		grnHeaderDTO.setQualityCheckedById(model.getQualityCheckedBy().getId());
		grnHeaderDTO.setQualityCheckedByName(model.getQualityCheckedBy().getName());
		grnHeaderDTO.setStockCheckedByName(model.getStockCheckedBy().getName());
		grnHeaderDTO.setStockCheckedById(model.getStockCheckedBy().getId());
		grnHeaderDTO.setFinancialYearId(model.getFinancialYear().getId());
		grnHeaderDTO.setSupplierId(model.getSupplier().getId());
		grnHeaderDTO.setSupplierName(model.getPartyName());
//		grnHeaderDTO.setSupplierName(model.getSupplier().getPartyType().getName());
//		grnHeaderDTO.setDeliveryChallanHeaderId(model.getDeliveryChallanHeader().getId());
		grnHeaderDTO.setDeliveryChallanHeaderId(model.getDeliveryChallanHeader() != null ? model.getDeliveryChallanHeader().getId() : null);
		grnHeaderDTO.setDeliveryChallanNumber(model.getDeliveryChallanHeader() != null ? model.getDeliveryChallanHeader().getDeliveryChallanNumber() : null);
		grnHeaderDTO.setDeliveryChallanDate(model.getDeliveryChallanHeader() != null ? model.getDeliveryChallanHeader().getDeliveryChallanDate() : null);
		grnHeaderDTO.setPurchaseOrderNumber(model.getDeliveryChallanHeader() != null ? model.getDeliveryChallanHeader().getPurchaseOrderNumber() : null);
		grnHeaderDTO.setPurchaseOrderDate(model.getDeliveryChallanHeader() != null ? model.getDeliveryChallanHeader().getPurchaseOrderDate() : null);
		grnHeaderDTO.setTermsAndConditionNote(model.getTermsAndConditionNote());
		grnHeaderDTO.setTermsAndConditionId(model.getTermsAndConditon() != null ? model.getTermsAndConditon().getId() : null);
		grnHeaderDTO.setInvoiceHeaderIds(model.getInvoiceHeaders() != null ? model.getInvoiceHeaders().stream().map(inv -> inv.getId()).collect(Collectors.toList()) : null);
		grnHeaderDTO.setTaxId(model.getTax() != null ? model.getTax().getId() : null);
		grnHeaderDTO.setStatusName(model.getGrnStatus() != null ? model.getGrnStatus().getName() : null);
		grnHeaderDTO.setSupplierAddress(model.getSupplierAddress());
		grnHeaderDTO.setSupplierGSTN(model.getGstNumber());
		grnHeaderDTO.setInclusiveTax(model.getDeliveryChallanHeader().getInclusiveTax());
		
		grnHeaderDTO.setCompanyName(model.getCompanyName());
		grnHeaderDTO.setCompanygGstRegistrationTypeId(model.getCompanygGstRegistrationTypeId());
		grnHeaderDTO.setCompanyPinCode(model.getCompanyPinCode());
		grnHeaderDTO.setCompanyStateId(model.getCompanyStateId());
		grnHeaderDTO.setCompanyStateName(model.getCompanyStateName());
		grnHeaderDTO.setCompanyCountryId(model.getCompanyCountryId());
		grnHeaderDTO.setCompanyPrimaryMobile(model.getCompanyPrimaryMobile());
		grnHeaderDTO.setCompanySecondaryMobile(model.getCompanySecondaryMobile());
		grnHeaderDTO.setCompanyContactPersonNumber(model.getCompanyContactPersonNumber());
		grnHeaderDTO.setCompanyContactPersonName(model.getCompanyContactPersonName());
		grnHeaderDTO.setCompanyPrimaryTelephone(model.getCompanyPrimaryTelephone());
		grnHeaderDTO.setCompanySecondaryTelephone(model.getCompanySecondaryTelephone());
		grnHeaderDTO.setCompanyWebsite(model.getCompanyWebsite());
		grnHeaderDTO.setCompanyEmail(model.getCompanyEmail());
		grnHeaderDTO.setCompanyFaxNumber(model.getCompanyFaxNumber());
		grnHeaderDTO.setCompanyAddress(model.getCompanyAddress());
		grnHeaderDTO.setCompanyTagLine(model.getCompanyTagLine());
		grnHeaderDTO.setCompanyGstNumber(model.getCompanyGstNumber());
		grnHeaderDTO.setCompanyPanNumber(model.getCompanyPanNumber());
		grnHeaderDTO.setCompanyPanDate(model.getCompanyPanDate());
		grnHeaderDTO.setCompanyCeritificateImagePath(model.getCompanyCeritificateImagePath());
		grnHeaderDTO.setCompanyLogoPath(model.getCompanyLogoPath());
		
		grnHeaderDTO.setPartyName(model.getPartyName());
		grnHeaderDTO.setPartyContactPersonNumber(model.getPartyContactPersonNumber());
		grnHeaderDTO.setPartyPinCode(model.getPartyPinCode());
		grnHeaderDTO.setPartyAreaId(model.getPartyAreaId());
		grnHeaderDTO.setPartyCityId(model.getPartyCityId());
		grnHeaderDTO.setPartyStateId(model.getPartyStateId());
		grnHeaderDTO.setPartyCountryId(model.getPartyCountryId());
		grnHeaderDTO.setPartyPrimaryTelephone(model.getPartyPrimaryTelephone());
		grnHeaderDTO.setPartySecondaryTelephone(model.getPartySecondaryTelephone());
		grnHeaderDTO.setPartyPrimaryMobile(model.getPartyPrimaryMobile());
		grnHeaderDTO.setPartySecondaryMobile(model.getPartySecondaryMobile());
		grnHeaderDTO.setPartyEmail(model.getPartyEmail());
		grnHeaderDTO.setPartyWebsite(model.getPartyWebsite());
		grnHeaderDTO.setPartyContactPersonName(model.getPartyContactPersonName());
//		grnHeaderDTO.setPartyBillToAddress(model.getPartyBillToAddress());
//		grnHeaderDTO.setPartyShipAddress(model.getPartyBillToAddress());
		grnHeaderDTO.setPartyDueDaysLimit(model.getPartyDueDaysLimit());
		grnHeaderDTO.setPartyGstRegistrationTypeId(model.getPartyGstRegistrationTypeId());
//		grnHeaderDTO.setPartyGstNumber(model.getPartyGstNumber());
		grnHeaderDTO.setPartyPanNumber(model.getPartyPanNumber());
		grnHeaderDTO.setIsIgst(model.getIsIgst());
		grnHeaderDTO.setPartyCode(model.getSupplier().getPartyCode());
		grnHeaderDTO.setCreatedBy(model.getCreatedBy());
		grnHeaderDTO.setUpdateBy(model.getUpdatedBy());
		grnHeaderDTO.setCreatedDate(model.getCreatedDateTime());
		grnHeaderDTO.setUpdatedDate(model.getUpdatedDateTime());
		
		return grnHeaderDTO;
	}
	
	@Override
	public GRNHeader dtoToModelMap(GRNHeaderDTO dto) {
		
		GRNHeader grnHeaderOut=new GRNHeader();
		
		//Not setting Id
		grnHeaderOut.setId(dto.getId());
		grnHeaderOut.setGrnId(dto.getGrnId() != null ? dto.getGrnId() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getGrnTypeId())).getTransactionId());
		grnHeaderOut.setGrnNumber(dto.getGrnNumber() != null ? dto.getGrnNumber() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getGrnTypeId())).getTransactionNumber());
		grnHeaderOut.setGrnDate(dto.getGrnDate());
		grnHeaderOut.setGrnType(transactionTypeService.getModelById(dto.getGrnTypeId()));
		grnHeaderOut.setPurchaseOrderHeader(dto.getPurchaseOrderHeaderId()!=null? purchaseOrderService.getModelById(dto.getPurchaseOrderHeaderId()):null);
		grnHeaderOut.setComments(dto.getComments());
		
		grnHeaderOut.setVerbal(dto.getVerbal());
		grnHeaderOut.setGrnStatus(dto.getGrnId()!=null ? statusService.getStatusById(dto.getGrnStatusId()):null);
		grnHeaderOut.setCompany(dto.getCompanyId()!=null ? companyService.getModelById(dto.getCompanyId()):null);
		grnHeaderOut.setQualityCheckedBy(dto.getQualityCheckedById() !=null ? userService.getUserById(dto.getQualityCheckedById()):null);
		grnHeaderOut.setStockCheckedBy(dto.getStockCheckedById() !=null ? userService.getUserById(dto.getStockCheckedById()):null);
		grnHeaderOut.setFinancialYear(dto.getFinancialYearId() !=null ? financialYearService.getById(dto.getFinancialYearId()) :null);
		grnHeaderOut.setSupplier(partyService.getPartyObjectById(dto.getSupplierId()));
		grnHeaderOut.setCompany(dto.getCompanyId()!=null ? companyService.getModelById(dto.getCompanyId()):null);
		//grnHeaderOut.setDcHeader(dto.getDcHeaderId() !=null ? deliveryChallanService.getModelById(dto.getDcHeaderId()) :null);
		grnHeaderOut.setDeliveryChallanHeader(dto.getDeliveryChallanHeaderId() != null ? deliveryChallanService.getModelById(dto.getDeliveryChallanHeaderId()) : null);
//		grnHeaderOut.setDeliveryChallanHeader(dto.getDeliveryChallanNumber() !=null ? deliveryChallanService.getModelById(dto.getDeliveryChallanNumber()) : null);
		grnHeaderOut.setTermsAndConditionNote(dto.getTermsAndConditionNote());
		grnHeaderOut.setTermsAndConditon(dto.getTermsAndConditionId() !=null ? termsAndConditionService.getTermsAndConditionById(dto.getTermsAndConditionId()):null);
//		grnHeaderOut.setsu(dto.getInvoiceHeaderIds());
		grnHeaderOut.setInvoiceHeaders(dto.getInvoiceHeaderIds() != null ? dto.getInvoiceHeaderIds().stream().map(invId -> invoiceService.getInvoiceHeaderByHeaderId(invId)).collect(Collectors.toList()):null);
		grnHeaderOut.setGrnItems(grnItemMapper.dtoToModelList(dto.getGrnItems()));
		grnHeaderOut.setTax(taxService.getById(dto.getTaxId()));
		grnHeaderOut.setSupplierAddress(dto.getSupplierAddress());
		
		grnHeaderOut.setGstNumber(dto.getSupplierGSTN());
		
		grnHeaderOut.setCompanyName(dto.getCompanyName());
		grnHeaderOut.setCompanygGstRegistrationTypeId(dto.getCompanygGstRegistrationTypeId());
		grnHeaderOut.setCompanyPinCode(dto.getCompanyPinCode());
		grnHeaderOut.setCompanyStateId(dto.getCompanyStateId());
		grnHeaderOut.setCompanyStateName(dto.getCompanyStateName());
		grnHeaderOut.setCompanyCountryId(dto.getCompanyCountryId());
		grnHeaderOut.setCompanyPrimaryMobile(dto.getCompanyPrimaryMobile());
		grnHeaderOut.setCompanySecondaryMobile(dto.getCompanySecondaryMobile());
		grnHeaderOut.setCompanyContactPersonNumber(dto.getCompanyContactPersonNumber());
		grnHeaderOut.setCompanyContactPersonName(dto.getCompanyContactPersonName());
		grnHeaderOut.setCompanyPrimaryTelephone(dto.getCompanyPrimaryTelephone());
		grnHeaderOut.setCompanySecondaryTelephone(dto.getCompanySecondaryTelephone());
		grnHeaderOut.setCompanyWebsite(dto.getCompanyWebsite());
		grnHeaderOut.setCompanyEmail(dto.getCompanyEmail());
		grnHeaderOut.setCompanyFaxNumber(dto.getCompanyFaxNumber());
		grnHeaderOut.setCompanyAddress(dto.getCompanyAddress());
		grnHeaderOut.setCompanyTagLine(dto.getCompanyTagLine());
		grnHeaderOut.setCompanyGstNumber(dto.getCompanyGstNumber());
		grnHeaderOut.setCompanyPanNumber(dto.getCompanyPanNumber());
		grnHeaderOut.setCompanyPanDate(dto.getCompanyPanDate());
		grnHeaderOut.setCompanyCeritificateImagePath(dto.getCompanyCeritificateImagePath());
		grnHeaderOut.setCompanyLogoPath(dto.getCompanyLogoPath());
		
		grnHeaderOut.setPartyName(dto.getSupplierName());
		grnHeaderOut.setPartyContactPersonNumber(dto.getPartyContactPersonNumber());
		grnHeaderOut.setPartyPinCode(dto.getPartyPinCode());
		grnHeaderOut.setPartyAreaId(dto.getPartyAreaId());
		grnHeaderOut.setPartyCityId(dto.getPartyCityId());
		grnHeaderOut.setPartyStateId(dto.getPartyStateId());
		grnHeaderOut.setPartyCountryId(dto.getPartyCountryId());
		grnHeaderOut.setPartyPrimaryTelephone(dto.getPartyPrimaryTelephone());
		grnHeaderOut.setPartySecondaryTelephone(dto.getPartySecondaryTelephone());
		grnHeaderOut.setPartyPrimaryMobile(dto.getPartyPrimaryMobile());
		grnHeaderOut.setPartySecondaryMobile(dto.getPartySecondaryMobile());
		grnHeaderOut.setPartyEmail(dto.getPartyEmail());
		grnHeaderOut.setPartyWebsite(dto.getPartyWebsite());
		grnHeaderOut.setPartyContactPersonName(dto.getPartyContactPersonName());
//		grnHeaderOut.setPartyBillToAddress(dto.getPartyBillToAddress());
//		grnHeaderOut.setPartyShipAddress(dto.getPartyBillToAddress());
		grnHeaderOut.setPartyDueDaysLimit(dto.getPartyDueDaysLimit());
		grnHeaderOut.setPartyGstRegistrationTypeId(dto.getPartyGstRegistrationTypeId());
//		grnHeaderOut.setPartyGstNumber(dto.getPartyGstNumber());
		grnHeaderOut.setPartyPanNumber(dto.getPartyPanNumber());
		grnHeaderOut.setIsIgst(dto.getIsIgst());
		grnHeaderOut.setCreatedBy(dto.getCreatedBy());
		grnHeaderOut.setCreatedDateTime(dto.getCreatedDate());

		return grnHeaderOut;
	}

	
	
	@Override
	public List<GRNHeaderDTO> modelToDTOList(List<GRNHeader> modelList) {
//      List<GRNHeaderDTO> grnHeaderDTOListOut=modelList.stream()
//    		  .map(
//    		   grnHeader -> modelToDTOMap(grnHeader))
//    		  .collect(Collectors.toList());
//      
//      log.info("grnHeaderDTOListOut: "+grnHeaderDTOListOut);
//      return grnHeaderDTOListOut;
		
		return modelList.stream()
				.map(grnHeader -> modelToDTOMap(grnHeader))
				.collect(Collectors.toList());
    		  
	}

	@Override
	public List<GRNHeader> dtoToModelList(List<GRNHeaderDTO> dtoList) {
	  
//		List<GRNHeader> grnHeaderListOut=dtoList.stream()
//				.map(
//			     grnDTO -> dtoToModelMap(grnDTO))
//				.collect(Collectors.toList());
//		return grnHeaderListOut;
		
		return dtoList.stream()
				.map(grnDTO -> dtoToModelMap(grnDTO))
				.collect(Collectors.toList());
	
	}

	public List<GRNHeaderDTO> modelToRecentDTOList(List<GRNHeader> modelList) {

		return modelList.stream()
				.map(grnHeader -> modelToDTOWithoutItemMap(grnHeader))
				.collect(Collectors.toList());
    		  
	}
	
}
