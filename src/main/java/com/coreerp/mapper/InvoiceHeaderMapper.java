package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.dao.StateRepository;
import com.coreerp.model.InvoiceHeaderAudit;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.InvoiceDTO;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.service.CompanyService;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.PartyService;
import com.coreerp.service.StatusService;
import com.coreerp.service.TaxService;
import com.coreerp.service.TransactionNumberService;
import com.coreerp.service.TransactionTypeService;
import com.coreerp.utils.TransactionDataException;

@Component
public class InvoiceHeaderMapper implements AbstractMapper<InvoiceDTO, InvoiceHeader> {

	final static Logger log = LogManager.getLogger(InvoiceHeaderMapper.class);
	
	
	@Autowired
	InvoiceItemMapper invoiceItemMapper;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	DeliveryChallanHeaderMapper deliveryChallanHeaderMapper; 
	
	@Autowired
	FinancialYearService financialYearService;
	
	@Autowired
	PartyService partyService;
	
	@Autowired
	PurchaseOrderHeaderMapper purchaseOrderMapper;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	TransactionNumberService transactionNumberService;
	
	@Autowired
	TransactionTypeService transactionTypeService ; 
	
	@Autowired
	StatusService statusService;
	
	@Autowired
    private ModelMapper modelMapper;

	@Autowired
	StateRepository stateRepository;
	
	@Override
	public InvoiceDTO modelToDTOMap(InvoiceHeader model) {
		InvoiceDTO invoiceDTO = modelToDTOWithoutItemMap(model);
		invoiceDTO.setInvoiceItems(invoiceItemMapper.modelToDTOList(model.getInvoiceItems()));
		return invoiceDTO;
	}

	public InvoiceDTO modelToDTOWithoutItemMap(InvoiceHeader model) {
		InvoiceDTO invoiceDTO = new InvoiceDTO();//modelMapper.map(model, InvoiceDTO.class);//new InvoiceDTO();
		
		//invoiceDTO.setAddress(model.getPartyAddress());
		invoiceDTO.setAdvanceAmount(model.getAdvanceAmount());
		invoiceDTO.setBillToAddress(model.getBillToAddress());
		invoiceDTO.setCessAmount(model.getCessAmount());
		invoiceDTO.setCessPercent(model.getCessPercent());
		invoiceDTO.setCgstTaxAmount(model.getCgstTaxAmount());
		invoiceDTO.setCgstTaxRate(model.getCgstTaxRate());
		invoiceDTO.setCompanyId(model.getCompany().getId());
		invoiceDTO.setDeliveryChallanDate(model.getDeliveryChallanDate());
		invoiceDTO.setDeliveryChallanNumber(model.getDeliveryChallanNumber());
		invoiceDTO.setGrnDate(model.getGrnDate());
		invoiceDTO.setGrnNumber(model.getGrnNumber());
		invoiceDTO.setProformaInvoiceDate(model.getProformaInvoiceDate());
		invoiceDTO.setProformaInvoiceNumber(model.getProformaInvoiceNumber());
		invoiceDTO.setSourceInvoiceNumber(model.getSourceInvoiceNumber());
		invoiceDTO.setSourceInvoiceDate(model.getSourceInvoiceDate());
		invoiceDTO.setDeliveryTerms(model.getDeliveryTerms());
		invoiceDTO.setDiscountAmount(model.getDiscountAmount());
		invoiceDTO.setDiscountPercent(model.getDiscountPercent());
		invoiceDTO.setDocumentThrough(model.getDocumentThrough());
		invoiceDTO.setDueAmount(model.getDueAmount());
		invoiceDTO.seteWayBillNumber(model.geteWayBillNumber());
		invoiceDTO.setFinancialYearId(model.getFinancialYear().getId());
		invoiceDTO.setGrandTotal(model.getGrandTotal());
		invoiceDTO.setGstNumber(model.getGstNumber());
		invoiceDTO.setId(model.getId());
		invoiceDTO.setIgstTaxAmount(model.getIgstTaxAmount());
		invoiceDTO.setIgstTaxRate(model.getIgstTaxRate());
		invoiceDTO.setInternalReferenceDate(model.getInternalReferenceDate());
		invoiceDTO.setInternalReferenceNumber(model.getInternalReferenceNumber());
		invoiceDTO.setInvId(model.getInvId());
		invoiceDTO.setInvoiceDate(model.getInvoiceDate());
		invoiceDTO.setInvoiceNumber(model.getInvoiceNumber());
		invoiceDTO.setInvoiceTypeId(model.getInvoiceType().getId());
		invoiceDTO.setIsReverseCharge(model.getIsReverseCharge());
		//invoiceDTO.setIsRoudedOff();
		invoiceDTO.setLabourChargesOnly(model.getLabourChargesOnly());
		invoiceDTO.setModeOfDispatch(model.getModeOfDispatch());
		invoiceDTO.setNetAmount(model.getNetAmount());
		invoiceDTO.setNumOfPackages(model.getNumOfPackages());
		invoiceDTO.setPartyId(model.getParty().getId());
		invoiceDTO.setPartyName(model.getParty().getName());
		invoiceDTO.setPaymentDueDate(model.getPaymentDueDate());
		invoiceDTO.setPaymentTerms(model.getPaymentTerms());
		invoiceDTO.setPurchaseOrderDate(model.getPurchaseOrderDate());
		invoiceDTO.setPurchaseOrderNumber(model.getPurchaseOrderNumber());
		invoiceDTO.setReferenceDate(model.getInternalReferenceDate());
		invoiceDTO.setRoundOffAmount(model.getRoundOffAmount());
		invoiceDTO.setSgstTaxAmount(model.getSgstTaxAmount());
		invoiceDTO.setSgstTaxRate(model.getSgstTaxRate());
		invoiceDTO.setShipToAddress(model.getShipToAddress());
		invoiceDTO.setStateId(model.getParty().getState().getId());
		invoiceDTO.setStateName(stateRepository.getOne(model.getPartyStateId()).getName());
		invoiceDTO.setStatusId(model.getStatus().getId());
		invoiceDTO.setSubTotalAmount(model.getSubTotal());
		invoiceDTO.setTaxAmount(model.getTaxAmount());
		invoiceDTO.setTaxId(model.getTax() != null ? model.getTax().getId(): null);
		invoiceDTO.setTermsAndConditions(model.getTermsAndConditions());
		invoiceDTO.setTimeOfInvoice(model.getTimeOfInvoice());
		invoiceDTO.setTotalDiscount(model.getDiscountAmount());
		invoiceDTO.setTotalTaxableAmount(model.getTotalTaxableAmount());
		invoiceDTO.setTransportationCharges(model.getTransportationCharges());
		invoiceDTO.setVehicleNumber(model.getVehicleNumber());
		invoiceDTO.setVendorCode(model.getVendorCode());
		invoiceDTO.setStatusName(model.getStatus().getName());
		invoiceDTO.setRemarks(model.getRemarks());
		invoiceDTO.setAddress(model.getParty().getAddress());
		invoiceDTO.setInclusiveTax(model.getInclusiveTax());
		invoiceDTO.setDeliveryChallanDateString(model.getDeliveryChallanDateString());
        //log.info("Items...");
        //log.info(model.getInvoiceItems());	
		//invoiceDTO.setInvoiceItems(invoiceItemMapper.modelToDTOList(model.getInvoiceItems()));
		//log.info("item size: "+invoiceDTO.getInvoiceItems().size());
		
		invoiceDTO.setCompanyName(model.getCompanyName());
		invoiceDTO.setCompanygGstRegistrationTypeId(model.getCompanygGstRegistrationTypeId());
		invoiceDTO.setCompanyPinCode(model.getCompanyPinCode());
		invoiceDTO.setCompanyStateId(model.getCompanyStateId());
		invoiceDTO.setCompanyStateName(model.getCompanyStateName());
		invoiceDTO.setCompanyCountryId(model.getCompanyCountryId());
		invoiceDTO.setCompanyPrimaryMobile(model.getCompanyPrimaryMobile());
		invoiceDTO.setCompanySecondaryMobile(model.getCompanySecondaryMobile());
		invoiceDTO.setCompanyContactPersonNumber(model.getCompanyContactPersonNumber());
		invoiceDTO.setCompanyContactPersonName(model.getCompanyContactPersonName());
		invoiceDTO.setCompanyPrimaryTelephone(model.getCompanyPrimaryTelephone());
		invoiceDTO.setCompanySecondaryTelephone(model.getCompanySecondaryTelephone());
		invoiceDTO.setCompanyWebsite(model.getCompanyWebsite());
		invoiceDTO.setCompanyEmail(model.getCompanyEmail());
		invoiceDTO.setCompanyFaxNumber(model.getCompanyFaxNumber());
		invoiceDTO.setCompanyAddress(model.getCompanyAddress());
		invoiceDTO.setCompanyTagLine(model.getCompanyTagLine());
		invoiceDTO.setCompanyGstNumber(model.getCompanyGstNumber());
		invoiceDTO.setCompanyPanNumber(model.getCompanyPanNumber());
		invoiceDTO.setCompanyPanDate(model.getCompanyPanDate());
		invoiceDTO.setCompanyCeritificateImagePath(model.getCompanyCeritificateImagePath());
		invoiceDTO.setCompanyLogoPath(model.getCompanyLogoPath());
		
		invoiceDTO.setPartyName(model.getPartyName());
		invoiceDTO.setPartyContactPersonNumber(model.getPartyContactPersonNumber());
		invoiceDTO.setPartyPinCode(model.getPartyPinCode());
		invoiceDTO.setPartyAreaId(model.getPartyAreaId());
		invoiceDTO.setPartyCityId(model.getPartyCityId());
		invoiceDTO.setPartyStateId(model.getPartyStateId());
		invoiceDTO.setPartyCountryId(model.getPartyCountryId());
		invoiceDTO.setPartyPrimaryTelephone(model.getPartyPrimaryTelephone());
		invoiceDTO.setPartySecondaryTelephone(model.getPartySecondaryTelephone());
		invoiceDTO.setPartyPrimaryMobile(model.getPartyPrimaryMobile());
		invoiceDTO.setPartySecondaryMobile(model.getPartySecondaryMobile());
		invoiceDTO.setPartyEmail(model.getPartyEmail());
		invoiceDTO.setPartyWebsite(model.getPartyWebsite());
		invoiceDTO.setPartyContactPersonName(model.getPartyContactPersonName());
//		invoiceDTO.setPartyBillToAddress(model.getPartyBillToAddress());
//		invoiceDTO.setPartyShipAddress(model.getPartyBillToAddress());
		invoiceDTO.setPartyDueDaysLimit(model.getPartyDueDaysLimit());
		invoiceDTO.setPartyGstRegistrationTypeId(model.getPartyGstRegistrationTypeId());
//		invoiceDTO.setPartyGstNumber(model.getPartyGstNumber());
		invoiceDTO.setPartyPanNumber(model.getPartyPanNumber());
		invoiceDTO.setIsIgst(model.getIsIgst());
		invoiceDTO.setIsReused(model.getIsReused());
		invoiceDTO.setPartyCode(model.getParty().getPartyCode());
		invoiceDTO.setTcsPercentage(model.getTcsPercentage());
		invoiceDTO.setTcsAmount(model.getTcsAmount());
		invoiceDTO.setCreatedBy(model.getCreatedBy());
		invoiceDTO.setUpdateBy(model.getUpdatedBy());
		invoiceDTO.setCreatedDate(model.getCreatedDateTime());
		invoiceDTO.setUpdatedDate(model.getUpdatedDateTime());
		invoiceDTO.setMaterialNoteType(model.getMaterialNoteType());
		invoiceDTO.setCurrencyId(model.getCurrencyId());
		invoiceDTO.setCurrencyRate(model.getCurrencyRate());
		invoiceDTO.setTotalAmountCurrency(model.getTotalAmountCurrency());
		invoiceDTO.setFinalDestination(model.getFinalDestination());
		invoiceDTO.setVesselNumber(model.getVesselNumber());
		invoiceDTO.setShipper(model.getShipper());
		invoiceDTO.setCityOfLoading(model.getCityOfLoading());
		invoiceDTO.setCityOfDischarge(model.getCityOfDischarge());
		invoiceDTO.setCountryOfOriginOfGoods(model.getCountryOfOriginOfGoods());
		invoiceDTO.setCountryOfDestination(model.getCountryOfDestination());
		invoiceDTO.setCurrencyName(model.getCurrencyName());
		return invoiceDTO;
	}
	
	@Override
	public InvoiceHeader dtoToModelMap(InvoiceDTO dto) {
		
		
		InvoiceHeader invoiceHeaderOut = new InvoiceHeader();
		
		invoiceHeaderOut.setAdvanceAmount(dto.getAdvanceAmount());
		invoiceHeaderOut.setBillToAddress(dto.getBillToAddress());
		invoiceHeaderOut.setCessAmount(dto.getCessAmount());
		invoiceHeaderOut.setCessPercent(dto.getCessPercent());
		invoiceHeaderOut.setCgstTaxAmount(dto.getCgstTaxAmount());
		invoiceHeaderOut.setCgstTaxRate(dto.getCgstTaxRate());
		invoiceHeaderOut.setCompany(companyService.getModelById(dto.getCompanyId()));
		invoiceHeaderOut.setDeliveryChallanDate(dto.getDeliveryChallanDate());		
		invoiceHeaderOut.setDeliveryChallanNumber(dto.getDeliveryChallanNumber());
		invoiceHeaderOut.setGrnDate(dto.getGrnDate());		
		invoiceHeaderOut.setGrnNumber(dto.getGrnNumber());
		invoiceHeaderOut.setProformaInvoiceDate(dto.getProformaInvoiceDate());
		invoiceHeaderOut.setProformaInvoiceNumber(dto.getProformaInvoiceNumber());
		invoiceHeaderOut.setSourceInvoiceDate(dto.getSourceInvoiceDate());
		invoiceHeaderOut.setSourceInvoiceNumber(dto.getSourceInvoiceNumber());
		invoiceHeaderOut.setDeliveryTerms(dto.getDeliveryTerms());
		invoiceHeaderOut.setDiscountAmount(dto.getTotalDiscount());
		invoiceHeaderOut.setDiscountPercent(dto.getDiscountPercent());
		invoiceHeaderOut.setDocumentThrough(dto.getDocumentThrough());
		invoiceHeaderOut.setGrandTotal(dto.getGrandTotal());
		invoiceHeaderOut.setRemarks(dto.getRemarks());
		invoiceHeaderOut.setGstNumber(dto.getGstNumber());
		try {
			invoiceHeaderOut.setDueAmount(dto.getDueAmount()!= null ? dto.getDueAmount() : dto.getGrandTotal());
		} catch (TransactionDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error(e);
		}
		invoiceHeaderOut.seteWayBillNumber(dto.geteWayBillNumber());
		invoiceHeaderOut.setFinancialYear(financialYearService.getById(dto.getFinancialYearId()));
		invoiceHeaderOut.setId(dto.getId());
		invoiceHeaderOut.setIgstTaxAmount(dto.getIgstTaxAmount());
		invoiceHeaderOut.setIgstTaxRate(dto.getIgstTaxRate());
		invoiceHeaderOut.setInternalReferenceDate(dto.getInternalReferenceDate());
		invoiceHeaderOut.setInternalReferenceNumber(dto.getInternalReferenceNumber());
		invoiceHeaderOut.setInvoiceDate(dto.getInvoiceDate());
		invoiceHeaderOut.setInvoiceItems(invoiceItemMapper.dtoToModelList(dto.getInvoiceItems()));
		invoiceHeaderOut.setInvoiceNumber(dto.getInvoiceNumber()!= null ? dto.getInvoiceNumber() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getInvoiceTypeId())).getTransactionNumber());
		invoiceHeaderOut.setInvId(dto.getInvId() != null ? dto.getInvId() : transactionNumberService.getTransactionNumber(transactionTypeService.getModelById(dto.getInvoiceTypeId())).getTransactionId());
		invoiceHeaderOut.setStatus(dto.getId() != null ? statusService.getStatusById(dto.getStatusId()) : null);
		invoiceHeaderOut.setInvoiceType(transactionTypeService.getModelById(dto.getInvoiceTypeId()));
		invoiceHeaderOut.setIsReverseCharge(dto.getIsReverseCharge());
		invoiceHeaderOut.setLabourChargesOnly(dto.getLabourChargesOnly());
		invoiceHeaderOut.setModeOfDispatch(dto.getModeOfDispatch());
		
		Double netAmount = dto.getInvoiceItems().stream()
							.mapToDouble(item -> item.getPrice() * item.getQuantity())
							.sum();
		
		invoiceHeaderOut.setNetAmount((dto.getNetAmount() != null && dto.getNetAmount() == netAmount) ? dto.getNetAmount() : netAmount );
		//invoiceHeaderOut.setNotForSale(dto.getno);
		invoiceHeaderOut.setNumOfPackages(dto.getNumOfPackages());
		invoiceHeaderOut.setParty(partyService.getPartyObjectById(dto.getPartyId()));
		invoiceHeaderOut.setPartyAddress(dto.getAddress());
		invoiceHeaderOut.setPaymentDueDate(dto.getPaymentDueDate());
		invoiceHeaderOut.setPaymentTerms(dto.getPaymentTerms());
		invoiceHeaderOut.setPurchaseOrderDate(dto.getPurchaseOrderDate());
		invoiceHeaderOut.setPurchaseOrderNumber(dto.getPurchaseOrderNumber());
		invoiceHeaderOut.setRoundOffAmount(dto.getRoundOffAmount());
		invoiceHeaderOut.setSgstTaxAmount(dto.getSgstTaxAmount());
		invoiceHeaderOut.setSgstTaxRate(dto.getSgstTaxRate());
		invoiceHeaderOut.setShipToAddress(dto.getShipToAddress());
		invoiceHeaderOut.setSubTotal(dto.getSubTotalAmount());
		invoiceHeaderOut.setTax(dto.getTaxId() != null ?taxService.getById(dto.getTaxId()): null);
		invoiceHeaderOut.setSgstTaxRate(dto.getSgstTaxRate());
		invoiceHeaderOut.setCgstTaxRate(dto.getCgstTaxRate());
		invoiceHeaderOut.setIgstTaxRate(dto.getIgstTaxRate());
		invoiceHeaderOut.setTaxAmount(dto.getTaxAmount());
		invoiceHeaderOut.setTermsAndConditions(dto.getTermsAndConditions());
		invoiceHeaderOut.setTimeOfInvoice(dto.getTimeOfInvoice());
		invoiceHeaderOut.setTotalTaxableAmount(dto.getTotalTaxableAmount());
		invoiceHeaderOut.setTransportationCharges(dto.getTransportationCharges());
		invoiceHeaderOut.setVehicleNumber(dto.getVehicleNumber());
		invoiceHeaderOut.setVendorCode(dto.getVendorCode());
		invoiceHeaderOut.setInvoiceItems(invoiceItemMapper.dtoToModelList(dto.getInvoiceItems()));
		invoiceHeaderOut.setInclusiveTax(dto.getInclusiveTax());
		invoiceHeaderOut.setDeliveryChallanDateString(dto.getDeliveryChallanDateString());
		
		invoiceHeaderOut.setCompanyName(dto.getCompanyName());
		invoiceHeaderOut.setCompanygGstRegistrationTypeId(dto.getCompanygGstRegistrationTypeId());
		invoiceHeaderOut.setCompanyPinCode(dto.getCompanyPinCode());
		invoiceHeaderOut.setCompanyStateId(dto.getCompanyStateId());
		invoiceHeaderOut.setCompanyStateName(dto.getCompanyStateName());
		invoiceHeaderOut.setCompanyCountryId(dto.getCompanyCountryId());
		invoiceHeaderOut.setCompanyPrimaryMobile(dto.getCompanyPrimaryMobile());
		invoiceHeaderOut.setCompanySecondaryMobile(dto.getCompanySecondaryMobile());
		invoiceHeaderOut.setCompanyContactPersonNumber(dto.getCompanyContactPersonNumber());
		invoiceHeaderOut.setCompanyContactPersonName(dto.getCompanyContactPersonName());
		invoiceHeaderOut.setCompanyPrimaryTelephone(dto.getCompanyPrimaryTelephone());
		invoiceHeaderOut.setCompanySecondaryTelephone(dto.getCompanySecondaryTelephone());
		invoiceHeaderOut.setCompanyWebsite(dto.getCompanyWebsite());
		invoiceHeaderOut.setCompanyEmail(dto.getCompanyEmail());
		invoiceHeaderOut.setCompanyFaxNumber(dto.getCompanyFaxNumber());
		invoiceHeaderOut.setCompanyAddress(dto.getCompanyAddress());
		invoiceHeaderOut.setCompanyTagLine(dto.getCompanyTagLine());
		invoiceHeaderOut.setCompanyGstNumber(dto.getCompanyGstNumber());
		invoiceHeaderOut.setCompanyPanNumber(dto.getCompanyPanNumber());
		invoiceHeaderOut.setCompanyPanDate(dto.getCompanyPanDate());
		invoiceHeaderOut.setCompanyCeritificateImagePath(dto.getCompanyCeritificateImagePath());
		invoiceHeaderOut.setCompanyLogoPath(dto.getCompanyLogoPath());
		
		invoiceHeaderOut.setPartyName(dto.getPartyName());
		invoiceHeaderOut.setPartyContactPersonNumber(dto.getPartyContactPersonNumber());
		invoiceHeaderOut.setPartyPinCode(dto.getPartyPinCode());
		invoiceHeaderOut.setPartyAreaId(dto.getPartyAreaId());
		invoiceHeaderOut.setPartyCityId(dto.getPartyCityId());
		invoiceHeaderOut.setPartyStateId(dto.getPartyStateId());
		invoiceHeaderOut.setPartyCountryId(dto.getPartyCountryId());
		invoiceHeaderOut.setPartyPrimaryTelephone(dto.getPartyPrimaryTelephone());
		invoiceHeaderOut.setPartySecondaryTelephone(dto.getPartySecondaryTelephone());
		invoiceHeaderOut.setPartyPrimaryMobile(dto.getPartyPrimaryMobile());
		invoiceHeaderOut.setPartySecondaryMobile(dto.getPartySecondaryMobile());
		invoiceHeaderOut.setPartyEmail(dto.getPartyEmail());
		invoiceHeaderOut.setPartyWebsite(dto.getPartyWebsite());
		invoiceHeaderOut.setPartyContactPersonName(dto.getPartyContactPersonName());
//		invoiceHeaderOut.setPartyBillToAddress(dto.getPartyBillToAddress());
//		invoiceHeaderOut.setPartyShipAddress(dto.getPartyBillToAddress());
		invoiceHeaderOut.setPartyDueDaysLimit(dto.getPartyDueDaysLimit());
		invoiceHeaderOut.setPartyGstRegistrationTypeId(dto.getPartyGstRegistrationTypeId());
//		invoiceHeaderOut.setPartyGstNumber(dto.getPartyGstNumber());
		invoiceHeaderOut.setPartyPanNumber(dto.getPartyPanNumber());
		invoiceHeaderOut.setIsIgst(dto.getIsIgst());
		invoiceHeaderOut.setIsReused(dto.getIsReused());
		if(dto.getTcsAmount()!=null){
			invoiceHeaderOut.setTcsPercentage(dto.getTcsPercentage());
			invoiceHeaderOut.setTcsAmount(dto.getTcsAmount());
		}
		else{
			invoiceHeaderOut.setTcsPercentage(0.0);
			invoiceHeaderOut.setTcsAmount(0.0);
		}
		invoiceHeaderOut.setCreatedBy(dto.getCreatedBy());
		invoiceHeaderOut.setCreatedDateTime(dto.getCreatedDate());
		invoiceHeaderOut.setMaterialNoteType(dto.getMaterialNoteType());
		invoiceHeaderOut.setCurrencyId(dto.getCurrencyId());
		invoiceHeaderOut.setCurrencyRate(dto.getCurrencyRate());
		invoiceHeaderOut.setTotalAmountCurrency(dto.getTotalAmountCurrency());
		invoiceHeaderOut.setFinalDestination(dto.getFinalDestination());
		invoiceHeaderOut.setVesselNumber(dto.getVesselNumber());
		invoiceHeaderOut.setShipper(dto.getShipper());
		invoiceHeaderOut.setCityOfLoading(dto.getCityOfLoading());
		invoiceHeaderOut.setCityOfDischarge(dto.getCityOfDischarge());
		invoiceHeaderOut.setCountryOfOriginOfGoods(dto.getCountryOfOriginOfGoods());
		invoiceHeaderOut.setCountryOfDestination(dto.getCountryOfDestination());
		invoiceHeaderOut.setCurrencyName(dto.getCurrencyName());
		return invoiceHeaderOut;
	}

	@Override
	public List<InvoiceDTO> modelToDTOList(List<InvoiceHeader> modelList) {

		List<InvoiceDTO> invoiceDTOListOut = modelList.stream()
				.map(
				 invoiceHeader -> modelToDTOMap(invoiceHeader))
				.collect(Collectors.toList());
		
		return invoiceDTOListOut;
	}

	@Override
	public List<InvoiceHeader> dtoToModelList(List<InvoiceDTO> dtoList) {
		
		List<InvoiceHeader> invoiceHeaderListOut = dtoList.stream()
				.map(
				invoiceDTO -> dtoToModelMap(invoiceDTO))
				.collect(Collectors.toList());
		
		return invoiceHeaderListOut;
	}
	
	
	public List<InvoiceDTO> modelToRecentDTOList(List<InvoiceHeader> modelList) {

		List<InvoiceDTO> invoiceDTOListOut = modelList.stream()
				.map(
				 invoiceHeader -> modelToDTOWithoutItemMap(invoiceHeader))
				.collect(Collectors.toList());
		
		return invoiceDTOListOut;
	}



	public InvoiceHeaderAudit modelToAuditMap(InvoiceHeader  model) {

		modelMapper.getConfiguration().setAmbiguityIgnored(true);
		InvoiceHeaderAudit invoiceAudit = modelMapper.map(model, InvoiceHeaderAudit.class );//modelToDTOWithoutItemMap(model);
		modelMapper.getConfiguration().setAmbiguityIgnored(false);
		return invoiceAudit;
	}


	public List<InvoiceHeaderAudit> modelToAuditList(List<InvoiceHeader> modelList) {

		List<InvoiceHeaderAudit> invoiceAuditListOut = modelList.stream()
				.map(
						invoiceHeader -> modelToAuditMap(invoiceHeader))
				.collect(Collectors.toList());

		return invoiceAuditListOut;
	}

}
