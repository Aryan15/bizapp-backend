package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.AccountDTO;
import com.coreerp.model.Account;
import com.coreerp.service.AccountClassService;

@Component
public class AccountMapper implements AbstractMapper<AccountDTO, Account> {

	@Autowired
	AccountClassService accountClassService;
	
	
	@Override
	public AccountDTO modelToDTOMap(Account model) {
		
		AccountDTO dto = new AccountDTO();
		
		dto.setAccountClassId(model.getAccountClass().getId());
		dto.setAccountNumber(model.getAccountNumber());
		dto.setDescription(model.getDescription());
		dto.setId(model.getId());
		dto.setName(model.getName());
		return dto;
	}

	@Override
	public Account dtoToModelMap(AccountDTO dto) {
		
		Account model = new Account();
		
		model.setAccountClass(accountClassService.getModelById(dto.getAccountClassId()));
		model.setAccountNumber(dto.getAccountNumber());
		model.setDescription(dto.getDescription());
		model.setId(dto.getId());
		model.setName(dto.getName());
		
		return model;
	}

	@Override
	public List<AccountDTO> modelToDTOList(List<Account> modelList) {
		return modelList.stream()
				.map(model -> modelToDTOMap(model))
				.collect(Collectors.toList());
	}

	@Override
	public List<Account> dtoToModelList(List<AccountDTO> dtoList) {
		return dtoList.stream()
				.map(dto -> dtoToModelMap(dto))
				.collect(Collectors.toList());
	}

}
