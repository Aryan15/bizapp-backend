package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.FinancialYearDTO;
import com.coreerp.model.FinancialYear;

@Component
public class FinancialYearMapper implements AbstractMapper<FinancialYearDTO, FinancialYear> {
	
	@Autowired
	ModelMapper modelMapper;
	
	@Override
	public FinancialYearDTO modelToDTOMap(FinancialYear model) {
		
		FinancialYearDTO FinancialYearDTO = modelMapper.map(model, FinancialYearDTO.class);
		return FinancialYearDTO;
	}

	@Override
	public FinancialYear dtoToModelMap(FinancialYearDTO dto) {
		FinancialYear FinancialYear = modelMapper.map(dto, FinancialYear.class);
		return FinancialYear;
	}

	@Override
	public List<FinancialYearDTO> modelToDTOList(List<FinancialYear> modelList) {
		return modelList.stream()
				.map(FinancialYear -> modelToDTOMap(FinancialYear))
				.collect(Collectors.toList());
	}

	@Override
	public List<FinancialYear> dtoToModelList(List<FinancialYearDTO> dtoList) {
		return dtoList.stream()
				.map(FinancialYearDTO -> dtoToModelMap(FinancialYearDTO))
				.collect(Collectors.toList());
	}


}
