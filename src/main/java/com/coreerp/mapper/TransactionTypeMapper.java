package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dto.TransactionTypeDTO;
import com.coreerp.model.TransactionType;

@Component
public class TransactionTypeMapper implements AbstractMapper<TransactionTypeDTO, TransactionType> {

	@Autowired
	ModelMapper modelMapper;
	
	
	@Override
	public TransactionTypeDTO modelToDTOMap(TransactionType model) {

		return modelMapper.map(model, TransactionTypeDTO.class);
	}

	@Override
	public TransactionType dtoToModelMap(TransactionTypeDTO dto) {
		return modelMapper.map(dto, TransactionType.class);
	}

	@Override
	public List<TransactionTypeDTO> modelToDTOList(List<TransactionType> modelList) {
		return modelList.stream()
				.map(item -> modelToDTOMap(item))
				.collect(Collectors.toList());
	}

	@Override
	public List<TransactionType> dtoToModelList(List<TransactionTypeDTO> dtoList) {
		return dtoList.stream()
				.map(item -> dtoToModelMap(item))
				.collect(Collectors.toList());
	}

}
