package com.coreerp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.coreerp.dao.PrintTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dto.NumberRangeConfigurationDTO;
import com.coreerp.model.NumberRangeConfiguration;

@Component
public class NumberRangeConfigurationMapper
		implements AbstractMapper<NumberRangeConfigurationDTO, NumberRangeConfiguration> {

//	@Autowired
//	ModelMapper modelMapper;
	
	@Autowired
	TransactionTypeRepository transactionTypeRepository;

	@Autowired
	PrintTemplateRepository printTemplateRepository;
	
	@Override
	public NumberRangeConfigurationDTO modelToDTOMap(NumberRangeConfiguration model) {
		
		NumberRangeConfigurationDTO dto = new NumberRangeConfigurationDTO();
		
		if(model != null)
		{
			dto.setAutoNumber(model.getAutoNumber());
			dto.setAutoNumberReset(model.getAutoNumberReset());
			dto.setDeleted(model.getDeleted());
			dto.setId(model.getId());
			dto.setPrefix(model.getPrefix());
			dto.setPostfix(model.getPostfix());
			dto.setStartNumber(model.getStartNumber());
			dto.setTransactionTypeId(model.getTransactionType().getId());
			dto.setTransactionTypeName(model.getTransactionType().getName());
			dto.setTermsAndConditionCheck(model.getTermsAndConditionCheck());
			dto.setDelimiter(model.getDelimiter());
			dto.setFinancialYearCheck(model.getFinancialYearCheck());
			dto.setPrintTemplateId(model.getPrintTemplate() != null ? model.getPrintTemplate().getId() : null);
			dto.setPrintTemplateTopSize(model.getPrintTemplateTopSize());
			dto.setIsZeroPrefix(model.getIsZeroPrefix());
			dto.setIsJasperPrint(model.getPrintTemplate() != null ? model.getPrintTemplate().getIsJasperPrint() : null);
			dto.setJasperFileName(model.getPrintTemplate() != null ? model.getPrintTemplate().getJasperFileName() : null);
			dto.setPrintHeaderText(model.getPrintHeaderText());
			dto.setAllowShipingAddress(model.getAllowShipingAddress());
		}
		return dto;//modelMapper.map(model, NumberRangeConfigurationDTO.class);
	}

	@Override
	public NumberRangeConfiguration dtoToModelMap(NumberRangeConfigurationDTO dto) {
		
		NumberRangeConfiguration model = new NumberRangeConfiguration ();
		
		model.setAutoNumber(dto.getAutoNumber());
		model.setAutoNumberReset(dto.getAutoNumberReset());
		model.setDeleted(dto.getDeleted());
		model.setId(dto.getId());
		model.setPrefix(dto.getPrefix());
		model.setPostfix(dto.getPostfix());
		model.setStartNumber(dto.getStartNumber());
		model.setTransactionType( transactionTypeRepository.getOne(dto.getTransactionTypeId()));
		model.setTermsAndConditionCheck(dto.getTermsAndConditionCheck());
		model.setDelimiter(dto.getDelimiter());
		model.setFinancialYearCheck(dto.getFinancialYearCheck());
		model.setPrintTemplate(dto.getPrintTemplateId() != null ? printTemplateRepository.getOne(dto.getPrintTemplateId()) : null);
		model.setPrintTemplateTopSize(dto.getPrintTemplateTopSize());
		model.setIsZeroPrefix(dto.getIsZeroPrefix());
		model.setPrintHeaderText(dto.getPrintHeaderText());
		model.setAllowShipingAddress(dto.getAllowShipingAddress());

		return model;//modelMapper.map(dto, NumberRangeConfiguration.class);
	}

	@Override
	public List<NumberRangeConfigurationDTO> modelToDTOList(List<NumberRangeConfiguration> modelList) {
		return modelList.stream()
				.map(model -> modelToDTOMap(model))
				.collect(Collectors.toList());
	}

	@Override
	public List<NumberRangeConfiguration> dtoToModelList(List<NumberRangeConfigurationDTO> dtoList) {
		return dtoList.stream()
				.map(dto -> dtoToModelMap(dto))
				.collect(Collectors.toList());
	}

}
