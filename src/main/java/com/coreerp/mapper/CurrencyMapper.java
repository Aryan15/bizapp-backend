package com.coreerp.mapper;


import com.coreerp.dto.CompanyDTO;
import com.coreerp.dto.CurrencyDTO;
import com.coreerp.model.Company;
import com.coreerp.model.Currency;
import com.coreerp.model.PartyBankMap;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CurrencyMapper implements AbstractMapper<CurrencyDTO, Currency>{

    @Autowired
    private ModelMapper modelMapper;


    @Override
    public CurrencyDTO modelToDTOMap(Currency model) {

        CurrencyDTO currencyDTO = modelMapper.map(model, CurrencyDTO.class);

        return currencyDTO;
    }

    @Override
    public Currency dtoToModelMap(CurrencyDTO dto) {
        Currency currency = modelMapper.map(dto, Currency.class);

        return currency;
    }

    @Override
    public List<CurrencyDTO> modelToDTOList(List<Currency> modelList) {
        return modelList.stream()
                .map(currency -> modelToDTOMap(currency))
                .collect(Collectors.toList());
    }

    @Override
    public List<Currency> dtoToModelList(List<CurrencyDTO> dtoList) {
        return dtoList.stream()
                .map(currencyDTO -> dtoToModelMap(currencyDTO))
                .collect(Collectors.toList());
    }
}
