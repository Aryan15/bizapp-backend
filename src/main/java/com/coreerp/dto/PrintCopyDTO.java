package com.coreerp.dto;

import java.util.List;

import com.coreerp.model.PrintCopy;

public class PrintCopyDTO {
	private List<PrintCopy> printCopiesList;

	public List<PrintCopy> getPrintCopiesList() {
		return printCopiesList;
	}

	public void setPrintCopiesList(List<PrintCopy> printCopiesList) {
		this.printCopiesList = printCopiesList;
	}

	
}
