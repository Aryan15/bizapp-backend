package com.coreerp.dto;

import java.util.Date;

public class EwayBillTrackingDTO {

    private Long id;

    private String eWayBillNo;

    private Date eWayBillDate;

    private Date validUpto;

    private Date docDate;

    private String docNo;

    private String pdfUrl;

    private String detailedpdfUrl;

    private String jsonData;

    private String invoiceHeaderId;

    private Integer status;

    private  String error;

    private String transporterId;

    private String transporterName;

    private String transDocNo;

    private String vehicleNo;

    private String portName;

    private String portPin;

    private Integer portStateId;

    private Date cancelledDate;

    private String response;

    private String vehicleType;

    private Integer reasonCode;

    private String reasonRemarks;

    private Integer transactionMode;

    private Integer remainingDistance;

    private Integer extensionReasonCode;

    private String extensionReasonRemarks;

    private Long fromState;

    private Long fromPlace;

    private String fromPincode;

    private Date transDocDate;

    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private Integer transDistance;
    private Integer noOfItems;
    private String hsnCode;
    private Integer isCurrent;
    private String cancelledReason;
    private String cancelledRemarks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String geteWayBillNo() {
        return eWayBillNo;
    }

    public void seteWayBillNo(String eWayBillNo) {
        this.eWayBillNo = eWayBillNo;
    }

    public Date geteWayBillDate() {
        return eWayBillDate;
    }

    public void seteWayBillDate(Date eWayBillDate) {
        this.eWayBillDate = eWayBillDate;
    }

    public Date getValidUpto() {
        return validUpto;
    }

    public void setValidUpto(Date validUpto) {
        this.validUpto = validUpto;
    }

    public Date getDocDate() {
        return docDate;
    }

    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public String getInvoiceHeaderId() {
        return invoiceHeaderId;
    }

    public void setInvoiceHeaderId(String invoiceHeaderId) {
        this.invoiceHeaderId = invoiceHeaderId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransporterId() {
        return transporterId;
    }

    public void setTransporterId(String transporterId) {
        this.transporterId = transporterId;
    }

    public String getTransporterName() {
        return transporterName;
    }

    public void setTransporterName(String transporterName) {
        this.transporterName = transporterName;
    }

    public String getTransDocNo() {
        return transDocNo;
    }

    public void setTransDocNo(String transDocNo) {
        this.transDocNo = transDocNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    public String getPortPin() {
        return portPin;
    }

    public void setPortPin(String portPin) {
        this.portPin = portPin;
    }

    public Date getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Integer getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonRemarks() {
        return reasonRemarks;
    }

    public void setReasonRemarks(String reasonRemarks) {
        this.reasonRemarks = reasonRemarks;
    }

    public Integer getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(Integer transactionMode) {
        this.transactionMode = transactionMode;
    }

    public Integer getRemainingDistance() {
        return remainingDistance;
    }

    public void setRemainingDistance(Integer remainingDistance) {
        this.remainingDistance = remainingDistance;
    }

    public Integer getExtensionReasonCode() {
        return extensionReasonCode;
    }

    public void setExtensionReasonCode(Integer extensionReasonCode) {
        this.extensionReasonCode = extensionReasonCode;
    }

    public String getExtensionReasonRemarks() {
        return extensionReasonRemarks;
    }

    public void setExtensionReasonRemarks(String extensionReasonRemarks) {
        this.extensionReasonRemarks = extensionReasonRemarks;
    }

    public Long getFromState() {
        return fromState;
    }

    public void setFromState(Long fromState) {
        this.fromState = fromState;
    }

    public Long getFromPlace() {
        return fromPlace;
    }

    public void setFromPlace(Long fromPlace) {
        this.fromPlace = fromPlace;
    }

    public String getFromPincode() {
        return fromPincode;
    }

    public void setFromPincode(String fromPincode) {
        this.fromPincode = fromPincode;
    }

    public Date getTransDocDate() {
        return transDocDate;
    }

    public void setTransDocDate(Date transDocDate) {
        this.transDocDate = transDocDate;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public Integer getTransDistance() {
        return transDistance;
    }

    public void setTransDistance(Integer transDistance) {
        this.transDistance = transDistance;
    }

    public String getDetailedpdfUrl() {
        return detailedpdfUrl;
    }

    public void setDetailedpdfUrl(String detailedpdfUrl) {
        this.detailedpdfUrl = detailedpdfUrl;
    }

    public Integer getNoOfItems() {
        return noOfItems;
    }

    public void setNoOfItems(Integer noOfItems) {
        this.noOfItems = noOfItems;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public Integer getIsCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(Integer isCurrent) {
        this.isCurrent = isCurrent;
    }

    public String getCancelledReason() {
        return cancelledReason;
    }

    public void setCancelledReason(String cancelledReason) {
        this.cancelledReason = cancelledReason;
    }

    public String getCancelledRemarks() {
        return cancelledRemarks;
    }

    public void setCancelledRemarks(String cancelledRemarks) {
        this.cancelledRemarks = cancelledRemarks;
    }

    @Override
    public String toString() {
        return "EwayBillTrackingDTO{" +
                "id=" + id +
                ", eWayBillNo='" + eWayBillNo + '\'' +
                ", eWayBillDate=" + eWayBillDate +
                ", validUpto=" + validUpto +
                ", docDate=" + docDate +
                ", docNo='" + docNo + '\'' +
                ", pdfUrl='" + pdfUrl + '\'' +
                ", detailedpdfUrl='" + detailedpdfUrl + '\'' +
                ", jsonData='" + jsonData + '\'' +
                ", invoiceHeaderId='" + invoiceHeaderId + '\'' +
                ", status=" + status +
                ", error='" + error + '\'' +
                ", transporterId='" + transporterId + '\'' +
                ", transporterName='" + transporterName + '\'' +
                ", transDocNo='" + transDocNo + '\'' +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", portName='" + portName + '\'' +
                ", portPin='" + portPin + '\'' +
                ", portStateId='" + portStateId + '\'' +
                ", cancelledDate=" + cancelledDate +
                ", response='" + response + '\'' +
                ", vehicleType='" + vehicleType + '\'' +
                ", reasonCode=" + reasonCode +
                ", reasonRemarks='" + reasonRemarks + '\'' +
                ", transactionMode=" + transactionMode +
                ", remainingDistance=" + remainingDistance +
                ", extensionReasonCode=" + extensionReasonCode +
                ", extensionReasonRemarks='" + extensionReasonRemarks + '\'' +
                ", fromState=" + fromState +
                ", fromPlace=" + fromPlace +
                ", fromPincode='" + fromPincode + '\'' +
                ", transDocDate=" + transDocDate +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", addressLine3='" + addressLine3 + '\'' +
                ", transDistance=" + transDistance +
                ", noOfItems=" + noOfItems +
                ", hsnCode='" + hsnCode + '\'' +
                ", isCurrent=" + isCurrent +
                ", cancelledReason='" + cancelledReason + '\'' +
                ", cancelledRemarks='" + cancelledRemarks + '\'' +
                '}';
    }

    public Integer getPortStateId() {
        return portStateId;
    }

    public void setPortStateId(Integer portStateId) {
        this.portStateId = portStateId;
    }
}
