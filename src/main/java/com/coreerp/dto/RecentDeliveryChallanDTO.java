package com.coreerp.dto;

import java.util.List;

public class RecentDeliveryChallanDTO {

	private List<DeliveryChallanDTO> deliveryChallanHeaders;
	
	private Long totalCount;

	public List<DeliveryChallanDTO> getDeliveryChallanHeaders() {
		return deliveryChallanHeaders;
	}

	public void setDeliveryChallanHeaders(List<DeliveryChallanDTO> deliveryChallanHeaders) {
		this.deliveryChallanHeaders = deliveryChallanHeaders;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
