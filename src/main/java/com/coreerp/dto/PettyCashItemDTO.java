package com.coreerp.dto;

import com.coreerp.model.Company;
import com.coreerp.model.PettyCashHeader;
import com.coreerp.model.Status;

public class PettyCashItemDTO {

    private String id;
    private String headerId;
    private Long categoryId;
    private String categoryName;
    private Long paymentMethodId;
    private String paymentMethodName;
    private String remarks;
    private Long tagId;
    private String tagName;
    private  Double incomingAmount;
    private Double outgoingAmount;





    public String getHeaderId() {
        return headerId;
    }

    public void setHeaderId(String headerId) {
        this.headerId = headerId;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public Double getIncomingAmount() {
        return incomingAmount;
    }

    public void setIncomingAmount(Double incomingAmount) {
        this.incomingAmount = incomingAmount;
    }

    public Double getOutgoingAmount() {
        return outgoingAmount;
    }

    public void setOutgoingAmount(Double outgoingAmount) {
        this.outgoingAmount = outgoingAmount;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
