package com.coreerp.dto;

import java.util.List;

import com.coreerp.reports.model.BalanceReport;
import com.coreerp.reports.model.PayableReceivableReport;

public class PayableReceivableReportDTO {

	
	private List<PayableReceivableReport> payableReceivableReportItems;
	
	private List<BalanceReport> balanceReportModels;
	
	private Long totalCount;

	public List<PayableReceivableReport> getPayableReceivableReportItems() {
		return payableReceivableReportItems;
	}

	public void setPayableReceivableReportItems(List<PayableReceivableReport> payableReceivableReportItems) {
		this.payableReceivableReportItems = payableReceivableReportItems;
	}

	public List<BalanceReport> getBalanceReportModels() {
		return balanceReportModels;
	}

	public void setBalanceReportModels(List<BalanceReport> balanceReportModels) {
		this.balanceReportModels = balanceReportModels;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	
	
	
}
