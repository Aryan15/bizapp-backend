package com.coreerp.dto;

import java.util.List;

public class InvoiceWrapperDTO {

	private InvoiceDTO invoiceHeader;
	private List<String> itemToBeRemove;
	public InvoiceDTO getInvoiceHeader() {
		return invoiceHeader;
	}
	public void setInvoiceHeader(InvoiceDTO invoiceHeader) {
		this.invoiceHeader = invoiceHeader;
	}
	public List<String> getItemToBeRemove() {
		return itemToBeRemove;
	}
	public void setItemToBeRemove(List<String> itemToBeRemove) {
		this.itemToBeRemove = itemToBeRemove;
	}
	
	
}
