package com.coreerp.dto;

public class NumberRangeConfigurationDTO {

	private Long id;
	
	private String deleted;
	
	private Long transactionTypeId;
	
	private String transactionTypeName;
	
	private String prefix;
	
	private String postfix;
	
	private Integer startNumber;
	
	private Integer autoNumber;
	
	private Integer autoNumberReset;
	
	private Integer termsAndConditionCheck;
	
	private String delimiter;
	
	private Integer financialYearCheck;
	
	private Long printTemplateId;
	
	private Long printTemplateTopSize;
	
	private Integer isZeroPrefix;

	private Integer isJasperPrint;

	private String jasperFileName;

	private  String printHeaderText;

	private Integer allowShipingAddress;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public Long getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	
	public String getTransactionTypeName() {
		return transactionTypeName;
	}

	public void setTransactionTypeName(String transactionTypeName) {
		this.transactionTypeName = transactionTypeName;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	

	public String getPostfix() {
		return postfix;
	}

	public void setPostfix(String postfix) {
		this.postfix = postfix;
	}

	public Integer getStartNumber() {
		return startNumber;
	}

	public void setStartNumber(Integer startNumber) {
		this.startNumber = startNumber;
	}

	public Integer getAutoNumber() {
		return autoNumber;
	}

	public void setAutoNumber(Integer autoNumber) {
		this.autoNumber = autoNumber;
	}

	public Integer getAutoNumberReset() {
		return autoNumberReset;
	}

	public void setAutoNumberReset(Integer autoNumberReset) {
		this.autoNumberReset = autoNumberReset;
	}

	public Integer getTermsAndConditionCheck() {
		return termsAndConditionCheck;
	}

	public void setTermsAndConditionCheck(Integer termsAndConditionCheck) {
		this.termsAndConditionCheck = termsAndConditionCheck;
	}

	public String getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	public Integer getFinancialYearCheck() {
		return financialYearCheck;
	}

	public void setFinancialYearCheck(Integer financialYearCheck) {
		this.financialYearCheck = financialYearCheck;
	}

	public Long getPrintTemplateId() {
		return printTemplateId;
	}

	public void setPrintTemplateId(Long printTemplateId) {
		this.printTemplateId = printTemplateId;
	}

	public Long getPrintTemplateTopSize() {
		return printTemplateTopSize;
	}

	public void setPrintTemplateTopSize(Long printTemplateTopSize) {
		this.printTemplateTopSize = printTemplateTopSize;
	}

	public Integer getIsZeroPrefix() {
		return isZeroPrefix;
	}

	public void setIsZeroPrefix(Integer isZeroPrefix) {
		this.isZeroPrefix = isZeroPrefix;
	}


	public Integer getIsJasperPrint() {
		return isJasperPrint;
	}

	public void setIsJasperPrint(Integer isJasperPrint) {
		this.isJasperPrint = isJasperPrint;
	}

	public String getJasperFileName() {
		return jasperFileName;
	}

	public void setJasperFileName(String jasperFileName) {
		this.jasperFileName = jasperFileName;
	}

	public String getPrintHeaderText() {
		return printHeaderText;
	}

	public void setPrintHeaderText(String printHeaderText) {
		this.printHeaderText = printHeaderText;
	}

	public Integer getAllowShipingAddress() {
		return allowShipingAddress;
	}

	public void setAllowShipingAddress(Integer allowShipingAddress) {
		this.allowShipingAddress = allowShipingAddress;
	}
}
