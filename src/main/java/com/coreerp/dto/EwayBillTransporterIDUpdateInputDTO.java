package com.coreerp.dto;

import java.util.List;

public class EwayBillTransporterIDUpdateInputDTO {

    private String userCode ;
    private String clientCode ;
    private String password ;
    private List<EwayBillTransporterIDListUpdateInputDTO> transporteridlist;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<EwayBillTransporterIDListUpdateInputDTO> getTransporteridlist() {
        return transporteridlist;
    }

    public void setTransporteridlist(List<EwayBillTransporterIDListUpdateInputDTO> transporteridlist) {
        this.transporteridlist = transporteridlist;
    }

    @Override
    public String toString() {
        return "EwayBillTransporterIDUpdateInputDTO{" +
                "userCode='" + userCode + '\'' +
                ", clientCode='" + clientCode + '\'' +
                ", password='" + password + '\'' +
                ", transporteridlist=" + transporteridlist +
                '}';
    }
}
