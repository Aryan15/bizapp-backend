package com.coreerp.dto;

public class InvoiceItemDTO {

	private String id;
	
	private String headerId;
    private Integer slNo;
    private Long materialId;
    private Double quantity;
    private Double price;
    private Double amount;
    private Double discountPercentage;
    private Double discountAmount;
    private Double amountAfterDiscount;
    private Double transportationAmount;
    private Double cessPercentage;
    private Double cessAmount;
    private Double igstTaxPercentage;
    private Double igstTaxAmount;
    private Double sgstTaxPercentage;
    private Double sgstTaxAmount;
    private Double cgstTaxPercentage;
    private Double cgstTaxAmount;
    private Double amountAfterTax;
    private String remarks;
	private String purchaseOrderHeaderId;
	private String purchaseOrderItemId;
	private String grnHeaderId;
	private String grnItemId;
	private String deliveryChallanHeaderId;
	private String deliveryChallanItemId;
	private String proformaInvoiceHeaderId;
	private String proformaInvoiceItemId;
	private String sourceInvoiceHeaderId;
	private String sourceInvoiceItemId;
	private Long unitOfMeasurementId;
	private Long taxId;
    private String partNumber;
    private String hsnOrSac;
    private String partName;
    private String uom;
    private Integer inclusiveTax;
    private Double noteBalanceQuantity;
    private String specification;
    private String processId;
    private String processName;
    private Integer isContainer;
    private String outName;
	private String outPartNumber;
    
	public String getOutName() {
		return outName;
	}
	public void setOutName(String outName) {
		this.outName = outName;
	}
	public String getOutPartNumber() {
		return outPartNumber;
	}
	public void setOutPartNumber(String outPartNumber) {
		this.outPartNumber = outPartNumber;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getHeaderId() {
		return headerId;
	}
	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	public Integer getSlNo() {
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	public Long getMaterialId() {
		return materialId;
	}
	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}
	public void setAmountAfterDiscount(Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}
	public Double getTransportationAmount() {
		return transportationAmount;
	}
	public void setTransportationAmount(Double transportationAmount) {
		this.transportationAmount = transportationAmount;
	}
	public Double getCessPercentage() {
		return cessPercentage;
	}
	public void setCessPercentage(Double cessPercentage) {
		this.cessPercentage = cessPercentage;
	}
	public Double getCessAmount() {
		return cessAmount;
	}
	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}
	public Double getIgstTaxPercentage() {
		return igstTaxPercentage;
	}
	public void setIgstTaxPercentage(Double igstTaxPercentage) {
		this.igstTaxPercentage = igstTaxPercentage;
	}
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}
	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}
	public Double getSgstTaxPercentage() {
		return sgstTaxPercentage;
	}
	public void setSgstTaxPercentage(Double sgstTaxPercentage) {
		this.sgstTaxPercentage = sgstTaxPercentage;
	}
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}
	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}
	public Double getCgstTaxPercentage() {
		return cgstTaxPercentage;
	}
	public void setCgstTaxPercentage(Double cgstTaxPercentage) {
		this.cgstTaxPercentage = cgstTaxPercentage;
	}
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}
	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}
	public Double getAmountAfterTax() {
		return amountAfterTax;
	}
	public void setAmountAfterTax(Double amountAfterTax) {
		this.amountAfterTax = amountAfterTax;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getPurchaseOrderHeaderId() {
		return purchaseOrderHeaderId;
	}
	public void setPurchaseOrderHeaderId(String purchaseOrderHeaderId) {
		this.purchaseOrderHeaderId = purchaseOrderHeaderId;
	}

	public String getDeliveryChallanHeaderId() {
		return deliveryChallanHeaderId;
	}
	public void setDeliveryChallanHeaderId(String deliveryChallanHeaderId) {
		this.deliveryChallanHeaderId = deliveryChallanHeaderId;
	}
	public Long getUnitOfMeasurementId() {
		return unitOfMeasurementId;
	}
	public void setUnitOfMeasurementId(Long unitOfMeasurementId) {
		this.unitOfMeasurementId = unitOfMeasurementId;
	}
	public Long getTaxId() {
		return taxId;
	}
	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}
	
	
	
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getHsnOrSac() {
		return hsnOrSac;
	}
	public void setHsnOrSac(String hsnOrSac) {
		this.hsnOrSac = hsnOrSac;
	}
	public String getPartName() {
		return partName;
	}
	public void setPartName(String partName) {
		this.partName = partName;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getPurchaseOrderItemId() {
		return purchaseOrderItemId;
	}
	public void setPurchaseOrderItemId(String purchaseOrderItemId) {
		this.purchaseOrderItemId = purchaseOrderItemId;
	}
	public String getDeliveryChallanItemId() {
		return deliveryChallanItemId;
	}
	public void setDeliveryChallanItemId(String deliveryChallanItemId) {
		this.deliveryChallanItemId = deliveryChallanItemId;
	}
	
	public String getProformaInvoiceHeaderId() {
		return proformaInvoiceHeaderId;
	}
	public void setProformaInvoiceHeaderId(String proformaInvoiceHeaderId) {
		this.proformaInvoiceHeaderId = proformaInvoiceHeaderId;
	}
	public String getProformaInvoiceItemId() {
		return proformaInvoiceItemId;
	}
	public void setProformaInvoiceItemId(String proformaInvoiceItemId) {
		this.proformaInvoiceItemId = proformaInvoiceItemId;
	}
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}
	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}
	public String getSourceInvoiceHeaderId() {
		return sourceInvoiceHeaderId;
	}
	public void setSourceInvoiceHeaderId(String sourceInvoiceHeaderId) {
		this.sourceInvoiceHeaderId = sourceInvoiceHeaderId;
	}
	public String getSourceInvoiceItemId() {
		return sourceInvoiceItemId;
	}
	public void setSourceInvoiceItemId(String sourceInvoiceItemId) {
		this.sourceInvoiceItemId = sourceInvoiceItemId;
	}
	public Double getNoteBalanceQuantity() {
		return noteBalanceQuantity;
	}
	public void setNoteBalanceQuantity(Double noteBalanceQuantity) {
		this.noteBalanceQuantity = noteBalanceQuantity;
	}
	public String getGrnHeaderId() {
		return grnHeaderId;
	}
	public void setGrnHeaderId(String grnHeaderId) {
		this.grnHeaderId = grnHeaderId;
	}
	public String getGrnItemId() {
		return grnItemId;
	}
	public void setGrnItemId(String grnItemId) {
		this.grnItemId = grnItemId;
	}
	
	public String getSpecification() {
		return specification;
	}
	public void setSpecification(String specification) {
		this.specification = specification;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public Integer getIsContainer() {
		return isContainer;
	}
	public void setIsContainer(Integer isContainer) {
		this.isContainer = isContainer;
	}
 
	
	
    
}
