package com.coreerp.dto;

import java.util.List;

import com.coreerp.reports.model.B2BGSTReport;

public class B2BGSTReportDTO {

	
	private List<B2BGSTReport> b2bGSTReports;
	
	private Long totalCount;


	public List<B2BGSTReport> getB2bGSTReports() {
		return b2bGSTReports;
	}

	public void setB2bGSTReports(List<B2BGSTReport> b2bGSTReports) {
		this.b2bGSTReports = b2bGSTReports;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
	
}
