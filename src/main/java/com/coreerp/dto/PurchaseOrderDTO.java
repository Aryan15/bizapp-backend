package com.coreerp.dto;

import java.util.Date;
import java.util.List;

public class PurchaseOrderDTO {
	private String id;
	private String purchaseOrderNumber ;
	private Long purchaseOrderId;
	private Date purchaseOrderDate;
    private String quotationNumber ;    
    private Date quotationDate ;
    private String internalReferenceNumber;
    private Date internalReferenceDate;    
    private Long partyId ;
    private Double subTotalAmount;   
    private Double totalDiscount;
    private Double discountPercent;
    private Double totalTaxableAmount;
    private Long taxId;
    private Double roundOffAmount ;
    private Double grandTotal;
    private Long statusId;
    private Long financialYearId;
    private Long companyId;
    private Long purchaseOrderTypeId;
    private String paymentTerms;
    private String deliveryTerms;
    private String termsAndConditions;
    private Double taxAmount;
    private Double advanceAmount;
    private Double sgstTaxRate;
    private Double sgstTaxAmount;
    private Double cgstTaxRate;
    private Double cgstTaxAmount;
    private Double igstTaxRate;
    private Double igstTaxAmount;
    private Long isReverseCharge;
    private Double transportationCharges;
    private List<PurchaseOrderItemDTO> purchaseOrderItems;
    private String partyName;
    private String statusName;
    private String address;
    private String gstNumber;
    private String shipToAddress;
    private Date purchaseOrderEndDate;
    private Integer isOpenEnded;
    private String remarks;
    private Integer inclusiveTax;
    
//    private String billToAddress ;
 
    
    private String companyName; 
    private Long companygGstRegistrationTypeId;
    private String companyPinCode; 
    private Long companyStateId;
    private String companyStateName;
    private Long companyCountryId;
    private String companyPrimaryMobile;
    private String companySecondaryMobile;
    private String companyContactPersonNumber;
    private String companyContactPersonName;
    private String companyPrimaryTelephone;
    private String companySecondaryTelephone;
    private String companyWebsite;
    private String companyEmail;
    private String companyFaxNumber;
    private String companyAddress;
    private String companyTagLine;
    private String companyGstNumber;
    private String companyPanNumber;
    private String companyPanDate;
    private String companyCeritificateImagePath;
    private String companyLogoPath;
    
    private String partyContactPersonNumber;
    private String partyPinCode;
    private Long partyAreaId;
    private Long partyCityId;
    private Long partyStateId;
    private Long partyCountryId;
    private String partyPrimaryTelephone;
    private String partySecondaryTelephone;
    private String partyPrimaryMobile;
    private String partySecondaryMobile;
    private String partyEmail;
    private String partyWebsite;
    private String partyContactPersonName;
    private String partyBillToAddress;
    private String partyShipAddress;
    private String partyDueDaysLimit;
    private String partyGstRegistrationTypeId;
    private String partyGstNumber;
    private String partyPanNumber;
    private String isIgst;
    private String partyCode;

    private String createdBy;
    private String updateBy;
    private Date createdDate;
    private Date updatedDate;
    private Long closePo;
    private Long partyCurrencyId;
	private String currencyName;


	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}
	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}
	public Long getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Long purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public Date getPurchaseOrderDate() {
		return purchaseOrderDate;
	}
	public void setPurchaseOrderDate(Date purchaseOrderDate) {
		this.purchaseOrderDate = purchaseOrderDate;
	}
	public String getQuotationNumber() {
		return quotationNumber;
	}
	public void setQuotationNumber(String quotationNumber) {
		this.quotationNumber = quotationNumber;
	}
	public Date getQuotationDate() {
		return quotationDate;
	}
	public void setQuotationDate(Date quotationDate) {
		this.quotationDate = quotationDate;
	}
	public String getInternalReferenceNumber() {
		return internalReferenceNumber;
	}
	public void setInternalReferenceNumber(String internalReferenceNumber) {
		this.internalReferenceNumber = internalReferenceNumber;
	}
	public Date getInternalReferenceDate() {
		return internalReferenceDate;
	}
	public void setInternalReferenceDate(Date internalReferenceDate) {
		this.internalReferenceDate = internalReferenceDate;
	}

	public Double getSubTotalAmount() {
		return subTotalAmount;
	}
	public void setSubTotalAmount(Double subTotalAmount) {
		this.subTotalAmount = subTotalAmount;
	}

	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public Double getDiscountPercent() {
		return discountPercent;
	}
	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}


	public Double getTotalTaxableAmount() {
		return totalTaxableAmount;
	}
	public void setTotalTaxableAmount(Double totalTaxableAmount) {
		this.totalTaxableAmount = totalTaxableAmount;
	}
	public Double getRoundOffAmount() {
		return roundOffAmount;
	}
	public void setRoundOffAmount(Double roundOffAmount) {
		this.roundOffAmount = roundOffAmount;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public Long getPurchaseOrderTypeId() {
		return purchaseOrderTypeId;
	}
	public void setPurchaseOrderTypeId(Long purchaseOrderTypeId) {
		this.purchaseOrderTypeId = purchaseOrderTypeId;
	}
	public String getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public String getDeliveryTerms() {
		return deliveryTerms;
	}
	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}
	public String getTermsAndConditions() {
		return termsAndConditions;
	}
	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}
	public Double getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Double getAdvanceAmount() {
		return advanceAmount;
	}
	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}
//	public String getPurchaseOrderStatus() {
//		return purchaseOrderStatus;
//	}
//	public void setPurchaseOrderStatus(String purchaseOrderStatus) {
//		this.purchaseOrderStatus = purchaseOrderStatus;
//	}
	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}
	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}
	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}
	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}
	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}
	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}
	public Double getIgstTaxRate() {
		return igstTaxRate;
	}
	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}
	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	public Double getTransportationCharges() {
		return transportationCharges;
	}
	public void setTransportationCharges(Double transportationCharges) {
		this.transportationCharges = transportationCharges;
	}
	public List<PurchaseOrderItemDTO> getPurchaseOrderItems() {
		return purchaseOrderItems;
	}
	public void setPurchaseOrderItems(List<PurchaseOrderItemDTO> purchaseOrderItems) {
		this.purchaseOrderItems = purchaseOrderItems;
	}
	public Long getPartyId() {
		return partyId;
	}
	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}
	public Long getTaxId() {
		return taxId;
	}
	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}
	public Long getFinancialYearId() {
		return financialYearId;
	}
	public void setFinancialYearId(Long financialYearId) {
		this.financialYearId = financialYearId;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getGstNumber() {
		return gstNumber;
	}
	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}
	public String getShipToAddress() {
		return shipToAddress;
	}
	public void setShipToAddress(String shipToAddress) {
		this.shipToAddress = shipToAddress;
	}
	
	
	public Long getIsReverseCharge() {
		return isReverseCharge;
	}
	public void setIsReverseCharge(Long isReverseCharge) {
		this.isReverseCharge = isReverseCharge;
	}
	@Override
	public String toString() {
		return "PurchaseOrderDTO [id=" + id + ", purchaseOrderNumber=" + purchaseOrderNumber + ", purchaseOrderId="
				+ purchaseOrderId + ", purchaseOrderDate=" + purchaseOrderDate + ", quotationNumber=" + quotationNumber
				+ ", quotationDate=" + quotationDate + ", internalReferenceNumber=" + internalReferenceNumber
				+ ", internalReferenceDate=" + internalReferenceDate + ", partyId=" + partyId + ", subTotalAmount="
				+ subTotalAmount + ", discountAmount=" + totalDiscount + ", discountPercent=" + discountPercent
				+ ", netAmount=" + ", taxId=" + taxId + ", roundOffAmount=" + roundOffAmount
				+ ", grandTotal=" + grandTotal + ", status=" + statusId + ", financialYearId=" + financialYearId
				+ ", companyId=" + companyId + ", purchaseOrderTypeId=" + purchaseOrderTypeId + ", paymentTerms="
				+ paymentTerms + ", deliveryTerms=" + deliveryTerms + ", termsAndConditions=" + termsAndConditions
				+ ", taxAmount=" + taxAmount + ", advanceAmount=" + advanceAmount + ", sgstTaxRate=" + sgstTaxRate
				+ ", sgstTaxAmount=" + sgstTaxAmount + ", cgstTaxRate=" + cgstTaxRate + ", cgstTaxAmount="
				+ cgstTaxAmount + ", igstTaxRate=" + igstTaxRate + ", igstTaxAmount=" + igstTaxAmount
				+ ", transportationCharges=" + transportationCharges
				+ ", TAG LINE ="+ companyTagLine
				+ ", purchaseOrderItems=" + purchaseOrderItems + "]";
	}
	public Date getPurchaseOrderEndDate() {
		return purchaseOrderEndDate;
	}
	public void setPurchaseOrderEndDate(Date purchaseOrderEndDate) {
		this.purchaseOrderEndDate = purchaseOrderEndDate;
	}
	public Integer getIsOpenEnded() {
		return isOpenEnded;
	}
	public void setIsOpenEnded(Integer isOpenEnded) {
		this.isOpenEnded = isOpenEnded;
	}
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}
	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}
//....
	
	public String getCompanyLogoPath() {
		return companyLogoPath;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getCompanygGstRegistrationTypeId() {
		return companygGstRegistrationTypeId;
	}
	public void setCompanygGstRegistrationTypeId(Long companygGstRegistrationTypeId) {
		this.companygGstRegistrationTypeId = companygGstRegistrationTypeId;
	}
	public String getCompanyPinCode() {
		return companyPinCode;
	}
	public void setCompanyPinCode(String companyPinCode) {
		this.companyPinCode = companyPinCode;
	}
	public Long getCompanyStateId() {
		return companyStateId;
	}
	public void setCompanyStateId(Long companyStateId) {
		this.companyStateId = companyStateId;
	}
	public String getCompanyStateName() {
		return companyStateName;
	}
	public void setCompanyStateName(String companyStateName) {
		this.companyStateName = companyStateName;
	}
	public Long getCompanyCountryId() {
		return companyCountryId;
	}
	public void setCompanyCountryId(Long companyCountryId) {
		this.companyCountryId = companyCountryId;
	}
	public String getCompanyPrimaryMobile() {
		return companyPrimaryMobile;
	}
	public void setCompanyPrimaryMobile(String companyPrimaryMobile) {
		this.companyPrimaryMobile = companyPrimaryMobile;
	}
	public String getCompanySecondaryMobile() {
		return companySecondaryMobile;
	}
	public void setCompanySecondaryMobile(String companySecondaryMobile) {
		this.companySecondaryMobile = companySecondaryMobile;
	}
	public String getCompanyContactPersonNumber() {
		return companyContactPersonNumber;
	}
	public void setCompanyContactPersonNumber(String companyContactPersonNumber) {
		this.companyContactPersonNumber = companyContactPersonNumber;
	}
	public String getCompanyContactPersonName() {
		return companyContactPersonName;
	}
	public void setCompanyContactPersonName(String companyContactPersonName) {
		this.companyContactPersonName = companyContactPersonName;
	}
	public String getCompanyPrimaryTelephone() {
		return companyPrimaryTelephone;
	}
	public void setCompanyPrimaryTelephone(String companyPrimaryTelephone) {
		this.companyPrimaryTelephone = companyPrimaryTelephone;
	}
	public String getCompanySecondaryTelephone() {
		return companySecondaryTelephone;
	}
	public void setCompanySecondaryTelephone(String companySecondaryTelephone) {
		this.companySecondaryTelephone = companySecondaryTelephone;
	}
	public String getCompanyWebsite() {
		return companyWebsite;
	}
	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public String getCompanyFaxNumber() {
		return companyFaxNumber;
	}
	public void setCompanyFaxNumber(String companyFaxNumber) {
		this.companyFaxNumber = companyFaxNumber;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyGstNumber() {
		return companyGstNumber;
	}
	public void setCompanyGstNumber(String companyGstNumber) {
		this.companyGstNumber = companyGstNumber;
	}

	public String getCompanyPanDate() {
		return companyPanDate;
	}
	public void setCompanyPanDate(String companyPanDate) {
		this.companyPanDate = companyPanDate;
	}
	public String getCompanyCeritificateImagePath() {
		return companyCeritificateImagePath;
	}
	public void setCompanyCeritificateImagePath(String companyCeritificateImagePath) {
		this.companyCeritificateImagePath = companyCeritificateImagePath;
	}
	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}
	public String getPartyContactPersonNumber() {
		return partyContactPersonNumber;
	}
	public void setPartyContactPersonNumber(String partyContactPersonNumber) {
		this.partyContactPersonNumber = partyContactPersonNumber;
	}
	public String getPartyPinCode() {
		return partyPinCode;
	}
	public void setPartyPinCode(String partyPinCode) {
		this.partyPinCode = partyPinCode;
	}
	public Long getPartyAreaId() {
		return partyAreaId;
	}
	public void setPartyAreaId(Long partyAreaId) {
		this.partyAreaId = partyAreaId;
	}
	public Long getPartyCityId() {
		return partyCityId;
	}
	public void setPartyCityId(Long partyCityId) {
		this.partyCityId = partyCityId;
	}
	public Long getPartyStateId() {
		return partyStateId;
	}
	public void setPartyStateId(Long partyStateId) {
		this.partyStateId = partyStateId;
	}
	public Long getPartyCountryId() {
		return partyCountryId;
	}
	public void setPartyCountryId(Long partyCountryId) {
		this.partyCountryId = partyCountryId;
	}
	public String getPartyPrimaryTelephone() {
		return partyPrimaryTelephone;
	}
	public void setPartyPrimaryTelephone(String partyPrimaryTelephone) {
		this.partyPrimaryTelephone = partyPrimaryTelephone;
	}
	public String getPartySecondaryTelephone() {
		return partySecondaryTelephone;
	}
	public void setPartySecondaryTelephone(String partySecondaryTelephone) {
		this.partySecondaryTelephone = partySecondaryTelephone;
	}
	public String getPartyPrimaryMobile() {
		return partyPrimaryMobile;
	}
	public void setPartyPrimaryMobile(String partyPrimaryMobile) {
		this.partyPrimaryMobile = partyPrimaryMobile;
	}
	public String getPartySecondaryMobile() {
		return partySecondaryMobile;
	}
	public void setPartySecondaryMobile(String partySecondaryMobile) {
		this.partySecondaryMobile = partySecondaryMobile;
	}
	public String getPartyEmail() {
		return partyEmail;
	}
	public void setPartyEmail(String partyEmail) {
		this.partyEmail = partyEmail;
	}
	public String getPartyWebsite() {
		return partyWebsite;
	}
	public void setPartyWebsite(String partyWebsite) {
		this.partyWebsite = partyWebsite;
	}
	public String getPartyContactPersonName() {
		return partyContactPersonName;
	}
	public void setPartyContactPersonName(String partyContactPersonName) {
		this.partyContactPersonName = partyContactPersonName;
	}
	public String getPartyBillToAddress() {
		return partyBillToAddress;
	}
	public void setPartyBillToAddress(String partyBillToAddress) {
		this.partyBillToAddress = partyBillToAddress;
	}
	public String getPartyShipAddress() {
		return partyShipAddress;
	}
	public void setPartyShipAddress(String partyShipAddress) {
		this.partyShipAddress = partyShipAddress;
	}
	public String getPartyDueDaysLimit() {
		return partyDueDaysLimit;
	}
	public void setPartyDueDaysLimit(String partyDueDaysLimit) {
		this.partyDueDaysLimit = partyDueDaysLimit;
	}
	public String getPartyGstRegistrationTypeId() {
		return partyGstRegistrationTypeId;
	}
	public void setPartyGstRegistrationTypeId(String partyGstRegistrationTypeId) {
		this.partyGstRegistrationTypeId = partyGstRegistrationTypeId;
	}
	public String getPartyGstNumber() {
		return partyGstNumber;
	}
	public void setPartyGstNumber(String partyGstNumber) {
		this.partyGstNumber = partyGstNumber;
	}
	public String getPartyPanNumber() {
		return partyPanNumber;
	}
	public void setPartyPanNumber(String partyPanNumber) {
		this.partyPanNumber = partyPanNumber;
	}
	public String getIsIgst() {
		return isIgst;
	}
	public void setIsIgst(String isIgst) {
		this.isIgst = isIgst;
	}

	public String getCompanyTagLine() {
		return companyTagLine;
	}

	public void setCompanyTagLine(String companyTagLine) {
		this.companyTagLine = companyTagLine;
	}

	public String getCompanyPanNumber() {
		return companyPanNumber;
	}

	public void setCompanyPanNumber(String companyPanNumber) {
		this.companyPanNumber = companyPanNumber;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getClosePo() {
		return closePo;
	}

	public void setClosePo(Long closePo) {
		this.closePo = closePo;
	}

	public Long getPartyCurrencyId() {
		return partyCurrencyId;
	}

	public void setPartyCurrencyId(Long partyCurrencyId) {
		this.partyCurrencyId = partyCurrencyId;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
}
