package com.coreerp.dto;

import java.util.List;

public class RecentVoucherDTO {
	
private List<VoucherHeaderDTO> voucherHeaders;
	
	private Long totalCount;

	public List<VoucherHeaderDTO> getVoucherHeaders() {
		return voucherHeaders;
	}

	public void setVoucherHeaders(List<VoucherHeaderDTO> voucherHeaders) {
		this.voucherHeaders = voucherHeaders;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	
	

}
