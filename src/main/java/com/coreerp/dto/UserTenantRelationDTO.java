package com.coreerp.dto;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@NamedNativeQuery(
		  name = "qryUTRWithSubscrEndDate"
		, query = " select u.id, "
				+ " u.username, "
				+ " u.tenant, "
				+ " u.otp_date, "
				+ " u.otp_number, "
				+ " u.email, "
				+ " u.name, "
				+ " u.reg_date, "
				+ " u.created_date_time, "
				+ " u.created_by, "
				+ " u.updated_date_time, "
				+ " u.updated_by, "
				+ " u.is_active, "
				+ " u.address, "
				+ " u.contact_person_name, "
				+ " u.primary_email_id, "
				+ " u.primary_mobile_number, "
				+ " u.secondary_email_id, "
				+ " u.secondary_mobile_number, "
				+ " u.company_name, "
				+ " u.is_primary, "
				+ " u.company_id, "
				+ " p.from_date, "
				+ " p.to_date,"
				+ " u.is_password_reset_required "
				+ " from user_tenant_relation u "
				+ " inner join tr_customer_plan_subs p "
				+ " on(p.company_id = u.company_id) "
				+ " where p.is_current_plan = 1 "
				+ " and u.username = :username"
		, resultClass = UserTenantRelationDTO.class)
public class UserTenantRelationDTO {

	
    private Integer id;
    private String username;
    private String tenant;
    private String email;
	private Date otpDate;	
	private Long numberOfOTP;
	private String name;
	private Date registrationDate;
	private Date createdDateTime;
	private String createdBy;
	private Date updatedDateTime;
	private String updatedBy;
	private Integer isActive;
	private Integer companyId;
	private Date fromDate;
	private Date toDate;
	private Integer isPrimary;
	private Integer isPasswordResetRequired;
	@Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name="username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name="tenant")
    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }
    
    @Column(name="otp_date")
    @Basic
    @Temporal(TemporalType.DATE)
	public Date getOtpDate() {
		return otpDate;
	}

	public void setOtpDate(Date otpDate) {
		this.otpDate = otpDate;
	}

	@Column(name="otp_number")
	public Long getNumberOfOTP() {
		return numberOfOTP != null ? numberOfOTP : 0;
	}

	public void setNumberOfOTP(Long numberOfOTP) {
		this.numberOfOTP = numberOfOTP;
	}
	
	@Column(name="email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="reg_date")
	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Column(name="created_date_time", updatable = false)
	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	@Column(name="created_by", updatable = false)	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="updated_date_time")
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "UserTenantRelation [id=" + id + ", username=" + username + ", tenant=" + tenant + ", email=" + email
				+ ", otpDate=" + otpDate + ", numberOfOTP=" + numberOfOTP + ", name=" + name + "]";
	}

	@Column(name="is_active")
	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	@Column(name="company_id")
	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	
	@Column(name="from_date")
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	
	@Column(name="to_date")
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Integer getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Integer isPrimary) {
		this.isPrimary = isPrimary;
	}

	@Column(name="is_password_reset_required")
	public Integer getIsPasswordResetRequired() {
		return isPasswordResetRequired;
	}

	public void setIsPasswordResetRequired(Integer isPasswordResetRequired) {
		this.isPasswordResetRequired = isPasswordResetRequired;
	}
}
