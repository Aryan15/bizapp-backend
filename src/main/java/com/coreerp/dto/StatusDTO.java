package com.coreerp.dto;

public class StatusDTO {

private	Long id;
private String name;
private String deleted;
private Long transactionTypeId;
private String transactionTypeName;

public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getDeleted() {
	return deleted;
}
public void setDeleted(String deleted) {
	this.deleted = deleted;
}
public Long getTransactionTypeId() {
	return transactionTypeId;
}
public void setTransactionTypeId(Long transactionTypeId) {
	this.transactionTypeId = transactionTypeId;
}
public String getTransactionTypeName() {
	return transactionTypeName;
}
public void setTransactionTypeName(String transactionTypeName) {
	this.transactionTypeName = transactionTypeName;
}




}
