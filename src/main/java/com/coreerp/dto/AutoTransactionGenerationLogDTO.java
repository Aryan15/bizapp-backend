package com.coreerp.dto;

import java.util.Date;

public class AutoTransactionGenerationLogDTO {
    private String invoiceHeaderId;

    private Date transactionGenratedDate;

    private  String generatedInvoiceId;

    public String getInvoiceHeaderId() {
        return invoiceHeaderId;
    }

    public void setInvoiceHeaderId(String invoiceHeaderId) {
        this.invoiceHeaderId = invoiceHeaderId;
    }

    public Date getTransactionGenratedDate() {
        return transactionGenratedDate;
    }

    public void setTransactionGenratedDate(Date transactionGenratedDate) {
        this.transactionGenratedDate = transactionGenratedDate;
    }

    public String getGeneratedInvoiceId() {
        return generatedInvoiceId;
    }

    public void setGeneratedInvoiceId(String generatedInvoiceId) {
        this.generatedInvoiceId = generatedInvoiceId;
    }
}
