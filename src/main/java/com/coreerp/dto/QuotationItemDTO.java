package com.coreerp.dto;

public class QuotationItemDTO {

	private String id;

	private String quotationHeaderId;
	
	private Long materialId;

	private Integer slNo;
	
	private Double quantity;
	
	private Double price;
	
	private Double amount;
	
	private String status;
	
	private Double taxPercentage;
	
	private Double taxAmount;
	
	private Double amountAfterTax;
	
	private Double discountPercentage;
	
	private Double discountAmount;
	
	private Double amountAfterDiscount;
	
	private Long unitOfMeasurementId;
	
	private Long taxId;
	
	private Integer inclusiveTax;
	
	private Double taxRate;
	
	private String remarks;
	
	private Double sgstTaxPercentage;
	
	private Double sgstTaxAmount;
	
	private Double cgstTaxPercentage;
	
	private Double cgstTaxAmount;
	
	private Double igstTaxPercentage;
	
	private Double igstTaxAmount;

    private String partNumber;
    private String hsnOrSac;
    private String partName;
    private String uom;
    private String specification;
    
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getQuotationHeaderId() {
		return quotationHeaderId;
	}

	public void setQuotationHeaderId(String quotationHeaderId) {
		this.quotationHeaderId = quotationHeaderId;
	}


	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}


	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getTaxPercentage() {
		return taxPercentage;
	}

	public void setTaxPercentage(Double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Double getAmountAfterTax() {
		return amountAfterTax;
	}

	public void setAmountAfterTax(Double amountAfterTax) {
		this.amountAfterTax = amountAfterTax;
	}

	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}

	public void setAmountAfterDiscount(Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}



	public Long getUnitOfMeasurementId() {
		return unitOfMeasurementId;
	}

	public void setUnitOfMeasurementId(Long unitOfMeasurementId) {
		this.unitOfMeasurementId = unitOfMeasurementId;
	}

	public Long getTaxId() {
		return taxId;
	}

	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}



	public Integer getInclusiveTax() {
		return inclusiveTax;
	}

	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}

	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}

	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}

	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}


	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}

	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getHsnOrSac() {
		return hsnOrSac;
	}

	public void setHsnOrSac(String hsnOrSac) {
		this.hsnOrSac = hsnOrSac;
	}

	public String getPartName() {
		return partName;
	}

	public void setPartName(String partName) {
		this.partName = partName;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getSgstTaxPercentage() {
		return sgstTaxPercentage;
	}

	public void setSgstTaxPercentage(Double sgstTaxPercentage) {
		this.sgstTaxPercentage = sgstTaxPercentage;
	}

	public Double getCgstTaxPercentage() {
		return cgstTaxPercentage;
	}

	public void setCgstTaxPercentage(Double cgstTaxPercentage) {
		this.cgstTaxPercentage = cgstTaxPercentage;
	}

	public Double getIgstTaxPercentage() {
		return igstTaxPercentage;
	}

	public void setIgstTaxPercentage(Double igstTaxPercentage) {
		this.igstTaxPercentage = igstTaxPercentage;
	}

	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	

	
	
	
}
