package com.coreerp.dto;

import java.util.List;

public class PartyWithPriceListDTO {

	private PartyDTO partyDTO;
	
	private List<MaterialPriceListDTO> materialPriceListDTOList;
	
	private List<Long> materialPriceListDeletedIds;
	
	private List<PartyBankMapDTO> partyBankMapDTOList;
	
	private List<Long> partyBankMapDeletedIds;

	public PartyDTO getPartyDTO() {
		return partyDTO;
	}

	public void setPartyDTO(PartyDTO partyDTO) {
		this.partyDTO = partyDTO;
	}

	public List<MaterialPriceListDTO> getMaterialPriceListDTOList() {
		return materialPriceListDTOList;
	}

	public void setMaterialPriceListDTOList(List<MaterialPriceListDTO> materialPriceListDTOList) {
		this.materialPriceListDTOList = materialPriceListDTOList;
	}

	public List<Long> getMaterialPriceListDeletedIds() {
		return materialPriceListDeletedIds;
	}

	public void setMaterialPriceListDeletedIds(List<Long> materialPriceListDeletedIds) {
		this.materialPriceListDeletedIds = materialPriceListDeletedIds;
	}

	public List<PartyBankMapDTO> getPartyBankMapDTOList() {
		return partyBankMapDTOList;
	}

	public void setPartyBankMapDTOList(List<PartyBankMapDTO> partyBankMapDTOList) {
		this.partyBankMapDTOList = partyBankMapDTOList;
	}

	public List<Long> getPartyBankMapDeletedIds() {
		return partyBankMapDeletedIds;
	}

	public void setPartyBankMapDeletedIds(List<Long> partyBankMapDeletedIds) {
		this.partyBankMapDeletedIds = partyBankMapDeletedIds;
	}
	
	
	
}
