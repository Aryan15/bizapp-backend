package com.coreerp.dto;

public class RegistrationDTO {

	private CompanyDTO companyDTO;
	
	private UserDTO userDTO;

	private Integer enableJobwork;

	public CompanyDTO getCompanyDTO() {
		return companyDTO;
	}

	public void setCompanyDTO(CompanyDTO companyDTO) {
		this.companyDTO = companyDTO;
	}

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}


	public Integer getEnableJobwork() {
		return enableJobwork;
	}

	public void setEnableJobwork(Integer enableJobwork) {
		this.enableJobwork = enableJobwork;
	}
}
