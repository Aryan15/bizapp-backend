package com.coreerp.dto;

import java.util.Date;

public class JournalEntryDTO {
	
	private Long id;

	private Date businessDate;
	
	
	private Date transactionDate;
	
	private Long transactionTypeId;
		
	private String transactionId;
	
	private Integer transactionIdSequence;
	
	private Long partyId;
	
	private Double amount;
	
	private Long debitAccountId;
	
	private Long creditAccountId;
		
	private String isReversal;
	
	private String remarks;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	

	public Integer getTransactionIdSequence() {
		return transactionIdSequence;
	}

	public void setTransactionIdSequence(Integer transactionIdSequence) {
		this.transactionIdSequence = transactionIdSequence;
	}

	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Long getDebitAccountId() {
		return debitAccountId;
	}

	public void setDebitAccountId(Long debitAccountId) {
		this.debitAccountId = debitAccountId;
	}

	public Long getCreditAccountId() {
		return creditAccountId;
	}

	public void setCreditAccountId(Long creditAccountId) {
		this.creditAccountId = creditAccountId;
	}

	public String getIsReversal() {
		return isReversal;
	}

	public void setIsReversal(String isReversal) {
		this.isReversal = isReversal;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	
}
