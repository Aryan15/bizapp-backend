package com.coreerp.dto;

import java.util.List;

public class DeliveryChallanHeaderWrapperDTO {

	private List<String> itemToBeRemove;
	
	private DeliveryChallanDTO deliveryChallanHeader ;

	public List<String> getItemToBeRemove() {
		return itemToBeRemove;
	}

	public void setItemToBeRemove(List<String> itemToBeRemove) {
		this.itemToBeRemove = itemToBeRemove;
	}

	public DeliveryChallanDTO getDeliveryChallanHeader() {
		return deliveryChallanHeader;
	}

	public void setDeliveryChallanHeader(DeliveryChallanDTO deliveryChallanHeader) {
		this.deliveryChallanHeader = deliveryChallanHeader;
	}
	
	
}
