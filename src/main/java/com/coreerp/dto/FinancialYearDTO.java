package com.coreerp.dto;

import java.util.Date;

public class FinancialYearDTO {
	
	private Long id;
	
	private Date startDate;
	
	private Date endDate;
	
	private String financialYear;
	
	private Integer isActive;
	
	private String deleted;
	
	private Integer isModified;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}



	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public Integer getIsModified() {
		return isModified;
	}

	public void setIsModified(Integer isModified) {
		this.isModified = isModified;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}


	

}
