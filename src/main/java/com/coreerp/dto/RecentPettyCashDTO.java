package com.coreerp.dto;

import java.util.List;

public class RecentPettyCashDTO {

    private List<PettyCashHeaderDTO> pettyCashHeaders;

    private Long totalCount;


    public List<PettyCashHeaderDTO> getPettyCashHeaders() {
        return pettyCashHeaders;
    }

    public void setPettyCashHeaders(List<PettyCashHeaderDTO> pettyCashHeaders) {
        this.pettyCashHeaders = pettyCashHeaders;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
