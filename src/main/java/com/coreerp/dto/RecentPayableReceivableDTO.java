package com.coreerp.dto;

import java.util.List;

public class RecentPayableReceivableDTO {

	private List<PayableReceivableHeaderDTO> payableReceivableHeaders;
	
	private Long totalCount;

	public List<PayableReceivableHeaderDTO> getPayableReceivableHeaders() {
		return payableReceivableHeaders;
	}

	public void setPayableReceivableHeaders(List<PayableReceivableHeaderDTO> payableReceivableHeaders) {
		this.payableReceivableHeaders = payableReceivableHeaders;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
