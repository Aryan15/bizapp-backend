package com.coreerp.dto;

import java.util.List;

public class EwayBillByNumberInputDTO {
    private String userCode ;
    private String clientCode ;
    private String password ;
    private String RequestorGSTIN;
    private List<EwayBillListInputDTO> ewblist;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<EwayBillListInputDTO> getEwblist() {
        return ewblist;
    }

    public void setEwblist(List<EwayBillListInputDTO> ewblist) {
        this.ewblist = ewblist;
    }

    public String getRequestorGSTIN() {
        return RequestorGSTIN;
    }

    public void setRequestorGSTIN(String requestorGSTIN) {
        RequestorGSTIN = requestorGSTIN;
    }

    @Override
    public String toString() {
        return "EwayBillByNumberInputDTO{" +
                "userCode='" + userCode + '\'' +
                ", clientCode='" + clientCode + '\'' +
                ", password='" + password + '\'' +
                ", RequestorGSTIN='" + RequestorGSTIN + '\'' +
                ", ewblist=" + ewblist +
                '}';
    }
}
