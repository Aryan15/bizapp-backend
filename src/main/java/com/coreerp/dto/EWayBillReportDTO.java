package com.coreerp.dto;

import com.coreerp.reports.model.EWayBillReport;
import com.coreerp.reports.model.EwayBillRecentReport;

import java.util.List;

public class EWayBillReportDTO {
    private List<EWayBillReport> ewayBillReports;

    private Long totalCount;

    public List<EWayBillReport> getEwayBillReports() {
        return ewayBillReports;
    }

    public void setEwayBillReports(List<EWayBillReport> ewayBillReports) {
        this.ewayBillReports = ewayBillReports;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
