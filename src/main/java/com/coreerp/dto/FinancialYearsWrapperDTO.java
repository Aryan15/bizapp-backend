package com.coreerp.dto;

import java.util.List;

public class FinancialYearsWrapperDTO {
	
	private List<FinancialYearDTO> financialYearList;

	public List<FinancialYearDTO> getFinancialYearList() {
		return financialYearList;
	}

	public void setFinancialYearList(List<FinancialYearDTO> financialYearList) {
		this.financialYearList = financialYearList;
	}
	
	

}
