package com.coreerp.dto;

import java.util.List;

import com.coreerp.reports.model.InvoiceTaxReport;

public class InvoiceTaxReportDTO {

	private List<InvoiceTaxReport> invoiceTaxReports;
	
	private Long totalCount;

	public List<InvoiceTaxReport> getInvoiceTaxReports() {
		return invoiceTaxReports;
	}

	public void setInvoiceTaxReports(List<InvoiceTaxReport> invoiceTaxReports) {
		this.invoiceTaxReports = invoiceTaxReports;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
}
