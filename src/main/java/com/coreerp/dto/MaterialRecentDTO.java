package com.coreerp.dto;

import java.util.List;

public class MaterialRecentDTO {
	
	private List<MaterialDTO> materials;
	
	private Long totalCount;

	public List<MaterialDTO> getMaterials() {
		return materials;
	}

	public void setMaterials(List<MaterialDTO> materials) {
		this.materials = materials;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	

}
