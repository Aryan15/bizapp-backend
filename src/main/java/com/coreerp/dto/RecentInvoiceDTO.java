package com.coreerp.dto;

import java.util.List;

public class RecentInvoiceDTO {

	private List<InvoiceDTO> invoiceHeaders;
	
	private Long totalCount;

	public List<InvoiceDTO> getInvoiceHeaders() {
		return invoiceHeaders;
	}

	public void setInvoiceHeaders(List<InvoiceDTO> invoiceHeaders) {
		this.invoiceHeaders = invoiceHeaders;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
