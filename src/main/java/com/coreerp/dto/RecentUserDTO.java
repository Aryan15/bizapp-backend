package com.coreerp.dto;

import java.util.List;

public class RecentUserDTO {

	List<UserDTO> users;
	Long totalCount;
	public List<UserDTO> getUsers() {
		return users;
	}
	public void setUsers(List<UserDTO> users) {
		this.users = users;
	}
	public Long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
