package com.coreerp.dto;

import java.util.Date;
import java.util.List;

public class GRNHeaderDTO {
 
	private String id;
	private Long grnId;
	private String grnNumber;
	private Date grnDate;
	private String purchaseOrderHeaderId;
	private String purchaseOrderNumber;
	private Date purchaseOrderDate;
	private String comments;
	private Integer verbal;
	private Long grnStatusId;
	private Long companyId;
	private Long qualityCheckedById;
	private Long stockCheckedById;
	private Long financialYearId;
	private Long supplierId;
	private String supplierName;
	private String deliveryChallanHeaderId;
	private String deliveryChallanNumber;
	private Date deliveryChallanDate;
	private String termsAndConditionNote;
	private Long termsAndConditionId;
	private List<String> invoiceHeaderIds;	
	private Long grnTypeId;
	private List<GRNItemDTO> grnItems;
	private Long taxId;
	private String statusName;
	private String supplierAddress;
	private String supplierGSTN;
	
	private String stockCheckedByName;
	private String qualityCheckedByName;
	private Integer inclusiveTax;
	
    private String companyName; 
    private Long companygGstRegistrationTypeId;
    private String companyPinCode; 
    private Long companyStateId;
    private String companyStateName;
    private Long companyCountryId;
    private String companyPrimaryMobile;
    private String companySecondaryMobile;
    private String companyContactPersonNumber;
    private String companyContactPersonName;
    private String companyPrimaryTelephone;
    private String companySecondaryTelephone;
    private String companyWebsite;
    private String companyEmail;
    private String companyFaxNumber;
    private String companyAddress;
    private String companyTagLine;
    private String companyGstNumber;
    private String companyPanNumber;
    private String companyPanDate;
    private String companyCeritificateImagePath;
    private String companyLogoPath;
    
    private String partyName;
    private String partyContactPersonNumber;
    private String partyPinCode;
    private Long partyAreaId;
    private Long partyCityId;
    private Long partyStateId;
    private Long partyCountryId;
    private String partyPrimaryTelephone;
    private String partySecondaryTelephone;
    private String partyPrimaryMobile;
    private String partySecondaryMobile;
    private String partyEmail;
    private String partyWebsite;
    private String partyContactPersonName;
    private String partyBillToAddress;
    private String partyShipAddress;
    private String partyDueDaysLimit;
    private String partyGstRegistrationTypeId;
    private String partyGstNumber;
    private String partyPanNumber;
    private String isIgst;
    private  String partyCode;
	private String createdBy;
	private String updateBy;
	private Date createdDate;
	private Date updatedDate;
    
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getGrnId() {
		return grnId;
	}
	public void setGrnId(Long grnId) {
		this.grnId = grnId;
	}
	public String getGrnNumber() {
		return grnNumber;
	}
	public void setGrnNumber(String grnNumber) {
		this.grnNumber = grnNumber;
	}
	public Date getGrnDate() {
		return grnDate;
	}
	public void setGrnDate(Date grnDate) {
		this.grnDate = grnDate;
	}
	public String getPurchaseOrderHeaderId() {
		return purchaseOrderHeaderId;
	}
	public void setPurchaseOrderHeaderId(String purchaseOrderHeaderId) {
		this.purchaseOrderHeaderId = purchaseOrderHeaderId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Integer getVerbal() {
		return verbal;
	}
	public void setVerbal(Integer verbal) {
		this.verbal = verbal;
	}
	public Long getGrnStatusId() {
		return grnStatusId;
	}
	public void setGrnStatusId(Long grnStatusId) {
		this.grnStatusId = grnStatusId;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Long getQualityCheckedById() {
		return qualityCheckedById;
	}
	public void setQualityCheckedById(Long qualityCheckedById) {
		this.qualityCheckedById = qualityCheckedById;
	}
	public Long getStockCheckedById() {
		return stockCheckedById;
	}
	public void setStockCheckedById(Long stockCheckedById) {
		this.stockCheckedById = stockCheckedById;
	}
	public Long getFinancialYearId() {
		return financialYearId;
	}
	public void setFinancialYearId(Long financialYearId) {
		this.financialYearId = financialYearId;
	}
	public Long getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}
	public String getDeliveryChallanHeaderId() {
		return deliveryChallanHeaderId;
	}
	public void setDeliveryChallanHeaderId(String deliveryChallanHeaderId) {
		this.deliveryChallanHeaderId = deliveryChallanHeaderId;
	}
	public String getTermsAndConditionNote() {
		return termsAndConditionNote;
	}
	public void setTermsAndConditionNote(String termsAndConditionNote) {
		this.termsAndConditionNote = termsAndConditionNote;
	}
	public Long getTermsAndConditionId() {
		return termsAndConditionId;
	}
	public void setTermsAndConditionId(Long termsAndConditionId) {
		this.termsAndConditionId = termsAndConditionId;
	}
	public List<String> getInvoiceHeaderIds() {
		return invoiceHeaderIds;
	}
	public void setInvoiceHeaderIds(List<String> invoiceHeaderIds) {
		this.invoiceHeaderIds = invoiceHeaderIds;
	}
	
	public Long getGrnTypeId() {
		return grnTypeId;
	}
	public void setGrnTypeId(Long grnTypeId) {
		this.grnTypeId = grnTypeId;
	}
	public List<GRNItemDTO> getGrnItems() {
		return grnItems;
	}
	public void setGrnItems(List<GRNItemDTO> grnItems) {
		this.grnItems = grnItems;
	}
	public String getDeliveryChallanNumber() {
		return deliveryChallanNumber;
	}
	public void setDeliveryChallanNumber(String deliveryChallanNumber) {
		this.deliveryChallanNumber = deliveryChallanNumber;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public Long getTaxId() {
		return taxId;
	}
	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}
	public Date getDeliveryChallanDate() {
		return deliveryChallanDate;
	}
	public void setDeliveryChallanDate(Date deliveryChallanDate) {
		this.deliveryChallanDate = deliveryChallanDate;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}
	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}
	public Date getPurchaseOrderDate() {
		return purchaseOrderDate;
	}
	public void setPurchaseOrderDate(Date purchaseOrderDate) {
		this.purchaseOrderDate = purchaseOrderDate;
	}
	public String getSupplierAddress() {
		return supplierAddress;
	}
	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}
	public String getSupplierGSTN() {
		return supplierGSTN;
	}
	public void setSupplierGSTN(String supplierGSTN) {
		this.supplierGSTN = supplierGSTN;
	}
	public String getStockCheckedByName() {
		return stockCheckedByName;
	}
	public void setStockCheckedByName(String stockCheckedByName) {
		this.stockCheckedByName = stockCheckedByName;
	}
	public String getQualityCheckedByName() {
		return qualityCheckedByName;
	}
	public void setQualityCheckedByName(String qualityCheckedByName) {
		this.qualityCheckedByName = qualityCheckedByName;
	}
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}
	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getCompanygGstRegistrationTypeId() {
		return companygGstRegistrationTypeId;
	}
	public void setCompanygGstRegistrationTypeId(Long companygGstRegistrationTypeId) {
		this.companygGstRegistrationTypeId = companygGstRegistrationTypeId;
	}
	public String getCompanyPinCode() {
		return companyPinCode;
	}
	public void setCompanyPinCode(String companyPinCode) {
		this.companyPinCode = companyPinCode;
	}
	public Long getCompanyStateId() {
		return companyStateId;
	}
	public void setCompanyStateId(Long companyStateId) {
		this.companyStateId = companyStateId;
	}
	
	
	public String getCompanyStateName() {
		return companyStateName;
	}
	public void setCompanyStateName(String companyStateName) {
		this.companyStateName = companyStateName;
	}
	public Long getCompanyCountryId() {
		return companyCountryId;
	}
	public void setCompanyCountryId(Long companyCountryId) {
		this.companyCountryId = companyCountryId;
	}
	public String getCompanyPrimaryMobile() {
		return companyPrimaryMobile;
	}
	public void setCompanyPrimaryMobile(String companyPrimaryMobile) {
		this.companyPrimaryMobile = companyPrimaryMobile;
	}
	public String getCompanySecondaryMobile() {
		return companySecondaryMobile;
	}
	public void setCompanySecondaryMobile(String companySecondaryMobile) {
		this.companySecondaryMobile = companySecondaryMobile;
	}
	public String getCompanyContactPersonNumber() {
		return companyContactPersonNumber;
	}
	public void setCompanyContactPersonNumber(String companyContactPersonNumber) {
		this.companyContactPersonNumber = companyContactPersonNumber;
	}
	public String getCompanyContactPersonName() {
		return companyContactPersonName;
	}
	public void setCompanyContactPersonName(String companyContactPersonName) {
		this.companyContactPersonName = companyContactPersonName;
	}
	public String getCompanyPrimaryTelephone() {
		return companyPrimaryTelephone;
	}
	public void setCompanyPrimaryTelephone(String companyPrimaryTelephone) {
		this.companyPrimaryTelephone = companyPrimaryTelephone;
	}
	public String getCompanySecondaryTelephone() {
		return companySecondaryTelephone;
	}
	public void setCompanySecondaryTelephone(String companySecondaryTelephone) {
		this.companySecondaryTelephone = companySecondaryTelephone;
	}
	public String getCompanyWebsite() {
		return companyWebsite;
	}
	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public String getCompanyFaxNumber() {
		return companyFaxNumber;
	}
	public void setCompanyFaxNumber(String companyFaxNumber) {
		this.companyFaxNumber = companyFaxNumber;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyTagLine() {
		return companyTagLine;
	}

	public void setCompanyTagLine(String companyTagLine) {
		this.companyTagLine = companyTagLine;
	}

	public String getCompanyGstNumber() {
		return companyGstNumber;
	}
	public void setCompanyGstNumber(String companyGstNumber) {
		this.companyGstNumber = companyGstNumber;
	}

	public String getCompanyPanNumber() {
		return companyPanNumber;
	}

	public void setCompanyPanNumber(String companyPanNumber) {
		this.companyPanNumber = companyPanNumber;
	}

	public String getCompanyPanDate() {
		return companyPanDate;
	}
	public void setCompanyPanDate(String companyPanDate) {
		this.companyPanDate = companyPanDate;
	}
	public String getCompanyCeritificateImagePath() {
		return companyCeritificateImagePath;
	}
	public void setCompanyCeritificateImagePath(String companyCeritificateImagePath) {
		this.companyCeritificateImagePath = companyCeritificateImagePath;
	}
	public String getCompanyLogoPath() {
		return companyLogoPath;
	}
	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}
	
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	
	public String getPartyContactPersonNumber() {
		return partyContactPersonNumber;
	}
	public void setPartyContactPersonNumber(String partyContactPersonNumber) {
		this.partyContactPersonNumber = partyContactPersonNumber;
	}
	public String getPartyPinCode() {
		return partyPinCode;
	}
	public void setPartyPinCode(String partyPinCode) {
		this.partyPinCode = partyPinCode;
	}
	public Long getPartyAreaId() {
		return partyAreaId;
	}
	public void setPartyAreaId(Long partyAreaId) {
		this.partyAreaId = partyAreaId;
	}
	public Long getPartyCityId() {
		return partyCityId;
	}
	public void setPartyCityId(Long partyCityId) {
		this.partyCityId = partyCityId;
	}
	public Long getPartyStateId() {
		return partyStateId;
	}
	public void setPartyStateId(Long partyStateId) {
		this.partyStateId = partyStateId;
	}
	public Long getPartyCountryId() {
		return partyCountryId;
	}
	public void setPartyCountryId(Long partyCountryId) {
		this.partyCountryId = partyCountryId;
	}
	public String getPartyPrimaryTelephone() {
		return partyPrimaryTelephone;
	}
	public void setPartyPrimaryTelephone(String partyPrimaryTelephone) {
		this.partyPrimaryTelephone = partyPrimaryTelephone;
	}
	public String getPartySecondaryTelephone() {
		return partySecondaryTelephone;
	}
	public void setPartySecondaryTelephone(String partySecondaryTelephone) {
		this.partySecondaryTelephone = partySecondaryTelephone;
	}
	public String getPartyPrimaryMobile() {
		return partyPrimaryMobile;
	}
	public void setPartyPrimaryMobile(String partyPrimaryMobile) {
		this.partyPrimaryMobile = partyPrimaryMobile;
	}
	public String getPartySecondaryMobile() {
		return partySecondaryMobile;
	}
	public void setPartySecondaryMobile(String partySecondaryMobile) {
		this.partySecondaryMobile = partySecondaryMobile;
	}
	public String getPartyEmail() {
		return partyEmail;
	}
	public void setPartyEmail(String partyEmail) {
		this.partyEmail = partyEmail;
	}
	public String getPartyWebsite() {
		return partyWebsite;
	}
	public void setPartyWebsite(String partyWebsite) {
		this.partyWebsite = partyWebsite;
	}
	public String getPartyContactPersonName() {
		return partyContactPersonName;
	}
	public void setPartyContactPersonName(String partyContactPersonName) {
		this.partyContactPersonName = partyContactPersonName;
	}
	public String getPartyBillToAddress() {
		return partyBillToAddress;
	}
	public void setPartyBillToAddress(String partyBillToAddress) {
		this.partyBillToAddress = partyBillToAddress;
	}
	public String getPartyShipAddress() {
		return partyShipAddress;
	}
	public void setPartyShipAddress(String partyShipAddress) {
		this.partyShipAddress = partyShipAddress;
	}
	public String getPartyDueDaysLimit() {
		return partyDueDaysLimit;
	}
	public void setPartyDueDaysLimit(String partyDueDaysLimit) {
		this.partyDueDaysLimit = partyDueDaysLimit;
	}
	public String getPartyGstRegistrationTypeId() {
		return partyGstRegistrationTypeId;
	}
	public void setPartyGstRegistrationTypeId(String partyGstRegistrationTypeId) {
		this.partyGstRegistrationTypeId = partyGstRegistrationTypeId;
	}
	public String getPartyGstNumber() {
		return partyGstNumber;
	}
	public void setPartyGstNumber(String partyGstNumber) {
		this.partyGstNumber = partyGstNumber;
	}
	public String getPartyPanNumber() {
		return partyPanNumber;
	}
	public void setPartyPanNumber(String partyPanNumber) {
		this.partyPanNumber = partyPanNumber;
	}
	public String getIsIgst() {
		return isIgst;
	}
	public void setIsIgst(String isIgst) {
		this.isIgst = isIgst;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
