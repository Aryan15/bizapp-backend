package com.coreerp.dto;

import java.util.List;

public class UserRoleWrapperDTO {

	List<UserRoleMenuDTO> userRoleMenus;

	public List<UserRoleMenuDTO> getUserRoleMenus() {
		return userRoleMenus;
	}

	public void setUserRoleMenus(List<UserRoleMenuDTO> userRoleMenus) {
		this.userRoleMenus = userRoleMenus;
	}
	
	
}
