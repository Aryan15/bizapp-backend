package com.coreerp.dto;



import java.util.List;

import com.coreerp.reports.model.QuotationSummaryReport;

public class QuotationReportDTO {

    private List<QuotationSummaryReport> quotationSummaryReports;

    private Long totalCount;

    public List<QuotationSummaryReport> getQuotationSummaryReports() {
        return quotationSummaryReports;
    }

    public void setQuotationSummaryReports(List<QuotationSummaryReport> quotationSummaryReports) {
        this.quotationSummaryReports = quotationSummaryReports;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
