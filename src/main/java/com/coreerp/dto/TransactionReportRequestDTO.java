package com.coreerp.dto;

import java.util.Date;
import java.util.List;

public class TransactionReportRequestDTO {

	private Long partyId;
	
	private Date transactionFromDate;
	
	private Date transactionToDate;
	
	private Long financialYearId;
	
	private Long materialId;
	
	private List<Long> partyTypeIds;
	
	private String createdBy;

	private String statusName;
	private String videoHeader;

	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	public Date getTransactionFromDate() {
		return transactionFromDate;
	}

	public void setTransactionFromDate(Date transactionFromDate) {
		this.transactionFromDate = transactionFromDate;
	}

	public Date getTransactionToDate() {
		return transactionToDate;
	}

	public void setTransactionToDate(Date transactionToDate) {
		this.transactionToDate = transactionToDate;
	}

	public Long getFinancialYearId() {
		return financialYearId;
	}

	public void setFinancialYearId(Long financialYearId) {
		this.financialYearId = financialYearId;
	}

	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}



	public List<Long> getPartyTypeIds() {
		return partyTypeIds;
	}

	public void setPartyTypeIds(List<Long> partyTypeIds) {
		this.partyTypeIds = partyTypeIds;
	}


	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getVideoHeader() {
		return videoHeader;
	}

	public void setVideoHeader(String videoHeader) {
		this.videoHeader = videoHeader;
	}

	@Override
	public String toString() {
		return "TransactionReportRequestDTO [partyId=" + partyId + ", transactionFromDate=" + transactionFromDate
				+ ", transactionToDate=" + transactionToDate + ", financialYearId=" + financialYearId + ", materialId="
				+ materialId + ", partyTypeIds=" + partyTypeIds + ", createdBy=" + createdBy + ", statusName=" + statusName + ",videoHeader=" +videoHeader + "]";
	}
	
	
	
	
}
