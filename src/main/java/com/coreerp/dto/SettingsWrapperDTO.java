package com.coreerp.dto;

import java.util.List;

import com.coreerp.model.PrintCopy;
import com.coreerp.model.Role;
import com.coreerp.model.UnitOfMeasurement;

public class SettingsWrapperDTO {
	
	private GlobalSettingDTO globalSetting;
	
	private List<NumberRangeConfigurationDTO> numberRangeConfigurations;
	
	private List<TaxDTO> taxes;
	
	private List<UnitOfMeasurement> unitOfMeasurements;
	
	private List<TermsAndConditionDTO> termsAndConditions;
	
	private List<Role> roles;
	
	private List<PrintCopy> printCopies;

	//private  CompanyEmailDTO emailDTO;


	public GlobalSettingDTO getGlobalSetting() {
		return globalSetting;
	}

	public void setGlobalSetting(GlobalSettingDTO globalSetting) {
		this.globalSetting = globalSetting;
	}

	public List<NumberRangeConfigurationDTO> getNumberRangeConfigurations() {
		return numberRangeConfigurations;
	}

	public void setNumberRangeConfigurations(List<NumberRangeConfigurationDTO> numberRangeConfigurations) {
		this.numberRangeConfigurations = numberRangeConfigurations;
	}

	public List<TaxDTO> getTaxes() {
		return taxes;
	}

	public void setTaxes(List<TaxDTO> taxes) {
		this.taxes = taxes;
	}

	public List<UnitOfMeasurement> getUnitOfMeasurements() {
		return unitOfMeasurements;
	}

	public void setUnitOfMeasurements(List<UnitOfMeasurement> unitOfMeasurements) {
		this.unitOfMeasurements = unitOfMeasurements;
	}

	public List<PrintCopy> getPrintCopies() {
		return printCopies;
	}

	public void setPrintCopies(List<PrintCopy> printCopies) {
		this.printCopies = printCopies;
	}

	public List<TermsAndConditionDTO> getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(List<TermsAndConditionDTO> termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}


//	public CompanyEmailDTO getEmailDTO() {
//		return emailDTO;
//	}
//
//	public void setEmailDTO(CompanyEmailDTO emailDTO) {
//		this.emailDTO = emailDTO;
//	}
}
