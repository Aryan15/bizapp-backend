package com.coreerp.dto;

public class TransactionResponseDTO {

	String responseString;
	Integer responseStatus; // 1 success, 0 error

	public String getResponseString() {
		return responseString;
	}

	public void setResponseString(String responseString) {
		this.responseString = responseString;
	}

	public Integer getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(Integer responseStatus) {
		this.responseStatus = responseStatus;
	}

	@Override
	public String toString() {
		return "TransactionResponseDTO [responseString=" + responseString + ", responseStatus=" + responseStatus + "]";
	}
	

	
	
	
}
