package com.coreerp.dto;

import java.util.Date;
import java.util.List;

public class EwayBillResponseJsonDataDTO {


    private String  ewbNo;
    private String ewayBillDate;
    private String genMode;
    private String userGstin ;
    private Integer fromStateCode;
    private Integer actualFromStateCode;
    private String toGstin;
    private String toTrdName;
    private String toAddr1;
    private String toAddr2;
    private String toPlace;
    private Integer toPincode;
    private Integer toStateCode;
    private Integer actualToStateCode;
    private Double totalValue;
    private Double cgstValue;
    private Double sgstValue;
    private Double igstValue;
    private Double cessValue;
    private String transporterName;
    private String transporterId;
    private String transDocNo;
    private String transDocDate;
    private Integer actualDist;
    private Integer noValidDays;
    private String vehicleNo;
    private String vehicleType;
    private Double totInvValue;
    private String mainHsnCode;
    private String validUpto;
    private Integer extendedTimes;
    private String rejectStatus;
    private Integer tripSheetNo;
    private String enteredDate;
    private String status;
    private Integer transactionType;
    private String shipToGstin;
    private String shipToTradename;
    private String  dispatchFromGstin;
    private String dispatchFromTradename;
    private Integer subSupplyType ;
    private String tripSheetEwbBills;

    private List<EwayBillInvoiceItemInputDTO> itemList;

    private List<EwayBillVehicleUpdateListInputDTO> VehiclListDetails;

    public String getEwbNo() {
        return ewbNo;
    }

    public void setEwbNo(String ewbNo) {
        this.ewbNo = ewbNo;
    }

    public String getEwayBillDate() {
        return ewayBillDate;
    }

    public void setEwayBillDate(String ewayBillDate) {
        this.ewayBillDate = ewayBillDate;
    }

    public String getGenMode() {
        return genMode;
    }

    public void setGenMode(String genMode) {
        this.genMode = genMode;
    }

    public String getUserGstin() {
        return userGstin;
    }

    public void setUserGstin(String userGstin) {
        this.userGstin = userGstin;
    }

    public Integer getFromStateCode() {
        return fromStateCode;
    }

    public void setFromStateCode(Integer fromStateCode) {
        this.fromStateCode = fromStateCode;
    }

    public Integer getActualFromStateCode() {
        return actualFromStateCode;
    }

    public void setActualFromStateCode(Integer actualFromStateCode) {
        this.actualFromStateCode = actualFromStateCode;
    }

    public String getToGstin() {
        return toGstin;
    }

    public void setToGstin(String toGstin) {
        this.toGstin = toGstin;
    }

    public String getToTrdName() {
        return toTrdName;
    }

    public void setToTrdName(String toTrdName) {
        this.toTrdName = toTrdName;
    }

    public String getToAddr1() {
        return toAddr1;
    }

    public void setToAddr1(String toAddr1) {
        this.toAddr1 = toAddr1;
    }

    public String getToAddr2() {
        return toAddr2;
    }

    public void setToAddr2(String toAddr2) {
        this.toAddr2 = toAddr2;
    }

    public String getToPlace() {
        return toPlace;
    }

    public void setToPlace(String toPlace) {
        this.toPlace = toPlace;
    }

    public Integer getToPincode() {
        return toPincode;
    }

    public void setToPincode(Integer toPincode) {
        this.toPincode = toPincode;
    }

    public Integer getToStateCode() {
        return toStateCode;
    }

    public void setToStateCode(Integer toStateCode) {
        this.toStateCode = toStateCode;
    }

    public Integer getActualToStateCode() {
        return actualToStateCode;
    }

    public void setActualToStateCode(Integer actualToStateCode) {
        this.actualToStateCode = actualToStateCode;
    }

    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public Double getCgstValue() {
        return cgstValue;
    }

    public void setCgstValue(Double cgstValue) {
        this.cgstValue = cgstValue;
    }

    public Double getSgstValue() {
        return sgstValue;
    }

    public void setSgstValue(Double sgstValue) {
        this.sgstValue = sgstValue;
    }

    public Double getIgstValue() {
        return igstValue;
    }

    public void setIgstValue(Double igstValue) {
        this.igstValue = igstValue;
    }

    public Double getCessValue() {
        return cessValue;
    }

    public void setCessValue(Double cessValue) {
        this.cessValue = cessValue;
    }

    public String getTransporterName() {
        return transporterName;
    }

    public void setTransporterName(String transporterName) {
        this.transporterName = transporterName;
    }

    public String getTransporterId() {
        return transporterId;
    }

    public void setTransporterId(String transporterId) {
        this.transporterId = transporterId;
    }

    public String getTransDocNo() {
        return transDocNo;
    }

    public void setTransDocNo(String transDocNo) {
        this.transDocNo = transDocNo;
    }

    public String getTransDocDate() {
        return transDocDate;
    }

    public void setTransDocDate(String transDocDate) {
        this.transDocDate = transDocDate;
    }

    public Integer getActualDist() {
        return actualDist;
    }

    public void setActualDist(Integer actualDist) {
        this.actualDist = actualDist;
    }

    public Integer getNoValidDays() {
        return noValidDays;
    }

    public void setNoValidDays(Integer noValidDays) {
        this.noValidDays = noValidDays;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Double getTotInvValue() {
        return totInvValue;
    }

    public void setTotInvValue(Double totInvValue) {
        this.totInvValue = totInvValue;
    }

    public String getMainHsnCode() {
        return mainHsnCode;
    }

    public void setMainHsnCode(String mainHsnCode) {
        this.mainHsnCode = mainHsnCode;
    }

    public String getValidUpto() {
        return validUpto;
    }

    public void setValidUpto(String validUpto) {
        this.validUpto = validUpto;
    }

    public Integer getExtendedTimes() {
        return extendedTimes;
    }

    public void setExtendedTimes(Integer extendedTimes) {
        this.extendedTimes = extendedTimes;
    }

    public String getRejectStatus() {
        return rejectStatus;
    }

    public void setRejectStatus(String rejectStatus) {
        this.rejectStatus = rejectStatus;
    }

    public Integer getTripSheetNo() {
        return tripSheetNo;
    }

    public void setTripSheetNo(Integer tripSheetNo) {
        this.tripSheetNo = tripSheetNo;
    }

    public String getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(String enteredDate) {
        this.enteredDate = enteredDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(Integer transactionType) {
        this.transactionType = transactionType;
    }

    public String getShipToGstin() {
        return shipToGstin;
    }

    public void setShipToGstin(String shipToGstin) {
        this.shipToGstin = shipToGstin;
    }

    public String getShipToTradename() {
        return shipToTradename;
    }

    public void setShipToTradename(String shipToTradename) {
        this.shipToTradename = shipToTradename;
    }

    public String getDispatchFromGstin() {
        return dispatchFromGstin;
    }

    public void setDispatchFromGstin(String dispatchFromGstin) {
        this.dispatchFromGstin = dispatchFromGstin;
    }

    public String getDispatchFromTradename() {
        return dispatchFromTradename;
    }

    public void setDispatchFromTradename(String dispatchFromTradename) {
        this.dispatchFromTradename = dispatchFromTradename;
    }

    public Integer getSubSupplyType() {
        return subSupplyType;
    }

    public void setSubSupplyType(Integer subSupplyType) {
        this.subSupplyType = subSupplyType;
    }

    public String getTripSheetEwbBills() {
        return tripSheetEwbBills;
    }

    public void setTripSheetEwbBills(String tripSheetEwbBills) {
        this.tripSheetEwbBills = tripSheetEwbBills;
    }

    public List<EwayBillInvoiceItemInputDTO> getItemList() {
        return itemList;
    }

    public void setItemList(List<EwayBillInvoiceItemInputDTO> itemList) {
        this.itemList = itemList;
    }

    public List<EwayBillVehicleUpdateListInputDTO> getVehiclListDetails() {
        return VehiclListDetails;
    }

    public void setVehiclListDetails(List<EwayBillVehicleUpdateListInputDTO> vehiclListDetails) {
        VehiclListDetails = vehiclListDetails;
    }

    @Override
    public String toString() {
        return "EwayBillResponseJsonDataDTO{" +
                "ewbNo='" + ewbNo + '\'' +
                ", ewayBillDate='" + ewayBillDate + '\'' +
                ", genMode='" + genMode + '\'' +
                ", userGstin='" + userGstin + '\'' +
                ", fromStateCode=" + fromStateCode +
                ", actualFromStateCode=" + actualFromStateCode +
                ", toGstin='" + toGstin + '\'' +
                ", toTrdName='" + toTrdName + '\'' +
                ", toAddr1='" + toAddr1 + '\'' +
                ", toAddr2='" + toAddr2 + '\'' +
                ", toPlace='" + toPlace + '\'' +
                ", toPincode=" + toPincode +
                ", toStateCode=" + toStateCode +
                ", actualToStateCode=" + actualToStateCode +
                ", totalValue=" + totalValue +
                ", cgstValue=" + cgstValue +
                ", sgstValue=" + sgstValue +
                ", igstValue=" + igstValue +
                ", cessValue=" + cessValue +
                ", transporterName='" + transporterName + '\'' +
                ", transporterId='" + transporterId + '\'' +
                ", transDocNo='" + transDocNo + '\'' +
                ", transDocDate='" + transDocDate + '\'' +
                ", actualDist=" + actualDist +
                ", noValidDays=" + noValidDays +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", vehicleType='" + vehicleType + '\'' +
                ", totInvValue=" + totInvValue +
                ", mainHsnCode='" + mainHsnCode + '\'' +
                ", validUpto='" + validUpto + '\'' +
                ", extendedTimes=" + extendedTimes +
                ", rejectStatus='" + rejectStatus + '\'' +
                ", tripSheetNo=" + tripSheetNo +
                ", enteredDate='" + enteredDate + '\'' +
                ", status='" + status + '\'' +
                ", transactionType=" + transactionType +
                ", shipToGstin='" + shipToGstin + '\'' +
                ", shipToTradename='" + shipToTradename + '\'' +
                ", dispatchFromGstin='" + dispatchFromGstin + '\'' +
                ", dispatchFromTradename='" + dispatchFromTradename + '\'' +
                ", subSupplyType=" + subSupplyType +
                ", tripSheetEwbBills='" + tripSheetEwbBills + '\'' +
                ", itemList=" + itemList +
                ", VehiclListDetails=" + VehiclListDetails +
                '}';
    }
}
