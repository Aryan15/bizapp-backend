package com.coreerp.dto;

import java.util.List;

public class PartyDueAmountDTO {
    String partyName;
    Long partyId;
    Double partyDueAmount;
    String partyCount;

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public Long getPartyId() {
        return partyId;
    }

    public void setPartyId(Long partyId) {
        this.partyId = partyId;
    }

    public Double getPartyDueAmount() {
        return partyDueAmount;
    }

    public void setPartyDueAmount(Double partyDueAmount) {
        this.partyDueAmount = partyDueAmount;
    }



    public String getPartyCount() {
        return partyCount;
    }

    public void setPartyCount(String partyCount) {
        this.partyCount = partyCount;
    }
}
