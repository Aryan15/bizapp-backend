package com.coreerp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

public class AutoTransactionGenerationDTO {

    private Long id;

    private String invoiceHeaderId;

    private Integer periodToCreateTransaction;

    private Integer daysOfMonth;

    private Integer countOfInvoice;

    private Date startDate;

    private Integer daysOfWeek;

    private String invoiceNumber;

    private Integer stopTranaction;

    private Integer email;

    private Long partyId;

    private String partyName;


    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    private Integer totalCount;


    private List<Date> sheduledDates;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvoiceHeaderId() {
        return invoiceHeaderId;
    }

    public void setInvoiceHeaderId(String invoiceHeaderId) {
        this.invoiceHeaderId = invoiceHeaderId;
    }


    public Integer getPeriodToCreateTransaction() {
        return periodToCreateTransaction;
    }

    public void setPeriodToCreateTransaction(Integer periodToCreateTransaction) {
        this.periodToCreateTransaction = periodToCreateTransaction;
    }

    public Integer getDaysOfMonth() {
        return daysOfMonth;
    }

    public void setDaysOfMonth(Integer daysOfMonth) {
        this.daysOfMonth = daysOfMonth;
    }

    public Integer getCountOfInvoice() {
        return countOfInvoice;
    }

    public void setCountOfInvoice(Integer countOfInvoice) {
        this.countOfInvoice = countOfInvoice;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Integer getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(Integer daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }


    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }


    public Integer getStopTranaction() {
        return stopTranaction;
    }

    public void setStopTranaction(Integer stopTranaction) {
        this.stopTranaction = stopTranaction;
    }

    public Integer getEmail() {
        return email;
    }

    public void setEmail(Integer email) {
        this.email = email;
    }


    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }


    public Long getPartyId() {
        return partyId;
    }

    public void setPartyId(Long partyId) {
        this.partyId = partyId;
    }

    public List<Date> getSheduledDates() {
        return sheduledDates;
    }

    public void setSheduledDates(List<Date> sheduledDates) {
        this.sheduledDates = sheduledDates;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }
}
