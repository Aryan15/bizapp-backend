package com.coreerp.dto;

import com.coreerp.reports.model.PartyPriceListReport;

import java.util.List;

public class PartyMaterialPriceListDTO {
    private List<PartyPriceListReport> partyPriceListReport;

    private Integer totalCount;


    public List<PartyPriceListReport> getPartyPriceListReport() {
        return partyPriceListReport;
    }

    public void setPartyPriceListReport(List<PartyPriceListReport> partyPriceListReport) {
        this.partyPriceListReport = partyPriceListReport;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
}
