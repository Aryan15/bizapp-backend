package com.coreerp.dto;

public class CompanyEmailDTO  {
    private Long id;
    private Long companyId;
    private String emailUserName;
    private String emailPassword;
    private Long emailPort;
    private String emailTemplate;
    private String emailSubject;
    private String emailHost;
    private Integer isEmailEnabled;
    private String emailSmtpStarttlsEnable;
    private String emailSmtpAuth;
    private String emailTransportProtocol;
    private String emailDebug;
    private String emailFrom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getEmailUserName() {
        return emailUserName;
    }

    public void setEmailUserName(String emailUserName) {
        this.emailUserName = emailUserName;
    }

    public String getEmailPassword() {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    public Long getEmailPort() {
        return emailPort;
    }

    public void setEmailPort(Long emailPort) {
        this.emailPort = emailPort;
    }

    public String getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(String emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailHost() {
        return emailHost;
    }

    public void setEmailHost(String emailHost) {
        this.emailHost = emailHost;
    }

    public Integer getIsEmailEnabled() {
        return isEmailEnabled;
    }

    public void setIsEmailEnabled(Integer isEmailEnabled) {
        this.isEmailEnabled = isEmailEnabled;
    }

    public String getEmailSmtpStarttlsEnable() {
        return emailSmtpStarttlsEnable;
    }

    public void setEmailSmtpStarttlsEnable(String emailSmtpStarttlsEnable) {
        this.emailSmtpStarttlsEnable = emailSmtpStarttlsEnable;
    }

    public String getEmailSmtpAuth() {
        return emailSmtpAuth;
    }

    public void setEmailSmtpAuth(String emailSmtpAuth) {
        this.emailSmtpAuth = emailSmtpAuth;
    }

    public String getEmailTransportProtocol() {
        return emailTransportProtocol;
    }

    public void setEmailTransportProtocol(String emailTransportProtocol) {
        this.emailTransportProtocol = emailTransportProtocol;
    }

    public String getEmailDebug() {
        return emailDebug;
    }

    public void setEmailDebug(String emailDebug) {
        this.emailDebug = emailDebug;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }
}
