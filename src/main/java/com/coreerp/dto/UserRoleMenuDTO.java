package com.coreerp.dto;

import java.util.List;

public class UserRoleMenuDTO {
	private Long menuId;
	private Integer isRoleAssigned;
	private Integer menuOrder;
	private String menuName;
	private List<UserRoleActivityDTO> activities;
	public Long getMenuId() {
		return menuId;
	}
	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}
	public Integer getIsRoleAssigned() {
		return isRoleAssigned;
	}
	public void setIsRoleAssigned(Integer isRoleAssigned) {
		this.isRoleAssigned = isRoleAssigned;
	}
	public Integer getMenuOrder() {
		return menuOrder;
	}
	public void setMenuOrder(Integer menuOrder) {
		this.menuOrder = menuOrder;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public List<UserRoleActivityDTO> getActivities() {
		return activities;
	}
	public void setActivities(List<UserRoleActivityDTO> activities) {
		this.activities = activities;
	}
	
	
	
}
