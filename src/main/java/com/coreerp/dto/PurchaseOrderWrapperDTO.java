package com.coreerp.dto;

import java.util.List;

public class PurchaseOrderWrapperDTO {
	private List<String> itemToBeRemove;
    private PurchaseOrderDTO purchaseOrderHeader;
	public List<String> getItemToBeRemove() {
		return itemToBeRemove;
	}
	public void setItemToBeRemove(List<String> itemToBeRemove) {
		this.itemToBeRemove = itemToBeRemove;
	}
	public PurchaseOrderDTO getPurchaseOrderHeader() {
		return purchaseOrderHeader;
	}
	public void setPurchaseOrderHeader(PurchaseOrderDTO purchaseOrderHeader) {
		this.purchaseOrderHeader = purchaseOrderHeader;
	}
    
    
}
