package com.coreerp.dto;

public class TaxDTO {

	private Long id;
	private String name;
	private Double rate;
	private String deleted;
	private Long taxTypeId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public Long getTaxTypeId() {
		return taxTypeId;
	}
	public void setTaxTypeId(Long taxTypeId) {
		this.taxTypeId = taxTypeId;
	}
	
	
}
