package com.coreerp.dto;

import org.springframework.stereotype.Component;


public class RoleDTO {

	private Long id;
	private String role;
	private Long roleTypeId;
	private String roleTypeName;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Long getRoleTypeId() {
		return roleTypeId;
	}
	public void setRoleTypeId(Long roleTypeId) {
		this.roleTypeId = roleTypeId;
	}
	public String getRoleTypeName() {
		return roleTypeName;
	}
	public void setRoleTypeName(String roleTypeName) {
		this.roleTypeName = roleTypeName;
	}
	
	
	
}
