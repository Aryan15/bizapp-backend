package com.coreerp.dto;

public class DeactivatedUser {
	
	private Integer id;
	
	private String tenant;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTenant() {
		return tenant;
	}

	public void setTenant(String tenant) {
		this.tenant = tenant;
	}

	@Override
	public String toString() {
		return "DeactivatedUser [id=" + id + ", tenant=" + tenant + "]";
	}
	
	
	

}
