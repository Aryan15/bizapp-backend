package com.coreerp.dto;

import java.util.List;

public class TallyExportTrackerReport {


    private List<TallyExportTrackerDTO> tallyExportTrackerItems;

    private Long totalCount;

    public List<TallyExportTrackerDTO> getTallyExportTrackerItems() {
        return tallyExportTrackerItems;
    }

    public void setTallyExportTrackerItems(List<TallyExportTrackerDTO> tallyExportTrackerItems) {
        this.tallyExportTrackerItems = tallyExportTrackerItems;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
