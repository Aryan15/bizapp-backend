package com.coreerp.dto;

public class MaterialPriceListDTO {

	private Long id;
	
	private Long partyId;
	
	private String materialTypeName;
    
	private String materialName;
    
	private String partNumber;
	
	private Long materialId;
	
	private Double discountPercentage;
	
	private Double sellingPrice;
	
	private String comment;
	
	private String deleted;
	
	private Double currentSellingPrice; 
	
	private Double currentBuyingPrice;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public Double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public String getMaterialTypeName() {
		return materialTypeName;
	}

	public void setMaterialTypeName(String materialTypeName) {
		this.materialTypeName = materialTypeName;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public Double getCurrentSellingPrice() {
		return currentSellingPrice;
	}

	public void setCurrentSellingPrice(Double currentSellingPrice) {
		this.currentSellingPrice = currentSellingPrice;
	}

	public Double getCurrentBuyingPrice() {
		return currentBuyingPrice;
	}

	public void setCurrentBuyingPrice(Double currentBuyingPrice) {
		this.currentBuyingPrice = currentBuyingPrice;
	}

	
	
}
