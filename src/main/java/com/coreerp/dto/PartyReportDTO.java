package com.coreerp.dto;

import java.util.List;

public class PartyReportDTO {

	private List<PartyDTO> parties;
	
	private Long totalCount;

	public List<PartyDTO> getParties() {
		return parties;
	}

	public void setParties(List<PartyDTO> parties) {
		this.parties = parties;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
