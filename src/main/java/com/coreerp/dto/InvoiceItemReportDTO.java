package com.coreerp.dto;

import java.util.List;

import com.coreerp.reports.model.InvoiceItemReportModel;

public class InvoiceItemReportDTO {

	private List<InvoiceItemReportModel> invoiceReportItems;
	
	private Long totalCount;

	public List<InvoiceItemReportModel> getInvoiceReportItems() {
		return invoiceReportItems;
	}

	public void setInvoiceReportItems(List<InvoiceItemReportModel> invoiceReportItems) {
		this.invoiceReportItems = invoiceReportItems;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
