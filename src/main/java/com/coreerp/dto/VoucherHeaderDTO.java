package com.coreerp.dto;

import java.util.Date;
import java.util.List;

public class VoucherHeaderDTO {
	
	private String id;
	
	private Double amount;

	private String voucherNumber;
	
	private Long voucherId;
	
	private Date voucherDate;
	
	private Long voucherTypeId;
	
	private String comments;
	
	private String paidTo;
	
	private String chequeNumber;
	
	private Long companyId;
	
	private Long financialYearId;
	
	private String bankName;
	
	private Date chequeDate;
	
	private List<VoucherItemDTO> voucherItems;
	
    private String companyName; 
    private Long companygGstRegistrationTypeId;
    private String companyPinCode; 
    private Long companyStateId;
    private String companyStateName;
    private Long companyCountryId;
    private String companyPrimaryMobile;
    private String companySecondaryMobile;
    private String companyContactPersonNumber;
    private String companyContactPersonName;
    private String companyPrimaryTelephone;
    private String companySecondaryTelephone;
    private String companyWebsite;
    private String companyEmail;
    private String companyFaxNumber;
    private String companyAddress;
    private String companytagLine;
    private String companyGstNumber;
    private String compapanNumber;
    private String companyPanDate;
    private String companyCeritificateImagePath;
    private String companyLogoPath;
	private String createdBy;
	private String updateBy;
	private Date createdDate;
	private Date updatedDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	
	public Long getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(Long voucherId) {
		this.voucherId = voucherId;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public Long getVoucherTypeId() {
		return voucherTypeId;
	}

	public void setVoucherTypeId(Long voucherTypeId) {
		this.voucherTypeId = voucherTypeId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPaidTo() {
		return paidTo;
	}

	public void setPaidTo(String paidTo) {
		this.paidTo = paidTo;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public List<VoucherItemDTO> getVoucherItems() {
		return voucherItems;
	}

	public void setVoucherItems(List<VoucherItemDTO> voucherItems) {
		this.voucherItems = voucherItems;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getFinancialYearId() {
		return financialYearId;
	}

	public void setFinancialYearId(Long financialYearId) {
		this.financialYearId = financialYearId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCompanygGstRegistrationTypeId() {
		return companygGstRegistrationTypeId;
	}

	public void setCompanygGstRegistrationTypeId(Long companygGstRegistrationTypeId) {
		this.companygGstRegistrationTypeId = companygGstRegistrationTypeId;
	}

	public String getCompanyPinCode() {
		return companyPinCode;
	}

	public void setCompanyPinCode(String companyPinCode) {
		this.companyPinCode = companyPinCode;
	}

	public Long getCompanyStateId() {
		return companyStateId;
	}

	public void setCompanyStateId(Long companyStateId) {
		this.companyStateId = companyStateId;
	}
	

	public String getCompanyStateName() {
		return companyStateName;
	}

	public void setCompanyStateName(String companyStateName) {
		this.companyStateName = companyStateName;
	}

	public Long getCompanyCountryId() {
		return companyCountryId;
	}

	public void setCompanyCountryId(Long companyCountryId) {
		this.companyCountryId = companyCountryId;
	}

	public String getCompanyPrimaryMobile() {
		return companyPrimaryMobile;
	}

	public void setCompanyPrimaryMobile(String companyPrimaryMobile) {
		this.companyPrimaryMobile = companyPrimaryMobile;
	}

	public String getCompanySecondaryMobile() {
		return companySecondaryMobile;
	}

	public void setCompanySecondaryMobile(String companySecondaryMobile) {
		this.companySecondaryMobile = companySecondaryMobile;
	}

	public String getCompanyContactPersonNumber() {
		return companyContactPersonNumber;
	}

	public void setCompanyContactPersonNumber(String companyContactPersonNumber) {
		this.companyContactPersonNumber = companyContactPersonNumber;
	}

	public String getCompanyContactPersonName() {
		return companyContactPersonName;
	}

	public void setCompanyContactPersonName(String companyContactPersonName) {
		this.companyContactPersonName = companyContactPersonName;
	}

	public String getCompanyPrimaryTelephone() {
		return companyPrimaryTelephone;
	}

	public void setCompanyPrimaryTelephone(String companyPrimaryTelephone) {
		this.companyPrimaryTelephone = companyPrimaryTelephone;
	}

	public String getCompanySecondaryTelephone() {
		return companySecondaryTelephone;
	}

	public void setCompanySecondaryTelephone(String companySecondaryTelephone) {
		this.companySecondaryTelephone = companySecondaryTelephone;
	}

	public String getCompanyWebsite() {
		return companyWebsite;
	}

	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}

	public String getCompanyEmail() {
		return companyEmail;
	}

	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}

	public String getCompanyFaxNumber() {
		return companyFaxNumber;
	}

	public void setCompanyFaxNumber(String companyFaxNumber) {
		this.companyFaxNumber = companyFaxNumber;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanytagLine() {
		return companytagLine;
	}

	public void setCompanytagLine(String companytagLine) {
		this.companytagLine = companytagLine;
	}

	public String getCompanyGstNumber() {
		return companyGstNumber;
	}

	public void setCompanyGstNumber(String companyGstNumber) {
		this.companyGstNumber = companyGstNumber;
	}

	public String getCompapanNumber() {
		return compapanNumber;
	}

	public void setCompapanNumber(String compapanNumber) {
		this.compapanNumber = compapanNumber;
	}

	public String getCompanyPanDate() {
		return companyPanDate;
	}

	public void setCompanyPanDate(String companyPanDate) {
		this.companyPanDate = companyPanDate;
	}

	public String getCompanyCeritificateImagePath() {
		return companyCeritificateImagePath;
	}

	public void setCompanyCeritificateImagePath(String companyCeritificateImagePath) {
		this.companyCeritificateImagePath = companyCeritificateImagePath;
	}

	public String getCompanyLogoPath() {
		return companyLogoPath;
	}

	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}


	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
