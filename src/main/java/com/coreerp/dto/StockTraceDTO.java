package com.coreerp.dto;

import java.util.Date;

public class StockTraceDTO {

	private String id;
	
	private Date businessDate;
	
	private Date transactionDate;
	
	private Long materialId;
	
	private Long transactionTypeId;
	
	private String transactionHeaderId;
	
	private String transactionId;
	
	private Integer transactionIdSequence;
	
	private String transactionNote;
	
	private Long partyId;
	
	private Double quantityIn;
	
	private Double quantityOut;
	
	private String reverse;
	
	private String remarks;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	public Long getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	
	public String getTransactionHeaderId() {
		return transactionHeaderId;
	}

	public void setTransactionHeaderId(String transactionHeaderId) {
		this.transactionHeaderId = transactionHeaderId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	
	public Integer getTransactionIdSequence() {
		return transactionIdSequence;
	}

	public void setTransactionIdSequence(Integer transactionIdSequence) {
		this.transactionIdSequence = transactionIdSequence;
	}

	public String getTransactionNote() {
		return transactionNote;
	}

	public void setTransactionNote(String transactionNote) {
		this.transactionNote = transactionNote;
	}

	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	public Double getQuantityIn() {
		return quantityIn;
	}

	public void setQuantityIn(Double quantityIn) {
		this.quantityIn = quantityIn;
	}

	public Double getQuantityOut() {
		return quantityOut;
	}

	public void setQuantityOut(Double quantityOut) {
		this.quantityOut = quantityOut;
	}

	public String getReverse() {
		return reverse;
	}

	public void setReverse(String reverse) {
		this.reverse = reverse;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}
