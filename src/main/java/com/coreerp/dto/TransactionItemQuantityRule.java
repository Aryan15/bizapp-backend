package com.coreerp.dto;

public class TransactionItemQuantityRule {

	private Integer statusRuleId;
//	private String transactionType;
//	private String status;
//	private String prevStatus;
//	private String sourceTransactionType;
	private Double sourceQuantity;
	private String itemId;
	private String headerId;
	private Double quantity;
	//private Double prevQuantity;
	private Double prevDCBalanceQuantity;
	private Double prevGRNBalanceQuantity;
	private Double prevInvoiceBalanceQuantity;
	private Double dCBalanceQuantity;
	private Double gRNBalanceQuantity;
	private Double invoiceBalanceQuantity;
	public Integer getStatusRuleId() {
		return statusRuleId;
	}
	public void setStatusRuleId(Integer statusRuleId) {
		this.statusRuleId = statusRuleId;
	}
	public Double getSourceQuantity() {
		return sourceQuantity;
	}
	public void setSourceQuantity(Double sourceQuantity) {
		this.sourceQuantity = sourceQuantity;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getHeaderId() {
		return headerId;
	}
	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getPrevDCBalanceQuantity() {
		return prevDCBalanceQuantity;
	}
	public void setPrevDCBalanceQuantity(Double prevDCBalanceQuantity) {
		this.prevDCBalanceQuantity = prevDCBalanceQuantity;
	}
	public Double getPrevGRNBalanceQuantity() {
		return prevGRNBalanceQuantity;
	}
	public void setPrevGRNBalanceQuantity(Double prevGRNBalanceQuantity) {
		this.prevGRNBalanceQuantity = prevGRNBalanceQuantity;
	}
	public Double getPrevInvoiceBalanceQuantity() {
		return prevInvoiceBalanceQuantity;
	}
	public void setPrevInvoiceBalanceQuantity(Double prevInvoiceBalanceQuantity) {
		this.prevInvoiceBalanceQuantity = prevInvoiceBalanceQuantity;
	}
	public Double getdCBalanceQuantity() {
		return dCBalanceQuantity;
	}
	public void setdCBalanceQuantity(Double dCBalanceQuantity) {
		this.dCBalanceQuantity = dCBalanceQuantity;
	}
	public Double getgRNBalanceQuantity() {
		return gRNBalanceQuantity;
	}
	public void setgRNBalanceQuantity(Double gRNBalanceQuantity) {
		this.gRNBalanceQuantity = gRNBalanceQuantity;
	}
	public Double getInvoiceBalanceQuantity() {
		return invoiceBalanceQuantity;
	}
	public void setInvoiceBalanceQuantity(Double invoiceBalanceQuantity) {
		this.invoiceBalanceQuantity = invoiceBalanceQuantity;
	}
	@Override
	public String toString() {
		return "TransactionItemQuantityRule [statusRuleId=" + statusRuleId + ", sourceQuantity=" + sourceQuantity
				+ ", itemId=" + itemId + ", headerId=" + headerId + ", quantity=" + quantity +  "prevDCBalanceQuantity=" + prevDCBalanceQuantity + ", prevGRNBalanceQuantity="
				+ prevGRNBalanceQuantity + ", prevInvoiceBalanceQuantity=" + prevInvoiceBalanceQuantity
				+ ", dCBalanceQuantity=" + dCBalanceQuantity + ", gRNBalanceQuantity=" + gRNBalanceQuantity
				+ ", invoiceBalanceQuantity=" + invoiceBalanceQuantity + "]";
	}
	
	
	
	
}
