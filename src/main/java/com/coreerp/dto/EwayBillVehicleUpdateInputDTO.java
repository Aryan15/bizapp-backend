package com.coreerp.dto;

import java.util.List;

public class EwayBillVehicleUpdateInputDTO {


    private String userCode ;
    private String clientCode ;
    private String password ;
    private List<EwayBillVehicleUpdateListInputDTO> vehicleupdatelist;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<EwayBillVehicleUpdateListInputDTO> getVehicleupdatelist() {
        return vehicleupdatelist;
    }

    public void setVehicleupdatelist(List<EwayBillVehicleUpdateListInputDTO> vehicleupdatelist) {
        this.vehicleupdatelist = vehicleupdatelist;
    }

    @Override
    public String toString() {
        return "EwayBillVehicleUpdateInputDTO{" +
                "userCode='" + userCode + '\'' +
                ", clientCode='" + clientCode + '\'' +
                ", password='" + password + '\'' +
                ", vehicleupdatelist=" + vehicleupdatelist +
                '}';
    }
}
