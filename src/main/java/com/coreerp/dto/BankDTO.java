package com.coreerp.dto;

public class BankDTO {
	
	 private Long id;

	 private String bankname;
	 
	 private String bankAddress;	 
	 
	 private String deleted;
	 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBankname() {
		return bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}


	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "BankDTO [id=" + id + ", bankname=" + bankname + ", bankAddress=" + bankAddress + ", deleted=" + deleted
				+ "]";
	}



	 
	 
}
