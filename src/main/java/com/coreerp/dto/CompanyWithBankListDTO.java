package com.coreerp.dto;

import java.util.List;

public class CompanyWithBankListDTO {
	
	private CompanyDTO companyDTO;
	
	private List<PartyBankMapDTO> partyBankMapDTOList;
	
	private List<Long> partyBankMapListDeletedIds;

	public CompanyDTO getCompanyDTO() {
		return companyDTO;
	}

	public void setCompanyDTO(CompanyDTO companyDTO) {
		this.companyDTO = companyDTO;
	}

	public List<PartyBankMapDTO> getPartyBankMapDTOList() {
		return partyBankMapDTOList;
	}

	public void setPartyBankMapDTOList(List<PartyBankMapDTO> partyBankMapDTOList) {
		this.partyBankMapDTOList = partyBankMapDTOList;
	}

	public List<Long> getPartyBankMapListDeletedIds() {
		return partyBankMapListDeletedIds;
	}

	public void setPartyBankMapListDeletedIds(List<Long> partyBankMapListDeletedIds) {
		this.partyBankMapListDeletedIds = partyBankMapListDeletedIds;
	}
	
	

}
