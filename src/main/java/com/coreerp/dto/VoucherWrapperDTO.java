package com.coreerp.dto;

import java.util.List;

public class VoucherWrapperDTO {

	private List<String> itemToBeRemove;
	private VoucherHeaderDTO voucherHeader;
	public List<String> getItemToBeRemove() {
		return itemToBeRemove;
	}
	public void setItemToBeRemove(List<String> itemToBeRemove) {
		this.itemToBeRemove = itemToBeRemove;
	}
	public VoucherHeaderDTO getVoucherHeader() {
		return voucherHeader;
	}
	public void setVoucherHeader(VoucherHeaderDTO voucherHeader) {
		this.voucherHeader = voucherHeader;
	}
	
	

	
}
