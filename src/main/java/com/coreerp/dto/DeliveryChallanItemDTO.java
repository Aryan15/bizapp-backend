package com.coreerp.dto;

import java.util.Date;

public class DeliveryChallanItemDTO {

	private String id;
	
	private String headerId;
	
	private Long materialId;

	private Integer slNo;
	
	private String materialName;
	
	private Double quantity;
	
	private String remarks;
	
	private String status;
	
	private String purchaseOrderHeaderId;
	
	private String purchaseOrderItemId;
	
	private Double price;
	
	private Double amount;
	
	private Long unitOfMeasurementId;
	
	private Long taxId;
	
	private Double taxRate;
	
	private String deliveryChallanType;
	
	private String deliveryChallanItemStatus;
	
	private String processId;
	
	private Double invoiceBalanceQuantity;
	
	private String batchCode;
	
	private String purchaseOrderNumber;
	
	private String batchCodeId;
	
	private String inDeliveryChallanNumber;
	
	private Date inDeliveryChallanDate;
	
	private Double sgstTaxPercentage;
	
	private Double sgstTaxAmount;
	
	private Double cgstTaxPercentage;
	
	private Double cgstTaxAmount;
	
	private Double igstTaxPercentage;
	
	private Double igstTaxAmount;
	
	private Double taxAmount;
	
    private String partNumber;
    private String hsnOrSac;
    private String partName;
    private String uom;
    private Integer inclusiveTax;
    private Double discountPercentage;
    private Double discountAmount;
    private Double amountAfterDiscount;
    private Double grnBalanceQuantity;
    private String sourceDeliveryChallanHeaderId;
    private String sourceDeliveryChallanItemId;
    private Double receivedQuantity;
    
    private Double incomingQuantity;
    private Double dcBalanceQuantity;
    private String processName;
    private String specification;
    private String sourceDeliveryChallanNumber;
    private Date sourceDeliveryChallanDate;
    private Integer isContainer;
	private String outName;
	private String outPartNumber;

	private String jwNoteItemId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getHeaderId() {
		return headerId;
	}

	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	
	
	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}



	public String getPurchaseOrderHeaderId() {
		return purchaseOrderHeaderId;
	}

	public void setPurchaseOrderHeaderId(String purchaseOrderHeaderId) {
		this.purchaseOrderHeaderId = purchaseOrderHeaderId;
	}

	

	public String getPurchaseOrderItemId() {
		return purchaseOrderItemId;
	}

	public void setPurchaseOrderItemId(String purchaseOrderItemId) {
		this.purchaseOrderItemId = purchaseOrderItemId;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Long getUnitOfMeasurementId() {
		return unitOfMeasurementId;
	}

	public void setUnitOfMeasurementId(Long unitOfMeasurementId) {
		this.unitOfMeasurementId = unitOfMeasurementId;
	}

	public Long getTaxId() {
		return taxId;
	}

	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public String getDeliveryChallanType() {
		return deliveryChallanType;
	}

	public void setDeliveryChallanType(String deliveryChallanType) {
		this.deliveryChallanType = deliveryChallanType;
	}

	public String getDeliveryChallanItemStatus() {
		return deliveryChallanItemStatus;
	}

	public void setDeliveryChallanItemStatus(String deliveryChallanItemStatus) {
		this.deliveryChallanItemStatus = deliveryChallanItemStatus;
	}

	
	

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public Double getInvoiceBalanceQuantity() {
		return invoiceBalanceQuantity;
	}

	public void setInvoiceBalanceQuantity(Double invoiceBalanceQuantity) {
		this.invoiceBalanceQuantity = invoiceBalanceQuantity;
	}

	public Double getGrnBalanceQuantity() {
		return grnBalanceQuantity;
	}

	public void setGrnBalanceQuantity(Double grnBalanceQuantity) {
		this.grnBalanceQuantity = grnBalanceQuantity;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public String getBatchCodeId() {
		return batchCodeId;
	}

	public void setBatchCodeId(String batchCodeId) {
		this.batchCodeId = batchCodeId;
	}

	public String getInDeliveryChallanNumber() {
		return inDeliveryChallanNumber;
	}

	public void setInDeliveryChallanNumber(String inDeliveryChallanNumber) {
		this.inDeliveryChallanNumber = inDeliveryChallanNumber;
	}

	public Date getInDeliveryChallanDate() {
		return inDeliveryChallanDate;
	}

	public void setInDeliveryChallanDate(Date inDeliveryChallanDate) {
		this.inDeliveryChallanDate = inDeliveryChallanDate;
	}


	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}

	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}


	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}

	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}


	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}

	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getHsnOrSac() {
		return hsnOrSac;
	}

	public void setHsnOrSac(String hsnOrSac) {
		this.hsnOrSac = hsnOrSac;
	}

	public String getPartName() {
		return partName;
	}

	public void setPartName(String partName) {
		this.partName = partName;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getSgstTaxPercentage() {
		return sgstTaxPercentage;
	}

	public void setSgstTaxPercentage(Double sgstTaxPercentage) {
		this.sgstTaxPercentage = sgstTaxPercentage;
	}

	public Double getCgstTaxPercentage() {
		return cgstTaxPercentage;
	}

	public void setCgstTaxPercentage(Double cgstTaxPercentage) {
		this.cgstTaxPercentage = cgstTaxPercentage;
	}

	public Double getIgstTaxPercentage() {
		return igstTaxPercentage;
	}

	public void setIgstTaxPercentage(Double igstTaxPercentage) {
		this.igstTaxPercentage = igstTaxPercentage;
	}

	
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}

	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	
	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}

	public void setAmountAfterDiscount(Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}
	
	

	

	public String getSourceDeliveryChallanHeaderId() {
		return sourceDeliveryChallanHeaderId;
	}

	public void setSourceDeliveryChallanHeaderId(String sourceDeliveryChallanHeaderId) {
		this.sourceDeliveryChallanHeaderId = sourceDeliveryChallanHeaderId;
	}

	public String getSourceDeliveryChallanItemId() {
		return sourceDeliveryChallanItemId;
	}

	public void setSourceDeliveryChallanItemId(String sourceDeliveryChallanItemId) {
		this.sourceDeliveryChallanItemId = sourceDeliveryChallanItemId;
	}
	

	
	public Double getReceivedQuantity() {
		return receivedQuantity;
	}

	public void setReceivedQuantity(Double receivedQuantity) {
		this.receivedQuantity = receivedQuantity;
	}



	public Double getIncomingQuantity() {
		return incomingQuantity;
	}

	public void setIncomingQuantity(Double incomingQuantity) {
		this.incomingQuantity = incomingQuantity;
	}
	
	

	public Double getDcBalanceQuantity() {
		return dcBalanceQuantity;
	}

	public void setDcBalanceQuantity(Double dcBalanceQuantity) {
		this.dcBalanceQuantity = dcBalanceQuantity;
	}

	@Override
	public String toString() {
		return "DeliveryChallanItemDTO [id=" + id + ", headerId=" + headerId + ", materialId=" + materialId
				+ ", materialName=" + materialName + ", quantity=" + quantity + ", remarks=" + remarks + ", status="
				+ status + ", purchaseOrderHeaderId=" + purchaseOrderHeaderId + ", purchaseOrderItemId="
				+ purchaseOrderItemId + ", price=" + price + ", amount=" + amount + ", unitOfMeasurementId="
				+ unitOfMeasurementId + ", taxId=" + taxId + ", taxRate=" + taxRate + ", deliveryChallanType="
				+ deliveryChallanType + ", deliveryChallanItemStatus=" + deliveryChallanItemStatus + ", process="
				+ processId + ", invoiceBalanceQuantity=" + invoiceBalanceQuantity + ", batchCode=" + batchCode
				+ ", purchaseOrderNumber=" + purchaseOrderNumber + ", batchCodeId=" + batchCodeId
				+ ", inDeliveryChallanNumber=" + inDeliveryChallanNumber + ", inDeliveryChallanDate="
				+ inDeliveryChallanDate + ", sgstTaxPercentage=" + sgstTaxPercentage + ", sgstTaxAmount="
				+ sgstTaxAmount + ", cgstTaxPercentage=" + cgstTaxPercentage + ", cgstTaxAmount=" + cgstTaxAmount
				+ ", igstTaxPercentage=" + igstTaxPercentage + ", igstTaxAmount=" + igstTaxAmount + ", taxAmount="
				+ taxAmount + ", partNumber=" + partNumber + ", hsnOrSac=" + hsnOrSac + ", partName=" + partName
				+ ", uom=" + uom + ", inclusiveTax=" + inclusiveTax + ", discountPercentage=" + discountPercentage
				+ ", discountAmount=" + discountAmount + ", amountAfterDiscount=" + amountAfterDiscount + "]";
	}

	/**
	 * @return the slNo
	 */
	public Integer getSlNo() {
		return slNo;
	}

	/**
	 * @param slNo the slNo to set
	 */
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getSourceDeliveryChallanNumber() {
		return sourceDeliveryChallanNumber;
	}

	public void setSourceDeliveryChallanNumber(String sourceDeliveryChallanNumber) {
		this.sourceDeliveryChallanNumber = sourceDeliveryChallanNumber;
	}

	public Date getSourceDeliveryChallanDate() {
		return sourceDeliveryChallanDate;
	}

	public void setSourceDeliveryChallanDate(Date sourceDeliveryChallanDate) {
		this.sourceDeliveryChallanDate = sourceDeliveryChallanDate;
	}

	public Integer getIsContainer() {
		return isContainer;
	}

	public void setIsContainer(Integer isContainer) {
		this.isContainer = isContainer;
	}

	public String getOutName() {
		return outName;
	}

	public void setOutName(String outName) {
		this.outName = outName;
	}

	public String getOutPartNumber() {
		return outPartNumber;
	}

	public void setOutPartNumber(String outPartNumber) {
		this.outPartNumber = outPartNumber;
	}

	public String getJwNoteItemId() {
		return jwNoteItemId;
	}

	public void setJwNoteItemId(String jwNoteItemId) {
		this.jwNoteItemId = jwNoteItemId;
	}
}
