package com.coreerp.dto;

public class TransactionStatusRule {

	private Integer ruleId;
	
	private String transactionType;
	
	private String transactionId;
	
	private Double quantity;
	
	private Double dcBalanceQuantity;
	
	private Double grnBalanceQuantity;
	
	private Double invoiceBalanceQuantity;
	
	private String latestStatus;	
	
	private String currentStatus;
			
	private String sourceTransactionType;
	
	private String sourceTransactionId;
	
	private String sourceTransactionStatus;
	
	private Double sourceQuantity;
		
	private Double sourcePrevQuantity;
	
	private String newStatus;
	
	private Boolean itemUpdateRequired;

	public Integer getRuleId() {
		return ruleId;
	}

	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getDcBalanceQuantity() {
		return dcBalanceQuantity;
	}

	public void setDcBalanceQuantity(Double dcBalanceQuantity) {
		this.dcBalanceQuantity = dcBalanceQuantity;
	}

	public Double getGrnBalanceQuantity() {
		return grnBalanceQuantity;
	}

	public void setGrnBalanceQuantity(Double grnBalanceQuantity) {
		this.grnBalanceQuantity = grnBalanceQuantity;
	}

	public Double getInvoiceBalanceQuantity() {
		return invoiceBalanceQuantity;
	}

	public void setInvoiceBalanceQuantity(Double invoiceBalanceQuantity) {
		this.invoiceBalanceQuantity = invoiceBalanceQuantity;
	}

	public String getLatestStatus() {
		return latestStatus;
	}

	public void setLatestStatus(String latestStatus) {
		this.latestStatus = latestStatus;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getSourceTransactionType() {
		return sourceTransactionType;
	}

	public void setSourceTransactionType(String sourceTransactionType) {
		this.sourceTransactionType = sourceTransactionType;
	}

	public String getSourceTransactionId() {
		return sourceTransactionId;
	}

	public void setSourceTransactionId(String sourceTransactionId) {
		this.sourceTransactionId = sourceTransactionId;
	}

	public String getSourceTransactionStatus() {
		return sourceTransactionStatus;
	}

	public void setSourceTransactionStatus(String sourceTransactionStatus) {
		this.sourceTransactionStatus = sourceTransactionStatus;
	}

	public Double getSourceQuantity() {
		return sourceQuantity;
	}

	public void setSourceQuantity(Double sourceQuantity) {
		this.sourceQuantity = sourceQuantity;
	}

	public Double getSourcePrevQuantity() {
		return sourcePrevQuantity;
	}

	public void setSourcePrevQuantity(Double sourcePrevQuantity) {
		this.sourcePrevQuantity = sourcePrevQuantity;
	}

	public String getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(String newStatus) {
		this.newStatus = newStatus;
	}

	public Boolean getItemUpdateRequired() {
		return itemUpdateRequired;
	}

	public void setItemUpdateRequired(Boolean itemUpdateRequired) {
		this.itemUpdateRequired = itemUpdateRequired;
	}

	@Override
	public String toString() {
		return "TransactionStatusRule [ruleId=" + ruleId + ", transactionType=" + transactionType + ", transactionId="
				+ transactionId + ", quantity=" + quantity + ", dcBalanceQuantity=" + dcBalanceQuantity
				+ ", grnBalanceQuantity=" + grnBalanceQuantity + ", invoiceBalanceQuantity=" + invoiceBalanceQuantity
				+ ", latestStatus=" + latestStatus + ", currentStatus=" + currentStatus + ", sourceTransactionType="
				+ sourceTransactionType + ", sourceTransactionId=" + sourceTransactionId + ", sourceTransactionStatus="
				+ sourceTransactionStatus + ", sourceQuantity=" + sourceQuantity + ", sourcePrevQuantity="
				+ sourcePrevQuantity + ", newStatus=" + newStatus + ", itemUpdateRequired=" + itemUpdateRequired + "]";
	}

	
	
	
}
