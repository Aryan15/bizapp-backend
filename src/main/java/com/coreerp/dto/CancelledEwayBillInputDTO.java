package com.coreerp.dto;

public class CancelledEwayBillInputDTO {

    private String EwbNo;
    private String CancelledReason;
    private  String CancelledRemarks;

    public String getEwbNo() {
        return EwbNo;
    }

    public void setEwbNo(String ewbNo) {
        EwbNo = ewbNo;
    }

    public String getCancelledReason() {
        return CancelledReason;
    }

    public void setCancelledReason(String cancelledReason) {
        CancelledReason = cancelledReason;
    }

    public String getCancelledRemarks() {
        return CancelledRemarks;
    }

    public void setCancelledRemarks(String cancelledRemarks) {
        CancelledRemarks = cancelledRemarks;
    }
}
