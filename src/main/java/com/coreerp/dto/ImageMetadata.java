package com.coreerp.dto;

public class ImageMetadata {
	
	private Integer height;
	private Integer width;
	private Integer size;
	private String name;
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "ImageMetadata [height=" + height + ", width=" + width + ", size=" + size + ", name=" + name + "]";
	}
	
	

}
