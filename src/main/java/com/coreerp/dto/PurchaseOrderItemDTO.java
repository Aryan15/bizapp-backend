package com.coreerp.dto;

public class PurchaseOrderItemDTO {

	private String id;
	private String headerId;
    private Integer slNo;
    private Long materialId;
    private Double quantity;
    private Double dcBalanceQuantity;
    private Double price;
    private Double amount;
    private Double discountPercentage;
    private Double discountAmount;
    private Double amountAfterDiscount;
    private Double transportationAmount;
    private Double cessPercentage;
    private Double cessAmount;
    private Double igstTaxPercentage;
    private Double igstTaxAmount;
    private Double sgstTaxPercentage;
    private Double sgstTaxAmount;
    private Double cgstTaxPercentage;
    private Double cgstTaxAmount;
    private Double amountAfterTax;
    private String remarks;
	private String quotationHeaderId;
	private String quotationItemId;
	private Long unitOfMeasurementId;
	private Long taxId;
    private String partNumber;
    private String hsnOrSac;
    private String partName;
    private String uom;
    private Integer inclusiveTax;
    private Double invoiceBalanceQuantity;
    private String processId;
    private String processName;
    private String specification;
    private Integer isContainer;
	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getHeaderId() {
		return headerId;
	}
	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	public Integer getSlNo() {
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	public Long getMaterialId() {
		return materialId;
	}
	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	

	
	public Double getDcBalanceQuantity() {
		return dcBalanceQuantity;
	}
	public void setDcBalanceQuantity(Double dcBalanceQuantity) {
		this.dcBalanceQuantity = dcBalanceQuantity;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}
	public void setAmountAfterDiscount(Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}
	public Double getTransportationAmount() {
		return transportationAmount;
	}
	public void setTransportationAmount(Double transportationAmount) {
		this.transportationAmount = transportationAmount;
	}
	public Double getCessPercentage() {
		return cessPercentage;
	}
	public void setCessPercentage(Double cessPercentage) {
		this.cessPercentage = cessPercentage;
	}
	public Double getCessAmount() {
		return cessAmount;
	}
	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}
	public Double getIgstTaxPercentage() {
		return igstTaxPercentage;
	}
	public void setIgstTaxPercentage(Double igstTaxPercentage) {
		this.igstTaxPercentage = igstTaxPercentage;
	}
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}
	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}
	public Double getSgstTaxPercentage() {
		return sgstTaxPercentage;
	}
	public void setSgstTaxPercentage(Double sgstTaxPercentage) {
		this.sgstTaxPercentage = sgstTaxPercentage;
	}
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}
	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}
	public Double getCgstTaxPercentage() {
		return cgstTaxPercentage;
	}
	public void setCgstTaxPercentage(Double cgstTaxPercentage) {
		this.cgstTaxPercentage = cgstTaxPercentage;
	}
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}
	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}
	public Double getAmountAfterTax() {
		return amountAfterTax;
	}
	public void setAmountAfterTax(Double amountAfterTax) {
		this.amountAfterTax = amountAfterTax;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getQuotationHeaderId() {
		return quotationHeaderId;
	}
	public void setQuotationHeaderId(String quotationHeaderId) {
		this.quotationHeaderId = quotationHeaderId;
	}

	
	public String getQuotationItemId() {
		return quotationItemId;
	}
	public void setQuotationItemId(String quotationItemId) {
		this.quotationItemId = quotationItemId;
	}
	public Long getUnitOfMeasurementId() {
		return unitOfMeasurementId;
	}
	public void setUnitOfMeasurementId(Long unitOfMeasurementId) {
		this.unitOfMeasurementId = unitOfMeasurementId;
	}
	public Long getTaxId() {
		return taxId;
	}
	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getHsnOrSac() {
		return hsnOrSac;
	}
	public void setHsnOrSac(String hsnOrSac) {
		this.hsnOrSac = hsnOrSac;
	}
	public String getPartName() {
		return partName;
	}
	public void setPartName(String partName) {
		this.partName = partName;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	
	
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}
	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}
	
	
	public Double getInvoiceBalanceQuantity() {
		return invoiceBalanceQuantity;
	}
	public void setInvoiceBalanceQuantity(Double invoiceBalanceQuantity) {
		this.invoiceBalanceQuantity = invoiceBalanceQuantity;
	}
	@Override
	public String toString() {
		return "PurchaseOrderItemDTO [id=" + id + ", headerId=" + headerId + ", slNo=" + slNo + ", materialId="
				+ materialId + ", quantity=" + quantity + ", price=" + price + ", amount=" + amount
				+ ", discountPercentage=" + discountPercentage + ", discountAmount=" + discountAmount
				+ ", amountAfterDiscount=" + amountAfterDiscount + ", transportationAmount=" + transportationAmount
				+ ", cessPercentage=" + cessPercentage + ", cessAmount=" + cessAmount + ", igstTaxPercentage="
				+ igstTaxPercentage + ", igstTaxAmount=" + igstTaxAmount + ", sgstTaxPercentage=" + sgstTaxPercentage
				+ ", sgstTaxAmount=" + sgstTaxAmount + ", cgstTaxPercentage=" + cgstTaxPercentage + ", cgstTaxAmount="
				+ cgstTaxAmount + ", amountAfterTax=" + amountAfterTax + ", remarks=" + remarks + ", quotationHeaderId="
				+ quotationHeaderId + ", unitOfMeasurementId=" + unitOfMeasurementId + ", taxId=" + taxId
				+ ", partNumber=" + partNumber + ", hsnOrSac=" + hsnOrSac + ", partName=" + partName + ", uom=" + uom
				+ "]";
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getSpecification() {
		return specification;
	}
	public void setSpecification(String specification) {
		this.specification = specification;
	}
	public Integer getIsContainer() {
		return isContainer;
	}
	public void setIsContainer(Integer isContainer) {
		this.isContainer = isContainer;
	}

	

	
	

}
