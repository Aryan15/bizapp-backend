package com.coreerp.dto;

public class MaterialStockCheckDTO {
	private Long id;
	private String deleted;
	private String name;
	private String partNumber;
	private Double itemQuantity;
	private Double minimumStock;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public Double getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(Double itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	public Double getMinimumStock() {
		return minimumStock;
	}
	public void setMinimumStock(Double minimumStock) {
		this.minimumStock = minimumStock;
	}
	@Override
	public String toString() {
		return "MaterialStockCheckDTO [id=" + id + ", deleted=" + deleted + ", name=" + name + ", partNumber="
				+ partNumber + ", itemQuantity=" + itemQuantity + ", minimumStock=" + minimumStock + "]";
	}
	
	
}
