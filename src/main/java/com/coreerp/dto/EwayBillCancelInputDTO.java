package com.coreerp.dto;

import java.util.List;

public class EwayBillCancelInputDTO {

    private String userCode ;
    private String clientCode ;
    private String password ;
    private List<CancelledEwayBillInputDTO> cancelledewblist;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<CancelledEwayBillInputDTO> getCancelledewblist() {
        return cancelledewblist;
    }

    public void setCancelledewblist(List<CancelledEwayBillInputDTO> cancelledewblist) {
        this.cancelledewblist = cancelledewblist;
    }
}
