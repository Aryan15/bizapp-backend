package com.coreerp.dto;

import java.util.List;

public class RecentQuotationDTO {

	private List<QuotationDTO> quotationHeaders;
	
	private Long totalCount;

	public List<QuotationDTO> getQuotationHeaders() {
		return quotationHeaders;
	}

	public void setQuotationHeaders(List<QuotationDTO> quotationHeaders) {
		this.quotationHeaders = quotationHeaders;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
	
}
