package com.coreerp.dto;

import java.util.List;

public class PayableReceivableWrapperDTO {

	private PayableReceivableHeaderDTO payableReceivableHeader;
	
	private List<String> itemToBeRemove;

	public PayableReceivableHeaderDTO getPayableReceivableHeader() {
		return payableReceivableHeader;
	}

	public void setPayableReceivableHeader(PayableReceivableHeaderDTO payableReceivableHeader) {
		this.payableReceivableHeader = payableReceivableHeader;
	}

	public List<String> getItemToBeRemove() {
		return itemToBeRemove;
	}

	public void setItemToBeRemove(List<String> itemToBeRemove) {
		this.itemToBeRemove = itemToBeRemove;
	}
	
	
}
