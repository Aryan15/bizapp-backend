package com.coreerp.dto;

import java.util.List;

public class UserRoleActivityDTO {
	
	private Long arbId;
	private Long roleId;
	private Integer isRoleAssigned;
	private Long activityId;
	private Integer activityOrder;
	private String activityName;
	private List<UserRoleMenuDTO> submenus;
	private Integer permissionToApprove;
	private Integer permissionToCancel;
	private Integer permissionToCreate;
	private Integer permissionToDelete;
	private Integer permissionToPrint;
	private Integer permissionToUpdate;
	
	
	
	
//	public UserRoleActivityDTO(Long roleId, Integer isRoleAssigned, Long activityId, Integer activityOrder,
//			String activityName, List<UserRoleMenuDTO> submenus, Integer permissionToApprove,
//			Integer permissionToCancel, Integer permissionToCreate, Integer permissionToDelete,
//			Integer permissionToPrint, Integer permissionToUpdate) {
//		super();
//		this.roleId = roleId;
//		this.isRoleAssigned = isRoleAssigned;
//		this.activityId = activityId;
//		this.activityOrder = activityOrder;
//		this.activityName = activityName;
//		this.submenus = submenus;
//		this.permissionToApprove = permissionToApprove;
//		this.permissionToCancel = permissionToCancel;
//		this.permissionToCreate = permissionToCreate;
//		this.permissionToDelete = permissionToDelete;
//		this.permissionToPrint = permissionToPrint;
//		this.permissionToUpdate = permissionToUpdate;
//	}
	
	public UserRoleActivityDTO() {
		super();
	}

	
	public Long getArbId() {
		return arbId;
	}


	public void setArbId(Long arbId) {
		this.arbId = arbId;
	}


	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Integer getIsRoleAssigned() {
		return isRoleAssigned;
	}
	public void setIsRoleAssigned(Integer isRoleAssigned) {
		this.isRoleAssigned = isRoleAssigned;
	}
	public Long getActivityId() {
		return activityId;
	}
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}
	public Integer getActivityOrder() {
		return activityOrder;
	}
	public void setActivityOrder(Integer activityOrder) {
		this.activityOrder = activityOrder;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public List<UserRoleMenuDTO> getSubmenus() {
		return submenus;
	}
	public void setSubmenus(List<UserRoleMenuDTO> submenus) {
		this.submenus = submenus;
	}
	public Integer getPermissionToApprove() {
		return permissionToApprove;
	}
	public void setPermissionToApprove(Integer permissionToApprove) {
		this.permissionToApprove = permissionToApprove;
	}
	public Integer getPermissionToCancel() {
		return permissionToCancel;
	}
	public void setPermissionToCancel(Integer permissionToCancel) {
		this.permissionToCancel = permissionToCancel;
	}
	public Integer getPermissionToCreate() {
		return permissionToCreate;
	}
	public void setPermissionToCreate(Integer permissionToCreate) {
		this.permissionToCreate = permissionToCreate;
	}
	public Integer getPermissionToDelete() {
		return permissionToDelete;
	}
	public void setPermissionToDelete(Integer permissionToDelete) {
		this.permissionToDelete = permissionToDelete;
	}
	public Integer getPermissionToPrint() {
		return permissionToPrint;
	}
	public void setPermissionToPrint(Integer permissionToPrint) {
		this.permissionToPrint = permissionToPrint;
	}
	public Integer getPermissionToUpdate() {
		return permissionToUpdate;
	}
	public void setPermissionToUpdate(Integer permissionToUpdate) {
		this.permissionToUpdate = permissionToUpdate;
	}
	@Override
	public String toString() {
		return "UserRoleActivityDTO [roleId=" + roleId + ", isRoleAssigned=" + isRoleAssigned + ", activityId="
				+ activityId + ", activityOrder=" + activityOrder + ", activityName=" + activityName + ", submenus="
				+ submenus + ", permissionToApprove=" + permissionToApprove + ", permissionToCancel="
				+ permissionToCancel + ", permissionToCreate=" + permissionToCreate + ", permissionToDelete="
				+ permissionToDelete + ", permissionToPrint=" + permissionToPrint + ", permissionToUpdate="
				+ permissionToUpdate + "]";
	}
	
	

}
