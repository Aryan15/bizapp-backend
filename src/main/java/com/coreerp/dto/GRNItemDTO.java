package com.coreerp.dto;

public class GRNItemDTO {

	private String id;
	private String grnHeaderId;
	private Long materialId;
	private Integer slNo;
	private String materialName;
	private Double deliveryChallanQuantity;
	private Double acceptedQuantity;
	private Double rejectedQuantity;
	private String purchaseOrderHeaderId;
	private Double invoiceBalanceQuantity;
	
	private String remarks;
	private Double amount;
	private String deliveryChallanHeaderId;
	private String deliveryChallanItemId;
	private Long unitOfMeasurementId;
	private Double unitPrice;
	private String materialSpecification;
	private Long taxId;	
	private Double discountPercentage;
	private Double discountAmount;
	private Double amountAfterDiscount;
	private Double sgstTaxPercentage;
	private Double sgstTaxAmount;
	private Double cgstTaxPercentage;
	private Double cgstTaxAmount;
	private Double igstTaxPercentage;
	private Double igstTaxAmount;
	private Double taxAmount;
	private String partNumber;
	private String hsnOrSac;
	private String partName;
	private String uom;
	private Integer inclusiveTax;
	private String status;
	private String grnItemStatus;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGrnHeaderId() {
		return grnHeaderId;
	}
	public void setGrnHeaderId(String grnHeaderId) {
		this.grnHeaderId = grnHeaderId;
	}
	public Long getMaterialId() {
		return materialId;
	}
	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}
	
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public Double getDeliveryChallanQuantity() {
		return deliveryChallanQuantity;
	}
	public void setDeliveryChallanQuantity(Double deliveryChallanQuantity) {
		this.deliveryChallanQuantity = deliveryChallanQuantity;
	}
	public Double getAcceptedQuantity() {
		return acceptedQuantity;
	}
	public void setAcceptedQuantity(Double acceptedQuantity) {
		this.acceptedQuantity = acceptedQuantity;
	}
	public Double getRejectedQuantity() {
		return rejectedQuantity;
	}
	public void setRejectedQuantity(Double rejectedQuantity) {
		this.rejectedQuantity = rejectedQuantity;
	}
	public String getPurchaseOrderHeaderId() {
		return purchaseOrderHeaderId;
	}
	public void setPurchaseOrderHeaderId(String purchaseOrderHeaderId) {
		this.purchaseOrderHeaderId = purchaseOrderHeaderId;
	}
	
	public Double getInvoiceBalanceQuantity() {
		return invoiceBalanceQuantity;
	}
	public void setInvoiceBalanceQuantity(Double invoiceBalanceQuantity) {
		this.invoiceBalanceQuantity = invoiceBalanceQuantity;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getDeliveryChallanHeaderId() {
		return deliveryChallanHeaderId;
	}
	public void setDeliveryChallanHeaderId(String deliveryChallanHeaderId) {
		this.deliveryChallanHeaderId = deliveryChallanHeaderId;
	}
	public String getDeliveryChallanItemId() {
		return deliveryChallanItemId;
	}
	public void setDeliveryChallanItemId(String deliveryChallanItemId) {
		this.deliveryChallanItemId = deliveryChallanItemId;
	}
	public Long getUnitOfMeasurementId() {
		return unitOfMeasurementId;
	}
	public void setUnitOfMeasurementId(Long unitOfMeasurementId) {
		this.unitOfMeasurementId = unitOfMeasurementId;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getMaterialSpecification() {
		return materialSpecification;
	}
	public void setMaterialSpecification(String materialSpecification) {
		this.materialSpecification = materialSpecification;
	}
	public Long getTaxId() {
		return taxId;
	}
	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}
	public Double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}
	public void setAmountAfterDiscount(Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}
	public Double getSgstTaxPercentage() {
		return sgstTaxPercentage;
	}
	public void setSgstTaxPercentage(Double sgstTaxPercentage) {
		this.sgstTaxPercentage = sgstTaxPercentage;
	}
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}
	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}
	public Double getCgstTaxPercentage() {
		return cgstTaxPercentage;
	}
	public void setCgstTaxPercentage(Double cgstTaxPercentage) {
		this.cgstTaxPercentage = cgstTaxPercentage;
	}
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}
	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}
	public Double getIgstTaxPercentage() {
		return igstTaxPercentage;
	}
	public void setIgstTaxPercentage(Double igstTaxPercentage) {
		this.igstTaxPercentage = igstTaxPercentage;
	}
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}
	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}
	public Double getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getHsnOrSac() {
		return hsnOrSac;
	}
	public void setHsnOrSac(String hsnOrSac) {
		this.hsnOrSac = hsnOrSac;
	}
	public String getPartName() {
		return partName;
	}
	public void setPartName(String partName) {
		this.partName = partName;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}
	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	/**
	 * @return the slNo
	 */
	public Integer getSlNo() {
		return slNo;
	}

	/**
	 * @param slNo the slNo to set
	 */
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getGrnItemStatus() {
		return grnItemStatus;
	}
	public void setGrnItemStatus(String grnItemStatus) {
		this.grnItemStatus = grnItemStatus;
	}
	
	
	

}
