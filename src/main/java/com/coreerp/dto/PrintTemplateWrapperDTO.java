package com.coreerp.dto;

import java.util.List;

import com.coreerp.model.NumberRangeConfiguration;

public class PrintTemplateWrapperDTO {

	private List<NumberRangeConfigurationDTO> templates;

	public List<NumberRangeConfigurationDTO> getTemplates() {
		return templates;
	}

	public void setTemplates(List<NumberRangeConfigurationDTO> templates) {
		this.templates = templates;
	}
	
	
	
}
