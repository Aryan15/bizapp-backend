package com.coreerp.dto;

import com.coreerp.model.HelpVideoModel;
import com.coreerp.reports.model.InvoiceItemReportModel;

import java.util.List;

public class HelpVideoDTO {

    private List<HelpVideoModel> helpVideo;

    private Long totalCount;

    public List<HelpVideoModel> getHelpVideo() {
        return helpVideo;
    }

    public void setHelpVideo(List<HelpVideoModel> helpVideo) {
        this.helpVideo = helpVideo;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
