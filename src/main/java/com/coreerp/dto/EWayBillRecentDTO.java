package com.coreerp.dto;

import com.coreerp.reports.model.EWayBillReport;
import com.coreerp.reports.model.EwayBillRecentReport;

import java.util.List;

public class EWayBillRecentDTO {
    private List<EwayBillRecentReport> eWayBillRecentReports;

    private Long totalCount;

    public List<EwayBillRecentReport> geteWayBillRecentReports() {
        return eWayBillRecentReports;
    }

    public void seteWayBillRecentReports(List<EwayBillRecentReport> eWayBillRecentReports) {
        this.eWayBillRecentReports = eWayBillRecentReports;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
