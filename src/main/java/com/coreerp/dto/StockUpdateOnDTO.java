package com.coreerp.dto;

public class StockUpdateOnDTO {


	private Long id;
	private String deleted;
	private Long transactionTypeId;
	private String transactionTypeName;
	//private Long userTypeId;
	private Long partyTypeId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public Long getTransactionTypeId() {
		return transactionTypeId;
	}
	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}
	
	
	
	public String getTransactionTypeName() {
		return transactionTypeName;
	}
	public void setTransactionTypeName(String transactionTypeName) {
		this.transactionTypeName = transactionTypeName;
	}
//	public Long getUserTypeId() {
//		return userTypeId;
//	}
//	public void setUserTypeId(Long userTypeId) {
//		this.userTypeId = userTypeId;
//	}
	
	public Long getPartyTypeId() {
		return partyTypeId;
	}
	public void setPartyTypeId(Long partyTypeId) {
		this.partyTypeId = partyTypeId;
	}
	

	@Override
	public String toString() {
		return "StockUpdateOnDTO [id=" + id + ", deleted=" + deleted + ", transactionTypeId=" + transactionTypeId
				+ ", transactionTypeName=" + transactionTypeName + ", partyTypeId=" + partyTypeId + "]";
	}
}
