package com.coreerp.dto;

import java.util.List;

public class PartyRecentDTO {

	private List<PartyWithPriceListDTO> partyWithPriceLists;
	
	private Long totalCount;

	public List<PartyWithPriceListDTO> getPartyWithPriceLists() {
		return partyWithPriceLists;
	}

	public void setPartyWithPriceLists(List<PartyWithPriceListDTO> partyWithPriceLists) {
		this.partyWithPriceLists = partyWithPriceLists;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
	
}
