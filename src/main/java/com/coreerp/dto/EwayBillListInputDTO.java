package com.coreerp.dto;

public class EwayBillListInputDTO {

    private String ewbNo;

    public String getEwbNo() {
        return ewbNo;
    }

    public void setEwbNo(String ewbNo) {
        this.ewbNo = ewbNo;
    }

    @Override
    public String toString() {
        return "EwayBillListInputDTO{" +
                "ewbNo='" + ewbNo + '\'' +
                '}';
    }
}
