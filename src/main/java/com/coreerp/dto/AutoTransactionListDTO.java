package com.coreerp.dto;

public class AutoTransactionListDTO {


    private AutoTransactionGenerationDTO autoTransactionGenerationDTO;

    public AutoTransactionGenerationDTO getAutoTransactionGenerationDTO() {
        return autoTransactionGenerationDTO;
    }

    public void setAutoTransactionGenerationDTO(AutoTransactionGenerationDTO autoTransactionGenerationDTO) {
        this.autoTransactionGenerationDTO = autoTransactionGenerationDTO;
    }
}
