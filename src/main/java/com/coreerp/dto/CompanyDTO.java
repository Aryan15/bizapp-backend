package com.coreerp.dto;

import java.util.Date;
import java.util.List;

import com.coreerp.model.Address;

public class CompanyDTO {

	private Long id;
	
	private String name;
	
	private String address;
	
	private Long cityId;
	
	private Long stateId;
	
	private Long countryId;
	
	private Long gstRegistrationTypeId;
	
	private String gstRegistrationName;
	
	private String gstNumber;
	
	private List<Long> materialIds;
	
	private String deleted;
	
	private String pinCode;
	
	private String primaryTelephone;
	
	private String secondaryTelephone;
	
	private String tinNumber;
	
	private Date tinDate;
	
	private String faxNumber;
	
	private String contactPersonName;
	
	private String primaryMobile;
	
	private String secondaryMobile;
	
	private String email;
	
	private String website;
	
	private String contactPersonNumber;
	
	private Long statusId;
	
	private String panNumber;
	
	private String panDate;
	
	private String companyLogoPath;

	private List<Address> addressesListDTO;
	
	private String stateCode;
	
	private String stateName;
	
	private String tagLine;
	
	private String bank;
	private String branch;
	private String account;
	private String ifsc;
	private Long companyCurrencyId;

	public long getBankAdCode() {
		return bankAdCode;
	}

	public void setBankAdCode(long bankAdCode) {
		this.bankAdCode = bankAdCode;
	}

	private long bankAdCode;
	private String ceritificateImagePath;
	private String signatureImagePath;
	private String msmeNumber;

	private String cinNumber;
	private String subject;
	private String iecCode;
	private Long creditLimit;
	private String insuranceType;
    private String insuranceRef;
    private String financePerson;
    private String financeEmail;

	public CompanyDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}
	
	

	public Long getGstRegistrationTypeId() {
		return gstRegistrationTypeId;
	}

	public void setGstRegistrationTypeId(Long gstRegistrationTypeId) {
		this.gstRegistrationTypeId = gstRegistrationTypeId;
	}
	
	

	public String getGstRegistrationName() {
		return gstRegistrationName;
	}

	public void setGstRegistrationName(String gstRegistrationName) {
		this.gstRegistrationName = gstRegistrationName;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public List<Long> getMaterialIds() {
		return materialIds;
	}

	public void setMaterialIds(List<Long> materialIds) {
		this.materialIds = materialIds;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getPrimaryTelephone() {
		return primaryTelephone;
	}

	public void setPrimaryTelephone(String primaryTelephone) {
		this.primaryTelephone = primaryTelephone;
	}

	public String getSecondaryTelephone() {
		return secondaryTelephone;
	}

	public void setSecondaryTelephone(String secondaryTelephone) {
		this.secondaryTelephone = secondaryTelephone;
	}

	public String getTinNumber() {
		return tinNumber;
	}

	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}

	public Date getTinDate() {
		return tinDate;
	}

	public void setTinDate(Date tinDate) {
		this.tinDate = tinDate;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getPrimaryMobile() {
		return primaryMobile;
	}

	public void setPrimaryMobile(String primaryMobile) {
		this.primaryMobile = primaryMobile;
	}

	public String getSecondaryMobile() {
		return secondaryMobile;
	}

	public void setSecondaryMobile(String secondaryMobile) {
		this.secondaryMobile = secondaryMobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getContactPersonNumber() {
		return contactPersonNumber;
	}

	public void setContactPersonNumber(String contactPersonNumber) {
		this.contactPersonNumber = contactPersonNumber;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getPanDate() {
		return panDate;
	}

	public void setPanDate(String panDate) {
		this.panDate = panDate;
	}

	
	
	public String getCompanyLogoPath() {
		return companyLogoPath;
	}

	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}

	public List<Address> getAddressesListDTO() {
		return addressesListDTO;
	}

	public void setAddressesListDTO(List<Address> addressesListDTO) {
		this.addressesListDTO = addressesListDTO;
	}
	
	

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}



	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getCeritificateImagePath() {
		return ceritificateImagePath;
	}

	public void setCeritificateImagePath(String ceritificateImagePath) {
		this.ceritificateImagePath = ceritificateImagePath;
	}

	public String getTagLine() {
		return tagLine;
	}

	public void setTagLine(String tagLine) {
		this.tagLine = tagLine;
	}

	public String getSignatureImagePath() {
		return signatureImagePath;
	}

	public void setSignatureImagePath(String signatureImagePath) {
		this.signatureImagePath = signatureImagePath;
	}


	public String getMsmeNumber() {
		return msmeNumber;
	}

	public void setMsmeNumber(String msmeNumber) {
		this.msmeNumber = msmeNumber;
	}

	public String getCinNumber() {
		return cinNumber;
	}

	public void setCinNumber(String cinNumber) {
		this.cinNumber = cinNumber;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getIecCode() {
		return iecCode;
	}

	public void setIecCode(String iecCode) {
		this.iecCode = iecCode;
	}

	public Long getCompanyCurrencyId() {
		return companyCurrencyId;
	}

	public Long getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(Long creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getInsuranceRef() {
		return insuranceRef;
	}

	public void setInsuranceRef(String insuranceRef) {
		this.insuranceRef = insuranceRef;
	}

	public String getFinancePerson() {
		return financePerson;
	}

	public void setFinancePerson(String financePerson) {
		this.financePerson = financePerson;
	}

	public String getFinanceEmail() {
		return financeEmail;
	}

	public void setFinanceEmail(String financeEmail) {
		this.financeEmail = financeEmail;
	}

	public void setCompanyCurrencyId(Long companyCurrencyId) {
		this.companyCurrencyId = companyCurrencyId;
	}

	@Override
	public String toString() {
		return "CompanyDTO{" +
				"id=" + id +
				", name='" + name + '\'' +
				", address='" + address + '\'' +
				", cityId=" + cityId +
				", stateId=" + stateId +
				", countryId=" + countryId +
				", gstRegistrationTypeId=" + gstRegistrationTypeId +
				", gstRegistrationName='" + gstRegistrationName + '\'' +
				", gstNumber='" + gstNumber + '\'' +
				", materialIds=" + materialIds +
				", deleted='" + deleted + '\'' +
				", pinCode='" + pinCode + '\'' +
				", primaryTelephone='" + primaryTelephone + '\'' +
				", secondaryTelephone='" + secondaryTelephone + '\'' +
				", tinNumber='" + tinNumber + '\'' +
				", tinDate=" + tinDate +
				", faxNumber='" + faxNumber + '\'' +
				", contactPersonName='" + contactPersonName + '\'' +
				", primaryMobile='" + primaryMobile + '\'' +
				", secondaryMobile='" + secondaryMobile + '\'' +
				", email='" + email + '\'' +
				", website='" + website + '\'' +
				", contactPersonNumber='" + contactPersonNumber + '\'' +
				", statusId=" + statusId +
				", panNumber='" + panNumber + '\'' +
				", panDate='" + panDate + '\'' +
				", companyLogoPath='" + companyLogoPath + '\'' +
				", addressesListDTO=" + addressesListDTO +
				", stateCode='" + stateCode + '\'' +
				", stateName='" + stateName + '\'' +
				", tagLine='" + tagLine + '\'' +
				", bank='" + bank + '\'' +
				", branch='" + branch + '\'' +
				", account='" + account + '\'' +
				", ifsc='" + ifsc + '\'' +
				", bankAdCode=" + bankAdCode +
				", ceritificateImagePath='" + ceritificateImagePath + '\'' +
				", signatureImagePath='" + signatureImagePath + '\'' +
				", msmeNumber='" + msmeNumber + '\'' +
				", cinNumber='" + cinNumber + '\'' +
				", subject='" + subject + '\'' +
				", iecCode='" + iecCode + '\'' +
				", companyCurrencyId='" + companyCurrencyId + '\'' +
				", creditLimit='" + creditLimit + '\'' +
				", insuranceType='" + insuranceType + '\'' +
				", insuranceRef='" + insuranceRef + '\'' +
				", financePerson='" + financePerson + '\'' +
				", financeEmail='" + financeEmail + '\'' +
				'}';
	}
}
