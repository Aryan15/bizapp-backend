package com.coreerp.dto;

public class StateDTO {

	private Long id;
	
	private String name;
	
	private String stateCode;
	
	private Long countryId;
	
//	private List<City> cities;
	
	private String deleted;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "StateDTO [id=" + id + ", name=" + name + ", countryId=" + countryId + ", deleted="
				+ deleted + "]";
	}
	
	
}
