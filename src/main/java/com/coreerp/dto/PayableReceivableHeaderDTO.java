package com.coreerp.dto;

import java.util.Date;
import java.util.List;

public class PayableReceivableHeaderDTO {
	
	private String id;
	private String payReferenceNumber;
	
	private Long payReferenceId;
	
	private Date payReferenceDate;
	
	private Long partyId;
	
	private Long paymentMethodId;
	
	private Long cardTypeId;
	
	private String paymentDocumentNumber;
	
	private String bankName;
	
	private Date paymentDocumentDate;
	
	private String paymentNote;
	
	private String clearingMode;
	
	private String paymentStatus;
	
	private Long statusId;
	
	private Long financialYearId;
	
	private Long companyId;
	
	private Long transactionTypeId;
	
	private Double paymentAmount;
	
	private Integer isPost;
	
	private String statusName;
	
	private String partyName;
	
	List<PayableReceivableItemDTO> payableReceivableItems;
	
	private List<String> creditDebitNumber;
	
	private List<String> creditDebitId;
	
	private String paymentMode;
	
    private String companyName; 
    private Long companygGstRegistrationTypeId;
    private String companyPinCode; 
    private Long companyStateId;
    private String companyStateName;
    private Long companyCountryId;
    private String companyPrimaryMobile;
    private String companySecondaryMobile;
    private String companyContactPersonNumber;
    private String companyContactPersonName;
    private String companyPrimaryTelephone;
    private String companySecondaryTelephone;
    private String companyWebsite;
    private String companyEmail;
    private String companyFaxNumber;
    private String companyAddress;
    private String companyTagLine;
    private String companyGstNumber;
    private String companyPanNumber;
    private String companyPanDate;
    private String companyCeritificateImagePath;
    private String companyLogoPath;
    
    private String partyContactPersonNumber;
    private String partyPinCode;
    private Long partyAreaId;
    private Long partyCityId;
    private Long partyStateId;
    private Long partyCountryId;
    private String partyPrimaryTelephone;
    private String partySecondaryTelephone;
    private String partyPrimaryMobile;
    private String partySecondaryMobile;
    private String partyEmail;
    private String partyWebsite;
    private String partyContactPersonName;
    private String partyBillToAddress;
    private String partyShipAddress;
    private String partyDueDaysLimit;
    private String partyGstRegistrationTypeId;
    private String partyGstNumber;
    private String partyPanNumber;
    private String isIgst;
    private String partyCode;

	private String createdBy;
	private String updateBy;
	private Date createdDate;
	private Date updatedDate;

	
	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPayReferenceNumber() {
		return payReferenceNumber;
	}

	public void setPayReferenceNumber(String payReferenceNumber) {
		this.payReferenceNumber = payReferenceNumber;
	}

	public Long getPayReferenceId() {
		return payReferenceId;
	}

	public void setPayReferenceId(Long payReferenceId) {
		this.payReferenceId = payReferenceId;
	}

	public Date getPayReferenceDate() {
		return payReferenceDate;
	}

	public void setPayReferenceDate(Date payReferenceDate) {
		this.payReferenceDate = payReferenceDate;
	}



	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	public Long getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(Long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public Long getCardTypeId() {
		return cardTypeId;
	}

	public void setCardTypeId(Long cardTypeId) {
		this.cardTypeId = cardTypeId;
	}

	public String getPaymentDocumentNumber() {
		return paymentDocumentNumber;
	}

	public void setPaymentDocumentNumber(String paymentDocumentNumber) {
		this.paymentDocumentNumber = paymentDocumentNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getPaymentDocumentDate() {
		return paymentDocumentDate;
	}

	public void setPaymentDocumentDate(Date paymentDocumentDate) {
		this.paymentDocumentDate = paymentDocumentDate;
	}

	public String getPaymentNote() {
		return paymentNote;
	}

	public void setPaymentNote(String paymentNote) {
		this.paymentNote = paymentNote;
	}

	public String getClearingMode() {
		return clearingMode;
	}

	public void setClearingMode(String clearingMode) {
		this.clearingMode = clearingMode;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}


	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Long getFinancialYearId() {
		return financialYearId;
	}

	public void setFinancialYearId(Long financialYearId) {
		this.financialYearId = financialYearId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}



	public Long getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Integer getIsPost() {
		return isPost;
	}

	public void setIsPost(Integer isPost) {
		this.isPost = isPost;
	}
	
	

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public List<PayableReceivableItemDTO> getPayableReceivableItems() {
		return payableReceivableItems;
	}

	public void setPayableReceivableItems(List<PayableReceivableItemDTO> payableReceivableItems) {
		this.payableReceivableItems = payableReceivableItems;
	}

	public List<String> getCreditDebitNumber() {
		return creditDebitNumber;
	}

	public void setCreditDebitNumber(List<String> creditDebitNumber) {
		this.creditDebitNumber = creditDebitNumber;
	}

	public List<String> getCreditDebitId() {
		return creditDebitId;
	}

	public void setCreditDebitId(List<String> creditDebitId) {
		this.creditDebitId = creditDebitId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCompanygGstRegistrationTypeId() {
		return companygGstRegistrationTypeId;
	}

	public void setCompanygGstRegistrationTypeId(Long companygGstRegistrationTypeId) {
		this.companygGstRegistrationTypeId = companygGstRegistrationTypeId;
	}

	public String getCompanyPinCode() {
		return companyPinCode;
	}

	public void setCompanyPinCode(String companyPinCode) {
		this.companyPinCode = companyPinCode;
	}

	public Long getCompanyStateId() {
		return companyStateId;
	}

	public void setCompanyStateId(Long companyStateId) {
		this.companyStateId = companyStateId;
	}

	public String getCompanyStateName() {
		return companyStateName;
	}

	public void setCompanyStateName(String companyStateName) {
		this.companyStateName = companyStateName;
	}

	public Long getCompanyCountryId() {
		return companyCountryId;
	}

	public void setCompanyCountryId(Long companyCountryId) {
		this.companyCountryId = companyCountryId;
	}

	public String getCompanyPrimaryMobile() {
		return companyPrimaryMobile;
	}

	public void setCompanyPrimaryMobile(String companyPrimaryMobile) {
		this.companyPrimaryMobile = companyPrimaryMobile;
	}

	public String getCompanySecondaryMobile() {
		return companySecondaryMobile;
	}

	public void setCompanySecondaryMobile(String companySecondaryMobile) {
		this.companySecondaryMobile = companySecondaryMobile;
	}

	public String getCompanyContactPersonNumber() {
		return companyContactPersonNumber;
	}

	public void setCompanyContactPersonNumber(String companyContactPersonNumber) {
		this.companyContactPersonNumber = companyContactPersonNumber;
	}

	public String getCompanyContactPersonName() {
		return companyContactPersonName;
	}

	public void setCompanyContactPersonName(String companyContactPersonName) {
		this.companyContactPersonName = companyContactPersonName;
	}

	public String getCompanyPrimaryTelephone() {
		return companyPrimaryTelephone;
	}

	public void setCompanyPrimaryTelephone(String companyPrimaryTelephone) {
		this.companyPrimaryTelephone = companyPrimaryTelephone;
	}

	public String getCompanySecondaryTelephone() {
		return companySecondaryTelephone;
	}

	public void setCompanySecondaryTelephone(String companySecondaryTelephone) {
		this.companySecondaryTelephone = companySecondaryTelephone;
	}

	public String getCompanyWebsite() {
		return companyWebsite;
	}

	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}

	public String getCompanyEmail() {
		return companyEmail;
	}

	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}

	public String getCompanyFaxNumber() {
		return companyFaxNumber;
	}

	public void setCompanyFaxNumber(String companyFaxNumber) {
		this.companyFaxNumber = companyFaxNumber;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyTagLine() {
		return companyTagLine;
	}

	public void setCompanyTagLine(String companyTagLine) {
		this.companyTagLine = companyTagLine;
	}

	public String getCompanyGstNumber() {
		return companyGstNumber;
	}

	public void setCompanyGstNumber(String companyGstNumber) {
		this.companyGstNumber = companyGstNumber;
	}

	public String getCompanyPanNumber() {
		return companyPanNumber;
	}

	public void setCompanyPanNumber(String companyPanNumber) {
		this.companyPanNumber = companyPanNumber;
	}

	public String getCompanyPanDate() {
		return companyPanDate;
	}

	public void setCompanyPanDate(String companyPanDate) {
		this.companyPanDate = companyPanDate;
	}

	public String getCompanyCeritificateImagePath() {
		return companyCeritificateImagePath;
	}

	public void setCompanyCeritificateImagePath(String companyCeritificateImagePath) {
		this.companyCeritificateImagePath = companyCeritificateImagePath;
	}

	public String getCompanyLogoPath() {
		return companyLogoPath;
	}

	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}

	public String getPartyContactPersonNumber() {
		return partyContactPersonNumber;
	}

	public void setPartyContactPersonNumber(String partyContactPersonNumber) {
		this.partyContactPersonNumber = partyContactPersonNumber;
	}

	public String getPartyPinCode() {
		return partyPinCode;
	}

	public void setPartyPinCode(String partyPinCode) {
		this.partyPinCode = partyPinCode;
	}

	public Long getPartyAreaId() {
		return partyAreaId;
	}

	public void setPartyAreaId(Long partyAreaId) {
		this.partyAreaId = partyAreaId;
	}

	public Long getPartyCityId() {
		return partyCityId;
	}

	public void setPartyCityId(Long partyCityId) {
		this.partyCityId = partyCityId;
	}

	public Long getPartyStateId() {
		return partyStateId;
	}

	public void setPartyStateId(Long partyStateId) {
		this.partyStateId = partyStateId;
	}

	public Long getPartyCountryId() {
		return partyCountryId;
	}

	public void setPartyCountryId(Long partyCountryId) {
		this.partyCountryId = partyCountryId;
	}

	public String getPartyPrimaryTelephone() {
		return partyPrimaryTelephone;
	}

	public void setPartyPrimaryTelephone(String partyPrimaryTelephone) {
		this.partyPrimaryTelephone = partyPrimaryTelephone;
	}

	public String getPartySecondaryTelephone() {
		return partySecondaryTelephone;
	}

	public void setPartySecondaryTelephone(String partySecondaryTelephone) {
		this.partySecondaryTelephone = partySecondaryTelephone;
	}

	public String getPartyPrimaryMobile() {
		return partyPrimaryMobile;
	}

	public void setPartyPrimaryMobile(String partyPrimaryMobile) {
		this.partyPrimaryMobile = partyPrimaryMobile;
	}

	public String getPartySecondaryMobile() {
		return partySecondaryMobile;
	}

	public void setPartySecondaryMobile(String partySecondaryMobile) {
		this.partySecondaryMobile = partySecondaryMobile;
	}

	public String getPartyEmail() {
		return partyEmail;
	}

	public void setPartyEmail(String partyEmail) {
		this.partyEmail = partyEmail;
	}

	public String getPartyWebsite() {
		return partyWebsite;
	}

	public void setPartyWebsite(String partyWebsite) {
		this.partyWebsite = partyWebsite;
	}

	public String getPartyContactPersonName() {
		return partyContactPersonName;
	}

	public void setPartyContactPersonName(String partyContactPersonName) {
		this.partyContactPersonName = partyContactPersonName;
	}

	public String getPartyBillToAddress() {
		return partyBillToAddress;
	}

	public void setPartyBillToAddress(String partyBillToAddress) {
		this.partyBillToAddress = partyBillToAddress;
	}

	public String getPartyShipAddress() {
		return partyShipAddress;
	}

	public void setPartyShipAddress(String partyShipAddress) {
		this.partyShipAddress = partyShipAddress;
	}

	public String getPartyDueDaysLimit() {
		return partyDueDaysLimit;
	}

	public void setPartyDueDaysLimit(String partyDueDaysLimit) {
		this.partyDueDaysLimit = partyDueDaysLimit;
	}

	public String getPartyGstRegistrationTypeId() {
		return partyGstRegistrationTypeId;
	}

	public void setPartyGstRegistrationTypeId(String partyGstRegistrationTypeId) {
		this.partyGstRegistrationTypeId = partyGstRegistrationTypeId;
	}

	public String getPartyGstNumber() {
		return partyGstNumber;
	}

	public void setPartyGstNumber(String partyGstNumber) {
		this.partyGstNumber = partyGstNumber;
	}

	public String getPartyPanNumber() {
		return partyPanNumber;
	}

	public void setPartyPanNumber(String partyPanNumber) {
		this.partyPanNumber = partyPanNumber;
	}

	public String getIsIgst() {
		return isIgst;
	}

	public void setIsIgst(String isIgst) {
		this.isIgst = isIgst;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
