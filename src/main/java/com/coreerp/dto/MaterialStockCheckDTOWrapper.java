package com.coreerp.dto;

import java.util.List;

public class MaterialStockCheckDTOWrapper {

	private List<MaterialStockCheckDTO> materialStockCheckDTOs;

	public List<MaterialStockCheckDTO> getMaterialStockCheckDTOs() {
		return materialStockCheckDTOs;
	}

	public void setMaterialStockCheckDTOs(List<MaterialStockCheckDTO> materialStockCheckDTOs) {
		this.materialStockCheckDTOs = materialStockCheckDTOs;
	}
	
	
}
