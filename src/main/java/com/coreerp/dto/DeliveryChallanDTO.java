package com.coreerp.dto;

import java.util.Date;
import java.util.List;

public class DeliveryChallanDTO {

	private String id;
	
	private String deliveryChallanNumber;
	
	private Long deliveryChallanId;
	
	private Date deliveryChallanDate;
	
	private String internalReferenceNumber;
	
	private Date internalReferenceDate;	
	
	private String purchaseOrderNumber;
	
	private Date purchaseOrderDate;
	
	private Long deliveryChallanTypeId;
	
	private Long partyId;
	
	private String officeAddress;
	
	private String deliveryAddress;
	
	private Date dispatchDate;
	
	private String inspectedBy;
	
	private Date inspectionDate;
	
	private String modeOfDispatch;
	
	private String vehicleNumber;
	
	private String deliveryNote;
	
	private Double totalQuantity;
	
	private Long statusId;
	
	//private String deliveryChallanStatus;
	
	private Long financialYearId;
	
	private Long companyId;
	
	private String numberOfPackages;
	
	private String deliveryTerms;
	
	private String termsAndConditions;
	
	private String paymentTerms;
	
	private String dispatchTime;
	
	private Long taxId;
	
	private Long labourChargesOnly;
	
	private String inDeliveryChallanNumber;
	
	private Date inDeliveryChallanDate;
	
	private String returnableDeliveryChallan;
	
	private String nonReturnableDeliveryChallan;
	
	private String notForSale;
	
	private String forSale;
	
	private String returnForJobWork;
	
	private String modeOfDispatchStatus;
	
	private String vehicleNumberStatus;
	
	private String numberOfPackagesStatus;
	
	private String personName;
	
	private Double sgstTaxRate;
	
	private Double sgstTaxAmount;
	
	private Double cgstTaxRate;
	
	private Double cgstTaxAmount;
	
	private Double igstTaxRate;
	
	private Double igstTaxAmount;
	
	private Double taxAmount;
	
	private Double price;
	
	private Double amount;
	
	private String eWayBill;
	
	private Double balanceQuantity;
	
	private List<DeliveryChallanItemDTO> deliveryChallanItems;
	
	private String statusName;
	
	private String partyName;
	
	private Double discountPercentage;
	 
	private Double discountAmount;
	    
    private Double amountAfterDiscount;
    private String remarks;
    
    private String  gstNumber;
    
    private String address;
    
    private Integer inclusiveTax;
    
    private String billToAddress;
    private String shipToAddress;
    
//	.........
    private String companyName; 
    private Long companygGstRegistrationTypeId;
    private String companyPinCode; 
    private Long companyStateId;
    private String companyStateName;
    private Long companyCountryId;
    private String companyPrimaryMobile;
    private String companySecondaryMobile;
    private String companyContactPersonNumber;
    private String companyContactPersonName;
    private String companyPrimaryTelephone;
    private String companySecondaryTelephone;
    private String companyWebsite;
    private String companyEmail;
    private String companyFaxNumber;
    private String companyAddress;
    private String companyTagLine;
    private String companyGstNumber;
    private String companyPanNumber;
    private String companyPanDate;
    private String companyCeritificateImagePath;
    private String companyLogoPath;
    
    private String partyContactPersonNumber;
    private String partyPinCode;
    private Long partyAreaId;
    private Long partyCityId;
    private Long partyStateId;
    private Long partyCountryId;
    private String partyPrimaryTelephone;
    private String partySecondaryTelephone;
    private String partyPrimaryMobile;
    private String partySecondaryMobile;
    private String partyEmail;
    private String partyWebsite;
    private String partyContactPersonName;
    private String partyBillToAddress;
    private String partyShipAddress;
    private String partyDueDaysLimit;
    private String partyGstRegistrationTypeId;
    private String partyGstNumber;
    private String partyPanNumber;
    private String isIgst;
    private String partyCode;
	private String createdBy;
	private String updateBy;
	private Date createdDate;
	private Date updatedDate;
	private String jwNoteId;
    public String getBillToAddress() {
		return billToAddress;
	}

	public void setBillToAddress(String billToAddress) {
		this.billToAddress = billToAddress;
	}
	
	public String getShipToAddress() {
		return shipToAddress;
	}

	public void setShipToAddress(String shipToAddress) {
		this.shipToAddress = shipToAddress;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



	public String getDeliveryChallanNumber() {
		return deliveryChallanNumber;
	}

	public void setDeliveryChallanNumber(String deliveryChallanNumber) {
		this.deliveryChallanNumber = deliveryChallanNumber;
	}

	public Long getDeliveryChallanId() {
		return deliveryChallanId;
	}

	public void setDeliveryChallanId(Long deliveryChallanId) {
		this.deliveryChallanId = deliveryChallanId;
	}

	public Date getDeliveryChallanDate() {
		return deliveryChallanDate;
	}

	public void setDeliveryChallanDate(Date deliveryChallanDate) {
		this.deliveryChallanDate = deliveryChallanDate;
	}

	public String getInternalReferenceNumber() {
		return internalReferenceNumber;
	}

	public void setInternalReferenceNumber(String internalReferenceNumber) {
		this.internalReferenceNumber = internalReferenceNumber;
	}

	public Date getInternalReferenceDate() {
		return internalReferenceDate;
	}

	public void setInternalReferenceDate(Date internalReferenceDate) {
		this.internalReferenceDate = internalReferenceDate;
	}

	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public Date getPurchaseOrderDate() {
		return purchaseOrderDate;
	}

	public void setPurchaseOrderDate(Date purchaseOrderDate) {
		this.purchaseOrderDate = purchaseOrderDate;
	}



	public Long getDeliveryChallanTypeId() {
		return deliveryChallanTypeId;
	}

	public void setDeliveryChallanTypeId(Long deliveryChallanTypeId) {
		this.deliveryChallanTypeId = deliveryChallanTypeId;
	}

	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public Date getDispatchDate() {
		return dispatchDate;
	}

	public void setDispatchDate(Date dispatchDate) {
		this.dispatchDate = dispatchDate;
	}

	public String getInspectedBy() {
		return inspectedBy;
	}

	public void setInspectedBy(String inspectedBy) {
		this.inspectedBy = inspectedBy;
	}

	public Date getInspectionDate() {
		return inspectionDate;
	}

	public void setInspectionDate(Date inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	public String getModeOfDispatch() {
		return modeOfDispatch;
	}

	public void setModeOfDispatch(String modeOfDispatch) {
		this.modeOfDispatch = modeOfDispatch;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getDeliveryNote() {
		return deliveryNote;
	}

	public void setDeliveryNote(String deliveryNote) {
		this.deliveryNote = deliveryNote;
	}

	public Double getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}


//	public String getDeliveryChallanStatus() {
//		return deliveryChallanStatus;
//	}
//
//	public void setDeliveryChallanStatus(String deliveryChallanStatus) {
//		this.deliveryChallanStatus = deliveryChallanStatus;
//	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Long getFinancialYearId() {
		return financialYearId;
	}

	public void setFinancialYearId(Long financialYearId) {
		this.financialYearId = financialYearId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getNumberOfPackages() {
		return numberOfPackages;
	}

	public void setNumberOfPackages(String numberOfPackages) {
		this.numberOfPackages = numberOfPackages;
	}

	public String getDeliveryTerms() {
		return deliveryTerms;
	}

	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}

	public String getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getDispatchTime() {
		return dispatchTime;
	}

	public void setDispatchTime(String dispatchTime) {
		this.dispatchTime = dispatchTime;
	}

	public Long getTaxId() {
		return taxId;
	}

	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}

	public Long getLabourChargesOnly() {
		return labourChargesOnly;
	}

	public void setLabourChargesOnly(Long labourChargesOnly) {
		this.labourChargesOnly = labourChargesOnly;
	}

	public String getInDeliveryChallanNumber() {
		return inDeliveryChallanNumber;
	}

	public void setInDeliveryChallanNumber(String inDeliveryChallanNumber) {
		this.inDeliveryChallanNumber = inDeliveryChallanNumber;
	}

	public Date getInDeliveryChallanDate() {
		return inDeliveryChallanDate;
	}

	public void setInDeliveryChallanDate(Date inDeliveryChallanDate) {
		this.inDeliveryChallanDate = inDeliveryChallanDate;
	}

	public String getReturnableDeliveryChallan() {
		return returnableDeliveryChallan;
	}

	public void setReturnableDeliveryChallan(String returnableDeliveryChallan) {
		this.returnableDeliveryChallan = returnableDeliveryChallan;
	}

	public String getNonReturnableDeliveryChallan() {
		return nonReturnableDeliveryChallan;
	}

	public void setNonReturnableDeliveryChallan(String nonReturnableDeliveryChallan) {
		this.nonReturnableDeliveryChallan = nonReturnableDeliveryChallan;
	}

	public String getNotForSale() {
		return notForSale;
	}

	public void setNotForSale(String notForSale) {
		this.notForSale = notForSale;
	}

	public String getForSale() {
		return forSale;
	}

	public void setForSale(String forSale) {
		this.forSale = forSale;
	}

	public String getReturnForJobWork() {
		return returnForJobWork;
	}

	public void setReturnForJobWork(String returnForJobWork) {
		this.returnForJobWork = returnForJobWork;
	}

	public String getModeOfDispatchStatus() {
		return modeOfDispatchStatus;
	}

	public void setModeOfDispatchStatus(String modeOfDispatchStatus) {
		this.modeOfDispatchStatus = modeOfDispatchStatus;
	}

	public String getVehicleNumberStatus() {
		return vehicleNumberStatus;
	}

	public void setVehicleNumberStatus(String vehicleNumberStatus) {
		this.vehicleNumberStatus = vehicleNumberStatus;
	}

	public String getNumberOfPackagesStatus() {
		return numberOfPackagesStatus;
	}

	public void setNumberOfPackagesStatus(String numberOfPackagesStatus) {
		this.numberOfPackagesStatus = numberOfPackagesStatus;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}

	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}

	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}

	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}

	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}

	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}

	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}

	public Double getIgstTaxRate() {
		return igstTaxRate;
	}

	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}

	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}

	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public List<DeliveryChallanItemDTO> getDeliveryChallanItems() {
		return deliveryChallanItems;
	}

	public void setDeliveryChallanItems(List<DeliveryChallanItemDTO> deliveryChallanItems) {
		this.deliveryChallanItems = deliveryChallanItems;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String geteWayBill() {
		return eWayBill;
	}

	public void seteWayBill(String eWayBill) {
		this.eWayBill = eWayBill;
	}

	public Double getBalanceQuantity() {
		return balanceQuantity;
	}

	public void setBalanceQuantity(Double balanceQuantity) {
		this.balanceQuantity = balanceQuantity;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}

	public void setAmountAfterDiscount(Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getInclusiveTax() {
		return inclusiveTax;
	}

	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCompanygGstRegistrationTypeId() {
		return companygGstRegistrationTypeId;
	}

	public void setCompanygGstRegistrationTypeId(Long companygGstRegistrationTypeId) {
		this.companygGstRegistrationTypeId = companygGstRegistrationTypeId;
	}

	public String getCompanyPinCode() {
		return companyPinCode;
	}

	public void setCompanyPinCode(String companyPinCode) {
		this.companyPinCode = companyPinCode;
	}

	public Long getCompanyStateId() {
		return companyStateId;
	}

	public void setCompanyStateId(Long companyStateId) {
		this.companyStateId = companyStateId;
	}

	
	public String getCompanyStateName() {
		return companyStateName;
	}

	public void setCompanyStateName(String companyStateName) {
		this.companyStateName = companyStateName;
	}

	public Long getCompanyCountryId() {
		return companyCountryId;
	}

	public void setCompanyCountryId(Long companyCountryId) {
		this.companyCountryId = companyCountryId;
	}

	public String getCompanyPrimaryMobile() {
		return companyPrimaryMobile;
	}

	public void setCompanyPrimaryMobile(String companyPrimaryMobile) {
		this.companyPrimaryMobile = companyPrimaryMobile;
	}

	public String getCompanySecondaryMobile() {
		return companySecondaryMobile;
	}

	public void setCompanySecondaryMobile(String companySecondaryMobile) {
		this.companySecondaryMobile = companySecondaryMobile;
	}

	public String getCompanyContactPersonNumber() {
		return companyContactPersonNumber;
	}

	public void setCompanyContactPersonNumber(String companyContactPersonNumber) {
		this.companyContactPersonNumber = companyContactPersonNumber;
	}

	public String getCompanyContactPersonName() {
		return companyContactPersonName;
	}

	public void setCompanyContactPersonName(String companyContactPersonName) {
		this.companyContactPersonName = companyContactPersonName;
	}

	public String getCompanyPrimaryTelephone() {
		return companyPrimaryTelephone;
	}

	public void setCompanyPrimaryTelephone(String companyPrimaryTelephone) {
		this.companyPrimaryTelephone = companyPrimaryTelephone;
	}

	public String getCompanySecondaryTelephone() {
		return companySecondaryTelephone;
	}

	public void setCompanySecondaryTelephone(String companySecondaryTelephone) {
		this.companySecondaryTelephone = companySecondaryTelephone;
	}

	public String getCompanyWebsite() {
		return companyWebsite;
	}

	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}

	public String getCompanyEmail() {
		return companyEmail;
	}

	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}

	public String getCompanyFaxNumber() {
		return companyFaxNumber;
	}

	public void setCompanyFaxNumber(String companyFaxNumber) {
		this.companyFaxNumber = companyFaxNumber;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyTagLine() {
		return companyTagLine;
	}

	public void setCompanyTagLine(String companyTagLine) {
		this.companyTagLine = companyTagLine;
	}

	public String getCompanyGstNumber() {
		return companyGstNumber;
	}

	public void setCompanyGstNumber(String companyGstNumber) {
		this.companyGstNumber = companyGstNumber;
	}

	public String getCompanyPanNumber() {
		return companyPanNumber;
	}

	public void setCompanyPanNumber(String companyPanNumber) {
		this.companyPanNumber = companyPanNumber;
	}

	public String getCompanyPanDate() {
		return companyPanDate;
	}

	public void setCompanyPanDate(String companyPanDate) {
		this.companyPanDate = companyPanDate;
	}

	public String getCompanyCeritificateImagePath() {
		return companyCeritificateImagePath;
	}

	public void setCompanyCeritificateImagePath(String companyCeritificateImagePath) {
		this.companyCeritificateImagePath = companyCeritificateImagePath;
	}

	public String getCompanyLogoPath() {
		return companyLogoPath;
	}

	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}

	public String getPartyContactPersonNumber() {
		return partyContactPersonNumber;
	}

	public void setPartyContactPersonNumber(String partyContactPersonNumber) {
		this.partyContactPersonNumber = partyContactPersonNumber;
	}

	public String getPartyPinCode() {
		return partyPinCode;
	}

	public void setPartyPinCode(String partyPinCode) {
		this.partyPinCode = partyPinCode;
	}

	public Long getPartyAreaId() {
		return partyAreaId;
	}

	public void setPartyAreaId(Long partyAreaId) {
		this.partyAreaId = partyAreaId;
	}

	public Long getPartyCityId() {
		return partyCityId;
	}

	public void setPartyCityId(Long partyCityId) {
		this.partyCityId = partyCityId;
	}

	public Long getPartyStateId() {
		return partyStateId;
	}

	public void setPartyStateId(Long partyStateId) {
		this.partyStateId = partyStateId;
	}

	public Long getPartyCountryId() {
		return partyCountryId;
	}

	public void setPartyCountryId(Long partyCountryId) {
		this.partyCountryId = partyCountryId;
	}

	public String getPartyPrimaryTelephone() {
		return partyPrimaryTelephone;
	}

	public void setPartyPrimaryTelephone(String partyPrimaryTelephone) {
		this.partyPrimaryTelephone = partyPrimaryTelephone;
	}

	public String getPartySecondaryTelephone() {
		return partySecondaryTelephone;
	}

	public void setPartySecondaryTelephone(String partySecondaryTelephone) {
		this.partySecondaryTelephone = partySecondaryTelephone;
	}

	public String getPartyPrimaryMobile() {
		return partyPrimaryMobile;
	}

	public void setPartyPrimaryMobile(String partyPrimaryMobile) {
		this.partyPrimaryMobile = partyPrimaryMobile;
	}

	public String getPartySecondaryMobile() {
		return partySecondaryMobile;
	}

	public void setPartySecondaryMobile(String partySecondaryMobile) {
		this.partySecondaryMobile = partySecondaryMobile;
	}

	public String getPartyEmail() {
		return partyEmail;
	}

	public void setPartyEmail(String partyEmail) {
		this.partyEmail = partyEmail;
	}

	public String getPartyWebsite() {
		return partyWebsite;
	}

	public void setPartyWebsite(String partyWebsite) {
		this.partyWebsite = partyWebsite;
	}

	public String getPartyContactPersonName() {
		return partyContactPersonName;
	}

	public void setPartyContactPersonName(String partyContactPersonName) {
		this.partyContactPersonName = partyContactPersonName;
	}

	public String getPartyBillToAddress() {
		return partyBillToAddress;
	}

	public void setPartyBillToAddress(String partyBillToAddress) {
		this.partyBillToAddress = partyBillToAddress;
	}

	public String getPartyShipAddress() {
		return partyShipAddress;
	}

	public void setPartyShipAddress(String partyShipAddress) {
		this.partyShipAddress = partyShipAddress;
	}

	public String getPartyDueDaysLimit() {
		return partyDueDaysLimit;
	}

	public void setPartyDueDaysLimit(String partyDueDaysLimit) {
		this.partyDueDaysLimit = partyDueDaysLimit;
	}

	public String getPartyGstRegistrationTypeId() {
		return partyGstRegistrationTypeId;
	}

	public void setPartyGstRegistrationTypeId(String partyGstRegistrationTypeId) {
		this.partyGstRegistrationTypeId = partyGstRegistrationTypeId;
	}

	public String getPartyGstNumber() {
		return partyGstNumber;
	}

	public void setPartyGstNumber(String partyGstNumber) {
		this.partyGstNumber = partyGstNumber;
	}

	public String getPartyPanNumber() {
		return partyPanNumber;
	}

	public void setPartyPanNumber(String partyPanNumber) {
		this.partyPanNumber = partyPanNumber;
	}

	public String getIsIgst() {
		return isIgst;
	}

	public void setIsIgst(String isIgst) {
		this.isIgst = isIgst;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getJwNoteId() {
		return jwNoteId;
	}

	public void setJwNoteId(String jwNoteId) {
		this.jwNoteId = jwNoteId;
	}
}
