package com.coreerp.dto;

import java.util.List;

import com.coreerp.model.ActivityRoleBinding;

public class ActivityDTO implements Comparable<ActivityDTO>{
	private Long id;
	private String name;
	private String pageUrl;
	private String urlParams;
	private String imagePath;
	private String description;
	private String deleted;
	private Integer activityOrder;
	//private CompanyDTO company;
	private MenuDTO subMenu;	
	private List<ActivityRoleBindingDTO> activityRoleBindings;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPageUrl() {
		return pageUrl;
	}
	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}
	
	public String getUrlParams() {
		return urlParams;
	}
	public void setUrlParams(String urlParams) {
		this.urlParams = urlParams;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public Integer getActivityOrder() {
		return activityOrder;
	}
	public void setActivityOrder(Integer activityOrder) {
		this.activityOrder = activityOrder;
	}
//	public CompanyDTO getCompany() {
//		return company;
//	}
//	public void setCompany(CompanyDTO company) {
//		this.company = company;
//	}
	
	
	public MenuDTO getSubMenu() {
		return subMenu;
	}
	public void setSubMenu(MenuDTO subMenu) {
		this.subMenu = subMenu;
	}
	public List<ActivityRoleBindingDTO> getActivityRoleBindings() {
		return activityRoleBindings;
	}
	public void setActivityRoleBindings(List<ActivityRoleBindingDTO> activityRoleBindings) {
		this.activityRoleBindings = activityRoleBindings;
	}
	@Override
	public int compareTo(ActivityDTO o) {
		if(activityOrder == o.getActivityOrder()){
			return 0;
		}
		else if(activityOrder > o.getActivityOrder()){
			return 1;
		}else
			return -1;
	}

	
	
}
