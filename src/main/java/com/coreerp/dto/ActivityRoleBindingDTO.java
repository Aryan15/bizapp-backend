package com.coreerp.dto;

import com.coreerp.model.Role;

public class ActivityRoleBindingDTO {

    private Long id;
    private Long activityId;
	//private Role role;
	//private UserDTO user;
    private Long roleId;
    private Long userId;
	private String deleted;
    private Long companyId;
	private Integer permissionToCreate;
	private Integer permissionToUpdate;
	private Integer permissionToDelete;
	private Integer permissionToCancel;
	private Integer permissionToApprove;
	private Integer permissionToPrint;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getActivityId() {
		return activityId;
	}
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}
//	public Role getRole() {
//		return role;
//	}
//	public void setRole(Role role) {
//		this.role = role;
//	}

	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
//	public Company getCompany() {
//		return company;
//	}
//	public void setCompany(Company company) {
//		this.company = company;
//	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
	public Integer getPermissionToCreate() {
		return permissionToCreate;
	}

	public void setPermissionToCreate(Integer permissionToCreate) {
		this.permissionToCreate = permissionToCreate;
	}
	public Integer getPermissionToUpdate() {
		return permissionToUpdate;
	}
	public void setPermissionToUpdate(Integer permissionToUpdate) {
		this.permissionToUpdate = permissionToUpdate;
	}
	public Integer getPermissionToDelete() {
		return permissionToDelete;
	}
	public void setPermissionToDelete(Integer permissionToDelete) {
		this.permissionToDelete = permissionToDelete;
	}
	public Integer getPermissionToCancel() {
		return permissionToCancel;
	}
	public void setPermissionToCancel(Integer permissionToCancel) {
		this.permissionToCancel = permissionToCancel;
	}
	public Integer getPermissionToApprove() {
		return permissionToApprove;
	}
	public void setPermissionToApprove(Integer permissionToApprove) {
		this.permissionToApprove = permissionToApprove;
	}
	public Integer getPermissionToPrint() {
		return permissionToPrint;
	}
	public void setPermissionToPrint(Integer permissionToPrint) {
		this.permissionToPrint = permissionToPrint;
	}
	
	
//	public UserDTO getUser() {
//		return user;
//	}
//	public void setUser(UserDTO user) {
//		this.user = user;
//	}
	
	

	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "ActivityRoleBindingDTO [id=" + id + ", activityId=" + activityId + ", roleId=" + roleId + ", userId="
				+ userId + ", deleted=" + deleted + ", companyId=" + companyId + ", permissionToCreate="
				+ permissionToCreate + ", permissionToUpdate=" + permissionToUpdate + ", permissionToDelete="
				+ permissionToDelete + ", permissionToCancel=" + permissionToCancel + ", permissionToApprove="
				+ permissionToApprove + ", permissionToPrint=" + permissionToPrint + "]";
	}
	
	
	
}
