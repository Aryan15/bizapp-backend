package com.coreerp.dto;

import java.util.List;

public class EwayBillInvoiceBillInputDTO {

    private String userGstin ;
    private String supplyType ;
    private Integer subSupplyType ;
    private String docType ;
    private String docNo ;
    private String docDate ;
    private String fromGstin;
    private String fromTrdName;
    private String fromAddr1;
    private String fromAddr2;
    private String fromPlace;
    private Integer fromPincode;
    private Integer fromStateCode;
    private Integer actualFromStateCode;
    private String toGstin;
    private String toTrdName;
    private String toAddr1;
    private String toAddr2;
    private String toPlace;
    private Integer toPincode;
    private Integer toStateCode;
    private Integer actualToStateCode;
    private Double totalValue;
    private Double cgstValue;
    private Double sgstValue;
    private Double igstValue;
    private Double cessValue;
    private Double TotNonAdvolVal;
    private Double OthValue;
    private Double totInvValue;
    private Integer transMode;
    private Double transDistance;
    private String transporterName;
    private String transporterId;
    private String transDocNo;
    private String transDocDate;
    private String vehicleNo;
    private String vehicleType;
    private String mainHsnCode;
    private String shipToGstin;
    private String shipToTradename;
    private String  dispatchFromGstin;
    private String dispatchFromTradename;
    private String  portPin;
    private String portName;
    private List<EwayBillInvoiceItemInputDTO> itemList;


    public String getUserGstin() {
        return userGstin;
    }

    public void setUserGstin(String userGstin) {
        this.userGstin = userGstin;
    }

    public String getSupplyType() {
        return supplyType;
    }

    public void setSupplyType(String supplyType) {
        this.supplyType = supplyType;
    }

    public Integer getSubSupplyType() {
        return subSupplyType;
    }

    public void setSubSupplyType(Integer subSupplyType) {
        this.subSupplyType = subSupplyType;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getFromGstin() {
        return fromGstin;
    }

    public void setFromGstin(String fromGstin) {
        this.fromGstin = fromGstin;
    }

    public String getFromTrdName() {
        return fromTrdName;
    }

    public void setFromTrdName(String fromTrdName) {
        this.fromTrdName = fromTrdName;
    }

    public String getFromAddr1() {
        return fromAddr1;
    }

    public void setFromAddr1(String fromAddr1) {
        this.fromAddr1 = fromAddr1;
    }

    public String getFromAddr2() {
        return fromAddr2;
    }

    public void setFromAddr2(String fromAddr2) {
        this.fromAddr2 = fromAddr2;
    }

    public String getFromPlace() {
        return fromPlace;
    }

    public void setFromPlace(String fromPlace) {
        this.fromPlace = fromPlace;
    }

    public Integer getFromPincode() {
        return fromPincode;
    }

    public void setFromPincode(Integer fromPincode) {
        this.fromPincode = fromPincode;
    }

    public Integer getFromStateCode() {
        return fromStateCode;
    }

    public void setFromStateCode(Integer fromStateCode) {
        this.fromStateCode = fromStateCode;
    }

    public Integer getActualFromStateCode() {
        return actualFromStateCode;
    }

    public void setActualFromStateCode(Integer actualFromStateCode) {
        this.actualFromStateCode = actualFromStateCode;
    }

    public String getToGstin() {
        return toGstin;
    }

    public void setToGstin(String toGstin) {
        this.toGstin = toGstin;
    }

    public String getToTrdName() {
        return toTrdName;
    }

    public void setToTrdName(String toTrdName) {
        this.toTrdName = toTrdName;
    }

    public String getToAddr1() {
        return toAddr1;
    }

    public void setToAddr1(String toAddr1) {
        this.toAddr1 = toAddr1;
    }

    public String getToAddr2() {
        return toAddr2;
    }

    public void setToAddr2(String toAddr2) {
        this.toAddr2 = toAddr2;
    }

    public String getToPlace() {
        return toPlace;
    }

    public void setToPlace(String toPlace) {
        this.toPlace = toPlace;
    }

    public Integer getToPincode() {
        return toPincode;
    }

    public void setToPincode(Integer toPincode) {
        this.toPincode = toPincode;
    }

    public Integer getToStateCode() {
        return toStateCode;
    }

    public void setToStateCode(Integer toStateCode) {
        this.toStateCode = toStateCode;
    }

    public Integer getActualToStateCode() {
        return actualToStateCode;
    }

    public void setActualToStateCode(Integer actualToStateCode) {
        this.actualToStateCode = actualToStateCode;
    }

    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public Double getCgstValue() {
        return cgstValue;
    }

    public void setCgstValue(Double cgstValue) {
        this.cgstValue = cgstValue;
    }

    public Double getSgstValue() {
        return sgstValue;
    }

    public void setSgstValue(Double sgstValue) {
        this.sgstValue = sgstValue;
    }

    public Double getIgstValue() {
        return igstValue;
    }

    public void setIgstValue(Double igstValue) {
        this.igstValue = igstValue;
    }

    public Double getCessValue() {
        return cessValue;
    }

    public void setCessValue(Double cessValue) {
        this.cessValue = cessValue;
    }

    public Double getTotNonAdvolVal() {
        return TotNonAdvolVal;
    }

    public void setTotNonAdvolVal(Double totNonAdvolVal) {
        TotNonAdvolVal = totNonAdvolVal;
    }

    public Double getOthValue() {
        return OthValue;
    }

    public void setOthValue(Double othValue) {
        OthValue = othValue;
    }

    public Double getTotInvValue() {
        return totInvValue;
    }

    public void setTotInvValue(Double totInvValue) {
        this.totInvValue = totInvValue;
    }

    public Integer getTransMode() {
        return transMode;
    }

    public void setTransMode(Integer transMode) {
        this.transMode = transMode;
    }

    public Double getTransDistance() {
        return transDistance;
    }

    public void setTransDistance(Double transDistance) {
        this.transDistance = transDistance;
    }

    public String getTransporterName() {
        return transporterName;
    }

    public void setTransporterName(String transporterName) {
        this.transporterName = transporterName;
    }

    public String getTransporterId() {
        return transporterId;
    }

    public void setTransporterId(String transporterId) {
        this.transporterId = transporterId;
    }

    public String getTransDocNo() {
        return transDocNo;
    }

    public void setTransDocNo(String transDocNo) {
        this.transDocNo = transDocNo;
    }

    public String getTransDocDate() {
        return transDocDate;
    }

    public void setTransDocDate(String transDocDate) {
        this.transDocDate = transDocDate;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getMainHsnCode() {
        return mainHsnCode;
    }

    public void setMainHsnCode(String mainHsnCode) {
        this.mainHsnCode = mainHsnCode;
    }

    public String getShipToGstin() {
        return shipToGstin;
    }

    public void setShipToGstin(String shipToGstin) {
        this.shipToGstin = shipToGstin;
    }

    public String getShipToTradename() {
        return shipToTradename;
    }

    public void setShipToTradename(String shipToTradename) {
        this.shipToTradename = shipToTradename;
    }

    public String getDispatchFromGstin() {
        return dispatchFromGstin;
    }

    public void setDispatchFromGstin(String dispatchFromGstin) {
        this.dispatchFromGstin = dispatchFromGstin;
    }

    public String getDispatchFromTradename() {
        return dispatchFromTradename;
    }

    public void setDispatchFromTradename(String dispatchFromTradename) {
        this.dispatchFromTradename = dispatchFromTradename;
    }

    public String getPortPin() {
        return portPin;
    }

    public void setPortPin(String portPin) {
        this.portPin = portPin;
    }

    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }
    public List<EwayBillInvoiceItemInputDTO> getItemList() {
        return itemList;
    }

    public void setItemList(List<EwayBillInvoiceItemInputDTO> itemList) {
        this.itemList = itemList;
    }

    @Override
    public String toString() {
        return "EwayBillInvoiceBillInputDTO{" +
                "userGstin='" + userGstin + '\'' +
                ", supplyType='" + supplyType + '\'' +
                ", subSupplyType=" + subSupplyType +
                ", docType='" + docType + '\'' +
                ", docNo='" + docNo + '\'' +
                ", docDate='" + docDate + '\'' +
                ", fromGstin='" + fromGstin + '\'' +
                ", fromTrdName='" + fromTrdName + '\'' +
                ", fromAddr1='" + fromAddr1 + '\'' +
                ", fromAddr2='" + fromAddr2 + '\'' +
                ", fromPlace='" + fromPlace + '\'' +
                ", fromPincode=" + fromPincode +
                ", fromStateCode=" + fromStateCode +
                ", actualFromStateCode=" + actualFromStateCode +
                ", toGstin='" + toGstin + '\'' +
                ", toTrdName='" + toTrdName + '\'' +
                ", toAddr1='" + toAddr1 + '\'' +
                ", toAddr2='" + toAddr2 + '\'' +
                ", toPlace='" + toPlace + '\'' +
                ", toPincode=" + toPincode +
                ", toStateCode=" + toStateCode +
                ", actualToStateCode=" + actualToStateCode +
                ", totalValue=" + totalValue +
                ", cgstValue=" + cgstValue +
                ", sgstValue=" + sgstValue +
                ", igstValue=" + igstValue +
                ", cessValue=" + cessValue +
                ", TotNonAdvolVal=" + TotNonAdvolVal +
                ", OthValue=" + OthValue +
                ", totInvValue=" + totInvValue +
                ", transMode=" + transMode +
                ", transDistance=" + transDistance +
                ", transporterName='" + transporterName + '\'' +
                ", transporterId='" + transporterId + '\'' +
                ", transDocNo='" + transDocNo + '\'' +
                ", transDocDate='" + transDocDate + '\'' +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", vehicleType='" + vehicleType + '\'' +
                ", mainHsnCode='" + mainHsnCode + '\'' +
                ", shipToGstin='" + shipToGstin + '\'' +
                ", shipToTradename='" + shipToTradename + '\'' +
                ", dispatchFromGstin='" + dispatchFromGstin + '\'' +
                ", dispatchFromTradename='" + dispatchFromTradename + '\'' +
                ", portPin='" + portPin + '\'' +
                ", portName='" + portName + '\'' +
                ", itemList=" + itemList +
                '}';
    }
}
