package com.coreerp.dto;

import java.util.List;

import com.coreerp.reports.model.VoucherReportModel;

public class VoucherReportDTO {

	private List<VoucherReportModel> voucherReportItems;
	private Long totalCount;
	public List<VoucherReportModel> getVoucherReportItems() {
		return voucherReportItems;
	}
	public void setVoucherReportItems(List<VoucherReportModel> voucherReportItems) {
		this.voucherReportItems = voucherReportItems;
	}
	public Long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
