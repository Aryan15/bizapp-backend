package com.coreerp.dto;

import java.util.List;

public class RecentAutoTransactionDTO {


    private List<AutoTransactionGenerationDTO> autoTransactionGenerationDTOS;

    private Long totalCount;


    public List<AutoTransactionGenerationDTO> getAutoTransactionGenerationDTOS() {
        return autoTransactionGenerationDTOS;
    }

    public void setAutoTransactionGenerationDTOS(List<AutoTransactionGenerationDTO> autoTransactionGenerationDTOS) {
        this.autoTransactionGenerationDTOS = autoTransactionGenerationDTOS;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
