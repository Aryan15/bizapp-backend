package com.coreerp.dto;

public class GlobalSettingDTO {

	private Long id;
	private String deleted;
	private Long financialYearId;
	private Long stockIncreaseOnId;
	private Long stockDecreaseOnId;
	private Integer itemLevelTax;
	private Integer itemLevelDiscount;
	private String browserPath;
	private Integer salePrice;
	private Integer purchasePrice;
	private Integer specialPriceCalculation;
	private Integer inclusiveTax;
	private Long barCodeStatus;
	private Long printStatus;
	private Long includeSignatureStatus;
	private String helpDocument;
	private Double sgstPercentage;
	private Double cgstPercentage;
	private Integer disableItemGroup;
	private Integer cutomerMateriaBinding;
	private Integer showOnlyCustomerMaterialInSales;
	private Integer roundOffTaxTransaction;
	private Integer stockCheckRequired;
	private Integer itemLevelTaxPurchase;
	private Integer enableTally;
	private Integer allowReuse;
	private Integer allowPartyCode;
	private Integer allowEwayBill;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public Long getFinancialYearId() {
		return financialYearId;
	}
	public void setFinancialYearId(Long financialYearId) {
		this.financialYearId = financialYearId;
	}
	public Long getStockIncreaseOnId() {
		return stockIncreaseOnId;
	}
	public void setStockIncreaseOnId(Long stockIncreaseOnId) {
		this.stockIncreaseOnId = stockIncreaseOnId;
	}
	public Long getStockDecreaseOnId() {
		return stockDecreaseOnId;
	}
	public void setStockDecreaseOnId(Long stockDecreaseOnId) {
		this.stockDecreaseOnId = stockDecreaseOnId;
	}
	public Integer getItemLevelTax() {
		return itemLevelTax;
	}
	public void setItemLevelTax(Integer itemLevelTax) {
		this.itemLevelTax = itemLevelTax;
	}
	public Integer getItemLevelDiscount() {
		return itemLevelDiscount;
	}
	public void setItemLevelDiscount(Integer itemLevelDiscount) {
		this.itemLevelDiscount = itemLevelDiscount;
	}
	public String getBrowserPath() {
		return browserPath;
	}
	public void setBrowserPath(String browserPath) {
		this.browserPath = browserPath;
	}
	public Integer getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(Integer salePrice) {
		this.salePrice = salePrice;
	}
	public Integer getPurchasePrice() {
		return purchasePrice;
	}
	public void setPurchasePrice(Integer purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	public Integer getSpecialPriceCalculation() {
		return specialPriceCalculation;
	}
	public void setSpecialPriceCalculation(Integer specialPriceCalculation) {
		this.specialPriceCalculation = specialPriceCalculation;
	}
	public Integer getInclusiveTax() {
		return inclusiveTax;
	}
	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}
	public Long getBarCodeStatus() {
		return barCodeStatus;
	}
	public void setBarCodeStatus(Long barCodeStatus) {
		this.barCodeStatus = barCodeStatus;
	}
	public Long getPrintStatus() {
		return printStatus;
	}
	public void setPrintStatus(Long printStatus) {
		this.printStatus = printStatus;
	}
	public Long getIncludeSignatureStatus() {
		return includeSignatureStatus;
	}
	public void setIncludeSignatureStatus(Long includeSignatureStatus) {
		this.includeSignatureStatus = includeSignatureStatus;
	}
	public String getHelpDocument() {
		return helpDocument;
	}
	public void setHelpDocument(String helpDocument) {
		this.helpDocument = helpDocument;
	}
	public Double getSgstPercentage() {
		return sgstPercentage;
	}
	public void setSgstPercentage(Double sgstPercentage) {
		this.sgstPercentage = sgstPercentage;
	}
	public Double getCgstPercentage() {
		return cgstPercentage;
	}
	public void setCgstPercentage(Double cgstPercentage) {
		this.cgstPercentage = cgstPercentage;
	}
	public Integer getDisableItemGroup() {
		return disableItemGroup;
	}
	public void setDisableItemGroup(Integer disableItemGroup) {
		this.disableItemGroup = disableItemGroup;
	}
	public Integer getCutomerMateriaBinding() {
		return cutomerMateriaBinding;
	}
	public void setCutomerMateriaBinding(Integer cutomerMateriaBinding) {
		this.cutomerMateriaBinding = cutomerMateriaBinding;
	}
	public Integer getShowOnlyCustomerMaterialInSales() {
		return showOnlyCustomerMaterialInSales;
	}
	public void setShowOnlyCustomerMaterialInSales(Integer showOnlyCustomerMaterialInSales) {
		this.showOnlyCustomerMaterialInSales = showOnlyCustomerMaterialInSales;
	}
	
	
	public Integer getRoundOffTaxTransaction() {
		return roundOffTaxTransaction;
	}
	public void setRoundOffTaxTransaction(Integer roundOffTaxTransaction) {
		this.roundOffTaxTransaction = roundOffTaxTransaction;
	}
	
	
	public Integer getStockCheckRequired() {
		return stockCheckRequired;
	}
	public void setStockCheckRequired(Integer stockCheckRequired) {
		this.stockCheckRequired = stockCheckRequired;
	}

	@Override
	public String toString() {
		return "GlobalSettingDTO{" +
				"id=" + id +
				", deleted='" + deleted + '\'' +
				", financialYearId=" + financialYearId +
				", stockIncreaseOnId=" + stockIncreaseOnId +
				", stockDecreaseOnId=" + stockDecreaseOnId +
				", itemLevelTax=" + itemLevelTax +
				", itemLevelDiscount=" + itemLevelDiscount +
				", browserPath='" + browserPath + '\'' +
				", salePrice=" + salePrice +
				", purchasePrice=" + purchasePrice +
				", specialPriceCalculation=" + specialPriceCalculation +
				", inclusiveTax=" + inclusiveTax +
				", barCodeStatus=" + barCodeStatus +
				", printStatus=" + printStatus +
				", includeSignatureStatus=" + includeSignatureStatus +
				", helpDocument='" + helpDocument + '\'' +
				", sgstPercentage=" + sgstPercentage +
				", cgstPercentage=" + cgstPercentage +
				", disableItemGroup=" + disableItemGroup +
				", cutomerMateriaBinding=" + cutomerMateriaBinding +
				", showOnlyCustomerMaterialInSales=" + showOnlyCustomerMaterialInSales +
				", roundOffTaxTransaction=" + roundOffTaxTransaction +
				", stockCheckRequired=" + stockCheckRequired +
				", itemLevelTaxPurchase=" + itemLevelTaxPurchase +
				", enableTally=" + enableTally +
				", allowReuse=" + allowReuse +
				", allowPartyCode=" + allowPartyCode +
				", allowEwayBill=" + allowEwayBill +
				'}';
	}

	public Integer getItemLevelTaxPurchase() {
		return itemLevelTaxPurchase;
	}

	public void setItemLevelTaxPurchase(Integer itemLevelTaxPurchase) {
		this.itemLevelTaxPurchase = itemLevelTaxPurchase;
	}

	public Integer getEnableTally() {
		return enableTally;
	}

	public void setEnableTally(Integer enableTally) {
		this.enableTally = enableTally;
	}

	public Integer getAllowReuse() {
		return allowReuse;
	}

	public void setAllowReuse(Integer allowReuse) {
		this.allowReuse = allowReuse;
	}

	public Integer getAllowPartyCode() {
		return allowPartyCode;
	}

	public void setAllowPartyCode(Integer allowPartyCode) {
		this.allowPartyCode = allowPartyCode;
	}

	public Integer getAllowEwayBill() {
		return allowEwayBill;
	}

	public void setAllowEwayBill(Integer allowEwayBill) {
		this.allowEwayBill = allowEwayBill;
	}
}
