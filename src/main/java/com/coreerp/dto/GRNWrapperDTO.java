package com.coreerp.dto;

import java.util.List;

public class GRNWrapperDTO {

	private List<String> itemToBeRemove;
	private GRNHeaderDTO grnHeader;
	public List<String> getItemToBeRemove() {
		return itemToBeRemove;
	}
	public void setItemToBeRemove(List<String> itemToBeRemove) {
		this.itemToBeRemove = itemToBeRemove;
	}
	public GRNHeaderDTO getGrnHeader() {
		return grnHeader;
	}
	public void setGrnHeader(GRNHeaderDTO grnHeader) {
		this.grnHeader = grnHeader;
	}
	
	
	
}
