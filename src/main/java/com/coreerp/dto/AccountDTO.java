package com.coreerp.dto;

public class AccountDTO {

	private Long id;
	
	private String accountNumber;
	
	private String name;
	
	private String description;
	
	private Long accountClassId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getAccountClassId() {
		return accountClassId;
	}

	public void setAccountClassId(Long accountClassId) {
		this.accountClassId = accountClassId;
	}
	
	
}
