package com.coreerp.dto;

import java.util.List;

import com.coreerp.reports.model.DeliveryChallanItemModel;

public class DeliveryChallanItemReportDTO {

	private List<DeliveryChallanItemModel> deliveryChallanReportItems;
	
	private Long totalCount;

	public List<DeliveryChallanItemModel> getDeliveryChallanReportItems() {
		return deliveryChallanReportItems;
	}

	public void setDeliveryChallanReportItems(List<DeliveryChallanItemModel> deliveryChallanReportItems) {
		this.deliveryChallanReportItems = deliveryChallanReportItems;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
