package com.coreerp.dto;

import java.util.List;

public class QuotationWrapperDTO {

	private QuotationDTO quotationHeader;
    private List<String> itemToBeRemove;
	public QuotationDTO getQuotationHeader() {
		return quotationHeader;
	}
	public void setQuotationHeader(QuotationDTO quotationHeader) {
		this.quotationHeader = quotationHeader;
	}
	public List<String> getItemToBeRemove() {
		return itemToBeRemove;
	}
	public void setItemToBeRemove(List<String> itemToBeRemove) {
		this.itemToBeRemove = itemToBeRemove;
	}
    
    
}
