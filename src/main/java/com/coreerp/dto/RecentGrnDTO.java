package com.coreerp.dto;

import java.util.List;

public class RecentGrnDTO {

	private List<GRNHeaderDTO> grnHeaders;
	
	private Long totalCount;

	public List<GRNHeaderDTO> getGrnHeaders() {
		return grnHeaders;
	}

	public void setGrnHeaders(List<GRNHeaderDTO> grnHeaders) {
		this.grnHeaders = grnHeaders;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
