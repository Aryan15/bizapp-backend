package com.coreerp.dto;



import java.util.Date;

public class TallyExportTrackerDTO {
    private Date fromDate;

    private Date toDate;

    private String invoiceType;

    private Integer voucherCount;

    private Integer stockItemCount;

    private String partyType;

    private Integer partyCount;
      private Date dateAndTime;


    public Date getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public Integer getVoucherCount() {
        return voucherCount;
    }

    public void setVoucherCount(Integer voucherCount) {
        this.voucherCount = voucherCount;
    }

    public Integer getStockItemCount() {
        return stockItemCount;
    }

    public void setStockItemCount(Integer stockItemCount) {
        this.stockItemCount = stockItemCount;
    }

    public String getPartyType() {
        return partyType;
    }

    public void setPartyType(String partyType) {
        this.partyType = partyType;
    }

    public Integer getPartyCount() {
        return partyCount;
    }

    public void setPartyCount(Integer partyCount) {
        this.partyCount = partyCount;
    }
}
