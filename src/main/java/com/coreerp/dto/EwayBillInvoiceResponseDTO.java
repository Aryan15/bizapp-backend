package com.coreerp.dto;

import java.util.Date;
import java.util.List;

public class EwayBillInvoiceResponseDTO {

    private boolean Flag;

    private String Message;

    private String JsonData;

    private String ewayBillNo;

    private String ewayBillDate;

    private String validUpto;

    private String alert;

    private String docDate;

    private String docNo;

    private String pdfUrl;

    private String detailedpdfUrl;

    private String error_log_ids;

    public boolean isFlag() {
        return Flag;
    }

    public void setFlag(boolean flag) {
        Flag = flag;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getJsonData() {
        return JsonData;
    }

    public void setJsonData(String jsonData) {
        JsonData = jsonData;
    }

    public String getEwayBillNo() {
        return ewayBillNo;
    }

    public void setEwayBillNo(String ewayBillNo) {
        this.ewayBillNo = ewayBillNo;
    }

    public String getEwayBillDate() {
        return ewayBillDate;
    }

    public void setEwayBillDate(String ewayBillDate) {
        this.ewayBillDate = ewayBillDate;
    }

    public String getValidUpto() {
        return validUpto;
    }

    public void setValidUpto(String validUpto) {
        this.validUpto = validUpto;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public String getDetailedpdfUrl() {
        return detailedpdfUrl;
    }

    public void setDetailedpdfUrl(String detailedpdfUrl) {
        this.detailedpdfUrl = detailedpdfUrl;
    }

    public String getError_log_ids() {
        return error_log_ids;
    }

    public void setError_log_ids(String error_log_ids) {
        this.error_log_ids = error_log_ids;
    }

    @Override
    public String toString() {
        return "EwayBillInvoiceResponseDTO{" +
                "Flag=" + Flag +
                ", Message='" + Message + '\'' +
                ", JsonData='" + JsonData + '\'' +
                ", ewayBillNo='" + ewayBillNo + '\'' +
                ", ewayBillDate='" + ewayBillDate + '\'' +
                ", validUpto='" + validUpto + '\'' +
                ", alert='" + alert + '\'' +
                ", docDate='" + docDate + '\'' +
                ", docNo='" + docNo + '\'' +
                ", pdfUrl='" + pdfUrl + '\'' +
                ", detailedpdfUrl='" + detailedpdfUrl + '\'' +
                ", error_log_ids='" + error_log_ids + '\'' +
                '}';
    }
}
