package com.coreerp.dto;

public class EwayBillVehicleUpdateListInputDTO {

    private Long ewbNo;
    private String vehicleNo;
    private String fromPlace;
    private Integer fromState;
    private String reasonCode;
    private String reasonRem;
    private String transDocNo;
    private String transDocDate;
    private String transMode;
    private String vehicleType;
    //extra fields for response
    private String tripshtNo;
    private String  userGSTINTransin;
    private String  enteredDate;
    private Integer GroupNo;

    public Long getEwbNo() {
        return ewbNo;
    }

    public void setEwbNo(Long ewbNo) {
        this.ewbNo = ewbNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public Integer getFromState() {
        return fromState;
    }

    public void setFromState(Integer fromState) {
        this.fromState = fromState;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonRem() {
        return reasonRem;
    }

    public void setReasonRem(String reasonRem) {
        this.reasonRem = reasonRem;
    }

    public String getTransDocNo() {
        return transDocNo;
    }

    public void setTransDocNo(String transDocNo) {
        this.transDocNo = transDocNo;
    }

    public String getTransDocDate() {
        return transDocDate;
    }

    public void setTransDocDate(String transDocDate) {
        this.transDocDate = transDocDate;
    }

    public String getTransMode() {
        return transMode;
    }

    public void setTransMode(String transMode) {
        this.transMode = transMode;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getFromPlace() {
        return fromPlace;
    }

    public void setFromPlace(String fromPlace) {
        this.fromPlace = fromPlace;
    }

    public String getTripshtNo() {
        return tripshtNo;
    }

    public void setTripshtNo(String tripshtNo) {
        this.tripshtNo = tripshtNo;
    }

    public String getUserGSTINTransin() {
        return userGSTINTransin;
    }

    public void setUserGSTINTransin(String userGSTINTransin) {
        this.userGSTINTransin = userGSTINTransin;
    }

    public String getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(String enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Integer getGroupNo() {
        return GroupNo;
    }

    public void setGroupNo(Integer groupNo) {
        GroupNo = groupNo;
    }

    @Override
    public String toString() {
        return "EwayBillVehicleUpdateListInputDTO{" +
                "ewbNo=" + ewbNo +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", fromPlace='" + fromPlace + '\'' +
                ", fromState=" + fromState +
                ", reasonCode='" + reasonCode + '\'' +
                ", reasonRem='" + reasonRem + '\'' +
                ", transDocNo='" + transDocNo + '\'' +
                ", transDocDate='" + transDocDate + '\'' +
                ", transMode='" + transMode + '\'' +
                ", vehicleType='" + vehicleType + '\'' +
                ", tripshtNo='" + tripshtNo + '\'' +
                ", userGSTINTransin='" + userGSTINTransin + '\'' +
                ", enteredDate='" + enteredDate + '\'' +
                ", GroupNo=" + GroupNo +
                '}';
    }
}
