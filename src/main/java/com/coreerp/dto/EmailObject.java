package com.coreerp.dto;

public class EmailObject {
	private String otp;
	private String name;
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "EmailObject [otp=" + otp + ", name=" + name + "]";
	}
	
	
}
