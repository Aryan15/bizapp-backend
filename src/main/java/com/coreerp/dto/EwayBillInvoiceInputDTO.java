package com.coreerp.dto;

import java.util.List;

public class EwayBillInvoiceInputDTO {

    private String  version   ;
    private String userCode ;
    private String clientCode ;
    private String password ;
    private List<EwayBillInvoiceBillInputDTO>  billLists;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<EwayBillInvoiceBillInputDTO> getBillLists() {
        return billLists;
    }

    public void setBillLists(List<EwayBillInvoiceBillInputDTO> billLists) {
        this.billLists = billLists;
    }

    @Override
    public String toString() {
        return "EwayBillInvoiceInputDTO{" +
                "version='" + version + '\'' +
                ", userCode='" + userCode + '\'' +
                ", clientCode='" + clientCode + '\'' +
                ", password='" + password + '\'' +
                ", billLists=" + billLists +
                '}';
    }
}
