package com.coreerp.dto;

import java.util.List;

public class EwayBillExtendValidityInputDTO {

    private String userCode ;
    private String clientCode ;
    private String password ;
    private List<EwayBillExtendValidityListInputDTO> extendewblist;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<EwayBillExtendValidityListInputDTO> getExtendewblist() {
        return extendewblist;
    }

    public void setExtendewblist(List<EwayBillExtendValidityListInputDTO> extendewblist) {
        this.extendewblist = extendewblist;
    }

    @Override
    public String toString() {
        return "EwayBillExtendValidityInputDTO{" +
                "userCode='" + userCode + '\'' +
                ", clientCode='" + clientCode + '\'' +
                ", password='" + password + '\'' +
                ", extendewblist=" + extendewblist +
                '}';
    }
}
