package com.coreerp.dto;

public class MaterialDTO  implements Comparable<MaterialDTO> {

	private Long id;
	private String deleted;
	private String name;
	private String partNumber;
	private Long supplyTypeId;
	private Long materialTypeId;
	private Long unitOfMeasurementId;
	private String unitOfMeasurementName;
	private Double price;
	private String specification;
	private Double stock;
	private Long companyId;
	private Double buyingPrice;
	private Long taxId;
	private Double mrp;
	private Double discountPercentage;
	private Long partyId;
	private String hsnCode;
	private Double cessPercentage;
	private String imagePath;
	private Double openingStock;
	private Double minimumStock;
	private Integer isContainer;
	private String outName;
	private String outPartNumber;
	private String materialTypeName;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	
	
	public Long getSupplyTypeId() {
		return supplyTypeId;
	}
	public void setSupplyTypeId(Long supplyTypeId) {
		this.supplyTypeId = supplyTypeId;
	}
	public Long getMaterialTypeId() {
		return materialTypeId;
	}
	public void setMaterialTypeId(Long materialTypeId) {
		this.materialTypeId = materialTypeId;
	}
	public Long getUnitOfMeasurementId() {
		return unitOfMeasurementId;
	}
	public void setUnitOfMeasurementId(Long unitOfMeasurementId) {
		this.unitOfMeasurementId = unitOfMeasurementId;
	}
	
	
	public String getUnitOfMeasurementName() {
		return unitOfMeasurementName;
	}
	public void setUnitOfMeasurementName(String unitOfMeasurementName) {
		this.unitOfMeasurementName = unitOfMeasurementName;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getSpecification() {
		return specification;
	}
	public void setSpecification(String specification) {
		this.specification = specification;
	}
	public Double getStock() {
		return stock;
	}
	public void setStock(Double stock) {
		this.stock = stock;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Double getBuyingPrice() {
		return buyingPrice;
	}
	public void setBuyingPrice(Double buyingPrice) {
		this.buyingPrice = buyingPrice;
	}
	public Long getTaxId() {
		return taxId;
	}
	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	public Long getPartyId() {
		return partyId;
	}
	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public Double getCessPercentage() {
		return cessPercentage;
	}
	public void setCessPercentage(Double cessPercentage) {
		this.cessPercentage = cessPercentage;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	@Override
	public int compareTo(MaterialDTO other) {
		return name.toLowerCase().compareTo(other.name.toLowerCase());
	}
	public Double getOpeningStock() {
		return openingStock;
	}
	public void setOpeningStock(Double openingStock) {
		this.openingStock = openingStock;
	}
	public Double getMinimumStock() {
		return minimumStock;
	}
	public void setMinimumStock(Double minimumStock) {
		this.minimumStock = minimumStock;
	}

	public String getOutName() {
		return outName;
	}

	public void setOutName(String outName) {
		this.outName = outName;
	}

	public String getOutPartNumber() {
		return outPartNumber;
	}

	public void setOutPartNumber(String outPartNumber) {
		this.outPartNumber = outPartNumber;
	}

	public Integer getIsContainer() {
		return isContainer;
	}

	public void setIsContainer(Integer isContainer) {
		this.isContainer = isContainer;
	}
	public String getMaterialTypeName() {
		return materialTypeName;
	}
	public void setMaterialTypeName(String materialTypeName) {
		this.materialTypeName = materialTypeName;
	}
	
	
}
