package com.coreerp.dto;

import java.util.List;

import com.coreerp.model.Address;

public class PartyDTO implements Comparable<PartyDTO> {

	private Long id;
	
	private String name;
	
	private String address;
	
	private Long countryId;
	
	private Long stateId;
	
	private String stateCode;
	
	private String stateName;

	private Long cityId;
	
	private Long areaId;

	private String areaName;
	
	private String pinCode;
	
	private String primaryTelephone;
	
	private String secondaryTelephone;
	
	private String primaryMobile;
	
	private String secondaryMobile;
	
	private String email;
	
	private String contactPersonName;
	
	private String contactPersonNumber;
	
	private String webSite;
	
	private Long partyTypeId;
	
	private String partyTypeName ;
	
	private String deleted;
	
	private String billAddress;
	
	private String panNumber;
	
	private Long gstRegistrationTypeId;
	
	private String gstNumber;
	
	private List<Address> addressesListDTO;
	
	private Integer dueDaysLimit;
	
	private Integer isIgst;

	private Integer partyId;

	private  String partyCode;

	private Long partyCurrencyId;
    private String partyCurrencyName;
	//private Integer portStateId;




	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	
	
	
	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	


	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public Long getPartyTypeId() {
		return partyTypeId;
	}

	public void setPartyTypeId(Long partyTypeId) {
		this.partyTypeId = partyTypeId;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getPrimaryTelephone() {
		return primaryTelephone;
	}

	public void setPrimaryTelephone(String primaryTelephone) {
		this.primaryTelephone = primaryTelephone;
	}

	public String getSecondaryTelephone() {
		return secondaryTelephone;
	}

	public void setSecondaryTelephone(String secondaryTelephone) {
		this.secondaryTelephone = secondaryTelephone;
	}

	public String getPrimaryMobile() {
		return primaryMobile;
	}

	public void setPrimaryMobile(String primaryMobile) {
		this.primaryMobile = primaryMobile;
	}

	public String getSecondaryMobile() {
		return secondaryMobile;
	}

	public void setSecondaryMobile(String secondaryMobile) {
		this.secondaryMobile = secondaryMobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getContactPersonNumber() {
		return contactPersonNumber;
	}

	public void setContactPersonNumber(String contactPersonNumber) {
		this.contactPersonNumber = contactPersonNumber;
	}

	public String getWebSite() {
		return webSite;
	}

	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	
	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public String getBillAddress() {
		return billAddress;
	}

	public void setBillAddress(String billAddress) {
		this.billAddress = billAddress;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	
	

	public Long getGstRegistrationTypeId() {
		return gstRegistrationTypeId;
	}

	public void setGstRegistrationTypeId(Long gstRegistrationTypeId) {
		this.gstRegistrationTypeId = gstRegistrationTypeId;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}
	
	

	public String getPartyTypeName() {
		return partyTypeName;
	}

	public void setPartyTypeName(String partyTypeName) {
		this.partyTypeName = partyTypeName;
	}


	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public List<Address> getAddressesListDTO() {
		return addressesListDTO;
	}

	public void setAddressesListDTO(List<Address> addressesListDTO) {
		this.addressesListDTO = addressesListDTO;
	}


	@Override
	public int compareTo(PartyDTO other) {
		return name.toLowerCase().compareTo(other.name.toLowerCase());
	}

	public Integer getDueDaysLimit() {
		return dueDaysLimit;
	}

	public void setDueDaysLimit(Integer dueDaysLimit) {
		this.dueDaysLimit = dueDaysLimit;
	}

	public Integer getIsIgst() {
		return isIgst;
	}

	public void setIsIgst(Integer isIgst) {
		this.isIgst = isIgst;
	}

	public Integer getPartyId() {
		return partyId;
	}

	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public Long getPartyCurrencyId() {
		return partyCurrencyId;
	}

	public void setPartyCurrencyId(Long partyCurrencyId) {
		this.partyCurrencyId = partyCurrencyId;
	}

	public String getPartyCurrencyName() {
		return partyCurrencyName;
	}

	public void setPartyCurrencyName(String partyCurrencyName) {
		this.partyCurrencyName = partyCurrencyName;
	}

	@Override
	public String toString() {
		return "PartyDTO{" +
				"id=" + id +
				", name='" + name + '\'' +
				", address='" + address + '\'' +
				", countryId=" + countryId +
				", stateId=" + stateId +
				", stateCode='" + stateCode + '\'' +
				", stateName='" + stateName + '\'' +
				", cityId=" + cityId +
				", areaId=" + areaId +
				", areaName='" + areaName + '\'' +
				", pinCode='" + pinCode + '\'' +
				", primaryTelephone='" + primaryTelephone + '\'' +
				", secondaryTelephone='" + secondaryTelephone + '\'' +
				", primaryMobile='" + primaryMobile + '\'' +
				", secondaryMobile='" + secondaryMobile + '\'' +
				", email='" + email + '\'' +
				", contactPersonName='" + contactPersonName + '\'' +
				", contactPersonNumber='" + contactPersonNumber + '\'' +
				", webSite='" + webSite + '\'' +
				", partyTypeId=" + partyTypeId +
				", partyTypeName='" + partyTypeName + '\'' +
				", deleted='" + deleted + '\'' +
				", billAddress='" + billAddress + '\'' +
				", panNumber='" + panNumber + '\'' +
				", gstRegistrationTypeId=" + gstRegistrationTypeId +
				", gstNumber='" + gstNumber + '\'' +
				", addressesListDTO=" + addressesListDTO +
				", dueDaysLimit=" + dueDaysLimit +
				", isIgst=" + isIgst +
				", partyId=" + partyId +
				", partyCode='" + partyCode + '\'' +
				", partyCurrencyId=" + partyCurrencyId +
				", partyCurrencyName=" + partyCurrencyName +
				'}';
	}
}
