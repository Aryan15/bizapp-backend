package com.coreerp.dto;

public class EmailDTO {
    private Long id;

    private String deleted;

   private String emailDebug;
    private String  emailFrom ;
    private String emailHost ;
    private String emailPassword;
    private Integer emailPort ;
    private String emailSmtpAuth;
    private String emailSmtpStartTlsEnable;
    private String emailTransportProtocol;
    private String emailUserName;
    private Integer emailEnabled ;
    private String emailText;
    private String emailSubject;
    private Integer emailType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getEmailDebug() {
        return emailDebug;
    }

    public void setEmailDebug(String emailDebug) {
        this.emailDebug = emailDebug;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    public String getEmailHost() {
        return emailHost;
    }

    public void setEmailHost(String emailHost) {
        this.emailHost = emailHost;
    }

    public String getEmailPassword() {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    public Integer getEmailPort() {
        return emailPort;
    }

    public void setEmailPort(Integer emailPort) {
        this.emailPort = emailPort;
    }

    public String getEmailSmtpAuth() {
        return emailSmtpAuth;
    }

    public void setEmailSmtpAuth(String emailSmtpAuth) {
        this.emailSmtpAuth = emailSmtpAuth;
    }

    public String getEmailSmtpStartTlsEnable() {
        return emailSmtpStartTlsEnable;
    }

    public void setEmailSmtpStartTlsEnable(String emailSmtpStartTlsEnable) {
        this.emailSmtpStartTlsEnable = emailSmtpStartTlsEnable;
    }

    public String getEmailTransportProtocol() {
        return emailTransportProtocol;
    }

    public void setEmailTransportProtocol(String emailTransportProtocol) {
        this.emailTransportProtocol = emailTransportProtocol;
    }

    public String getEmailUserName() {
        return emailUserName;
    }

    public void setEmailUserName(String emailUserName) {
        this.emailUserName = emailUserName;
    }

    public Integer getEmailEnabled() {
        return emailEnabled;
    }

    public void setEmailEnabled(Integer emailEnabled) {
        this.emailEnabled = emailEnabled;
    }

    public String getEmailText() {
        return emailText;
    }

    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public Integer getEmailType() {
        return emailType;
    }

    public void setEmailType(Integer emailType) {
        this.emailType = emailType;
    }
}
