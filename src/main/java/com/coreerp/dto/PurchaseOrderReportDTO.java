package com.coreerp.dto;

import java.util.List;

import com.coreerp.reports.model.PoBalanceReport;
import com.coreerp.reports.model.PurchaseOrderReport;

public class PurchaseOrderReportDTO {

	private List<PurchaseOrderReport> purchaseOrderReports;
	private List<PoBalanceReport> poBalanceReport;
	
	private Long totalCount;

	public List<PurchaseOrderReport> getPurchaseOrderReports() {
		return purchaseOrderReports;
	}

	public void setPurchaseOrderReports(List<PurchaseOrderReport> purchaseOrderReports) {
		this.purchaseOrderReports = purchaseOrderReports;
	}

	public List<PoBalanceReport> getPoBalanceReport() {
		return poBalanceReport;
	}

	public void setPoBalanceReport(List<PoBalanceReport> poBalanceReport) {
		this.poBalanceReport = poBalanceReport;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
