package com.coreerp.dto;

public class EwayBillInvoiceItemInputDTO {

    private Integer itemNo ;

    private String productName ;

    private String productDesc;

    private Integer hsnCode;

    private Double quantity;

    private String qtyUnit;

    private Double taxableAmount;

    private Double sgstRate;

    private Double cgstRate;

    private Double igstRate;

    private Double cessRate;

    private Double cessNonAdvol;

    public Integer getItemNo() {
        return itemNo;
    }

    public void setItemNo(Integer itemNo) {
        this.itemNo = itemNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public Integer getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(Integer hsnCode) {
        this.hsnCode = hsnCode;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getQtyUnit() {
        return qtyUnit;
    }

    public void setQtyUnit(String qtyUnit) {
        this.qtyUnit = qtyUnit;
    }

    public Double getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(Double taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public Double getSgstRate() {
        return sgstRate;
    }

    public void setSgstRate(Double sgstRate) {
        this.sgstRate = sgstRate;
    }

    public Double getCgstRate() {
        return cgstRate;
    }

    public void setCgstRate(Double cgstRate) {
        this.cgstRate = cgstRate;
    }

    public Double getIgstRate() {
        return igstRate;
    }

    public void setIgstRate(Double igstRate) {
        this.igstRate = igstRate;
    }

    public Double getCessRate() {
        return cessRate;
    }

    public void setCessRate(Double cessRate) {
        this.cessRate = cessRate;
    }

    public Double getCessNonAdvol() {
        return cessNonAdvol;
    }

    public void setCessNonAdvol(Double cessNonAdvol) {
        this.cessNonAdvol = cessNonAdvol;
    }

    @Override
    public String toString() {
        return "EwayBillInvoiceItemInputDTO{" +
                "itemNo='" + itemNo + '\'' +
                ", productName='" + productName + '\'' +
                ", productDesc='" + productDesc + '\'' +
                ", hsnCode=" + hsnCode +
                ", quantity=" + quantity +
                ", qtyUnit='" + qtyUnit + '\'' +
                ", taxableAmount=" + taxableAmount +
                ", sgstRate=" + sgstRate +
                ", cgstRate=" + cgstRate +
                ", igstRate=" + igstRate +
                ", cessRate=" + cessRate +
                ", cessNonAdvol=" + cessNonAdvol +
                '}';
    }
}
