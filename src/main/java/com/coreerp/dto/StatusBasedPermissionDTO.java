package com.coreerp.dto;

public class StatusBasedPermissionDTO {

	private Long id;
    private Long statusId;
    private String deleted= "N";
	private Integer permissionToCreate;
	private Integer permissionToUpdate;
	private Integer permissionToDelete;
	private Integer permissionToCancel;
	private Integer permissionToApprove;
	private Integer permissionToPrint;
	private Integer permissionToDraft;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public Integer getPermissionToCreate() {
		return permissionToCreate;
	}
	public void setPermissionToCreate(Integer permissionToCreate) {
		this.permissionToCreate = permissionToCreate;
	}
	public Integer getPermissionToUpdate() {
		return permissionToUpdate;
	}
	public void setPermissionToUpdate(Integer permissionToUpdate) {
		this.permissionToUpdate = permissionToUpdate;
	}
	public Integer getPermissionToDelete() {
		return permissionToDelete;
	}
	public void setPermissionToDelete(Integer permissionToDelete) {
		this.permissionToDelete = permissionToDelete;
	}
	public Integer getPermissionToCancel() {
		return permissionToCancel;
	}
	public void setPermissionToCancel(Integer permissionToCancel) {
		this.permissionToCancel = permissionToCancel;
	}
	public Integer getPermissionToApprove() {
		return permissionToApprove;
	}
	public void setPermissionToApprove(Integer permissionToApprove) {
		this.permissionToApprove = permissionToApprove;
	}
	public Integer getPermissionToPrint() {
		return permissionToPrint;
	}
	public void setPermissionToPrint(Integer permissionToPrint) {
		this.permissionToPrint = permissionToPrint;
	}
	public Integer getPermissionToDraft() {
		return permissionToDraft;
	}
	public void setPermissionToDraft(Integer permissionToDraft) {
		this.permissionToDraft = permissionToDraft;
	}
	
	
}
