package com.coreerp.dto;

import java.util.Date;
import java.util.List;

public class PayableReceivableItemDTO {
	
	private String id;
	private String payableReceivableHeaderId;
	private String invoiceHeaderId;
	private String invoiceNumber;
	private Date invoiceDate;
	private Date paymentDueDate;
	private Double invoiceAmount;
	private Double paidAmount;
	private Double dueAmount;
	private Double payingAmount;
	private String invoiceStatus;
	private String paymentItemStatus;
	private Double tdsPercentage;
	private Double tdsAmount;
	private Double amountAfterTds;
	private Double debitAmount;
	private List<String> creditDebitHeaderId;
	private Integer slNo;
	private Double taxableAmount;
	private Double taxAmount;
	private Double preTdsPer;
	private Double preTdsAmount;
	private Double roundOff;


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPayableReceivableHeaderId() {
		return payableReceivableHeaderId;
	}
	public void setPayableReceivableHeaderId(String payableReceivableHeaderId) {
		this.payableReceivableHeaderId = payableReceivableHeaderId;
	}
	
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getInvoiceHeaderId() {
		return invoiceHeaderId;
	}
	public void setInvoiceHeaderId(String invoiceHeaderId) {
		this.invoiceHeaderId = invoiceHeaderId;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Date getPaymentDueDate() {
		return paymentDueDate;
	}
	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Double getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}
	public Double getPayingAmount() {
		return payingAmount;
	}
	public void setPayingAmount(Double payingAmount) {
		this.payingAmount = payingAmount;
	}
	public String getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	public String getPaymentItemStatus() {
		return paymentItemStatus;
	}
	public void setPaymentItemStatus(String paymentItemStatus) {
		this.paymentItemStatus = paymentItemStatus;
	}
	public Double getTdsPercentage() {
		return tdsPercentage;
	}
	public void setTdsPercentage(Double tdsPercentage) {
		this.tdsPercentage = tdsPercentage;
	}
	public Double getTdsAmount() {
		return tdsAmount;
	}
	public void setTdsAmount(Double tdsAmount) {
		this.tdsAmount = tdsAmount;
	}
	public Double getAmountAfterTds() {
		return amountAfterTds;
	}
	public void setAmountAfterTds(Double amountAfterTds) {
		this.amountAfterTds = amountAfterTds;
	}
	public Double getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}
	public List<String> getCreditDebitHeaderId() {
		return creditDebitHeaderId;
	}
	public void setCreditDebitHeaderId(List<String> creditDebitHeaderId) {
		this.creditDebitHeaderId = creditDebitHeaderId;
	}
	public Integer getSlNo() {
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public Double getTaxableAmount() {
		return taxableAmount;
	}

	public void setTaxableAmount(Double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Double getPreTdsPer() {
		return preTdsPer;
	}

	public void setPreTdsPer(Double preTdsPer) {
		this.preTdsPer = preTdsPer;
	}


	public Double getPreTdsAmount() {
		return preTdsAmount;
	}

	public void setPreTdsAmount(Double preTdsAmount) {
		this.preTdsAmount = preTdsAmount;
	}

	public Double getRoundOff() {
		return roundOff;
	}

	public void setRoundOff(Double roundOff) {
		this.roundOff = roundOff;
	}
}
