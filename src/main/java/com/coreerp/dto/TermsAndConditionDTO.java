package com.coreerp.dto;

public class TermsAndConditionDTO {

	private Long id;
	private String name;
	private String termsAndCondition;
	private String paymentTerms;
	private String deliveryTerms;
	private Integer defaultTermsAndCondition;
	private Long transactionTypeId;
	private String deleted;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getTermsAndCondition() {
		return termsAndCondition;
	}
	public void setTermsAndCondition(String termsAndCondition) {
		this.termsAndCondition = termsAndCondition;
	}
	public String getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public String getDeliveryTerms() {
		return deliveryTerms;
	}
	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}
	public Integer getDefaultTermsAndCondition() {
		return defaultTermsAndCondition;
	}
	public void setDefaultTermsAndCondition(Integer defaultTermsAndCondition) {
		this.defaultTermsAndCondition = defaultTermsAndCondition;
	}
	public Long getTransactionTypeId() {
		return transactionTypeId;
	}
	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	
	
}
