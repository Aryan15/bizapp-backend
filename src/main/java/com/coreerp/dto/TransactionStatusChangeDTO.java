package com.coreerp.dto;

public class TransactionStatusChangeDTO {
	
	private Long transactionTypeId;
	
	private String transactionTypeName;
	
	private String transactionId;
	
	private String transactionNumber;
	
	private Long statusId;
	
	private String statusName;
	
	private String latest;
	
	private Long sourceTransactionTypeId;
	
	private String sourceTransactionTypeName;
	
	private String sourceTransactionId;
	
	private String sourceTransactionNumber;
	
	private String sourceOperation;

	public Long getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}


	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getLatest() {
		return latest;
	}

	public void setLatest(String latest) {
		this.latest = latest;
	}

	public Long getSourceTransactionTypeId() {
		return sourceTransactionTypeId;
	}

	public void setSourceTransactionTypeId(Long sourceTransactionTypeId) {
		this.sourceTransactionTypeId = sourceTransactionTypeId;
	}

	public String getSourceTransactionId() {
		return sourceTransactionId;
	}

	public void setSourceTransactionId(String sourceTransactionId) {
		this.sourceTransactionId = sourceTransactionId;
	}

	public String getSourceTransactionNumber() {
		return sourceTransactionNumber;
	}

	public void setSourceTransactionNumber(String sourceTransactionNumber) {
		this.sourceTransactionNumber = sourceTransactionNumber;
	}

	public String getSourceOperation() {
		return sourceOperation;
	}

	public void setSourceOperation(String sourceOperation) {
		this.sourceOperation = sourceOperation;
	}



	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getTransactionTypeName() {
		return transactionTypeName;
	}

	public void setTransactionTypeName(String transactionTypeName) {
		this.transactionTypeName = transactionTypeName;
	}

	public String getSourceTransactionTypeName() {
		return sourceTransactionTypeName;
	}

	public void setSourceTransactionTypeName(String sourceTransactionTypeName) {
		this.sourceTransactionTypeName = sourceTransactionTypeName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((latest == null) ? 0 : latest.hashCode());
		result = prime * result + ((sourceOperation == null) ? 0 : sourceOperation.hashCode());
		result = prime * result + ((sourceTransactionId == null) ? 0 : sourceTransactionId.hashCode());
		result = prime * result + ((sourceTransactionNumber == null) ? 0 : sourceTransactionNumber.hashCode());
		result = prime * result + ((sourceTransactionTypeId == null) ? 0 : sourceTransactionTypeId.hashCode());
		result = prime * result + ((sourceTransactionTypeName == null) ? 0 : sourceTransactionTypeName.hashCode());
		result = prime * result + ((statusId == null) ? 0 : statusId.hashCode());
		result = prime * result + ((statusName == null) ? 0 : statusName.hashCode());
		result = prime * result + ((transactionId == null) ? 0 : transactionId.hashCode());
		result = prime * result + ((transactionNumber == null) ? 0 : transactionNumber.hashCode());
		result = prime * result + ((transactionTypeId == null) ? 0 : transactionTypeId.hashCode());
		result = prime * result + ((transactionTypeName == null) ? 0 : transactionTypeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransactionStatusChangeDTO other = (TransactionStatusChangeDTO) obj;
		if (latest == null) {
			if (other.latest != null)
				return false;
		} else if (!latest.equals(other.latest))
			return false;
		if (sourceOperation == null) {
			if (other.sourceOperation != null)
				return false;
		} else if (!sourceOperation.equals(other.sourceOperation))
			return false;
		if (sourceTransactionId == null) {
			if (other.sourceTransactionId != null)
				return false;
		} else if (!sourceTransactionId.equals(other.sourceTransactionId))
			return false;
		if (sourceTransactionNumber == null) {
			if (other.sourceTransactionNumber != null)
				return false;
		} else if (!sourceTransactionNumber.equals(other.sourceTransactionNumber))
			return false;
		if (sourceTransactionTypeId == null) {
			if (other.sourceTransactionTypeId != null)
				return false;
		} else if (!sourceTransactionTypeId.equals(other.sourceTransactionTypeId))
			return false;
		if (sourceTransactionTypeName == null) {
			if (other.sourceTransactionTypeName != null)
				return false;
		} else if (!sourceTransactionTypeName.equals(other.sourceTransactionTypeName))
			return false;
		if (statusId == null) {
			if (other.statusId != null)
				return false;
		} else if (!statusId.equals(other.statusId))
			return false;
		if (statusName == null) {
			if (other.statusName != null)
				return false;
		} else if (!statusName.equals(other.statusName))
			return false;
		if (transactionId == null) {
			if (other.transactionId != null)
				return false;
		} else if (!transactionId.equals(other.transactionId))
			return false;
		if (transactionNumber == null) {
			if (other.transactionNumber != null)
				return false;
		} else if (!transactionNumber.equals(other.transactionNumber))
			return false;
		if (transactionTypeId == null) {
			if (other.transactionTypeId != null)
				return false;
		} else if (!transactionTypeId.equals(other.transactionTypeId))
			return false;
		if (transactionTypeName == null) {
			if (other.transactionTypeName != null)
				return false;
		} else if (!transactionTypeName.equals(other.transactionTypeName))
			return false;
		return true;
	}
	
	
	
}
