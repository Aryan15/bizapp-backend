package com.coreerp.dto;

import com.coreerp.model.Company;
import com.coreerp.model.FinancialYear;
import com.coreerp.model.Status;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

public class PettyCashHeaderDTO {
    private String id;
    private String pettyCashNumber;
    private Long peetCashId;
   private Long pettyCashTypeId;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date pettyCashDate;
    private Double totalIncomingAmount;
    private Double totalOutgoingAmount;
    private  Long isDayClosed;
    private String comments;
    private String approvedBy;
    private  Date approvedDate;
    private Long financialYearId;
    private Long companyId;
    private Long statusId;
    private String statusName;
    private Double totalAmount;
    private Double bankAmount;
    private Double cashAmount;
    private Double balanceAmount;
    private Long isLatest;
    private List<PettyCashItemDTO> pettyCashItems;

    public Long getFinancialYearId() {
        return financialYearId;
    }

    public void setFinancialYearId(Long financialYearId) {
        this.financialYearId = financialYearId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getPettyCashDate() {
        return pettyCashDate;
    }

    public void setPettyCashDate(Date pettyCashDate) {
        this.pettyCashDate = pettyCashDate;
    }

    public Double getTotalIncomingAmount() {
        return totalIncomingAmount;
    }

    public void setTotalIncomingAmount(Double totalIncomingAmount) {
        this.totalIncomingAmount = totalIncomingAmount;
    }

    public Double getTotalOutgoingAmount() {
        return totalOutgoingAmount;
    }

    public void setTotalOutgoingAmount(Double totalOutgoingAmount) {
        this.totalOutgoingAmount = totalOutgoingAmount;
    }

    public Long getIsDayClosed() {
        return isDayClosed;
    }

    public void setIsDayClosed(Long isDayClosed) {
        this.isDayClosed = isDayClosed;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }


    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getBankAmount() {
        return bankAmount;
    }

    public void setBankAmount(Double bankAmount) {
        this.bankAmount = bankAmount;
    }

    public Double getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(Double cashAmount) {
        this.cashAmount = cashAmount;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }


    public List<PettyCashItemDTO> getPettyCashItems() {
        return pettyCashItems;
    }

    public void setPettyCashItems(List<PettyCashItemDTO> pettyCashItems) {
        this.pettyCashItems = pettyCashItems;
    }

    public String getPettyCashNumber() {
        return pettyCashNumber;
    }

    public void setPettyCashNumber(String pettyCashNumber) {
        this.pettyCashNumber = pettyCashNumber;
    }

    public Long getPeetCashId() {
        return peetCashId;
    }

    public void setPeetCashId(Long peetCashId) {
        this.peetCashId = peetCashId;
    }

    public Long getPettyCashTypeId() {
        return pettyCashTypeId;
    }

    public void setPettyCashTypeId(Long pettyCashTypeId) {
        this.pettyCashTypeId = pettyCashTypeId;
    }


    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }


    public Long getIsLatest() {
        return isLatest;
    }

    public void setIsLatest(Long isLatest) {
        this.isLatest = isLatest;
    }
}
