package com.coreerp.dto;

import java.util.List;

import com.coreerp.model.Company;

public class MenuDTO {

	private Long id;
	
	private Integer menuOrder;
	
	private String name;
	
	//private Company company;
	
	
	//private MenuDTO parentMenu;
	
	private List<ActivityDTO> activities;
	
	private Integer menuLevel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getMenuOrder() {
		return menuOrder;
	}

	public void setMenuOrder(Integer menuOrder) {
		this.menuOrder = menuOrder;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public Company getCompany() {
//		return company;
//	}
//
//	public void setCompany(Company company) {
//		this.company = company;
//	}
	
	
//
//	public MenuDTO getParentMenu() {
//		return parentMenu;
//	}
//
//	public void setParentMenu(MenuDTO parentMenu) {
//		this.parentMenu = parentMenu;
//	}

	public List<ActivityDTO> getActivities() {
		return activities;
	}

	public void setActivities(List<ActivityDTO> activities) {
		this.activities = activities;
	}
	
	

	public Integer getMenuLevel() {
		return menuLevel;
	}

	public void setMenuLevel(Integer menuLevel) {
		this.menuLevel = menuLevel;
	}

	@Override
	public String toString() {
		return "MenuDTO [id=" + id + ", menuOrder=" + menuOrder + ", name=" + name  + "]";
	}
	
	
}
