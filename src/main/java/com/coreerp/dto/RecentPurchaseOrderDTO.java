package com.coreerp.dto;

import java.util.List;

public class RecentPurchaseOrderDTO {

	private List<PurchaseOrderDTO> purchaseOrderHeaders;
	
	private Long totalCount;

	public List<PurchaseOrderDTO> getPurchaseOrderHeaders() {
		return purchaseOrderHeaders;
	}

	public void setPurchaseOrderHeaders(List<PurchaseOrderDTO> purchaseOrderHeaders) {
		this.purchaseOrderHeaders = purchaseOrderHeaders;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
	
}
