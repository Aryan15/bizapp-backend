package com.coreerp.dto;

public class CurrencyDTO {
    private Long id;

    private String currencyName;

    private String deleted = "N";

    private String shortName;
    private String currencySymbol;
    private String currencyFraction;
    private String currencyDecimal;


    public String getCurrencyFraction() {
        return currencyFraction;
    }

    public void setCurrencyFraction(String currencyFraction) {
        this.currencyFraction = currencyFraction;
    }

    public String getCurrencyDecimal() {
        return currencyDecimal;
    }

    public void setCurrencyDecimal(String currencyDecimal) {
        this.currencyDecimal = currencyDecimal;
    }




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }
}
