package com.coreerp.dto;

import java.util.List;

import com.coreerp.reports.model.InvoiceReport;

public class InvoiceReportDTO {

	private List<InvoiceReport> invoiceReports;
	
	private Long totalCount;

	public List<InvoiceReport> getInvoiceReports() {
		return invoiceReports;
	}

	public void setInvoiceReports(List<InvoiceReport> invoiceReports) {
		this.invoiceReports = invoiceReports;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
	
}
