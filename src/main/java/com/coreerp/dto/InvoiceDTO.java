package com.coreerp.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class InvoiceDTO {

	
	private String id;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date invoiceDate;
	private String gstNumber;
    private String invoiceNumber ;
    private Long invId;
    private Date purchaseOrderDate ;
    private String purchaseOrderNumber ;
    private Date referenceDate;
    private String address ;
    private String internalReferenceNumber;
    private Date deliveryChallanDate ;
    private String deliveryChallanNumber ;
    private Long stateId ;
    private String stateName;
    private Date paymentDueDate ;
    private String eWayBillNumber ; 
    private Double totalTaxableAmount ;    
    private Double grandTotal;
    private String billToAddress ;
    private Long labourChargesOnly  ;
    private Double subTotalAmount;   
    private String paymentTerms;
    private String deliveryTerms;
    private String termsAndConditions;
    private String modeOfDispatch ;
    private String vehicleNumber ;
    private String numOfPackages;
    private String documentThrough ;
    private String shipToAddress ;
    private Double totalDiscount; 
    private Double netAmount ;
    private Double transportationCharges;
    private Double advanceAmount;
    private Boolean isRoudedOff;
    private Double roundOffAmount ;
    private String proformaInvoiceNumber;
    private Date proformaInvoiceDate;
    private String grnNumber;
    private Date grnDate;
    private String sourceInvoiceNumber;
    private Date sourceInvoiceDate;
    private Double cessAmount;
    private Double cessPercent;
    private Double sgstTaxRate;
    private Double sgstTaxAmount;
    private Double cgstTaxRate;
    private Double cgstTaxAmount;
    private Double igstTaxRate;
    private Double igstTaxAmount;
    
    private Long companyId;
    private Double discountAmount;
    private Double discountPercent;
    private Double dueAmount;
    private Long financialYearId;
    private Date internalReferenceDate;
    //private String invoiceStatus;
    private Long invoiceTypeId;
    private Long isReverseCharge;
    private Long partyId;
    private String partyName;
    private String remarks;
//    private String partyAddress;
//    private String partyGstNumber;
//    private String partyStateId;
//    private String partyStateName;
   
    

	private List<InvoiceItemDTO> invoiceItems;
    private Long taxId;
    private Double taxAmount;
    private String timeOfInvoice;
    private String vendorCode;
    private Long statusId;
    private String statusName;
    private Integer inclusiveTax;
    private String deliveryChallanDateString;
    
    private String companyName; 
    private Long companygGstRegistrationTypeId;
    private String companyPinCode; 
    private Long companyStateId;
    private String companyStateName;
    private Long companyCountryId;
    private String companyPrimaryMobile;
    private String companySecondaryMobile;
    private String companyContactPersonNumber;
    private String companyContactPersonName;
    private String companyPrimaryTelephone;
    private String companySecondaryTelephone;
    private String companyWebsite;
    private String companyEmail;
    private String companyFaxNumber;
    private String companyAddress;
    private String companyTagLine;
    private String companyGstNumber;
    private String companyPanNumber;
    private String companyPanDate;
    private String companyCeritificateImagePath;
    private String companyLogoPath;
    
    private String partyContactPersonNumber;
    private String partyPinCode;
    private Long partyAreaId;
    private Long partyCityId;
    private Long partyStateId;
    private Long partyCountryId;
    private String partyPrimaryTelephone;
    private String partySecondaryTelephone;
    private String partyPrimaryMobile;
    private String partySecondaryMobile;
    private String partyEmail;
    private String partyWebsite;
    private String partyContactPersonName;
    private String partyBillToAddress;
    private String partyShipAddress;
    private String partyDueDaysLimit;
    private String partyGstRegistrationTypeId;
    private String partyGstNumber;
    private String partyPanNumber;
    private String isIgst;
    private Integer isReused;
    private String reusedInvoiceId;
    private String partyCode;
    private String transactionStatus;
    private Double preTdsPer;
	private Double preTdsAmount;
	private Double tcsPercentage;
	private Double tcsAmount;

	private String createdBy;
	private String updateBy;
	private Date createdDate;
	private Date updatedDate;
	private Long materialNoteType;
	private Long currencyId;
	private Double currencyRate;
	private Double totalAmountCurrency;

	private String finalDestination;
	private String vesselNumber;
	private String shipper;
	private String cityOfLoading;
	private String cityOfDischarge;
	private String countryOfOriginOfGoods;
	private String countryOfDestination;
	private String currencyName;



	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getGstNumber() {
		return gstNumber;
	}
	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	
	
	public Long getInvId() {
		return invId;
	}
	public void setInvId(Long invId) {
		this.invId = invId;
	}
	public Date getPurchaseOrderDate() {
		return purchaseOrderDate;
	}
	public void setPurchaseOrderDate(Date purchaseOrderDate) {
		this.purchaseOrderDate = purchaseOrderDate;
	}

	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}
	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}
	public Date getReferenceDate() {
		return referenceDate;
	}
	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getInternalReferenceNumber() {
		return internalReferenceNumber;
	}
	public void setInternalReferenceNumber(String internalReferenceNumber) {
		this.internalReferenceNumber = internalReferenceNumber;
	}
	public Date getDeliveryChallanDate() {
		return deliveryChallanDate;
	}
	public void setDeliveryChallanDate(Date deliveryChallanDate) {
		this.deliveryChallanDate = deliveryChallanDate;
	}
	public String getDeliveryChallanNumber() {
		return deliveryChallanNumber;
	}
	public void setDeliveryChallanNumber(String deliveryChallanNumber) {
		this.deliveryChallanNumber = deliveryChallanNumber;
	}
	
	

	public Long getStateId() {
		return stateId;
	}
	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public Date getPaymentDueDate() {
		return paymentDueDate;
	}
	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	public String geteWayBillNumber() {
		return eWayBillNumber;
	}
	public void seteWayBillNumber(String eWayBillNumber) {
		this.eWayBillNumber = eWayBillNumber;
	}
	public Double getTotalTaxableAmount() {
		return totalTaxableAmount;
	}
	public void setTotalTaxableAmount(Double totalTaxableAmount) {
		this.totalTaxableAmount = totalTaxableAmount;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}
	public String getBillToAddress() {
		return billToAddress;
	}
	public void setBillToAddress(String billToAddress) {
		this.billToAddress = billToAddress;
	}
	public Long getLabourChargesOnly() {
		return labourChargesOnly;
	}
	public void setLabourChargesOnly(Long labourChargesOnly) {
		this.labourChargesOnly = labourChargesOnly;
	}

	public Double getSubTotalAmount() {
		return subTotalAmount;
	}
	public void setSubTotalAmount(Double subTotalAmount) {
		this.subTotalAmount = subTotalAmount;
	}
	public String getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public String getDeliveryTerms() {
		return deliveryTerms;
	}
	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}
	public String getTermsAndConditions() {
		return termsAndConditions;
	}
	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}
	public String getModeOfDispatch() {
		return modeOfDispatch;
	}
	public void setModeOfDispatch(String modeOfDispatch) {
		this.modeOfDispatch = modeOfDispatch;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getNumOfPackages() {
		return numOfPackages;
	}
	public void setNumOfPackages(String numOfPackages) {
		this.numOfPackages = numOfPackages;
	}
	public String getDocumentThrough() {
		return documentThrough;
	}
	public void setDocumentThrough(String documentThrough) {
		this.documentThrough = documentThrough;
	}
	public String getShipToAddress() {
		return shipToAddress;
	}
	public void setShipToAddress(String shipToAddress) {
		this.shipToAddress = shipToAddress;
	}
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public Double getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(Double netAmount) {
		this.netAmount = netAmount;
	}
	public Double getTransportationCharges() {
		return transportationCharges;
	}
	public void setTransportationCharges(Double transportationCharges) {
		this.transportationCharges = transportationCharges;
	}
	public Double getAdvanceAmount() {
		return advanceAmount;
	}
	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}
	public Boolean getIsRoudedOff() {
		return isRoudedOff;
	}
	public void setIsRoudedOff(Boolean isRoudedOff) {
		this.isRoudedOff = isRoudedOff;
	}
	public Double getRoundOffAmount() {
		return roundOffAmount;
	}
	public void setRoundOffAmount(Double roundOffAmount) {
		this.roundOffAmount = roundOffAmount;
	}
	
	
	public Double getCessAmount() {
		return cessAmount;
	}
	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}
	public Double getCessPercent() {
		return cessPercent;
	}
	public void setCessPercent(Double cessPercent) {
		this.cessPercent = cessPercent;
	}
	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}
	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}
	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}
	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}
	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}
	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}
	public Double getIgstTaxRate() {
		return igstTaxRate;
	}
	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}
	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}
	
	
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public List<InvoiceItemDTO> getInvoiceItems() {
		return invoiceItems;
	}
	public void setInvoiceItems(List<InvoiceItemDTO> invoiceItems) {
		this.invoiceItems = invoiceItems;
	}
	
	

	

	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Double getDiscountPercent() {
		return discountPercent;
	}
	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}

	public Double getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}
	
	
	
	
	public Date getInternalReferenceDate() {
		return internalReferenceDate;
	}
	public void setInternalReferenceDate(Date internalReferenceDate) {
		this.internalReferenceDate = internalReferenceDate;
	}
	public Long getFinancialYearId() {
		return financialYearId;
	}
	public void setFinancialYearId(Long financialYearId) {
		this.financialYearId = financialYearId;
	}
	
	
//	public String getInvoiceStatus() {
//		return invoiceStatus;
//	}
//	public void setInvoiceStatus(String invoiceStatus) {
//		this.invoiceStatus = invoiceStatus;
//	}
//	
	

	
	
	public Long getInvoiceTypeId() {
		return invoiceTypeId;
	}
	public void setInvoiceTypeId(Long invoiceTypeId) {
		this.invoiceTypeId = invoiceTypeId;
	}
	
	
	public Long getPartyId() {
		return partyId;
	}
	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}
	
	
	public Long getTaxId() {
		return taxId;
	}
	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}
	
	public Double getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	
	
	public String getTimeOfInvoice() {
		return timeOfInvoice;
	}
	public void setTimeOfInvoice(String timeOfInvoice) {
		this.timeOfInvoice = timeOfInvoice;
	}
	
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
		
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	
	
	public String getProformaInvoiceNumber() {
		return proformaInvoiceNumber;
	}
	public void setProformaInvoiceNumber(String proformaInvoiceNumber) {
		this.proformaInvoiceNumber = proformaInvoiceNumber;
	}
	public Date getProformaInvoiceDate() {
		return proformaInvoiceDate;
	}
	public void setProformaInvoiceDate(Date proformaInvoiceDate) {
		this.proformaInvoiceDate = proformaInvoiceDate;
	}
	
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	
	public String getGrnNumber() {
		return grnNumber;
	}
	public void setGrnNumber(String grnNumber) {
		this.grnNumber = grnNumber;
	}
	public Date getGrnDate() {
		return grnDate;
	}
	public void setGrnDate(Date grnDate) {
		this.grnDate = grnDate;
	}
	public String getSourceInvoiceNumber() {
		return sourceInvoiceNumber;
	}
	public void setSourceInvoiceNumber(String sourceInvoiceNumber) {
		this.sourceInvoiceNumber = sourceInvoiceNumber;
	}
	public Date getSourceInvoiceDate() {
		return sourceInvoiceDate;
	}
	public void setSourceInvoiceDate(Date sourceInvoiceDate) {
		this.sourceInvoiceDate = sourceInvoiceDate;
	}
	
	
	
	public Long getIsReverseCharge() {
		return isReverseCharge;
	}
	public void setIsReverseCharge(Long isReverseCharge) {
		this.isReverseCharge = isReverseCharge;
	}

	public Integer getInclusiveTax() {
		return inclusiveTax;
	}
	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}
	public String getDeliveryChallanDateString() {
		return deliveryChallanDateString;
	}
	public void setDeliveryChallanDateString(String deliveryChallanDateString) {
		this.deliveryChallanDateString = deliveryChallanDateString;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getCompanygGstRegistrationTypeId() {
		return companygGstRegistrationTypeId;
	}
	public void setCompanygGstRegistrationTypeId(Long companygGstRegistrationTypeId) {
		this.companygGstRegistrationTypeId = companygGstRegistrationTypeId;
	}
	public String getCompanyPinCode() {
		return companyPinCode;
	}
	public void setCompanyPinCode(String companyPinCode) {
		this.companyPinCode = companyPinCode;
	}
	public Long getCompanyStateId() {
		return companyStateId;
	}
	public void setCompanyStateId(Long companyStateId) {
		this.companyStateId = companyStateId;
	}
	
	
	public String getCompanyStateName() {
		return companyStateName;
	}
	public void setCompanyStateName(String companyStateName) {
		this.companyStateName = companyStateName;
	}
	public Long getCompanyCountryId() {
		return companyCountryId;
	}
	public void setCompanyCountryId(Long companyCountryId) {
		this.companyCountryId = companyCountryId;
	}
	public String getCompanyPrimaryMobile() {
		return companyPrimaryMobile;
	}
	public void setCompanyPrimaryMobile(String companyPrimaryMobile) {
		this.companyPrimaryMobile = companyPrimaryMobile;
	}
	public String getCompanySecondaryMobile() {
		return companySecondaryMobile;
	}
	public void setCompanySecondaryMobile(String companySecondaryMobile) {
		this.companySecondaryMobile = companySecondaryMobile;
	}
	public String getCompanyContactPersonNumber() {
		return companyContactPersonNumber;
	}
	public void setCompanyContactPersonNumber(String companyContactPersonNumber) {
		this.companyContactPersonNumber = companyContactPersonNumber;
	}
	public String getCompanyContactPersonName() {
		return companyContactPersonName;
	}
	public void setCompanyContactPersonName(String companyContactPersonName) {
		this.companyContactPersonName = companyContactPersonName;
	}
	public String getCompanyPrimaryTelephone() {
		return companyPrimaryTelephone;
	}
	public void setCompanyPrimaryTelephone(String companyPrimaryTelephone) {
		this.companyPrimaryTelephone = companyPrimaryTelephone;
	}
	public String getCompanySecondaryTelephone() {
		return companySecondaryTelephone;
	}
	public void setCompanySecondaryTelephone(String companySecondaryTelephone) {
		this.companySecondaryTelephone = companySecondaryTelephone;
	}
	public String getCompanyWebsite() {
		return companyWebsite;
	}
	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public String getCompanyFaxNumber() {
		return companyFaxNumber;
	}
	public void setCompanyFaxNumber(String companyFaxNumber) {
		this.companyFaxNumber = companyFaxNumber;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public String getCompanyTagLine() {
		return companyTagLine;
	}

	public void setCompanyTagLine(String companyTagLine) {
		this.companyTagLine = companyTagLine;
	}

	public String getCompanyGstNumber() {
		return companyGstNumber;
	}
	public void setCompanyGstNumber(String companyGstNumber) {
		this.companyGstNumber = companyGstNumber;
	}

	public String getCompanyPanNumber() {
		return companyPanNumber;
	}

	public void setCompanyPanNumber(String companyPanNumber) {
		this.companyPanNumber = companyPanNumber;
	}

	public String getCompanyPanDate() {
		return companyPanDate;
	}
	public void setCompanyPanDate(String companyPanDate) {
		this.companyPanDate = companyPanDate;
	}
	public String getCompanyCeritificateImagePath() {
		return companyCeritificateImagePath;
	}
	public void setCompanyCeritificateImagePath(String companyCeritificateImagePath) {
		this.companyCeritificateImagePath = companyCeritificateImagePath;
	}
	public String getCompanyLogoPath() {
		return companyLogoPath;
	}
	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}
	public String getPartyContactPersonNumber() {
		return partyContactPersonNumber;
	}
	public void setPartyContactPersonNumber(String partyContactPersonNumber) {
		this.partyContactPersonNumber = partyContactPersonNumber;
	}
	public String getPartyPinCode() {
		return partyPinCode;
	}
	public void setPartyPinCode(String partyPinCode) {
		this.partyPinCode = partyPinCode;
	}
	public Long getPartyAreaId() {
		return partyAreaId;
	}
	public void setPartyAreaId(Long partyAreaId) {
		this.partyAreaId = partyAreaId;
	}
	public Long getPartyCityId() {
		return partyCityId;
	}
	public void setPartyCityId(Long partyCityId) {
		this.partyCityId = partyCityId;
	}
	public Long getPartyStateId() {
		return partyStateId;
	}
	public void setPartyStateId(Long partyStateId) {
		this.partyStateId = partyStateId;
	}
	public Long getPartyCountryId() {
		return partyCountryId;
	}
	public void setPartyCountryId(Long partyCountryId) {
		this.partyCountryId = partyCountryId;
	}
	public String getPartyPrimaryTelephone() {
		return partyPrimaryTelephone;
	}
	public void setPartyPrimaryTelephone(String partyPrimaryTelephone) {
		this.partyPrimaryTelephone = partyPrimaryTelephone;
	}
	public String getPartySecondaryTelephone() {
		return partySecondaryTelephone;
	}
	public void setPartySecondaryTelephone(String partySecondaryTelephone) {
		this.partySecondaryTelephone = partySecondaryTelephone;
	}
	public String getPartyPrimaryMobile() {
		return partyPrimaryMobile;
	}
	public void setPartyPrimaryMobile(String partyPrimaryMobile) {
		this.partyPrimaryMobile = partyPrimaryMobile;
	}
	public String getPartySecondaryMobile() {
		return partySecondaryMobile;
	}
	public void setPartySecondaryMobile(String partySecondaryMobile) {
		this.partySecondaryMobile = partySecondaryMobile;
	}
	public String getPartyEmail() {
		return partyEmail;
	}
	public void setPartyEmail(String partyEmail) {
		this.partyEmail = partyEmail;
	}
	public String getPartyWebsite() {
		return partyWebsite;
	}
	public void setPartyWebsite(String partyWebsite) {
		this.partyWebsite = partyWebsite;
	}
	public String getPartyContactPersonName() {
		return partyContactPersonName;
	}
	public void setPartyContactPersonName(String partyContactPersonName) {
		this.partyContactPersonName = partyContactPersonName;
	}
	public String getPartyBillToAddress() {
		return partyBillToAddress;
	}
	public void setPartyBillToAddress(String partyBillToAddress) {
		this.partyBillToAddress = partyBillToAddress;
	}
	public String getPartyShipAddress() {
		return partyShipAddress;
	}
	public void setPartyShipAddress(String partyShipAddress) {
		this.partyShipAddress = partyShipAddress;
	}
	public String getPartyDueDaysLimit() {
		return partyDueDaysLimit;
	}
	public void setPartyDueDaysLimit(String partyDueDaysLimit) {
		this.partyDueDaysLimit = partyDueDaysLimit;
	}
	public String getPartyGstRegistrationTypeId() {
		return partyGstRegistrationTypeId;
	}
	public void setPartyGstRegistrationTypeId(String partyGstRegistrationTypeId) {
		this.partyGstRegistrationTypeId = partyGstRegistrationTypeId;
	}
	public String getPartyGstNumber() {
		return partyGstNumber;
	}
	public void setPartyGstNumber(String partyGstNumber) {
		this.partyGstNumber = partyGstNumber;
	}
	public String getPartyPanNumber() {
		return partyPanNumber;
	}
	public void setPartyPanNumber(String partyPanNumber) {
		this.partyPanNumber = partyPanNumber;
	}
	public String getIsIgst() {
		return isIgst;
	}
	public void setIsIgst(String isIgst) {
		this.isIgst = isIgst;
	}


	public Boolean getRoudedOff() {
		return isRoudedOff;
	}

	public void setRoudedOff(Boolean roudedOff) {
		isRoudedOff = roudedOff;
	}

	public Integer getIsReused() {
		return isReused;
	}

	public void setIsReused(Integer isReused) {
		this.isReused = isReused;
	}

	public String getReusedInvoiceId() {
		return reusedInvoiceId;
	}

	public void setReusedInvoiceId(String reusedInvoiceId) {
		this.reusedInvoiceId = reusedInvoiceId;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public Double getPreTdsPer() {
		return preTdsPer;
	}

	public void setPreTdsPer(Double preTdsPer) {
		this.preTdsPer = preTdsPer;
	}

	public Double getPreTdsAmount() {
		return preTdsAmount;
	}

	public void setPreTdsAmount(Double preTdsAmount) {
		this.preTdsAmount = preTdsAmount;
	}

	public Double getTcsPercentage() {
		return tcsPercentage;
	}

	public void setTcsPercentage(Double tcsPercentage) {
		this.tcsPercentage = tcsPercentage;
	}

	public Double getTcsAmount() {
		return tcsAmount;
	}

	public void setTcsAmount(Double tcsAmount) {
		this.tcsAmount = tcsAmount;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getMaterialNoteType() {
		return materialNoteType;
	}

	public void setMaterialNoteType(Long materialNoteType) {
		this.materialNoteType = materialNoteType;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Double getCurrencyRate() {
		return currencyRate;
	}

	public void setCurrencyRate(Double currencyRate) {
		this.currencyRate = currencyRate;
	}

	public Double getTotalAmountCurrency() {
		return totalAmountCurrency;
	}

	public void setTotalAmountCurrency(Double totalAmountCurrency) {
		this.totalAmountCurrency = totalAmountCurrency;
	}

	public String getFinalDestination() {
		return finalDestination;
	}

	public void setFinalDestination(String finalDestination) {
		this.finalDestination = finalDestination;
	}

	public String getVesselNumber() {
		return vesselNumber;
	}

	public void setVesselNumber(String vesselNumber) {
		this.vesselNumber = vesselNumber;
	}

	public String getShipper() {
		return shipper;
	}

	public void setShipper(String shipper) {
		this.shipper = shipper;
	}

	public String getCityOfLoading() {
		return cityOfLoading;
	}

	public void setCityOfLoading(String cityOfLoading) {
		this.cityOfLoading = cityOfLoading;
	}

	public String getCityOfDischarge() {
		return cityOfDischarge;
	}

	public void setCityOfDischarge(String cityOfDischarge) {
		this.cityOfDischarge = cityOfDischarge;
	}

	public String getCountryOfOriginOfGoods() {
		return countryOfOriginOfGoods;
	}

	public void setCountryOfOriginOfGoods(String countryOfOriginOfGoods) {
		this.countryOfOriginOfGoods = countryOfOriginOfGoods;
	}

	public String getCountryOfDestination() {
		return countryOfDestination;
	}

	public void setCountryOfDestination(String countryOfDestination) {
		this.countryOfDestination = countryOfDestination;
	}



	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	@Override
	public String toString() {
		return "InvoiceDTO{" +
				"id='" + id + '\'' +
				", invoiceDate=" + invoiceDate +
				", gstNumber='" + gstNumber + '\'' +
				", invoiceNumber='" + invoiceNumber + '\'' +
				", invId=" + invId +
				", purchaseOrderDate=" + purchaseOrderDate +
				", purchaseOrderNumber='" + purchaseOrderNumber + '\'' +
				", referenceDate=" + referenceDate +
				", address='" + address + '\'' +
				", internalReferenceNumber='" + internalReferenceNumber + '\'' +
				", deliveryChallanDate=" + deliveryChallanDate +
				", deliveryChallanNumber='" + deliveryChallanNumber + '\'' +
				", stateId=" + stateId +
				", stateName='" + stateName + '\'' +
				", paymentDueDate=" + paymentDueDate +
				", eWayBillNumber='" + eWayBillNumber + '\'' +
				", totalTaxableAmount=" + totalTaxableAmount +
				", grandTotal=" + grandTotal +
				", billToAddress='" + billToAddress + '\'' +
				", labourChargesOnly=" + labourChargesOnly +
				", subTotalAmount=" + subTotalAmount +
				", paymentTerms='" + paymentTerms + '\'' +
				", deliveryTerms='" + deliveryTerms + '\'' +
				", termsAndConditions='" + termsAndConditions + '\'' +
				", modeOfDispatch='" + modeOfDispatch + '\'' +
				", vehicleNumber='" + vehicleNumber + '\'' +
				", numOfPackages='" + numOfPackages + '\'' +
				", documentThrough='" + documentThrough + '\'' +
				", shipToAddress='" + shipToAddress + '\'' +
				", totalDiscount=" + totalDiscount +
				", netAmount=" + netAmount +
				", transportationCharges=" + transportationCharges +
				", advanceAmount=" + advanceAmount +
				", isRoudedOff=" + isRoudedOff +
				", roundOffAmount=" + roundOffAmount +
				", proformaInvoiceNumber='" + proformaInvoiceNumber + '\'' +
				", proformaInvoiceDate=" + proformaInvoiceDate +
				", grnNumber='" + grnNumber + '\'' +
				", grnDate=" + grnDate +
				", sourceInvoiceNumber='" + sourceInvoiceNumber + '\'' +
				", sourceInvoiceDate=" + sourceInvoiceDate +
				", cessAmount=" + cessAmount +
				", cessPercent=" + cessPercent +
				", sgstTaxRate=" + sgstTaxRate +
				", sgstTaxAmount=" + sgstTaxAmount +
				", cgstTaxRate=" + cgstTaxRate +
				", cgstTaxAmount=" + cgstTaxAmount +
				", igstTaxRate=" + igstTaxRate +
				", igstTaxAmount=" + igstTaxAmount +
				", companyId=" + companyId +
				", discountAmount=" + discountAmount +
				", discountPercent=" + discountPercent +
				", dueAmount=" + dueAmount +
				", financialYearId=" + financialYearId +
				", internalReferenceDate=" + internalReferenceDate +
				", invoiceTypeId=" + invoiceTypeId +
				", isReverseCharge=" + isReverseCharge +
				", partyId=" + partyId +
				", partyName='" + partyName + '\'' +
				", remarks='" + remarks + '\'' +
				", invoiceItems=" + invoiceItems +
				", taxId=" + taxId +
				", taxAmount=" + taxAmount +
				", timeOfInvoice='" + timeOfInvoice + '\'' +
				", vendorCode='" + vendorCode + '\'' +
				", statusId=" + statusId +
				", statusName='" + statusName + '\'' +
				", inclusiveTax=" + inclusiveTax +
				", deliveryChallanDateString='" + deliveryChallanDateString + '\'' +
				", companyName='" + companyName + '\'' +
				", companygGstRegistrationTypeId=" + companygGstRegistrationTypeId +
				", companyPinCode='" + companyPinCode + '\'' +
				", companyStateId=" + companyStateId +
				", companyStateName='" + companyStateName + '\'' +
				", companyCountryId=" + companyCountryId +
				", companyPrimaryMobile='" + companyPrimaryMobile + '\'' +
				", companySecondaryMobile='" + companySecondaryMobile + '\'' +
				", companyContactPersonNumber='" + companyContactPersonNumber + '\'' +
				", companyContactPersonName='" + companyContactPersonName + '\'' +
				", companyPrimaryTelephone='" + companyPrimaryTelephone + '\'' +
				", companySecondaryTelephone='" + companySecondaryTelephone + '\'' +
				", companyWebsite='" + companyWebsite + '\'' +
				", companyEmail='" + companyEmail + '\'' +
				", companyFaxNumber='" + companyFaxNumber + '\'' +
				", companyAddress='" + companyAddress + '\'' +
				", companyTagLine='" + companyTagLine + '\'' +
				", companyGstNumber='" + companyGstNumber + '\'' +
				", companyPanNumber='" + companyPanNumber + '\'' +
				", companyPanDate='" + companyPanDate + '\'' +
				", companyCeritificateImagePath='" + companyCeritificateImagePath + '\'' +
				", companyLogoPath='" + companyLogoPath + '\'' +
				", partyContactPersonNumber='" + partyContactPersonNumber + '\'' +
				", partyPinCode='" + partyPinCode + '\'' +
				", partyAreaId=" + partyAreaId +
				", partyCityId=" + partyCityId +
				", partyStateId=" + partyStateId +
				", partyCountryId=" + partyCountryId +
				", partyPrimaryTelephone='" + partyPrimaryTelephone + '\'' +
				", partySecondaryTelephone='" + partySecondaryTelephone + '\'' +
				", partyPrimaryMobile='" + partyPrimaryMobile + '\'' +
				", partySecondaryMobile='" + partySecondaryMobile + '\'' +
				", partyEmail='" + partyEmail + '\'' +
				", partyWebsite='" + partyWebsite + '\'' +
				", partyContactPersonName='" + partyContactPersonName + '\'' +
				", partyBillToAddress='" + partyBillToAddress + '\'' +
				", partyShipAddress='" + partyShipAddress + '\'' +
				", partyDueDaysLimit='" + partyDueDaysLimit + '\'' +
				", partyGstRegistrationTypeId='" + partyGstRegistrationTypeId + '\'' +
				", partyGstNumber='" + partyGstNumber + '\'' +
				", partyPanNumber='" + partyPanNumber + '\'' +
				", isIgst='" + isIgst + '\'' +
				", isReused=" + isReused +
				", reusedInvoiceId='" + reusedInvoiceId + '\'' +
				", partyCode='" + partyCode + '\'' +
				", transactionStatus='" + transactionStatus + '\'' +
				", preTdsPer=" + preTdsPer +
				", preTdsAmount=" + preTdsAmount +
				", tcsPercentage=" + tcsPercentage +
				", tcsAmount=" + tcsAmount +
				", createdBy='" + createdBy + '\'' +
				", updateBy='" + updateBy + '\'' +
				", createdDate=" + createdDate +
				", updatedDate=" + updatedDate +
				", materialNoteType=" + materialNoteType +
				", currencyId=" + currencyId +
				", currencyRate=" + currencyRate +
				", totalAmountCurrency=" + totalAmountCurrency +
				", finalDestination='" + finalDestination + '\'' +
				", vesselNumber='" + vesselNumber + '\'' +
				", shipper='" + shipper + '\'' +
				", cityOfLoading='" + cityOfLoading + '\'' +
				", cityOfDischarge='" + cityOfDischarge + '\'' +
				", countryOfOriginOfGoods='" + countryOfOriginOfGoods + '\'' +
				", countryOfDestination='" + countryOfDestination + '\'' +
				", currencyName='" + currencyName + '\'' +
				'}';
	}
}
