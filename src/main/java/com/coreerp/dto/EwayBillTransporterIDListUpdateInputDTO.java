package com.coreerp.dto;

public class EwayBillTransporterIDListUpdateInputDTO {

    private Long ewbNo;

    private String transporterId;

    public Long getEwbNo() {
        return ewbNo;
    }

    public void setEwbNo(Long ewbNo) {
        this.ewbNo = ewbNo;
    }

    public String getTransporterId() {
        return transporterId;
    }

    public void setTransporterId(String transporterId) {
        this.transporterId = transporterId;
    }

    @Override
    public String toString() {
        return "EwayBillTransporterIDListUpdateInputDTO{" +
                "ewbNo=" + ewbNo +
                ", transporterId='" + transporterId + '\'' +
                '}';
    }
}
