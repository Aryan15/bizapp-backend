package com.coreerp.dto;

import java.util.List;

public class PettyCashWrapperDTO {

    private List<String> itemToBeRemove;
    private PettyCashHeaderDTO pettyCashHeader;
    public List<String> getItemToBeRemove() {
        return itemToBeRemove;
    }
    public void setItemToBeRemove(List<String> itemToBeRemove) {
        this.itemToBeRemove = itemToBeRemove;
    }

    public PettyCashHeaderDTO getPettyCashHeader() {
        return pettyCashHeader;
    }

    public void setPettyCashHeader(PettyCashHeaderDTO pettyCashHeader) {
        this.pettyCashHeader = pettyCashHeader;
    }
}
