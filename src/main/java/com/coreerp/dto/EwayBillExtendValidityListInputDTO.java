package com.coreerp.dto;

public class EwayBillExtendValidityListInputDTO {

    private Long ewbNo;
    private String vehicleNo;
    private String fromPlace;
    private String fromState;
    private Integer remainingDistance;
    private String transDocNo;
    private String transDocDate;
    private String transMode;
    private Integer extnRsnCode;
    private String extnRemarks;
    private Integer fromPincode;
    private String consignmentStatus;
    private String transitType;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;

    public Long getEwbNo() {
        return ewbNo;
    }

    public void setEwbNo(Long ewbNo) {
        this.ewbNo = ewbNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getFromPlace() {
        return fromPlace;
    }

    public void setFromPlace(String fromPlace) {
        this.fromPlace = fromPlace;
    }

    public String getFromState() {
        return fromState;
    }

    public void setFromState(String fromState) {
        this.fromState = fromState;
    }

    public Integer getRemainingDistance() {
        return remainingDistance;
    }

    public void setRemainingDistance(Integer remainingDistance) {
        this.remainingDistance = remainingDistance;
    }

    public String getTransDocNo() {
        return transDocNo;
    }

    public void setTransDocNo(String transDocNo) {
        this.transDocNo = transDocNo;
    }

    public String getTransDocDate() {
        return transDocDate;
    }

    public void setTransDocDate(String transDocDate) {
        this.transDocDate = transDocDate;
    }

    public String getTransMode() {
        return transMode;
    }

    public void setTransMode(String transMode) {
        this.transMode = transMode;
    }

    public Integer getExtnRsnCode() {
        return extnRsnCode;
    }

    public void setExtnRsnCode(Integer extnRsnCode) {
        this.extnRsnCode = extnRsnCode;
    }

    public String getExtnRemarks() {
        return extnRemarks;
    }

    public void setExtnRemarks(String extnRemarks) {
        this.extnRemarks = extnRemarks;
    }

    public Integer getFromPincode() {
        return fromPincode;
    }

    public void setFromPincode(Integer fromPincode) {
        this.fromPincode = fromPincode;
    }

    public String getConsignmentStatus() {
        return consignmentStatus;
    }

    public void setConsignmentStatus(String consignmentStatus) {
        this.consignmentStatus = consignmentStatus;
    }

    public String getTransitType() {
        return transitType;
    }

    public void setTransitType(String transitType) {
        this.transitType = transitType;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    @Override
    public String toString() {
        return "EwayBillExtendValidityListInputDTO{" +
                "ewbNo=" + ewbNo +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", fromPlace='" + fromPlace + '\'' +
                ", fromState='" + fromState + '\'' +
                ", remainingDistance=" + remainingDistance +
                ", transDocNo='" + transDocNo + '\'' +
                ", transDocDate='" + transDocDate + '\'' +
                ", transMode='" + transMode + '\'' +
                ", extnRsnCode=" + extnRsnCode +
                ", extnRemarks='" + extnRemarks + '\'' +
                ", fromPincode=" + fromPincode +
                ", consignmentStatus='" + consignmentStatus + '\'' +
                ", transitType='" + transitType + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", addressLine3='" + addressLine3 + '\'' +
                '}';
    }
}
