package com.coreerp;


import java.util.Date;

public class ApplicationConstants {
	
	// PARTY_TYPE
	public static final String PARTY_TYPE_CUSTOMER="Customer";
	public static final String PARTY_TYPE_SUPPLIER="Supplier";
	public static final String PARTY_TYPE_BOTH="Both";
	public static final String PARTY_TYPE_ALL="ALL";
	
	//TRANSACTION LIST
	public static final String TRANSACTION_CUSTOMER_PURCHASE_ORDER = "Customer PO";
	public static final String TRANSACTION_SUPPLIER_PURCHASE_ORDER = "Supplier PO";
	public static final String TRANSACTION_CUSTOMER_INVOICE = "Customer Invoice";
	

	//GENERIC TRANSACTION STATUS
	public static final String TRANSACTION_NEW="New";
	public static final String TRANSACTION_OPEN="Open";
	public static final String TRANSACTION_CANCELLED="Cancelled";
	public static final String TRANSACTION_PARTIAL="Partial";
	public static final String TRANSACTION_COMPLETED="Completed";
	public static final String TRANSACTION_DELETED="Deleted";	
	
	//QUOTATION
	public static final String QUOTATION_STATUS_NEW=TRANSACTION_NEW;
	public static final String QUOTATION_STATUS_OPEN=TRANSACTION_OPEN;
	public static final String QUOTATION_STATUS_PARTIAL=TRANSACTION_PARTIAL;
	public static final String QUOTATION_STATUS_CANCELLED=TRANSACTION_CANCELLED;
	public static final String QUOTATION_STATUS_DELETED=TRANSACTION_DELETED;
	public static final String QUOTATION_STATUS_COMPLETED=TRANSACTION_COMPLETED;
	public static final String QUOTATION_TYPE_CUSTOMER="Customer Quotation";
	public static final String QUOTATION_TYPE_SUPPLIER="Supplier Quotation";
	public static final String QUOTATION_TYPE_JOBWORK="Jobwork Quotation";
	
	//PURCHASE ORDER
	public static final String PURCHASE_ORDER_STATUS_NEW=TRANSACTION_NEW;
	public static final String PURCHASE_ORDER_STATUS_OPEN=TRANSACTION_OPEN;
	//public static final String PURCHASE_ORDER_STATUS_CANCELLED=TRANSACTION_CANCELLED;
	//public static final String PURCHASE_ORDER_STATUS_PARTIAL=TRANSACTION_PARTIAL;
	public static final String PURCHASE_ORDER_STATUS_DELETED=TRANSACTION_DELETED;
	//public static final String PURCHASE_ORDER_STATUS_COMPLETED=TRANSACTION_COMPLETED;	
	public static final String PURCHASE_ORDER_STATUS_DC_GENERATED="DC Generated";
	public static final String PURCHASE_ORDER_STATUS_INCOMING_JW_DC_CREATED="I/C JW DC Created";
	public static final String PURCHASE_ORDER_STATUS_INCOMING_JW_DC_PARTIAL="I/C JW DC Partial";
	public static final String PURCHASE_ORDER_STATUS_OUTGOING_JW_DC_CREATED="O/G JW DC Created";
	public static final String PURCHASE_ORDER_STATUS_OUTGOING_JW_DC_PARTIAL="O/G JW DC Partial";
	public static final String PURCHASE_ORDER_STATUS_PARTIAL_DC="Partial DC";
	public static final String PURCHASE_ORDER_STATUS_PARTIAL_INVOICED="Partially Invoiced"; 
	public static final String PURCHASE_ORDER_STATUS_INVOICED="Invoiced";
	public static final String PURCHASE_ORDER_TYPE_CUSTOMER="Customer PO";
	public static final String PURCHASE_ORDER_TYPE_SUPPLIER="Supplier PO";
	
	public static final String PURCHASE_ORDER_INCOMING_JOBWORK="Jobwork PO";
	public static final String PURCHASE_ORDER_OUTGOING_JOBWORK="Subcontracting PO";
	
	//DELIVERY CHALLAN
	
	public static final String DC_STATUS_NEW=TRANSACTION_NEW;
	//public static final String DC_STATUS_OPEN=TRANSACTION_OPEN;
	public static final String DC_STATUS_CANCELLED=TRANSACTION_CANCELLED;
	public static final String DC_STATUS_PARTIALLY_INVOICED="Partially Invoiced";
	public static final String DC_STATUS_DELETED=TRANSACTION_DELETED;
	public static final String DC_STATUS_INVOICED="Invoiced";
	public static final String DC_STATUS_PARTIAL_GRN="Partial GRN";
	public static final String DC_STATUS_GRN_GENEREATED="GRN Generated";
	public static final String DC_STATUS_OUTDC_GENEREATED="OUT DC Generated";
	public static final String DC_STATUS_OUTDC_PARTIAL="OUT DC Partially Generated";
	public static final String DC_STATUS_INDC_GENEREATED="IN DC Generated";
	public static final String DC_STATUS_INDC_PARTIAL="IN DC Partially Generated";
	
	public static final String DC_TYPE_CUSTOMER="Customer DC";
	public static final String DC_TYPE_SUPPLIER="Incoming DC";
	
	//public static final String INCOMING_JOBWORK_IN_DC="Incoming DC";
	//public static final String DC_TYPE_SUPPLIER="Incoming DC";
	
	public static final String INCOMING_JOBWORK_IN_DC="Jobwork In DC";
	public static final String INCOMING_JOBWORK_OUT_DC= "Jobwork Out DC";
	public static final String OUTGOING_JOBWORK_OUT_DC= "Subcontracting Out DC";
	public static final String OUTGOING_JOBWORK_IN_DC= "Subcontracting In DC";
    
	//INVOICE
	public static final String INVOICE_STATUS_NEW=TRANSACTION_NEW;
	public static final String INVOICE_STATUS_OPEN=TRANSACTION_OPEN;
	public static final String INVOICE_STATUS_CANCELLED=TRANSACTION_CANCELLED;
	public static final String INVOICE_STATUS_PARTIAL="Partially Paid";
	public static final String INVOICE_STATUS_DELETED=TRANSACTION_DELETED;
	public static final String INVOICE_STATUS_COMPLETED=TRANSACTION_COMPLETED;
	public static final String INVOICE_STATUS_NOTE_PARTIAL="Note Partially Created";
	public static final String INVOICE_STATUS_NOTE_COMPLETED="Completed";
	public static final String INVOICE_STATUS_NOTE_CREATED="Note Created";
	public static final String PROFORMA_INVOICE_STATUS_NEW = TRANSACTION_NEW;
	public static final String PROFORMA_INVOICE_STATUS_INVOICED ="Invoiced";
	
	public static final String INVOICE_TYPE_CUSTOMER="Customer Invoice";
	public static final String INVOICE_TYPE_SUPPLIER="Purchase Invoice";
	public static final String INVOICE_TYPE_PROFORMA="Proforma Invoice";
	public static final String INVOICE_TYPE_INCOMING_JW_INVOICE="Jobwork Invoice";
	public static final String INVOICE_TYPE_OUTGOING_JW_INVOICE="Subcontracting Invoice";
	public static final String PETTY_CASH="Petty Cash";
	public static final String CUSTOMER="Customer";
	public static final String SUPPLIER="Supplier";
	public static final String CUSTOMER_CODE="Customer Code";
	public static final String SUPPLIER_CODE="Supplier Code";
	public static final String JOBWORK_PROFORMA_INVOICE="Jobwork Proforma Invoice";
	public static final String JOBWORK_CREDIT_NOTE="Jobwork Credit Note";
	public static final String SUBCONTRACT_DEBIT_NOTE="Subcontract Debit Note";
	public static final Long SUBCONTRACT_DEBIT_NOTE_ID=29L;

	//GRN
	
	public static final String GRN_STATUS_NEW=TRANSACTION_NEW;
	public static final String GRN_STATUS_OPEN=TRANSACTION_OPEN;
	public static final String GRN_STATUS_CANCELLED=TRANSACTION_CANCELLED;
	public static final String GRN_STATUS_PARTIALLY_INVOICED=TRANSACTION_PARTIAL;
	public static final String GRN_STATUS_DELETED=TRANSACTION_DELETED;
	public static final String GRN_STATUS_INVOICED=TRANSACTION_COMPLETED;
	
	public static final String GRN_TYPE_SUPPLIER="GRN";
	
	
	
	//CREDIT AND DEBIT NOTES
	
	public static final String NOTE_STATUS_NEW=TRANSACTION_NEW;
	public static final String NOTE_STATUS_OPEN=TRANSACTION_OPEN;
	public static final String NOTE_STATUS_CANCELLED=TRANSACTION_CANCELLED;
	public static final String NOTE_STATUS_PARTIAL=TRANSACTION_PARTIAL;
	public static final String NOTE_STATUS_DELETED=TRANSACTION_DELETED;
	public static final String NOTE_STATUS_COMPLETED=TRANSACTION_COMPLETED;
	public static final String NOTE_TYPE_DEBIT="Debit Note";
	public static final String NOTE_TYPE_CREDIT="Credit Note";
	
	
	
	//PAYABLE RECEIVABLE
	
	public static final String PR_SUPPLIER_PAYABLE="Supplier Payments";
	public static final String PR_CUSTOMER_RECEIVABLE="Customer Receipts";
	public static final String PR_STATUS_NEW=TRANSACTION_NEW;
	public static final String PR_STATUS_OPEN=TRANSACTION_OPEN;
	public static final String PR_STATUS_CANCELLED=TRANSACTION_CANCELLED;
	public static final String PR_STATUS_PARTIAL=TRANSACTION_PARTIAL;
	public static final String PR_STATUS_DELETED=TRANSACTION_DELETED;
	public static final String PR_STATUS_COMPLETED=TRANSACTION_COMPLETED;
	public static final String PR_STATUS_DRAFT="Draft";
	public static final String PR_SOURCE_SP="BUY-SELL";
	public static final String PR_SOURCE_JS="JW-SC";
	//DEFAULT_TENANT_ID
	public static final String DEFAULT_TENANT_ID = "mybos_coreerp";
	
	public static final Integer MATERIAL_PRICING_TYPE_BUYING_PRICE = 1;	
	public static final Integer MATERIAL_PRICING_TYPE_SELLING_PRICE = 2;
	public static final Integer MATERIAL_PRICING_TYPE_MRP = 3;
	
	//SUPPLY TYPE
	public static final String SUPPLY_TYPE_GOODS="Goods";
	public static final String SUPPLY_TYPE_SERVICE="Service";
	
    //TYPE OF TRANSACTION
	public static final String TXN_SELL="Sell";
	public static final String TXN_PURCHASE = "Purchase";
	
	//TYPE OF VOUCHER
	public static final String CASH_VOUCHER="Cash Voucher";
	public static final String CHEQUE_VOUCHER="Cheque Voucher";
	public static final String EXPENSE_VOUCHER="Expense Tracker";

	
	//PAYMENT METHOD
	public static final String PAYMENT_METHOD_CHECQUE_OR_DD="Cheque/DD";
	
	public static final Integer OTP_PER_DAY=3;
	
	public static final Integer OTP_EXPIRY_HOURS=24;
	
	public static final String ROLE_TYPE_SYSTEM="SYSTEM";
	
	// MATERIAL_TYPE
	public static final String MATERIAL_TYPE_RAW_MATERIAL="Raw Material";
	public static final String MATERIAL_TYPE_SEMI_FINISHED="Semi Finished";
	public static final String MATERIAL_TYPE_FINISHED="Finished";
	public static final String MATERIAL_TYPE_JOBWORK="Jobwork";

	//PRINT
	public static final String PRINT_ALL="ALL";


	//TALLY EXPORT CONSTANTS
	public static final String TALLLY_SUNDRY_DEBTORS = "Sundry Debtors";
	public static final String TALLLY_SUNDRY_CREDITORS = "Sundry Creditors";
	public static final String TALLY_BILLING_TYPE="New Ref";
	public static final String TALLY_NO_CONSTANT="No";
	public static final String TALLY_YES_CONSTANT="Yes";
//	public static final String  TALLY_INVOICE_TYPE="Customer Invoice";
	public static final String  TALLY_TAX_INVOICE="Tax Invoice";
	public static final String  TALLY_PURCHASE_INVOICE="PURCHASE Invoice";
	public static final String  TALLY_INVOICE_SUB_TYPE_CONSTANT="Goods";
	public static final String  TALLY_VEHICLE_TYPE="Regular";
	public static final String  TALLY_TAX_SGST="SGST";
	public static final String  TALLY_TAX_CGST="CGST";
	public static final String  TALLY_TAX_IGST="IGST";
	public static final String  TALLY_SALES_VOUCHER="Sales";
	public static final String  TALLY_PURCHASE_VOUCHER="Purchase";
	public static final String  TALLY_REQUEST="Import Data";
	public static final String  TALLY_REPORT_NAME="All Masters";

	public static final String  TALLY_PERSISTED_VIEW="Invoice Voucher View";
	public static final String  TALLY_GST_SUBTYPE="Supply";
	public static final String  TALLY_CURRENCY_TYPE="Rupees";
	public static final String  TALLY_SERVICE_CATEGORY="Not Applicable";
	public static final Integer  TALLY_LANGUAGE_ID=1033;
	public static final String  TALLY_EXCISE_APPLICABLE="Applicable";
	public static final String  TALLY_INBUILT_TALLY_NAME="SET-SETS";
	public static final String  TALLY_TAX_TYPE="GST";
	public static final String  TALLY_COSTING_METHOD="Avg. Cost";
	public static final String  TALLY_VALUATION_METHOD="Avg. Price";
	public static final  String  TALLY_FINANCIAL_START_DATE="20190401";
	public static final  String  TALLY_BANK_COUNTRY_NAME="INDIA";
	public static final  String  TALLY_BANK_PARENT="bank Accounts";
	public static final  String  TALLY_EI_DISCOUNT_RATE="EI Discount Rate";
	public static final  String  TALLY_EI_DISCOUNT_AMOUNT="EI Discount AMOUNT";
	public static final  String  TALLY_TYPE="Number";
	public static final  String  TALLY_TYPE_AMOUNT="AMOUNT";
	public static final  String  TALLY_TYPE_DISCOUNT="DISCOUNT";
	public static final  String  TALLY_TYPE_PARENT="Indirect Expenses";
	public static final  String  TALLY_TYPE_ROUNDOFF="INVOICE ROUNDING";
	public static final  Integer  PERIOD_OF_MONTH=1;
	public static final  Integer  PERIOD_OF_QUATER=2;
	public static final  Integer  PERIOD_OF_WEEK=3;
	public static final  Integer  PERIOD_OF_DAY=4;
	public static final  Long  COMPLETED_STATUS=124L;
	public static final  Long  NEW_STATUS=123L;
	public static final  String  REPORT_NAME="InvoiceTaxGroup2";
	public static final  String  COPY_TEXT="ORIGINAL";
	public static final  String  PRINT_HEADER_TEXT="TAX INVOICE";
	public static final Long TRANSACTION_TYPE_CUSTOMER = 26L;
  public static final Long TRANSACTION_TYPE_SUPPLIER = 27L;
	public static final Long PARTY_TYPE_CUSTOMER_ID = 1L;
	public static final Long PARTY_TYPE_SUPPLIER_ID = 2L;
	//Eway bill
	public static final  String  EWAY_BILL_FAILED_TO_GENERATE="E-Way Bill Generation Failed !";
	public static final Integer EWAY_BILL_ACTIVE=2;
	public static final Integer EWAY_BILL_INITIAL_RECORD=1;
	public static final Integer EWAY_BILL_FAILED=4;
	public static final Integer EWAY_BILL_CANCELLED=3;
	public static final Long MATERIAL_NOTE_TYPE_REWORK = 1L;
	public static final Long MATERIAL_NOTE_TYPE_SCRAP = 2L;
}
