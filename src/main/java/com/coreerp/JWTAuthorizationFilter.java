package com.coreerp;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.coreerp.jwt.JwtTokenUtil;

import io.jsonwebtoken.Jwts;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	final static Logger log = LogManager.getLogger(JWTAuthorizationFilter.class);
	
//	@Value("${jwt.header}")
//	private String tokenHeader;

//	@Value("${jwt.token.prefix}")
//	private String tokenPrefix;

	@Autowired
	JwtTokenUtil jwtTokenUtil;

	public JWTAuthorizationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		//log.info("in doFilterInternal ");
		//log.info("this.tokenHeader :"+ this.tokenHeader);
		log.info("req: "+req.getRequestURL());
		try {
			//String header = req.getHeader(this.tokenHeader);
			String header = req.getHeader("Authorization");
			//log.info("header: "+header);
			if (header == null || !header.startsWith("Bearer")) {
//				log.info("cannot authenticate");
				chain.doFilter(req, res);
				return;
			}
		} catch (NullPointerException e) {
			log.info("NPE... cannot authenticate");
			chain.doFilter(req, res);
			return;
		}

		UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		//log.info("Setting security context: "+authentication);

		chain.doFilter(req, res);
	}

	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest req) {

		String token = req.getHeader("Authorization");

		if (token != null) {
			//String user = jwtTokenUtil.getUsernameFromToken(token);
			
			try {
				String user = Jwts.parser()
				          .setSigningKey("multierp")
				          .parseClaimsJws(token.replace("Bearer", ""))
				          .getBody()
				          //.getSubject()
				          .get("username").toString()
				          ;
				
				if (user != null) {
					//log.info("got user: "+user);
					return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
				}
			}catch(io.jsonwebtoken.ExpiredJwtException e) {
				log.error("Token Expired: "+e);
				return null;
			}
			return null;
		}

		return null;

	}
}
