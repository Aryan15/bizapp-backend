package com.coreerp.reports.serviceimpl;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.reports.dao.RecentDeliveryChallanRepository;
import com.coreerp.reports.service.RecentDeliveryChallanService;

@Service
public class RecentDeliveryChallanServiceImpl implements RecentDeliveryChallanService {

	@Autowired
	RecentDeliveryChallanRepository recentDeliveryChallanRepository;
	
	
	@Override
	public Page<DeliveryChallanHeader> getPage(TransactionType deliveryChallanType, FinancialYear financialYear, String sortColumn, String sortDirection, int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentDeliveryChallanRepository.findByDeliveryChallanTypeAndFinancialYear(deliveryChallanType, financialYear, request);
	}

	@Override
	public Page<DeliveryChallanHeader> getPageByDeliveryChallanNumber(TransactionType deliveryChallanType, String deliveryChallanNumber, String sortColumn,
			String sortDirection, int pageNumber, int pageSize) {
		
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentDeliveryChallanRepository.findByDeliveryChallanTypeAndDeliveryChallanNumberContainsIgnoreCase(deliveryChallanType, deliveryChallanNumber,  request);
	}

	@Override
	public Page<DeliveryChallanHeader> getPageByDeliveryChallanDate(TransactionType deliveryChallanType, Date deliveryChallanDate, String sortColumn,
			String sortDirection, int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentDeliveryChallanRepository.findByDeliveryChallanTypeAndDeliveryChallanDate(deliveryChallanType, deliveryChallanDate, request);
	}

	@Override
	public Page<DeliveryChallanHeader> getPageByPartyName(TransactionType deliveryChallanType, List<Party> parties,String sortColumn, String sortDirection,
			int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentDeliveryChallanRepository.findByDeliveryChallanTypeAndPartyIn(deliveryChallanType, parties, request);
	}


	@Override
	public Page<DeliveryChallanHeader> getDcPageList(TransactionType dcType, FinancialYear financialYear, String serchText, String sortColumn, String sortDirection,
												 int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);

		Long dcTType=dcType.getId();
		Long finanicialYear=financialYear.getId();

		Page<DeliveryChallanHeader> recentDo =recentDeliveryChallanRepository.getAllRecentTransaction(dcTType,finanicialYear,serchText,request);
		return recentDo;
	}



}
