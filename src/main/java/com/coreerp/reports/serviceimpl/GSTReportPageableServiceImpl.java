package com.coreerp.reports.serviceimpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.reports.dao.DeliveryChallanItemReportPageableRepository;
import com.coreerp.reports.dao.GSTReportRepository;
import com.coreerp.reports.model.B2BGSTReport;
import com.coreerp.reports.service.GSTReportPageableService;

@Service
public class GSTReportPageableServiceImpl implements GSTReportPageableService {

	
	@Autowired
	GSTReportRepository gstReportRepository;
	
	@Override
	public Page<B2BGSTReport> getPagedB2BReport(Long financialYearId, Date fromDate, Date toDate, Long transactionId,
			Integer pageNumber, Integer pageSize, String sortDirection, String sortColumn) {

		PageRequest pageRequest = null;
		if(pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0){
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		if(pageSize == -99){
			pageRequest =
	                new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		
		return gstReportRepository.getB2BReport(financialYearId, fromDate, toDate, transactionId, pageRequest);
	}
	
}
