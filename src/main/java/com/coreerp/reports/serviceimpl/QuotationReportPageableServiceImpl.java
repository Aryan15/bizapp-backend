package com.coreerp.reports.serviceimpl;

import com.coreerp.dto.QuotationReportDTO;
import com.coreerp.reports.dao.QuotationReportRepository;
import com.coreerp.reports.model.QuotationSummaryReport;
import com.coreerp.reports.service.QuotationReportPageableService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class QuotationReportPageableServiceImpl implements QuotationReportPageableService {

    final static Logger log = LogManager.getLogger(QuotationReportPageableServiceImpl.class);

    @Autowired
    QuotationReportRepository quotationReportRepository;

    @Override
    public QuotationReportDTO getPagedQuotationReport(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId,
                                                      List<Long> partyTypeIds, String createdBy, Integer pageNumber, Integer pageSize, String sortDirection, String sortColumn) {

        PageRequest pageRequest = null;
        if (pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0) {
            pageRequest =
                    new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "quotationDate");
        }

        if (pageSize == -99) {
            pageRequest =
                    new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "quotationDate");
        }

        Page<QuotationSummaryReport> quoReport = quotationReportRepository.getReport(financialYearId, fromDate, toDate, partyId, partyTypeIds, transactionTypeId, createdBy, pageRequest);

        log.info(quoReport.getContent()+"quoReport.getContent()");
        QuotationReportDTO retReport = new QuotationReportDTO();
        retReport.setQuotationSummaryReports(quoReport.getContent());
        retReport.setTotalCount(quoReport.getTotalElements());



        return retReport;

    }
}