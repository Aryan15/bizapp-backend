package com.coreerp.reports.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.coreerp.dto.InvoiceTaxReportDTO;
import com.coreerp.model.GlobalSetting;
import com.coreerp.reports.dao.InvoiceTaxReportPageableRepository;
import com.coreerp.reports.model.InvoiceTaxReport;
import com.coreerp.reports.service.InvoiceTaxReportPageableService;
import com.coreerp.service.FinancialYearService;
import com.coreerp.service.GlobalSettingService;

@Service
public class InvoiceTaxReportPageableServiceImpl implements InvoiceTaxReportPageableService {

	@Autowired
	InvoiceTaxReportPageableRepository invoiceTaxReportPageableRepository;
	
	@Autowired
	FinancialYearService financialYearService;
	
	@Autowired
	GlobalSettingService globalSettingService;
	
	@Override
	public InvoiceTaxReportDTO getPagedInvoiceTaxReport(Long invoiceTypeId, Long financialYearId, Date fromDate,
			Date toDate, Long partyId, Long materialId, List<Long> partyTypeIds, Integer pageNumber, Integer pageSize,
			String sortDirection, String sortColumn) {
		//Sort sort = new Sort(new Order(Direction.ASC, "invoiceNumber"));
		
		List<Order> sortOrders = new ArrayList<Order>();
		sortOrders.add(new Order(sortDirection.equals("desc") ? Direction.DESC : Direction.ASC, "invoiceNumber")); // To make sure invoiceNumber is always part of ordering
		if(sortColumn != null){			
			if (sortDirection.equals("asc")) {
				sortOrders.add(new Order(Direction.ASC, sortColumn));
			} else if (sortDirection.equals("desc")) {
				sortOrders.add(new Order(Direction.DESC, sortColumn));
			} 
		}
		PageRequest pageRequest = null;
		if(pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0){
			pageRequest =
                new PageRequest(pageNumber, pageSize, new Sort(sortOrders));
		}
		if(pageSize == -99){
			pageRequest =
	                new PageRequest(pageNumber, Integer.MAX_VALUE,new Sort(sortOrders));
		}
		
		InvoiceTaxReportDTO invoiceTaxReportDTO = new InvoiceTaxReportDTO();
		
		GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);
		
		Page<InvoiceTaxReport> invoiceTaxReport = null;

		Integer isItemLevelTax = invoiceTypeId == 1 ? globalSetting.getItemLevelTax() : globalSetting.getItemLevelTaxPurchase();

		if(isItemLevelTax == 1) {
			invoiceTaxReport = invoiceTaxReportPageableRepository.getReport(financialYearId, fromDate, toDate, partyId, materialId, partyTypeIds, invoiceTypeId, pageRequest);
			
		}else {
			invoiceTaxReport = invoiceTaxReportPageableRepository.getHeaderLevelTaxReport(financialYearId, fromDate, toDate, partyId, materialId, partyTypeIds, invoiceTypeId, pageRequest);
		}
		
		 
		invoiceTaxReportDTO.setInvoiceTaxReports(invoiceTaxReport.getContent());
		invoiceTaxReportDTO.setTotalCount(invoiceTaxReport.getTotalElements());
		return invoiceTaxReportDTO;
	}

//	@Override
//	public Long getInvoiceTaxReportCount(Long invoiceTypeId, Long financialYearId, Date fromDate, Date toDate,
//			Long partyId, Long materialId, List<Long> partyTypeIds) {
//		
//
//		return (long) invoiceTaxReportPageableRepository.getReport(financialYearId, fromDate, toDate, partyId, materialId, partyTypeIds, invoiceTypeId, null).getContent().size();
//	}

}
