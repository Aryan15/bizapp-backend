package com.coreerp.reports.serviceimpl;

  import com.coreerp.reports.dao.RecentEWayBillRepository;
  import com.coreerp.reports.model.EWayBillReport;
  import com.coreerp.reports.model.EwayBillRecentReport;
  import com.coreerp.reports.service.RecentEWayBillService;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.data.domain.Page;
  import org.springframework.data.domain.PageRequest;
  import org.springframework.data.domain.Sort;
  import org.springframework.stereotype.Service;

  import java.util.Date;

@Service
public class RecentEWayBillServiceImpl implements RecentEWayBillService {

    @Autowired
    RecentEWayBillRepository recentEWayBillRepository;

    @Override
    public Page<EwayBillRecentReport> getPage(String search, String sortColumn, String sortDirection, int pageNumber, int pageSize) {

        //PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
        PageRequest pageRequest = null;
        if( pageNumber >= 0 && pageSize >= 0){
            pageRequest =
                    new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "eWayBillDate");
        }

        if(pageSize == -99){
            pageRequest =
                    new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "eWayBillDate");
        }
        return recentEWayBillRepository.getEwayBillRecent(search, pageRequest);

    }

     @Override
     public Page<EWayBillReport> getReport( Date fromDate, Date toDate, Long partyId, String sortColumn, String sortDirection, int pageNumber, int pageSize) {

         //PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
         PageRequest pageRequest = null;
         if( pageNumber >= 0 && pageSize >= 0){
             pageRequest =
                     new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "eWayBillDate");
         }

         if(pageSize == -99){
             pageRequest =
                     new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "eWayBillDate");
         }
         return recentEWayBillRepository.getEwayBillReport(fromDate, toDate, partyId, pageRequest);

     }
 }
