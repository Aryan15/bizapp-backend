package com.coreerp.reports.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import com.coreerp.dto.BankDTO;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coreerp.controller.PartyController;
import com.coreerp.dao.PartyBankMapRepository;
import com.coreerp.dto.PartyBankMapDTO;
import com.coreerp.mapper.PartyBankMapper;
import com.coreerp.model.Party;
import com.coreerp.model.PartyBankMap;
import com.coreerp.reports.service.PartyBankMapService;
import com.coreerp.service.PartyService;

@Service
public class PartyBankMapServiceImpl implements PartyBankMapService {

	
final static Logger log = LogManager.getLogger(PartyBankMapServiceImpl.class);
	
	@Autowired
	PartyBankMapRepository partyBankMapRepository;
	
	@Autowired
	PartyBankMapper partyBankMapMapper;
	
	@Autowired
	PartyService partyService;
	
	
	@Override
	public PartyBankMapDTO save(PartyBankMapDTO partyBankMapDTO) {
		return partyBankMapMapper.modelToDTOMap(partyBankMapRepository.save(partyBankMapMapper.dtoToModelMap(partyBankMapDTO)));

	}

	@Override
	public PartyBankMapDTO getById(Long partyBankMapId) {
		return partyBankMapMapper.modelToDTOMap(partyBankMapRepository.getOne(partyBankMapId));
	}

	@Override
	public PartyBankMap getModelById(Long partyBankMapId) {
		
		return partyBankMapRepository.getOne(partyBankMapId);
	}

	@Override
	public void deleteById(Long id) {
		partyBankMapRepository.deleteById(id);

	}

	@Override
	public List<PartyBankMapDTO> saveList(List<PartyBankMapDTO> partyBankMapDTOList) {
		List<PartyBankMapDTO> dtoOut = new ArrayList<PartyBankMapDTO>();
		
		partyBankMapDTOList.forEach(dto -> {
			log.info(dto.getBankAdCode()+" ======================");
			dtoOut.add(partyBankMapMapper.modelToDTOMap(partyBankMapRepository.save(partyBankMapMapper.dtoToModelMap(dto))));

		});

		return dtoOut;
	}

	@Override
	public List<PartyBankMapDTO> getForParty(Long partyId) {
		//Party party = partyService.getPartyObjectById(partyId);
		
//		if(party != null){
//			log.info("getForParty: party: "+party.getName());
//		}
		
		List<PartyBankMap> partyBankMapLists = partyBankMapRepository.findByPartyId(partyId);
		
		log.info("getForParty: partyBankMapLists size: "+partyBankMapLists.size());
		
		return partyBankMapMapper.modelToDTOList(partyBankMapLists);
	}

	@Override
	public List<PartyBankMapDTO> getForCompany(Long companyId) {
		List<PartyBankMap> partyBankMapLists = partyBankMapRepository.findByCompanyId(companyId);
		
		log.info("getForCompany: partyBankMapLists size: "+partyBankMapLists.size());
		
		return partyBankMapMapper.modelToDTOList(partyBankMapLists);
	}
	@Override
	public List<PartyBankMapDTO> getAll() {
		return partyBankMapMapper.modelToDTOList(partyBankMapRepository.findAll());
	}

}
