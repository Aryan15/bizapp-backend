package com.coreerp.reports.serviceimpl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.dto.VoucherHeaderDTO;
import com.coreerp.reports.dao.VoucherReportRepository;
import com.coreerp.reports.model.VoucherReportModel;
import com.coreerp.reports.service.VoucherPageableService;

@Service
public class VoucherPageableServiceImpl implements VoucherPageableService {

	@Autowired
	VoucherReportRepository voucherReportRepository;
	@Override
	public Page<VoucherReportModel> getPagedVoucherReport(Long transactionTypeId, Long financialYearId, Date fromDate,
			Date toDate, Long expenseId, Integer pageNumber, Integer pageSize, String sortDirection, String sortColumn) {

		PageRequest pageRequest = null;
		if(pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0){
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "voucherDate");
		}
		
		if(pageSize == -99){
			pageRequest =
	                new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "voucherDate");
		}
		
		
		return voucherReportRepository.getReport(financialYearId, fromDate, toDate, transactionTypeId, expenseId, pageRequest);
	}

}
