package com.coreerp.reports.serviceimpl;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.reports.dao.RecentQuotationRepository;
import com.coreerp.reports.service.RecentQuotationService;

@Service
public class RecentQuotationServiceImpl implements RecentQuotationService {
	
	@Autowired
	RecentQuotationRepository recentQuotationRepository;

	@Override
	public Page<QuotationHeader> getPage(TransactionType quotationType, FinancialYear financialYear, String sortColumn, String sortDirection, int pageNumber, int pageSize) {

		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentQuotationRepository.findByQuotationTypeAndFinancialYear(quotationType, financialYear, request);
	}

	@Override
	public Page<QuotationHeader> getPageByQuotationNumber(TransactionType quotationType, FinancialYear financialYear, String quotationNumber, String sortColumn,
			String sortDirection, int pageNumber, int pageSize) {
		
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentQuotationRepository.findByQuotationTypeAndFinancialYearAndQuotationNumberContainsIgnoreCase(quotationType, financialYear, quotationNumber,  request);
	}

	@Override
	public Page<QuotationHeader> getPageByQuotationDate(TransactionType quotationType, FinancialYear financialYear, Date quotationStartDate, Date quotationEndDate, String sortColumn, String sortDirection,
			int pageNumber, int pageSize) {
		
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentQuotationRepository.findByQuotationTypeAndFinancialYearAndQuotationDateBetween(quotationType, financialYear, quotationStartDate, quotationEndDate, request);
	}

	@Override
	public Page<QuotationHeader> getPageByPartyName(TransactionType quotationType, FinancialYear financialYear, List<Party> parties, String sortColumn, String sortDirection,
			int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentQuotationRepository.findByQuotationTypeAndFinancialYearAndPartyIn(quotationType, financialYear, parties, request);
	}

	@Override
	public Page<QuotationHeader> getQuotationPageList(TransactionType quotationType, FinancialYear financialYear, String serchText, String sortColumn, String sortDirection,
												 int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);

		Long qType=quotationType.getId();
		Long finanicialYear=financialYear.getId();

		Page<QuotationHeader> recentQ =recentQuotationRepository.getAllRecentTransaction(qType,finanicialYear,serchText,request);
		return recentQ;
	}





}
