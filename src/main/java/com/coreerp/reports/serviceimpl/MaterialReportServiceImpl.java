package com.coreerp.reports.serviceimpl;

import java.util.List;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.MaterialTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.model.Material;
import com.coreerp.model.MaterialType;
import com.coreerp.model.SupplyType;
import com.coreerp.reports.dao.MaterialReportRepository;
import com.coreerp.reports.service.MaterialReportService;

@Service
public class MaterialReportServiceImpl implements MaterialReportService {
	
	@Autowired
	MaterialReportRepository materialReportRepository;

	@Autowired
	MaterialTypeRepository materialTypeRepository;

	@Override
	public Page<Material> getPage(SupplyType supplyType, String sortColumn, String sortDirection, int pageNumber, int pageSize) {
		
		
		//PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		PageRequest pageRequest = null;
		if( pageNumber >= 0 && pageSize >= 0){
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		
		if(pageSize == -99){
			pageRequest =
	                new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}

		return materialReportRepository.findBySupplyType(supplyType,pageRequest);
	}

//	@Override
//	public Long getTotalCount(SupplyType supplyType) {
//		return materialReportRepository.countBySupplyType(supplyType);
//	}

	@Override
	public Page<Material> getPageByName(SupplyType supplyType, String name, String sortColumn, String sortDirection, int pageNumber, int pageSize) {
		
		PageRequest pageRequest = null;
		if( pageNumber >= 0 && pageSize >= 0){
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		
		
		return materialReportRepository.findBySupplyTypeAndNameContainsIgnoreCase(supplyType, name, pageRequest);
	}

	@Override
	public Page<Material> getPageByPartNumber(SupplyType supplyType, String partNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize) {
		
		PageRequest pageRequest = null;
		if( pageNumber >= 0 && pageSize >= 0){
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		
		return materialReportRepository.findBySupplyTypeAndPartNumberContainsIgnoreCase(supplyType, partNumber, pageRequest);
	}

	public Page<Material> getPageNonJobwork(SupplyType supplyType, String sortColumn, String sortDirection, int pageNumber, int pageSize){


		//PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		PageRequest pageRequest = null;
		if( pageNumber >= 0 && pageSize >= 0){
			pageRequest =
					new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}

		if(pageSize == -99){
			pageRequest =
					new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}

		MaterialType materialType = materialTypeRepository.findByName(ApplicationConstants.MATERIAL_TYPE_JOBWORK);

		return materialReportRepository.findBySupplyTypeAndMaterialTypeNot(supplyType,materialType,pageRequest);
	}

	public Page<Material> getPageByNameNonJobwork(SupplyType supplyType, String name, String sortColumn, String sortDirection, int pageNumber, int pageSize){
		PageRequest pageRequest = null;
		if( pageNumber >= 0 && pageSize >= 0){
			pageRequest =
					new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}

		MaterialType materialType = materialTypeRepository.findByName(ApplicationConstants.MATERIAL_TYPE_JOBWORK);
		return materialReportRepository.findBySupplyTypeAndNameContainsIgnoreCaseAndMaterialTypeNot(supplyType, name, materialType, pageRequest);
	}

	public Page<Material> getPageByPartNumberNonJobwork(SupplyType supplyType, String partNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize){
		PageRequest pageRequest = null;
		if( pageNumber >= 0 && pageSize >= 0){
			pageRequest =
					new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}

		MaterialType materialType = materialTypeRepository.findByName(ApplicationConstants.MATERIAL_TYPE_JOBWORK);
		return materialReportRepository.findBySupplyTypeAndPartNumberContainsIgnoreCaseAndMaterialTypeNot(supplyType, partNumber, materialType, pageRequest);
	}


	
}


