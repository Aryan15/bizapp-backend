package com.coreerp.reports.serviceimpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.reports.dao.DeliveryChallanItemReportPageableRepository;
import com.coreerp.reports.model.DeliveryChallanItemModel;
import com.coreerp.reports.service.DeliveryChallanItemReportPageableService;

@Service
public class DeliveryChallanItemReportPageableServiceImpl implements DeliveryChallanItemReportPageableService {

	
	@Autowired
	DeliveryChallanItemReportPageableRepository deliveryChallanItemReportPageableRepository;
	
	
	@Override
	public List<DeliveryChallanItemModel> getPagedDeliveryChallanItemReport(Long deliveryChallanTypeId,Long financialYearId, Date fromDate,
			Date toDate, Long partyId,  Long materialId, List<Long> partyTypeIds, Integer pageNumber, Integer pageSize,
			String sortDirection, String sortColumn) {
		
		PageRequest pageRequest = null;
		if(pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0){
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "deliveryChallanDate");
		}
		if(pageSize == -99){
			pageRequest =
	                new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "deliveryChallanDate");
		}
		return deliveryChallanItemReportPageableRepository.getReport(deliveryChallanTypeId,financialYearId, fromDate, toDate, partyId,  materialId, partyTypeIds, pageRequest).getContent();
		

	}

	@Override
	public Long getDeliveryChallanItemReportCount(Long deliveryChallanTypeId,Long financialYearId, Date fromDate, Date toDate, Long partyId,
			List<Long> partyTypeIds, Long materialId) {
		return (long) deliveryChallanItemReportPageableRepository.getReport(deliveryChallanTypeId,financialYearId, fromDate, toDate, partyId, materialId, partyTypeIds, null).getContent().size();
	}

}
