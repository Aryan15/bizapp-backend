package com.coreerp.reports.serviceimpl;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.coreerp.reports.dao.RecentPayableReceivableRepository;
import com.coreerp.reports.service.RecentPayableReceivableService;

@Service
public class RecentPayableReceivableServiceImpl implements RecentPayableReceivableService {

	@Autowired
	RecentPayableReceivableRepository recentPayableReceivableRepository;
	
	@Override
	public Page<PayableReceivableHeader> getPage(TransactionType transactionType,FinancialYear financialYear, String sortColumn,
			String sortDirection, int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		
		return recentPayableReceivableRepository.findByTransactionTypeAndFinancialYear(transactionType,financialYear, request);
	}

	@Override
	public Page<PayableReceivableHeader> getPageByPayReferenceNumber(TransactionType transactionType, FinancialYear financialYear,String invoiceNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		return recentPayableReceivableRepository.findByTransactionTypeAndFinancialYearAndPayReferenceNumberContainsIgnoreCase(transactionType,financialYear, invoiceNumber, request);
	}

	@Override
	public Page<PayableReceivableHeader> getPageByPayReferenceDate(TransactionType transactionType,FinancialYear financialYear, Date invoiceDate,
			String sortColumn, String sortDirection, int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		return recentPayableReceivableRepository.findByTransactionTypeAndFinancialYearAndPayReferenceDate(transactionType,financialYear, invoiceDate, request);
	}

	@Override
	public Page<PayableReceivableHeader> getPageByPartyName(TransactionType transactionType,FinancialYear financialYear, List<Party> parties,
			String sortColumn, String sortDirection, int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		return recentPayableReceivableRepository.findByTransactionTypeAndFinancialYearAndPartyIn(transactionType,financialYear, parties, request);
	}


	@Override
	public Page<PayableReceivableHeader> getPayRecPageList(TransactionType payType, FinancialYear financialYear, String serchText, String sortColumn, String sortDirection,
										  int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);

		Long payTType=payType.getId();
		Long finanicialYear=financialYear.getId();

		Page<PayableReceivableHeader> recentPay =recentPayableReceivableRepository.getAllRecentTransaction(payTType,finanicialYear,serchText,request);
		return recentPay;
	}

}
