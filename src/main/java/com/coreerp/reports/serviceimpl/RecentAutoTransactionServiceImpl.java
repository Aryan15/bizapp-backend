package com.coreerp.reports.serviceimpl;

import com.coreerp.model.AutoTransactionGeneration;

import com.coreerp.reports.dao.RecentAutoTransactionRepository;
import com.coreerp.reports.service.RecentAutoTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class RecentAutoTransactionServiceImpl implements RecentAutoTransactionService {

    @Autowired
    RecentAutoTransactionRepository recentAutoTransactionRepository;


    @Override
    public Page<AutoTransactionGeneration> getPage(String sortColumn, String sortDirection,
                                                   int pageNumber, int pageSize) {

        PageRequest pageRequest = null;
        if( pageNumber >= 0 && pageSize >= 0) {

            pageRequest = new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn);
        }
        if(pageSize == -99) {
            pageRequest = new PageRequest(pageNumber ,  Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn);

        }
        return recentAutoTransactionRepository.findAll(pageRequest);
    }


    @Override
    public Page<AutoTransactionGeneration> getPageByInvoiceNumber(String invoiceNumber,
                                                          String sortColumn, String sortDirection, int pageNumber, int pageSize) {

        PageRequest pageRequest = null;
        if( pageNumber >= 0 && pageSize >= 0) {
            pageRequest =

                    pageRequest = new PageRequest(pageNumber , pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn);
        }
        return recentAutoTransactionRepository.findByInvoiceNumberContainsIgnoreCase(invoiceNumber, pageRequest);
    }

    @Override
    public Page<AutoTransactionGeneration> getPageByStartDate(Date startDate, String sortColumn,
                                                        String sortDirection, int pageNumber, int pageSize) {
        PageRequest pageRequest = null;
        if( pageNumber >= 0 && pageSize >= 0) {


            pageRequest =   new PageRequest(pageNumber , pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn);
        }

        return recentAutoTransactionRepository.findByStartDate(startDate, pageRequest);
    }
}
