package com.coreerp.reports.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.model.Party;
import com.coreerp.model.PartyType;
import com.coreerp.reports.dao.PartyReportRepository;
import com.coreerp.reports.service.PartyReportService;

@Service
public class PartyReportServiceImpl implements PartyReportService {

	@Autowired
	PartyReportRepository partReportRepository;
	
	@Override
	public List<Party> getPage(List<PartyType> partyTypes, String sortColumn, String sortDirection, int pageNumber,
			int pageSize) {
		//PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		PageRequest pageRequest = null;
		if( pageNumber >= 0 && pageSize >= 0){
			//pageNumber--;
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "name");
		}
		if(pageSize == -99){
			pageRequest =
	                new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "name");
		}
		return partReportRepository.findByPartyTypeIn(partyTypes, pageRequest).getContent();
	}

	@Override
	public List<Party> getPageByName(List<PartyType> partyTypes, String name, String sortColumn, String sortDirection,
			int pageNumber, int pageSize) {
		
		//PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		PageRequest pageRequest = null;
		if( pageNumber >= 0 && pageSize >= 0){
			//pageNumber--;
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "name");
		}
		
		return partReportRepository.findByPartyTypeInAndNameContainsIgnoreCaseOrPartyCodeContainsIgnoreCase(partyTypes,name,name,pageRequest).getContent();
	}

	@Override
	public Long getTotalCount() {
		return partReportRepository.count();
	}

	@Override
	public Long getTotalCountByName(List<PartyType> partyTypes, String searchText) {
		return partReportRepository.countByPartyTypeInAndNameContainsIgnoreCaseOrPartyCodeContainsIgnoreCase(partyTypes, searchText,searchText);
	}

	@Override
	public Long getTotalCountForPartyTypes(List<PartyType> partyTypes) {
		return partReportRepository.countByPartyTypeIn(partyTypes);
	}

	@Override
	public Page<Party> getPageReport(List<Long> partyTypeIds, String createdBy, String sortColumn, String sortDirection, int pageNumber, int pageSize) {
		
		PageRequest pageRequest = null;
		if( pageNumber >= 0 && pageSize >= 0){
			//pageNumber--;
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "name");
		}
		
		return partReportRepository.findByPartyTypeInAndCreatedBy(partyTypeIds, createdBy, pageRequest);
	}
	

}
