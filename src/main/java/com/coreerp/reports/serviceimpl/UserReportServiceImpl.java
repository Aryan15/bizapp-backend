package com.coreerp.reports.serviceimpl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.RoleRepository;
import com.coreerp.dao.RoleTypeRepository;
import com.coreerp.model.Role;
import com.coreerp.model.RoleType;
import com.coreerp.model.User;
import com.coreerp.reports.dao.UserReportRepository;
import com.coreerp.reports.service.UserReportService;

@Service
public class UserReportServiceImpl implements UserReportService {

	@Autowired
	UserReportRepository userReportRepository ; 
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	RoleTypeRepository roleTypeRepository;
	
	@Override
	public Page<User> getPage(String sortColumn, String sortDirection, int pageNumber, int pageSize) {
		
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
//		Set<Role> roles = new HashSet<Role>();
		
		RoleType systemRoleType = roleTypeRepository.findByName(ApplicationConstants.ROLE_TYPE_SYSTEM);
		
		List<Role> roles = roleRepository.findByRoleType(systemRoleType);
		
		return userReportRepository.findDistinctByRolesNotIn(roles, request);
	}

	@Override
	public Page<User> getPageByName(String name, String sortColumn, String sortDirection, int pageNumber,
			int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return userReportRepository.findByNameContainsIgnoreCase(name, request);
	}

	@Override
	public Page<User> getPageByLastName(String lastName, String sortColumn, String sortDirection, int pageNumber,
			int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return userReportRepository.findByLastNameContainsIgnoreCase(lastName, request);
	}

//	@Override
//	public Long getTotalCount() {
//		return userReportRepository.count();
//	}
//
//	@Override
//	public Long getTotalCountByName(String searchText) {
//		return userReportRepository.countByNameContainsIgnoreCase(searchText);
//	}
//
//	@Override
//	public Long getTotalCountByLastName(String searchText) {
//		return userReportRepository.countByLastNameContainsIgnoreCase(searchText);
//	}

}
