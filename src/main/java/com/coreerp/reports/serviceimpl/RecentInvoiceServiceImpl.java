package com.coreerp.reports.serviceimpl;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.reports.dao.RecentInvoiceRepository;
import com.coreerp.reports.service.RecentInvoiceService;

@Service
public class RecentInvoiceServiceImpl implements RecentInvoiceService {

	@Autowired
	RecentInvoiceRepository recentInvoiceRepository;
	
	
	
	@Override
	public Page<InvoiceHeader> getPage(TransactionType invoiceType, FinancialYear financialYear, String sortColumn, String sortDirection, int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentInvoiceRepository.findByInvoiceTypeAndFinancialYear(invoiceType, financialYear, request);
	}

	@Override
	public Page<InvoiceHeader> getPageByInvoiceNumber(TransactionType invoiceType, FinancialYear financialYear, String invoiceNumber, String sortColumn, String sortDirection,
			int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);

		return recentInvoiceRepository.findByInvoiceTypeAndFinancialYearAndInvoiceNumberContainsIgnoreCase(invoiceType, financialYear, invoiceNumber,  request);
	}

	@Override
	public Page<InvoiceHeader> getPageByInvoiceDate(TransactionType invoiceType, FinancialYear financialYear, Date invoiceFromDate, Date invoiceToDate, String sortColumn, String sortDirection,
			int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentInvoiceRepository.findByInvoiceTypeAndFinancialYearAndInvoiceDateBetween(invoiceType, financialYear, invoiceFromDate, invoiceToDate, request);
	}

	@Override
	public Page<InvoiceHeader> getPageByPartyName(TransactionType invoiceType, FinancialYear financialYear, List<Party> parties, String sortColumn, String sortDirection,
			int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentInvoiceRepository.findByInvoiceTypeAndFinancialYearAndPartyIn(invoiceType, financialYear, parties,request);
	}



	@Override
	public Page<InvoiceHeader> getInvoicePageList(TransactionType invoiceType, FinancialYear financialYear, String serchText, String sortColumn, String sortDirection,
													 int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);

		Long invoiceTType=invoiceType.getId();
		Long finanicialYear=financialYear.getId();

		Page<InvoiceHeader> recentInvoice =recentInvoiceRepository.getAllRecentTransaction(invoiceTType,finanicialYear,serchText,request);
		return recentInvoice;
	}

}
