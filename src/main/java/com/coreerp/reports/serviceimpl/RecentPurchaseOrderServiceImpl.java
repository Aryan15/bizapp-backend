package com.coreerp.reports.serviceimpl;

import java.util.Date;
import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.model.FinancialYear;
import com.coreerp.model.Party;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.model.TransactionType;
import com.coreerp.reports.dao.RecentPurchaseOrderHeaderRepository;
import com.coreerp.reports.service.RecentPurchaseOrderService;

@Service
public class RecentPurchaseOrderServiceImpl implements RecentPurchaseOrderService {

	@Autowired
	RecentPurchaseOrderHeaderRepository recentPurchaseOrderHeaderRepository;
	
		
	@Override
	public Page<PurchaseOrderHeader> getPage(TransactionType purchaseOrderType, FinancialYear financialYear, String sortColumn, String sortDirection, int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);

		return recentPurchaseOrderHeaderRepository.findByPurchaseOrderTypeAndFinancialYear(purchaseOrderType, financialYear, request);
	}

	@Override
	public Page<PurchaseOrderHeader> getPageByPurchaseOrderNumber(TransactionType purchaseOrderType, String purchaseOrderNumber, String sortColumn,
			String sortDirection, int pageNumber, int pageSize) {
		
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentPurchaseOrderHeaderRepository.findByPurchaseOrderTypeAndPurchaseOrderNumberContainsIgnoreCase(purchaseOrderType, purchaseOrderNumber,  request);
	}

	@Override
	public Page<PurchaseOrderHeader> getPageByPurchaseOrderDate(TransactionType purchaseOrderType, Date purchaseOrderDate, String sortColumn,
			String sortDirection, int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentPurchaseOrderHeaderRepository.findByPurchaseOrderTypeAndPurchaseOrderDate(purchaseOrderType, purchaseOrderDate, request);
	}

	@Override
	public Page<PurchaseOrderHeader> getPageByPartyName(TransactionType purchaseOrderType, List<Party> parties, String sortColumn, String sortDirection,
			int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);

		return recentPurchaseOrderHeaderRepository.findByPurchaseOrderTypeAndPartyIn(purchaseOrderType, parties, request);
	}

	@Override
	public Page<PurchaseOrderHeader> getPageList(TransactionType purchaseOrderType, FinancialYear financialYear, String serchText, String sortColumn, String sortDirection,
												 int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);

		Long poType=purchaseOrderType.getId();
		Long finanicialYear=financialYear.getId();

		Page<PurchaseOrderHeader> recentPo =recentPurchaseOrderHeaderRepository.getAllRecentTransaction(poType,finanicialYear,serchText,request);
	return recentPo;
	}

}
