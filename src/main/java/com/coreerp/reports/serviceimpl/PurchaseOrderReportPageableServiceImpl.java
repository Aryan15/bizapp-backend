package com.coreerp.reports.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.coreerp.ApplicationConstants;
import com.coreerp.dto.PayableReceivableReportDTO;
import com.coreerp.dto.PurchaseOrderReportDTO;
import com.coreerp.model.PartyType;
import com.coreerp.reports.model.BalanceReport;
import com.coreerp.reports.model.PoBalanceReport;
import com.coreerp.serviceimpl.InvoiceServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.reports.dao.PurchaseOrderReportRepository;
import com.coreerp.reports.model.PurchaseOrderReport;
import com.coreerp.reports.service.PurchaseOrderReportPageableService;

@Service
public class PurchaseOrderReportPageableServiceImpl implements PurchaseOrderReportPageableService {
	final static Logger log = LogManager.getLogger(PurchaseOrderReportPageableServiceImpl.class);
	@Autowired
	PurchaseOrderReportRepository purchaseOrderReportRepository;
	
	
	@Override
	public List<PurchaseOrderReport> getPagedPurchaseOrderReport(Long transactionTypeId, Long financialYearId,
			Date fromDate, Date toDate, Long partyId,  Long materialId,  List<Long> partyTypeIds, Integer pageNumber, Integer pageSize,
			String sortDirection, String sortColumn) {
		
		PageRequest pageRequest = null;
		if(pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0){
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "purchaseOrderDate");
		}
		if(pageSize == -99){
			pageRequest =
	                new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "purchaseOrderDate");
		}
		return purchaseOrderReportRepository.getReport(financialYearId, fromDate, toDate, partyId,materialId, partyTypeIds, transactionTypeId, pageRequest).getContent();
	}

	@Override
	public Long getPurchaseOrderReportCount(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate,
			Long partyId, Long materialId,  List<Long> partyTypeIds) {
		return (long) purchaseOrderReportRepository.getReport(financialYearId, fromDate, toDate, partyId,materialId, partyTypeIds, transactionTypeId, null).getContent().size();
	}


	@Override
	public PurchaseOrderReportDTO getPagedBalanceReport(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId,
															List<Long> partyTypeIds, String createdBy, Long statusId, Integer pageNumber, Integer pageSize, String sortDirection, String sortColumn) {
		PageRequest pageRequest = null;
		if(pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0){
			pageRequest =
					new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "purchaseOrderDate");
		}
		if(pageSize == -99){
			pageRequest =
					new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "purchaseOrderDate");
		}
		PurchaseOrderReportDTO purchaseOrderReportDTO = new PurchaseOrderReportDTO();
        Long dcTransaction =null;
		List<Long> statusIds = new ArrayList<Long>();


		if(transactionTypeId == 11l){
			statusIds.add(62l);
			statusIds.add(65l);
			statusIds.add(66l);
		}

		if(transactionTypeId == 2l){
			statusIds.add(8l);
			statusIds.add(9l);
			statusIds.add(10l);
		}


        if(transactionTypeId ==20l){
			statusIds.add(100l);
			statusIds.add(99l);
			statusIds.add(82l);
			dcTransaction =13l;

		}

		if(transactionTypeId ==21l){
			statusIds.add(96l);
			statusIds.add(101l);
			statusIds.add(102l);
			dcTransaction =16l;

		}

		log.info(statusIds+"---");

		Page<PoBalanceReport> balanceReport =  purchaseOrderReportRepository.getBalanceReport(financialYearId,fromDate,toDate,partyId,partyTypeIds,transactionTypeId,statusIds,dcTransaction,pageRequest);

		purchaseOrderReportDTO.setPoBalanceReport(balanceReport.getContent());
	    purchaseOrderReportDTO.setTotalCount(balanceReport.getTotalElements());

		return purchaseOrderReportDTO;

	}

}
