package com.coreerp.reports.serviceimpl;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.reports.dao.RecentGRNRepository;
import com.coreerp.reports.service.RecentGRNService;

@Service
public class RecentGRNServiceImpl implements RecentGRNService {

	@Autowired
	RecentGRNRepository recentGRNRepository;
	
	@Override
	public Page<GRNHeader> getPage(TransactionType grnType, FinancialYear financialYear, String sortColumn, String sortDirection, int pageNumber,
			int pageSize) {
		if(sortColumn.equals("partyName")) {
			sortColumn = "supplierName";
		}
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentGRNRepository.findByGrnTypeAndFinancialYear(grnType, financialYear, request);
	}

	@Override
	public Page<GRNHeader> getPageByGrnNumber(TransactionType grnType, String grnNumber, String sortColumn,
			String sortDirection, int pageNumber, int pageSize) {
		
		if(sortColumn.equals("partyName")) {
			sortColumn = "supplierName";
		}
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentGRNRepository.findByGrnTypeAndGrnNumberContainsIgnoreCase(grnType, grnNumber, request);
	}

	@Override
	public Page<GRNHeader> getPageByGrnDate(TransactionType grnType, Date grnDate, Date searchEndDate, String sortColumn,
			String sortDirection, int pageNumber, int pageSize) {
		if(sortColumn.equals("partyName")) {
			sortColumn = "supplierName";
		}
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
//		return recentGRNRepository.findByGrnTypeAndGrnDateBetween(grnType, grnDate, searchEndDate, request);
				return recentGRNRepository.findByGrnTypeAndGrnDateBetween(grnType, grnDate, searchEndDate, request);

	}

	@Override
	public Page<GRNHeader> getPageByPartyName(TransactionType grnType, List<Party> parties, String sortColumn,
			String sortDirection, int pageNumber, int pageSize) {
		if(sortColumn.equals("partyName")) {
			sortColumn = "supplierName";
		}
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		return recentGRNRepository.findByGrnTypeAndSupplierIn(grnType, parties, request);
	}

	@Override
	public Page<GRNHeader> getGrnPageList(TransactionType grnType, FinancialYear financialYear, String serchText, String sortColumn, String sortDirection,
												  int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);

		Long grnTType=grnType.getId();
		Long finanicialYear=financialYear.getId();

		Page<GRNHeader> recentGrn =recentGRNRepository.getAllRecentTransaction(grnTType,finanicialYear,serchText,request);
		return recentGrn;
	}


}
