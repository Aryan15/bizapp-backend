package com.coreerp.reports.serviceimpl;


import com.coreerp.model.PettyCashHeader;
import com.coreerp.model.TransactionType;
import com.coreerp.model.VoucherHeader;
import com.coreerp.reports.dao.RecentPettyCashRepository;
import com.coreerp.reports.service.RecentPettyCashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class RecentPettyCashServiceImpl implements RecentPettyCashService {

@Autowired
    RecentPettyCashRepository recentPettyCashRepository;


    @Override
    public Page<PettyCashHeader> getPage(TransactionType pettyCashType, String sortColumn, String sortDirection,
                                         int pageNumber, int pageSize) {
        PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);

        return recentPettyCashRepository.findByPettyCashTypeId(pettyCashType, request);
    }


    @Override
    public Page<PettyCashHeader> getPageByPettyCashNumber(TransactionType pettyCashType, String pettyCashNumber,
                                                      String sortColumn, String sortDirection, int pageNumber, int pageSize) {
        PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);

        return recentPettyCashRepository.findByPettyCashTypeIdAndPettyCashNumberContainsIgnoreCase(pettyCashType, pettyCashNumber, request);
    }

    @Override
    public Page<PettyCashHeader> getPageByPettyCashDate(TransactionType pettyCashType, Date pettyCashDate, String sortColumn,
                                                    String sortDirection, int pageNumber, int pageSize) {
        // TODO Auto-generated method stub
        PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
        return recentPettyCashRepository.findByPettyCashTypeIdAndPettyCashDate(pettyCashType, pettyCashDate, request);
    }
}
