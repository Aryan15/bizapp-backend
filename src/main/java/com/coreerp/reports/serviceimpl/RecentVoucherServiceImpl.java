package com.coreerp.reports.serviceimpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.model.TransactionType;
import com.coreerp.model.VoucherHeader;
import com.coreerp.reports.dao.RecentGRNRepository;
import com.coreerp.reports.dao.RecentVoucherRepository;
import com.coreerp.reports.service.RecentVoucherService;

@Service
public class RecentVoucherServiceImpl implements RecentVoucherService {

	
	@Autowired
	RecentVoucherRepository recentVoucherRepository;
	
	
	@Override
	public Page<VoucherHeader> getPage(TransactionType voucherType, String sortColumn, String sortDirection,
			int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentVoucherRepository.findByVoucherType(voucherType, request);
	}

	@Override
	public Page<VoucherHeader> getPageByVoucherNumber(TransactionType voucherType, String voucherNumber,
			String sortColumn, String sortDirection, int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		
		return recentVoucherRepository.findByVoucherTypeAndVoucherNumberContainsIgnoreCase(voucherType, voucherNumber, request);
	}

	@Override
	public Page<VoucherHeader> getPageByVoucherDate(TransactionType voucherType, Date voucherDate, String sortColumn,
			String sortDirection, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		return recentVoucherRepository.findByVoucherTypeAndVoucherDate(voucherType, voucherDate, request);
	}

	@Override
	public Page<VoucherHeader> getPageByPaidTo(TransactionType voucherType, String paidTo, String sortColumn,
			String sortDirection, int pageNumber, int pageSize) {
		PageRequest request = new PageRequest(pageNumber - 1, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC , sortColumn);
		return recentVoucherRepository.findByVoucherTypeAndPaidToContainsIgnoreCase(voucherType, paidTo, request);
	
	}


}
