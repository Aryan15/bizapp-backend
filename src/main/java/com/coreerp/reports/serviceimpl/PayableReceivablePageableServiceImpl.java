package com.coreerp.reports.serviceimpl;

import java.beans.PropertyDescriptor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.coreerp.ApplicationConstants;
import com.coreerp.controller.PayableReceivableController;
import com.coreerp.dto.InvoiceDTO;
import com.coreerp.dto.InvoiceItemDTO;
import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.model.PartyType;
import com.coreerp.reports.dao.PayableReceivableReportRepository;
import com.coreerp.reports.model.PayableReceivableReport;
import com.coreerp.reports.service.PayableReceivablePageableService;
import com.coreerp.service.PartyTypeService;
import org.apache.commons.beanutils.PropertyUtils;
@Service
public class PayableReceivablePageableServiceImpl implements PayableReceivablePageableService {

	final static Logger log = LogManager.getLogger(PayableReceivablePageableServiceImpl.class);
	
	@Autowired
	PayableReceivableReportRepository payableReceivableReportRepository;
	
	
	@Autowired
	PartyTypeService partyTypeService;
	
	@Override
	public Page<PayableReceivableReport> getPagedPayableReceivableReport(Long transactionTypeId,
																		 Long inTypeId,
																		 Long financialYearId,
			Date fromDate, Date toDate, Long partyId, List<Long> partyTypeIds, Integer pageNumber, Integer pageSize,
			String sortDirection, String sortColumn) {


		PageRequest pageRequest = null;
		if(pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0){
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		if(pageSize == -99){
			pageRequest =
	                new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		return payableReceivableReportRepository.getReport(financialYearId, fromDate, toDate, partyId, partyTypeIds, transactionTypeId, ApplicationConstants.PR_STATUS_DRAFT, inTypeId, pageRequest);
	}

	@Override
	public ByteArrayInputStream payableReceivableToExcel(List<PayableReceivableReport> report) throws IOException{

		String[] COLUMNs = {"Id", "Invoice Number", "Invoice Date", "Invoice Amount"};
	    try(
	        Workbook workbook = new XSSFWorkbook();
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	    ){
	    	
	    	CreationHelper createHelper = workbook.getCreationHelper();
	    	   
	        Sheet sheet = workbook.createSheet("Customers");
	     
	        Font headerFont = workbook.createFont();
	        headerFont.setBold(true);
	        headerFont.setColor(IndexedColors.BLUE.getIndex());
	     
	        CellStyle headerCellStyle = workbook.createCellStyle();
	        headerCellStyle.setFont(headerFont);
	     
	        // Row for Header
	        Row headerRow = sheet.createRow(0);
	     
	        // Header
	        for (int col = 0; col < COLUMNs.length; col++) {
	          Cell cell = headerRow.createCell(col);
	          cell.setCellValue(COLUMNs[col]);
	          cell.setCellStyle(headerCellStyle);
	        }
	     
	       
	        int rowIdx = 1;
	        for(PayableReceivableReport prReport: report) {
	        	Row row = sheet.createRow(rowIdx++);
	        	   
	            row.createCell(0).setCellValue(prReport.getId());
	            row.createCell(1).setCellValue(prReport.getInvoiceNumber());
	            row.createCell(2).setCellValue(prReport.getInvoiceDate());
	            row.createCell(3).setCellValue(prReport.getInvoiceAmount());
	            
	        }
	        workbook.write(out);
	        return new ByteArrayInputStream(out.toByteArray());
	    }
	    
	    
	    
	}
	
	
	@Override
	public Page<PayableReceivableReport>  getReport(
			Long transactionTypeId,
			Long inTypeId,
			Integer pageNumber,
			Integer pageSize,
			String sortDir,
			String sortColumn,
			TransactionReportRequestDTO transactioReportRequestDTO
			){
    	
    	
    	List<Long> partyTypeIds = null;
		
		if(transactioReportRequestDTO.getPartyTypeIds().isEmpty()){
			PartyType partyTypeCustomer = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_CUSTOMER);
			PartyType partyTypeSupplier = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_SUPPLIER);
			PartyType partyTypeBoth = partyTypeService.getPartyTypeByName(ApplicationConstants.PARTY_TYPE_BOTH);
			
			List<PartyType> partyTypeList = new ArrayList<PartyType>();
			
			partyTypeList.add(partyTypeCustomer);
			partyTypeList.add(partyTypeSupplier);
			partyTypeList.add(partyTypeBoth);
			
			//List<Long> partyTypeIds = new ArrayList<Long>();
			
			partyTypeIds = partyTypeList.stream()
							.map(partyType -> partyType.getId())
							.collect(Collectors.toList());
			
		}
		else{
			
			partyTypeIds = transactioReportRequestDTO.getPartyTypeIds();
			
		}
		log.info("party type ids: "+partyTypeIds);
		
		log.info("sortColumn: "+sortColumn);
		if(sortColumn.equals("partyName"))
			sortColumn = "party.name";

		if(sortColumn.equals("invoiceNumber"))
			sortColumn = "inh.invoiceNumber";

		if(sortColumn.equals("invoiceDate"))
			sortColumn = "inh.invoiceDate";

		if(sortColumn.equals("payReferenceNumber"))
			sortColumn = "prh.payReferenceNumber";

		if(sortColumn.equals("payReferenceDate"))
			sortColumn = "prh.payReferenceDate";

		//TransactionType transactionType = transactionTypeService.getModelById(transactionTypeId);
		
		return getPagedPayableReceivableReport(transactionTypeId, inTypeId, transactioReportRequestDTO.getFinancialYearId(), transactioReportRequestDTO.getTransactionFromDate(), transactioReportRequestDTO.getTransactionToDate(), transactioReportRequestDTO.getPartyId(), partyTypeIds, pageNumber, pageSize, sortDir, sortColumn);
		
		
    }
	
//	public ByteArrayInputStream nestedExcel() throws IOException{
//
//
//
//
//		List<InvoiceDTO> report = new ArrayList<InvoiceDTO>();
//		InvoiceDTO inv1 = new InvoiceDTO();
//
//		inv1.setId("1");
//		inv1.setGrandTotal(20000.00);
//		inv1.setInvoiceNumber("INV-01");
//		inv1.setInvoiceDate(new Date());
//
//		List<InvoiceItemDTO> invoiceItems1 = new ArrayList<InvoiceItemDTO>();
//
//		InvoiceItemDTO invi1 = new InvoiceItemDTO();
//		invi1.setId("10");
//		invi1.setHeaderId("1");
//		invi1.setAmount(200.00);
//		invi1.setPartName("Item1");
//
//
//		invoiceItems1.add(invi1);
//		invi1 = new InvoiceItemDTO();
//		invi1.setId("11");
//		invi1.setHeaderId("1");
//		invi1.setAmount(300.00);
//		invi1.setPartName("Item2");
//
//		invoiceItems1.add(invi1);
//
//		inv1.setInvoiceItems(invoiceItems1);
//
//		report.add(inv1);
//
//
//		inv1 = new InvoiceDTO();
//
//		inv1.setId("2");
//		inv1.setGrandTotal(5000.00);
//		inv1.setInvoiceNumber("INV-02");
//		inv1.setInvoiceDate(new Date());
//
//		invoiceItems1 = new ArrayList<InvoiceItemDTO>();
//
//		invi1 = new InvoiceItemDTO();
//		invi1.setId("20");
//		invi1.setHeaderId("2");
//		invi1.setAmount(440.00);
//		invi1.setPartName("Itemffdfd");
//
//
//		invoiceItems1.add(invi1);
//		invi1 = new InvoiceItemDTO();
//		invi1.setId("21");
//		invi1.setHeaderId("2");
//		invi1.setAmount(344400.00);
//		invi1.setPartName("Item2sasdsd");
//
//		invoiceItems1.add(invi1);
//
//		invi1 = new InvoiceItemDTO();
//		invi1.setId("31");
//		invi1.setHeaderId("2");
//		invi1.setAmount(7600.00);
//		invi1.setPartName("Item2third");
//
//		invoiceItems1.add(invi1);
//
//
//		invi1 = new InvoiceItemDTO();
//		invi1.setId("41");
//		invi1.setHeaderId("2");
//		invi1.setAmount(5400.00);
//		invi1.setPartName("Item2fourth");
//
//		invoiceItems1.add(invi1);
//
//		inv1.setInvoiceItems(invoiceItems1);
//
//		report.add(inv1);
//
//
//		String[] COLUMNs = {"Id", "Invoice Amount", "Invoice Number", "Invoice Date", "Items"};
//		String[] ITEM_COLUMNs = {"Id", "Amount", "Material"};
//
////		report.forEach(r -> {
////			showFields(r);
////		});
//	    try(
//	        Workbook workbook = new XSSFWorkbook();
//	        ByteArrayOutputStream out = new ByteArrayOutputStream();
//	    ){
//
//	    	CreationHelper createHelper = workbook.getCreationHelper();
//
//	        Sheet sheet = workbook.createSheet("Customers");
//
//	        Font headerFont = workbook.createFont();
//	        headerFont.setBold(true);
//	        headerFont.setColor(IndexedColors.BLUE.getIndex());
//
//	        CellStyle headerCellStyle = workbook.createCellStyle();
//	        headerCellStyle.setFont(headerFont);
//	        headerCellStyle.setBorderBottom(BorderStyle.MEDIUM);
//	        headerCellStyle.setBorderTop(BorderStyle.MEDIUM);
//	        headerCellStyle.setBorderRight(BorderStyle.MEDIUM);
//	        headerCellStyle.setBorderLeft(BorderStyle.MEDIUM);
//
//	        // Row for Header
//
//			int dataStartRow = 0;
//			int dataStartCol = 0;
//	        Row headerRow = sheet.createRow(dataStartRow);
//
//	        // Header
//	        for (int col = dataStartCol; col < COLUMNs.length; col++) {
//	          Cell cell = headerRow.createCell(col);
//	          cell.setCellValue(COLUMNs[col]);
//	          cell.setCellStyle(headerCellStyle);
//	        }
//	        int itemStartCol = 4;
//	        int itemEndCol = 6;
//	        sheet.addMergedRegion(new CellRangeAddress(dataStartRow,dataStartRow,itemStartCol,itemEndCol));
//
//	        CellStyle cellStyle = workbook.createCellStyle();
//
//	        cellStyle.setBorderBottom(BorderStyle.MEDIUM);
//	        cellStyle.setBorderTop(BorderStyle.MEDIUM);
//	        cellStyle.setBorderRight(BorderStyle.MEDIUM);
//	        cellStyle.setBorderLeft(BorderStyle.MEDIUM);
//
//
//	        int rowIdx = 1;
//	        int startRow = rowIdx;
//	        for(InvoiceDTO prReport: report) {
//				int colIndex = dataStartCol;
//	        	Row row = sheet.createRow(rowIdx++);
//	        	Cell cell = row.createCell(colIndex++);
//	        	cell.setCellValue(prReport.getId());
//	        	cell.setCellStyle(cellStyle);
//	            cell = row.createCell(colIndex++);
//	            cell.setCellValue(prReport.getInvoiceNumber());
//	            cell.setCellStyle(cellStyle);
//	            cell = row.createCell(colIndex++);
//	            cell.setCellValue(prReport.getInvoiceDate());
//	            cell.setCellStyle(cellStyle);
//	            cell = row.createCell(colIndex++);
//	            cell.setCellValue(prReport.getGrandTotal());
//	            cell.setCellStyle(cellStyle);
//
//	            // Item Header
////	            Row rowItem = sheet.createRow(rowIdx++);
//		        for (int col = 0; col < ITEM_COLUMNs.length; col++) {
//		          cell = row.createCell(col+colIndex);
//		          cell.setCellValue(ITEM_COLUMNs[col]);
//		          cell.setCellStyle(headerCellStyle);
//		        }
//
//	            for(InvoiceItemDTO item: prReport.getInvoiceItems()) {
//					int itemColIndex = colIndex;
//	            	Row rowItem = sheet.createRow(rowIdx++);
//	            	cell = rowItem.createCell(itemColIndex++);
//	            	cell.setCellValue(item.getId());
//	            	cell.setCellStyle(cellStyle);
//	            	cell = rowItem.createCell(itemColIndex++);
//	            	cell.setCellValue(item.getAmount());
//	            	cell.setCellStyle(cellStyle);
//	            	cell = rowItem.createCell(itemColIndex++);
//	            	cell.setCellValue(item.getPartName());
//	            	cell.setCellStyle(cellStyle);
//	            }
//
//	            for(int i =dataStartCol; i < colIndex; i++){
//					sheet.addMergedRegion(new CellRangeAddress(startRow,(rowIdx - 1),i,i));
//				}
//
//		        startRow = rowIdx;
//	        }
//
//
//	        setBordersToMergedCells(sheet);
//	        workbook.write(out);
//	        return new ByteArrayInputStream(out.toByteArray());
//	    }
//	}


	public ByteArrayInputStream nestedExcel() throws IOException{




		List<InvoiceDTO> report = new ArrayList<InvoiceDTO>();
		InvoiceDTO inv1 = new InvoiceDTO();

		inv1.setId("1");
		inv1.setGrandTotal(20000.00);
		inv1.setInvoiceNumber("INV-01");
		inv1.setInvoiceDate(new Date());

		List<InvoiceItemDTO> invoiceItems1 = new ArrayList<InvoiceItemDTO>();

		InvoiceItemDTO invi1 = new InvoiceItemDTO();
		invi1.setId("10");
		invi1.setHeaderId("1");
		invi1.setAmount(200.00);
		invi1.setPartName("Item1");


		invoiceItems1.add(invi1);
		invi1 = new InvoiceItemDTO();
		invi1.setId("11");
		invi1.setHeaderId("1");
		invi1.setAmount(300.00);
		invi1.setPartName("Item2");

		invoiceItems1.add(invi1);

		inv1.setInvoiceItems(invoiceItems1);

		report.add(inv1);


		inv1 = new InvoiceDTO();

		inv1.setId("2");
		inv1.setGrandTotal(5000.00);
		inv1.setInvoiceNumber("INV-02");
		inv1.setInvoiceDate(new Date());

		invoiceItems1 = new ArrayList<InvoiceItemDTO>();

		invi1 = new InvoiceItemDTO();
		invi1.setId("20");
		invi1.setHeaderId("2");
		invi1.setAmount(440.00);
		invi1.setPartName("Itemffdfd");


		invoiceItems1.add(invi1);
		invi1 = new InvoiceItemDTO();
		invi1.setId("21");
		invi1.setHeaderId("2");
		invi1.setAmount(344400.00);
		invi1.setPartName("Item2sasdsd");

		invoiceItems1.add(invi1);

		invi1 = new InvoiceItemDTO();
		invi1.setId("31");
		invi1.setHeaderId("2");
		invi1.setAmount(7600.00);
		invi1.setPartName("Item2third");

		invoiceItems1.add(invi1);


		invi1 = new InvoiceItemDTO();
		invi1.setId("41");
		invi1.setHeaderId("2");
		invi1.setAmount(5400.00);
		invi1.setPartName("Item2fourth");

		invoiceItems1.add(invi1);

		inv1.setInvoiceItems(invoiceItems1);

		report.add(inv1);


		String[] COLUMNs = {"Id", "Invoice Amount", "Invoice Number", "Invoice Date", "Items"};
		String[] ITEM_COLUMNs = {"Id", "Amount", "Material"};

//		report.forEach(r -> {
//			showFields(r);
//		});
		try(
				Workbook workbook = new XSSFWorkbook();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
		){

			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("Customers");

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLUE.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setBorderBottom(BorderStyle.MEDIUM);
			headerCellStyle.setBorderTop(BorderStyle.MEDIUM);
			headerCellStyle.setBorderRight(BorderStyle.MEDIUM);
			headerCellStyle.setBorderLeft(BorderStyle.MEDIUM);

			// Row for Header

			int dataStartRow = 0;
			int dataStartCol = 0;
			Row headerRow = sheet.createRow(dataStartRow);

			// Header
			for (int col = dataStartCol; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}
			int itemStartCol = 4;
			int itemEndCol = 6;
			sheet.addMergedRegion(new CellRangeAddress(dataStartRow,dataStartRow,itemStartCol,itemEndCol));

			CellStyle cellStyle = workbook.createCellStyle();

			cellStyle.setBorderBottom(BorderStyle.MEDIUM);
			cellStyle.setBorderTop(BorderStyle.MEDIUM);
			cellStyle.setBorderRight(BorderStyle.MEDIUM);
			cellStyle.setBorderLeft(BorderStyle.MEDIUM);


			int rowIdx = 1;
			int startRow = rowIdx;
			writeFields(report, dataStartCol, sheet, rowIdx);

			setBordersToMergedCells(sheet);
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		}
	}
	
	private void setBordersToMergedCells(Sheet sheet) {
		  List<CellRangeAddress> mergedRegions = sheet.getMergedRegions();
		  for (CellRangeAddress rangeAddress : mergedRegions) {
		    RegionUtil.setBorderTop(BorderStyle.MEDIUM, rangeAddress, sheet);
		    RegionUtil.setBorderLeft(BorderStyle.MEDIUM, rangeAddress, sheet);
		    RegionUtil.setBorderRight(BorderStyle.MEDIUM, rangeAddress, sheet);
		    RegionUtil.setBorderBottom(BorderStyle.MEDIUM, rangeAddress, sheet);
		  }
		}

	private void showFields(Object o) {


		PropertyDescriptor[] objDescriptors = PropertyUtils.getPropertyDescriptors(o);
		for (PropertyDescriptor objDescriptor : objDescriptors) {
			try {
				String propertyName = objDescriptor.getName();
				Object propType = PropertyUtils.getPropertyType(o, propertyName);
				Object propValue = PropertyUtils.getProperty(o, propertyName);

				// Printing the details
				if(propValue != null)
				System.out.println("Property="+propertyName+", Type="+propType+", Value="+propValue);
				if(propType.toString().equals("interface java.util.List")){
					Collection propList = (Collection) propValue;
					propList.forEach(item -> {
						PropertyDescriptor[] itemDescriptors = PropertyUtils.getPropertyDescriptors(item);

						for(PropertyDescriptor itemDescriptor : itemDescriptors){
							String itemName = itemDescriptor.getName();
							try{
								Object itemType = PropertyUtils.getPropertyType(item, itemName);
								Object itemValue = PropertyUtils.getProperty(item, itemName);
								// Printing the details
								if(itemValue != null)
									System.out.println("Item Property="+itemName+", Item Type="+itemType+", Item Value="+itemValue);
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
					});
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}


	private void writeCell(Object o
			, Row row
			, int colIndex
			, Sheet sheet
			, int rowIdx) {


		PropertyDescriptor[] objDescriptors = PropertyUtils.getPropertyDescriptors(o);
		for (PropertyDescriptor objDescriptor : objDescriptors) {
			try {
				String propertyName = objDescriptor.getName();
				Object propType = PropertyUtils.getPropertyType(o, propertyName);
				Object propValue = PropertyUtils.getProperty(o, propertyName);

				// Wrrite to cell
				if(propValue != null){
					Cell cell = row.createCell(colIndex++);
					cell.setCellValue(propValue.toString());

					if(propType.toString().equals("interface java.util.List")){
						Collection propList = (Collection) propValue;
						int finalColIndex = colIndex;
						//propList.forEach(item -> {
						for(Object item: propList){
							PropertyDescriptor[] itemDescriptors = PropertyUtils.getPropertyDescriptors(item);

							for(PropertyDescriptor itemDescriptor : itemDescriptors){
								String itemName = itemDescriptor.getName();
								try{
									Object itemType = PropertyUtils.getPropertyType(item, itemName);
									Object itemValue = PropertyUtils.getProperty(item, itemName);
									// Printing the details
									if(itemValue != null)
									{

										int itemColIndex = finalColIndex;
										Row rowItem = sheet.createRow(rowIdx++);
										cell = rowItem.createCell(itemColIndex++);
										cell.setCellValue(itemValue.toString());

									}
								} catch (Exception e) {
									e.printStackTrace();
								}

							}
						};
					}

				}


			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private void writeFields(List<?> report
			, int dataStartCol
			, Sheet sheet
			, int rowIdx)
	{
		int startRow = rowIdx;
		for(Object prReport: report) {
			int colIndex = dataStartCol;
			Row row = sheet.createRow(rowIdx++);

			writeCell(prReport, row, colIndex, sheet, rowIdx);
			for(int i =dataStartCol; i < colIndex; i++){
				sheet.addMergedRegion(new CellRangeAddress(startRow,(rowIdx - 1),i,i));
			}

			startRow = rowIdx;
		}
	}
    
}
