package com.coreerp.reports.serviceimpl;

import java.util.Date;
import java.util.List;

import com.coreerp.*;
import org.apache.logging.log4j.Logger;import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.controller.InvoiceController;
import com.coreerp.dto.InvoiceDTO;
import com.coreerp.dto.InvoiceReportDTO;
import com.coreerp.dto.PayableReceivableReportDTO;
import com.coreerp.reports.dao.InvoiceReportRepository;
import com.coreerp.reports.model.BalanceReport;
import com.coreerp.reports.model.InvoiceReport;
import com.coreerp.reports.service.InvoiceReportPageableService;

@Service
public class InvoiceReportPageableServiceImpl implements InvoiceReportPageableService {

	final static Logger log = LogManager.getLogger(InvoiceReportPageableServiceImpl.class);
	
	@Autowired
	InvoiceReportRepository invoiceReportRepository;
	
	@Override
	public InvoiceReportDTO getPagedInvoiceReport(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId,
			List<Long> partyTypeIds, String createdBy, Integer pageNumber, Integer pageSize, String sortDirection, String sortColumn) {
		
		PageRequest pageRequest = null;
		if(pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0){
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		
		if(pageSize == -99){
			pageRequest =
	                new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		
		Page<InvoiceReport> invReport = invoiceReportRepository.getReport(financialYearId, fromDate, toDate, partyId, partyTypeIds, transactionTypeId, createdBy, pageRequest);
		//invReport.getTotalElements()
		
		log.info("total count: "+invReport.getTotalElements());
		log.info("report: "+invReport.getContent());
		
		InvoiceReportDTO retReport = new InvoiceReportDTO();
		retReport.setInvoiceReports(invReport.getContent());
		retReport.setTotalCount(invReport.getTotalElements());
		

		
		return retReport; //invoiceReportRepository.getReport(financialYearId, fromDate, toDate, partyId, partyTypeIds, transactionTypeId, pageRequest).getContent();
	
	}

//	@Override
//	public Long getInvoiceReportCount(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId,
//			List<Long> partyTypeIds) {
//		
//		return (long) invoiceReportRepository.getReport(financialYearId, fromDate, toDate, partyId, partyTypeIds, transactionTypeId, null).getContent().size();
//	}

	@Override
	public PayableReceivableReportDTO getPagedBalanceReport(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId,
			List<Long> partyTypeIds, String createdBy,Long statusId, Integer pageNumber, Integer pageSize, String sortDirection, String sortColumn) {
		PageRequest pageRequest = null;
		if(pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0){
			pageRequest =
					new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		if(pageSize == -99){
			pageRequest =
					new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		PayableReceivableReportDTO payableReceivableReportDTO = new PayableReceivableReportDTO();
		String Cancelled= ApplicationConstants.INVOICE_STATUS_CANCELLED;
		Page<BalanceReport> balanceReport =  invoiceReportRepository.getBalanceReport(financialYearId, fromDate, toDate, partyId, partyTypeIds,transactionTypeId,statusId, createdBy,Cancelled,pageRequest);
		
		payableReceivableReportDTO.setBalanceReportModels(balanceReport.getContent());
		payableReceivableReportDTO.setTotalCount(balanceReport.getTotalElements());
		
		return payableReceivableReportDTO;
	
	}

//	@Override
//	public Long getBalanceReportCount(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId,
//			List<Long> partyTypeIds) {
//		return (long) invoiceReportRepository.getBalanceReport(financialYearId, fromDate, toDate, partyId, partyTypeIds, transactionTypeId, null).getContent().size();
//	}

}
