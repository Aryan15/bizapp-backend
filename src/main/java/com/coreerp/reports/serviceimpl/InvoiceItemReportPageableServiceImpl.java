package com.coreerp.reports.serviceimpl;

import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.coreerp.controller.InvoiceController;
import com.coreerp.model.FinancialYear;
import com.coreerp.reports.dao.InvoiceItemReportPageableRepository;
import com.coreerp.reports.model.InvoiceItemReportModel;
import com.coreerp.reports.service.InvoiceItemReportPageableService;
import com.coreerp.service.FinancialYearService;

@Service
public class InvoiceItemReportPageableServiceImpl implements InvoiceItemReportPageableService {

	@Autowired
	InvoiceItemReportPageableRepository invoiceItemReportPageableRepository;
	
	@Autowired
	FinancialYearService financialYearService;
	
	@Override
	public Page<InvoiceItemReportModel> getPagedInvoiceItemReport(Long invoiceTypeId, Long financialYearId, Date fromDate, Date toDate,

			Long partyId, List<Long> partyTypeIds, Long materialId, Integer isHeaderLevel, String createdBy, Integer pageNumber, Integer pageSize, String sortDirection, String sortColumn) {
		

		PageRequest pageRequest = null;
		if(pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0){
			pageRequest =
                new PageRequest(pageNumber, pageSize, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		
		if(pageSize == -99){
			pageRequest =
	                new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortColumn != null ? sortColumn : "invoiceDate");
		}
		//FinancialYear financialYear = financialYearService.getById(financialYearId);

		return invoiceItemReportPageableRepository.getReport(financialYearId, fromDate, toDate, partyId, partyTypeIds, invoiceTypeId, materialId, isHeaderLevel, createdBy, pageRequest);
}



	
}
