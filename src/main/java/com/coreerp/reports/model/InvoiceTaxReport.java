package com.coreerp.reports.model;

import java.util.Date;

public class InvoiceTaxReport {
	 
	 private String id;
	 private String partyName;
	 private String gstNumber;	 
	 private String invoiceNumber;
	 private Date invoiceDate; 
	 private Double netAmount;
	 private Double cgstTaxPercent;
	 private Double cgstTaxAmount;
	 private Double sgstTaxPercent;
	 private Double sgstTaxAmount;
	 private Double igstTaxPercent;
	 private Double igstTaxAmount;
	 private Double grandTotal;
	 private String statusName;
	 private String partyCode;
	 
	public InvoiceTaxReport(String id, String partyName, String gstNumber, String invoiceNumber, Date invoiceDate,
			Double netAmount, Double cgstTaxPercent, Double cgstTaxAmount, Double sgstTaxPercent, Double sgstTaxAmount,
			Double igstTaxPercent, Double igstTaxAmount, Double grandTotal, String statusName,String partyCode) {
		super();
		this.id = id;
		this.partyName = partyCode+"-"+partyName;
		this.gstNumber = gstNumber;
		this.invoiceNumber = invoiceNumber;
		this.invoiceDate = invoiceDate;
		this.netAmount = netAmount;
		this.cgstTaxPercent = cgstTaxPercent;
		this.cgstTaxAmount = cgstTaxAmount;
		this.sgstTaxPercent = sgstTaxPercent;
		this.sgstTaxAmount = sgstTaxAmount;
		this.igstTaxPercent = igstTaxPercent;
		this.igstTaxAmount = igstTaxAmount;
		this.grandTotal = grandTotal;
		this.statusName = statusName;
		this.partyCode =partyCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getGstNumber() {
		return gstNumber;
	}
	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Double getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(Double netAmount) {
		this.netAmount = netAmount;
	}
	public Double getCgstTaxPercent() {
		return cgstTaxPercent;
	}
	public void setCgstTaxPercent(Double cgstTaxPercent) {
		this.cgstTaxPercent = cgstTaxPercent;
	}
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}
	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}
	public Double getSgstTaxPercent() {
		return sgstTaxPercent;
	}
	public void setSgstTaxPercent(Double sgstTaxPercent) {
		this.sgstTaxPercent = sgstTaxPercent;
	}
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}
	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}
	public Double getIgstTaxPercent() {
		return igstTaxPercent;
	}
	public void setIgstTaxPercent(Double igstTaxPercent) {
		this.igstTaxPercent = igstTaxPercent;
	}
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}
	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}
}
