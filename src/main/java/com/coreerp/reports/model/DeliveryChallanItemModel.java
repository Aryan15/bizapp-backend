package com.coreerp.reports.model;

import java.util.Date;

public class DeliveryChallanItemModel {

	private String id;
	private String deliveryChallanNumber;
	private Date deliveryChallanDate;
	private String partyName;
	private String materialName;
	private String remarks;
	private Double quantity;
	private String uom;
	private String hsnOrSac;
	 private String statusName;
	 private  String partyCode;
	 private String partNumber;
	
	
	
	public DeliveryChallanItemModel(String id, String deliveryChallanNumber, Date deliveryChallanDate, String partyName,
			String materialName, String remarks, Double quantity,String uom, String hsnOrSac , String statusName,String partyCode,String partNumber) {
		super();
		this.id = id;
		this.deliveryChallanNumber = deliveryChallanNumber;
		this.deliveryChallanDate = deliveryChallanDate;
		this.partyName =partyCode+"-"+partyName;
		if(partNumber!=null){
			this.materialName = materialName+"/"+partNumber;
		}
		else{
			this.materialName = materialName;
		}
		this.remarks = remarks;
		this.quantity = quantity;
		this.uom = uom;
		this.hsnOrSac = hsnOrSac;
		 this.statusName = statusName;
		 this.partyCode = partyCode;
		 this.partNumber =partNumber;

	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDeliveryChallanNumber() {
		return deliveryChallanNumber;
	}
	public void setDeliveryChallanNumber(String deliveryChallanNumber) {
		this.deliveryChallanNumber = deliveryChallanNumber;
	}
	public Date getDeliveryChallanDate() {
		return deliveryChallanDate;
	}
	public void setDeliveryChallanDate(Date deliveryChallanDate) {
		this.deliveryChallanDate = deliveryChallanDate;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public String getHsnOrSac() {
		return hsnOrSac;
	}
	public void setHsnOrSac(String hsnOrSac) {
		this.hsnOrSac = hsnOrSac;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}


	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
}
