package com.coreerp.reports.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;


//@Entity
//@NamedNativeQuery(
//		  name = "qryInvoiceReport"
//		, query = " SELECT inh.id "
//				+ ",inh.inv_number "
//				+ ",inh.inv_date "
//				+ ",party.name AS party_name "
//				+ ",inh.total_taxable_amount "
//				+ ",inh.cgst_tax_rate "
//				+ ",inh.cgst_tax_amount "
//				+ ",inh.sgst_tax_rate "
//				+ ",inh.sgst_tax_amount "
//				+ ",inh.igst_tax_rate "
//				+ ",inh.igst_tax_amount "
//				+ ",inh.advance_amount "
//				+ ",inh.grand_total "
//				+ " FROM tr_invoice_header inh "
//				+ " INNER JOIN ma_party party "
//				+ " ON (party.id = inh.party_id) "
//				+ " WHERE 1 = 1 "
//				+ " AND inh.financial_year_id = IFNULL(:fin_year_id, inh.financial_year_id) "
//				+ " AND inh.inv_date BETWEEN IFNULL(:from_date, inh.inv_date) AND IFNULL(:to_date, inh.inv_date) "
//				+ " AND inh.party_id = IFNULL(:party_id, inh.party_id) "
//				+ " AND party.party_type_id IN (:party_type_ids) "
//				+ " ORDER BY inv_date "
//				+ " ,inv_number "
//				+ " ,party_name "
//		, resultClass = InvoiceReport.class)
public class InvoiceReport {

	 
	 private String id;
	
	 
	 private String invoiceNumber;

	 
	 private Date invoiceDate; 
	 
	 
	 private String sourceInvoiceNumber;
	 
	 
	 private Date sourceInvoiceDate;
	 
	 
	 private String partyName;
	 
	 
	 private Double totalTaxableAmount;
	 
	 
	 private Double cgstTaxRate;
	 
	 
	 private Double cgstTaxAmount;
	 
	  
	 private Double sgstTaxRate;
	
	 
	 private Double sgstTaxAmount;
	 
	 
	 private Double igstTaxRate;
	 
	 private Double discountAmount;
	 
	 
	 private Double igstTaxAmount;
	 
	 
	 private Double advanceAmount;
	 
	 
	 private Double grandTotal;

	 private String statusName;
	 
	 private Double amount;
	 
	 private Integer inclusiveTax;

	 private String partyCode;

	 private Double tcsPercentage;

	 private Double tcsAmount;

	 private String gstNumber;

	public InvoiceReport(String id, String invoiceNumber,  Date invoiceDate, String sourceInvoiceNumber, Date sourceInvoiceDate, String partyName, Double totalTaxableAmount,
			Double cgstTaxRate, Double cgstTaxAmount, Double sgstTaxRate, Double sgstTaxAmount, Double igstTaxRate,
			Double igstTaxAmount, Double discountAmount, Double advanceAmount, Double grandTotal, String statusName, Double amount, Integer inclusiveTax,String partyCode,Double tcsPercentage,Double tcsAmount,String gstNumber) {
		super();
		this.id = id;
		this.invoiceNumber = invoiceNumber;
		this.invoiceDate = invoiceDate;
		this.partyName = partyCode+"-"+partyName;
		this.totalTaxableAmount = totalTaxableAmount;
		this.cgstTaxRate = cgstTaxRate;
		this.cgstTaxAmount = cgstTaxAmount;
		this.sgstTaxRate = sgstTaxRate;
		this.sgstTaxAmount = sgstTaxAmount;
		this.igstTaxRate = igstTaxRate;
		this.igstTaxAmount = igstTaxAmount;
		this.discountAmount = discountAmount;
		this.advanceAmount = advanceAmount;
		this.grandTotal = grandTotal;
		this.statusName = statusName;
		this.sourceInvoiceNumber = sourceInvoiceNumber;
		this.sourceInvoiceDate = sourceInvoiceDate;
		this.amount = amount;
		this.inclusiveTax = inclusiveTax;
		this.partyCode = partyCode;
		this.tcsPercentage = tcsPercentage;
		this.tcsAmount = tcsAmount;
		this.gstNumber =gstNumber;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public Double getTotalTaxableAmount() {
		return totalTaxableAmount;
	}

	public void setTotalTaxableAmount(Double totalTaxableAmount) {
		this.totalTaxableAmount = totalTaxableAmount;
	}

	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}

	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}

	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}

	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}

	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}

	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}

	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}

	public Double getIgstTaxRate() {
		return igstTaxRate;
	}

	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}

	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}

	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	public Double getAdvanceAmount() {
		return advanceAmount;
	}

	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getSourceInvoiceNumber() {
		return sourceInvoiceNumber;
	}

	public void setSourceInvoiceNumber(String sourceInvoiceNumber) {
		this.sourceInvoiceNumber = sourceInvoiceNumber;
	}

	public Date getSourceInvoiceDate() {
		return sourceInvoiceDate;
	}

	public void setSourceInvoiceDate(Date sourceInvoiceDate) {
		this.sourceInvoiceDate = sourceInvoiceDate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Integer getInclusiveTax() {
		return inclusiveTax;
	}

	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public Double getTcsPercentage() {
		return tcsPercentage;
	}

	public void setTcsPercentage(Double tcsPercentage) {
		this.tcsPercentage = tcsPercentage;
	}

	public Double getTcsAmount() {
		return tcsAmount;
	}

	public void setTcsAmount(Double tcsAmount) {
		this.tcsAmount = tcsAmount;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}
}
