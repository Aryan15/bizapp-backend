package com.coreerp.reports.model;

import java.util.Date;

public class PurchaseOrderReport {

	private String id;
	private String purchaseOrderNumber;
	private Date purchaseOrderDate;
	private String customerName;
	private String materialName;
	private String partNumber;
	private Double quantity;
	private String unitOfMeasurementName;
	private Double price;
	private Double amount;
	private Double totalAmount;
	private Double discountAmount;
	private Double cgstTaxPercentage;
	private Double cgstTaxAmount;
	private Double sgstTaxPercentage;
	private Double sgstTaxAmount;
	private Double igstTaxPercentage;
	private Double igstTaxAmount;
	private String statusName;
	private String internalReferenceNumber;
	private String remarks;
	private String partyCode;


	
	
	
	public PurchaseOrderReport(String id, String purchaseOrderNumber, Date purchaseOrderDate, String customerName,
			String materialName,String partNumber, Double quantity, String unitOfMeasurementName, Double price, Double amount,
			Double totalAmount, Double discountAmount, Double cgstTaxPercentage, Double cgstTaxAmount,
			Double sgstTaxPercentage, Double sgstTaxAmount, Double igstTaxPercentage, Double igstTaxAmount, String statusName,String internalReferenceNumber,String remarks,String partyCode) {
		super();
		this.id = id;
		this.purchaseOrderNumber = purchaseOrderNumber;
		this.purchaseOrderDate = purchaseOrderDate;
		this.customerName = partyCode+"-"+customerName;
		this.remarks = remarks;
		this.internalReferenceNumber = internalReferenceNumber;
		if(partNumber!=null){
			this.materialName = materialName+"/"+partNumber;
		}
		else{
			this.materialName = materialName;
		}
		this.partNumber =partNumber;
		this.quantity = quantity;
		this.unitOfMeasurementName = unitOfMeasurementName;
		this.price = price;
		this.amount = amount;
		this.totalAmount = totalAmount;
		this.discountAmount = discountAmount;
		this.cgstTaxPercentage = cgstTaxPercentage;
		this.cgstTaxAmount = cgstTaxAmount;
		this.sgstTaxPercentage = sgstTaxPercentage;
		this.sgstTaxAmount = sgstTaxAmount;
		this.igstTaxPercentage = igstTaxPercentage;
		this.igstTaxAmount = igstTaxAmount;
		this.statusName = statusName;
		this.internalReferenceNumber = internalReferenceNumber;
		this.remarks = remarks;
		this.partyCode = partyCode;


	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}
	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}
	public Date getPurchaseOrderDate() {
		return purchaseOrderDate;
	}
	public void setPurchaseOrderDate(Date purchaseOrderDate) {
		this.purchaseOrderDate = purchaseOrderDate;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public String getUnitOfMeasurementName() {
		return unitOfMeasurementName;
	}
	public void setUnitOfMeasurementName(String unitOfMeasurementName) {
		this.unitOfMeasurementName = unitOfMeasurementName;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Double getCgstTaxPercentage() {
		return cgstTaxPercentage;
	}
	public void setCgstTaxPercentage(Double cgstTaxPercentage) {
		this.cgstTaxPercentage = cgstTaxPercentage;
	}
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}
	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}
	public Double getSgstTaxPercentage() {
		return sgstTaxPercentage;
	}
	public void setSgstTaxPercentage(Double sgstTaxPercentage) {
		this.sgstTaxPercentage = sgstTaxPercentage;
	}
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}
	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}
	public Double getIgstTaxPercentage() {
		return igstTaxPercentage;
	}
	public void setIgstTaxPercentage(Double igstTaxPercentage) {
		this.igstTaxPercentage = igstTaxPercentage;
	}
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}
	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getInternalReferenceNumber() {
		return internalReferenceNumber;
	}

	public void setInternalReferenceNumber(String internalReferenceNumber) {
		this.internalReferenceNumber = internalReferenceNumber;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
}
