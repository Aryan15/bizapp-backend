package com.coreerp.reports.model;

import java.util.Date;

public class PayableReceivableReport {
	private String id;
	private String invoiceNumber;
	private Date invoiceDate;
	private Double invoiceAmount;
	private Double tdsPercentage;
	private Double tdsAmount;
	private Double amountAfterTds;
	private Double paidAmount;
	private Double payingAmount;
	private String bankName;
	private String paymentMethodName;
	private Double dueAmount;
	private String partyName;
	private String paymentDocumentNumber;
	private Date payReferenceDate;
	private String payReferenceNumber;
	private String statusName;
	private String partyCode;
	 
	public PayableReceivableReport(String id, String invoiceNumber, Date invoiceDate, Double invoiceAmount,
			Double tdsPercentage, Double tdsAmount, Double amountAfterTds, Double paidAmount, Double payingAmount,
			String bankName, String paymentMethodName, Double dueAmount, String partyName,
			String paymentDocumentNumber, Date payReferenceDate, String payReferenceNumber, String statusName,String partyCode) {
		super();
		this.id = id;
		this.invoiceNumber = invoiceNumber;
		this.invoiceDate = invoiceDate;
		this.invoiceAmount = invoiceAmount;
		this.tdsPercentage = tdsPercentage;
		this.tdsAmount = tdsAmount;
		this.amountAfterTds = amountAfterTds;
		this.paidAmount = paidAmount;
		this.payingAmount = payingAmount;
		this.bankName = bankName;
		this.paymentMethodName = paymentMethodName;
		this.dueAmount = dueAmount;
		this.partyName = partyCode+"-"+partyName;
		if(paymentDocumentNumber!=null){
			this.paymentDocumentNumber = paymentDocumentNumber;
		}

		if(bankName!=null){
			this.paymentDocumentNumber = this.paymentDocumentNumber+"/"+bankName;
		}
		this.payReferenceDate = payReferenceDate;
		this.payReferenceNumber = payReferenceNumber;
		this.statusName = statusName;
		this.partyCode =partyCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public Double getTdsPercentage() {
		return tdsPercentage;
	}
	public void setTdsPercentage(Double tdsPercentage) {
		this.tdsPercentage = tdsPercentage;
	}
	public Double getTdsAmount() {
		return tdsAmount;
	}
	public void setTdsAmount(Double tdsAmount) {
		this.tdsAmount = tdsAmount;
	}
	public Double getAmountAfterTds() {
		return amountAfterTds;
	}
	public void setAmountAfterTds(Double amountAfterTds) {
		this.amountAfterTds = amountAfterTds;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Double getPayingAmount() {
		return payingAmount;
	}
	public void setPayingAmount(Double payingAmount) {
		this.payingAmount = payingAmount;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getPaymentMethodName() {
		return paymentMethodName;
	}
	public void setPaymentMethodName(String paymentMethodName) {
		this.paymentMethodName = paymentMethodName;
	}
	public Double getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getPaymentDocumentNumber() {
		return paymentDocumentNumber;
	}
	public void setPaymentDocumentNumber(String paymentDocumentNumber) {
		this.paymentDocumentNumber = paymentDocumentNumber;
	}
	public Date getPayReferenceDate() {
		return payReferenceDate;
	}
	public void setPayReferenceDate(Date payReferenceDate) {
		this.payReferenceDate = payReferenceDate;
	}
	public String getPayReferenceNumber() {
		return payReferenceNumber;
	}
	public void setPayReferenceNumber(String payReferenceNumber) {
		this.payReferenceNumber = payReferenceNumber;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}
}
