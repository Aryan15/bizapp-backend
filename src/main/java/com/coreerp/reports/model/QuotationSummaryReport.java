package com.coreerp.reports.model;

import java.util.Date;

public class QuotationSummaryReport {

    private String id;


    private String quotationNumber;


    private Date quotationDate;





    private String partyName;


  private Double totalTaxableAmount;


    private Double cgstTaxRate;


    private Double cgstTaxAmount;


    private Double sgstTaxRate;


    private Double sgstTaxAmount;


    private Double igstTaxRate;

    private Double discountAmount;


    private Double igstTaxAmount;

    private Double grandTotal;

    private String statusName;

    private Double amount;

    private Integer inclusiveTax;

    private String partyCode;


    public QuotationSummaryReport(String id, String quotationNumber,  Date quotationDate,String partyName,
                         Double cgstTaxRate, Double cgstTaxAmount, Double sgstTaxRate, Double sgstTaxAmount, Double igstTaxRate,
                         Double igstTaxAmount, Double discountAmount,Double grandTotal, String statusName, Double amount,Double totalTaxableAmount,Integer inclusiveTax,String partyCode) {
        super();
        this.id = id;
        this.quotationNumber = quotationNumber;
        this.quotationDate = quotationDate;
        this.partyName = partyCode+"-"+partyName;

        this.cgstTaxRate = cgstTaxRate;
        this.cgstTaxAmount = cgstTaxAmount;
        this.sgstTaxRate = sgstTaxRate;
        this.sgstTaxAmount = sgstTaxAmount;
        this.igstTaxRate = igstTaxRate;
        this.igstTaxAmount = igstTaxAmount;
        this.discountAmount = discountAmount;
        this.grandTotal = grandTotal;
        this.statusName = statusName;
        this.amount = amount;
        this.totalTaxableAmount = totalTaxableAmount;

        this.inclusiveTax = inclusiveTax;
        this.partyCode =partyCode;

    }







    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuotationNumber() {
        return quotationNumber;
    }

    public void setQuotationNumber(String quotationNumber) {
        this.quotationNumber = quotationNumber;
    }

    public Date getQuotationDate() {
        return quotationDate;
    }

    public void setQuotationDate(Date quotationDate) {
        this.quotationDate = quotationDate;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

   public Double getTotalTaxableAmount() {
        return totalTaxableAmount;
    }

    public void setTotalTaxableAmount(Double totalTaxableAmount) {
        this.totalTaxableAmount = totalTaxableAmount;
    }

    public Double getCgstTaxRate() {
        return cgstTaxRate;
    }

    public void setCgstTaxRate(Double cgstTaxRate) {
        this.cgstTaxRate = cgstTaxRate;
    }

    public Double getCgstTaxAmount() {
        return cgstTaxAmount;
    }

    public void setCgstTaxAmount(Double cgstTaxAmount) {
        this.cgstTaxAmount = cgstTaxAmount;
    }

    public Double getSgstTaxRate() {
        return sgstTaxRate;
    }

    public void setSgstTaxRate(Double sgstTaxRate) {
        this.sgstTaxRate = sgstTaxRate;
    }

    public Double getSgstTaxAmount() {
        return sgstTaxAmount;
    }

    public void setSgstTaxAmount(Double sgstTaxAmount) {
        this.sgstTaxAmount = sgstTaxAmount;
    }

    public Double getIgstTaxRate() {
        return igstTaxRate;
    }

    public void setIgstTaxRate(Double igstTaxRate) {
        this.igstTaxRate = igstTaxRate;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Double getIgstTaxAmount() {
        return igstTaxAmount;
    }

    public void setIgstTaxAmount(Double igstTaxAmount) {
        this.igstTaxAmount = igstTaxAmount;
    }


    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }


    public Integer getInclusiveTax() {
        return inclusiveTax;
    }

    public void setInclusiveTax(Integer inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
    }


    public String getPartyCode() {
        return partyCode;
    }

    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }
}
