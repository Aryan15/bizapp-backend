package com.coreerp.reports.model;

public class DashboardTransactionSummary {

	private Integer monthNumber;
	private String month;
	private String transactionName;
	private Double transactionAmount;
	private Long transactionCount;
	private Double dueAmount;
	private Integer financialYear;
	public DashboardTransactionSummary(Integer monthNumber, String month, String transactionName, Double transactionAmount, Long transactionCount,
			Double dueAmount,Integer financialYear) {
		super();
		this.monthNumber = monthNumber;
		this.month = month;
		this.transactionName = transactionName;
		this.transactionAmount = transactionAmount;
		this.transactionCount = transactionCount;
		this.dueAmount = dueAmount;
		this.financialYear = financialYear;
	}
	public String getTransactionName() {
		return transactionName;
	}
	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}
	public Double getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public Long getTransactionCount() {
		return transactionCount;
	}
	public void setTransactionCount(Long transactionCount) {
		this.transactionCount = transactionCount;
	}
	public Double getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public Integer getMonthNumber() {
		return monthNumber;
	}
	public void setMonthNumber(Integer monthNumber) {
		this.monthNumber = monthNumber;
	}

	public Integer getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(Integer financialYear) {
		this.financialYear = financialYear;
	}
}
