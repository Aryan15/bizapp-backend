package com.coreerp.reports.model;

import java.util.Date;

public class EwayBillRecentReport {

    private Long id;
    private String invoiceHeaderId;
    private String invoiceNumber;
    private String eWayBillNo;
    private Date eWayBillDate;
    private String partyName;

    public EwayBillRecentReport(Long id, String invoiceHeaderId, String invoiceNumber, String eWayBillNo, Date eWayBillDate, String partyName) {
        super();
        this.id = id;
        this.invoiceHeaderId = invoiceHeaderId;
        this.invoiceNumber = invoiceNumber;
        this.eWayBillNo = eWayBillNo;
        this.eWayBillDate = eWayBillDate;
        this.partyName = partyName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvoiceHeaderId() {
        return invoiceHeaderId;
    }

    public void setInvoiceHeaderId(String invoiceHeaderId) {
        this.invoiceHeaderId = invoiceHeaderId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String geteWayBillNo() {
        return eWayBillNo;
    }

    public void seteWayBillNo(String eWayBillNo) {
        this.eWayBillNo = eWayBillNo;
    }

    public Date geteWayBillDate() {
        return eWayBillDate;
    }

    public void seteWayBillDate(Date eWayBillDate) {
        this.eWayBillDate = eWayBillDate;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }
}
