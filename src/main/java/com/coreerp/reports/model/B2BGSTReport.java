package com.coreerp.reports.model;

import java.util.Date;

public class B2BGSTReport {

	
//	java.lang.String, 
//	java.lang.String, 
//	java.lang.String, 
//	java.util.Date, 
//	double, 
//	java.lang.String, 
//	long, --  Long 
//	double, 
//	java.lang.String, 
//	java.lang.String, 
//	int,  -- Integer
//	double, double, double, double, double, double
	
	private String id;
	private String gstin;
	private String invoiceNumber;
	private Date invoiceDate;
	private Double invoiceValue;
	private String stateCode;
	private Long reverseCharge;
	private Double applicablePercentOfTaxRate;
	private String invoiceType;
	private String eCommerceGstn;
	private Integer serialNumber;
	private Double rate;
	private Double taxableValue;
	private Double integratedTax;
	private Double centralTax;
	private Double stateTax;
	private Double cess;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the gstin
	 */
	public String getGstin() {
		return gstin;
	}

	/**
	 * @param gstin the gstin to set
	 */
	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	/**
	 * @param invoiceNumber the invoiceNumber to set
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * @return the invoiceDate
	 */
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	/**
	 * @param invoiceDate the invoiceDate to set
	 */
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	/**
	 * @return the invoiceValue
	 */
	public Double getInvoiceValue() {
		return invoiceValue;
	}

	/**
	 * @param invoiceValue the invoiceValue to set
	 */
	public void setInvoiceValue(Double invoiceValue) {
		this.invoiceValue = invoiceValue;
	}

	/**
	 * @return the stateCode
	 */
	public String getStateCode() {
		return stateCode;
	}

	/**
	 * @param stateCode the stateCode to set
	 */
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	/**
	 * @return the reverseCharge
	 */



	/**
	 * @return the invoiceType
	 */
	public String getInvoiceType() {
		return invoiceType;
	}

	/**
	 * @param invoiceType the invoiceType to set
	 */
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	/**
	 * @return the eCommerceGstn
	 */
	public String geteCommerceGstn() {
		return eCommerceGstn;
	}

	/**
	 * @param eCommerceGstn the eCommerceGstn to set
	 */
	public void seteCommerceGstn(String eCommerceGstn) {
		this.eCommerceGstn = eCommerceGstn;
	}

	

	/**
	 * @return the rate
	 */
	public Double getRate() {
		return rate;
	}

	/**
	 * @param rate the rate to set
	 */
	public void setRate(Double rate) {
		this.rate = rate;
	}

	/**
	 * @return the taxableValue
	 */
	public Double getTaxableValue() {
		return taxableValue;
	}

	/**
	 * @param taxableValue the taxableValue to set
	 */
	public void setTaxableValue(Double taxableValue) {
		this.taxableValue = taxableValue;
	}

	/**
	 * @return the integratedTax
	 */
	public Double getIntegratedTax() {
		return integratedTax;
	}

	/**
	 * @param integratedTax the integratedTax to set
	 */
	public void setIntegratedTax(Double integratedTax) {
		this.integratedTax = integratedTax;
	}

	/**
	 * @return the centralTax
	 */
	public Double getCentralTax() {
		return centralTax;
	}

	/**
	 * @param centralTax the centralTax to set
	 */
	public void setCentralTax(Double centralTax) {
		this.centralTax = centralTax;
	}

	/**
	 * @return the stateTax
	 */
	public Double getStateTax() {
		return stateTax;
	}

	/**
	 * @param stateTax the stateTax to set
	 */
	public void setStateTax(Double stateTax) {
		this.stateTax = stateTax;
	}

	/**
	 * @return the cess
	 */
	public Double getCess() {
		return cess;
	}

	/**
	 * @param cess the cess to set
	 */
	public void setCess(Double cess) {
		this.cess = cess;
	}


	public B2BGSTReport(String id, String gstin, String invoiceNumber, Date invoiceDate, Double invoiceValue, String stateCode, Long reverseCharge, Double applicablePercentOfTaxRate, String invoiceType, String eCommerceGstn, Integer serialNumber, Double rate, Double taxableValue, Double integratedTax, Double centralTax, Double stateTax, Double cess) {
		super();
		this.id = id;
		this.gstin = gstin;
		this.invoiceNumber = invoiceNumber;
		this.invoiceDate = invoiceDate;
		this.invoiceValue = invoiceValue;
		this.stateCode = stateCode;
		this.reverseCharge = reverseCharge;
		this.applicablePercentOfTaxRate = applicablePercentOfTaxRate;
		this.invoiceType = invoiceType;
		this.eCommerceGstn = eCommerceGstn;
		this.serialNumber = serialNumber;
		this.rate = rate;
		this.taxableValue = taxableValue;
		this.integratedTax = integratedTax;
		this.centralTax = centralTax;
		this.stateTax = stateTax;
		this.cess = cess;
	}

	public Double getApplicablePercentOfTaxRate() {
		return applicablePercentOfTaxRate;
	}

	public void setApplicablePercentOfTaxRate(Double applicablePercentOfTaxRate) {
		this.applicablePercentOfTaxRate = applicablePercentOfTaxRate;
	}

	public Long getReverseCharge() {
		return reverseCharge;
	}

	public void setReverseCharge(Long reverseCharge) {
		this.reverseCharge = reverseCharge;
	}

	public Integer getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}


	
}
