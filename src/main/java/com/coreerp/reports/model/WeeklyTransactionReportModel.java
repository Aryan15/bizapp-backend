package com.coreerp.reports.model;

import java.util.Date;

public class WeeklyTransactionReportModel {
    private Long invoiceCount;
    private Double invoiceAmount;
    private Double BalanecAmount;
    private Double paidAmount;


    public WeeklyTransactionReportModel(Long invoiceCount, Double invoiceAmount, Double BalanecAmount,Double paidAmount)
             {
        super();
        this.invoiceCount = invoiceCount;
        this.invoiceAmount = invoiceAmount;
        this.BalanecAmount = BalanecAmount;
        this.paidAmount = invoiceAmount-BalanecAmount;

    }











//    public Long getPoCount() {
//        return poCount;
//    }
//
//    public void setPoCount(Long poCount) {
//        this.poCount = poCount;
//    }

    public Long getInvoiceCount() {
        return invoiceCount;
    }

    public void setInvoiceCount(Long invoiceCount) {
        this.invoiceCount = invoiceCount;
    }

    public Double getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(Double invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public Double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Double getBalanecAmount() {
        return BalanecAmount;
    }

    public void setBalanecAmount(Double balanecAmount) {
        BalanecAmount = balanecAmount;
    }

//    public Date getFromDate() {
//        return fromDate;
//    }
//
//    public void setFromDate(Date fromDate) {
//        this.fromDate = fromDate;
//    }
//
//    public Date getToDate() {
//        return toDate;
//    }
//
//    public void setToDate(Date toDate) {
//        this.toDate = toDate;
//    }
//
//    public String getTransactionName() {
//        return transactionName;
//    }
//
//    public void setTransactionName(String transactionName) {
//        this.transactionName = transactionName;
//    }
}
