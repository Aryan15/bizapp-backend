package com.coreerp.reports.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

//@Entity
//@NamedQueries({
//@NamedQuery(name = "InvoiceItemReportModel.getReport",
//query = "SELECT ini.id "
//		+ ",inh.invoiceNumber "
//		+ ",inh.invoiceDate "
//		+ ",party.name AS partyName"
//		+ ",inh.totalTaxableAmount "
//		+ ",inh.cgstTaxRate "
//		+ ",inh.cgstTaxAmount "
//		+ ",inh.sgstTaxRate "
//		+ ",inh.sgstTaxAmount "
//		+ ",inh.igstTaxRate "
//		+ ",inh.igstTaxAmount "
//		+ ",inh.advanceAmount "
//		+ ",inh.grandTotal "
//		+ ",mat.name AS description "
//		+ ",ini.quantity "
//		+ ",ini.price "
//		+ ",ini.discountAmount "
//		+ " FROM InvoiceHeader inh "
//		+ " join inh.financialYear finY"
//		+ " join inh.invoiceItems ini"
//		+ " join ini.material mat"
//		+ " join inh.party party "
//		+ " join party.partyType pType"
//		+ " where 1 = 1 "
//		//+ " and (finY.id = ?1 or ?1 is null or ?1='') "
//		+ " and finY.id = :financialYearId  "
//		//+ " and ((?2 is null or ?2='' and (?3 is not null and ?3!='' and inh.invoiceDate <= ?3) ) or (?3 is null or ?3='' and (?2 is not null and ?3!='' and inh.invoiceDate >= ?2 )) or ((?2 is null or ?2='') and (?3 is null or ?3='' )) or (inh.invoiceDate BETWEEN ?2 AND ?3)) "
//		+ " and inh.invoiceDate BETWEEN :fromDate AND :toDate "
//		//+ " and (party.id = ?4 or ?4 is null or ?4='' )"
//		+ " and party.id = :partyId "
//		//+ " and (pType.id in ?5 or ?5 is null or ?5='' )"
//		+ " and pType.id in :partyTypeIds "
//)
//})
public class InvoiceItemReportModel {

	private String id;

	private String invoiceNumber;

	private Date invoiceDate;

	private String partyName;

	private Double totalTaxableAmount;

	private Double cgstTaxRate;

	private Double cgstTaxAmount;

	private Double sgstTaxRate;

	private Double sgstTaxAmount;

	private Double igstTaxRate;

	private Double igstTaxAmount;

	private Double advanceAmount;

	private Double grandTotal;

	private String description;

	private Double quantity;

	private Double price;

	private Double discountAmount;
	
	private String statusName;

	private String uom;
	
	private Double amountAfterDiscount;
	
	private Integer inclusiveTax;
	
	private Double taxableAmount;

	private  String partNumber;

	private String invoiceHeaderId;

	private String partyCode;

	private String hsnCode;

	private String gstNumber;

	private String internalReferenceNumber;

	public InvoiceItemReportModel(String id, String invoiceNumber, Date invoiceDate, String partyName,
			Double totalTaxableAmount, Double cgstTaxRate, Double cgstTaxAmount, Double sgstTaxRate,
			Double sgstTaxAmount, Double igstTaxRate, Double igstTaxAmount, Double advanceAmount, Double grandTotal,
			String description,String partNumber,String invoiceHeaderId, Double quantity, Double price, Double discountAmount, String statusName, String uom,
			Double amountAfterDiscount, Integer inclusiveTax,String partyCode, Double taxableAmount,String hsnCode,String gstNumber,String internalReferenceNumber) {
		super();
		this.id = id;
		this.invoiceNumber = invoiceNumber;
		this.invoiceDate = invoiceDate;
		this.partyName =partyCode+"-"+partyName;
		this.totalTaxableAmount = totalTaxableAmount;
		this.cgstTaxRate = cgstTaxRate;
		this.cgstTaxAmount = cgstTaxAmount;
		this.sgstTaxRate = sgstTaxRate;
		this.sgstTaxAmount = sgstTaxAmount;
		this.igstTaxRate = igstTaxRate;
		this.igstTaxAmount = igstTaxAmount;
		this.advanceAmount = advanceAmount;
		this.grandTotal = grandTotal;
		this.description = description;
        this.partNumber = partNumber;
        this.invoiceHeaderId = invoiceHeaderId;
		this.quantity = quantity;
		this.price = price;
		this.discountAmount = discountAmount;
		this.statusName = statusName;
		this.uom = uom;
		this.amountAfterDiscount = amountAfterDiscount;
		this.inclusiveTax = inclusiveTax;
		this.partyCode = partyCode;
		this.taxableAmount = taxableAmount;
		this.hsnCode = hsnCode;
		this.gstNumber = gstNumber;
		this.internalReferenceNumber = internalReferenceNumber;


	}

	//
	// @Id
	// @Column(name="id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	// @Column(name="invoiceNumber")
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	// @Column(name="invoiceDate")
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	// @Column(name="partyName")
	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	// @Column(name="totalTaxableAmount")
	public Double getTotalTaxableAmount() {
		return totalTaxableAmount;
	}

	public void setTotalTaxableAmount(Double totalTaxableAmount) {
		this.totalTaxableAmount = totalTaxableAmount;
	}

	// @Column(name="cgstTaxRate")
	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}

	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}

	// @Column(name="cgstTaxAmount")
	public Double getCgstTaxAmount() {
		return cgstTaxAmount;
	}

	public void setCgstTaxAmount(Double cgstTaxAmount) {
		this.cgstTaxAmount = cgstTaxAmount;
	}

	// @Column(name="sgstTaxRate")
	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}

	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	// @Column(name="sgstTaxAmount")
	public Double getSgstTaxAmount() {
		return sgstTaxAmount;
	}

	public void setSgstTaxAmount(Double sgstTaxAmount) {
		this.sgstTaxAmount = sgstTaxAmount;
	}

	// @Column(name="igstTaxRate")
	public Double getIgstTaxRate() {
		return igstTaxRate;
	}

	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}

	// @Column(name="igstTaxAmount")
	public Double getIgstTaxAmount() {
		return igstTaxAmount;
	}

	public void setIgstTaxAmount(Double igstTaxAmount) {
		this.igstTaxAmount = igstTaxAmount;
	}

	// @Column(name="advanceAmount")
	public Double getAdvanceAmount() {
		return advanceAmount;
	}

	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

	// @Column(name="grandTotal")
	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	// @Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	// @Column(name="quantity")
	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	// @Column(name="price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	// @Column(name="discountAmount")
	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	@Override
	public String toString() {
		return "InvoiceItemReportModel [id=" + id + ", invoiceNumber=" + invoiceNumber + ", invoiceDate=" + invoiceDate
				+ ", partyName=" + partyName + ", totalTaxableAmount=" + totalTaxableAmount + ", cgstTaxRate="
				+ cgstTaxRate + ", cgstTaxAmount=" + cgstTaxAmount + ", sgstTaxRate=" + sgstTaxRate + ", sgstTaxAmount="
				+ sgstTaxAmount + ", igstTaxRate=" + igstTaxRate + ", igstTaxAmount=" + igstTaxAmount
				+ ", advanceAmount=" + advanceAmount + ", grandTotal=" + grandTotal + ", description=" + description + ", partNumber="
				+ partNumber + ", quantity=" + quantity + ", price=" + price + ", discountAmount=" + discountAmount +",invoiceHeaderId="+invoiceHeaderId +",partyCode="+partyCode+",hsnCode="+hsnCode +",gstNumber="+gstNumber+ ",internalReferenceNumber="+internalReferenceNumber+ "]";
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}

	public void setAmountAfterDiscount(Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}

	public Integer getInclusiveTax() {
		return inclusiveTax;
	}

	public void setInclusiveTax(Integer inclusiveTax) {
		this.inclusiveTax = inclusiveTax;
	}

	public Double getTaxableAmount() {
		return taxableAmount;
	}

	public void setTaxableAmount(Double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

	public String getInvoiceHeaderId() {
		return invoiceHeaderId;
	}

	public void setInvoiceHeaderId(String invoiceHeaderId) {
		this.invoiceHeaderId = invoiceHeaderId;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getInternalReferenceNumber() {
		return internalReferenceNumber;
	}

	public void setInternalReferenceNumber(String internalReferenceNumber) {
		this.internalReferenceNumber = internalReferenceNumber;
	}
}
