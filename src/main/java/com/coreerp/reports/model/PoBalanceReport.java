package com.coreerp.reports.model;
import java.util.Date;
public class PoBalanceReport {
    private String poNumber;
    private Date purchaseOrderDate;
    private Double poQuantity;
    private Double price;
    private Double dcPendingQuantity;
    private Double invoicePendingQuantity;
    private String dcNumber;
    private Date dcDate;
    private Double jwInDCInvoicePendingQuantity;
    private Double jwInDCQuantity;
    private Double jwInDCPendingQuantity;
    private Double jwInDCIncomingQuantity;
    private String jwOutdcNumber;
    private Date jwOutdcDate;
    private Double jwOutDcInvoicePendingQuantity;
    private Double jwOutDcQuantity;
    private Double jwOutDCPendingQuantity;
    private Double jwOutDCIncomingQuantity;


    private String partyName;
    private String statusName;
    private String materialName;
    private String partNumber;
//    private String poHeaderId;
//    private Double  quanity ;
//
//
//
    private Double dcBalanceAmount;
    private Double invoiceBalanceAmount;
    private String partyCode;
    private Long poTypeId ;
    private String poItemId;
    private String dcTranType;
    private Long dcTransactionTypeId;
    private Integer isOpenEnded;

    private Double inGrnBalance;
    private Double grnDcQuantity;
    private Double grnAcceptedQuantity;
    private Double rejectedQuantity;

    private String grnNumber;
    private Double dcGrnBalanceQuantity;
 private String dcstatusId;
 private String internelRefNumber;


    public PoBalanceReport(String poNumber, Date purchaseOrderDate,Double poQuantity,Double price,Double dcPendingQuantity,Double invoicePendingQuantity,String dcNumber,Date dcDate,Double jwInDCInvoicePendingQuantity,Double jwInDCQuantity,Double jwInDCPendingQuantity,Double jwInDCIncomingQuantity,String jwOutdcNumber,Date jwOutdcDate,Double jwOutDcInvoicePendingQuantity,Double jwOutDcQuantity,Double jwOutDCPendingQuantity,Double jwOutDCIncomingQuantity,String partyName,String statusName,String materialName,String partNumber,String partyCode,Long poTypeId,String poItemId,String dcTranType,Long dcTransactionTypeId,Integer isOpenEnded,Double inGrnBalance,Double grnDcQuantity,Double grnAcceptedQuantity,Double rejectedQuantity,String grnNumber,Double dcGrnBalanceQuantity,String dcstatusId,String internelRefNumber) {

        super();


        this.poNumber = poNumber;
        this.purchaseOrderDate = purchaseOrderDate;
        this.poQuantity = poQuantity;
        this.price = price;
        if (poTypeId == 11 || poTypeId == 2) {
            if (statusName.equalsIgnoreCase("New") || statusName.equalsIgnoreCase("Invoiced") || statusName.equalsIgnoreCase("Partially Invoiced")) {
                this.dcPendingQuantity = dcPendingQuantity;
                this.invoicePendingQuantity = invoicePendingQuantity;
            } else if (statusName.equalsIgnoreCase("Partial DC") || statusName.equalsIgnoreCase("DC Generated")) {
                this.dcPendingQuantity = dcPendingQuantity;
                if(jwInDCQuantity==null){
                    jwInDCQuantity=0.0;
                }
                if(jwInDCInvoicePendingQuantity==null){
                    jwInDCInvoicePendingQuantity=0.0;
                }
                this.invoicePendingQuantity = (invoicePendingQuantity - jwInDCQuantity) + jwInDCInvoicePendingQuantity;

            }


        }

        if(dcstatusId!=null) {
            if (poTypeId == 2 && (dcstatusId.equalsIgnoreCase("Partial GRN") || dcstatusId.equalsIgnoreCase("GRN Generated"))) {

                if(dcGrnBalanceQuantity==null){
                    dcGrnBalanceQuantity=0.0;
                }
                if(dcPendingQuantity==null){
                    dcPendingQuantity=0.0;
                }
                if(inGrnBalance==null){
                    inGrnBalance=0.0;
                }
                this.invoicePendingQuantity = dcPendingQuantity + dcGrnBalanceQuantity + inGrnBalance;


            }
        }

         if (poTypeId == 20 || poTypeId == 21) {
            if (statusName.equalsIgnoreCase("New") || statusName.equalsIgnoreCase("Invoiced") || statusName.equalsIgnoreCase("Partially Invoiced")) {
                this.dcPendingQuantity = dcPendingQuantity;
                this.invoicePendingQuantity = invoicePendingQuantity;
            } else if ((statusName.equalsIgnoreCase("I/C JW DC Created") || statusName.equalsIgnoreCase("I/C JW DC Partial")|| statusName.equalsIgnoreCase("O/G JW DC Created") || statusName.equalsIgnoreCase("O/G JW DC Partial")) ) {
                if(jwInDCPendingQuantity==null){
                    jwInDCPendingQuantity=0.0;
                }
                if(jwOutDcInvoicePendingQuantity==null){
                    jwOutDcInvoicePendingQuantity=0.0;
                }
                if(jwInDCQuantity==null){
                    jwInDCQuantity=0.0;
                }
                if(jwOutDcQuantity==null){
                    jwOutDcQuantity=0.0;
                }
                if(jwInDCPendingQuantity!=0 && dcPendingQuantity!=0){
                    jwInDCPendingQuantity = (poQuantity - jwInDCQuantity) + jwInDCPendingQuantity;
                }
                if(dcPendingQuantity==0 && jwInDCPendingQuantity!=0 ){

                    jwInDCPendingQuantity = poQuantity - jwOutDcQuantity;

                }
                if(jwInDCPendingQuantity==0){
                    jwInDCPendingQuantity = dcPendingQuantity;

                }
                jwOutDcInvoicePendingQuantity = (poQuantity - jwOutDcQuantity) + jwOutDcInvoicePendingQuantity;
               this.dcPendingQuantity = jwInDCPendingQuantity;
                this.invoicePendingQuantity = jwOutDcInvoicePendingQuantity;

            }
            if(isOpenEnded!=null && isOpenEnded==1){
                this.dcPendingQuantity = 1.0;
                this.invoicePendingQuantity = 1.0;
            }

        }


        this.dcNumber = dcNumber;
        this.dcDate = dcDate;
        this.jwInDCInvoicePendingQuantity = jwInDCInvoicePendingQuantity;
        this.jwInDCQuantity = jwInDCQuantity;
        this.jwInDCPendingQuantity = jwInDCPendingQuantity;
        this.jwInDCIncomingQuantity = jwInDCIncomingQuantity;
        this.jwOutdcNumber = jwOutdcNumber;
        this.jwOutdcDate = jwOutdcDate;
        this.jwOutDcInvoicePendingQuantity = jwOutDcInvoicePendingQuantity;
        this.jwOutDcQuantity = jwOutDcQuantity;
        this.jwOutDCPendingQuantity = jwOutDCPendingQuantity;
        this.jwOutDCIncomingQuantity = jwOutDCIncomingQuantity;

        this.partyName = partyName + "-" + partyCode;
        this.statusName = statusName;
        if (partNumber != null) {
            this.materialName = materialName + "-" + partNumber;
        } else {
            this.materialName = materialName;
        }

        this.partNumber = partNumber;


        this.partyCode = partyCode;
        this.poTypeId = poTypeId;
        if (price != null) {
            if (this.dcPendingQuantity == null) {
                this.dcPendingQuantity = 0.0;
            }
            if (this.invoicePendingQuantity == null) {
                this.invoicePendingQuantity = 0.0;
            }
            this.dcBalanceAmount = price * this.dcPendingQuantity;
            this.invoiceBalanceAmount = price * this.invoicePendingQuantity;
        }
//        if(isOpenEnded!=null && isOpenEnded==1){
//            this.dcBalanceAmount = 1.0;
//            this.invoiceBalanceAmount = 1.0;
//        }
        this.poItemId = poItemId;

        this.dcTranType = dcTranType;

        this.dcTransactionTypeId =dcTransactionTypeId;

        this.isOpenEnded =isOpenEnded;

        this.inGrnBalance =inGrnBalance;
        this.grnDcQuantity =grnDcQuantity;
        this.grnAcceptedQuantity =grnAcceptedQuantity;
        this.rejectedQuantity = rejectedQuantity;
        this.grnNumber =grnNumber;
        this.dcGrnBalanceQuantity =dcGrnBalanceQuantity;
       this.dcstatusId =dcstatusId;
       this.internelRefNumber = internelRefNumber;
    }



    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public Date getPurchaseOrderDate() {
        return purchaseOrderDate;
    }

    public void setPurchaseOrderDate(Date purchaseOrderDate) {
        this.purchaseOrderDate = purchaseOrderDate;
    }

    public Double getPoQuantity() {
        return poQuantity;
    }

    public void setPoQuantity(Double poQuantity) {
        this.poQuantity = poQuantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDcPendingQuantity() {
        return dcPendingQuantity;
    }

    public void setDcPendingQuantity(Double dcPendingQuantity) {
        this.dcPendingQuantity = dcPendingQuantity;
    }

    public Double getInvoicePendingQuantity() {
        return invoicePendingQuantity;
    }

    public void setInvoicePendingQuantity(Double invoicePendingQuantity) {
        this.invoicePendingQuantity = invoicePendingQuantity;
    }

    public String getDcNumber() {
        return dcNumber;
    }

    public void setDcNumber(String dcNumber) {
        this.dcNumber = dcNumber;
    }

    public Date getDcDate() {
        return dcDate;
    }

    public void setDcDate(Date dcDate) {
        this.dcDate = dcDate;
    }

    public Double getJwInDCInvoicePendingQuantity() {
        return jwInDCInvoicePendingQuantity;
    }

    public void setJwInDCInvoicePendingQuantity(Double jwInDCInvoicePendingQuantity) {
        this.jwInDCInvoicePendingQuantity = jwInDCInvoicePendingQuantity;
    }

    public Double getJwInDCQuantity() {
        return jwInDCQuantity;
    }

    public void setJwInDCQuantity(Double jwInDCQuantity) {
        this.jwInDCQuantity = jwInDCQuantity;
    }

    public Double getJwInDCPendingQuantity() {
        return jwInDCPendingQuantity;
    }

    public void setJwInDCPendingQuantity(Double jwInDCPendingQuantity) {
        this.jwInDCPendingQuantity = jwInDCPendingQuantity;
    }

    public Double getJwInDCIncomingQuantity() {
        return jwInDCIncomingQuantity;
    }

    public void setJwInDCIncomingQuantity(Double jwInDCIncomingQuantity) {
        this.jwInDCIncomingQuantity = jwInDCIncomingQuantity;
    }

    public String getJwOutdcNumber() {
        return jwOutdcNumber;
    }

    public void setJwOutdcNumber(String jwOutdcNumber) {
        this.jwOutdcNumber = jwOutdcNumber;
    }

    public Date getJwOutdcDate() {
        return jwOutdcDate;
    }

    public void setJwOutdcDate(Date jwOutdcDate) {
        this.jwOutdcDate = jwOutdcDate;
    }

    public Double getJwOutDcInvoicePendingQuantity() {
        return jwOutDcInvoicePendingQuantity;
    }

    public void setJwOutDcInvoicePendingQuantity(Double jwOutDcInvoicePendingQuantity) {
        this.jwOutDcInvoicePendingQuantity = jwOutDcInvoicePendingQuantity;
    }

    public Double getJwOutDcQuantity() {
        return jwOutDcQuantity;
    }

    public void setJwOutDcQuantity(Double jwOutDcQuantity) {
        this.jwOutDcQuantity = jwOutDcQuantity;
    }

    public Double getJwOutDCPendingQuantity() {
        return jwOutDCPendingQuantity;
    }

    public void setJwOutDCPendingQuantity(Double jwOutDCPendingQuantity) {
        this.jwOutDCPendingQuantity = jwOutDCPendingQuantity;
    }

    public Double getJwOutDCIncomingQuantity() {
        return jwOutDCIncomingQuantity;
    }

    public void setJwOutDCIncomingQuantity(Double jwOutDCIncomingQuantity) {
        this.jwOutDCIncomingQuantity = jwOutDCIncomingQuantity;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartyCode() {
        return partyCode;
    }

    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }


    public Long getPoTypeId() {
        return poTypeId;
    }

    public void setPoTypeId(Long poTypeId) {
        this.poTypeId = poTypeId;
    }

    public Double getDcBalanceAmount() {
        return dcBalanceAmount;
    }

    public void setDcBalanceAmount(Double dcBalanceAmount) {
        this.dcBalanceAmount = dcBalanceAmount;
    }

    public Double getInvoiceBalanceAmount() {
        return invoiceBalanceAmount;
    }

    public void setInvoiceBalanceAmount(Double invoiceBalanceAmount) {
        this.invoiceBalanceAmount = invoiceBalanceAmount;
    }

    public String getPoItemId() {
        return poItemId;
    }

    public void setPoItemId(String poItemId) {
        this.poItemId = poItemId;
    }

    public String getDcTranType() {
        return dcTranType;
    }

    public void setDcTranType(String dcTranType) {
        this.dcTranType = dcTranType;
    }

    public Long getDcTransactionTypeId() {
        return dcTransactionTypeId;
    }

    public void setDcTransactionTypeId(Long dcTransactionTypeId) {
        this.dcTransactionTypeId = dcTransactionTypeId;
    }

    public Integer getIsOpenEnded() {
        return isOpenEnded;
    }

    public void setIsOpenEnded(Integer isOpenEnded) {
        this.isOpenEnded = isOpenEnded;
    }

    public String getDcstatusId() {
        return dcstatusId;
    }

    public void setDcstatusId(String dcstatusId) {
        this.dcstatusId = dcstatusId;
    }

    public Double getInGrnBalance() {
        return inGrnBalance;
    }

    public void setInGrnBalance(Double inGrnBalance) {
        this.inGrnBalance = inGrnBalance;
    }

    public Double getGrnDcQuantity() {
        return grnDcQuantity;
    }

    public void setGrnDcQuantity(Double grnDcQuantity) {
        this.grnDcQuantity = grnDcQuantity;
    }

    public Double getGrnAcceptedQuantity() {
        return grnAcceptedQuantity;
    }

    public void setGrnAcceptedQuantity(Double grnAcceptedQuantity) {
        this.grnAcceptedQuantity = grnAcceptedQuantity;
    }

    public Double getRejectedQuantity() {
        return rejectedQuantity;
    }

    public void setRejectedQuantity(Double rejectedQuantity) {
        this.rejectedQuantity = rejectedQuantity;
    }

    public String getGrnNumber() {
        return grnNumber;
    }

    public void setGrnNumber(String grnNumber) {
        this.grnNumber = grnNumber;
    }

    public Double getDcGrnBalanceQuantity() {
        return dcGrnBalanceQuantity;
    }

    public void setDcGrnBalanceQuantity(Double dcGrnBalanceQuantity) {
        this.dcGrnBalanceQuantity = dcGrnBalanceQuantity;
    }

    public String getInternelRefNumber() {
        return internelRefNumber;
    }

    public void setInternelRefNumber(String internelRefNumber) {
        this.internelRefNumber = internelRefNumber;
    }
}
