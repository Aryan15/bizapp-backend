package com.coreerp.reports.model;

import java.util.Date;

public class BalanceReport {
	private String id;
	private String invoiceNumber;
	private Date invoiceDate;
	private Double invoiceAmount;
	private Double paidAmount;
	private Double dueAmount;
	private String partyName;
	private String gstin;
	 private String statusName;
	 private String partyCode;
	public BalanceReport(String id, String invoiceNumber, Date invoiceDate, Double invoiceAmount, Double paidAmount,
			Double dueAmount, String partyName, String gstin, String statusName,String partyCode) {
		super();
		this.id = id;
		this.invoiceNumber = invoiceNumber;
		this.invoiceDate = invoiceDate;
		this.invoiceAmount = invoiceAmount;
		this.paidAmount = paidAmount;
		this.dueAmount = dueAmount;
		this.partyName = partyCode+"-"+partyName;
		this.gstin = gstin;
		this.statusName = statusName;
		this.partyCode = partyCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Double getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getGstin() {
		return gstin;
	}
	public void setGstin(String gstin) {
		this.gstin = gstin;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}
}
