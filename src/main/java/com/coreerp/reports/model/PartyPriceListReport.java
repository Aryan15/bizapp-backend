package com.coreerp.reports.model;

import java.util.Date;

public class PartyPriceListReport {
    private Long id;
    private  double partyPrice;
    private  String comments;
    private String  partyName;
    private Long materialId;
    private  String materialName;
    private  double currentPrice;
    private  String partNumber;
    private  String materialTypeName;

    public PartyPriceListReport(Long id,Double partyPrice,String comments, String partyName,Long materialId, String materialName,
                                Double currentPrice,  String partNumber, String materialTypeName ) {
        super();
        this.id =id;
        if(partyPrice!=null) {
            this.partyPrice = partyPrice;
        }
        if(comments!=null) {
            this.comments = comments;
        }
        if(partyName!=null) {
            this.partyName = partyName;
        }

        this.materialId = materialId;
        this.materialName = materialName;
        if(currentPrice!=null) {
            this.currentPrice = currentPrice;
        }
        if(partNumber!=null) {
            this.partNumber = partNumber;
        }
        if(materialTypeName!=null) {
            this.materialTypeName = materialTypeName;
        }



    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getMaterialTypeName() {
        return materialTypeName;
    }

    public void setMaterialTypeName(String materialTypeName) {
        this.materialTypeName = materialTypeName;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }




    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Long materialId) {
        this.materialId = materialId;
    }

    public double getPartyPrice() {
        return partyPrice;
    }

    public void setPartyPrice(double partyPrice) {
        this.partyPrice = partyPrice;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }
}
