package com.coreerp.reports.model;

import java.util.Date;

public class EWayBillReport {

    private Long id;
    private String invoiceHeaderId;
    private String invoiceNumber;
    private String eWayBillNo;
    private Date eWayBillDate;
    private String partyName;
    private String supplyType;
    private Date invoiceDate;
    private String partyGstNum;
    private String transportedGstNum;
    private String fromAddress;
    private String toAddress;
    private String status;
    private Integer noOfItems;
    private String hsnCode;
    private String hsnDescription;
    private Double assesableValue;
    private Double sgst;
    private Double cgst;
    private Double igst;
    private Double cess;
    private Double noncess;
    private Double otherValue;
    private Double invoiceValue;
    private Date eWayBillValidity;
    private String pdfUrl;
    private String detailedpdfUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvoiceHeaderId() {
        return invoiceHeaderId;
    }

    public void setInvoiceHeaderId(String invoiceHeaderId) {
        this.invoiceHeaderId = invoiceHeaderId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String geteWayBillNo() {
        return eWayBillNo;
    }

    public void seteWayBillNo(String eWayBillNo) {
        this.eWayBillNo = eWayBillNo;
    }

    public Date geteWayBillDate() {
        return eWayBillDate;
    }

    public void seteWayBillDate(Date eWayBillDate) {
        this.eWayBillDate = eWayBillDate;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getSupplyType() {
        return supplyType;
    }

    public void setSupplyType(String supplyType) {
        this.supplyType = supplyType;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getPartyGstNum() {
        return partyGstNum;
    }

    public void setPartyGstNum(String partyGstNum) {
        this.partyGstNum = partyGstNum;
    }

    public String getTransportedGstNum() {
        return transportedGstNum;
    }

    public void setTransportedGstNum(String transportedGstNum) {
        this.transportedGstNum = transportedGstNum;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getNoOfItems() {
        return noOfItems;
    }

    public void setNoOfItems(Integer noOfItems) {
        this.noOfItems = noOfItems;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public String getHsnDescription() {
        return hsnDescription;
    }

    public void setHsnDescription(String hsnDescription) {
        this.hsnDescription = hsnDescription;
    }

    public Double getAssesableValue() {
        return assesableValue;
    }

    public void setAssesableValue(Double assesableValue) {
        this.assesableValue = assesableValue;
    }

    public Double getSgst() {
        return sgst;
    }

    public void setSgst(Double sgst) {
        this.sgst = sgst;
    }

    public Double getCgst() {
        return cgst;
    }

    public void setCgst(Double cgst) {
        this.cgst = cgst;
    }

    public Double getIgst() {
        return igst;
    }

    public void setIgst(Double igst) {
        this.igst = igst;
    }

    public Double getCess() {
        return cess;
    }

    public void setCess(Double cess) {
        this.cess = cess;
    }

    public Double getNoncess() {
        return noncess;
    }

    public void setNoncess(Double noncess) {
        this.noncess = noncess;
    }

    public Double getOtherValue() {
        return otherValue;
    }

    public void setOtherValue(Double otherValue) {
        this.otherValue = otherValue;
    }

    public Double getInvoiceValue() {
        return invoiceValue;
    }

    public void setInvoiceValue(Double invoiceValue) {
        this.invoiceValue = invoiceValue;
    }

    public Date geteWayBillValidity() {
        return eWayBillValidity;
    }

    public void seteWayBillValidity(Date eWayBillValidity) {
        this.eWayBillValidity = eWayBillValidity;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public String getDetailedpdfUrl() {
        return detailedpdfUrl;
    }

    public void setDetailedpdfUrl(String detailedpdfUrl) {
        this.detailedpdfUrl = detailedpdfUrl;
    }

    public EWayBillReport(Long id, String invoiceHeaderId, String invoiceNumber, String eWayBillNo, Date eWayBillDate, String partyName, String supplyType, Date invoiceDate, String partyGstNum, String transportedGstNum, String fromAddress, String toAddress, String status, Integer noOfItems, String hsnCode, String hsnDescription, Double assesableValue, Double sgst, Double cgst, Double igst, Double cess, Double noncess, Double otherValue, Double invoiceValue, Date eWayBillValidity, String pdfUrl, String detailedpdfUrl) {
        super();
        this.id = id;
        this.invoiceHeaderId = invoiceHeaderId;
        this.invoiceNumber = invoiceNumber;
        this.eWayBillNo = eWayBillNo;
        this.eWayBillDate = eWayBillDate;
        this.partyName = partyName;
        this.supplyType = supplyType;
        this.invoiceDate = invoiceDate;
        this.partyGstNum = partyGstNum;
        this.transportedGstNum = transportedGstNum;
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.status = status;
        this.noOfItems = noOfItems;
        if(hsnCode==null){
            this.hsnCode="";
        }
        else {
            this.hsnCode = hsnCode;
        }
        this.hsnDescription = hsnDescription;
        this.assesableValue = assesableValue;
        this.sgst = sgst;
        this.cgst = cgst;
        this.igst = igst;
        this.cess = cess;
        this.noncess = noncess;
        this.otherValue = otherValue;
        this.invoiceValue = invoiceValue;
        this.eWayBillValidity = eWayBillValidity;
        this.pdfUrl = pdfUrl;
        this.detailedpdfUrl = detailedpdfUrl;
    }


}
