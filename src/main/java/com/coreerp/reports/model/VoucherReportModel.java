package com.coreerp.reports.model;

import java.util.Date;

public class VoucherReportModel {

	private String id;
	
	private Double amount;

	private String voucherNumber;
	
	private Long voucherId;
	
	private Date voucherDate;
	
	private Long voucherTypeId;
	
	private String comments;
	
	private String paidTo;
	
	private String chequeNumber;
	
	private Long companyId;
	
	private Long financialYearId;
	
	private String bankName;
	
	private Date chequeDate;
	
	private String itemId;
	
	private String description;
	
	private Double itemAmount;
	
	private Long expenseHeaderId;
	
	private String expenseName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Long getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(Long voucherId) {
		this.voucherId = voucherId;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public Long getVoucherTypeId() {
		return voucherTypeId;
	}

	public void setVoucherTypeId(Long voucherTypeId) {
		this.voucherTypeId = voucherTypeId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPaidTo() {
		return paidTo;
	}

	public void setPaidTo(String paidTo) {
		this.paidTo = paidTo;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getFinancialYearId() {
		return financialYearId;
	}

	public void setFinancialYearId(Long financialYearId) {
		this.financialYearId = financialYearId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(Double itemAmount) {
		this.itemAmount = itemAmount;
	}

	public Long getExpenseHeaderId() {
		return expenseHeaderId;
	}

	public void setExpenseHeaderId(Long expenseHeaderId) {
		this.expenseHeaderId = expenseHeaderId;
	}

	public String getExpenseName() {
		return expenseName;
	}

	public void setExpenseName(String expenseName) {
		this.expenseName = expenseName;
	}

	public VoucherReportModel(String id, Double amount, String voucherNumber, Long voucherId, Date voucherDate,
			Long voucherTypeId, String comments, String paidTo, String chequeNumber, Long companyId,
			Long financialYearId, String bankName, Date chequeDate, String itemId, String description,
			Double itemAmount, Long expenseHeaderId, String expenseName) {
		super();
		this.id = id;
		this.amount = amount;
		this.voucherNumber = voucherNumber;
		this.voucherId = voucherId;
		this.voucherDate = voucherDate;
		this.voucherTypeId = voucherTypeId;
		this.comments = comments;
		this.paidTo = paidTo;
		this.chequeNumber = chequeNumber;
		this.companyId = companyId;
		this.financialYearId = financialYearId;
		this.bankName = bankName;
		this.chequeDate = chequeDate;
		this.itemId = itemId;
		this.description = description;
		this.itemAmount = itemAmount;
		this.expenseHeaderId = expenseHeaderId;
		this.expenseName = expenseName;
	} 
	
	
}
