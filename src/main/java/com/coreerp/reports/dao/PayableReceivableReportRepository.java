package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.coreerp.model.PayableReceivableHeader;
import com.coreerp.reports.model.PayableReceivableReport;

public interface PayableReceivableReportRepository extends PagingAndSortingRepository<PayableReceivableHeader, String> {

	@Query(			
			"SELECT NEW com.coreerp.reports.model.PayableReceivableReport( prh.id "
			+ ",inh.invoiceNumber "
			+ ",inh.invoiceDate "
			+ ",inh.grandTotal AS invoiceAmount "
			+ ",pri.tdsPercentage "
			+ ",pri.tdsAmount "
			+ ",pri.amountAfterTds "
			+ ",pri.paidAmount "
			+ ",pri.payingAmount "
			+ ",prh.bankName "
			+ ",pm.name AS paymentMethodName "
			+ ",pri.dueAmount "
			+ ",party.name AS partyName "
			+ ",prh.paymentDocumentNumber "
			+ ",prh.payReferenceDate AS payReferenceDate "
			+ ",prh.payReferenceNumber AS payReferenceNumber "
			+ ",status.name AS statusName "
			+ ",party.partyCode AS partyCode ) "
			+ " FROM PayableReceivableItem pri "
			+ " join pri.payableReceivableHeader prh "
			+ " join pri.invoiceHeader inh "
			+ " join inh.invoiceType inType "
			+ " join prh.financialYear finY "
			+ " join inh.party party "
			+ " join prh.status status "
			+ " join party.partyType pType "
			+ " join prh.transactionType tType"
			+ " left join prh.paymentMethod pm "
			+ " where 1 = 1 "
			+ " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and (date(prh.payReferenceDate) >= :fromDate or :fromDate is null) "
			+ " and (date(prh.payReferenceDate) <= :toDate or :toDate is null) "
			+ " and (party.id = :partyId or :partyId is null ) "
			+ " and pType.id in :partyTypeIds "
			+ " and tType.id = :transactionTypeId "	
			+ " and status.name != :statusName "
			+ " and (inType.id = :inTypeId or :inTypeId is null ) "
			)
	public Page<PayableReceivableReport> getReport(@Param("financialYearId") Long financialYearId
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate
			, @Param("partyId") Long partyId
			, @Param("partyTypeIds") List<Long> partyTypeIds
			, @Param("transactionTypeId") Long transactionTypeId
			, @Param("statusName") String statusName
			, @Param("inTypeId") Long inTypeId
			, Pageable pageable);		
}
