package com.coreerp.reports.dao;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.coreerp.model.InvoiceItem;
import com.coreerp.reports.model.B2BGSTReport;

public interface GSTReportRepository extends PagingAndSortingRepository<InvoiceItem, String> {


    @Query(			
            "  SELECT NEW com.coreerp.reports.model.B2BGSTReport("
            + "	ih.id "
            + " ,p.gstNumber "
            + " ,ih.invoiceNumber "
            + " ,ih.invoiceDate "
            + " ,ih.grandTotal "
            + " ,st.stateCode "
            + " ,ih.isReverseCharge "            
            + " ,100.00 AS applicablePercentOfTaxRate "
            + " ,'R' AS invoiceType "
			+ " ,'' AS eCommerceGstn "
			+ " ,ii.slNo AS serialNumber "
//			+ " ,(ii.igstTaxPercentage > 0 ? ii.igstTaxPercentage : (ii.sgstTaxPercentage + ii.cgstTaxPercentage)) AS rate "
			+ ", CASE ii.igstTaxPercentage WHEN 0 THEN (ii.sgstTaxPercentage + ii.cgstTaxPercentage) ELSE ii.igstTaxPercentage END AS rate "
			+ " ,ii.amountAfterTax AS taxableValue "
			+ " ,ii.igstTaxAmount AS integratedTax "
			+ " ,ii.cgstTaxAmount AS centralTax "
			+ " ,ii.sgstTaxAmount AS stateTax "
			+ " ,ii.cessAmount AS cess ) " 
            + " FROM InvoiceHeader ih " 
            + " join ih.invoiceItems ii "
            + " join ih.party p "
            + " join p.state st "
            + " join ih.company c "
            + " join c.state cst "
            + " join p.gstRegistrationType gt "
            + " join ih.invoiceType tr "
            + " join ih.financialYear finY"
            + " where 1 = 1 "
			+ " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and (date(ih.invoiceDate) >= :fromDate or :fromDate is null) "
			+ " and (date(ih.invoiceDate) <= :toDate or :toDate is null) "
			+ " and (tr.id = :transactionId or :transactionId is null ) "
            )            
	public Page<B2BGSTReport> getB2BReport(@Param("financialYearId") Long financialYearId
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate
			, @Param("transactionId") Long transactionId
			, Pageable pageable);
            

}