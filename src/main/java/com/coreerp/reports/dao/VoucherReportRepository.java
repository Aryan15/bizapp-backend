package com.coreerp.reports.dao;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.coreerp.model.VoucherHeader;
import com.coreerp.reports.model.VoucherReportModel;

public interface VoucherReportRepository extends PagingAndSortingRepository<VoucherHeader, String> {

	

	
@Query(
			
			" SELECT NEW com.coreerp.reports.model.VoucherReportModel("
			+ " vh.id,	"  
			+ " vh.amount, "  
			+ " vh.voucherNumber, "  
			+ " vh.voucherId, " 
			+ " vh.voucherDate,	 " 
			+ " vType.id AS voucherTypeId,	" 
			+ " vh.comments, "  
			+ " vh.paidTo, "  
			+ " vh.chequeNumber,	" 
			+ " c.id AS companyId, "  
			+ " finY.id AS financialYearId,	"  
			+ " vh.bankName, "  
			+ " vh.chequeDate,	" 
			+ " vi.id AS itemId, "  
			+ " vi.description,	"  
			+ " vi.amount AS itemAmount, "  
			+ " eh.id AS expenseHeaderId,	"  
			+ " eh.name AS expenseName "			
			+ " ) "
			+ " FROM VoucherHeader vh "
			+ " join vh.financialYear finY "
			+ " join vh.voucherItems vi "
			+ " join vh.voucherType vType "
			+ " join vi.expenseHeader eh "
			+ " join vh.company c "
			+ " where 1 = 1 "
			+ " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and (eh.id = :expenseId or :expenseId is null) "
			+ " and (date(vh.voucherDate) >= :fromDate or :fromDate is null) "
			+ " and (date(vh.voucherDate) <= :toDate or :toDate is null) "
			+ " and vType.id = :transactionTypeId "
			)
	public Page<VoucherReportModel> getReport(@Param("financialYearId") Long financialYearId
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate
			, @Param("transactionTypeId") Long transactionTypeId
			, @Param("expenseId") Long expenseId
			, Pageable pageable);	

	
//	public Page<VoucherHeader> findByFinancialYearAndVoucherDateBetween(@Param("financialYear") FinancialYear financialYear
//	, @Param("fromDate") Date fromDate
//	, @Param("toDate") Date toDate
//	, @Param("transactionTypeId") Long transactionTypeId
//	, Pageable pageable);
	
}
