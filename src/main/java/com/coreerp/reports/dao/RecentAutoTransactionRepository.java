package com.coreerp.reports.dao;

import com.coreerp.model.AutoTransactionGeneration;
import com.coreerp.model.PettyCashHeader;
import com.coreerp.model.TransactionType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;

public interface RecentAutoTransactionRepository extends PagingAndSortingRepository<AutoTransactionGeneration, Long> {

    public Page<AutoTransactionGeneration> findAll(Pageable pageable);
    public Page<AutoTransactionGeneration> findByInvoiceNumberContainsIgnoreCase(String invoiceNumber, Pageable pageable);
    public Page<AutoTransactionGeneration> findByStartDate(Date StartDate, Pageable pageable);
}
