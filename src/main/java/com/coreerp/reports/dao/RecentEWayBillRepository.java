package com.coreerp.reports.dao;

import com.coreerp.model.EwayBillTracking;
import com.coreerp.reports.model.EWayBillReport;
import com.coreerp.reports.model.EwayBillRecentReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface RecentEWayBillRepository extends PagingAndSortingRepository<EwayBillTracking, Long> {

    @Query("SELECT NEW com.coreerp.reports.model.EwayBillRecentReport(ebt.id "
            + ",inh.id AS invoiceHeaderId "
            + ",inh.invoiceNumber "
            + ",ebt.eWayBillNo "
            + ",ebt.eWayBillDate "
            + ",party.name AS partyName )"
            + " FROM EwayBillTracking ebt "
            + " join ebt.invoiceHeader inh "
            + " join inh.party party"
            + " where 1 = 1 "
            + " and  ebt.status = 2 "
            + " and ((inh.invoiceNumber like CONCAT('%',:search,'%') or :search is null)"
            + " or (party.name  like  CONCAT('%',:search,'%') or :search is null))"
     )
    Page<EwayBillRecentReport> getEwayBillRecent(String search, Pageable pageable);



    @Query("SELECT NEW com.coreerp.reports.model.EWayBillReport(ebt.id "
            + ", inh.id AS invoiceHeaderId "
            + ", inh.invoiceNumber "
            + ", ebt.eWayBillNo "
            + ", ebt.eWayBillDate "
            + ", party.name AS partyName"
            + ",'Outward' AS supplyType"
            + ", inh.invoiceDate"
            + ", party.gstNumber AS partyGstNum"
            + ", inh.companyGstNumber AS transportedGstNum"
            + ", inh.companyAddress AS fromAddress"
            + ", party.address AS toAddress"
            + ", CASE WHEN(ebt.validUpto > CURRENT_TIMESTAMP ) THEN 'ACTIVE' ELSE 'INACTIVE' END AS status "
            + ", coalesce(ebt.noOfItems,0) AS noOfItems"
            + ", ebt.hsnCode AS hsnCode "
            + ", '' AS hsnDescription"
            + ", 0.0 AS assesableValue"
            + ", inh.sgstTaxAmount AS sgst"
            + ", inh.cgstTaxAmount AS cgst"
            + ", inh.igstTaxAmount AS igst"
            + ", coalesce(inh.cessAmount,0.0) AS cess"
            + ", 0.0 AS noncess"
            + ", 0.0 AS otherValue"
            + ", inh.grandTotal AS invoiceValue"
            + ", ebt.validUpto AS eWayBillValidity"
            + ", ebt.pdfUrl AS pdfUrl"
            + ", ebt.detailedpdfUrl AS detailedpdfUrl"
            +" )"
            + " FROM EwayBillTracking ebt "
            + " join ebt.invoiceHeader inh "
            + " join inh.party party"
            + " where 1 = 1 "
            + " and  ebt.status = 2 "
            + " and (date(ebt.eWayBillDate) >= :fromDate or :fromDate is null) "
            + " and (date(ebt.eWayBillDate) <= :toDate or :toDate is null) "
            + " and (party.id = :partyId or :partyId is null ) "
    )
    Page<EWayBillReport> getEwayBillReport( @Param("fromDate") Date fromDate
            , @Param("toDate") Date toDate
            , @Param("partyId") Long partyId
            , Pageable pageable);

}
