package com.coreerp.reports.dao;


import com.coreerp.model.QuotationHeader;

import com.coreerp.reports.model.QuotationSummaryReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface QuotationReportRepository extends PagingAndSortingRepository<QuotationHeader, String> {



    @Query("SELECT NEW com.coreerp.reports.model.QuotationSummaryReport(quh.id "
                    + ",quh.quotationNumber "
                    + ",quh.quotationDate "
                    + ",party.name AS partyName"
                    + ",quh.cgstTaxRate "
                    + ",quh.cgstTaxAmount "
                    + ",quh.sgstTaxRate "
                    + ",quh.sgstTaxAmount "
                    + ",quh.igstTaxRate "
                    + ",quh.igstTaxAmount "
                    + ",quh.discountAmount "
                    + ",quh.grandTotal"
                    + ",status.name AS statusName "
                    + ",quh.subTotalAmount AS amount"
                    + ",quh.netAmount AS totalTaxableAmount "
                    + ",quh.inclusiveTax AS inclusiveTax "
                    + ",party.partyCode AS partyCode"
                    + " ) "
                    + " FROM QuotationHeader quh "
                    + " join quh.financialYear finY"
                    + " join quh.party party "
                    + " join quh.status status "
                    + " join party.partyType pType"
                    + " join quh.quotationType quoType"
                    + " where 1 = 1 "
                    + " and (finY.id = :financialYearId or :financialYearId is null) "
                    + " and (date(quh.quotationDate) >= :fromDate or :fromDate is null) "
                    + " and (date(quh.quotationDate) <= :toDate or :toDate is null) "
                    + " and (party.id = :partyId or :partyId is null ) "
                    + " and pType.id in :partyTypeIds "
                    + " and quoType.id = :transactionTypeId "
                    + " and (quh.createdBy = :createdBy or :createdBy is null) "
    )
    public Page<QuotationSummaryReport> getReport(@Param("financialYearId") Long financialYearId
            , @Param("fromDate") Date fromDate
            , @Param("toDate") Date toDate
            , @Param("partyId") Long partyId
            , @Param("partyTypeIds") List<Long> partyTypeIds
            , @Param("transactionTypeId") Long transactionTypeId
            , @Param("createdBy") String createdBy
            , Pageable pageable);

}
