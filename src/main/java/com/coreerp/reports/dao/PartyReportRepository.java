package com.coreerp.reports.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.coreerp.model.Party;
import com.coreerp.model.PartyType;

public interface PartyReportRepository extends PagingAndSortingRepository<Party, Long> {

	public Page<Party> findByPartyTypeIn(List<PartyType> partyTypes, Pageable pageable);
	
	public Page<Party> findByPartyTypeInAndNameContainsIgnoreCaseOrPartyCodeContainsIgnoreCase(List<PartyType> partyTypes,String name,String partyCode,Pageable pageable);
	
	public Long countByPartyTypeInAndNameContainsIgnoreCaseOrPartyCodeContainsIgnoreCase(List<PartyType> partyTypes, String name,String partyCode);

	public Long countByPartyTypeIn(List<PartyType> partyTypes);
	
	@Query("select p from Party p "
			+ " join p.partyType pType "
			+ " where pType.id in :partyTypeIds "
			+ " and ( p.createdBy = :createdBy or :createdBy is null ) ")
	public Page<Party> findByPartyTypeInAndCreatedBy(@Param("partyTypeIds") List<Long> partyTypeIds
			, @Param("createdBy") String createdBy
			, Pageable pageable);
	
	 
}
