package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.dom4j.datatype.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RecentDeliveryChallanRepository extends PagingAndSortingRepository<DeliveryChallanHeader, String> {

	public Page<DeliveryChallanHeader> findByDeliveryChallanTypeAndDeliveryChallanNumberContainsIgnoreCase(TransactionType deliveryChallanType, String name, Pageable pageable);
	
	public Page<DeliveryChallanHeader> findByDeliveryChallanTypeAndDeliveryChallanDate(TransactionType deliveryChallanType, Date deliveryChallanDate, Pageable pageable);
	
	public Page<DeliveryChallanHeader> findByDeliveryChallanTypeAndPartyIn(TransactionType deliveryChallanType, List<Party> parties, Pageable pageable);
	
	public Long countByDeliveryChallanTypeAndDeliveryChallanNumberContainsIgnoreCase(TransactionType deliveryChallanType, String name);
	
	public Long countByDeliveryChallanTypeAndDeliveryChallanDate(TransactionType deliveryChallanType, Date deliveryChallanDate); 
	
	public Long countByDeliveryChallanTypeAndPartyIn(TransactionType deliveryChallanType, List<Party> parties);

	public Page<DeliveryChallanHeader> findByDeliveryChallanTypeAndFinancialYear(TransactionType deliveryChallanType, FinancialYear financialYear, Pageable pageable);
	
	public Long countByDeliveryChallanType( TransactionType deliveryChallanType);



	@Query("select dh from DeliveryChallanHeader dh "
			+ " join dh.deliveryChallanType dcType "
			+ " join dh.financialYear fY "
			+ " join dh.party p"
			+ " where 1 = 1 "
			+ " and dcType.id = :dcType "
			+ " and fY.id = :financialId "
			+ " and ((dh.deliveryChallanNumber like CONCAT('%',:search,'%') or :search is null)"
			+ " or (dh.partyName like  CONCAT('%',:search,'%') or :search is null)"
			+ " or  (date(dh.deliveryChallanDate) like CONCAT('%',:search,'%') or :search is null)"
			+ " or (p.partyCode like CONCAT('%',:search,'%') or :search is null))")
	public Page<DeliveryChallanHeader> getAllRecentTransaction(Long dcType, Long financialId, String search, Pageable pageable);
}
