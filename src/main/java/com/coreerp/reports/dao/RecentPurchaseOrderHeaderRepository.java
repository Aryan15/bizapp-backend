package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.coreerp.model.FinancialYear;
import com.coreerp.model.Party;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.model.TransactionType;
import org.springframework.data.repository.query.*;

public interface RecentPurchaseOrderHeaderRepository extends PagingAndSortingRepository<PurchaseOrderHeader, String> {
	public Page<PurchaseOrderHeader> findByPurchaseOrderTypeAndPurchaseOrderNumberContainsIgnoreCase(TransactionType purchaseOrderType, String name, Pageable pageable);
	
	public Page<PurchaseOrderHeader> findByPurchaseOrderTypeAndPurchaseOrderDate(TransactionType purchaseOrderType, Date quotationDate, Pageable pageable);
	
	public Page<PurchaseOrderHeader> findByPurchaseOrderTypeAndPartyIn(TransactionType purchaseOrderType, List<Party> parties,Pageable pageable);
	
	public Long countByPurchaseOrderTypeAndPurchaseOrderNumberContainsIgnoreCase(TransactionType purchaseOrderType, String name);
	
	public Long countByPurchaseOrderTypeAndPurchaseOrderDate(TransactionType purchaseOrderType, Date poDate); 
	
	public Long countByPurchaseOrderTypeAndPartyIn(TransactionType purchaseOrderType, List<Party> parties);
	
	public Page<PurchaseOrderHeader> findByPurchaseOrderTypeAndFinancialYear(TransactionType purchaseOrderType, FinancialYear financialYear, Pageable pageable);
	
	public Long countByPurchaseOrderType(TransactionType purchaseOrderType);


	@Query("select ph from PurchaseOrderHeader ph "
			+ " join ph.purchaseOrderType pType "
			+ " join ph.financialYear fY "
			+ " join ph.party p"
			+ " where 1 = 1 "
			+ " and pType.id = :poType "
			+ " and fY.id = :financialId "
			+ " and ((ph.purchaseOrderNumber like CONCAT('%',:search,'%') or :search is null)"
			+ " or (ph.partyName like  CONCAT('%',:search,'%') or :search is null)"
	        + " or  (date(ph.purchaseOrderDate) like CONCAT('%',:search,'%') or :search is null)"
			+ " or (p.partyCode like CONCAT('%',:search,'%') or :search is null))")
	public Page<PurchaseOrderHeader> getAllRecentTransaction(Long poType, Long financialId,String search,Pageable pageable);


}
