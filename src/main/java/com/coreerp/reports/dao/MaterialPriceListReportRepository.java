package com.coreerp.reports.dao;

import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.MaterialPriceList;
import com.coreerp.model.Party;
import com.coreerp.model.TransactionType;
import com.coreerp.reports.model.PartyPriceListReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MaterialPriceListReportRepository extends PagingAndSortingRepository<MaterialPriceList, Long> {
    @Query("SELECT NEW com.coreerp.reports.model.PartyPriceListReport(ppl.id"
            + ",ppl.sellingPrice AS partyPrice "
            + ",ppl.comments AS comments "
            + ",party.name AS partyName "
            + ",mat.id AS materialId "
            + ",mat.name AS materialName "
            + ",mat.price AS currentPrice "
            + ",mat.partNumber AS partNumber "
            + ",matType.name as materialTypeName)"
            + " FROM MaterialPriceList ppl "
            + " join ppl.party party"
            + " join ppl.material mat"
            + " join mat.materialType matType"
            + " where 1 = 1 "
            + " and (party.partyType.id = :partyTypeId or :partyTypeId is null) "
            + " and (party.id = :partyId or :partyId is null ) "
    )
    public Page<PartyPriceListReport> getPriceListReport(@Param("partyTypeId") Long partyTypeId
            , @Param("partyId") Long partyId
            , Pageable pageable);

//    @Query("select i from PartyPriceListReport i where i.party.id = ?1")
    List<MaterialPriceList> findByParty(Party party);


}
