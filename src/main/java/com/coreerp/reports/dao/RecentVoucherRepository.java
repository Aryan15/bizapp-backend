package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.coreerp.model.VoucherHeader;
import com.coreerp.model.Party;
import com.coreerp.model.TransactionType;

public interface RecentVoucherRepository extends PagingAndSortingRepository<VoucherHeader, String> {

	public Page<VoucherHeader> findByVoucherTypeAndVoucherNumberContainsIgnoreCase(TransactionType VoucherType, String name, Pageable pageable);
	
	public Page<VoucherHeader> findByVoucherTypeAndVoucherDate(TransactionType VoucherType, Date VoucherDate, Pageable pageable);
	
	public Page<VoucherHeader> findByVoucherTypeAndPaidToContainsIgnoreCase(TransactionType VoucherType, String name, Pageable pageable);
	
	public Long countByVoucherTypeAndVoucherNumberContainsIgnoreCase(TransactionType VoucherType, String name);
	
	public Long countByVoucherTypeAndVoucherDate(TransactionType VoucherType, Date VoucherDate); 
	
	public Long countByVoucherTypeAndPaidTo(TransactionType VoucherType, String name);
	
	public Page<VoucherHeader> findByVoucherType(TransactionType VoucherType, Pageable pageable);
	
	public Long countByVoucherType(TransactionType VoucherType);
}
