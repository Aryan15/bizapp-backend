package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.coreerp.model.InvoiceItem;
import com.coreerp.reports.model.InvoiceTaxReport;

public interface InvoiceTaxReportPageableRepository extends PagingAndSortingRepository<InvoiceItem, String> {

	@Query("SELECT NEW com.coreerp.reports.model.InvoiceTaxReport(ini.id "
			+ ",party.name AS partyName "
			+ ",party.gstNumber "
			+ ",inh.invoiceNumber "
			+ ",inh.invoiceDate "
			+ ",inh.totalTaxableAmount AS netAmount "
			+ ",ini.cgstTaxPercentage AS cgstTaxPercent "
			+ ",ini.cgstTaxAmount "
			+ ",ini.sgstTaxPercentage AS sgstTaxPercent "
			+ ",ini.sgstTaxAmount "
			+ ",CASE WHEN ini.igstTaxPercentage > 0 THEN ini.igstTaxPercentage ELSE null END AS igstTaxPercent "
			+ ",CASE WHEN ini.igstTaxAmount > 0 THEN ini.igstTaxAmount ELSE null END AS igstTaxAmount "
			+ ",inh.grandTotal"
			+ ",status.name AS statusName "
			+ ",party.partyCode AS partyCode ) "
			+ " FROM InvoiceHeader inh "
			+ " join inh.financialYear finY"
			+ " join inh.invoiceItems ini"
			+ " join inh.party party "
			+ " join ini.material material "
			+ " join inh.status status "
			+ " join party.partyType pType"
			+ " join inh.invoiceType inType"
			+ " where 1 = 1 "
			+ " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and (date(inh.invoiceDate) >= :fromDate or :fromDate is null) "
			+ " and (date(inh.invoiceDate) <= :toDate or :toDate is null) "
			+ " and (party.id = :partyId or :partyId is null ) "
			+ " and (material.id = :materialId or :materialId is null ) "
			+ " and pType.id in :partyTypeIds "
			+ " and inType.id = :transactionTypeId "
			+ " and status.name != 'Cancelled' "
			)
	Page<InvoiceTaxReport> getReport(@Param("financialYearId") Long financialYearId
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate
			, @Param("partyId") Long partyId
			, @Param("materialId") Long materialId
			, @Param("partyTypeIds") List<Long> partyTypeIds
			, @Param("transactionTypeId") Long transactionTypeId
			, Pageable pageable);
	
	
	@Query("SELECT NEW com.coreerp.reports.model.InvoiceTaxReport(ini.id "
			+ ",party.name AS partyName "
			+ ",party.gstNumber "
			+ ",inh.invoiceNumber "
			+ ",inh.invoiceDate "
			+ ",inh.totalTaxableAmount AS netAmount "
			+ ",inh.cgstTaxRate AS cgstTaxPercent "
			+ ",inh.cgstTaxAmount "
			+ ",inh.sgstTaxRate AS sgstTaxPercent "
			+ ",inh.sgstTaxAmount "
			+ ",CASE WHEN inh.igstTaxRate > 0 THEN inh.igstTaxRate ELSE null END AS igstTaxPercent "
			+ ",CASE WHEN inh.igstTaxAmount > 0 THEN inh.igstTaxAmount ELSE null END AS igstTaxAmount "
			+ ",inh.grandTotal"
			+ ",status.name AS statusName "
			+ ",party.partyCode AS partyCode ) "
			+ " FROM InvoiceHeader inh "
			+ " join inh.financialYear finY"
			+ " join inh.invoiceItems ini"
			+ " join inh.party party "
			+ " join ini.material material "
			+ " join inh.status status "
			+ " join party.partyType pType"
			+ " join inh.invoiceType inType"
			+ " where 1 = 1 "
			+ " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and (date(inh.invoiceDate) >= :fromDate or :fromDate is null) "
			+ " and (date(inh.invoiceDate) <= :toDate or :toDate is null) "
			+ " and (party.id = :partyId or :partyId is null ) "
			+ " and (material.id = :materialId or :materialId is null ) "
			+ " and pType.id in :partyTypeIds "
			+ " and inType.id = :transactionTypeId "
			+ " and status.name != 'Cancelled' "
			)
	Page<InvoiceTaxReport> getHeaderLevelTaxReport(@Param("financialYearId") Long financialYearId
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate
			, @Param("partyId") Long partyId
			, @Param("materialId") Long materialId
			, @Param("partyTypeIds") List<Long> partyTypeIds
			, @Param("transactionTypeId") Long transactionTypeId
			, Pageable pageable);
}
