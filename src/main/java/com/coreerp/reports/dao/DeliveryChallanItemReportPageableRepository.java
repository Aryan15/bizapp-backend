package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.coreerp.model.DeliveryChallanItem;
import com.coreerp.reports.model.DeliveryChallanItemModel;

public interface DeliveryChallanItemReportPageableRepository
		extends PagingAndSortingRepository<DeliveryChallanItem, String> {

	@Query(			
			" SELECT NEW com.coreerp.reports.model.DeliveryChallanItemModel(dch.id "
			+ " ,dch.deliveryChallanNumber "
			+ " ,dch.deliveryChallanDate "
			+ " ,party.name AS partyName "
			+ " ,material.name AS materialName "
					+ " ,dci.remarks "
			+ " ,dci.quantity "
			+ " ,uom.name AS uom "
			+ " ,material.hsnCode AS hsnOrSac "
			+ ",status.name AS statusName"
					+ " ,party.partyCode AS partyCode "
					+ " ,material.partNumber AS  partNumber ) "
			+ " FROM DeliveryChallanHeader dch "
			+ " join dch.financialYear finY"
			+ " join dch.deliveryChallanItems dci "
			+ " join dch.party party "
			+ " join dch.deliveryChallanType dcType"
			+ " join party.partyType pType "
			+ " join dci.material material "
			+ " join dch.status status "
			+ " join dci.unitOfMeasurement uom "
			+ " where 1 = 1 "
			+ " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and (date(dch.deliveryChallanDate) >= :fromDate or :fromDate is null) "
			+ " and (date(dch.deliveryChallanDate) <= :toDate or :toDate is null) "
			+ " and (party.id = :partyId or :partyId is null ) "
			+ " and dcType.id = :deliveryChallanTypeId "
			+ " and (material.id = :materialId or :materialId is null ) "
			+ " and pType.id in :partyTypeIds "
			)
	public Page<DeliveryChallanItemModel> getReport(@Param("deliveryChallanTypeId") Long deliveryChallanTypeId
			, @Param("financialYearId") Long financialYearId
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate
			, @Param("partyId") Long partyId
			, @Param("materialId") Long materialId
			, @Param("partyTypeIds") List<Long> partyTypeIds
			, Pageable pageable);	
}
