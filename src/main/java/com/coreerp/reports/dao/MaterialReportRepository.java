package com.coreerp.reports.dao;

import com.coreerp.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MaterialReportRepository extends PagingAndSortingRepository<Material, Long> {
	
	public Page<Material> findBySupplyTypeAndNameContainsIgnoreCase(SupplyType SupplyType, String name, Pageable pageable);
	
	public Page<Material> findBySupplyTypeAndPartNumberContainsIgnoreCase(SupplyType SupplyType, String name, Pageable pageable);
	
	public Page<Material> findBySupplyType(SupplyType SupplyType, Pageable pageable);

	public Page<Material> findBySupplyTypeAndNameContainsIgnoreCaseAndMaterialTypeNot(SupplyType SupplyType, String name, MaterialType materialType, Pageable pageable);

	public Page<Material> findBySupplyTypeAndPartNumberContainsIgnoreCaseAndMaterialTypeNot(SupplyType SupplyType, String name, MaterialType materialType,Pageable pageable);

	public Page<Material> findBySupplyTypeAndMaterialTypeNot(SupplyType SupplyType, MaterialType materialType, Pageable pageable);

}
