package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.coreerp.dto.InvoiceDTO;
import com.coreerp.dto.InvoiceItemDTO;
import com.coreerp.model.InvoiceItem;
import com.coreerp.reports.model.InvoiceItemReportModel;

public interface InvoiceItemReportPageableRepository extends PagingAndSortingRepository<InvoiceItem, String> {

	@Query("SELECT NEW com.coreerp.reports.model.InvoiceItemReportModel(ini.id "
			+ ",inh.invoiceNumber "
			+ ",inh.invoiceDate "
			+ ",party.name AS partyName"
			+ ", CASE WHEN(ini.amount =0) THEN inh.netAmount ELSE ini.amount END AS totalTaxableAmount "
			+ ", CASE WHEN(ini.cgstTaxPercentage =0) THEN inh.cgstTaxRate ELSE ini.cgstTaxPercentage END AS cgstTaxPercentage "
			+ ", CASE WHEN(ini.cgstTaxAmount =0) THEN inh.cgstTaxAmount ELSE ini.cgstTaxAmount END AS cgstTaxAmount "
			+ ", CASE WHEN(ini.sgstTaxPercentage =0) THEN inh.sgstTaxRate ELSE ini.sgstTaxPercentage END AS sgstTaxPercentage "
			+ ", CASE WHEN(ini.sgstTaxAmount =0) THEN inh.sgstTaxAmount ELSE ini.sgstTaxAmount END AS sgstTaxAmount "
			+ ", CASE WHEN(ini.igstTaxPercentage =0) THEN inh.igstTaxRate ELSE ini.igstTaxPercentage END AS igstTaxPercentage "
			+ ", CASE WHEN(ini.igstTaxAmount =0) THEN inh.igstTaxAmount ELSE ini.igstTaxAmount END AS igstTaxAmount "	
			+ ",0.0 AS advanceAmount "
			+ ",CASE WHEN(ini.amountAfterTax =0) THEN inh.grandTotal ELSE ini.amountAfterTax END AS grandTotal "
			+ ",mat.name AS description "
            + ",mat.partNumber AS partNumber "
			+ ",inh.id AS invoiceHeaderId "
			+ ",ini.quantity "
			+ ",ini.price "
			+ ", CASE WHEN(ini.discountAmount =0 ) THEN inh.discountAmount ELSE ini.discountAmount END AS discountAmount "
			+ ",status.name AS statusName  "
			+ ",uom.name AS uom "
			+ ",ini.amountAfterDiscount AS amountAfterDiscount "
			+ ",ini.inclusiveTax AS inclusiveTax "
			+ ",party.partyCode AS partyCode "
			+ ", CASE WHEN (:isHeaderLevel = 1 AND ini.inclusiveTax = 0) THEN ini.amountAfterDiscount ELSE ini.amount END AS taxableAmount "
			+ ",mat.hsnCode AS hsnCode"
			+ ",party.gstNumber AS gstNumber"
			+ ",inh.internalReferenceNumber AS internalReferenceNumber )"
			+ " FROM InvoiceHeader inh "
			+ " join inh.financialYear finY"
			+ " join inh.invoiceItems ini"
			+ " join inh.status status"
			+ " join ini.material mat"
			+ " join inh.party party "
			+ " join ini.unitOfMeasurement uom "
			+ " join party.partyType pType"
			+ " join inh.invoiceType inType"
			+ " where 1 = 1 "
			+ " and (finY.id = :financialYearId or :financialYearId is null) "
			//+ " and inh.invoiceDate BETWEEN :fromDate AND :toDate "
			+ " and (date(inh.invoiceDate) >= :fromDate or :fromDate is null) "
			+ " and (date(inh.invoiceDate) <= :toDate or :toDate is null) "
			+ " and (party.id = :partyId or :partyId is null ) "
			+ " and pType.id in :partyTypeIds "
			+ " and inType.id = :transactionTypeId "
			+ " and (mat.id = :materialId or :materialId is null  )"
			+ " and :isHeaderLevel in (1,0)"
			+ " and (inh.createdBy = :createdBy or :createdBy is null ) "
			)
	Page<InvoiceItemReportModel> getReport(@Param("financialYearId") Long financialYearId
					, @Param("fromDate") Date fromDate
					, @Param("toDate") Date toDate
					, @Param("partyId") Long partyId
					, @Param("partyTypeIds") List<Long> partyTypeIds
					, @Param("transactionTypeId") Long transactionTypeId
					, @Param("materialId") Long materialId
					, @Param("isHeaderLevel") Integer isHeaderLevel
					, @Param("createdBy") String createdBy
					, Pageable pageable);
	
	


	

}
