package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RecentInvoiceRepository extends PagingAndSortingRepository<InvoiceHeader, String> {

	public Page<InvoiceHeader> findByInvoiceTypeAndFinancialYearAndInvoiceNumberContainsIgnoreCase(TransactionType invoiceType, FinancialYear financialYear, String name, Pageable pageable);
	
	public Page<InvoiceHeader> findByInvoiceTypeAndFinancialYearAndInvoiceDateBetween(TransactionType invoiceType, FinancialYear financialYear, Date invoicefromDate, Date invoiceToDate, Pageable pageable);
	
	public Page<InvoiceHeader> findByInvoiceTypeAndFinancialYearAndPartyIn(TransactionType invoiceType, FinancialYear financialYear, List<Party> parties, Pageable pageable);

	public Page<InvoiceHeader> findByInvoiceTypeAndFinancialYear(TransactionType invoiceType, FinancialYear financialYear, Pageable pageable);


	@Query("select ih from InvoiceHeader ih "
			+ " join ih.invoiceType iType "
			+ " join ih.financialYear fY "
			+ " join ih.party p"
			+ " where 1 = 1 "
			+ " and iType.id = :invoiceType "
			+ " and fY.id = :financialId "
			+ " and ((ih.invoiceNumber like CONCAT('%',:search,'%') or :search is null)"
			+ " or (ih.partyName like  CONCAT('%',:search,'%') or :search is null)"
			+ " or  (date(ih.invoiceDate) like CONCAT('%',:search,'%') or :search is null)"
			+ " or (p.partyCode like CONCAT('%',:search,'%') or :search is null))")
	public Page<InvoiceHeader> getAllRecentTransaction(Long invoiceType, Long financialId, String search, Pageable pageable);

}
