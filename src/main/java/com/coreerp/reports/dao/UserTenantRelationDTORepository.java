package com.coreerp.reports.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.coreerp.dto.UserTenantRelationDTO;

@Repository
public class UserTenantRelationDTORepository {

	final static Logger log = LogManager.getLogger(UserTenantRelationDTORepository.class);
	
	@PersistenceContext
	private EntityManager manager;
	
	
	public UserTenantRelationDTO getUserTenantRelationDTO(String username) {
		TypedQuery<UserTenantRelationDTO> query = manager.createNamedQuery("qryUTRWithSubscrEndDate", UserTenantRelationDTO.class);
		query.setParameter("username", username);
				try {
					UserTenantRelationDTO userTenantRelationDTO = query.getResultList().get(0);
					log.info("Returning " + userTenantRelationDTO);

					return userTenantRelationDTO;
				}catch (Exception e){
					log.info("error in getUserTenantRelationDTO "+username);
				}
				return null;
	}
	
}
