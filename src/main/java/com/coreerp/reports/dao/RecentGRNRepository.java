package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RecentGRNRepository extends PagingAndSortingRepository<GRNHeader, String> {

	public Page<GRNHeader> findByGrnTypeAndGrnNumberContainsIgnoreCase(TransactionType grnType, String name, Pageable pageable);
	
	public Page<GRNHeader> findByGrnTypeAndGrnDateBetween(TransactionType grnType, Date grnDate, Date searchEndDate, Pageable pageable);
	
	public Page<GRNHeader> findByGrnTypeAndSupplierIn(TransactionType grnType, List<Party> parties,Pageable pageable);
	
	public Long countByGrnTypeAndGrnNumberContainsIgnoreCase(TransactionType grnType, String name);
	
	public Long countByGrnTypeAndGrnDate(TransactionType grnType, Date grnDate); 
	
	public Long countByGrnTypeAndSupplierIn(TransactionType grnType, List<Party> parties);
	
	
	public Page<GRNHeader> findByGrnTypeAndFinancialYear(TransactionType grnType, FinancialYear financialYear, Pageable pageable);
	
	public Long countByGrnType(TransactionType grnType);



	@Query("select gh from GRNHeader gh "
			+ " join gh.grnType gType "
			+ " join gh.financialYear fY "
			+ " join gh.supplier p"
			+ " where 1 = 1 "
			+ " and gType.id = :grnType "
			+ " and fY.id = :financialId "
			+ " and ((gh.grnNumber like CONCAT('%',:search,'%') or :search is null)"
			+ " or (gh.partyName like  CONCAT('%',:search,'%') or :search is null)"
			+ " or  (date(gh.grnDate) like CONCAT('%',:search,'%') or :search is null)"
			+ " or (p.partyCode like CONCAT('%',:search,'%') or :search is null))")
	public Page<GRNHeader> getAllRecentTransaction(Long grnType, Long financialId, String search, Pageable pageable);

}
