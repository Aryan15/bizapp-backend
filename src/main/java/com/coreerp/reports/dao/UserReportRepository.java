package com.coreerp.reports.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.coreerp.model.Role;
import com.coreerp.model.User;

public interface UserReportRepository extends PagingAndSortingRepository<User, Long> {

	
	public Page<User> findDistinctByRolesNotIn(List<Role> roles, Pageable pageable);
	
	public Page<User> findByNameContainsIgnoreCase(String name, Pageable pageable);
	
	public Long countByNameContainsIgnoreCase(String name);
	
	public Page<User> findByLastNameContainsIgnoreCase(String lastName, Pageable pageable);
	
	public Long countByLastNameContainsIgnoreCase(String LastName);
	
	
 
}
