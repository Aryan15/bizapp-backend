package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.coreerp.model.InvoiceHeader;
import com.coreerp.reports.model.DashboardTransactionSummary;

public interface DashboardTransactionSummaryRepository extends JpaRepository<InvoiceHeader, String>{

	@Query(			
            "SELECT NEW com.coreerp.reports.model.DashboardTransactionSummary("
            + " MONTH(inh.invoiceDate) AS monthNumber "
            + " ,MONTHNAME(inh.invoiceDate) AS month "

            + " ,tt.name AS transactionName "
            + " ,sum(inh.grandTotal) AS transactionAmount "
            + " ,count(inh.id) AS transactionCount "
            + " ,sum(inh.dueAmount) AS dueAmount "
					+ " ,YEAR(inh.invoiceDate) AS financialYear "
            + " ) "
            + " FROM InvoiceHeader inh "
            + " join inh.invoiceType tt "
            + " join inh.financialYear finY "
            + " join inh.status st "
            + " where 1 = 1 "
            + " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and (date(inh.invoiceDate) >= :fromDate or :fromDate is null) "
			+ " and (date(inh.invoiceDate) <= :toDate or :toDate is null) "
			+ " and tt.id in :transactionTypeIds "
			+ " and st.name not in :statuses "
			+ " group by MONTH(inh.invoiceDate), MONTHNAME(inh.invoiceDate),YEAR(inh.invoiceDate), tt.name "
			+ " order by YEAR(inh.invoiceDate), MONTH(inh.invoiceDate)  ")
	public List<DashboardTransactionSummary> getInvoiceAndPendingPayment(
			  @Param("financialYearId") Long financialYearId
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate
			, @Param("statuses") List<String> statuses
			, @Param("transactionTypeIds") List<Long> transactionTypeIds);
	
	
	@Query(			
            "SELECT NEW com.coreerp.reports.model.DashboardTransactionSummary("
            + " MONTH(prh.payReferenceDate) as monthNumber "
            + " ,MONTHNAME(prh.payReferenceDate) as month "
            + " ,tt.name AS transactionName "
            + " ,sum(prh.paymentAmount) AS transactionAmount "
            + " ,count(prh.id) AS transactionCount "
            + " ,0.0 AS dueAmount "
					+ " ,YEAR(prh.payReferenceDate) AS financialYear "
            + " ) "
            + " FROM PayableReceivableHeader prh "
            + " join prh.paymentMethod pm"
            + " join prh.status st"
            + " join prh.transactionType tt "
            + " join prh.financialYear finY "
            + " where 1 = 1 "
            + " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and tt.id in :transactionTypeIds "
			+ " and prh.paymentDocumentDate > CURRENT_DATE"
			+ " and st.name = :status"
			+ " and pm.name = :paymentMethodName "
			+ " group by MONTH(prh.payReferenceDate), MONTHNAME(prh.payReferenceDate),YEAR(prh.payReferenceDate), tt.name "
			+ " order by YEAR(prh.payReferenceDate), MONTH(prh.payReferenceDate) ")
	public List<DashboardTransactionSummary> getPostDatedCheques(
			  @Param("financialYearId") Long financialYearId
			, @Param("transactionTypeIds") List<Long> transactionTypeIds
			, @Param("status") String status
			, @Param("paymentMethodName") String paymentMethodName);
	
	
	@Query(			
            "SELECT NEW com.coreerp.reports.model.DashboardTransactionSummary("
            + " MONTH(qh.quotationDate) AS monthNumber "
            + " ,MONTHNAME(qh.quotationDate) AS month "
            + " ,tt.name AS transactionName "
            + " ,sum(qh.grandTotal) AS transactionAmount "
            + " ,count(qh.id) AS transactionCount "
            + " ,0.0 AS dueAmount "
					+ " ,YEAR(qh.quotationDate) AS financialYear "
            + " ) "
            + " FROM QuotationHeader qh "
            + " join qh.status st"
            + " join qh.quotationType tt "
            + " join qh.financialYear finY "
            + " where 1 = 1 "
            + " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and tt.id in :transactionTypeIds "
			+ " and (date(qh.quotationDate) >= :fromDate or :fromDate is null) "
			+ " and (date(qh.quotationDate) <= :toDate or :toDate is null) "
			+ " and st.name in :statuses"
			+ " group by MONTH(qh.quotationDate), MONTHNAME(qh.quotationDate),YEAR(qh.quotationDate), tt.name "
			+ " order by YEAR(qh.quotationDate), MONTH(qh.quotationDate) ")
	public List<DashboardTransactionSummary> getOpenQuotations(
			  @Param("financialYearId") Long financialYearId
			, @Param("transactionTypeIds") List<Long> transactionTypeIds
			, @Param("statuses") List<String> statuses
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate);
	
	@Query(			
            "SELECT NEW com.coreerp.reports.model.DashboardTransactionSummary("
            + " MONTH(ph.purchaseOrderDate) AS monthName "
            + " ,MONTHNAME(ph.purchaseOrderDate) AS month "
            + " ,tt.name AS transactionName "
            + " ,sum(ph.grandTotal) AS transactionAmount "
            + " ,count(ph.id) AS transactionCount "
            + " ,0.0 AS dueAmount "
					+ " ,YEAR(ph.purchaseOrderDate) AS financialYear "
            + " ) "
            + " FROM PurchaseOrderHeader ph "
            + " join ph.status st"
            + " join ph.purchaseOrderType tt "
            + " join ph.financialYear finY "
            + " where 1 = 1 "
            + " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and tt.id in :transactionTypeIds "
			+ " and (date(ph.purchaseOrderDate) >= :fromDate or :fromDate is null) "
			+ " and (date(ph.purchaseOrderDate) <= :toDate or :toDate is null) "
			+ " and st.name in :statuses"
			+ " group by MONTH(ph.purchaseOrderDate), MONTHNAME(ph.purchaseOrderDate),YEAR(ph.purchaseOrderDate), tt.name"
			+ " order by YEAR(ph.purchaseOrderDate), MONTH(ph.purchaseOrderDate) ")
	public List<DashboardTransactionSummary> getOpenPurchaseOrders(
			  @Param("financialYearId") Long financialYearId
			, @Param("transactionTypeIds") List<Long> transactionTypeIds
			, @Param("statuses") List<String> statuses
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate);
	
	
	
	@Query(			
            "SELECT NEW com.coreerp.reports.model.DashboardTransactionSummary("
            + " MONTH(prh.payReferenceDate) AS monthName "
            + " ,MONTHNAME(prh.payReferenceDate) AS month "
            + " ,tt.name AS transactionName "
            + " ,sum(prh.paymentAmount) AS transactionAmount "
            + " ,count(prh.id) AS transactionCount "
            + " ,0.0 AS dueAmount "
					+ " ,YEAR(prh.payReferenceDate) AS financialYear "
            + " ) "
            + " FROM PayableReceivableHeader prh "
            + " join prh.transactionType tt "
            + " join prh.financialYear finY "
            + " join prh.status st "
            + " where 1 = 1 "
            + " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and (date(prh.payReferenceDate) >= :fromDate or :fromDate is null) "
			+ " and (date(prh.payReferenceDate) <= :toDate or :toDate is null) "
			+ " and tt.id in :transactionTypeIds "
			+ " and st.name not in :statuses " +
			" and prh.id not in (" +
			"   select prh1.id from PayableReceivableHeader prh1" +
			"   join prh1.payableReceivableItems pri1" +
			"   join pri1.invoiceHeader invh" +
			"   join invh.invoiceType invt" +
			"   where invt.id in (:sourceTransactionIds)) "
			+ " group by MONTH(prh.payReferenceDate), MONTHNAME(prh.payReferenceDate),YEAR(prh.payReferenceDate), tt.name "
			+ " order by YEAR(prh.payReferenceDate), MONTH(prh.payReferenceDate) ")
	public List<DashboardTransactionSummary> getPayReceive(
			  @Param("financialYearId") Long financialYearId
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate
			, @Param("statuses") List<String> statuses
			, @Param("transactionTypeIds") List<Long> transactionTypeIds
			, @Param("sourceTransactionIds") List<Long> sourceTransactionIds );
}
