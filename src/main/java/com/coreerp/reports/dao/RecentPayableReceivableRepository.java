package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RecentPayableReceivableRepository extends PagingAndSortingRepository<PayableReceivableHeader, String> {

	public Page<PayableReceivableHeader> findByTransactionTypeAndFinancialYearAndPayReferenceNumberContainsIgnoreCase(TransactionType transactionType,FinancialYear financialYear, String name, Pageable pageable);
	
	public Page<PayableReceivableHeader> findByTransactionTypeAndFinancialYearAndPayReferenceDate(TransactionType transactionType,FinancialYear financialYear, Date payReferenceDate, Pageable pageable);
	
	public Page<PayableReceivableHeader> findByTransactionTypeAndFinancialYearAndPartyIn(TransactionType transactionType,FinancialYear financialYear, List<Party> parties,Pageable pageable);
	
	public Long countByTransactionTypeAndPayReferenceNumberContainsIgnoreCase(TransactionType transactionType, String name);
	
	public Long countByTransactionTypeAndPayReferenceDate(TransactionType transactionType, Date payReferenceDate); 
	
	public Long countByTransactionTypeAndPartyIn(TransactionType transactionType, List<Party> parties);
	
	
	public Page<PayableReceivableHeader> findByTransactionTypeAndFinancialYear(TransactionType transactionType,FinancialYear financialYear, Pageable pageable);
	
	public Long countByTransactionType(TransactionType transactionType);


	@Query("select ph from PayableReceivableHeader ph "
			+ " join ph.transactionType pType "
			+ " join ph.financialYear fY "
			+ " join ph.party p"
			+ " where 1 = 1 "
			+ " and pType.id = :payType "
			+ " and fY.id = :financialId "
			+ " and ((ph.payReferenceNumber like CONCAT('%',:search,'%') or :search is null)"
			+ " or (ph.partyName like  CONCAT('%',:search,'%') or :search is null)"
			+ " or  (date(ph.payReferenceDate) like CONCAT('%',:search,'%') or :search is null)"
			+ " or (p.partyCode like CONCAT('%',:search,'%') or :search is null))")
	public Page<PayableReceivableHeader> getAllRecentTransaction(Long payType, Long financialId, String search, Pageable pageable);



}
