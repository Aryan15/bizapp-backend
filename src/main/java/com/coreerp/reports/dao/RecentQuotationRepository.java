package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import com.coreerp.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RecentQuotationRepository extends PagingAndSortingRepository<QuotationHeader, String> {

	public Page<QuotationHeader> findByQuotationTypeAndFinancialYearAndQuotationNumberContainsIgnoreCase(TransactionType quotationType, FinancialYear financialYear, String name, Pageable pageable);
	
	public Page<QuotationHeader> findByQuotationTypeAndFinancialYearAndQuotationDateBetween(TransactionType quotationType, FinancialYear financialYear, Date quotationStartDate, Date quotationEndDate, Pageable pageable);
	
	public Page<QuotationHeader> findByQuotationTypeAndFinancialYearAndPartyIn(TransactionType quotationType, FinancialYear financialYear, List<Party> parties,Pageable pageable);

	public Page<QuotationHeader> findByQuotationTypeAndFinancialYear(TransactionType quotationType, FinancialYear financialYear, Pageable pageable);

	@Query("select qh from QuotationHeader qh "
			+ " join qh.quotationType qType "
			+ " join qh.financialYear fY "
			+ " join qh.party p"
			+ " where 1 = 1 "
			+ " and qType.id = :qType "
			+ " and fY.id = :financialId "
			+ " and ((qh.quotationNumber like CONCAT('%',:search,'%') or :search is null)"
			+ " or (qh.partyName like  CONCAT('%',:search,'%') or :search is null)"
			+ " or  (date(qh.quotationDate) like CONCAT('%',:search,'%') or :search is null)"
			+ " or (p.partyCode like CONCAT('%',:search,'%') or :search is null))")
	public Page<QuotationHeader> getAllRecentTransaction(Long qType, Long financialId, String search, Pageable pageable);


}
