package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.coreerp.model.InvoiceHeader;
import com.coreerp.reports.model.BalanceReport;
import com.coreerp.reports.model.InvoiceReport;

public interface InvoiceReportRepository extends PagingAndSortingRepository<InvoiceHeader, String> {
	
	
	@Query(
			
			" SELECT NEW com.coreerp.reports.model.InvoiceReport(inh.id "
			+ ",inh.invoiceNumber "
			+ ",inh.invoiceDate "
			+ ",inh.sourceInvoiceNumber "
			+ ",inh.sourceInvoiceDate "
			+ ",party.name AS partyName"
			+ ",inh.totalTaxableAmount "
			+ ",inh.cgstTaxRate "
			+ ",inh.cgstTaxAmount "
			+ ",inh.sgstTaxRate "
			+ ",inh.sgstTaxAmount "
			+ ",inh.igstTaxRate "
			+ ",inh.igstTaxAmount "
			+ ",inh.discountAmount "
			+ ",inh.advanceAmount "
			+ ",inh.grandTotal"
			+ ",status.name AS statusName "
			+ ",inh.netAmount AS amount "
			+ ",inh.inclusiveTax AS inclusiveTax "
					+",party.partyCode AS partyCode"
					+",inh.tcsPercentage AS tcsPercentage "
					+",inh.tcsAmount AS tcsAmount "

					+ ",party.gstNumber AS gstNumber )"
			+ " FROM InvoiceHeader inh "
			+ " join inh.financialYear finY"
			+ " join inh.party party "
			+ " join inh.status status "
			+ " join party.partyType pType"
			+ " join inh.invoiceType inType"
			+ " where 1 = 1 "
			+ " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and (date(inh.invoiceDate) >= :fromDate or :fromDate is null) "
			+ " and (date(inh.invoiceDate) <= :toDate or :toDate is null) "
			+ " and (party.id = :partyId or :partyId is null ) "
			+ " and pType.id in :partyTypeIds "
			+ " and inType.id = :transactionTypeId "
			+ " and (inh.createdBy = :createdBy or :createdBy is null) "
			)
	public Page<InvoiceReport> getReport(@Param("financialYearId") Long financialYearId
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate
			, @Param("partyId") Long partyId
			, @Param("partyTypeIds") List<Long> partyTypeIds
			, @Param("transactionTypeId") Long transactionTypeId
			, @Param("createdBy") String createdBy
			, Pageable pageable);	

	
	@Query("SELECT NEW com.coreerp.reports.model.BalanceReport(inh.id "
		    + ",inh.invoiceNumber "			
			+ ",inh.invoiceDate "
			+ ",inh.grandTotal AS invoiceAmount"
			+ ",inh.grandTotal - inh.dueAmount AS paidAmount "
			+ ",inh.dueAmount "
			+ ",party.name AS partyName "					
			+ ",party.gstNumber AS gstin "		
			+ ",status.name AS statusName "
			+ ",party.partyCode AS partyCode ) "
			+ " FROM InvoiceHeader inh "
			+ " join inh.financialYear finY"
			+ " join inh.party party "
			+ " join party.partyType pType"
			+ " join inh.invoiceType inType"
			+ " join inh.status status "
			+ " where 1 = 1 "
			+ " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and (inh.invoiceDate >= :fromDate or :fromDate is null) "
			+ " and (inh.invoiceDate <= :toDate or :toDate is null) "
			+ " and (party.id = :partyId or :partyId is null ) "
			+ " and pType.id in :partyTypeIds "
			+ " and (inType.id = :transactionTypeId or :transactionTypeId is null) "
			+ " and (status.id = :statusId or :statusId is null) "
			+ " and (inh.createdBy = :createdBy or :createdBy is null) "
			+ " and (status.name != :Cancelled)"
			)
	Page<BalanceReport> getBalanceReport(@Param("financialYearId") Long financialYearId
					, @Param("fromDate") Date fromDate
					, @Param("toDate") Date toDate
					, @Param("partyId") Long partyId
					, @Param("partyTypeIds") List<Long> partyTypeIds
					, @Param("transactionTypeId") Long transactionTypeId
			        , @Param("statusId") Long statusId
					, @Param("createdBy") String createdBy
			        , @Param("Cancelled") String Cancelled
			        , Pageable pageable);

}
