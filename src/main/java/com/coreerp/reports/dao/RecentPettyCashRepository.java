package com.coreerp.reports.dao;

import com.coreerp.model.PettyCashHeader;

import com.coreerp.model.TransactionType;
import com.coreerp.model.VoucherHeader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;

public interface RecentPettyCashRepository extends PagingAndSortingRepository<PettyCashHeader, String> {

    public Page<PettyCashHeader> findByPettyCashTypeId(TransactionType pettyCashType, Pageable pageable);
    public Page<PettyCashHeader> findByPettyCashTypeIdAndPettyCashNumberContainsIgnoreCase(TransactionType pettyCashType, String name, Pageable pageable);
    public Page<PettyCashHeader> findByPettyCashTypeIdAndPettyCashDate(TransactionType pettyCashType, Date pettyCashDate, Pageable pageable);
}
