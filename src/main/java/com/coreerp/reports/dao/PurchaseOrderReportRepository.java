package com.coreerp.reports.dao;

import java.util.Date;
import java.util.List;

import com.coreerp.reports.model.BalanceReport;
import com.coreerp.reports.model.PoBalanceReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.reports.model.PurchaseOrderReport;

public interface PurchaseOrderReportRepository extends PagingAndSortingRepository<PurchaseOrderHeader, String> {

	@Query(
			" SELECT NEW com.coreerp.reports.model.PurchaseOrderReport(poh.id "
			+ ", poh.purchaseOrderNumber "
			+ ", poh.purchaseOrderDate "
			+ ", party.name AS customerName "
			+ ", mat.name AS materialName "
					+", mat.partNumber AS partNumber "
					+ ", poi.quantity "
			+ ", uom.name AS unitOfMeasurementName "
			+ ", poi.price "
			+ ", poi.amount "
			+ ", CASE WHEN(poi.amountAfterTax =0) THEN poh.netAmount ELSE poi.amountAfterTax END AS totalAmount "
			+ ", CASE WHEN(poi.discountAmount =0) THEN poh.discountAmount ELSE poi.discountAmount END AS discountAmount "			
			+ ", CASE WHEN(poi.cgstTaxPercentage =0) THEN poh.cgstTaxRate ELSE poi.cgstTaxPercentage END AS cgstTaxPercentage "
			+ ", CASE WHEN(poi.cgstTaxAmount =0) THEN poh.cgstTaxAmount ELSE poi.cgstTaxAmount END AS cgstTaxAmount "
			+ ", CASE WHEN(poi.sgstTaxPercentage =0) THEN poh.sgstTaxRate ELSE poi.sgstTaxPercentage END AS sgstTaxPercentage "
			+ ", CASE WHEN(poi.sgstTaxAmount =0) THEN poh.sgstTaxAmount ELSE poi.sgstTaxAmount END AS sgstTaxAmount "
			+ ", CASE WHEN(poi.igstTaxPercentage =0) THEN poh.igstTaxRate ELSE poi.igstTaxPercentage END AS igstTaxPercentage "
			+ ", CASE WHEN(poi.igstTaxAmount =0) THEN poh.igstTaxAmount ELSE poi.igstTaxAmount END AS igstTaxAmount  "			
			+ ", status.name AS statusName "
			+ ", poh.internalReferenceNumber"
					+ ", poh.remarks"
					+", party.partyCode AS partyCode)"

			+ " FROM PurchaseOrderHeader poh "
			+ " join poh.financialYear finY "
			+ " join poh.party party "
			+ " join poh.purchaseOrderItems poi "
			+ " join poi.unitOfMeasurement uom "
			+ " join poh.status status "
			+ " join poi.material mat "
			+ " join party.partyType pType "
			+ " join poh.purchaseOrderType poType "
			+ " where 1 = 1 "
			+ " and (finY.id = :financialYearId or :financialYearId is null) "
			+ " and (date(poh.purchaseOrderDate) >= :fromDate or :fromDate is null) "
			+ " and (date(poh.purchaseOrderDate) <= :toDate or :toDate is null) "
			+ " and (party.id = :partyId or :partyId is null ) "
			+ " and (mat.id = :materialId or :materialId is null  ) "
			+ " and pType.id in :partyTypeIds "
			+ " and poType.id = :transactionTypeId "
			)
	public Page<PurchaseOrderReport> getReport(@Param("financialYearId") Long financialYearId
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate
			, @Param("partyId") Long partyId
			, @Param("materialId") Long materialId
			, @Param("partyTypeIds") List<Long> partyTypeIds
			, @Param("transactionTypeId") Long transactionTypeId
			, Pageable pageable);

//	@Query(value = "SELECT NEW com.coreerp.reports.model.PoBalanceReport(tr_purchase_order_header.po_number AS poNumber"
//			+ " ,tr_purchase_order_header.po_date AS purchaseOrderDate"
//			+ " ,poi.quantity AS poQuantity"
//			+ " ,poi.price AS price"
//			+ " ,poi.dc_balance_quantity AS dcPendingQuantity"
//			+ " ,poi.invoice_balance_quantity AS invoicePendingQuantity"
//			+ " ,dh.dc_number AS dcNumber"
//			+ " ,dh.dc_date AS dcDate"
//			+ " ,di.invoice_balance_quantity AS jwInDCInvoicePendingQuantity"
//			+ " ,di.quantity AS jwInDCQuantity"
//			+ " ,di.dc_balance_quantity AS jwInDCPendingQuantity"
//			+ " ,di.incoming_quantity AS jwInDCIncomingQuantity"
//			+ " ,jwoutdc.dc_number AS jwOutdcNumber"
//			+ " ,jwoutdc.dc_date AS jwOutdcDate"
//			+ " ,jwoutdci.invoice_balance_quantity AS jwOutDcInvoicePendingQuantity"
//			+ " ,jwoutdci.quantity AS jwOutDcQuantity"
//			+ " ,jwoutdci.dc_balance_quantity AS jwOutDCPendingQuantity"
//			+ " ,jwoutdci.incoming_quantity AS jwOutDCIncomingQuantity"
//			+ " ,party.name AS partyName"
//			+ " ,status.name AS statusName"
//			+ " ,mat.name AS materialName"
//			+ " ,mat.number AS partNumber"
//			+ " ,party.party_code AS partyCode)"
//			+ " FROM tr_purchase_order_header "
//			+ " INNER JOIN tr_purchase_order_item poi"
//			+ " ON(tr_purchase_order_header.id = poi.po_header_id)"
//			+ " LEFT OUTER JOIN tr_delivery_challan_item di"
//			+ " ON(di.po_item_id = poi.id)"
//			+ " LEFT OUTER JOIN tr_delivery_challan_header dh"
//			+ " ON(dh.id = di.dc_header_id)"
//			+ " LEFT OUTER JOIN cd_transaction_type tt"
//			+ " ON(tt.id = dh.dc_type AND tt.name ='Jobwork In DC')"
//			+ " LEFT OUTER JOIN tr_delivery_challan_item jwoutdci"
//			+ " ON(jwoutdci.src_dc_item_id = di.id)"
//			+ " LEFT OUTER JOIN tr_delivery_challan_header jwoutdc"
//			+ " ON(jwoutdc.id = jwoutdci.dc_header_id)"
//			+ " JOIN ma_financial_year finY"
//			+ " ON (tr_purchase_order_header.financial_year_id = finY.id)"
//			+ " JOIN ma_party party"
//			+ " ON (tr_purchase_order_header.party_id = party.id)"
//			+ " JOIN ma_material mat "
//			+ " ON (poi.material_id = mat.id)"
//			+ " JOIN ma_party_type pType "
//			+ " ON (party.party_type_id = pType.id) "
//			+ " JOIN cd_transaction_type poType"
//			+ " ON (tr_purchase_order_header.po_type = poType.id)"
//			+ " JOIN cd_status STATUS"
//			+ " ON (tr_purchase_order_header.status_id = STATUS.id)"
//			+ " WHERE poi.dc_balance_quantity  > 0"
//			+ " OR di.dc_balance_quantity  > 0 "
//			+ " OR jwoutdci.invoice_balance_quantity > 0 "
//			+ " and (finY.id = ?1 or ?1 is null) "
//			+ " and (tr_purchase_order_header.po_date >= ?2 or ?2 is null) "
//			+ " and (tr_purchase_order_header.po_date <= ?3 or ?3 is null) "
//			+ " and (party.id = ?4 or ?4 is null) "
//			+ " and pType.id IN (?5) "
//			+ " and (poType.id = ?6 or ?6 is null) "
//			+ " and (status.id = ?7 or ?7 is null) " , nativeQuery = true)
//	Page<PoBalanceReport> getBalanceReport(@Param("financialYearId") Long financialYearId
//			, @Param("fromDate") Date fromDate
//			, @Param("toDate") Date toDate
//			, @Param("partyId") Long partyId
//			, @Param("partyTypeIds") List<Long> partyTypeIds
//			, @Param("transactionTypeId") Long transactionTypeId
//			, @Param("statusId") Long statusId
//			,Pageable pageable);


	@Query(
			" SELECT NEW com.coreerp.reports.model.PoBalanceReport(MAX(poh.purchaseOrderNumber) AS poNumber"
					+ ", MAX(poh.purchaseOrderDate) AS purchaseOrderDate"
					+ ", MAX(poi.quantity) AS quantity"
					+ ", MAX(poi.price) AS price"
					+ ", MAX(poi.dcBalanceQuantity) AS dcBalanceQuantity "
					+ ", MAX(poi.invoiceBalanceQuantity) AS invoiceBalanceQuantity "
					+ ", MAX(dh.deliveryChallanNumber) AS dcNumber"
					+ ", MAX(dh.deliveryChallanDate) AS jwindcDate"
					+ ", SUM(di.invoiceBalanceQuantity) AS jwinvoiceBalanceQuantity"
					+ ", SUM(di.quantity) AS jwindcqty"
					+ ", SUM(di.dcBalanceQuantity) as jwindcdcbalqty"
					+ ", SUM(di.incomingQuantity) AS jwindcdcincomingqty"
					+ ", MAX(jwoutdc.deliveryChallanNumber) AS jwoutdcnumber"
					+ ", MAX(jwoutdc.deliveryChallanDate) AS jwoutdcdate"
					+ ", SUM(jwoutdci.invoiceBalanceQuantity) AS jwoutinvoiceBalanceQuantity"
					+ ", SUM(jwoutdci.quantity) AS jwoutquantity"
					+ ", SUM(jwoutdci.dcBalanceQuantity) AS jwoutdcBalanceQuantity"
					+ ", SUM(jwoutdci.incomingQuantity) AS jwoutdcincomingqty"
					+ ", MAX(party.name) AS partyName"
					+ ", MAX(STATUS.name) AS statusName "
					+ ", MAX(mat.name) AS materialName "
					+ ", MAX(mat.partNumber) AS partNumber "
					+ ", MAX(party.partyCode) AS partyCode "
                    + ", MAX(poType.id) AS poTypeId  "
					+ ", MAX(poi.id) AS poItemId "
                    + ", tt.name AS dcTranType "
					+ ", tt.id AS dcTransactionTypeId "
					+ ", MAX(poh.isOpenEnded) AS isOpenEnded "
					+ ", SUM(gi.balanceQuantity) AS inGrnBalance "
					+ ", Max(gi.deliveryChallanQuantity) AS grnDcQuantity "
					+ ", SUM(gi.acceptedQuantity) AS grnAcceptedQuantity "
                    + ", SUM(gi.rejectedQuantity) AS rejectedQuantity "
					+ ", MAX(gh.grnNumber) AS grnNumber "
					+ ", MAX(di.grnBalanceQuantity) AS dcGrnBalanceQuantity "
					+ ", MAX(dcStatus.name) as dcstatusId "
					+ ", MAX(poh.internalReferenceNumber) AS internelRefNumber )"
                    + " FROM PurchaseOrderHeader  poh "
					+ " INNER JOIN PurchaseOrderItem poi "
					+ " ON (poh.id = poi.purchaseOrderHeader.id)"
					+ " LEFT OUTER JOIN DeliveryChallanItem di"
					+ " ON (di.purchaseOrderItem.id = poi.id)"
					+ " LEFT OUTER JOIN DeliveryChallanHeader dh"
					+ " ON (dh.id = di.deliveryChallanHeader.id)"
					+ " LEFT OUTER JOIN TransactionType tt"
					+ " ON (tt.id = dh.deliveryChallanType.id AND (tt.name = 'Jobwork In DC' or tt.name='Subcontracting Out DC'))"
					+ " LEFT OUTER JOIN DeliveryChallanItem jwoutdci "
					+ " ON (jwoutdci.sourceDeliveryChallanItem.id = di.id)"
					+ " LEFT OUTER JOIN DeliveryChallanHeader jwoutdc "
					+ " ON (jwoutdc.id = jwoutdci.deliveryChallanHeader.id)"
					+ " JOIN FinancialYear finY "
					+ " ON (poh.financialYear.id = finY.id)"
					+ " JOIN Party party "
					+ " ON (poh.party.id = party.id)"
					+ " JOIN Material mat "
					+ " ON (poi.material.id = mat.id)"
					+ " JOIN PartyType pType "
					+ " ON (party.partyType.id = pType.id) "
					+ " JOIN TransactionType poType"
					+ " ON (poh.purchaseOrderType.id = poType.id) "
					+ " JOIN Status STATUS"
					+ " ON (poh.status.id = STATUS.id)"
					+  " LEFT OUTER JOIN GRNItem gi "
                     + " ON (gi.deliveryChallanItem.id = di.id)"
                      + " LEFT OUTER JOIN GRNHeader gh "
                      + " ON (gh.id = gi.grnHeader.id) "
					+ " LEFT OUTER JOIN Status dcStatus"
					+ " ON (dh.status.id = dcStatus.id)"
					    + " where 1 = 1 "
					+ " and (finY.id = :financialYearId or :financialYearId is null) "
					+ " and (date(poh.purchaseOrderDate) >= :fromDate or :fromDate is null) "
					+ " and (date(poh.purchaseOrderDate) <= :toDate or :toDate is null) "
					+ " and (party.id = :partyId or :partyId is null) "
					+ " and pType.id in :partyTypeIds "
					+ " and poType.id = :transactionTypeId "
                    + " and ((tt.id =:dcTransaction or :dcTransaction is null) or STATUS.id in :statusId )"
					+ " GROUP BY poi.id,tt.name,tt.id "
	)
	Page<PoBalanceReport> getBalanceReport(@Param("financialYearId") Long financialYearId
			, @Param("fromDate") Date fromDate
			, @Param("toDate") Date toDate
			, @Param("partyId") Long partyId
			, @Param("partyTypeIds") List<Long> partyTypeIds
			, @Param("transactionTypeId") Long transactionTypeId
			, @Param("statusId") List<Long> statusId
			, @Param("dcTransaction") Long dcTransaction
			,Pageable pageable);



















	/*@Query(value = "SELECT poh.po_number AS poNumber "
			+ " ,poh.po_date AS purchaseOrderDate"
			+ " ,poi.quantity AS poQuantity"
			+ " ,poi.price AS price"
			+ " ,poi.dc_balance_quantity AS dcPendingQuantity"
			+ " ,poi.invoice_balance_quantity AS invoicePendingQuantity"
			+ " ,dh.dc_number AS dcNumber"
			+ " ,dh.dc_date AS dcDate"
			+ " ,di.invoice_balance_quantity AS jwInDCInvoicePendingQuantity"
			+ " ,di.quantity AS jwInDCQuantity"
			+ " ,di.dc_balance_quantity AS jwInDCPendingQuantity"
			+ " ,di.incoming_quantity AS jwInDCIncomingQuantity"
			+ " ,jwoutdc.dc_number AS jwOutdcNumber"
			+ " ,jwoutdc.dc_date AS jwOutdcDate"
			+ " ,jwoutdci.invoice_balance_quantity AS jwOutDcInvoicePendingQuantity"
			+ " ,jwoutdci.quantity AS jwOutDcQuantity"
			+ " ,jwoutdci.dc_balance_quantity AS jwOutDCPendingQuantity"
			+ " ,jwoutdci.incoming_quantity AS jwOutDCIncomingQuantity"
			+ " ,party.name AS partyName"
			+ " ,status.name AS statusName"
			+ " ,mat.name AS materialName"
			+ " ,mat.number AS partNumber"
			+ " ,party.party_code AS partyCode"
			+ " FROM tr_purchase_order_header poh"
			+ " INNER JOIN tr_purchase_order_item poi"
			+ " ON(poh.id = poi.po_header_id)"
			+ " LEFT OUTER JOIN tr_delivery_challan_item di"
			+ " ON(di.po_item_id = poi.id)"
			+ " LEFT OUTER JOIN tr_delivery_challan_header dh"
			+ " ON(dh.id = di.dc_header_id)"
			+ " LEFT OUTER JOIN cd_transaction_type tt"
			+ " ON(tt.id = dh.dc_type AND tt.name ='Jobwork In DC')"
			+ " LEFT OUTER JOIN tr_delivery_challan_item jwoutdci"
			+ " ON(jwoutdci.src_dc_item_id = di.id)"
			+ " LEFT OUTER JOIN tr_delivery_challan_header jwoutdc"
			+ " ON(jwoutdc.id = jwoutdci.dc_header_id)"
			+ " JOIN ma_financial_year finY"
			+ " ON (poh.financial_year_id = finY.id)"
			+ " JOIN ma_party party"
			+ " ON poh.party_id = party.id"
			+ " JOIN ma_material mat "
			+ " ON (poi.material_id = mat.id)"
			+ " JOIN ma_party_type pType "
			+ " ON (party.party_type_id = pType.id) "
			+ " JOIN cd_transaction_type poType"
			+ " ON (poh.po_type = poType.id)"
			+ " JOIN cd_status STATUS"
			+ " ON (poh.status_id = STATUS.id)"
			+ " WHERE poi.dc_balance_quantity  > 0"
			+ " OR di.dc_balance_quantity  > 0 "
			+ " OR jwoutdci.invoice_balance_quantity > 0 ", nativeQuery = true)
	Page<List> getBalanceReport(Pageable pageable);*/


}


//@Query("SELECT NEW com.coreerp.reports.model.PoBalanceReport(poh.purchaseOrderNumber "
//		+ ",poh.purchaseOrderDate "
//		+ ",party.name AS partyName"
//		+ ",status.name AS statusName "
//		+ ",mat.name AS materialName "
//		+ ",mat.partNumber AS partNumber "
//		+ ",poh.id AS poHeaderId "
//		+ ",poi.quantity AS quantity "
//		+ ",poi.price AS price "
//		+ ",poi.dcBalanceQuantity AS dcBalanceQuantity "
//		+ ",poi.invoiceBalanceQuantity AS invoiceBalanceQuantity "
//		+ ",party.partyCode AS partyCode ) "
//		+ " FROM PurchaseOrderHeader poh "
//		+ " join poh.financialYear finY"
//		+ " join poh.purchaseOrderItems poi"
//		+ " join poh.party party "
//		+ " join poi.material mat"
//		+ " join party.partyType pType"
//		+ " join poh.purchaseOrderType poType"
//		+ " join poh.status status "
//		+ " where 1 = 1 "
//		+ " and (finY.id = :financialYearId or :financialYearId is null) "
//		+ " and (poh.purchaseOrderDate >= :fromDate or :fromDate is null) "
//		+ " and (poh.purchaseOrderDate <= :toDate or :toDate is null) "
//		+ " and (party.id = :partyId or :partyId is null ) "
//		+ " and pType.id in :partyTypeIds "
//		+ " and (poType.id = :transactionTypeId or :transactionTypeId is null) "
//		+ " and (status.id = :statusId or :statusId is null) "
//		+ " and (poh.createdBy = :createdBy or :createdBy is null) "
//)
