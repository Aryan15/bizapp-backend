package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.data.domain.Page;

public interface RecentQuotationService {

	public Page<QuotationHeader> getPage(TransactionType quotationType, FinancialYear financialYear, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<QuotationHeader> getPageByQuotationNumber(TransactionType quotationType, FinancialYear financialYear, String quotationNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<QuotationHeader> getPageByQuotationDate(TransactionType quotationType, FinancialYear financialYear, Date quotationStartDate, Date quotationEndDate, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<QuotationHeader> getPageByPartyName(TransactionType quotationType, FinancialYear financialYear, List<Party> parties, String sortColumn, String sortDirection, int pageNumber, int pageSize);

	public Page<QuotationHeader> getQuotationPageList(TransactionType quotationType, FinancialYear financialYear, String searchText, String sortColumn, String sortDirection, int pageNumber, int pageSize);


}
