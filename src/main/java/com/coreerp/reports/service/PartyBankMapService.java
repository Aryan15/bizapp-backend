package com.coreerp.reports.service;

import java.util.List;

import com.coreerp.dto.PartyBankMapDTO;
import com.coreerp.model.PartyBankMap;

public interface PartyBankMapService {

	public PartyBankMapDTO save(PartyBankMapDTO partyBankMapDTO);
	
	
	public PartyBankMapDTO getById(Long partyBankMapId);
	
	public PartyBankMap getModelById(Long partyBankMapId);

	public void deleteById(Long id);

	public List<PartyBankMapDTO> saveList(List<PartyBankMapDTO> partyBankMapDTOList);


	public List<PartyBankMapDTO> getForParty(Long partyId);
	
	public List<PartyBankMapDTO> getForCompany(Long companyId);

	public List<PartyBankMapDTO> getAll();
}
