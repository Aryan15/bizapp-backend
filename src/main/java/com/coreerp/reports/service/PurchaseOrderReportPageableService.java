package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import com.coreerp.dto.PayableReceivableReportDTO;
import com.coreerp.dto.PurchaseOrderReportDTO;
import com.coreerp.reports.model.PurchaseOrderReport;

public interface PurchaseOrderReportPageableService {
	public List<PurchaseOrderReport> getPagedPurchaseOrderReport(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, Long materialId, List<Long> partyTypeIds,Integer pageNumber, Integer pageSize,String sortDirection, String sortColumn);
	public PurchaseOrderReportDTO getPagedBalanceReport(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, List<Long> partyTypeIds, String createdBy, Long statusId, Integer pageNumber, Integer pageSize, String sortDirection, String sortColumn);


	public Long getPurchaseOrderReportCount(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, Long materialId,  List<Long> partyTypeIds);



}
