package com.coreerp.reports.service;

import com.coreerp.model.PettyCashHeader;
import com.coreerp.model.TransactionType;

import org.springframework.data.domain.Page;

import java.util.Date;

public interface RecentPettyCashService {


    public Page<PettyCashHeader> getPage(TransactionType pettyCashType, String sortColumn, String sortDirection, int pageNumber, int pageSize);

    public Page<PettyCashHeader> getPageByPettyCashNumber(TransactionType pettyCashType, String pettyCashNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize);

    public Page<PettyCashHeader> getPageByPettyCashDate(TransactionType pettyCashType, Date pettyCashDate, String sortColumn, String sortDirection, int pageNumber, int pageSize);



}
