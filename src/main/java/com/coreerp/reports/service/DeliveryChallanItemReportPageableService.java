package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import com.coreerp.reports.model.DeliveryChallanItemModel;

public interface DeliveryChallanItemReportPageableService {

	public List<DeliveryChallanItemModel> getPagedDeliveryChallanItemReport(Long deliveryChallanTypeId,Long financialYearId, Date fromDate, Date toDate,Long materialId, Long partyId, List<Long> partyTypeIds,Integer pageNumber, Integer pageSize,String sortDirection, String sortColumn);
	
	public Long getDeliveryChallanItemReportCount(Long deliveryChallanTypeId,Long financialYearId, Date fromDate, Date toDate, Long partyId, List<Long> partyTypeIds, Long materialId);
}
