package com.coreerp.reports.service;


import com.coreerp.dto.QuotationReportDTO;

import java.util.Date;
import java.util.List;



public interface QuotationReportPageableService {

    public QuotationReportDTO getPagedQuotationReport(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, List<Long> partyTypeIds, String createdBy, Integer pageNumber, Integer pageSize, String sortDirection, String sortColumn);
}
