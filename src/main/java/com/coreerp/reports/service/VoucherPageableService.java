package com.coreerp.reports.service;

import java.util.Date;

import org.springframework.data.domain.Page;

import com.coreerp.dto.VoucherHeaderDTO;
import com.coreerp.reports.model.VoucherReportModel;

public interface VoucherPageableService {

	public Page<VoucherReportModel> getPagedVoucherReport(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long expenseId, Integer pageNumber, Integer pageSize,String sortDirection, String sortColumn);
	
}
