package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.data.domain.Page;

public interface RecentGRNService {
	public Page<GRNHeader> getPage(TransactionType grnType, FinancialYear financialYear, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<GRNHeader> getPageByGrnNumber(TransactionType grnType, String grnNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<GRNHeader> getPageByGrnDate(TransactionType grnType, Date grnDate, Date searchEndDate, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<GRNHeader> getPageByPartyName(TransactionType grnType, List<Party> parties, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	public Page<GRNHeader> getGrnPageList(TransactionType grnType, FinancialYear financialYear, String searchText, String sortColumn, String sortDirection, int pageNumber, int pageSize);

}
