package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.coreerp.reports.model.InvoiceItemReportModel;

public interface InvoiceItemReportPageableService {


	public Page<InvoiceItemReportModel> getPagedInvoiceItemReport(Long invoiceTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, List<Long> partyTypeIds, Long materialId, Integer isHeaderLevel, String createdBy, Integer pageNumber, Integer pageSize,String sortDirection, String sortColumn);
	
//	public Long getInvoiceItemReportCount(Long invoiceTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, List<Long> partyTypeIds, Long materialId);

}
