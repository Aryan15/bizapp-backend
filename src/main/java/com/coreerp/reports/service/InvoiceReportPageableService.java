package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import com.coreerp.dto.InvoiceReportDTO;
import com.coreerp.dto.PayableReceivableReportDTO;
import com.coreerp.reports.model.BalanceReport;
import com.coreerp.reports.model.InvoiceReport;

public interface InvoiceReportPageableService {

	public InvoiceReportDTO getPagedInvoiceReport(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, List<Long> partyTypeIds, String createdBy, Integer pageNumber, Integer pageSize,String sortDirection, String sortColumn);
	
//	public Long getInvoiceReportCount(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, List<Long> partyTypeIds);
	
	public PayableReceivableReportDTO getPagedBalanceReport(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, List<Long> partyTypeIds, String createdBy,Long statusId, Integer pageNumber, Integer pageSize,String sortDirection, String sortColumn);
	
	//public Long getBalanceReportCount(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, List<Long> partyTypeIds);
}
