package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.data.domain.Page;

public interface RecentInvoiceService {

	public Page<InvoiceHeader> getPage(TransactionType invoiceType, FinancialYear financialYear, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<InvoiceHeader> getPageByInvoiceNumber(TransactionType invoiceType, FinancialYear financialYear, String invoiceNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<InvoiceHeader> getPageByInvoiceDate(TransactionType invoiceType, FinancialYear financialYear, Date invoiceFromDate, Date invoiceToDate, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<InvoiceHeader> getPageByPartyName(TransactionType invoiceType, FinancialYear financialYear, List<Party> parties,String sortColumn, String sortDirection, int pageNumber, int pageSize);
	public Page<InvoiceHeader> getInvoicePageList(TransactionType invoiceType, FinancialYear financialYear, String searchText, String sortColumn, String sortDirection, int pageNumber, int pageSize);

}
