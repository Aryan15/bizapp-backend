package com.coreerp.reports.service;

import java.util.List;

import com.coreerp.model.Material;
import com.coreerp.model.MaterialType;
import com.coreerp.model.SupplyType;
import org.springframework.data.domain.Page;

public interface MaterialReportService {

	public Page<Material> getPage(SupplyType supplyType, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<Material> getPageByName(SupplyType supplyType, String name, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<Material> getPageByPartNumber(SupplyType supplyType, String partNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize);

	public Page<Material> getPageNonJobwork(SupplyType supplyType, String sortColumn, String sortDirection, int pageNumber, int pageSize);

	public Page<Material> getPageByNameNonJobwork(SupplyType supplyType, String name, String sortColumn, String sortDirection, int pageNumber, int pageSize);

	public Page<Material> getPageByPartNumberNonJobwork(SupplyType supplyType, String partNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize);

}
