package com.coreerp.reports.service;

import com.coreerp.model.AutoTransactionGeneration;
import org.springframework.data.domain.Page;

import java.util.Date;

public interface RecentAutoTransactionService {


    public Page<AutoTransactionGeneration> getPage(String sortColumn, String sortDirection, int pageNumber, int pageSize);

    public Page<AutoTransactionGeneration> getPageByInvoiceNumber(String invoiceNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize);

    public Page<AutoTransactionGeneration> getPageByStartDate(Date startDate, String sortColumn, String sortDirection, int pageNumber, int pageSize);

}
