package com.coreerp.reports.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.coreerp.model.User;

public interface UserReportService {

	public Page<User> getPage(String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<User> getPageByName(String name, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<User> getPageByLastName(String lastName, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
//	public Long getTotalCount();
//
//	public Long getTotalCountByName(String searchText);
//
//	public Long getTotalCountByLastName(String searchText);
}
