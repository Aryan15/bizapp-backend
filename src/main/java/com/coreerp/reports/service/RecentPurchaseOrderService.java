package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.coreerp.model.FinancialYear;
import com.coreerp.model.Party;
import com.coreerp.model.PurchaseOrderHeader;
import com.coreerp.model.TransactionType;

public interface RecentPurchaseOrderService {

	public Page<PurchaseOrderHeader> getPage(TransactionType purchaseOrderType, FinancialYear financialYear, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<PurchaseOrderHeader> getPageByPurchaseOrderNumber(TransactionType purchaseOrderType,String purchaseOrderNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<PurchaseOrderHeader> getPageByPurchaseOrderDate(TransactionType purchaseOrderType,Date purchaseOrderDate, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<PurchaseOrderHeader> getPageByPartyName(TransactionType purchaseOrderType,List<Party> parties,String sortColumn, String sortDirection, int pageNumber, int pageSize);

	public Page<PurchaseOrderHeader> getPageList(TransactionType purchaseOrderType, FinancialYear financialYear, String searchText, String sortColumn, String sortDirection, int pageNumber, int pageSize);



}
