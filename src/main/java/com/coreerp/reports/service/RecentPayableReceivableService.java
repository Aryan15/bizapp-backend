package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.coreerp.model.Party;
import com.coreerp.model.PayableReceivableHeader;
import com.coreerp.model.TransactionType;
import com.coreerp.model.FinancialYear;

public interface RecentPayableReceivableService {
	public Page<PayableReceivableHeader> getPage(TransactionType transactionType, FinancialYear financialYear, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<PayableReceivableHeader> getPageByPayReferenceNumber(TransactionType transactionType, FinancialYear financialYear, String invoiceNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<PayableReceivableHeader> getPageByPayReferenceDate(TransactionType transactionType, FinancialYear financialYear, Date invoiceDate, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<PayableReceivableHeader> getPageByPartyName(TransactionType transactionType, FinancialYear financialYear, List<Party> parties,String sortColumn, String sortDirection, int pageNumber, int pageSize);

	public Page<PayableReceivableHeader> getPayRecPageList(TransactionType grnType, FinancialYear financialYear, String searchText, String sortColumn, String sortDirection, int pageNumber, int pageSize);


}
