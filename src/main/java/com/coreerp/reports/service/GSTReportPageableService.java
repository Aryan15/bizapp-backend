package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.coreerp.reports.model.B2BGSTReport;

public interface GSTReportPageableService {

	
	public Page<B2BGSTReport> getPagedB2BReport(Long financialYearId, Date fromDate, Date toDate, Long transactionId, Integer pageNumber, Integer pageSize,String sortDirection, String sortColumn);
	
}
