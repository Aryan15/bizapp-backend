package com.coreerp.reports.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.coreerp.dto.TransactionReportRequestDTO;
import com.coreerp.reports.model.PayableReceivableReport;

public interface PayableReceivablePageableService {
	public Page<PayableReceivableReport> getPagedPayableReceivableReport(Long transactionTypeId, Long inTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, List<Long> partyTypeIds,Integer pageNumber, Integer pageSize,String sortDirection, String sortColumn);
	
	public Page<PayableReceivableReport>  getReport(
			Long transactionTypeId,
			Long inTypeId,
			Integer pageNumber,
			Integer pageSize,
			String sortDir,
			String sortColumn,
			TransactionReportRequestDTO transactioReportRequestDTO);
//	public Long getPayableReceivableReportCount(Long transactionTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, List<Long> partyTypeIds);
	
	public ByteArrayInputStream payableReceivableToExcel(List<PayableReceivableReport> report) throws IOException;
	
	public ByteArrayInputStream nestedExcel() throws IOException;

}
