package com.coreerp.reports.service;

 import com.coreerp.reports.model.EWayBillReport;
 import com.coreerp.reports.model.EwayBillRecentReport;
 import org.springframework.data.domain.Page;

 import java.util.Date;

public interface RecentEWayBillService {
    public Page<EwayBillRecentReport> getPage(String name, String sortColumn, String sortDirection, int pageNumber, int pageSize);
    public Page<EWayBillReport> getReport(Date fromDate, Date toDate, Long partyId, String sortColumn, String sortDirection, int pageNumber, int pageSize);

}
