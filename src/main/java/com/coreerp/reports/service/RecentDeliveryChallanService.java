package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import com.coreerp.model.*;
import org.springframework.data.domain.Page;

public interface RecentDeliveryChallanService {

	public Page<DeliveryChallanHeader> getPage(TransactionType deliveryChallanType, FinancialYear financialYear, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<DeliveryChallanHeader> getPageByDeliveryChallanNumber(TransactionType deliveryChallanType, String deliveryChallanNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<DeliveryChallanHeader> getPageByDeliveryChallanDate(TransactionType deliveryChallanType, Date deliveryChallanDate, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<DeliveryChallanHeader> getPageByPartyName(TransactionType deliveryChallanType, List<Party> parties, String sortColumn, String sortDirection, int pageNumber, int pageSize);

	public Page<DeliveryChallanHeader> getDcPageList(TransactionType dcType, FinancialYear financialYear, String searchText, String sortColumn, String sortDirection, int pageNumber, int pageSize);

}
