package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.coreerp.model.VoucherHeader;
import com.coreerp.model.Party;
import com.coreerp.model.TransactionType;

public interface RecentVoucherService {
	
	public Page<VoucherHeader> getPage(TransactionType voucherType, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<VoucherHeader> getPageByVoucherNumber(TransactionType voucherType, String voucherNumber, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<VoucherHeader> getPageByVoucherDate(TransactionType voucherType, Date voucherDate, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Page<VoucherHeader> getPageByPaidTo(TransactionType voucherType,  String paidTo, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	

}
