package com.coreerp.reports.service;

import java.util.Date;
import java.util.List;

import com.coreerp.dto.InvoiceTaxReportDTO;

public interface InvoiceTaxReportPageableService {

	public InvoiceTaxReportDTO getPagedInvoiceTaxReport(Long invoiceTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, Long materialId, List<Long> partyTypeIds,Integer pageNumber, Integer pageSize,String sortDirection, String sortColumn);
	
//	public Long getInvoiceTaxReportCount(Long invoiceTypeId, Long financialYearId, Date fromDate, Date toDate, Long partyId, Long materialId, List<Long> partyTypeIds);
}
