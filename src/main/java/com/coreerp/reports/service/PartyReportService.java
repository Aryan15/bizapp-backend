package com.coreerp.reports.service;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;

import com.coreerp.dto.PartyWithPriceListDTO;
import com.coreerp.model.Party;
import com.coreerp.model.PartyType;

public interface PartyReportService {

	public List<Party> getPage(List<PartyType> partyTypes, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public List<Party> getPageByName(List<PartyType> partyTypes, String name, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
	public Long getTotalCount();

	public Long getTotalCountByName(List<PartyType> partyTypes,String searchText);
	
	public Long getTotalCountForPartyTypes(List<PartyType> partyTypes);


	public Page<Party> getPageReport(List<Long> partyTypeIds, String createdBy, String sortColumn, String sortDirection, int pageNumber, int pageSize);
	
}
