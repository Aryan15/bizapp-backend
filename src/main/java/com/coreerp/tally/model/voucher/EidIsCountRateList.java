package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class EidIsCountRateList {

    @JacksonXmlProperty(isAttribute = true,localName="DESC" )
    private String desc="`EI DiscountRate`"  ;

    @JacksonXmlProperty(isAttribute = true, localName = "ISLIST")
    private String isList="YES" ;

    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type ="Number";

    @JacksonXmlProperty(isAttribute = true, localName = "INDEX")
    private String index="10001" ;

    @JsonProperty("UDF:EIDISCOUNTRATE")
    EidIsCountRate eidIsCountRate;

   /* private*/


    public EidIsCountRate getEidIsCountRate() {
        return eidIsCountRate;
    }

    public void setEidIsCountRate(EidIsCountRate eidIsCountRate) {
        this.eidIsCountRate = eidIsCountRate;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getIsList() {
        return isList;
    }

    public void setIsList(String isList) {
        this.isList = isList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

  /*  public String getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(String discountRate) {
        this.discountRate = discountRate;
    }*/
}
