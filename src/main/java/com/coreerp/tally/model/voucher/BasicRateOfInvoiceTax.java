package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class BasicRateOfInvoiceTax {


    @JacksonXmlProperty(isAttribute = true, localName = "Type")
    private String Type = "Number";


    @JsonProperty("BASICRATEOFINVOICETAX")
    private Double basicRateOfInvoiceTaxPercent;

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public Double getBasicRateOfInvoiceTaxPercent() {
        return basicRateOfInvoiceTaxPercent;
    }

    public void setBasicRateOfInvoiceTaxPercent(Double basicRateOfInvoiceTaxPercent) {
        this.basicRateOfInvoiceTaxPercent = basicRateOfInvoiceTaxPercent;
    }
}
