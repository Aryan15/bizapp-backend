package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DiscountLeadgerEntries {


    @JsonProperty("OLDAUDITENTRYIDS.LIST ")
    private OldAuditEntryList oldAuditEntryList;


    @JsonProperty("BASICRATEOFINVOICETAX.LIST")
    private BasicRateOfInvoiceTax basicRateOfInvoiceTax;



    @JsonProperty("LEDGERNAME")
    private String ledgerName ;

    @JsonProperty("GSTCLASS")
    private String gstClass ;


    @JsonProperty("ISDEEMEDPOSITIVE")
    private String isDeemedPositive ;

    @JsonProperty("LEDGERFROMITEM")
    private String ledgerFromItem ;

    @JsonProperty("ISPARTYLEDGER")
    private String isPartyLedger ;

    @JsonProperty("ISLASTDEEMEDPOSITIVE")
    private String isLastdeemedPositive ;

    @JsonProperty("ISCAPVATNOTCLAIMED")
    private String isCapvatNotClaimed ;

    @JsonProperty("AMOUNT")
    private Double amount ;

    @JsonProperty("VATEXPAMOUNT")
    private Double vatexpAmount ;


    public OldAuditEntryList getOldAuditEntryList() {
        return oldAuditEntryList;
    }

    public void setOldAuditEntryList(OldAuditEntryList oldAuditEntryList) {
        this.oldAuditEntryList = oldAuditEntryList;
    }

    public BasicRateOfInvoiceTax getBasicRateOfInvoiceTax() {
        return basicRateOfInvoiceTax;
    }

    public void setBasicRateOfInvoiceTax(BasicRateOfInvoiceTax basicRateOfInvoiceTax) {
        this.basicRateOfInvoiceTax = basicRateOfInvoiceTax;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getGstClass() {
        return gstClass;
    }

    public void setGstClass(String gstClass) {
        this.gstClass = gstClass;
    }

    public String getIsDeemedPositive() {
        return isDeemedPositive;
    }

    public void setIsDeemedPositive(String isDeemedPositive) {
        this.isDeemedPositive = isDeemedPositive;
    }

    public String getLedgerFromItem() {
        return ledgerFromItem;
    }

    public void setLedgerFromItem(String ledgerFromItem) {
        this.ledgerFromItem = ledgerFromItem;
    }

    public String getIsPartyLedger() {
        return isPartyLedger;
    }

    public void setIsPartyLedger(String isPartyLedger) {
        this.isPartyLedger = isPartyLedger;
    }

    public String getIsLastdeemedPositive() {
        return isLastdeemedPositive;
    }

    public void setIsLastdeemedPositive(String isLastdeemedPositive) {
        this.isLastdeemedPositive = isLastdeemedPositive;
    }

    public String getIsCapvatNotClaimed() {
        return isCapvatNotClaimed;
    }

    public void setIsCapvatNotClaimed(String isCapvatNotClaimed) {
        this.isCapvatNotClaimed = isCapvatNotClaimed;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getVatexpAmount() {
        return vatexpAmount;
    }

    public void setVatexpAmount(Double vatexpAmount) {
        this.vatexpAmount = vatexpAmount;
    }
}
