package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

public class EidIsCountRate {

    @JacksonXmlProperty(isAttribute = true,localName="DESC" )
    private String desc ="`EI DiscountRate`";

    @JacksonXmlText
    private String discountRate;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(String discountRate) {
        this.discountRate = discountRate;
    }
}
