package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


import java.util.List;


public class Voucher {


    @JacksonXmlProperty(isAttribute = true, localName = "REMOTEID")
    private String remoteId ;


    @JacksonXmlProperty(isAttribute = true, localName = "VCHTYPE")
    private String vchType ="Sales";

    @JacksonXmlProperty(isAttribute = true, localName = "ACTION")
    private String action ;

    @JacksonXmlProperty(isAttribute = true, localName = "OBJVIEW")
    private String objView = "Invoice Voucher View";

    @JsonProperty("ADDRESS.LIST")
    private AddressList addressList;

    @JsonProperty("BASICBUYERADDRESS.LIST")
    private BasicBuyerAddressList basicBuyerAddressList;

    @JsonProperty("BASICORDERTERMS.LIST")
    private BasicOrderTermsList basicOrderTermsList;


    @JsonProperty("DATE")
    private String date;


    @JsonProperty("GUID")
    private String guid;


    @JsonProperty("STATENAME")
   private String stateName ;

    @JsonProperty("CONTRYOFRESIDENCE")
    private String countryOfResidence ;


    @JsonProperty("PARTYGSTIN")
    private String partyGstin ;

    @JsonProperty("PARTYNAME")
    private String partyName;

    @JsonProperty("VOUCHERTYPENAME")
    private String voucherTypeName ;

    @JsonProperty("REFERENCE")
    private String reference ;

    @JsonProperty("VOUCHERNUMBER")
    private String voucherNumber ;

    @JsonProperty("PARTYLEDGERNAME")
    private String partyLeadgerName ;

    @JsonProperty("BASICBASEPARTYNAME")
    private String basicBasePartyName ;

    @JsonProperty("PERSISTEDVIEW")
    private String persistedView ;

    @JsonProperty("PLACEOFSUPPLY")
    private String placeOfSupply ;

    @JsonProperty("CONSIGNEEGSTIN")
    private String consigneeGstin ;

    @JsonProperty("BASICBUYERNAME")
    private String basicBuyerName ;

    @JsonProperty("BASICFINALDESTINATION")
    private String basicFinalDestination ;

    @JsonProperty("BASICSHIPVESSELNO")
    private String basicShipVesSelNo ;

    @JsonProperty("BASICDUEDATEOFPYMT")
    private String basicDueDateOfPymt ;

    @JsonProperty("CONSIGNEESTATENAME")
    private String consigneeStateName ;

    @JsonProperty("DIFFACTUALQTY")
    private String diffActualQty ;

    @JsonProperty("ASORIGINAL")
    private String asOriginal ;

    @JsonProperty("FORJOBCOSTING")
    private String forJobCosting ;

    @JsonProperty("ISOPTIONAL")
    private String isOptionl ;

   @JsonProperty("EFFECTIVEDATE")
    private String effectiveDate ;

    @JsonProperty("ISGSTOVERRIDDEN")
    private String isGstOverRidden ;

    @JsonProperty("IGNOREGSTINVALIDATION")
    private String ignoreGstInvalidation ;

    @JsonProperty("ISCANCELLED")
    private String isCancelled ;

    @JsonProperty("ISPOSTDATED")
    private String isPostDated ;

    @JsonProperty("USETRACKINGNUMBER")
    private String userTrackingNumber ;

    @JsonProperty("ISINVOICE")
    private String isInvoice ;

    @JsonProperty("ISDELETED")
    private String isDeleted ;

    @JsonProperty("CURRPARTYLEDGERNAME")
    private String currPartyLeadgerName ;

    @JsonProperty("CURRBASICBUYERNAME")
    private String currBasicBuyerName ;

    @JsonProperty("CURRPARTYNAME")
    private String currPartyName;

    @JsonProperty("CURRSTATENAME")
    private String currStateName ;

    @JsonProperty("TEMPGSTEWAYBILLNUMBER")
    private String tempGstWayBillNumber ;

    @JsonProperty("TEMPGSTEWAYSUBTYPE")
    private String tempGstEwaySubType ;

    @JsonProperty("TEMPGSTEWAYDOCUMENTTYPE")
    private String tempGstEwayDocumentType ;

    @JsonProperty("TEMPGSTEWAYCONSIGNOR")
    private String tempGstEWAYCONSIGNOR;

    @JsonProperty("TEMPGSTEWAYPINCODENUMBER")
    private String tempGstEwayPinCodeNumber ;

    @JsonProperty("TEMPGSTEWAYPINCODE")
    private String tempGstEwayPinCode ;

    @JsonProperty("TEMPGSTEWAYCONSIGNORTIN")
    private String tempGsteWayConsignortin ;

    @JsonProperty("TEMPGSTEWAYCONSIGNORSTATE")
    private String tempGstEwayConsignorState ;

    @JsonProperty("TEMPGSTEWAYCONSSHIPFROMSTATE")
    private String tempGstEwayConsshipFromState;

    @JsonProperty("TEMPGSTEWAYCONSIGNEE")
    private String tempGstEwayConsignee ;

    @JsonProperty("TEMPGSTEWAYCONSPINCODENUMBER")
    private String tempGstEwayConspinCodeNumber ;

    @JsonProperty("TEMPGSTEWAYCONSPINCODE")
    private String tempGstEwayConspinCode;

    @JsonProperty("TEMPGSTEWAYCONSTIN")
    private String tempGstEwayConstin;

    @JsonProperty("TEMPGSTEWAYCONSSTATE")
    private String tempGstEwayConsState;

    @JsonProperty("TEMPGSTEWAYCONSSHIPTOSTATE")
    private String tempGstEwayConsShipToState;

    @JsonProperty("TEMPGSTEWAYTRANSPORTMODE")
    private String tempGstEwayTransportMode;

    @JsonProperty("TEMPGSTEWAYVEHICLENUMBER")
    private String tempGstEwayVehicleNumber;


    @JsonProperty("TEMPGSTEWAYVEHICLETYPE")
    private String tempGstEwayVehicleType;

    @JsonProperty("HASDISCOUNTS")
    private String hasDiscounts;


    @JsonProperty("EWAYBILLDETAILS.LIST")
    private EwayBillDetailsList ewayBillDetailsList;

    @JsonProperty("INVOICEDELNOTES.LIST")
    private InvoiceDelNotesList invoiceDelNotesList;

    @JsonProperty("INVOICEORDER.LIST")
    private InvoiceOrderList invoiceOrderList;

    @JsonProperty("LEDGERENTRIES.LIST")
    private LedgerEntries ledgerEntries;

    @JsonProperty("LEDGERENTRIES.LIST ")
    private DiscountLeadgerEntries discountLeadgerEntries;

    @JsonProperty("LEDGERENTRIES.LIST  ")
    private GstLedger sgstLedger;
    @JsonProperty("LEDGERENTRIES.LIST   ")
    private GstLedger cgstLedger;
    @JsonProperty("LEDGERENTRIES.LIST    ")
  private GstLedger igstLedger;

    @JsonProperty("LEDGERENTRIES.LIST     ")
    private RoundOffLeadgerEntries roundOffLeadgerEntries;

//    @JacksonXmlElementWrapper(localName = "ALLINVENTORYENTRIES.LIST")
    @JsonProperty("ALLINVENTORYENTRIES.LIST")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Inventory> inventoryList;


    @JsonProperty("GSTEWAYCONSIGNORADDRESS.LIST")
    private GsteWayConsignorAddress gsteWayConsignorAddress;

    @JsonProperty("GSTEWAYCONSIGNORADDRESS.LIST ")
    private GsteWayConsignorAddress gsteWayConsignorAddressParty;


    public GsteWayConsignorAddress getGsteWayConsignorAddressParty() {
        return gsteWayConsignorAddressParty;
    }

    public void setGsteWayConsignorAddressParty(GsteWayConsignorAddress gsteWayConsignorAddressParty) {
        this.gsteWayConsignorAddressParty = gsteWayConsignorAddressParty;
    }

    public GsteWayConsignorAddress getGsteWayConsignorAddress() {
        return gsteWayConsignorAddress;
    }

    public void setGsteWayConsignorAddress(GsteWayConsignorAddress gsteWayConsignorAddress) {
        this.gsteWayConsignorAddress = gsteWayConsignorAddress;
    }

    public String getHasDiscounts() {
        return hasDiscounts;
    }

    public void setHasDiscounts(String hasDiscounts) {
        this.hasDiscounts = hasDiscounts;
    }

    public GstLedger getSgstLedger() {
        return sgstLedger;
    }

    public void setSgstLedger(GstLedger sgstLedger) {
        this.sgstLedger = sgstLedger;
    }

    public GstLedger getCgstLedger() {
        return cgstLedger;
    }

    public void setCgstLedger(GstLedger cgstLedger) {
        this.cgstLedger = cgstLedger;
    }

    public GstLedger getIgstLedger() {
        return igstLedger;
    }

    public void setIgstLedger(GstLedger igstLedger) {
        this.igstLedger = igstLedger;
    }

    public List<Inventory> getInventoryList() {
        return inventoryList;
    }

    public void setInventoryList(List<Inventory> inventoryList) {
        this.inventoryList = inventoryList;
    }

    public LedgerEntries getLedgerEntries() {
        return ledgerEntries;
    }

    public void setLedgerEntries(LedgerEntries ledgerEntries) {
        this.ledgerEntries = ledgerEntries;
    }

    public InvoiceOrderList getInvoiceOrderList() {
        return invoiceOrderList;
    }

    public void setInvoiceOrderList(InvoiceOrderList invoiceOrderList) {
        this.invoiceOrderList = invoiceOrderList;
    }

    public InvoiceDelNotesList getInvoiceDelNotesList() {
        return invoiceDelNotesList;
    }

    public void setInvoiceDelNotesList(InvoiceDelNotesList invoiceDelNotesList) {
        this.invoiceDelNotesList = invoiceDelNotesList;
    }

    public EwayBillDetailsList getEwayBillDetailsList() {
        return ewayBillDetailsList;
    }

    public void setEwayBillDetailsList(EwayBillDetailsList ewayBillDetailsList) {
        this.ewayBillDetailsList = ewayBillDetailsList;
    }

    public void setTempGstEwayPinCodeNumber(String tempGstEwayPinCodeNumber) {
        this.tempGstEwayPinCodeNumber = tempGstEwayPinCodeNumber;
    }

    public void setTempGstEwayPinCode(String tempGstEwayPinCode) {
        this.tempGstEwayPinCode = tempGstEwayPinCode;
    }



    public String getTempGstEwayConsignorState() {
        return tempGstEwayConsignorState;
    }

    public void setTempGstEwayConsignorState(String tempGstEwayConsignorState) {
        this.tempGstEwayConsignorState = tempGstEwayConsignorState;
    }

    public String getTempGstEwayConsshipFromState() {
        return tempGstEwayConsshipFromState;
    }

    public void setTempGstEwayConsshipFromState(String tempGstEwayConsshipFromState) {
        this.tempGstEwayConsshipFromState = tempGstEwayConsshipFromState;
    }

    public String getTempGstEwayConsignee() {
        return tempGstEwayConsignee;
    }

    public void setTempGstEwayConsignee(String tempGstEwayConsignee) {
        this.tempGstEwayConsignee = tempGstEwayConsignee;
    }

    public String getTempGstEwayPinCodeNumber() {
        return tempGstEwayPinCodeNumber;
    }

    public String getTempGstEwayPinCode() {
        return tempGstEwayPinCode;
    }

    public String getTempGstEwayConspinCodeNumber() {
        return tempGstEwayConspinCodeNumber;
    }

    public void setTempGstEwayConspinCodeNumber(String tempGstEwayConspinCodeNumber) {
        this.tempGstEwayConspinCodeNumber = tempGstEwayConspinCodeNumber;
    }

    public String getTempGstEwayConspinCode() {
        return tempGstEwayConspinCode;
    }

    public void setTempGstEwayConspinCode(String tempGstEwayConspinCode) {
        this.tempGstEwayConspinCode = tempGstEwayConspinCode;
    }

    public String getTempGstEwayConstin() {
        return tempGstEwayConstin;
    }

    public void setTempGstEwayConstin(String tempGstEwayConstin) {
        this.tempGstEwayConstin = tempGstEwayConstin;
    }

    public String getTempGstEwayConsState() {
        return tempGstEwayConsState;
    }

    public void setTempGstEwayConsState(String tempGstEwayConsState) {
        this.tempGstEwayConsState = tempGstEwayConsState;
    }

    public String getTempGstEwayConsShipToState() {
        return tempGstEwayConsShipToState;
    }

    public void setTempGstEwayConsShipToState(String tempGstEwayConsShipToState) {
        this.tempGstEwayConsShipToState = tempGstEwayConsShipToState;
    }

    public String getTempGstEwayTransportMode() {
        return tempGstEwayTransportMode;
    }

    public void setTempGstEwayTransportMode(String tempGstEwayTransportMode) {
        this.tempGstEwayTransportMode = tempGstEwayTransportMode;
    }

    public String getTempGstEwayVehicleNumber() {
        return tempGstEwayVehicleNumber;
    }

    public void setTempGstEwayVehicleNumber(String tempGstEwayVehicleNumber) {
        this.tempGstEwayVehicleNumber = tempGstEwayVehicleNumber;
    }

    public String getTempGstEwayVehicleType() {
        return tempGstEwayVehicleType;
    }

    public void setTempGstEwayVehicleType(String tempGstEwayVehicleType) {
        this.tempGstEwayVehicleType = tempGstEwayVehicleType;
    }


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }



    public String getObjView() {
        return objView;
    }

    public void setObjView(String objView) {
        this.objView = objView;
    }

    public String getVchType() {
        return vchType;
    }

    public void setVchType(String vchType) {
        this.vchType = vchType;
    }

    public String getVoucherTypeName() {
        return voucherTypeName;
    }

    public void setVoucherTypeName(String voucherTypeName) {
        this.voucherTypeName = voucherTypeName;
    }

    public String getVoucherNumber() {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryOfResidence() {
        return countryOfResidence;
    }

    public void setCountryOfResidence(String countryOfResidence) {
        this.countryOfResidence = countryOfResidence;
    }

    public String getPartyGstin() {
        return partyGstin;
    }

    public void setPartyGstin(String partyGstin) {
        this.partyGstin = partyGstin;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getReference() {
        return reference;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getPartyLeadgerName() {
        return partyLeadgerName;
    }

    public void setPartyLeadgerName(String partyLeadgerName) {
        this.partyLeadgerName = partyLeadgerName;
    }

    public String getBasicBasePartyName() {
        return basicBasePartyName;
    }

    public void setBasicBasePartyName(String basicBasePartyName) {
        this.basicBasePartyName = basicBasePartyName;
    }

    public String getPersistedView() {
        return persistedView;
    }

    public void setPersistedView(String persistedView) {
        this.persistedView = persistedView;
    }

    public String getPlaceOfSupply() {
        return placeOfSupply;
    }

    public void setPlaceOfSupply(String placeOfSupply) {
        this.placeOfSupply = placeOfSupply;
    }

    public String getConsigneeGstin() {
        return consigneeGstin;
    }

    public void setConsigneeGstin(String consigneeGstin) {
        this.consigneeGstin = consigneeGstin;
    }

    public String getBasicBuyerName() {
        return basicBuyerName;
    }

    public void setBasicBuyerName(String basicBuyerName) {
        this.basicBuyerName = basicBuyerName;
    }

    public String getBasicFinalDestination() {
        return basicFinalDestination;
    }

    public void setBasicFinalDestination(String basicFinalDestination) {
        this.basicFinalDestination = basicFinalDestination;
    }

    public String getBasicShipVesSelNo() {
        return basicShipVesSelNo;
    }

    public void setBasicShipVesSelNo(String basicShipVesSelNo) {
        this.basicShipVesSelNo = basicShipVesSelNo;
    }

    public String getBasicDueDateOfPymt() {
        return basicDueDateOfPymt;
    }

    public void setBasicDueDateOfPymt(String basicDueDateOfPymt) {
        this.basicDueDateOfPymt = basicDueDateOfPymt;
    }

    public String getConsigneeStateName() {
        return consigneeStateName;
    }

    public void setConsigneeStateName(String consigneeStateName) {
        this.consigneeStateName = consigneeStateName;
    }

    public String getDiffActualQty() {
        return diffActualQty;
    }

    public void setDiffActualQty(String diffActualQty) {
        this.diffActualQty = diffActualQty;
    }

    public String getAsOriginal() {
        return asOriginal;
    }

    public void setAsOriginal(String asOriginal) {
        this.asOriginal = asOriginal;
    }

    public String getForJobCosting() {
        return forJobCosting;
    }

    public void setForJobCosting(String forJobCosting) {
        this.forJobCosting = forJobCosting;
    }

    public String getIsOptionl() {
        return isOptionl;
    }

    public void setIsOptionl(String isOptionl) {
        this.isOptionl = isOptionl;
    }

    public String getIsGstOverRidden() {
        return isGstOverRidden;
    }

    public void setIsGstOverRidden(String isGstOverRidden) {
        this.isGstOverRidden = isGstOverRidden;
    }

    public String getIgnoreGstInvalidation() {
        return ignoreGstInvalidation;
    }

    public void setIgnoreGstInvalidation(String ignoreGstInvalidation) {
        this.ignoreGstInvalidation = ignoreGstInvalidation;
    }

    public String getIsCancelled() {
        return isCancelled;
    }

    public void setIsCancelled(String isCancelled) {
        this.isCancelled = isCancelled;
    }

    public String getIsPostDated() {
        return isPostDated;
    }

    public void setIsPostDated(String isPostDated) {
        this.isPostDated = isPostDated;
    }

    public String getUserTrackingNumber() {
        return userTrackingNumber;
    }

    public void setUserTrackingNumber(String userTrackingNumber) {
        this.userTrackingNumber = userTrackingNumber;
    }

    public String getIsInvoice() {
        return isInvoice;
    }

    public void setIsInvoice(String isInvoice) {
        this.isInvoice = isInvoice;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCurrPartyLeadgerName() {
        return currPartyLeadgerName;
    }

    public void setCurrPartyLeadgerName(String currPartyLeadgerName) {
        this.currPartyLeadgerName = currPartyLeadgerName;
    }

    public String getCurrBasicBuyerName() {
        return currBasicBuyerName;
    }

    public void setCurrBasicBuyerName(String currBasicBuyerName) {
        this.currBasicBuyerName = currBasicBuyerName;
    }

    public String getCurrPartyName() {
        return currPartyName;
    }

    public void setCurrPartyName(String currPartyName) {
        this.currPartyName = currPartyName;
    }

    public String getCurrStateName() {
        return currStateName;
    }

    public void setCurrStateName(String currStateName) {
        this.currStateName = currStateName;
    }

    public String getTempGstWayBillNumber() {
        return tempGstWayBillNumber;
    }

    public void setTempGstWayBillNumber(String tempGstWayBillNumber) {
        this.tempGstWayBillNumber = tempGstWayBillNumber;
    }

    public String getTempGstEwaySubType() {
        return tempGstEwaySubType;
    }

    public void setTempGstEwaySubType(String tempGstEwaySubType) {
        this.tempGstEwaySubType = tempGstEwaySubType;
    }

    public String getTempGstEwayDocumentType() {
        return tempGstEwayDocumentType;
    }

    public void setTempGstEwayDocumentType(String tempGstEwayDocumentType) {
        this.tempGstEwayDocumentType = tempGstEwayDocumentType;
    }

    public String getTempGstEWAYCONSIGNOR() {
        return tempGstEWAYCONSIGNOR;
    }

    public void setTempGstEWAYCONSIGNOR(String tempGstEWAYCONSIGNOR) {
        this.tempGstEWAYCONSIGNOR = tempGstEWAYCONSIGNOR;
    }


    public String getTempGsteWayConsignortin() {
        return tempGsteWayConsignortin;
    }

    public void setTempGsteWayConsignortin(String tempGsteWayConsignortin) {
        this.tempGsteWayConsignortin = tempGsteWayConsignortin;
    }

    public AddressList getAddressList() {
        return addressList;
    }

    public void setAddressList(AddressList addressList) {
        this.addressList = addressList;
    }


    public BasicBuyerAddressList getBasicBuyerAddressList() {
        return basicBuyerAddressList;
    }

    public void setBasicBuyerAddressList(BasicBuyerAddressList basicBuyerAddressList) {
        this.basicBuyerAddressList = basicBuyerAddressList;
    }

    public BasicOrderTermsList getBasicOrderTermsList() {
        return basicOrderTermsList;
    }

    public void setBasicOrderTermsList(BasicOrderTermsList basicOrderTermsList) {
        this.basicOrderTermsList = basicOrderTermsList;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }



    public String getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(String remoteId) {
        this.remoteId = remoteId;
    }

   public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public DiscountLeadgerEntries getDiscountLeadgerEntries() {
        return discountLeadgerEntries;
    }

    public void setDiscountLeadgerEntries(DiscountLeadgerEntries discountLeadgerEntries) {
        this.discountLeadgerEntries = discountLeadgerEntries;
    }
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public RoundOffLeadgerEntries getRoundOffLeadgerEntries() {
        return roundOffLeadgerEntries;
    }

    public void setRoundOffLeadgerEntries(RoundOffLeadgerEntries roundOffLeadgerEntries) {
        this.roundOffLeadgerEntries = roundOffLeadgerEntries;
    }
}
