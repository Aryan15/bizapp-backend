package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class RequestData {
//    @JsonProperty("TALLYMESSAGE")
    @JacksonXmlProperty(namespace = "TallyUDF", localName = "TALLYMESSAGE")
//    @JsonProperty("ALLINVENTORYENTRIES.LIST")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<TallyMessageVoucher> tallyMessageVoucher;

   /* @JacksonXmlProperty(namespace = "TallyUDF", localName = "TALLYMESSAGE ")
    private TallyMessage tallyMessage;

    @JacksonXmlProperty(namespace = "TallyUDF", localName = "TALLYMESSAGE  ")
    private TallyMessage tallyMessage1;*/

    public List<TallyMessageVoucher> getTallyMessageVoucher() {
        return tallyMessageVoucher;
    }

    public void setTallyMessageVoucher(List<TallyMessageVoucher> tallyMessageVoucher) {
        this.tallyMessageVoucher = tallyMessageVoucher;
    }

   /* public TallyMessage getTallyMessage() {
        return tallyMessage;
    }

    public void setTallyMessage(TallyMessage tallyMessage) {
        this.tallyMessage = tallyMessage;
    }

    public TallyMessage getTallyMessage1() {
        return tallyMessage1;
    }

    public void setTallyMessage1(TallyMessage tallyMessage1) {
        this.tallyMessage1 = tallyMessage1;
    }*/


}
