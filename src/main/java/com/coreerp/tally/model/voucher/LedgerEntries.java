package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LedgerEntries {

    @JsonProperty("OLDAUDITENTRYIDS.LIST ")
    private OldAuditEntryList oldAuditEntryList;

    @JsonProperty("LEDGERNAME")
    private String ledgerName ;

    @JsonProperty("ISDEEMEDPOSITIVE")
    private String isDeemedPositive ;

    @JsonProperty("LEDGERFROMITEM")
    private String ledgerFromItem ;

    @JsonProperty("ISPARTYLEDGER")
    private String isPartyLedger ;

    @JsonProperty("ISLASTDEEMEDPOSITIVE")
    private String isLastdeemedPositive ;

    @JsonProperty("ISCAPVATNOTCLAIMED")
    private String isCapvatNotClaimed ;

    @JsonProperty("AMOUNT")
    private Double amount ;

    @JsonProperty("NAME")
    private String name ;

    @JsonProperty("BILLTYPE")
    private String billType;


    @JsonProperty("TDSDEDUCTEEISSPECIALRATE")
    private String tdsdeductEeisspecialRate ;


    @JsonProperty("AMOUNT ")
    private Double amountTotal ;

    public OldAuditEntryList getOldAuditEntryList() {
        return oldAuditEntryList;
    }

    public void setOldAuditEntryList(OldAuditEntryList oldAuditEntryList) {
        this.oldAuditEntryList = oldAuditEntryList;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getTdsdeductEeisspecialRate() {
        return tdsdeductEeisspecialRate;
    }

    public void setTdsdeductEeisspecialRate(String tdsdeductEeisspecialRate) {
        this.tdsdeductEeisspecialRate = tdsdeductEeisspecialRate;
    }



    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getIsDeemedPositive() {
        return isDeemedPositive;
    }

    public void setIsDeemedPositive(String isDeemedPositive) {
        this.isDeemedPositive = isDeemedPositive;
    }

    public String getLedgerFromItem() {
        return ledgerFromItem;
    }

    public void setLedgerFromItem(String ledgerFromItem) {
        this.ledgerFromItem = ledgerFromItem;
    }

    public String getIsPartyLedger() {
        return isPartyLedger;
    }

    public void setIsPartyLedger(String isPartyLedger) {
        this.isPartyLedger = isPartyLedger;
    }

    public String getIsLastdeemedPositive() {
        return isLastdeemedPositive;
    }

    public void setIsLastdeemedPositive(String isLastdeemedPositive) {
        this.isLastdeemedPositive = isLastdeemedPositive;
    }

    public String getIsCapvatNotClaimed() {
        return isCapvatNotClaimed;
    }

    public void setIsCapvatNotClaimed(String isCapvatNotClaimed) {
        this.isCapvatNotClaimed = isCapvatNotClaimed;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(Double amountTotal) {
        this.amountTotal = amountTotal;
    }


}
