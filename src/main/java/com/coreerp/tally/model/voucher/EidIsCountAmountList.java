package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class EidIsCountAmountList {

    @JacksonXmlProperty(isAttribute = true,localName="DESC" )
    private String desc="`EI DiscountAmt`"  ;

    @JacksonXmlProperty(isAttribute = true, localName = "ISLIST")
    private String isList="YES" ;

    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type="Amount" ;

    @JacksonXmlProperty(isAttribute = true, localName = "INDEX")
    private String index ="10003";

    @JsonProperty("UDF:EIDISCOUNTAMT ")
    EidIsAmount eidIsAmount;


    public EidIsAmount getEidIsAmount() {
        return eidIsAmount;
    }

    public void setEidIsAmount(EidIsAmount eidIsAmount) {
        this.eidIsAmount = eidIsAmount;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getIsList() {
        return isList;
    }

    public void setIsList(String isList) {
        this.isList = isList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }


}
