package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Company {

    @JsonProperty("REMOTECMPINFO.LIST")
    private RemoteCmpInfo remoteCmpInfo;


    public RemoteCmpInfo getRemoteCmpInfo() {
        return remoteCmpInfo;
    }

    public void setRemoteCmpInfo(RemoteCmpInfo remoteCmpInfo) {
        this.remoteCmpInfo = remoteCmpInfo;
    }


}
