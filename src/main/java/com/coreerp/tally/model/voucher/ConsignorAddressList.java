package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ConsignorAddressList {
    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type ="String";

    @JsonProperty("CONSIGNORADDRESS")
    private String consignorAddress ;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }
}
