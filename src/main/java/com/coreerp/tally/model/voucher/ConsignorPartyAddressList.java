package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ConsignorPartyAddressList {

    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type ="String";

    @JsonProperty("CONSIGNORADDRESS ")
    private String consignorAddress ;



    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }
}
