package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class BasicOrderTermsList {

    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type ="String";

    @JsonProperty("BASICORDERTERMS")
    private String basicOrderTerms ;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBasicOrderTerms() {
        return basicOrderTerms;
    }

    public void setBasicOrderTerms(String basicOrderTerms) {
        this.basicOrderTerms = basicOrderTerms;
    }
}
