package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Body {

    @JsonProperty("IMPORTDATA")
    private ImportData importData;

    public ImportData getImportData() {
        return importData;
    }

    public void setImportData(ImportData importData) {
        this.importData = importData;
    }
}
