package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EwayBillDetailsList {

    @JsonProperty("CONSIGNORADDRESS.LIST")
ConsignorAddressList consignorAddressList;

    @JsonProperty("CONSIGNORADDRESS.LIST ")
    ConsignorPartyAddressList consignorPartyAddressList;

    @JsonProperty("DOCUMENTTYPE")
    private String documentType;

    @JsonProperty("CONSIGNEEGSTIN")
    private String consigneeGstin;

    @JsonProperty("CONSIGNEESTATENAME")
    private String consigneeStateName;

    @JsonProperty("CONSIGNEEPINCODE")
    private String consigneePinCode;

    @JsonProperty("BILLNUMBER")
    private String billNumber;

    @JsonProperty("SUBTYPE")
    private String subType;

    @JsonProperty("CONSIGNORNAME")
    private String consignorName;

    @JsonProperty("CONSIGNORPINCODE")
    private String consignorPinCode;

    @JsonProperty("CONSIGNORGSTIN")
    private String consignorGstin;

    @JsonProperty("CONSIGNORSTATENAME")
    private String consignorStateName;

    @JsonProperty("CONSIGNEENAME")
    private String consigneeName;

    @JsonProperty("SHIPPEDFROMSTATE")
    private String shippedFromState;

    @JsonProperty("SHIPPEDTOSTATE")
    private String shippedToState;

    @JsonProperty("IGNOREGSTINVALIDATION")
    private String ignoreGstInvalidation;


    @JsonProperty("TRANSPORTDETAILS.LIST")
    TransportDetailsList transportDetailsList;

    public TransportDetailsList getTransportDetailsList() {
        return transportDetailsList;
    }

    public void setTransportDetailsList(TransportDetailsList transportDetailsList) {
        this.transportDetailsList = transportDetailsList;
    }

    public String getIgnoreGstInvalidation() {
        return ignoreGstInvalidation;
    }

    public void setIgnoreGstInvalidation(String ignoreGstInvalidation) {
        this.ignoreGstInvalidation = ignoreGstInvalidation;
    }

    public ConsignorPartyAddressList getConsignorPartyAddressList() {
        return consignorPartyAddressList;
    }

    public void setConsignorPartyAddressList(ConsignorPartyAddressList consignorPartyAddressList) {
        this.consignorPartyAddressList = consignorPartyAddressList;
    }

    public ConsignorAddressList getConsignorAddressList() {
        return consignorAddressList;
    }

    public void setConsignorAddressList(ConsignorAddressList consignorAddressList) {
        this.consignorAddressList = consignorAddressList;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getConsigneeGstin() {
        return consigneeGstin;
    }

    public void setConsigneeGstin(String consigneeGstin) {
        this.consigneeGstin = consigneeGstin;
    }

    public String getConsigneeStateName() {
        return consigneeStateName;
    }

    public void setConsigneeStateName(String consigneeStateName) {
        this.consigneeStateName = consigneeStateName;
    }


    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsigneePinCode() {
        return consigneePinCode;
    }

    public void setConsigneePinCode(String consigneePinCode) {
        this.consigneePinCode = consigneePinCode;
    }

    public String getConsignorPinCode() {
        return consignorPinCode;
    }

    public void setConsignorPinCode(String consignorPinCode) {
        this.consignorPinCode = consignorPinCode;
    }

    public String getConsignorGstin() {
        return consignorGstin;
    }

    public void setConsignorGstin(String consignorGstin) {
        this.consignorGstin = consignorGstin;
    }

    public String getConsignorStateName() {
        return consignorStateName;
    }

    public void setConsignorStateName(String consignorStateName) {
        this.consignorStateName = consignorStateName;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getShippedFromState() {
        return shippedFromState;
    }

    public void setShippedFromState(String shippedFromState) {
        this.shippedFromState = shippedFromState;
    }

    public String getShippedToState() {
        return shippedToState;
    }

    public void setShippedToState(String shippedToState) {
        this.shippedToState = shippedToState;
    }
}
