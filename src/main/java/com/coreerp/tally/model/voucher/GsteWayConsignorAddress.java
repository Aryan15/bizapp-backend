package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GsteWayConsignorAddress {
    @JsonProperty("TEMPCONSIGNORADDRESS")
    private String tempConsignorAddress ;

    public String getTempConsignorAddress() {
        return tempConsignorAddress;
    }

    public void setTempConsignorAddress(String tempConsignorAddress) {
        this.tempConsignorAddress = tempConsignorAddress;
    }
}
