package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InvoiceOrderList {
   @JsonProperty("BASICORDERDATE")
    private String basicOrderDate;

    public String getBasicOrderDate() {
        return basicOrderDate;
    }

    public void setBasicOrderDate(String basicOrderDate) {
        this.basicOrderDate = basicOrderDate;
    }

    @JsonProperty("BASICPURCHASEORDERNO")
    private String basicPurchaseOrderNo;

    public String getBasicPurchaseOrderNo() {
        return basicPurchaseOrderNo;
    }

    public void setBasicPurchaseOrderNo(String basicPurchaseOrderNo) {
        this.basicPurchaseOrderNo = basicPurchaseOrderNo;
    }
}
