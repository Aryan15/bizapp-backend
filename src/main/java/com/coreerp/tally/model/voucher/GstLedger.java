package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GstLedger {

    @JsonProperty("LEDGERNAME")
    private String ledgerName ;

    @JsonProperty("ISDEEMEDPOSITIVE")
    private String isDeemedPositive ;

    @JsonProperty("LEDGERFROMITEM")
    private String ledgetFromItem;

    @JsonProperty("ISPARTYLEDGER")
    private String isPartyLedger;

    @JsonProperty("ISLASTDEEMEDPOSITIVE")
    private String isLastDeemedPositive ;

    @JsonProperty("ISCAPVATNOTCLAIMED")
    private String isCapVatNotClaimed;

    @JsonProperty("AMOUNT")
    private Double amount ;

    @JsonProperty("VATEXPAMOUNT")
    private Double vatexpAmount ;

    @JsonProperty("GSTDUTYAMOUNT")
    private Double gstDutyAmount;

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getIsDeemedPositive() {
        return isDeemedPositive;
    }

    public void setIsDeemedPositive(String isDeemedPositive) {
        this.isDeemedPositive = isDeemedPositive;
    }

    public String getLedgetFromItem() {
        return ledgetFromItem;
    }

    public void setLedgetFromItem(String ledgetFromItem) {
        this.ledgetFromItem = ledgetFromItem;
    }

    public String getIsPartyLedger() {
        return isPartyLedger;
    }

    public void setIsPartyLedger(String isPartyLedger) {
        this.isPartyLedger = isPartyLedger;
    }

    public String getIsLastDeemedPositive() {
        return isLastDeemedPositive;
    }

    public void setIsLastDeemedPositive(String isLastDeemedPositive) {
        this.isLastDeemedPositive = isLastDeemedPositive;
    }

    public String getIsCapVatNotClaimed() {
        return isCapVatNotClaimed;
    }

    public void setIsCapVatNotClaimed(String isCapVatNotClaimed) {
        this.isCapVatNotClaimed = isCapVatNotClaimed;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getVatexpAmount() {
        return vatexpAmount;
    }

    public void setVatexpAmount(Double vatexpAmount) {
        this.vatexpAmount = vatexpAmount;
    }

    public Double getGstDutyAmount() {
        return gstDutyAmount;
    }

    public void setGstDutyAmount(Double gstDutyAmount) {
        this.gstDutyAmount = gstDutyAmount;
    }
}
