package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class RoundOffBasicRateOfInvoiceTax {

    @JacksonXmlProperty(isAttribute = true, localName = "Type")
    private String Type = "Number";


    @JsonProperty("BASICRATEOFINVOICETAX")
    private Double roundOffBasicRateOfInvoiceTaxPercent;

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public Double getRoundOffBasicRateOfInvoiceTaxPercent() {
        return roundOffBasicRateOfInvoiceTaxPercent;
    }

    public void setRoundOffBasicRateOfInvoiceTaxPercent(Double roundOffBasicRateOfInvoiceTaxPercent) {
        this.roundOffBasicRateOfInvoiceTaxPercent = roundOffBasicRateOfInvoiceTaxPercent;
    }
}

