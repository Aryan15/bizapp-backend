package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransportDetailsList {

    @JsonProperty("TRANSPORTMODE")
    private String transportMode ="ROAD";

    @JsonProperty("VEHICLENUMBER")
    private String vehicleNumber ="VEHNYM222";

    @JsonProperty("VEHICLETYPE")
    private String vehicleType ="Regular";

    @JsonProperty("IGNOREVEHICLENOVALIDATION")
    private String ignoreVehicleNoValidation ="NO";

    public String getTransportMode() {
        return transportMode;
    }

    public void setTransportMode(String transportMode) {
        this.transportMode = transportMode;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getIgnoreVehicleNoValidation() {
        return ignoreVehicleNoValidation;
    }

    public void setIgnoreVehicleNoValidation(String ignoreVehicleNoValidation) {
        this.ignoreVehicleNoValidation = ignoreVehicleNoValidation;
    }






}
