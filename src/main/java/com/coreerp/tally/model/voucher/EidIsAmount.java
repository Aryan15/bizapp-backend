package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

public class EidIsAmount {

    @JacksonXmlProperty(isAttribute = true,localName="DESC" )
    private String desc ="`EI DiscountAmt`";


    @JacksonXmlText
    private String discountAmount;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }
}
