package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Inventory {

    @JsonProperty("STOCKITEMNAME")
    private String stockItemName ;

    @JsonProperty("ISDEEMEDPOSITIVE")
    private String isDeemedPositive;

    @JsonProperty("ISLASTDEEMEDPOSITIVE")
    private String isLastDeemedPositive ;

    @JsonProperty("ISAUTONEGATE")
    private String isAutoneGate;

    @JsonProperty("ISSCRAP")
    private String isScrap ;

    @JsonProperty("RATE")
    private String rate;

    @JsonProperty("AMOUNT")
    private Double amount ;

    @JsonProperty("ACTUALQTY")
    private String actualQty ;

    @JsonProperty("BILLEDQTY")
    private String billedQty ;

    @JsonProperty("DISCOUNT")
    private String discount ;

    @JsonProperty("ISINCLTAXRATEFIELDEDITED")
    private String isInclTaxRateFieldEdited ;

    @JsonProperty("ISEXCISEOVERRIDEDETAILS")
    private String isExciseOverRideDetails;

    @JsonProperty("ACCOUNTINGALLOCATIONS.LIST")
    private AccountingAllocations accountingAllocations;

    @JsonProperty("UDF:EIDISCOUNTRATE.LIST")
    private EidIsCountRateList eidIsCountRate;

    @JsonProperty("UDF:EIDISCOUNTAMT.LIST")
    private EidIsCountAmountList eidIsCountAmount;


    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

  public EidIsCountAmountList getEidIsCountAmount() {
        return eidIsCountAmount;
    }

    public void setEidIsCountAmount(EidIsCountAmountList eidIsCountAmount) {
        this.eidIsCountAmount = eidIsCountAmount;
    }

    public EidIsCountRateList getEidIsCountRate() {
        return eidIsCountRate;
    }

    public void setEidIsCountRate(EidIsCountRateList eidIsCountRate) {
        this.eidIsCountRate = eidIsCountRate;
    }

    public String getStockItemName() {
        return stockItemName;
    }

    public String getIsDeemedPositive() {
        return isDeemedPositive;
    }

    public void setIsDeemedPositive(String isDeemedPositive) {
        this.isDeemedPositive = isDeemedPositive;
    }

    public String getIsLastDeemedPositive() {
        return isLastDeemedPositive;
    }

    public void setIsLastDeemedPositive(String isLastDeemedPositive) {
        this.isLastDeemedPositive = isLastDeemedPositive;
    }

    public String getIsAutoneGate() {
        return isAutoneGate;
    }

    public void setIsAutoneGate(String isAutoneGate) {
        this.isAutoneGate = isAutoneGate;
    }

    public String getIsScrap() {
        return isScrap;
    }

    public void setIsScrap(String isScrap) {
        this.isScrap = isScrap;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getActualQty() {
        return actualQty;
    }

    public void setActualQty(String actualQty) {
        this.actualQty = actualQty;
    }

    public String getBilledQty() {
        return billedQty;
    }

    public void setBilledQty(String billedQty) {
        this.billedQty = billedQty;
    }

    public String getIsInclTaxRateFieldEdited() {
        return isInclTaxRateFieldEdited;
    }

    public void setIsInclTaxRateFieldEdited(String isInclTaxRateFieldEdited) {
        this.isInclTaxRateFieldEdited = isInclTaxRateFieldEdited;
    }

    public String getIsExciseOverRideDetails() {
        return isExciseOverRideDetails;
    }

    public void setIsExciseOverRideDetails(String isExciseOverRideDetails) {
        this.isExciseOverRideDetails = isExciseOverRideDetails;
    }

    public void setStockItemName(String stockItemName) {
        this.stockItemName = stockItemName;
    }

    public AccountingAllocations getAccountingAllocations() {
        return accountingAllocations;
    }

    public void setAccountingAllocations(AccountingAllocations accountingAllocations) {
        this.accountingAllocations = accountingAllocations;
    }

}
