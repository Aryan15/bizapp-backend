package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountingAllocations {

    @JsonProperty("LEDGERNAME")
    private String ledgerName ;

    @JsonProperty("ISDEEMEDPOSITIVE")
    private String isDeemedPositive;

    @JsonProperty("LEDGERFROMITEM")
    private String ledgerFromtem ;

    @JsonProperty("ISPARTYLEDGER")
    private String isPartyLedger;

    @JsonProperty("ISLASTDEEMEDPOSITIVE")
    private String isLastDeemedPositive;

    @JsonProperty("ISCAPVATNOTCLAIMED")
    private String isCapvatNotClaimed;

    @JsonProperty("AMOUNT")
    private Double amount;

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getIsDeemedPositive() {
        return isDeemedPositive;
    }

    public void setIsDeemedPositive(String isDeemedPositive) {
        this.isDeemedPositive = isDeemedPositive;
    }

    public String getLedgerFromtem() {
        return ledgerFromtem;
    }

    public void setLedgerFromtem(String ledgerFromtem) {
        this.ledgerFromtem = ledgerFromtem;
    }

    public String getIsPartyLedger() {
        return isPartyLedger;
    }

    public void setIsPartyLedger(String isPartyLedger) {
        this.isPartyLedger = isPartyLedger;
    }

    public String getIsLastDeemedPositive() {
        return isLastDeemedPositive;
    }

    public void setIsLastDeemedPositive(String isLastDeemedPositive) {
        this.isLastDeemedPositive = isLastDeemedPositive;
    }

    public String getIsCapvatNotClaimed() {
        return isCapvatNotClaimed;
    }

    public void setIsCapvatNotClaimed(String isCapvatNotClaimed) {
        this.isCapvatNotClaimed = isCapvatNotClaimed;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
