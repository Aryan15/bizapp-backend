package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

public class RequestDesc {

    @JsonProperty("REPORTNAME")
    private String reportName="Vouchers";

    @JsonProperty("STATICVARIABLES")
    StaticVariables staticVariables;

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public StaticVariables getStaticVariables() {
        return staticVariables;
    }

    public void setStaticVariables(StaticVariables staticVariables) {
        this.staticVariables = staticVariables;
    }
}
