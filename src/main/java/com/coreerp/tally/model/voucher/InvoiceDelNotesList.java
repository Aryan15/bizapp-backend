package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class InvoiceDelNotesList {

   @JsonProperty("BASICSHIPPINGDATE")
    private String basicShippingDate ;



    @JsonProperty("BASICSHIPDELIVERYNOTE")
    private String basicShipDeliveryNote ;

    public String getBasicShippingDate() {
        return basicShippingDate;
    }

    public void setBasicShippingDate(String basicShippingDate) {
        this.basicShippingDate = basicShippingDate;
    }

    public String getBasicShipDeliveryNote() {
        return basicShipDeliveryNote;
    }

    public void setBasicShipDeliveryNote(String basicShipDeliveryNote) {
        this.basicShipDeliveryNote = basicShipDeliveryNote;
    }
}
