package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;


//@JacksonXmlRootElement(namespace = "TallyUDF", localName = "TALLYMESSAGE")
public class TallyMessageVoucher {

    @JsonProperty("VOUCHER")
//    @JacksonXmlProperty(namespace = "TallyUDF", localName = "VOUCHER")
    private Voucher voucher;

    public Voucher getVoucher() {
        return voucher;
    }

    public void setVoucher(Voucher voucher) {
        this.voucher = voucher;
    }
}
