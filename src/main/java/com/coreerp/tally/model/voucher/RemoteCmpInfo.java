package com.coreerp.tally.model.voucher;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class RemoteCmpInfo {
    @JacksonXmlProperty(isAttribute = true, localName = "MERGE")
    private String merge ="Yes";

    @JsonProperty("REMOTECMPNAME")
    private String remoteCmpName ="Welkin Softtech";

    @JsonProperty("REMOTECMPSTATE")
    private String remoteCmpState ="Karnataka";

    public String getMerge() {
        return merge;
    }

    public void setMerge(String merge) {
        this.merge = merge;
    }

    public String getRemoteCmpName() {
        return remoteCmpName;
    }

    public void setRemoteCmpName(String remoteCmpName) {
        this.remoteCmpName = remoteCmpName;
    }

    public String getRemoteCmpState() {
        return remoteCmpState;
    }

    public void setRemoteCmpState(String remoteCmpState) {
        this.remoteCmpState = remoteCmpState;
    }
}
