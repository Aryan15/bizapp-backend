package com.coreerp.tally.model.bank;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "ENVELOPE")
public class BankEnvelope {

    @JsonProperty("HEADER")
  private   BankHeader bankHeader;

    @JsonProperty("BODY")
  private   BankBody bankBody;


    public BankHeader getBankHeader() {
        return bankHeader;
    }

    public void setBankHeader(BankHeader bankHeader) {
        this.bankHeader = bankHeader;
    }

    public BankBody getBankBody() {
        return bankBody;
    }

    public void setBankBody(BankBody bankBody) {
        this.bankBody = bankBody;
    }
}
