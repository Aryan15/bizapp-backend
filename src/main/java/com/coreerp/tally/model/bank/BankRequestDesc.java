package com.coreerp.tally.model.bank;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BankRequestDesc {

    @JsonProperty("REPORTNAME")
    private String reportName ;

    @JsonProperty("STATICVARIABLES")
    private BankStaticVariable bankStaticVariable;

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public BankStaticVariable getBankStaticVariable() {
        return bankStaticVariable;
    }

    public void setBankStaticVariable(BankStaticVariable bankStaticVariable) {
        this.bankStaticVariable = bankStaticVariable;
    }
}
