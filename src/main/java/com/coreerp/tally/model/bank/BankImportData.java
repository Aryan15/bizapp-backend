package com.coreerp.tally.model.bank;


import com.fasterxml.jackson.annotation.JsonProperty;

public class BankImportData {

    @JsonProperty("REQUESTDESC")
    private BankRequestDesc bankRequestDesc;

    @JsonProperty("REQUESTDATA")
    private BankRequestData bankRequestData;


    public BankRequestDesc getBankRequestDesc() {
        return bankRequestDesc;
    }

    public void setBankRequestDesc(BankRequestDesc bankRequestDesc) {
        this.bankRequestDesc = bankRequestDesc;
    }

    public BankRequestData getBankRequestData() {
        return bankRequestData;
    }

    public void setBankRequestData(BankRequestData bankRequestData) {
        this.bankRequestData = bankRequestData;
    }
}
