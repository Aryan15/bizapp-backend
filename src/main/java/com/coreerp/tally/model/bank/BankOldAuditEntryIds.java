package com.coreerp.tally.model.bank;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class BankOldAuditEntryIds {



    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type = "Number";

    @JsonProperty("OLDAUDITENTRYIDS")
    private Integer oldAuditEntryIds ;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getOldAuditEntryIds() {
        return oldAuditEntryIds;
    }

    public void setOldAuditEntryIds(Integer oldAuditEntryIds) {
        this.oldAuditEntryIds = oldAuditEntryIds;
    }
}
