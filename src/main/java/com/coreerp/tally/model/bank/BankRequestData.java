package com.coreerp.tally.model.bank;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class BankRequestData {

    @JacksonXmlProperty(namespace = "TallyUDF", localName = "TALLYMESSAGE")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<BankTallyMessage> bankTallyMessages;

    public List<BankTallyMessage> getBankTallyMessages() {
        return bankTallyMessages;
    }

    public void setBankTallyMessages(List<BankTallyMessage> bankTallyMessages) {
        this.bankTallyMessages = bankTallyMessages;
    }
}
