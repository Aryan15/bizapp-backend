package com.coreerp.tally.model.bank;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BankName {


    @JsonProperty("NAME")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
