package com.coreerp.tally.model.bank;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "ENVELOPE")
public class BankHeader {

    @JsonProperty("TALLLYREQUEST")
    private String tallyRequest;

    public String getTallyRequest() {
        return tallyRequest;
    }

    public void setTallyRequest(String tallyRequest) {
        this.tallyRequest = tallyRequest;
    }
}
