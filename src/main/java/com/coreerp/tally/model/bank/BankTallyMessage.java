package com.coreerp.tally.model.bank;

import com.coreerp.tally.model.common.TallyMessage;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BankTallyMessage implements TallyMessage {
    @JsonProperty("LEDGER")
    private BankLeadger bankLeadger;

    public BankLeadger getBankLeadger() {
        return bankLeadger;
    }

    public void setBankLeadger(BankLeadger bankLeadger) {
        this.bankLeadger = bankLeadger;
    }
}
