package com.coreerp.tally.model.bank;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BankStaticVariable {


    @JsonProperty("SVCURRENTCOMPANY")
    private String svCurrentCompany ;

    public String getSvCurrentCompany() {
        return svCurrentCompany;
    }

    public void setSvCurrentCompany(String svCurrentCompany) {
        this.svCurrentCompany = svCurrentCompany;
    }
}
