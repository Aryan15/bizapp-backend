package com.coreerp.tally.model.bank;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class BankLanguageName {


    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type = "String";


    @JsonProperty("NAME.LIST")
    private BankName bankName;

    @JsonProperty("LANGUAGEID")
    private Integer languageId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BankName getBankName() {
        return bankName;
    }

    public void setBankName(BankName bankName) {
        this.bankName = bankName;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }
}
