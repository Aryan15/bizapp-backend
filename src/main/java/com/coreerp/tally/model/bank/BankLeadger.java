package com.coreerp.tally.model.bank;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class BankLeadger {


    @JacksonXmlProperty(isAttribute = true, localName = "NAME")
    private String name;

    @JacksonXmlProperty(isAttribute = true, localName = "RESERVEDNAME")
    private String reservedName = "";

    @JsonProperty("MAILINGNAME.LIST")
    private BankMailingName bankMailingName;


    @JsonProperty("OLDAUDITENTRYIDS.LIST")
    private BankOldAuditEntryIds bankOldAuditEntryIds;

    @JsonProperty("STARTINGFROM")
    private String startingFrom;

    @JsonProperty("CURRENCYNAME")
    private String currencyName;


    @JsonProperty("COUNTRYNAME")
    private String countryName;

    @JsonProperty("PARENT")
    private String parent;

    @JsonProperty("IFSCODE")
    private String ifsCode;

    @JsonProperty("TAXTYPE")
    private String taxType;

    @JsonProperty("BANKDETAILS")
    private String bankDetails;

    @JsonProperty("BANKBRANCHNAME")
    private String bankBranchName;

    @JsonProperty("COUNTRYOFRESIDENCE")
    private String countryOfResidence;

    @JsonProperty("SERVICECATEGORY")
    private String serviceCategory;

    @JsonProperty("BANKINGCONFIGBANK")
    private String bankingConfigBank;

    @JsonProperty("BANKINGCONFIGBANKID")
    private String bankingConfigBankId;

    @JsonProperty("BANKACCHOLDERNAME")
    private String bankAccHolderName;

    @JsonProperty("LEDSTATENAME")
    private String bankStateName;

    @JsonProperty("ISBILLWISEON")
    private String isBillWiseon;

    @JsonProperty("ISCOSTCENTRESON")
    private String isCostcentereson;

    @JsonProperty("ASORIGINAL")
    private String asOriginal;

    @JsonProperty("ISABCENABLED")
    private String isAbcenabled;

    @JsonProperty("LANGUAGENAME.LIST")
    private BankLanguageName bankLanguageName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReservedName() {
        return reservedName;
    }

    public void setReservedName(String reservedName) {
        this.reservedName = reservedName;
    }

    public BankMailingName getBankMailingName() {
        return bankMailingName;
    }

    public void setBankMailingName(BankMailingName bankMailingName) {
        this.bankMailingName = bankMailingName;
    }

    public BankOldAuditEntryIds getBankOldAuditEntryIds() {
        return bankOldAuditEntryIds;
    }

    public void setBankOldAuditEntryIds(BankOldAuditEntryIds bankOldAuditEntryIds) {
        this.bankOldAuditEntryIds = bankOldAuditEntryIds;
    }

    public String getStartingFrom() {
        return startingFrom;
    }

    public void setStartingFrom(String startingFrom) {
        this.startingFrom = startingFrom;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getIfsCode() {
        return ifsCode;
    }

    public void setIfsCode(String ifsCode) {
        this.ifsCode = ifsCode;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(String bankDetails) {
        this.bankDetails = bankDetails;
    }

    public String getBankBranchName() {
        return bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    public String getCountryOfResidence() {
        return countryOfResidence;
    }

    public void setCountryOfResidence(String countryOfResidence) {
        this.countryOfResidence = countryOfResidence;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    public String getBankingConfigBank() {
        return bankingConfigBank;
    }

    public void setBankingConfigBank(String bankingConfigBank) {
        this.bankingConfigBank = bankingConfigBank;
    }

    public String getBankingConfigBankId() {
        return bankingConfigBankId;
    }

    public void setBankingConfigBankId(String bankingConfigBankId) {
        this.bankingConfigBankId = bankingConfigBankId;
    }

    public String getBankAccHolderName() {
        return bankAccHolderName;
    }

    public void setBankAccHolderName(String bankAccHolderName) {
        this.bankAccHolderName = bankAccHolderName;
    }

    public String getBankStateName() {
        return bankStateName;
    }

    public void setBankStateName(String bankStateName) {
        this.bankStateName = bankStateName;
    }

    public String getIsBillWiseon() {
        return isBillWiseon;
    }

    public void setIsBillWiseon(String isBillWiseon) {
        this.isBillWiseon = isBillWiseon;
    }

    public String getIsCostcentereson() {
        return isCostcentereson;
    }

    public void setIsCostcentereson(String isCostcentereson) {
        this.isCostcentereson = isCostcentereson;
    }

    public String getAsOriginal() {
        return asOriginal;
    }

    public void setAsOriginal(String asOriginal) {
        this.asOriginal = asOriginal;
    }

    public String getIsAbcenabled() {
        return isAbcenabled;
    }

    public void setIsAbcenabled(String isAbcenabled) {
        this.isAbcenabled = isAbcenabled;
    }

    public BankLanguageName getBankLanguageName() {
        return bankLanguageName;
    }

    public void setBankLanguageName(BankLanguageName bankLanguageName) {
        this.bankLanguageName = bankLanguageName;
    }
}