package com.coreerp.tally.model.bank;


import com.fasterxml.jackson.annotation.JsonProperty;

public class BankBody {
    @JsonProperty("IMPORTDATA")
    private BankImportData bankImportData;

    public BankImportData getBankImportData() {
        return bankImportData;
    }

    public void setBankImportData(BankImportData bankImportData) {
        this.bankImportData = bankImportData;
    }
}
