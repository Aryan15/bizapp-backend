package com.coreerp.tally.model.stockitem;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StockItemBody {

    @JsonProperty("IMPORTDATA")
    private   StockItemImportData stockItemImportData;

    public StockItemImportData getStockItemImportData() {
        return stockItemImportData;
    }

    public void setStockItemImportData(StockItemImportData stockItemImportData) {
        this.stockItemImportData = stockItemImportData;
    }
}
