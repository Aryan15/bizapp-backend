package com.coreerp.tally.model.stockitem;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StockItemStaticVariable {

    @JsonProperty("SVCURRENTCOMPANY")
    private String svCurrentCompany;

    public String getSvCurrentCompany() {
        return svCurrentCompany;
    }

    public void setSvCurrentCompany(String svCurrentCompany) {
        this.svCurrentCompany = svCurrentCompany;
    }
}
