package com.coreerp.tally.model.stockitem;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StockItemRequestDesc {

    @JsonProperty("REPORTNAME")
    private String reportName;

    @JsonProperty("STATICVARIABLES")
    private StockItemStaticVariable stockItemStaticVariable;

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public StockItemStaticVariable getStockItemStaticVariable() {
        return stockItemStaticVariable;
    }

    public void setStockItemStaticVariable(StockItemStaticVariable stockItemStaticVariable) {
        this.stockItemStaticVariable = stockItemStaticVariable;
    }
}
