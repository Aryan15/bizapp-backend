package com.coreerp.tally.model.stockitem;


import com.fasterxml.jackson.annotation.JsonProperty;

public class StockItemImportData {

    @JsonProperty("REQUESTDATA")
    private StockItemRequestData stockItemRequestData;

    @JsonProperty("REQUESTDESC")
    private StockItemRequestDesc stockItemRequestDesc;

    public StockItemRequestData getStockItemRequestData() {
        return stockItemRequestData;
    }

    public void setStockItemRequestData(StockItemRequestData stockItemRequestData) {
        this.stockItemRequestData = stockItemRequestData;
    }

    public StockItemRequestDesc getStockItemRequestDesc() {
        return stockItemRequestDesc;
    }

    public void setStockItemRequestDesc(StockItemRequestDesc stockItemRequestDesc) {
        this.stockItemRequestDesc = stockItemRequestDesc;
    }
}
