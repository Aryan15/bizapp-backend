package com.coreerp.tally.model.stockitem;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StockItemLanguageName {

    @JsonProperty("NAME.LIST ")
    private StockItemName stockItemName ;

    @JsonProperty("LANGUAGEID")
    private Integer languageId ;

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public StockItemName getStockItemName() {
        return stockItemName;
    }

    public void setStockItemName(StockItemName stockItemName) {
        this.stockItemName = stockItemName;
    }
}
