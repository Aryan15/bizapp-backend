package com.coreerp.tally.model.stockitem;



import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class StockItemRequestData {

    @JacksonXmlProperty(namespace = "TallyUDF", localName = "TALLYMESSAGE")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<StockItemTallyMessage> stockItemTallyMessages;

    public List<StockItemTallyMessage> getStockItemTallyMessages() {
        return stockItemTallyMessages;
    }

    public void setStockItemTallyMessages(List<StockItemTallyMessage> stockItemTallyMessages) {
        this.stockItemTallyMessages = stockItemTallyMessages;
    }
}
