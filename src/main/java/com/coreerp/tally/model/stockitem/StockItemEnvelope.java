package com.coreerp.tally.model.stockitem;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;


@JacksonXmlRootElement(localName = "ENVELOPE")
public class StockItemEnvelope {

    @JsonProperty("HEADER")
    private  StockItemHeader stockItemHeader;

    @JsonProperty("BODY")
    private  StockItemBody stockItemBody;

    public StockItemHeader getStockItemHeader() {
        return stockItemHeader;
    }

    public void setStockItemHeader(StockItemHeader stockItemHeader) {
        this.stockItemHeader = stockItemHeader;
    }

    public StockItemBody getStockItemBody() {
        return stockItemBody;
    }

    public void setStockItemBody(StockItemBody stockItemBody) {
        this.stockItemBody = stockItemBody;
    }
}
