package com.coreerp.tally.model.stockitem;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class StockItemItem {

    @JacksonXmlProperty(isAttribute = true, localName = "NAME")
    private String name ;

    @JacksonXmlProperty(isAttribute = true, localName = "RESERVEDNAME")
    private String reservedName ="";

    @JsonProperty("OLDAUDITENTRYIDS.LIST")
    private StockItemOldAuditEntryIds stockItemOldAuditEntryIds;


    @JsonProperty("PARENT")
    private String parent ;

    @JsonProperty("GSTAPPLICABLE")
    private String gstApplicable ;

    @JsonProperty("GSTTYPEOFSUPPLY")
    private String gstTypeOfSupply ;

    @JsonProperty("EXCISEAPPLICABILITY")
    private String exciseApplicability ;

    @JsonProperty("VATAPPLICABLE")
    private String vatApplicable ;

    @JsonProperty("COSTINGMETHOD")
    private String costingMethod ;

    @JsonProperty("VALUATIONMETHOD")
    private String valuationMethod ;

    @JsonProperty("BASEUNITS")
    private String baseUnits ;

    @JsonProperty("ISPERISHABLEON")
    private String isPerishableon ;

    @JsonProperty("ISENTRYTAXAPPLICABLE")
    private String isEntyTaxApplicable ;

    @JsonProperty("ISUPDATINGTARGETID")
    private String isUpdatingTargetId ;

    @JsonProperty("ASORIGINAL")
    private String asOriginal ;

    @JsonProperty("LANGUAGENAME.LIST")
    private StockItemLanguageName stockItemLanguageName ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReservedName() {
        return reservedName;
    }

    public void setReservedName(String reservedName) {
        this.reservedName = reservedName;
    }

    public StockItemOldAuditEntryIds getStockItemOldAuditEntryIds() {
        return stockItemOldAuditEntryIds;
    }

    public void setStockItemOldAuditEntryIds(StockItemOldAuditEntryIds stockItemOldAuditEntryIds) {
        this.stockItemOldAuditEntryIds = stockItemOldAuditEntryIds;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getGstApplicable() {
        return gstApplicable;
    }

    public void setGstApplicable(String gstApplicable) {
        this.gstApplicable = gstApplicable;
    }

    public String getGstTypeOfSupply() {
        return gstTypeOfSupply;
    }

    public void setGstTypeOfSupply(String gstTypeOfSupply) {
        this.gstTypeOfSupply = gstTypeOfSupply;
    }

    public String getExciseApplicability() {
        return exciseApplicability;
    }

    public void setExciseApplicability(String exciseApplicability) {
        this.exciseApplicability = exciseApplicability;
    }

    public String getVatApplicable() {
        return vatApplicable;
    }

    public void setVatApplicable(String vatApplicable) {
        this.vatApplicable = vatApplicable;
    }

    public String getCostingMethod() {
        return costingMethod;
    }

    public void setCostingMethod(String costingMethod) {
        this.costingMethod = costingMethod;
    }

    public String getValuationMethod() {
        return valuationMethod;
    }

    public void setValuationMethod(String valuationMethod) {
        this.valuationMethod = valuationMethod;
    }

    public String getBaseUnits() {
        return baseUnits;
    }

    public void setBaseUnits(String baseUnits) {
        this.baseUnits = baseUnits;
    }

    public String getIsPerishableon() {
        return isPerishableon;
    }

    public void setIsPerishableon(String isPerishableon) {
        this.isPerishableon = isPerishableon;
    }

    public String getIsEntyTaxApplicable() {
        return isEntyTaxApplicable;
    }

    public void setIsEntyTaxApplicable(String isEntyTaxApplicable) {
        this.isEntyTaxApplicable = isEntyTaxApplicable;
    }

    public String getIsUpdatingTargetId() {
        return isUpdatingTargetId;
    }

    public void setIsUpdatingTargetId(String isUpdatingTargetId) {
        this.isUpdatingTargetId = isUpdatingTargetId;
    }

    public String getAsOriginal() {
        return asOriginal;
    }

    public void setAsOriginal(String asOriginal) {
        this.asOriginal = asOriginal;
    }

    public StockItemLanguageName getStockItemLanguageName() {
        return stockItemLanguageName;
    }

    public void setStockItemLanguageName(StockItemLanguageName stockItemLanguageName) {
        this.stockItemLanguageName = stockItemLanguageName;
    }
}
