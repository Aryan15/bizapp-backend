package com.coreerp.tally.model.stockitem;

import com.coreerp.tally.model.common.TallyMessage;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StockItemTallyMessage implements TallyMessage {


    @JsonProperty("STOCKITEM")
    private StockItemItem stockItemItem;

    public StockItemItem getStockItemItem() {
        return stockItemItem;
    }

    public void setStockItemItem(StockItemItem stockItemItems) {
        this.stockItemItem = stockItemItems;
    }
}
