package com.coreerp.tally.model.discount;

import com.coreerp.tally.model.bank.BankLeadger;
import com.coreerp.tally.model.common.TallyMessage;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DiscountTallyMessage implements TallyMessage {

    @JsonProperty("LEDGER")
    private DiscountLeadger discountLeadger;

    public DiscountLeadger getDiscountLeadger() {
        return discountLeadger;
    }

    public void setDiscountLeadger(DiscountLeadger discountLeadger) {
        this.discountLeadger = discountLeadger;
    }
}
