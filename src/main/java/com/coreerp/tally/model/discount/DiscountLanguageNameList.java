package com.coreerp.tally.model.discount;

import com.coreerp.tally.model.bank.BankName;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class DiscountLanguageNameList {

    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type = "String";


    @JsonProperty("NAME.LIST")
    private DiscountName discountName;

    @JsonProperty("LANGUAGEID")
    private Integer languageId;


    public DiscountName getDiscountName() {
        return discountName;
    }

    public void setDiscountName(DiscountName discountName) {
        this.discountName = discountName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }



    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }
}



