package com.coreerp.tally.model.discount;

import com.coreerp.tally.model.bank.BankLanguageName;
import com.coreerp.tally.model.bank.BankMailingName;
import com.coreerp.tally.model.bank.BankOldAuditEntryIds;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class DiscountLeadger {

    @JacksonXmlProperty(isAttribute = true, localName = "NAME")
    private String name;

    @JacksonXmlProperty(isAttribute = true, localName = "RESERVEDNAME")
    private String reservedName = "";



    @JsonProperty("OLDAUDITENTRYIDS.LIST")
    private DiscountOldEntryList discountOldEntryList;

    @JsonProperty("CURRENCYNAME")
    private String currencyName;

    @JsonProperty("PARENT")
    private String parent;

    @JsonProperty("GSTAPPLICABLE")
    private String gstApplicable;

    @JsonProperty("TAXTYPE")
    private String taxType;


    @JsonProperty("GSTTYPEOFSUPPLY")
    private String gstTypeOfSupply;

    @JsonProperty("SERVICECATEGORY")
    private String serviceCategory;


    @JsonProperty("VATAPPLICABLE")
    private String vatApplicable;

    @JsonProperty("ISBILLWISEON")
    private String isBillWiseon;

    @JsonProperty("ISCOSTCENTRESON")
    private String isCostcentereson;

    @JsonProperty("ISINTERESTON")
    private String isIntereston;

    @JsonProperty("ISCOSTTRACKINGON")
    private String isCostTrackinGon;

    @JsonProperty("ISBENEFICIARYCODEON")
    private String isBeneficiaryCodeOn;

    @JsonProperty("ISUPDATINGTARGETID")
    private String isUpdatingTargetId;


    @JsonProperty("ASORIGINAL")
    private String asOriginal;

    @JsonProperty("ISCONDENSED")
    private String isCondensed;

    @JsonProperty("AFFECTSSTOCK")
    private String affectsStock;

    @JsonProperty("ISRATEINCLUSIVEVAT")
    private String isRateInclusiveVat;

    @JsonProperty("FORPAYROLL")
    private String forPayroll;

    @JsonProperty("ISABCENABLED")
    private String isAbcenAbled;


    @JsonProperty("ISCREDITDAYSCHKON")
    private String isCreditDaySchkon;

    @JsonProperty("INTERESTONBILLWISE")
    private String interStonBillWise;

    @JsonProperty("OVERRIDEINTEREST")
    private String overRidenInterest;

    @JsonProperty("OVERRIDEADVINTEREST")
    private String overRideAdvInterest;

    @JsonProperty("USEFORVAT")
    private String useForVat;

    @JsonProperty("IGNORETDSEXEMPT")
    private String ignoreTdsexempt;


    @JsonProperty("ISTCSAPPLICABLE")
    private String isTcsApplicable;

    @JsonProperty("ISTDSAPPLICABLE")
    private String isTdsApplicable;

    @JsonProperty("ISFBTAPPLICABLE")
    private String isFbtApplicable;

    @JsonProperty("ISGSTAPPLICABLE")
    private String isGstApplicable;

    @JsonProperty("ISEXCISEAPPLICABLE")
    private String isExciseApplicable;

    @JsonProperty("ISTDSEXPENSE")
    private String isTdsExpense;

    @JsonProperty("ISEDLIAPPLICABLE")
    private String isEdliApplicable;

    @JsonProperty("ISRELATEDPARTY")
    private String isRelatedParty;




    @JsonProperty("LANGUAGENAME.LIST")
    private DiscountLanguageNameList discountLanguageNameList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReservedName() {
        return reservedName;
    }

    public void setReservedName(String reservedName) {
        this.reservedName = reservedName;
    }

    public DiscountOldEntryList getDiscountOldEntryList() {
        return discountOldEntryList;
    }

    public void setDiscountOldEntryList(DiscountOldEntryList discountOldEntryList) {
        this.discountOldEntryList = discountOldEntryList;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getGstApplicable() {
        return gstApplicable;
    }

    public void setGstApplicable(String gstApplicable) {
        this.gstApplicable = gstApplicable;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getGstTypeOfSupply() {
        return gstTypeOfSupply;
    }

    public void setGstTypeOfSupply(String gstTypeOfSupply) {
        this.gstTypeOfSupply = gstTypeOfSupply;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    public String getVatApplicable() {
        return vatApplicable;
    }

    public void setVatApplicable(String vatApplicable) {
        this.vatApplicable = vatApplicable;
    }

    public String getIsBillWiseon() {
        return isBillWiseon;
    }

    public void setIsBillWiseon(String isBillWiseon) {
        this.isBillWiseon = isBillWiseon;
    }

    public String getIsCostcentereson() {
        return isCostcentereson;
    }

    public void setIsCostcentereson(String isCostcentereson) {
        this.isCostcentereson = isCostcentereson;
    }

    public String getIsIntereston() {
        return isIntereston;
    }

    public void setIsIntereston(String isIntereston) {
        this.isIntereston = isIntereston;
    }

    public String getIsCostTrackinGon() {
        return isCostTrackinGon;
    }

    public void setIsCostTrackinGon(String isCostTrackinGon) {
        this.isCostTrackinGon = isCostTrackinGon;
    }

    public String getIsBeneficiaryCodeOn() {
        return isBeneficiaryCodeOn;
    }

    public void setIsBeneficiaryCodeOn(String isBeneficiaryCodeOn) {
        this.isBeneficiaryCodeOn = isBeneficiaryCodeOn;
    }

    public String getIsUpdatingTargetId() {
        return isUpdatingTargetId;
    }

    public void setIsUpdatingTargetId(String isUpdatingTargetId) {
        this.isUpdatingTargetId = isUpdatingTargetId;
    }

    public String getAsOriginal() {
        return asOriginal;
    }

    public void setAsOriginal(String asOriginal) {
        this.asOriginal = asOriginal;
    }

    public String getIsCondensed() {
        return isCondensed;
    }

    public void setIsCondensed(String isCondensed) {
        this.isCondensed = isCondensed;
    }

    public String getAffectsStock() {
        return affectsStock;
    }

    public void setAffectsStock(String affectsStock) {
        this.affectsStock = affectsStock;
    }

    public String getIsRateInclusiveVat() {
        return isRateInclusiveVat;
    }

    public void setIsRateInclusiveVat(String isRateInclusiveVat) {
        this.isRateInclusiveVat = isRateInclusiveVat;
    }

    public String getForPayroll() {
        return forPayroll;
    }

    public void setForPayroll(String forPayroll) {
        this.forPayroll = forPayroll;
    }

    public String getIsAbcenAbled() {
        return isAbcenAbled;
    }

    public void setIsAbcenAbled(String isAbcenAbled) {
        this.isAbcenAbled = isAbcenAbled;
    }

    public String getIsGstApplicable() {
        return isGstApplicable;
    }

    public void setIsGstApplicable(String isGstApplicable) {
        this.isGstApplicable = isGstApplicable;
    }

    public String getIsCreditDaySchkon() {
        return isCreditDaySchkon;
    }

    public void setIsCreditDaySchkon(String isCreditDaySchkon) {
        this.isCreditDaySchkon = isCreditDaySchkon;
    }

    public String getInterStonBillWise() {
        return interStonBillWise;
    }

    public void setInterStonBillWise(String interStonBillWise) {
        this.interStonBillWise = interStonBillWise;
    }

    public String getOverRidenInterest() {
        return overRidenInterest;
    }

    public void setOverRidenInterest(String overRidenInterest) {
        this.overRidenInterest = overRidenInterest;
    }

    public String getOverRideAdvInterest() {
        return overRideAdvInterest;
    }

    public void setOverRideAdvInterest(String overRideAdvInterest) {
        this.overRideAdvInterest = overRideAdvInterest;
    }

    public String getUseForVat() {
        return useForVat;
    }

    public void setUseForVat(String useForVat) {
        this.useForVat = useForVat;
    }

    public String getIgnoreTdsexempt() {
        return ignoreTdsexempt;
    }

    public void setIgnoreTdsexempt(String ignoreTdsexempt) {
        this.ignoreTdsexempt = ignoreTdsexempt;
    }

    public String getIsTcsApplicable() {
        return isTcsApplicable;
    }

    public void setIsTcsApplicable(String isTcsApplicable) {
        this.isTcsApplicable = isTcsApplicable;
    }

    public String getIsTdsApplicable() {
        return isTdsApplicable;
    }

    public void setIsTdsApplicable(String isTdsApplicable) {
        this.isTdsApplicable = isTdsApplicable;
    }

    public String getIsFbtApplicable() {
        return isFbtApplicable;
    }

    public void setIsFbtApplicable(String isFbtApplicable) {
        this.isFbtApplicable = isFbtApplicable;
    }

    public String getIsExciseApplicable() {
        return isExciseApplicable;
    }

    public void setIsExciseApplicable(String isExciseApplicable) {
        this.isExciseApplicable = isExciseApplicable;
    }

    public String getIsTdsExpense() {
        return isTdsExpense;
    }

    public void setIsTdsExpense(String isTdsExpense) {
        this.isTdsExpense = isTdsExpense;
    }

    public String getIsEdliApplicable() {
        return isEdliApplicable;
    }

    public void setIsEdliApplicable(String isEdliApplicable) {
        this.isEdliApplicable = isEdliApplicable;
    }

    public String getIsRelatedParty() {
        return isRelatedParty;
    }

    public void setIsRelatedParty(String isRelatedParty) {
        this.isRelatedParty = isRelatedParty;
    }

    public DiscountLanguageNameList getDiscountLanguageNameList() {
        return discountLanguageNameList;
    }

    public void setDiscountLanguageNameList(DiscountLanguageNameList discountLanguageNameList) {
        this.discountLanguageNameList = discountLanguageNameList;
    }




    /* @JsonProperty("STARTINGFROM")
    private String startingFrom;



    @JsonProperty("COUNTRYNAME")
    private String countryName;


    @JsonProperty("IFSCODE")
    private String ifsCode;



    @JsonProperty("BANKDETAILS")
    private String bankDetails;

    @JsonProperty("BANKBRANCHNAME")
    private String bankBranchName;

    @JsonProperty("COUNTRYOFRESIDENCE")
    private String countryOfResidence;



    @JsonProperty("BANKINGCONFIGBANK")
    private String bankingConfigBank;

    @JsonProperty("BANKINGCONFIGBANKID")
    private String bankingConfigBankId;

    @JsonProperty("BANKACCHOLDERNAME")
    private String bankAccHolderName;

    @JsonProperty("LEDSTATENAME")
    private String bankStateName;






    @JsonProperty("ISABCENABLED")
    private String isAbcenabled;
*/






}
