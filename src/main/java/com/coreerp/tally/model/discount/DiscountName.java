package com.coreerp.tally.model.discount;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DiscountName {

    @JsonProperty("NAME")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
