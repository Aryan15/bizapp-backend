package com.coreerp.tally.model.master;

import com.coreerp.tally.model.common.TallyMessage;
import com.fasterxml.jackson.annotation.JsonProperty;


public class CustomerTallyMessage implements TallyMessage {

    @JsonProperty("LEDGER")
    private CustomerLedger customerLedger;


    public CustomerLedger getCustomerLedger() {
        return customerLedger;
    }

    public void setCustomerLedger(CustomerLedger customerLedger) {
        this.customerLedger = customerLedger;
    }
}
