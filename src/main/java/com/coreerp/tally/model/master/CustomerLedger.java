package com.coreerp.tally.model.master;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CustomerLedger {

    @JacksonXmlProperty(isAttribute = true, localName = "NAME")
    private String name = "Welkin Customer Name";

    @JacksonXmlProperty(isAttribute = true, localName = "RESERVEDNAME")
    private String reservedName = "";

  @JsonProperty("ADDRESS.LIST")
    private CustomerAddress customerAddress;

    @JsonProperty("MAILINGNAME.LIST")
    private  CustomerMailingName customerMailingName;

//    @JsonProperty("MAILINGNAME.LIST ")
//    private  CustomerMailingName suplierMailingName;


    @JsonProperty("OLDAUDITENTRYIDS.LIST")
    private  CustomerOldAuditEntryIds customerOldAuditEntryIds;

    @JsonProperty("CURRENCYNAME")
    private String currencyName ;

    @JsonProperty("COUNTRYNAME")
    private String countryName ;

    @JsonProperty("GSTREGISTRATIONTYPE")
    private String gstRegistrationType;


    @JsonProperty("VATDEALERTYPE")
    private String vatDealerType;

    @JsonProperty("PARENT")
    private String parent ;

    @JsonProperty("TAXTYPE")
    private String taxType;

    @JsonProperty("COUNTRYOFRESIDENCE")
    private String countryOfResidence ;

 @JsonProperty("PARTYGSTIN")
   private String partyGstin ;

    @JsonProperty("SERVICECATEGORY")
    private String serviceCategory ;

    @JsonProperty("LEDSTATENAME")
    private String ledStateName;

    @JsonProperty("ISBILLWISEON")
    private String isBillWiseon;

    @JsonProperty("ISUPDATINGTARGETID")
    private String isUpdatingTargetId ;

    @JsonProperty("ASORIGINAL")
    private String asOriginal ;

    @JsonProperty("ISCHEQUEPRINTINGENABLED")
    private String isChequePrintingEnabled ;

    @JsonProperty("LANGUAGENAME.LIST")
    private CustomerLanguageName customerLanguageName;

    @JsonProperty("PINCODE")
    private String pinCode ;

    @JsonProperty("INCOMETAXNUMBER")
    private String panNum ;

    @JsonProperty("BILLCREDITPERIOD")
    private String billCreaditPeriod ;



    @JsonProperty("PAYMENTDETAILS.LIST")
    private CustomerBankDetails customerBankDetails;


    public String getBillCreaditPeriod() {
        return billCreaditPeriod;
    }

    public void setBillCreaditPeriod(String billCreaditPeriod) {
        this.billCreaditPeriod = billCreaditPeriod;
    }

    public CustomerBankDetails getCustomerBankDetails() {
        return customerBankDetails;
    }

    public void setCustomerBankDetails(CustomerBankDetails customerBankDetails) {
        this.customerBankDetails = customerBankDetails;
    }


//


//
//    public CustomerMailingName getSuplierMailingName() {
//        return suplierMailingName;
//    }
//
//    public void setSuplierMailingName(CustomerMailingName suplierMailingName) {
//        this.suplierMailingName = suplierMailingName;
//    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getGstRegistrationType() {
        return gstRegistrationType;
    }

    public void setGstRegistrationType(String gstRegistrationType) {
        this.gstRegistrationType = gstRegistrationType;
    }

    public String getVatDealerType() {
        return vatDealerType;
    }

    public void setVatDealerType(String vatDealerType) {
        this.vatDealerType = vatDealerType;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getCountryOfResidence() {
        return countryOfResidence;
    }

    public void setCountryOfResidence(String countryOfResidence) {
        this.countryOfResidence = countryOfResidence;
    }

//    public String getPartyGstin() {
//        return partyGstin;
//    }
//
//    public void setPartyGstin(String partyGstin) {
//        this.partyGstin = partyGstin;
//    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    public String getLedStateName() {
        return ledStateName;
    }

    public void setLedStateName(String ledStateName) {
        this.ledStateName = ledStateName;
    }

    public String getIsBillWiseon() {
        return isBillWiseon;
    }

    public void setIsBillWiseon(String isBillWiseon) {
        this.isBillWiseon = isBillWiseon;
    }

    public String getIsUpdatingTargetId() {
        return isUpdatingTargetId;
    }

    public void setIsUpdatingTargetId(String isUpdatingTargetId) {
        this.isUpdatingTargetId = isUpdatingTargetId;
    }

    public String getAsOriginal() {
        return asOriginal;
    }

    public void setAsOriginal(String asOriginal) {
        this.asOriginal = asOriginal;
    }

    public String getIsChequePrintingEnabled() {
        return isChequePrintingEnabled;
    }

    public void setIsChequePrintingEnabled(String isChequePrintingEnabled) {
        this.isChequePrintingEnabled = isChequePrintingEnabled;
    }

    public CustomerLanguageName getCustomerLanguageName() {
        return customerLanguageName;
    }

    public void setCustomerLanguageName(CustomerLanguageName customerLanguageName) {
        this.customerLanguageName = customerLanguageName;
    }

    public CustomerOldAuditEntryIds getCustomerOldAuditEntryIds() {
        return customerOldAuditEntryIds;
    }

    public void setCustomerOldAuditEntryIds(CustomerOldAuditEntryIds customerOldAuditEntryIds) {
        this.customerOldAuditEntryIds = customerOldAuditEntryIds;
    }

    public CustomerMailingName getCustomerMailingName() {
        return customerMailingName;
    }

    public void setCustomerMailingName(CustomerMailingName customerMailingName) {
        this.customerMailingName = customerMailingName;
    }

//    public CustomerAddress getCustomerAddress() {
//        return customerAddress;
//    }
//
//    public void setCustomerAddress(CustomerAddress customerAddress) {
//        this.customerAddress = customerAddress;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReservedName() {
        return reservedName;
    }

    public void setReservedName(String reservedName) {
        this.reservedName = reservedName;
    }

    public CustomerAddress getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(CustomerAddress customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getPanNum() {
        return panNum;
    }

    public void setPanNum(String panNum) {
        this.panNum = panNum;
    }

    public String getPartyGstin() {
        return partyGstin;
    }

    public void setPartyGstin(String partyGstin) {
        this.partyGstin = partyGstin;
    }
}
