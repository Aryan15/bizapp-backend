package com.coreerp.tally.model.master;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;


@JacksonXmlRootElement(localName = "ENVELOPE")
public class CustomerEnvelope {

    @JsonProperty("HEADER")
    private CustomerHeader customerHeader;

    @JsonProperty("BODY")
    private CustomerBody customerBody;

    public CustomerBody getCustomerBody() {
        return customerBody;
    }

    public void setCustomerBody(CustomerBody customerBody) {
        this.customerBody = customerBody;
    }

    public CustomerHeader getCustomerHeader() {
        return customerHeader;
    }

    public void setCustomerHeader(CustomerHeader customerHeader) {
        this.customerHeader = customerHeader;
    }
}
