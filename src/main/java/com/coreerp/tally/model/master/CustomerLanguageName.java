package com.coreerp.tally.model.master;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerLanguageName {


    @JsonProperty("NAME.LIST")
    private CustomerName customerName;


//    @JsonProperty("NAME.LIST ")
//    private CustomerName suplierName;

    @JsonProperty("LANGUAGEID")
    private Integer languageId ;

//    public CustomerName getSuplierName() {
//        return suplierName;
//    }
//
//    public void setSuplierName(CustomerName suplierName) {
//        this.suplierName = suplierName;
//    }

    public CustomerName getCustomerName() {
        return customerName;
    }

    public void setCustomerName(CustomerName customerName) {
        this.customerName = customerName;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }
}
