package com.coreerp.tally.model.master;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;


public class CustomerRequestData {

    @JacksonXmlProperty(namespace = "TallyUDF", localName = "TALLYMESSAGE")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<CustomerTallyMessage> customerTallyMessages;

    public List<CustomerTallyMessage> getCustomerTallyMessages() {
        return customerTallyMessages;
    }

    public void setCustomerTallyMessages(List<CustomerTallyMessage> customerTallyMessages) {
        this.customerTallyMessages = customerTallyMessages;
    }
}
