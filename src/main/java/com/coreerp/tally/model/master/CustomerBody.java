package com.coreerp.tally.model.master;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerBody {

    @JsonProperty("IMPORTDATA")
    private CustomerImportData customerImportData;

    public CustomerImportData getCustomerImportData() {
        return customerImportData;
    }

    public void setCustomerImportData(CustomerImportData customerImportData) {
        this.customerImportData = customerImportData;
    }
}
