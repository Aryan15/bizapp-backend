package com.coreerp.tally.model.master;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerImportData {

    @JsonProperty("REQUESTDESC")
    private CustomerRequestDesc customerRequestDesc;

    @JsonProperty("REQUESTDATA")
    private CustomerRequestData customerRequestData;

    public CustomerRequestData getCustomerRequestData() {
        return customerRequestData;
    }

    public void setCustomerRequestData(CustomerRequestData customerRequestData) {
        this.customerRequestData = customerRequestData;
    }

    public CustomerRequestDesc getCustomerRequestDesc() {
        return customerRequestDesc;
    }

    public void setCustomerRequestDesc(CustomerRequestDesc customerRequestDesc) {
        this.customerRequestDesc = customerRequestDesc;
    }
}
