package com.coreerp.tally.model.master;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CustomerMailingName {

    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type ="String";


    @JsonProperty("MAILINGNAME")
    private String mailingName ;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMailingName() {
        return mailingName;
    }

    public void setMailingName(String mailingName) {
        this.mailingName = mailingName;
    }
}
