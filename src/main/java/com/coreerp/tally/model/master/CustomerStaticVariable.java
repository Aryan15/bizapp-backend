package com.coreerp.tally.model.master;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerStaticVariable {
    @JsonProperty("SVCURRENTCOMPANY")
    private String svCurrentCompany ;

    public String getSvCurrentCompany() {
        return svCurrentCompany;
    }

    public void setSvCurrentCompany(String svCurrentCompany) {
        this.svCurrentCompany = svCurrentCompany;
    }
}
