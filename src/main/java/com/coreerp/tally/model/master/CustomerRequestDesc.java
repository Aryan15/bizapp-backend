package com.coreerp.tally.model.master;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerRequestDesc {

    @JsonProperty("REPORTNAME")
    private String tallyRequest;


    @JsonProperty("STATICVARIABLES")
    private CustomerStaticVariable customerStaticVariable;

    public CustomerStaticVariable getCustomerStaticVariable() {
        return customerStaticVariable;
    }

    public void setCustomerStaticVariable(CustomerStaticVariable customerStaticVariable) {
        this.customerStaticVariable = customerStaticVariable;
    }

    public String getTallyRequest() {
        return tallyRequest;
    }

    public void setTallyRequest(String tallyRequest) {
        this.tallyRequest = tallyRequest;
    }


}
