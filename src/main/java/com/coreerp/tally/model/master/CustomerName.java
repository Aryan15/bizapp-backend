package com.coreerp.tally.model.master;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CustomerName {

    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type ="String";

    @JsonProperty("NAME")
    private String name ;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
