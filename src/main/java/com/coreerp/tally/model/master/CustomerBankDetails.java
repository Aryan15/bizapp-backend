package com.coreerp.tally.model.master;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerBankDetails {


    @JsonProperty("IFSCODE")
    private String ifsCode ;

    @JsonProperty("BANKNAME")
    private String bankName ;

    @JsonProperty("ACCOUNTNUMBER")
    private String accountNumber ;

    @JsonProperty("PAYMENTFAVOURING")
    private String paymentFavouring ;

    @JsonProperty("TRANSACTIONNAME")
    private String transactionName ;

    @JsonProperty("SETASDEFAULT")
    private String setAsDefault ;

    @JsonProperty("DEFAULTTRANSACTIONTYPE")
    private String defaultTransactionType ;


    public String getIfsCode() {
        return ifsCode;
    }

    public void setIfsCode(String ifsCode) {
        this.ifsCode = ifsCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPaymentFavouring() {
        return paymentFavouring;
    }

    public void setPaymentFavouring(String paymentFavouring) {
        this.paymentFavouring = paymentFavouring;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public String getSetAsDefault() {
        return setAsDefault;
    }

    public void setSetAsDefault(String setAsDefault) {
        this.setAsDefault = setAsDefault;
    }

    public String getDefaultTransactionType() {
        return defaultTransactionType;
    }

    public void setDefaultTransactionType(String defaultTransactionType) {
        this.defaultTransactionType = defaultTransactionType;
    }
}
