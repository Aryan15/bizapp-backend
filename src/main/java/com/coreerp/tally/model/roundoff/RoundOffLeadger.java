package com.coreerp.tally.model.roundoff;

import com.coreerp.tally.model.discount.DiscountLanguageNameList;
import com.coreerp.tally.model.discount.DiscountOldEntryList;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class RoundOffLeadger {

    @JacksonXmlProperty(isAttribute = true, localName = "NAME")
    private String name;

    @JacksonXmlProperty(isAttribute = true, localName = "RESERVEDNAME")
    private String reservedName = "";

    @JsonProperty("OLDAUDITENTRYIDS.LIST")
    private RoundOffOldAuditEntryIds roundOffOldAuditEntryIds;



    @JsonProperty("PARENT")
    private String parent;

    @JsonProperty("GSTAPPLICABLE")
    private String gstApplicable;

    @JsonProperty("TDSAPPLICABLE")
    private String tdsApplicable;

    @JsonProperty("TAXTYPE")
    private String taxType;

    @JsonProperty("ROUNDINGMETHOD")
    private String roundingMethod;

    @JsonProperty("SERVICECATEGORY")
    private String serviceCategory;

    @JsonProperty("VATAPPLICABLE")
    private String vatApplicable;

    @JsonProperty("ASORIGINAL")
    private String asOriginal;

    @JsonProperty("LANGUAGENAME.LIST")
    private RoundOffLanguageNameList roundOffLanguageNameList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReservedName() {
        return reservedName;
    }

    public void setReservedName(String reservedName) {
        this.reservedName = reservedName;
    }

    public RoundOffOldAuditEntryIds getRoundOffOldAuditEntryIds() {
        return roundOffOldAuditEntryIds;
    }

    public void setRoundOffOldAuditEntryIds(RoundOffOldAuditEntryIds roundOffOldAuditEntryIds) {
        this.roundOffOldAuditEntryIds = roundOffOldAuditEntryIds;
    }



    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getGstApplicable() {
        return gstApplicable;
    }

    public void setGstApplicable(String gstApplicable) {
        this.gstApplicable = gstApplicable;
    }

    public String getTdsApplicable() {
        return tdsApplicable;
    }

    public void setTdsApplicable(String tdsApplicable) {
        this.tdsApplicable = tdsApplicable;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getRoundingMethod() {
        return roundingMethod;
    }

    public void setRoundingMethod(String roundingMethod) {
        this.roundingMethod = roundingMethod;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    public String getVatApplicable() {
        return vatApplicable;
    }

    public void setVatApplicable(String vatApplicable) {
        this.vatApplicable = vatApplicable;
    }

    public String getAsOriginal() {
        return asOriginal;
    }

    public void setAsOriginal(String asOriginal) {
        this.asOriginal = asOriginal;
    }

    public RoundOffLanguageNameList getRoundOffLanguageNameList() {
        return roundOffLanguageNameList;
    }

    public void setRoundOffLanguageNameList(RoundOffLanguageNameList roundOffLanguageNameList) {
        this.roundOffLanguageNameList = roundOffLanguageNameList;
    }
}
