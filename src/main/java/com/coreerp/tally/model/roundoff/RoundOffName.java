package com.coreerp.tally.model.roundoff;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class RoundOffName {


   /* @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type = "String";
*/
    @JsonProperty("NAME")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
