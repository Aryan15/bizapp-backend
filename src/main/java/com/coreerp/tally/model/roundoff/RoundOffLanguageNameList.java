package com.coreerp.tally.model.roundoff;

import com.coreerp.tally.model.discount.DiscountName;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class RoundOffLanguageNameList {

    @JacksonXmlProperty(isAttribute = true, localName = "TYPE")
    private String type = "String";


    @JsonProperty("NAME.LIST")
    private RoundOffName roundOffName;

    @JsonProperty("LANGUAGEID")
    private Integer languageId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public RoundOffName getRoundOffName() {
        return roundOffName;
    }

    public void setRoundOffName(RoundOffName roundOffName) {
        this.roundOffName = roundOffName;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

}
