package com.coreerp.tally.model.roundoff;

import com.coreerp.tally.model.common.TallyMessage;
import com.coreerp.tally.model.discount.DiscountLeadger;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RoundOffTallyMessage implements TallyMessage {


    @JsonProperty("LEDGER")
    private RoundOffLeadger roundOffLeadger;

    public RoundOffLeadger getRoundOffLeadger() {
        return roundOffLeadger;
    }

    public void setRoundOffLeadger(RoundOffLeadger roundOffLeadger) {
        this.roundOffLeadger = roundOffLeadger;
    }
}
