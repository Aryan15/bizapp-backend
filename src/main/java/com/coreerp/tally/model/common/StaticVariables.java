package com.coreerp.tally.model.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StaticVariables {

    @JsonProperty("SVCURRENTCOMPANY")
    private String svCurrentCompany ;

    public String getSvCurrentCompany() {
        return svCurrentCompany;
    }

    public void setSvCurrentCompany(String svCurrentCompany) {
        this.svCurrentCompany = svCurrentCompany;
    }
}
