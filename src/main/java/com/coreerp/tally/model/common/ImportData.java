package com.coreerp.tally.model.common;

import com.coreerp.tally.model.gstsource.GstRequestDesc;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ImportData {


    @JsonProperty("REQUESTDESC")
    private RequestDesc requestDesc;

    @JsonProperty("REQUESTDATA")
    private RequestData requestData;

    public RequestData getRequestData() {
        return requestData;
    }

    public void setRequestData(RequestData requestData) {
        this.requestData = requestData;
    }

    public RequestDesc getRequestDesc() {
        return requestDesc;
    }

    public void setRequestDesc(RequestDesc requestDesc) {
        this.requestDesc = requestDesc;
    }
}
