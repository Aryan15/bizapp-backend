package com.coreerp.tally.model.common;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class RequestData {

    @JacksonXmlProperty(namespace = "TallyUDF", localName = "TALLYMESSAGE")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<TallyMessage> tallyMessages;

    public List<TallyMessage> getTallyMessages() {
        return tallyMessages;
    }

    public void setTallyMessages(List<TallyMessage> tallyMessages) {
        this.tallyMessages = tallyMessages;
    }
}
