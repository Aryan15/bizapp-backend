package com.coreerp.tally.model.common;

import com.coreerp.tally.model.gstsource.GstStaticVariable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestDesc {

    @JsonProperty("REPORTNAME")
    private String reportName ;

    @JsonProperty("STATICVARIABLES")
    private StaticVariables staticVariables;

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public StaticVariables getStaticVariables() {
        return staticVariables;
    }

    public void setStaticVariables(StaticVariables staticVariables) {
        this.staticVariables = staticVariables;
    }
}
