package com.coreerp.tally.model.gstsource;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GstRequestDesc {

    @JsonProperty("REPORTNAME")
    private String reportName ;

    @JsonProperty("STATICVARIABLES")
    private GstStaticVariable gstStaticVariable;

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public GstStaticVariable getGstStaticVariable() {
        return gstStaticVariable;
    }

    public void setGstStaticVariable(GstStaticVariable gstStaticVariable) {
        this.gstStaticVariable = gstStaticVariable;
    }
}
