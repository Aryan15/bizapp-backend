package com.coreerp.tally.model.gstsource;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class GstLedger {

    @JacksonXmlProperty(isAttribute = true, localName = "NAME")
    private String name ;

    @JacksonXmlProperty(isAttribute = true, localName = "RESERVEDNAME")
   private String reservedName="";

    @JsonProperty("MAILINGNAME.LIST")
    private GstMailingName gstMailingName;

    @JsonProperty("OLDAUDITENTRYIDS.LIST")
    private GstOldAuditEntryIds gstOldAuditEntryIds;

    @JsonProperty("CURRENCYNAME")
    private String currencyName;

    @JsonProperty("PARENT")
    private String parent;

    @JsonProperty("TAXTYPE")
    private String taxType ;

    @JsonProperty("GSTDUTYHEAD")
    private String gstDutyHead;

    @JsonProperty("GSTTYPEOFSUPPLY")
    private String gstTypeOfSupply;

    @JsonProperty("SERVICECATEGORY")
    private String serviceCategory;

    @JsonProperty("ISBILLWISEON")
    private String isBillWiseon ;

    @JsonProperty("ISUPDATINGTARGETID")
    private String isUpdatingTargetId;

    @JsonProperty("ASORIGINAL")
    private String asOriginal;

    @JsonProperty("AFFECTSSTOCK")
    private String affectsStock ;

    @JsonProperty("ISGSTAPPLICABLE")
    private String isGstApplicable;

    @JsonProperty("GSTAPPLICABLE")
    private String gstApplicable ;

    @JsonProperty("VATAPPLICABLE")
    private String vatApplicable;

    @JsonProperty("LANGUAGENAME.LIST")
    private GstLanguageName gstLanguageName;

    @JsonProperty("ISCOSTCENTRESON")
    private String isCostCentreSon;

    public GstLanguageName getGstLanguageName() {
        return gstLanguageName;
    }

    public void setGstLanguageName(GstLanguageName gstLanguageName) {
        this.gstLanguageName = gstLanguageName;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getGstDutyHead() {
        return gstDutyHead;
    }

    public void setGstDutyHead(String gstDutyHead) {
        this.gstDutyHead = gstDutyHead;
    }

    public String getGstTypeOfSupply() {
        return gstTypeOfSupply;
    }

    public void setGstTypeOfSupply(String gstTypeOfSupply) {
        this.gstTypeOfSupply = gstTypeOfSupply;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    public String getIsBillWiseon() {
        return isBillWiseon;
    }

    public void setIsBillWiseon(String isBillWiseon) {
        this.isBillWiseon = isBillWiseon;
    }

    public String getIsUpdatingTargetId() {
        return isUpdatingTargetId;
    }

    public void setIsUpdatingTargetId(String isUpdatingTargetId) {
        this.isUpdatingTargetId = isUpdatingTargetId;
    }

    public String getAsOriginal() {
        return asOriginal;
    }

    public void setAsOriginal(String asOriginal) {
        this.asOriginal = asOriginal;
    }

    public String getAffectsStock() {
        return affectsStock;
    }

    public void setAffectsStock(String affectsStock) {
        this.affectsStock = affectsStock;
    }

    public String getIsGstApplicable() {
        return isGstApplicable;
    }

    public void setIsGstApplicable(String isGstApplicable) {
        this.isGstApplicable = isGstApplicable;
    }

    public GstOldAuditEntryIds getGstOldAuditEntryIds() {
        return gstOldAuditEntryIds;
    }

    public void setGstOldAuditEntryIds(GstOldAuditEntryIds gstOldAuditEntryIds) {
        this.gstOldAuditEntryIds = gstOldAuditEntryIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReservedName() {
        return reservedName;
    }

    public void setReservedName(String reservedName) {
        this.reservedName = reservedName;
    }

    public GstMailingName getGstMailingName() {
        return gstMailingName;
    }

    public void setGstMailingName(GstMailingName gstMailingName) {
        this.gstMailingName = gstMailingName;
    }

    public String getGstApplicable() {
        return gstApplicable;
    }

    public void setGstApplicable(String gstApplicable) {
        this.gstApplicable = gstApplicable;
    }

    public String getVatApplicable() {
        return vatApplicable;
    }

    public void setVatApplicable(String vatApplicable) {
        this.vatApplicable = vatApplicable;
    }

    public String getIsCostCentreSon() {
        return isCostCentreSon;
    }

    public void setIsCostCentreSon(String isCostCentreSon) {
        this.isCostCentreSon = isCostCentreSon;
    }
}
