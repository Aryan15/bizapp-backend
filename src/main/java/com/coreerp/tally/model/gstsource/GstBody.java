package com.coreerp.tally.model.gstsource;

import com.fasterxml.jackson.annotation.JsonProperty;


public class GstBody {

    @JsonProperty("IMPORTDATA")
    private GstImportData gstImportData;

    public GstImportData getGstImportData() {
        return gstImportData;
    }

    public void setGstImportData(GstImportData gstImportData) {
        this.gstImportData = gstImportData;
    }
}
