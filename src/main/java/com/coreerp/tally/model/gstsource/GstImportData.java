package com.coreerp.tally.model.gstsource;

import com.fasterxml.jackson.annotation.JsonProperty;


public class GstImportData {

    @JsonProperty("REQUESTDESC")
    private GstRequestDesc gstRequestDesc;

    @JsonProperty("REQUESTDATA")
    private GstRequestData gstRequestData;

    public GstRequestData getGstRequestData() {
        return gstRequestData;
    }

    public void setGstRequestData(GstRequestData gstRequestData) {
        this.gstRequestData = gstRequestData;
    }

    public GstRequestDesc getGstRequestDesc() {
        return gstRequestDesc;
    }

    public void setGstRequestDesc(GstRequestDesc gstRequestDesc) {
        this.gstRequestDesc = gstRequestDesc;
    }
}
