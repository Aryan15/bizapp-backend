package com.coreerp.tally.model.gstsource;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;


@JacksonXmlRootElement(localName = "ENVELOPE")
public class GstEnvelope {

    @JsonProperty("HEADER")
    private GstHeader  gstHeader;

    @JsonProperty("BODY")
    private GstBody gstBody;

    public GstHeader getGstHeader() {
        return gstHeader;
    }

    public void setGstHeader(GstHeader gstHeader) {
        this.gstHeader = gstHeader;
    }

    public GstBody getGstBody() {
        return gstBody;
    }

    public void setGstBody(GstBody gstBody) {
        this.gstBody = gstBody;
    }
}
