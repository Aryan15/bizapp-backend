package com.coreerp.tally.model.gstsource;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GstName {

    @JsonProperty("NAME")
    private String name ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
