package com.coreerp.tally.model.gstsource;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class GstRequestData {

    @JacksonXmlProperty(namespace = "TallyUDF", localName = "TALLYMESSAGE")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<GstTallyMessage> gstTallyMessage;

    public List<GstTallyMessage> getGstTallyMessage() {
        return gstTallyMessage;
    }

    public void setGstTallyMessage(List<GstTallyMessage> gstTallyMessage) {
        this.gstTallyMessage = gstTallyMessage;
    }
}
