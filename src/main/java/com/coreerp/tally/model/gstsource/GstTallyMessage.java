package com.coreerp.tally.model.gstsource;

import com.coreerp.tally.model.common.TallyMessage;
import com.coreerp.tally.service.MasterService;
import com.fasterxml.jackson.annotation.JsonProperty;


public class GstTallyMessage implements TallyMessage {

    @JsonProperty("LEDGER")
    private GstLedger gstLedger;

    public GstLedger getGstLedger() {
        return gstLedger;
    }

    public void setGstLedger(GstLedger gstLedger) {
        this.gstLedger = gstLedger;
    }
}
