package com.coreerp.tally.model.gstsource;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GstStaticVariable {

    @JsonProperty("SVCURRENTCOMPANY")
    private String svCurrentCompany ;

    public String getSvCurrentCompany() {
        return svCurrentCompany;
    }

    public void setSvCurrentCompany(String svCurrentCompany) {
        this.svCurrentCompany = svCurrentCompany;
    }
}
