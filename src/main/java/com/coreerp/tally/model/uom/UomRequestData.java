package com.coreerp.tally.model.uom;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;


public class UomRequestData {

    @JacksonXmlProperty(namespace = "TallyUDF", localName = "TALLYMESSAGE")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<UomTallyMessage> uomTallyMessage;

    public List<UomTallyMessage> getUomTallyMessage() {
        return uomTallyMessage;
    }

    public void setUomTallyMessage(List<UomTallyMessage> uomTallyMessage) {
        this.uomTallyMessage = uomTallyMessage;
    }
}
