package com.coreerp.tally.model.uom;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UomBody {
    @JsonProperty("IMPORTDATA")
    private UomImportData uomImportData;

    public UomImportData getUomImportData() {
        return uomImportData;
    }

    public void setUomImportData(UomImportData uomImportData) {
        this.uomImportData = uomImportData;
    }
}
