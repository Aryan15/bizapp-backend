package com.coreerp.tally.model.uom;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UomImportData {


    @JsonProperty("REQUESTDESC")
    private UomRequestDesc uomRequestDesc;

    @JsonProperty("REQUESTDATA")
    private UomRequestData uomRequestData;


    public UomRequestData getUomRequestData() {
        return uomRequestData;
    }

    public void setUomRequestData(UomRequestData uomRequestData) {
        this.uomRequestData = uomRequestData;
    }

    public UomRequestDesc getUomRequestDesc() {
        return uomRequestDesc;
    }

    public void setUomRequestDesc(UomRequestDesc uomRequestDesc) {
        this.uomRequestDesc = uomRequestDesc;
    }
}
