package com.coreerp.tally.model.uom;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class UomUnit {

    @JacksonXmlProperty(isAttribute = true, localName = "NAME")
    private String name ;

    @JacksonXmlProperty(isAttribute = true, localName = "RESERVEDNAME")
    private String reservedName ="";


    @JsonProperty("NAME ")
    private String uomName;

    @JsonProperty("ORIGINALNAME")
    private String originalName;

    @JsonProperty("GSTREPUOM")
    private String gstrepUom;

    @JsonProperty("ISUPDATINGTARGETID")
    private String isUpdatingTargetId;

    @JsonProperty("ASORIGINAL")
    private String asOriginal;

    @JsonProperty("ISGSTEXCLUDED")
    private String isGstExcluded;

    @JsonProperty("ISSIMPLEUNIT")
    private String isSimpleUnit;

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getGstrepUom() {
        return gstrepUom;
    }

    public void setGstrepUom(String gstrepUom) {
        this.gstrepUom = gstrepUom;
    }

    public String getIsUpdatingTargetId() {
        return isUpdatingTargetId;
    }

    public void setIsUpdatingTargetId(String isUpdatingTargetId) {
        this.isUpdatingTargetId = isUpdatingTargetId;
    }

    public String getAsOriginal() {
        return asOriginal;
    }

    public void setAsOriginal(String asOriginal) {
        this.asOriginal = asOriginal;
    }

    public String getIsGstExcluded() {
        return isGstExcluded;
    }

    public void setIsGstExcluded(String isGstExcluded) {
        this.isGstExcluded = isGstExcluded;
    }

    public String getIsSimpleUnit() {
        return isSimpleUnit;
    }

    public void setIsSimpleUnit(String isSimpleUnit) {
        this.isSimpleUnit = isSimpleUnit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReservedName() {
        return reservedName;
    }

    public void setReservedName(String reservedName) {
        this.reservedName = reservedName;
    }
}
