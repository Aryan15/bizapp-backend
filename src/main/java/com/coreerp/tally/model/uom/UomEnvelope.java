package com.coreerp.tally.model.uom;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;



@JacksonXmlRootElement(localName = "ENVELOPE")
public class UomEnvelope {
    @JsonProperty("HEADER")
    private UomHeader uomHeader;

    @JsonProperty("BODY")
    private UomBody uomBody;

    public UomBody getUomBody() {
        return uomBody;
    }

    public void setUomBody(UomBody uomBody) {
        this.uomBody = uomBody;
    }

    public UomHeader getUomHeader() {
        return uomHeader;
    }

    public void setUomHeader(UomHeader uomHeader) {
        this.uomHeader = uomHeader;
    }
}
