package com.coreerp.tally.model.uom;

import com.coreerp.tally.model.common.TallyMessage;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UomTallyMessage implements TallyMessage {

    @JsonProperty("UNIT")
    private UomUnit uomUnit;

    public UomUnit getUomUnit() {
        return uomUnit;
    }

    public void setUomUnit(UomUnit uomUnit) {
        this.uomUnit = uomUnit;
    }
}
