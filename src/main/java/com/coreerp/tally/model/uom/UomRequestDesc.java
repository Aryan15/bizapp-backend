package com.coreerp.tally.model.uom;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UomRequestDesc {

    @JsonProperty("REPORTNAME")
    private String reportName ;

    @JsonProperty("STATICVARIABLES")
    private UomStaticVariable uomStaticVariable;

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public UomStaticVariable getUomStaticVariable() {
        return uomStaticVariable;
    }

    public void setUomStaticVariable(UomStaticVariable uomStaticVariable) {
        this.uomStaticVariable = uomStaticVariable;
    }
}
