package com.coreerp.tally.model.ledger;

import com.fasterxml.jackson.annotation.JsonProperty;


public class LeadgerImportData {

    @JsonProperty("REQUESTDATA")
    private LeadgerRequestData leadgerRequestData;

    @JsonProperty("REQUESTDESC")
    private LeadgerRequestDesc leadgerRequestDesc;

    public LeadgerRequestDesc getLeadgerRequestDesc() {
        return leadgerRequestDesc;
    }

    public void setLeadgerRequestDesc(LeadgerRequestDesc leadgerRequestDesc) {
        this.leadgerRequestDesc = leadgerRequestDesc;
    }

    public LeadgerRequestData getLeadgerRequestData() {
        return leadgerRequestData;
    }

    public void setLeadgerRequestData(LeadgerRequestData leadgerRequestData) {
        this.leadgerRequestData = leadgerRequestData;
    }
}
