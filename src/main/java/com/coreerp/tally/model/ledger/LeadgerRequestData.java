package com.coreerp.tally.model.ledger;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class LeadgerRequestData {

    @JacksonXmlProperty(namespace = "TallyUDF", localName = "TALLYMESSAGE")
    private LeadgerTallyMessage leadgerTallyMessage;

    public LeadgerTallyMessage getLeadgerTallyMessage() {
        return leadgerTallyMessage;
    }

    public void setLeadgerTallyMessage(LeadgerTallyMessage leadgerTallyMessage) {
        this.leadgerTallyMessage = leadgerTallyMessage;
    }
}
