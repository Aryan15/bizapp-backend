package com.coreerp.tally.model.ledger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;


@JacksonXmlRootElement(localName = "ENVELOPE")
public class LeadgerHeader {

    @JsonProperty("TALLLYREQUEST")
    private String tallyRequest = "Import Data";

    public String getTallyRequest() {
        return tallyRequest;
    }

    public void setTallyRequest(String tallyRequest) {
        this.tallyRequest = tallyRequest;
    }
}
