package com.coreerp.tally.model.ledger;

import com.fasterxml.jackson.annotation.JsonProperty;


public class LeadgerRequestDesc {


    @JsonProperty("REPORTNAME")
    private String reportName = "All Masters";

    @JsonProperty("STATICVARIABLES")
    private LeadgerStaticVariable leadgerStaticVariable;

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public LeadgerStaticVariable getLeadgerStaticVariable() {
        return leadgerStaticVariable;
    }

    public void setLeadgerStaticVariable(LeadgerStaticVariable leadgerStaticVariable) {
        this.leadgerStaticVariable = leadgerStaticVariable;
    }
}
