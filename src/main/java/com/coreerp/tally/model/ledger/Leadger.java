package com.coreerp.tally.model.ledger;

import com.coreerp.tally.model.voucher.BasicRateOfInvoiceTax;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Leadger {

    @JacksonXmlProperty(isAttribute = true, localName = "NAME")
    private String name = "Purchase";

    @JacksonXmlProperty(isAttribute = true, localName = "RESERVEDNAME")
    private String reservedName = "";

    @JsonProperty("OLDAUDITENTRYIDS.LIST ")
    private LeadgerOldAuditEntryIds leadgerOldAuditEntryIds;

    @JsonProperty("CURRENCYNAME")
    private String currencyName="RUPEE";

    @JsonProperty("PARENT")
    private String parent="Sales Accounts";

    @JsonProperty("GSTAPPLICABLE")
    private String gstApplicable=" Applicable";

    @JsonProperty("GSTTYPEOFSUPPLY")
    private String gstTypeOfSupply ="Goods";

    @JsonProperty("SERVICECATEGORY")
    private String serviceCategory="  Not Applicable";

    @JsonProperty("VATAPPLICABLE")
    private String vatApplicable="Applicable";

    @JsonProperty("ISBILLWISEON")
    private String isBillWiseon="No";

    @JsonProperty("ISCOSTCENTRESON")
    private String isCostcentreson="Yes";

    @JsonProperty("ISUPDATINGTARGETID")
    private String isUpdatingTargetId="No";

    @JsonProperty("ASORIGINAL")
    private String asOriginal="Yes";

    @JsonProperty("AFFECTSSTOCK")
    private String affectsStock="Yes";

    @JsonProperty("LANGUAGENAME.LIST")
    private LeadgerLanguageName leadgerLanguageName;

    @JsonProperty("BASICRATEOFINVOICETAX.LIST")
    private BasicRateOfInvoiceTax basicRateOfInvoiceTax;

    public BasicRateOfInvoiceTax getBasicRateOfInvoiceTax() {
        return basicRateOfInvoiceTax;
    }

    public void setBasicRateOfInvoiceTax(BasicRateOfInvoiceTax basicRateOfInvoiceTax) {
        this.basicRateOfInvoiceTax = basicRateOfInvoiceTax;
    }

    public LeadgerLanguageName getLeadgerLanguageName() {
        return leadgerLanguageName;
    }

    public void setLeadgerLanguageName(LeadgerLanguageName leadgerLanguageName) {
        this.leadgerLanguageName = leadgerLanguageName;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getGstApplicable() {
        return gstApplicable;
    }

    public void setGstApplicable(String gstApplicable) {
        this.gstApplicable = gstApplicable;
    }

    public String getGstTypeOfSupply() {
        return gstTypeOfSupply;
    }

    public void setGstTypeOfSupply(String gstTypeOfSupply) {
        this.gstTypeOfSupply = gstTypeOfSupply;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    public String getVatApplicable() {
        return vatApplicable;
    }

    public void setVatApplicable(String vatApplicable) {
        this.vatApplicable = vatApplicable;
    }

    public String getIsBillWiseon() {
        return isBillWiseon;
    }

    public void setIsBillWiseon(String isBillWiseon) {
        this.isBillWiseon = isBillWiseon;
    }

    public String getIsCostcentreson() {
        return isCostcentreson;
    }

    public void setIsCostcentreson(String isCostcentreson) {
        this.isCostcentreson = isCostcentreson;
    }

    public String getIsUpdatingTargetId() {
        return isUpdatingTargetId;
    }

    public void setIsUpdatingTargetId(String isUpdatingTargetId) {
        this.isUpdatingTargetId = isUpdatingTargetId;
    }

    public String getAsOriginal() {
        return asOriginal;
    }

    public void setAsOriginal(String asOriginal) {
        this.asOriginal = asOriginal;
    }

    public String getAffectsStock() {
        return affectsStock;
    }

    public void setAffectsStock(String affectsStock) {
        this.affectsStock = affectsStock;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReservedName() {
        return reservedName;
    }

    public void setReservedName(String reservedName) {
        this.reservedName = reservedName;
    }

    public LeadgerOldAuditEntryIds getLeadgerOldAuditEntryIds() {
        return leadgerOldAuditEntryIds;
    }

    public void setLeadgerOldAuditEntryIds(LeadgerOldAuditEntryIds leadgerOldAuditEntryIds) {
        this.leadgerOldAuditEntryIds = leadgerOldAuditEntryIds;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }
}
