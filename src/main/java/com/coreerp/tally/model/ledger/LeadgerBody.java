package com.coreerp.tally.model.ledger;

import com.fasterxml.jackson.annotation.JsonProperty;


public class LeadgerBody {

    @JsonProperty("IMPORTDATA")
    private LeadgerImportData leadgerImportData;

    public LeadgerImportData getLeadgerImportData() {
        return leadgerImportData;
    }

    public void setLeadgerImportData(LeadgerImportData leadgerImportData) {
        this.leadgerImportData = leadgerImportData;
    }
}
