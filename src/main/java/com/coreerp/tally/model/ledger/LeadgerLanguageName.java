package com.coreerp.tally.model.ledger;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LeadgerLanguageName {


    @JsonProperty("LANGUAGEID")
    private long languageId = 1033;

    @JsonProperty("NAME.LIST ")
    private LeadgerName leadgerName;

    public long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }

    public LeadgerName getLeadgerName() {
        return leadgerName;
    }

    public void setLeadgerName(LeadgerName leadgerName) {
        this.leadgerName = leadgerName;
    }
}
