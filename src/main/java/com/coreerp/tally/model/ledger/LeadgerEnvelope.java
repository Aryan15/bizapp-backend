package com.coreerp.tally.model.ledger;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;


@JacksonXmlRootElement(localName = "ENVELOPE")
public class LeadgerEnvelope {

    @JsonProperty("HEADER")
    private LeadgerHeader leadgerHeader;

    @JsonProperty("BODY")
    private LeadgerBody leadgerBody;

    public LeadgerHeader getLeadgerHeader() {
        return leadgerHeader;
    }

    public void setLeadgerHeader(LeadgerHeader leadgerHeader) {
        this.leadgerHeader = leadgerHeader;
    }

    public LeadgerBody getLeadgerBody() {
        return leadgerBody;
    }

    public void setLeadgerBody(LeadgerBody leadgerBody) {
        this.leadgerBody = leadgerBody;
    }
}
