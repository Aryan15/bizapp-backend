package com.coreerp.tally.model.ledger;

import com.fasterxml.jackson.annotation.JsonProperty;


public class LeadgerTallyMessage {

    @JsonProperty("LEDGER")
    private Leadger leadger;

    public Leadger getLeadger() {
        return leadger;
    }

    public void setLeadger(Leadger leadger) {
        this.leadger = leadger;
    }
}
