package com.coreerp.tally.model.ledger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class LeadgerOldAuditEntryIds {
    @JacksonXmlProperty(isAttribute = true, localName = "Type")
    private String Type = "Number";


    @JsonProperty("OLDAUDITENTRYIDS")
    private long oldAuditEntryIds =-1;

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public long getOldAuditEntryIds() {
        return oldAuditEntryIds;
    }

    public void setOldAuditEntryIds(long oldAuditEntryIds) {
        this.oldAuditEntryIds = oldAuditEntryIds;
    }
}
