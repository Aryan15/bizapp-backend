package com.coreerp.tally.service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dto.MaterialDTO;
import com.coreerp.service.MaterialService;
import com.coreerp.tally.model.stockitem.*;
import com.coreerp.tally.model.stockitem.StockItemBody;
import com.coreerp.tally.model.stockitem.StockItemEnvelope;
import com.coreerp.tally.model.stockitem.StockItemHeader;
import com.coreerp.tally.model.stockitem.StockItemImportData;
import com.coreerp.tally.model.stockitem.StockItemItem;
import com.coreerp.tally.model.stockitem.StockItemLanguageName;
import com.coreerp.tally.model.stockitem.StockItemName;
import com.coreerp.tally.model.stockitem.StockItemOldAuditEntryIds;
import com.coreerp.tally.model.stockitem.StockItemRequestData;
import com.coreerp.tally.model.stockitem.StockItemRequestDesc;
import com.coreerp.tally.model.stockitem.StockItemStaticVariable;
import com.coreerp.tally.model.stockitem.StockItemTallyMessage;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;



@Service
public class StockItemService {
    final static Logger log= LogManager.getLogger(StockItemService.class);

    @Autowired
    MaterialService materialService;

  /* public InputStreamResource getStockItemXml(){
        XmlMapper xmlMapper = new XmlMapper();
        StockItemEnvelope envelope = getStockItemEnvelope();
        InputStream targetStream = null;
        String outValue = new String();
        try {
            outValue = xmlMapper.writeValueAsString(envelope);
            outValue = outValue.replaceAll("xmlns:wstxns\\d+=","xmlns:UDF=");
            outValue = outValue.replaceAll("wstxns\\d+:", "");

            targetStream = new ByteArrayInputStream(outValue.getBytes());
        }catch (Exception e){
            log.error("Error: "+e);
        }
        return new InputStreamResource(targetStream);
    }



 private StockItemEnvelope getStockItemEnvelope(){
        StockItemEnvelope stockItemEnvelope = new StockItemEnvelope();

        stockItemEnvelope.setStockItemHeader(getStockItemHeader());
        stockItemEnvelope.setStockItemBody(getStockItemBody());

        return stockItemEnvelope;
    }

    private StockItemHeader getStockItemHeader(){
        StockItemHeader stockItemHeader = new StockItemHeader();
        stockItemHeader.setTallyRequest(ApplicationConstants.TALLY_REQUEST);
        return stockItemHeader;
    }

   private StockItemBody getStockItemBody(){
        StockItemBody StockItemBody = new StockItemBody();
        StockItemBody.setStockItemImportData(getStockItemImportData());
        return StockItemBody;
    }

    private StockItemImportData getStockItemImportData(){
        StockItemImportData stockItemImportData = new StockItemImportData();

        stockItemImportData.setStockItemRequestDesc(getStockItemRequestDesc());
        stockItemImportData.setStockItemRequestData(getStockItemRequestData());
        return stockItemImportData;
    }

    private StockItemRequestDesc getStockItemRequestDesc(){
        StockItemRequestDesc  stockItemRequestDesc = new StockItemRequestDesc();
        stockItemRequestDesc.setReportName(ApplicationConstants.TALLY_REPORT_NAME);
        stockItemRequestDesc.setStockItemStaticVariable(getStockItemStaticVariable());
        return stockItemRequestDesc;
    }
    private StockItemStaticVariable getStockItemStaticVariable(){
        StockItemStaticVariable stockItemStaticVariable = new StockItemStaticVariable();

        return  stockItemStaticVariable;
    }

    private StockItemRequestData getStockItemRequestData(){
        StockItemRequestData stockItemRequestData  = new StockItemRequestData();

        stockItemRequestData.setStockItemTallyMessages(getStockItemTallyMessages());

        return stockItemRequestData;
    }
*/
    public List<StockItemTallyMessage> getStockItemTallyMessages(){

        List<StockItemTallyMessage> stockItemTallyMessageArrayList = new ArrayList<>();




        List<MaterialDTO> materials =  materialService.getAllMaterials();

        materials.forEach(material ->
                stockItemTallyMessageArrayList.add(getStockItemTallyMessage(material))
        );



        return stockItemTallyMessageArrayList;
    }

  private StockItemTallyMessage getStockItemTallyMessage(MaterialDTO materialDTO){
        StockItemTallyMessage stockItemTallyMessage = new StockItemTallyMessage();

        stockItemTallyMessage.setStockItemItem(getStockItemItems(materialDTO));
        return stockItemTallyMessage;
    }

   private StockItemItem getStockItemItems(MaterialDTO materialDTO){
        StockItemItem stockItemItems = new StockItemItem();
        stockItemItems.setName(materialDTO.getName());

       stockItemItems.setStockItemOldAuditEntryIds(getStockItemOldAuditEntryIds());
        stockItemItems.setAsOriginal(ApplicationConstants.TALLY_YES_CONSTANT);
        stockItemItems.setBaseUnits(materialDTO.getUnitOfMeasurementName());
        stockItemItems.setCostingMethod(ApplicationConstants.TALLY_COSTING_METHOD);
        stockItemItems.setExciseApplicability(ApplicationConstants.TALLY_EXCISE_APPLICABLE);
        stockItemItems.setGstApplicable(ApplicationConstants.TALLY_EXCISE_APPLICABLE);
        stockItemItems.setGstTypeOfSupply(ApplicationConstants.TALLY_INVOICE_SUB_TYPE_CONSTANT);
//        stockItemItems.setParent(materialDTO.getPartNumber());
        stockItemItems.setIsEntyTaxApplicable(ApplicationConstants.TALLY_EXCISE_APPLICABLE);
       stockItemItems.setIsPerishableon(ApplicationConstants.TALLY_NO_CONSTANT);
       stockItemItems.setIsUpdatingTargetId(ApplicationConstants.TALLY_NO_CONSTANT);
       stockItemItems.setValuationMethod(ApplicationConstants.TALLY_VALUATION_METHOD);
       stockItemItems.setVatApplicable(ApplicationConstants.TALLY_EXCISE_APPLICABLE);
       stockItemItems.setStockItemLanguageName(getStockItemLanguageName(materialDTO));
        return stockItemItems;
    }


    private StockItemOldAuditEntryIds getStockItemOldAuditEntryIds(){
        StockItemOldAuditEntryIds stockItemOldAuditEntryIds = new StockItemOldAuditEntryIds();
        stockItemOldAuditEntryIds.setOldAuditEntryIds(1);
        return  stockItemOldAuditEntryIds;

    }
  private StockItemLanguageName getStockItemLanguageName(MaterialDTO materialDTO){
        StockItemLanguageName stockItemLanguageName = new StockItemLanguageName();
        stockItemLanguageName.setLanguageId(ApplicationConstants.TALLY_LANGUAGE_ID);
        stockItemLanguageName.setStockItemName(getStockItemName(materialDTO));
        return stockItemLanguageName;
    }
   private StockItemName getStockItemName(MaterialDTO materialDTO){
        StockItemName stockItemName = new StockItemName();
        stockItemName.setName(materialDTO.getName());
        return stockItemName;
    }


}
