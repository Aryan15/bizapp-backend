package com.coreerp.tally.service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.UnitOfMeasurementRepository;
import com.coreerp.model.UnitOfMeasurement;
import com.coreerp.tally.model.uom.*;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class UomService {

    @Autowired
    UnitOfMeasurementRepository unitOfMeasurementRepository;

//    public InputStreamResource getUomXML(){
//        XmlMapper xmlMapper = new XmlMapper();
//        UomEnvelope uomEnvelope= getUomEnvelope();
//        InputStream targetStream = null;
//        String outValue = new String();
//        try {
//            outValue = xmlMapper.writeValueAsString(uomEnvelope);
//            outValue = outValue.replaceAll("xmlns:wstxns\\d+=","xmlns:UDF=");
//            outValue = outValue.replaceAll("wstxns\\d+:", "");
//
//            targetStream = new ByteArrayInputStream(outValue.getBytes());
//        }catch (Exception e){
//
//        }
//        return new InputStreamResource(targetStream);
//    }
//
//    private UomEnvelope getUomEnvelope(){
//        UomEnvelope uomEnvelope= new UomEnvelope();
//        uomEnvelope.setUomHeader(getUomHeader());
//        uomEnvelope.setUomBody(getUomBody());
//        return uomEnvelope;
//    }
//
//    private UomHeader getUomHeader(){
//        UomHeader uomHeader = new UomHeader();
//        uomHeader.setTallyRequest(ApplicationConstants.TALLY_REQUEST);
//        return uomHeader;
//    }
//
//    private UomBody getUomBody(){
//        UomBody uomBody = new UomBody();
//        uomBody.setUomImportData(getUomImportData());
//        return uomBody;
//    }
//
//    private UomImportData getUomImportData(){
//        UomImportData uomImportData = new UomImportData();
//
//        uomImportData.setUomRequestDesc(getUomRequestDesc());
//        uomImportData.setUomRequestData(getUomRequestData());
//
//        return uomImportData;
//    }
//
//    private UomRequestDesc getUomRequestDesc(){
//        UomRequestDesc uomRequestDesc = new UomRequestDesc();
//        uomRequestDesc.setReportName(ApplicationConstants.TALLY_REPORT_NAME);
//        uomRequestDesc.setUomStaticVariable(getUomStaticVariable());
//        return uomRequestDesc;
//    }
//
//    private UomStaticVariable getUomStaticVariable(){
//        UomStaticVariable uomStaticVariable = new UomStaticVariable();
//
//        return uomStaticVariable;
//    }
//
//
//    private UomRequestData getUomRequestData(){
//        UomRequestData uomRequestData = new UomRequestData();
//        uomRequestData.setUomTallyMessage(getUomTallyMessages());
//        return uomRequestData;
//    }

    public List<UomTallyMessage> getUomTallyMessages(){
        List<UomTallyMessage> uomTallyMessageList = new ArrayList<>();
        List<UnitOfMeasurement> uoms = unitOfMeasurementRepository.findAll();
        uoms.forEach(uom -> {
            uomTallyMessageList.add(getUomTallyMessage(uom));
        });
        return uomTallyMessageList;
    }

    private UomTallyMessage getUomTallyMessage(UnitOfMeasurement uom){
        UomTallyMessage uomTallyMessage = new UomTallyMessage();

        uomTallyMessage.setUomUnit(getUomUint(uom));

        return uomTallyMessage;
    }

    private UomUnit getUomUint(UnitOfMeasurement uom){
        UomUnit uomUnit = new UomUnit();

        uomUnit.setName(uom.getName());
        uomUnit.setUomName(uom.getName());
//        uomUnit.setOriginalName(uom.getName());
        uomUnit.setGstrepUom(ApplicationConstants.TALLY_INBUILT_TALLY_NAME);
        uomUnit.setIsUpdatingTargetId(ApplicationConstants.TALLY_NO_CONSTANT);
        uomUnit.setAsOriginal(ApplicationConstants.TALLY_YES_CONSTANT);
        uomUnit.setIsGstExcluded(ApplicationConstants.TALLY_NO_CONSTANT);
        uomUnit.setIsSimpleUnit(ApplicationConstants.TALLY_YES_CONSTANT);
        return uomUnit;
    }

}
