package com.coreerp.tally.service;

import com.coreerp.ApplicationConstants;
import com.coreerp.tally.model.gstsource.*;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class GstEnvelopeService {

   /* public InputStreamResource getGstXML(){
        XmlMapper xmlMapper = new XmlMapper();
        GstEnvelope gstEnvelope = getGstEnvelope();
        InputStream targetStream = null;
        String outValue = new String();
        try {
            outValue = xmlMapper.writeValueAsString(gstEnvelope);
            outValue = outValue.replaceAll("xmlns:wstxns\\d+=","xmlns:UDF=");
            outValue = outValue.replaceAll("wstxns\\d+:", "");

            targetStream = new ByteArrayInputStream(outValue.getBytes());
        }catch (Exception e){

        }
        return new InputStreamResource(targetStream);
    }

    private GstEnvelope getGstEnvelope(){
        GstEnvelope gstEnvelope = new GstEnvelope();

        gstEnvelope.setGstHeader(getGstHeader());
        gstEnvelope.setGstBody(getGstBody());
        return gstEnvelope;
    }

    private GstHeader getGstHeader(){
        GstHeader gstHeader = new GstHeader();
        gstHeader.setTallyRequest(ApplicationConstants.TALLY_REQUEST);
        return gstHeader;
    }

    private GstBody getGstBody(){
        GstBody gstBody = new GstBody();

        gstBody.setGstImportData(getGstImportData());
        return gstBody;
    }

    private GstImportData getGstImportData(){
        GstImportData gstImportData = new GstImportData();

        gstImportData.setGstRequestDesc(getGstRequestDesc());
        gstImportData.setGstRequestData(getGstRequestData());
        return gstImportData;
    }

    private GstRequestDesc getGstRequestDesc(){
        GstRequestDesc gstRequestDesc = new GstRequestDesc();
        gstRequestDesc.setReportName(ApplicationConstants.TALLY_REPORT_NAME);
        gstRequestDesc.setGstStaticVariable(gstStaticVariable());
        return gstRequestDesc;
    }

    private  GstStaticVariable gstStaticVariable(){
        GstStaticVariable gstStaticVariable = new GstStaticVariable();
        return  gstStaticVariable;
    }

    private GstRequestData getGstRequestData(){
        GstRequestData gstRequestData = new GstRequestData();

        gstRequestData.setGstTallyMessage(getGstTallyMessages());
        return gstRequestData;
    }*/

    public List<GstTallyMessage> getGstTallyMessages(){
        List<GstTallyMessage> gstTallyMessages = new ArrayList<>();

        GstTallyMessage cgstTallyMessage = new GstTallyMessage();
        cgstTallyMessage.setGstLedger(getGstLedger("CGST","Duties & Taxes","Central Tax","No","","No"));

        gstTallyMessages.add(cgstTallyMessage);

        GstTallyMessage sgstTallyMessage = new GstTallyMessage();
        sgstTallyMessage.setGstLedger(getGstLedger("SGST","Duties & Taxes","State Tax","No","","No"));

        gstTallyMessages.add(sgstTallyMessage);


        GstTallyMessage igstTallyMessage = new GstTallyMessage();
        igstTallyMessage.setGstLedger(getGstLedger("IGST","Duties & Taxes", "Integrated Tax","No","","No"));

        gstTallyMessages.add(igstTallyMessage);


        GstTallyMessage purchaseTallyMessage = new GstTallyMessage();
        purchaseTallyMessage.setGstLedger(getGstLedger("Purchase","Purchase Accounts","","Yes","Yes",""));

        gstTallyMessages.add(purchaseTallyMessage);

        GstTallyMessage salesTallyMessage = new GstTallyMessage();
        salesTallyMessage.setGstLedger(getGstLedger("Sales", "Sales Accounts","","Yes","Yes",""));

        gstTallyMessages.add(salesTallyMessage);

        return gstTallyMessages;
    }

    private GstLedger getGstLedger(String gstType, String parent,String gstDutyHead,String affectStock,String  vat,String gstApplicable){
        GstLedger gstLedger = new GstLedger();
        gstLedger.setName(gstType);
        gstLedger.setGstOldAuditEntryIds(getOldAuditEntryIds());
//        gstLedger.setCurrencyName(ApplicationConstants.TALLY_CURRENCY_TYPE);
        gstLedger.setParent(parent);
        gstLedger.setTaxType(ApplicationConstants.TALLY_TAX_TYPE);
        gstLedger.setGstDutyHead(gstDutyHead);
        gstLedger.setGstTypeOfSupply(ApplicationConstants.TALLY_INVOICE_SUB_TYPE_CONSTANT);
//        gstLedger.setServiceCategory(ApplicationConstants.TALLY_SERVICE_CATEGORY);
        gstLedger.setIsBillWiseon(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setIsUpdatingTargetId(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setAsOriginal(ApplicationConstants.TALLY_YES_CONSTANT);
        gstLedger.setAffectsStock(affectStock);
        gstLedger.setIsGstApplicable(gstApplicable);
        gstLedger.setGstApplicable(gstApplicable);
        gstLedger.setVatApplicable(vat);
        gstLedger.setGstLanguageName(gstLanguageName(gstType));
        gstLedger.setIsCostCentreSon(ApplicationConstants.TALLY_YES_CONSTANT);
        return gstLedger;
    }

    private GstOldAuditEntryIds getOldAuditEntryIds(){
        GstOldAuditEntryIds gstOldAuditEntryIds = new GstOldAuditEntryIds();
        gstOldAuditEntryIds.setOldAuditEntryIds(1);
        return gstOldAuditEntryIds;
    }

    private GstLanguageName gstLanguageName(String gstType){
        GstLanguageName gstLanguageName = new GstLanguageName();
        gstLanguageName.setLanguageId(ApplicationConstants.TALLY_LANGUAGE_ID);
        gstLanguageName.setGstName(getGstName(gstType));
return  gstLanguageName;
    }
    private GstName getGstName(String name){
        GstName gstName = new GstName();
        gstName.setName(name);
        return  gstName;

    }

}
