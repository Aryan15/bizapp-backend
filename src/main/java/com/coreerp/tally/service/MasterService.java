package com.coreerp.tally.service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dto.PartyBankMapDTO;
import com.coreerp.reports.service.PartyBankMapService;
import com.coreerp.service.PartyService;
import com.coreerp.tally.model.bank.BankTallyMessage;
import com.coreerp.tally.model.common.*;
import com.coreerp.tally.model.gstsource.GstEnvelope;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class MasterService {

    @Autowired
    BankServiceses bankServiceses;

    @Autowired
    UomService uomService;

    @Autowired
    CustomerService customerService;

    @Autowired
    GstEnvelopeService gstEnvelopeService;

    @Autowired
    StockItemService stockItemService;

    @Autowired
   DiscountService discountService;

    @Autowired
   RoundOffService roundOffService;

    public InputStreamResource getGstXML(Long partyId){
        XmlMapper xmlMapper = new XmlMapper();
        Envelope envelope = getEnvelope(partyId);
        InputStream targetStream = null;
        String outValue = new String();
        try {
            outValue = xmlMapper.writeValueAsString(envelope);
            outValue = outValue.replaceAll("xmlns:wstxns\\d+=","xmlns:UDF=");
            outValue = outValue.replaceAll("wstxns\\d+:", "");

            targetStream = new ByteArrayInputStream(outValue.getBytes());
        }catch (Exception e){

        }
        return new InputStreamResource(targetStream);
    }

    private Envelope getEnvelope(Long partyId){
        Envelope envelope = new Envelope();
        envelope.setHeader(getHeader(partyId));
        envelope.setBody(getBody(partyId));

        return  envelope;
    }

    private Body getBody(Long partyId){
        Body body = new Body();

        body.setImportData(getImportData(partyId));
        return  body;
    }

    private Header getHeader(Long partyId){
     Header header = new Header();

        header.setTallyRequest(ApplicationConstants.TALLY_REQUEST);
        return  header;
    }

    private ImportData getImportData(Long partyId){
        ImportData importData = new ImportData();

        importData.setRequestData(getRequestData(partyId));
        importData.setRequestDesc(getRequestDesc());
        return  importData;
    }
    private  RequestDesc getRequestDesc() {
        RequestDesc requestDesc = new RequestDesc();
        requestDesc.setReportName(ApplicationConstants.TALLY_REPORT_NAME);
        requestDesc.setStaticVariables(getStaticVaiable());

        return  requestDesc;

    }

    private  StaticVariables getStaticVaiable(){
        StaticVariables staticVariables = new StaticVariables();

        return  staticVariables;
    }
    private RequestData getRequestData(Long partyId){
        RequestData requestData = new RequestData();

        requestData.setTallyMessages(getTallyMessages(partyId));
        return  requestData;
    }

    private List<TallyMessage> getTallyMessages(Long partyId){

        List<TallyMessage> tallyMessages = new ArrayList<>();

        tallyMessages.addAll(bankServiceses.getBankTallyMessages());
        tallyMessages.addAll(uomService.getUomTallyMessages());

        tallyMessages.addAll(customerService.getCustomerTallyMessages(partyId));
        tallyMessages.addAll(gstEnvelopeService.getGstTallyMessages());
        tallyMessages.addAll(stockItemService.getStockItemTallyMessages());
        tallyMessages.addAll(discountService.getDiscountTallyMessages());
        tallyMessages.addAll(roundOffService.getRoundOffTallyMessages());
        return tallyMessages;
    }


}
