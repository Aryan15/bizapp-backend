package com.coreerp.tally.service;

import com.coreerp.ApplicationConstants;
import com.coreerp.tally.model.discount.DiscountLanguageNameList;
import com.coreerp.tally.model.discount.DiscountName;
import com.coreerp.tally.model.discount.DiscountOldEntryList;
import com.coreerp.tally.model.discount.DiscountTallyMessage;
import com.coreerp.tally.model.roundoff.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoundOffService {
    final static Logger log= LogManager.getLogger(RoundOffService.class);

    public List<RoundOffTallyMessage> getRoundOffTallyMessages(){

        List<RoundOffTallyMessage> roundOffTallyMessageList= new ArrayList<>();




        roundOffTallyMessageList.add(getRoundOffTallyMessage());

        return roundOffTallyMessageList;
    }

    private  RoundOffTallyMessage getRoundOffTallyMessage(){

        RoundOffTallyMessage roundOffTallyMessage = new RoundOffTallyMessage();
        roundOffTallyMessage.setRoundOffLeadger(getRoundOfLeadger());

        return  roundOffTallyMessage;
    }

    private RoundOffLeadger getRoundOfLeadger(){
        RoundOffLeadger roundOffLeadger = new RoundOffLeadger();
        roundOffLeadger.setRoundOffOldAuditEntryIds(getOldEntryList());
        roundOffLeadger.setName(ApplicationConstants.TALLY_TYPE_ROUNDOFF);
        roundOffLeadger.setParent(ApplicationConstants.TALLY_TYPE_PARENT);
        roundOffLeadger.setTaxType("Others");
        roundOffLeadger.setRoundingMethod("Normal Rounding");
     /*   roundOffLeadger.setServiceCategory(ApplicationConstants.TALLY_SERVICE_CATEGORY);*/
        roundOffLeadger.setGstApplicable(ApplicationConstants.TALLY_EXCISE_APPLICABLE);
      /*  roundOffLeadger.setTdsApplicable(ApplicationConstants.TALLY_SERVICE_CATEGORY);*/
      /*  roundOffLeadger.setVatApplicable(ApplicationConstants.TALLY_SERVICE_CATEGORY);*/
        roundOffLeadger.setAsOriginal(ApplicationConstants.TALLY_YES_CONSTANT);
        roundOffLeadger.setRoundOffLanguageNameList(getRoundOffLanguageList());
        return  roundOffLeadger;
    }
    private RoundOffOldAuditEntryIds getOldEntryList() {
        RoundOffOldAuditEntryIds roundOffOldAuditEntryIds = new RoundOffOldAuditEntryIds();
        roundOffOldAuditEntryIds.setOldAuditEntryIds(-1);


        return  roundOffOldAuditEntryIds;

    }

    private RoundOffLanguageNameList getRoundOffLanguageList(){
        RoundOffLanguageNameList roundOffLanguageNameList= new RoundOffLanguageNameList();
        roundOffLanguageNameList.setLanguageId(ApplicationConstants.TALLY_LANGUAGE_ID);
        roundOffLanguageNameList.setRoundOffName(getRoundOffName());
        return roundOffLanguageNameList;
    }
    private RoundOffName getRoundOffName(){
        RoundOffName roundOffName =new RoundOffName();
        roundOffName.setName(ApplicationConstants.TALLY_TYPE_ROUNDOFF);
        return roundOffName;
    }



}
