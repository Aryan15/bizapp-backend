package com.coreerp.tally.service;

import com.coreerp.controller.InvoiceController;
import com.coreerp.dao.InvoiceHeaderRepository;
import com.coreerp.dao.TallyExportTrackerRepository;
import com.coreerp.dao.TransactionTypeRepository;
import com.coreerp.dto.PartyDTO;
import com.coreerp.dto.TallyExportTrackerDTO;
import com.coreerp.dto.TallyExportTrackerReport;
import com.coreerp.model.InvoiceHeader;
import com.coreerp.model.PartyType;
import com.coreerp.model.TallyExportTracker;
import com.coreerp.model.TransactionType;
import com.coreerp.service.PartyService;
import com.coreerp.service.PartyTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TallyExportTrackerService {
    final static Logger log = LogManager.getLogger(TallyExportTrackerService.class);
    @Autowired
    TransactionTypeRepository transactionTypeRepository;

    @Autowired
    InvoiceHeaderRepository invoiceHeaderRepository;

    @Autowired
    PartyTypeService partyTypeService;


    @Autowired
    PartyService partyService;

    @Autowired
    TallyExportTrackerRepository tallyExportTrackerRepository;

    public Boolean makeEntry(Long partyId ,Date fromDate, Date toDate, Long invoiceTypeId, Long partyTypeId){

        TransactionType invoiceType = transactionTypeRepository.getOne(invoiceTypeId);


        List<InvoiceHeader> invoiceHeaderList = invoiceHeaderRepository.findByInvoiceTypeAndAndPartyIdAndInvoiceDateGreaterThanEqualOrInvoiceDateLessThanEqual(invoiceType.getId(),partyId,fromDate,toDate);

        Integer invoiceCount = invoiceHeaderList.size();

        PartyType partyType = partyTypeService.getPartyTypeById(partyTypeId);
        List<PartyDTO> parties =  partyService.getPartiesByPartyType(partyType);

        Integer partyCount = parties.size();

        TallyExportTracker tallyExportTracker = new TallyExportTracker();
        tallyExportTracker.setFromDate(fromDate);
        tallyExportTracker.setToDate(toDate);
        tallyExportTracker.setInvoiceTypeId(invoiceTypeId);
        tallyExportTracker.setPartyTypeId(partyTypeId);
        tallyExportTracker.setPartyCount(partyCount);
        tallyExportTracker.setVoucherCount(invoiceCount);

        tallyExportTrackerRepository.save(tallyExportTracker);
       
        return true;

    }


    public TallyExportTrackerReport getPagedTallyTrackerReport(Integer pageNumber, Integer pageSize, String sortDirection, String sortColumn) {


        PageRequest pageRequest = null;
        if(pageNumber != null && pageSize != null && pageNumber >= 0 && pageSize >= 0){
            pageRequest =
                    new PageRequest(pageNumber, pageSize, sortDirection.equals("desc") ? Sort.Direction.DESC : Sort.Direction.ASC, sortColumn != null ? sortColumn : "createdDateTime");
        }

        if(pageSize == -99){
            pageRequest =
                    new PageRequest(pageNumber, Integer.MAX_VALUE, sortDirection.equals("desc") ? Sort.Direction.DESC : Sort.Direction.ASC, sortColumn != null ? sortColumn : "createdDateTime");
        }
        //FinancialYear financialYear = financialYearService.getById(financialYearId);

        Page<TallyExportTracker> data = tallyExportTrackerRepository.findAll(pageRequest);

        TallyExportTrackerReport returnData = new TallyExportTrackerReport();
        List<TallyExportTrackerDTO> dtoList = new ArrayList<>();
        data.get().forEach(d -> {
            TallyExportTrackerDTO dto = new TallyExportTrackerDTO();
            dto.setFromDate(d.getFromDate());
            dto.setToDate(d.getToDate());
            dto.setInvoiceType(d.getInvoiceTypeId().toString());
            dto.setPartyCount(d.getPartyCount());
            dto.setPartyType(d.getPartyTypeId().toString());
            dto.setVoucherCount(d.getVoucherCount());
            dto.setStockItemCount(d.getStockItemCount());
            dto.setDateAndTime(d.getCreatedDateTime());
            dtoList.add(dto);
        });

        returnData.setTallyExportTrackerItems(dtoList);
        returnData.setTotalCount(data.getTotalElements());

        return returnData;
    }
}
