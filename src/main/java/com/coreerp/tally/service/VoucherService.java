package com.coreerp.tally.service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dao.InvoiceHeaderRepository;
import com.coreerp.dao.StateRepository;
import com.coreerp.model.*;
import com.coreerp.service.GlobalSettingService;
import com.coreerp.serviceimpl.GRNBackTrackingServiceNew;
import com.coreerp.tally.model.voucher.BasicRateOfInvoiceTax;
import com.coreerp.tally.model.voucher.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class VoucherService {
    final static Logger log = LogManager.getLogger(VoucherService.class);

    @Autowired
    InvoiceHeaderRepository invoiceHeaderRepository;

    @Autowired
    StateRepository stateRepository;

    @Autowired
    GlobalSettingService globalSettingService;

    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    double totalGrandTotal = 0.0;

    public List<TallyMessageVoucher> getTallyMessageVoucherList(TransactionType invoiceType, Long partyId, Date invoiceFromDate, Date invoiceToDate) {

        List<InvoiceHeader> invoiceHeaderList = invoiceHeaderRepository.findByInvoiceTypeAndAndPartyIdAndInvoiceDateGreaterThanEqualOrInvoiceDateLessThanEqual(invoiceType.getId(), partyId, invoiceFromDate, invoiceToDate);

        List<TallyMessageVoucher> outTallyMessageVouchers = new ArrayList<>();
        invoiceHeaderList.forEach(inv -> {
            TallyMessageVoucher tallyMessageVoucher = getTallyMessageVoucher(inv);
            outTallyMessageVouchers.add(tallyMessageVoucher);

        });

        return outTallyMessageVouchers;

    }


    private TallyMessageVoucher getTallyMessageVoucher(InvoiceHeader invoiceHeader) {
        double sgst = invoiceHeader.getSgstTaxAmount();
        double cgst = invoiceHeader.getCgstTaxAmount();
        double igst = invoiceHeader.getIgstTaxAmount();
        double roundOff = invoiceHeader.getRoundOffAmount();
        totalGrandTotal = sgst + cgst + igst + roundOff;
        List<InvoiceItem> invoiceItems = invoiceHeader.getInvoiceItems();

        if (invoiceHeader.getInclusiveTax() != null && invoiceHeader.getInclusiveTax() == 1) {
            totalGrandTotal += invoiceItems.stream().mapToDouble(InvoiceItem::getAmount).sum();
        } else if (invoiceHeader.getInclusiveTax() != null && invoiceHeader.getInclusiveTax() != 1 && invoiceHeader.getDiscountPercent() != null) {

            totalGrandTotal += invoiceItems.stream().mapToDouble(InvoiceItem::getDiscountAmount).sum() + invoiceItems.stream().mapToDouble(InvoiceItem::getAmount).sum();



        }
         else {
            totalGrandTotal += invoiceItems.stream().mapToDouble(InvoiceItem::getAmountAfterDiscount).sum();

        }

         if(invoiceHeader.getDiscountPercent() != null){
             totalGrandTotal=totalGrandTotal-invoiceHeader.getDiscountAmount();

         }


        TallyMessageVoucher tallyMessageVoucher = new TallyMessageVoucher();
        Voucher voucher = getVoucher(invoiceHeader);
        tallyMessageVoucher.setVoucher(voucher);
        return tallyMessageVoucher;

    }


    private Voucher getVoucher(InvoiceHeader invoiceHeader) {
        List<String> salesTypes = new ArrayList<>();
        salesTypes.add(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
        salesTypes.add(ApplicationConstants.INVOICE_TYPE_INCOMING_JW_INVOICE);


        List<String> purchaseTypes = new ArrayList<>();
        purchaseTypes.add(ApplicationConstants.INVOICE_TYPE_SUPPLIER);
        purchaseTypes.add(ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE);

        Voucher voucher = new Voucher();

        voucher.setReference(invoiceHeader.getInvoiceNumber());

        //voucher.setVchKey(invoiceHeader.getId());
        voucher.setRemoteId(invoiceHeader.getId());
        voucher.setGuid(invoiceHeader.getId());
        if (invoiceHeader.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {
            voucher.setAction("Cancel");
        } else {
            voucher.setAction("Create");
        }


        if (!invoiceHeader.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {

            voucher.setLedgerEntries(getLedgerEntries(invoiceHeader));
            if (invoiceHeader.getDiscountPercent() != null) {
                voucher.setDiscountLeadgerEntries(getLedgerEntriesDiscount(invoiceHeader));
            }

        }


        voucher.setInvoiceOrderList(getInvoiceOrderList(invoiceHeader));
        voucher.setPartyName(invoiceHeader.getPartyName());
        voucher.setDate(dateFormat.format(invoiceHeader.getInvoiceDate()));
        voucher.setStateName(invoiceHeader.getParty().getState().getName());
        voucher.setCountryOfResidence(invoiceHeader.getParty().getCountry().getName());
        voucher.setPartyGstin(invoiceHeader.getGstNumber());
        voucher.setPartyName(invoiceHeader.getPartyName());


        if (salesTypes.contains(invoiceHeader.getInvoiceType().getName())) {
            voucher.setVoucherTypeName(ApplicationConstants.TALLY_SALES_VOUCHER);
        } else if (purchaseTypes.contains(invoiceHeader.getInvoiceType().getName())) {
            voucher.setVoucherTypeName(ApplicationConstants.TALLY_PURCHASE_VOUCHER);
        }
        voucher.setVoucherNumber(invoiceHeader.getInvoiceNumber());
        voucher.setPartyLeadgerName(invoiceHeader.getPartyName());
        voucher.setBasicBasePartyName(invoiceHeader.getPartyName());
        voucher.setPersistedView(ApplicationConstants.TALLY_PERSISTED_VIEW);
        voucher.setPlaceOfSupply(invoiceHeader.getParty().getState().getName());
        voucher.setPartyGstin(invoiceHeader.getGstNumber());
        voucher.setBasicBuyerName(invoiceHeader.getPartyName());
        voucher.setBasicFinalDestination(invoiceHeader.getPartyAddress());
        voucher.setBasicShipVesSelNo(invoiceHeader.getVehicleNumber());
//        voucher.setBasicDueDateOfPymt(invoiceHeader.getModeOfDispatch());
        voucher.setConsigneeStateName(invoiceHeader.getParty().getState().getName());
        voucher.setDiffActualQty(ApplicationConstants.TALLY_NO_CONSTANT);
        voucher.setAsOriginal(ApplicationConstants.TALLY_NO_CONSTANT);
        voucher.setForJobCosting(ApplicationConstants.TALLY_NO_CONSTANT);
        voucher.setIsOptionl(ApplicationConstants.TALLY_NO_CONSTANT);
        voucher.setEffectiveDate(dateFormat.format(invoiceHeader.getInvoiceDate()));
        voucher.setIsGstOverRidden(ApplicationConstants.TALLY_NO_CONSTANT);
        voucher.setIgnoreGstInvalidation(ApplicationConstants.TALLY_NO_CONSTANT);
        if (invoiceHeader.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {
            voucher.setIsCancelled(ApplicationConstants.TALLY_YES_CONSTANT);
        } else {
            voucher.setIsCancelled(ApplicationConstants.TALLY_NO_CONSTANT);
        }
        voucher.setIsPostDated(ApplicationConstants.TALLY_NO_CONSTANT);
        voucher.setUserTrackingNumber(ApplicationConstants.TALLY_NO_CONSTANT);
        voucher.setIsInvoice(ApplicationConstants.TALLY_YES_CONSTANT);
        voucher.setIsDeleted(ApplicationConstants.TALLY_NO_CONSTANT);

        voucher.setCurrPartyLeadgerName(invoiceHeader.getCompanyName());
        voucher.setCurrBasicBuyerName(invoiceHeader.getPartyName());
        voucher.setCurrPartyName(invoiceHeader.getPartyName());
        voucher.setCurrStateName(invoiceHeader.getCompanyStateName());
        voucher.setTempGstWayBillNumber(invoiceHeader.geteWayBillNumber());
        voucher.setTempGstEwaySubType(ApplicationConstants.TALLY_GST_SUBTYPE);

        if (salesTypes.contains(invoiceHeader.getInvoiceType().getName())) {
            voucher.setTempGstEwayDocumentType(ApplicationConstants.TALLY_TAX_INVOICE);
        } else if (purchaseTypes.contains(invoiceHeader.getInvoiceType().getName())) {
            voucher.setTempGstEwayDocumentType(ApplicationConstants.TALLY_PURCHASE_INVOICE);
        }


        voucher.setTempGstEWAYCONSIGNOR(invoiceHeader.getCompanyName());
        voucher.setTempGstEwayPinCodeNumber(invoiceHeader.getCompanyPinCode());
        voucher.setTempGstEwayPinCode(invoiceHeader.getCompanyPinCode());
        voucher.setTempGsteWayConsignortin(invoiceHeader.getCompanyGstNumber());
        voucher.setTempGstEwayConsignorState(invoiceHeader.getCompanyStateName());
        voucher.setTempGstEwayConsshipFromState(invoiceHeader.getCompanyStateName());
        voucher.setTempGstEwayConsignee(invoiceHeader.getPartyName());
        voucher.setTempGstEwayConspinCodeNumber(invoiceHeader.getPartyPinCode());
        voucher.setTempGstEwayConspinCode(invoiceHeader.getPartyPinCode());
        voucher.setTempGstEwayConstin(invoiceHeader.getGstNumber());
        voucher.setTempGstEwayConsState(invoiceHeader.getParty().getState().getName());
        voucher.setTempGstEwayConsShipToState(invoiceHeader.getParty().getState().getName());
        voucher.setTempGstEwayVehicleNumber(invoiceHeader.getVehicleNumber());
        voucher.setTempGstEwayVehicleType(invoiceHeader.getModeOfDispatch());
        if (!invoiceHeader.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {
            voucher.setInvoiceDelNotesList(getInvoiceDelNotesList(invoiceHeader));


            voucher.setEwayBillDetailsList(getEwayBillDetailsList(invoiceHeader));
            voucher.setBasicOrderTermsList(getBasicOrderTermsList(invoiceHeader));
            voucher.setBasicBuyerAddressList(getBasicBuyerAddressList(invoiceHeader));
            voucher.setAddressList(getAddressList(invoiceHeader));
            if (invoiceHeader.getDiscountAmount() != null && invoiceHeader.getDiscountAmount() != 0) {
                voucher.setHasDiscounts(ApplicationConstants.TALLY_YES_CONSTANT);
            } else {
                voucher.setHasDiscounts(ApplicationConstants.TALLY_NO_CONSTANT);
            }
        }
        /* if(invoiceHeader.getDiscountAmount()!=null ||invoiceHeader.getDiscountAmount()!=0) {*/

        /*   }*/
        /* else  {*/
        /*    voucher.setHasDiscounts(ApplicationConstants.TALLY_NO_CONSTANT);
        }*/

        //newly ADDED
      /*  voucher.setOldAuditEntryList(getOldAuditEntryList(invoiceHeader));
        voucher.setInvDeliveryDate();
        voucher.setFbtPaymentType();
        voucher.setVatDocumentType();
        voucher.setVatTransBillQty();
        voucher.setBasicDateOfInvoice();
        voucher.setBasicDateOfRemoval();
        voucher.setConsigneePinNumber();
        voucher.setIsMstfromSync();
        voucher.setUseForExcise();
        voucher.setIsForJobWorkIn();
        voucher.setAllowConsumption();
        voucher.setUseForIntrest();
        voucher.setUseForGainLoss();
        voucher.setUseForGoDownTransfer();
        voucher.setUseForCompound();
        voucher.setUseForServiceTax();
        voucher.setIsExciseVoucher();
        voucher.setExciseTaxOverRide();
        voucher.setUseForTaxUnitTransfer();
        voucher.setIgnorePosValidation();
        voucher.setExciseOpening();
        voucher.setUseForFinalProduction();
        voucher.setIstdsOverRidden();
        voucher.setIncludeAdvPymtvch();
        voucher.setIsSubWorksContract();
        voucher.setIsVatOverRidden();
        voucher.setIgnoreOrigVCHdate();
        voucher.setIsVatPaidAtCustoms();
        voucher.setIsDeclaredToCustoms();
        voucher.setIsServiceTaxOverRidden();
        voucher.setIsIsdvoucher();
        voucher.setIsExciseOverRidden();
        voucher.setIsExciseSupplyYvch();
        voucher.setGstNotExported();
        voucher.setIsVatPrincipalAccount();
        voucher.setIsBoeNotApplicable();
        voucher.setIsShippingWithInState();
        voucher.setIsOverseasTouristTrans();
        voucher.setIsDesignatedZoneParty();*/

        //
        if (!invoiceHeader.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {
            voucher.setSgstLedger(getSgstLedger(invoiceHeader));
            voucher.setCgstLedger(getCgstLedger(invoiceHeader));
            voucher.setIgstLedger(getIgstLedger(invoiceHeader));
            voucher.setRoundOffLeadgerEntries(getRoundOfLeadger(invoiceHeader));
            voucher.setGsteWayConsignorAddress(getGsteWayConsignorAddress(invoiceHeader));
            voucher.setGsteWayConsignorAddressParty(getGsteWayConsignorAddressParty(invoiceHeader));

        }
        if (!invoiceHeader.getStatus().getName().equals(ApplicationConstants.INVOICE_STATUS_CANCELLED)) {
            List<Inventory> inventoryList = getInventoryList(invoiceHeader.getInvoiceItems(), invoiceHeader);
            voucher.setInventoryList(inventoryList);
        }
        return voucher;

    }

    private EwayBillDetailsList getEwayBillDetailsList(InvoiceHeader invoiceHeader) {

        List<String> salesTypes = new ArrayList<>();
        salesTypes.add(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
        salesTypes.add(ApplicationConstants.INVOICE_TYPE_INCOMING_JW_INVOICE);


        List<String> purchaseTypes = new ArrayList<>();
        purchaseTypes.add(ApplicationConstants.INVOICE_TYPE_SUPPLIER);
        purchaseTypes.add(ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE);


        EwayBillDetailsList ewayBillDetailsList = new EwayBillDetailsList();
        ewayBillDetailsList.setConsignorAddressList(getConsignorAddressList(invoiceHeader));
        ewayBillDetailsList.setConsignorPartyAddressList(getConsignorPartyAddressList(invoiceHeader));

        if (salesTypes.contains(invoiceHeader.getInvoiceType().getName())) {
            ewayBillDetailsList.setDocumentType(ApplicationConstants.TALLY_TAX_INVOICE);
        } else {
            ewayBillDetailsList.setDocumentType(ApplicationConstants.TALLY_PURCHASE_INVOICE);
        }
//        if (invoiceHeader.getInvoiceType().equals(ApplicationConstants.TALLY_INVOICE_TYPE)) {
//            ewayBillDetailsList.setDocumentType(ApplicationConstants.TALLY_INVOICE_TYPE_CONSTANT);
//        }
        ewayBillDetailsList.setConsigneeGstin(invoiceHeader.getGstNumber());

        Optional<State> stateName = stateRepository.findById(invoiceHeader.getPartyStateId());
        ewayBillDetailsList.setConsigneeStateName(invoiceHeader.getParty().getState().getName());
        ewayBillDetailsList.setConsigneePinCode(invoiceHeader.getPartyPinCode());
        ewayBillDetailsList.setBillNumber(invoiceHeader.geteWayBillNumber());
        ewayBillDetailsList.setSubType(ApplicationConstants.TALLY_INVOICE_SUB_TYPE_CONSTANT);
        ewayBillDetailsList.setConsignorName(invoiceHeader.getCompanyName());
        ewayBillDetailsList.setConsignorPinCode(invoiceHeader.getCompanyPinCode());
        ewayBillDetailsList.setConsignorGstin(invoiceHeader.getCompanyGstNumber());
        ewayBillDetailsList.setConsignorStateName(invoiceHeader.getCompanyStateName());
        ewayBillDetailsList.setConsigneeName(invoiceHeader.getPartyName());
        ewayBillDetailsList.setShippedFromState(invoiceHeader.getCompanyStateName());
        ewayBillDetailsList.setShippedToState(invoiceHeader.getParty().getState().getName());
        ewayBillDetailsList.setIgnoreGstInvalidation(ApplicationConstants.TALLY_NO_CONSTANT);

        ewayBillDetailsList.setTransportDetailsList(getTransportDetailsList(invoiceHeader));

        return ewayBillDetailsList;
    }

    private ConsignorAddressList getConsignorAddressList(InvoiceHeader invoiceHeader) {
        ConsignorAddressList consignorAddressList = new ConsignorAddressList();
        consignorAddressList.setConsignorAddress(invoiceHeader.getCompanyAddress());

        return consignorAddressList;
    }

    private ConsignorPartyAddressList getConsignorPartyAddressList(InvoiceHeader invoiceHeader) {
        ConsignorPartyAddressList consignorPartyAddressList = new ConsignorPartyAddressList();
        consignorPartyAddressList.setConsignorAddress(invoiceHeader.getPartyAddress());

        return consignorPartyAddressList;
    }

    private TransportDetailsList getTransportDetailsList(InvoiceHeader invoiceHeader) {
        TransportDetailsList transportDetailsList = new TransportDetailsList();
        transportDetailsList.setTransportMode(invoiceHeader.getModeOfDispatch());
        transportDetailsList.setVehicleNumber(invoiceHeader.getVehicleNumber());
        transportDetailsList.setVehicleType(ApplicationConstants.TALLY_VEHICLE_TYPE);
        transportDetailsList.setIgnoreVehicleNoValidation(ApplicationConstants.TALLY_NO_CONSTANT);

        return transportDetailsList;
    }


    private BasicOrderTermsList getBasicOrderTermsList(InvoiceHeader invoiceHeader) {
        BasicOrderTermsList basicOrderTermsList = new BasicOrderTermsList();
        basicOrderTermsList.setBasicOrderTerms(invoiceHeader.getDeliveryTerms());
        return basicOrderTermsList;
    }

    private BasicBuyerAddressList getBasicBuyerAddressList(InvoiceHeader invoiceHeader) {
        BasicBuyerAddressList basicBuyerAddressList = new BasicBuyerAddressList();
        basicBuyerAddressList.setBasicBuyerAddress(invoiceHeader.getPartyAddress());

        return basicBuyerAddressList;
    }

    private AddressList getAddressList(InvoiceHeader invoiceHeader) {
        AddressList addressList = new AddressList();
        addressList.setAddress(invoiceHeader.getPartyAddress());
        return addressList;
    }

    /*private  OldAuditEntryList getOldAuditEntryList(InvoiceHeader invoiceHeader){
        OldAuditEntryList oldAuditEntryList = new OldAuditEntryList();
        oldAuditEntryList.setOldAuditEntryIds("-1");
          return  oldAuditEntryList;
    }*/

    private InvoiceDelNotesList getInvoiceDelNotesList(InvoiceHeader invoiceHeader) {

        InvoiceDelNotesList invoiceDelNotesList = new InvoiceDelNotesList();
        invoiceDelNotesList.setBasicShipDeliveryNote(invoiceHeader.getRemarks());
        invoiceDelNotesList.setBasicShippingDate(dateFormat.format(invoiceHeader.getInvoiceDate()));


        return invoiceDelNotesList;
    }

    private GstLedger getSgstLedger(InvoiceHeader invoiceHeader) {
        GstLedger gstLedger = new GstLedger();
        gstLedger.setLedgerName(ApplicationConstants.TALLY_TAX_SGST);
        gstLedger.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setIsCapVatNotClaimed(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setIsLastDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setLedgetFromItem(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setAmount(invoiceHeader.getSgstTaxAmount());
        gstLedger.setGstDutyAmount(invoiceHeader.getSgstTaxAmount());
        gstLedger.setVatexpAmount(invoiceHeader.getSgstTaxAmount());

        return gstLedger;
    }

    private GstLedger getCgstLedger(InvoiceHeader invoiceHeader) {
        GstLedger gstLedger = new GstLedger();
        gstLedger.setLedgerName(ApplicationConstants.TALLY_TAX_CGST);
        gstLedger.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setIsCapVatNotClaimed(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setIsLastDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setLedgetFromItem(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setAmount(invoiceHeader.getCgstTaxAmount());
        gstLedger.setGstDutyAmount(invoiceHeader.getCgstTaxAmount());
        gstLedger.setVatexpAmount(invoiceHeader.getCgstTaxAmount());
        return gstLedger;
    }

    private GstLedger getIgstLedger(InvoiceHeader invoiceHeader) {
        GstLedger gstLedger = new GstLedger();

        gstLedger.setLedgerName(ApplicationConstants.TALLY_TAX_IGST);
        gstLedger.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setIsCapVatNotClaimed(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setIsLastDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setLedgetFromItem(ApplicationConstants.TALLY_NO_CONSTANT);
        gstLedger.setAmount(invoiceHeader.getIgstTaxAmount());
        gstLedger.setGstDutyAmount(invoiceHeader.getIgstTaxAmount());
        gstLedger.setVatexpAmount(invoiceHeader.getIgstTaxAmount());

        return gstLedger;
    }

    private GsteWayConsignorAddress getGsteWayConsignorAddress(InvoiceHeader invoiceHeader) {
        GsteWayConsignorAddress gsteWayConsignorAddress = new GsteWayConsignorAddress();
        gsteWayConsignorAddress.setTempConsignorAddress(invoiceHeader.getCompanyAddress());
        return gsteWayConsignorAddress;
    }

    private GsteWayConsignorAddress getGsteWayConsignorAddressParty(InvoiceHeader invoiceHeader) {
        GsteWayConsignorAddress gsteWayConsignorAddress = new GsteWayConsignorAddress();
        gsteWayConsignorAddress.setTempConsignorAddress(invoiceHeader.getPartyAddress());
        return gsteWayConsignorAddress;
    }

    private LedgerEntries getLedgerEntries(InvoiceHeader invoiceHeader) {
        LedgerEntries ledgerEntries = new LedgerEntries();
        Double roundOffAmount = invoiceHeader.getRoundOffAmount();

            if (invoiceHeader.getGrandTotal() == totalGrandTotal) {
                ledgerEntries.setAmount(-1 * invoiceHeader.getGrandTotal());
                ledgerEntries.setAmountTotal(-1 * invoiceHeader.getGrandTotal());
            } else {

                ledgerEntries.setAmount(-1 * totalGrandTotal);
                ledgerEntries.setAmountTotal(-1 * totalGrandTotal);
            }

        ledgerEntries.setBillType(ApplicationConstants.TALLY_BILLING_TYPE);
        ledgerEntries.setIsCapvatNotClaimed(ApplicationConstants.TALLY_NO_CONSTANT);
        ledgerEntries.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        ledgerEntries.setIsLastdeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        ledgerEntries.setIsPartyLedger(ApplicationConstants.TALLY_NO_CONSTANT);
        ledgerEntries.setLedgerFromItem(ApplicationConstants.TALLY_NO_CONSTANT);
        ledgerEntries.setLedgerName(invoiceHeader.getPartyName());
        ledgerEntries.setName(invoiceHeader.getInvoiceNumber());
        return ledgerEntries;
    }


    private DiscountLeadgerEntries getLedgerEntriesDiscount(InvoiceHeader invoiceHeader) {
        DiscountLeadgerEntries discountledgerEntries = new DiscountLeadgerEntries();
        discountledgerEntries.setOldAuditEntryList(getOldAuditEntryList());
        discountledgerEntries.setBasicRateOfInvoiceTax(getBasicRate(invoiceHeader));
        discountledgerEntries.setAmount(-1 * invoiceHeader.getDiscountAmount());
//        if() {
//            discountledgerEntries.setAmount(-1 * invoiceHeader.getDiscountAmount());
//        }
//
//         else if( invoiceHeader.getDiscountPercent()==null && invoiceHeader.getDiscountPercent()==0){
//            discountledgerEntries.setAmount(0.0);
//        }
        discountledgerEntries.setIsCapvatNotClaimed(ApplicationConstants.TALLY_NO_CONSTANT);
        discountledgerEntries.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        discountledgerEntries.setIsLastdeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        discountledgerEntries.setLedgerFromItem(ApplicationConstants.TALLY_NO_CONSTANT);
        discountledgerEntries.setLedgerName(ApplicationConstants.TALLY_TYPE_DISCOUNT);
        discountledgerEntries.setVatexpAmount(-1 * invoiceHeader.getDiscountAmount());
        discountledgerEntries.setIsPartyLedger(ApplicationConstants.TALLY_NO_CONSTANT);

        return discountledgerEntries;
    }

    private RoundOffLeadgerEntries getRoundOfLeadger(InvoiceHeader invoiceHeader) {

        RoundOffLeadgerEntries roundOffLeadgerEntries = new RoundOffLeadgerEntries();
        roundOffLeadgerEntries.setOldAuditEntryList(getOldAuditEntryList());
        roundOffLeadgerEntries.setRoundOffBasicRateOfInvoiceTax(getRoundOfBasicTaxRate(invoiceHeader));
        roundOffLeadgerEntries.setAmount(invoiceHeader.getRoundOffAmount());
        roundOffLeadgerEntries.setIsCapvatNotClaimed(ApplicationConstants.TALLY_NO_CONSTANT);
        roundOffLeadgerEntries.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        roundOffLeadgerEntries.setIsLastdeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        roundOffLeadgerEntries.setLedgerFromItem(ApplicationConstants.TALLY_NO_CONSTANT);
        roundOffLeadgerEntries.setLedgerName(ApplicationConstants.TALLY_TYPE_ROUNDOFF);
        roundOffLeadgerEntries.setVatexpAmount(invoiceHeader.getRoundOffAmount());
        roundOffLeadgerEntries.setIsPartyLedger(ApplicationConstants.TALLY_NO_CONSTANT);
        return roundOffLeadgerEntries;
    }


    private OldAuditEntryList getOldAuditEntryList() {
        OldAuditEntryList oldAuditEntryList = new OldAuditEntryList();
        oldAuditEntryList.setOldAuditEntryIds("-1");
        return oldAuditEntryList;
    }

    private BasicRateOfInvoiceTax getBasicRate(InvoiceHeader invoiceHeader) {
        BasicRateOfInvoiceTax basicRateOfInvoiceTax = new BasicRateOfInvoiceTax();
        if (invoiceHeader.getDiscountPercent() != null) {
            basicRateOfInvoiceTax.setBasicRateOfInvoiceTaxPercent(-1 * invoiceHeader.getDiscountPercent());
        }

//        if(invoiceHeader.getDiscountPercent()!=null) {
//            basicRateOfInvoiceTax.setBasicRateOfInvoiceTaxPercent(-1 * invoiceHeader.getDiscountPercent());
//        }
//        else if( invoiceHeader.getDiscountPercent()==null && invoiceHeader.getDiscountPercent()==0){
//            basicRateOfInvoiceTax.setBasicRateOfInvoiceTaxPercent(0.0);
//        }
        return basicRateOfInvoiceTax;
    }

    private RoundOffBasicRateOfInvoiceTax getRoundOfBasicTaxRate(InvoiceHeader invoiceHeader) {
        RoundOffBasicRateOfInvoiceTax roundOffBasicRateOfInvoiceTax = new RoundOffBasicRateOfInvoiceTax();
        roundOffBasicRateOfInvoiceTax.setRoundOffBasicRateOfInvoiceTaxPercent(invoiceHeader.getRoundOffAmount());
        return roundOffBasicRateOfInvoiceTax;
    }


    private InvoiceOrderList getInvoiceOrderList(InvoiceHeader invoiceHeader) {
        InvoiceOrderList invoiceOrderList = new InvoiceOrderList();
        invoiceOrderList.setBasicOrderDate(invoiceHeader.getPurchaseOrderDate() != null ? dateFormat.format(invoiceHeader.getPurchaseOrderDate()) : null);
        invoiceOrderList.setBasicPurchaseOrderNo(invoiceHeader.getPurchaseOrderNumber());

        return invoiceOrderList;
    }

    private List<Inventory> getInventoryList(List<InvoiceItem> invoiceItems, InvoiceHeader invoiceHeader) {

        List<Inventory> outInventoryList = new ArrayList<>();


        invoiceItems.forEach(item -> {

            Inventory inventory = getInventory(item, invoiceHeader);
            outInventoryList.add(inventory);


        });

        return outInventoryList;
    }

    private Inventory getInventory(InvoiceItem invoiceItem, InvoiceHeader invoiceHeader) {
        Inventory inventory = new Inventory();
        GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);
        inventory.setActualQty(invoiceItem.getQuantity().toString());
        inventory.setStockItemName(invoiceItem.getMaterial().getName());
        inventory.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        inventory.setIsLastDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        inventory.setIsAutoneGate(ApplicationConstants.TALLY_NO_CONSTANT);
        inventory.setIsScrap(ApplicationConstants.TALLY_NO_CONSTANT);
        inventory.setRate(invoiceItem.getPrice().toString() + "/" + invoiceItem.getUnitOfMeasurement().getName());


        if (invoiceItem.getDiscountPercentage() != null) {
            inventory.setDiscount(invoiceItem.getDiscountPercentage().toString());
        }
        inventory.setBilledQty(invoiceItem.getQuantity().toString());
        if (invoiceHeader.getInclusiveTax() != null && invoiceHeader.getInclusiveTax() == 1) {
            inventory.setIsInclTaxRateFieldEdited(ApplicationConstants.TALLY_YES_CONSTANT);
            inventory.setAmount(invoiceItem.getAmount());
        } else if (invoiceHeader.getInclusiveTax() != null && invoiceHeader.getInclusiveTax() != 1 && invoiceHeader.getDiscountPercent() != null) {
            inventory.setAmount(invoiceItem.getAmount() + invoiceItem.getDiscountAmount());

        } else {
            inventory.setIsInclTaxRateFieldEdited(ApplicationConstants.TALLY_NO_CONSTANT);
            inventory.setAmount(invoiceItem.getAmountAfterDiscount());
        }


        inventory.setIsExciseOverRideDetails(ApplicationConstants.TALLY_NO_CONSTANT);
       /* if (globalSetting.getItemLevelDiscount()==0) {
            inventory.setEidIsCountRate(getEidIsCountRate(invoiceItem, invoiceHeader));
            inventory.setEidIsCountAmount(getEidIsCountAmount(invoiceItem, invoiceHeader));
        }*/
        inventory.setAccountingAllocations(getAccountingAllocations(invoiceItem, invoiceHeader));

        return inventory;
    }


    private EidIsCountRateList getEidIsCountRate(InvoiceItem invoiceItem, InvoiceHeader invoiceHeader) {

        EidIsCountRateList eidIsCountRate = new EidIsCountRateList();

        eidIsCountRate.setDesc(ApplicationConstants.TALLY_EI_DISCOUNT_RATE);
        eidIsCountRate.setIsList(ApplicationConstants.TALLY_YES_CONSTANT);
        eidIsCountRate.setType(ApplicationConstants.TALLY_TYPE);
        eidIsCountRate.setIndex("10001");

        eidIsCountRate.setEidIsCountRate(getEidIsCountRates(invoiceItem, invoiceHeader));
        return eidIsCountRate;
    }


    private EidIsCountRate getEidIsCountRates(InvoiceItem invoiceItem, InvoiceHeader invoiceHeader) {
        EidIsCountRate eidIsCountRate = new EidIsCountRate();
        if (invoiceHeader.getDiscountPercent() != null) {
            eidIsCountRate.setDiscountRate(invoiceHeader.getDiscountPercent().toString());
        }
        return eidIsCountRate;

    }

    private EidIsCountAmountList getEidIsCountAmount(InvoiceItem invoiceItem, InvoiceHeader invoiceHeader) {
        EidIsCountAmountList eidIsCountAmount = new EidIsCountAmountList();
        eidIsCountAmount.setDesc(ApplicationConstants.TALLY_EI_DISCOUNT_AMOUNT);
        eidIsCountAmount.setIsList(ApplicationConstants.TALLY_YES_CONSTANT);
        eidIsCountAmount.setType(ApplicationConstants.TALLY_TYPE_AMOUNT);
        eidIsCountAmount.setIndex("10003");
        eidIsCountAmount.setEidIsAmount(getEidIsAmount(invoiceItem, invoiceHeader));
        return eidIsCountAmount;
    }


    private EidIsAmount getEidIsAmount(InvoiceItem invoiceItem, InvoiceHeader invoiceHeader) {

        EidIsAmount eidIsAmount = new EidIsAmount();
        if (invoiceHeader.getDiscountAmount() != null) {
            eidIsAmount.setDiscountAmount(invoiceHeader.getDiscountAmount().toString());
        }
        return eidIsAmount;
    }

    private AccountingAllocations getAccountingAllocations(InvoiceItem invoiceItem, InvoiceHeader invoiceHeader) {
        AccountingAllocations accountingAllocations = new AccountingAllocations();
        GlobalSetting globalSetting = globalSettingService.getGlobalSettingById(1l);
        List<String> salesTypes = new ArrayList<>();
        salesTypes.add(ApplicationConstants.INVOICE_TYPE_CUSTOMER);
        salesTypes.add(ApplicationConstants.INVOICE_TYPE_INCOMING_JW_INVOICE);


        List<String> purchaseTypes = new ArrayList<>();
        purchaseTypes.add(ApplicationConstants.INVOICE_TYPE_SUPPLIER);
        purchaseTypes.add(ApplicationConstants.INVOICE_TYPE_OUTGOING_JW_INVOICE);

        if (salesTypes.contains(invoiceHeader.getInvoiceType().getName())) {
            accountingAllocations.setLedgerName(ApplicationConstants.TALLY_SALES_VOUCHER);
        } else {
            accountingAllocations.setLedgerName(ApplicationConstants.TALLY_PURCHASE_VOUCHER);
        }
        accountingAllocations.setIsDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        accountingAllocations.setIsCapvatNotClaimed(ApplicationConstants.TALLY_NO_CONSTANT);
        accountingAllocations.setIsPartyLedger(ApplicationConstants.TALLY_NO_CONSTANT);
        accountingAllocations.setIsLastDeemedPositive(ApplicationConstants.TALLY_NO_CONSTANT);
        accountingAllocations.setLedgerFromtem(ApplicationConstants.TALLY_NO_CONSTANT);
        if (invoiceHeader.getInclusiveTax() != null && invoiceHeader.getInclusiveTax() == 1) {

            accountingAllocations.setAmount(invoiceItem.getAmount());
        } else if (invoiceHeader.getInclusiveTax() != null && invoiceHeader.getInclusiveTax() != 1 && invoiceHeader.getDiscountPercent() != null) {

            accountingAllocations.setAmount(invoiceItem.getAmount() + invoiceItem.getDiscountAmount());
        } else {
            accountingAllocations.setAmount(invoiceItem.getAmountAfterDiscount());
        }
        return accountingAllocations;
    }
}
