package com.coreerp.tally.service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dto.PartyBankMapDTO;
import com.coreerp.tally.model.bank.*;
import com.coreerp.tally.model.discount.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DiscountService {
    final static Logger log= LogManager.getLogger(DiscountService.class);

    public List<DiscountTallyMessage> getDiscountTallyMessages(){

        List<DiscountTallyMessage> discountTallyMessageList= new ArrayList<>();




        discountTallyMessageList.add(getDiscountTallyMessage());

        return discountTallyMessageList;
    }
    private DiscountTallyMessage getDiscountTallyMessage(){
       DiscountTallyMessage discountTallyMessage = new DiscountTallyMessage();
        discountTallyMessage.setDiscountLeadger(getDiscountLeadger());

        return  discountTallyMessage;
    }
    private DiscountLeadger getDiscountLeadger(){

      DiscountLeadger discountLeadger = new DiscountLeadger();
        discountLeadger.setName(ApplicationConstants.TALLY_TYPE_DISCOUNT);
        discountLeadger.setParent(ApplicationConstants.TALLY_TYPE_PARENT);
        discountLeadger.setDiscountOldEntryList(getOldEntryList());
        discountLeadger.setGstApplicable(ApplicationConstants.TALLY_EXCISE_APPLICABLE);
        discountLeadger.setTaxType("Others");
        discountLeadger.setGstTypeOfSupply("Services");
       /* discountLeadger.setServiceCategory(ApplicationConstants.TALLY_SERVICE_CATEGORY);
        discountLeadger.setVatApplicable(ApplicationConstants.TALLY_SERVICE_CATEGORY);*/
        discountLeadger.setIsBillWiseon(ApplicationConstants.TALLY_NO_CONSTANT);
        discountLeadger.setIsCostcentereson(ApplicationConstants.TALLY_YES_CONSTANT);
        discountLeadger.setIsIntereston(ApplicationConstants.TALLY_NO_CONSTANT);
        discountLeadger.setIsCostTrackinGon(ApplicationConstants.TALLY_NO_CONSTANT);
        discountLeadger.setIsBeneficiaryCodeOn(ApplicationConstants.TALLY_NO_CONSTANT);
        discountLeadger.setIsUpdatingTargetId(ApplicationConstants.TALLY_NO_CONSTANT);
        discountLeadger.setAsOriginal(ApplicationConstants.TALLY_YES_CONSTANT);
        discountLeadger.setIsCondensed(ApplicationConstants.TALLY_NO_CONSTANT);
        discountLeadger.setAffectsStock(ApplicationConstants.TALLY_NO_CONSTANT);
        discountLeadger.setIsRateInclusiveVat(ApplicationConstants.TALLY_NO_CONSTANT);
        discountLeadger.setForPayroll(ApplicationConstants.TALLY_NO_CONSTANT);
        discountLeadger.setIsAbcenAbled(ApplicationConstants.TALLY_NO_CONSTANT);
        discountLeadger.setIsCreditDaySchkon(ApplicationConstants.TALLY_NO_CONSTANT);

        discountLeadger.setIsGstApplicable(ApplicationConstants.TALLY_NO_CONSTANT);


        discountLeadger.setDiscountLanguageNameList(getDiscountLanguageList());
        return  discountLeadger;
    }

    private DiscountOldEntryList getOldEntryList() {
 DiscountOldEntryList discountOldEntryList = new DiscountOldEntryList();
        discountOldEntryList.setOldAuditEntryIds(-1);


 return  discountOldEntryList;

    }

    private DiscountLanguageNameList getDiscountLanguageList(){
      DiscountLanguageNameList discountLanguageNameList = new DiscountLanguageNameList();
        discountLanguageNameList.setLanguageId(ApplicationConstants.TALLY_LANGUAGE_ID);
        discountLanguageNameList.setDiscountName(getDiscountName());
        return discountLanguageNameList;
    }
    private DiscountName getDiscountName(){
        DiscountName discountName =new DiscountName();
        discountName.setName(ApplicationConstants.TALLY_TYPE_DISCOUNT);
        return discountName;
    }

   /* private BankRequestDesc getBankRequestDesc(){

        BankRequestDesc bankRequestDesc = new BankRequestDesc();
        bankRequestDesc.setReportName(ApplicationConstants.TALLY_REPORT_NAME);
        bankRequestDesc.setBankStaticVariable(getBankStaticVariable());

        return  bankRequestDesc;
    }
    private BankStaticVariable getBankStaticVariable(){
        BankStaticVariable bankStaticVariable = new BankStaticVariable();
        return bankStaticVariable;
    }*/

}





