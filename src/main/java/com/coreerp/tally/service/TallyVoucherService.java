package com.coreerp.tally.service;

import com.coreerp.controller.TallyController;
import com.coreerp.model.TransactionType;
import com.coreerp.tally.model.voucher.*;
//import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Date;
import java.util.List;

@Service
public class TallyVoucherService {
    final static Logger log = LogManager.getLogger(TallyVoucherService.class);
    @Autowired
    VoucherService voucherService;

    public InputStreamResource getVoucherXml(TransactionType invoiceType,Long PartyId, Date invoiceFromDate, Date invoiceToDate){
        XmlMapper xmlMapper = new XmlMapper();
        Envelope envelope = getVoucherEnvelope(invoiceType,PartyId,invoiceFromDate, invoiceToDate);
        InputStream targetStream = null;
        String outValue = new String();
        try {
            outValue = xmlMapper.writeValueAsString(envelope);
            outValue = outValue.replaceAll("xmlns:wstxns\\d+=","xmlns:UDF=");
            outValue = outValue.replaceAll("wstxns\\d+:", "");

//            outValue = outValue.replace("xmlns:wstxns1=", "xmlns:UDF=");
//            outValue = outValue.replace("wstxns1:", "");
//
//            outValue = outValue.replace("xmlns:wstxns2=", "xmlns:UDF=");
//            outValue = outValue.replace("wstxns2:", "");
//            outValue = outValue.replace("xmlns:wstxns3=", "xmlns:UDF=");
//            outValue = outValue.replace("wstxns3:", "");
            targetStream = new ByteArrayInputStream(outValue.getBytes());
        }catch (Exception e){

        }
        return new InputStreamResource(targetStream);
    }

    public Envelope getVoucherEnvelope(TransactionType invoiceType,Long PartyId, Date invoiceFromDate, Date invoiceToDate){
        log.info("in getVoucherEnvelope");
        Envelope envelope = new Envelope();
        envelope.setHeader(getHeader());

        envelope.setBody(getBody(invoiceType,PartyId, invoiceFromDate, invoiceToDate));
        log.info("returning: "+envelope.getHeader());
        XmlMapper xmlMapper = new XmlMapper();
        try {
            String outValue = xmlMapper.writeValueAsString(envelope);
            log.info("outValue: "+outValue);
        }catch(JsonProcessingException e){
           log.info("error: "+e);
        }
        return envelope;
    }

    private Header getHeader(){
        return  new Header();
    }

    private Body getBody(TransactionType invoiceType,Long PartyId, Date invoiceFromDate, Date invoiceToDate){
        Body body = new Body();
        body.setImportData(getImportData(invoiceType,PartyId, invoiceFromDate, invoiceToDate));
        return body;
    }

    private ImportData getImportData(TransactionType invoiceType,Long PartyId, Date invoiceFromDate, Date invoiceToDate){
        ImportData importData = new ImportData();

        importData.setRequestDesc(getRequestDesc());
        importData.setRequestData(getRequestData(invoiceType,PartyId,invoiceFromDate, invoiceToDate));

        return importData;
    }

    private RequestDesc getRequestDesc(){
        RequestDesc requestDesc = new RequestDesc();
        return requestDesc;
    }

    private RequestData getRequestData(TransactionType invoiceType,Long PartyId, Date invoiceFromDate, Date invoiceToDate){
        RequestData requestData = new RequestData();
       /* requestData.setTallyMessage(getTallyMessage());
        requestData.setTallyMessage1(getTallyMessage());*/
        requestData.setTallyMessageVoucher(getTallyMessageVoucher(invoiceType,PartyId, invoiceFromDate, invoiceToDate));
        return  requestData;
    }

    private TallyMessage getTallyMessage(){
        TallyMessage tallyMessage =new TallyMessage();
        return tallyMessage;
    }

    private List<TallyMessageVoucher> getTallyMessageVoucher(TransactionType invoiceType,Long PartyId, Date invoiceFromDate, Date invoiceToDate){
        return voucherService.getTallyMessageVoucherList(invoiceType,PartyId,invoiceFromDate, invoiceToDate);
    }
}
