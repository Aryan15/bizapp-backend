package com.coreerp.tally.service;


import com.coreerp.ApplicationConstants;
import com.coreerp.dto.PartyBankMapDTO;
import com.coreerp.reports.service.PartyBankMapService;
import com.coreerp.service.PartyService;
import com.coreerp.tally.model.bank.*;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class BankServiceses {


    @Autowired
    PartyBankMapService partyBankMapService;
    @Autowired
    PartyService partyService;

    final static Logger log= LogManager.getLogger(BankServiceses.class);

//
//    public InputStreamResource getBankXML(){
//        XmlMapper xmlMapper = new XmlMapper();
//       BankEnvelope bankEnvelope = getBankEnvelope();
//        InputStream targetStream = null;
//        String outValue = new String();
//        try {
//            outValue = xmlMapper.writeValueAsString(bankEnvelope);
//            outValue = outValue.replaceAll("xmlns:wstxns\\d+=","xmlns:UDF=");
//            outValue = outValue.replaceAll("wstxns\\d+:", "");
//
//            targetStream = new ByteArrayInputStream(outValue.getBytes());
//        }catch (Exception e){
//
//        }
//        return new InputStreamResource(targetStream);
//    }
//
//    private BankEnvelope getBankEnvelope(){
//        BankEnvelope bankEnvelope = new BankEnvelope();
//
//        bankEnvelope.setBankBody(getBankBody());
//        bankEnvelope.setBankHeader(getBankHeader());
//        return bankEnvelope;
//    }
//
//    private BankHeader getBankHeader(){
//        BankHeader bankHeader = new BankHeader();
//        bankHeader.setTallyRequest(ApplicationConstants.TALLY_REQUEST);
//        return bankHeader;
//    }
//
//    private BankBody getBankBody(){
//
//        BankBody bankBody = new BankBody();
//
//        bankBody.setBankImportData(getBankImportData());
//        return bankBody;
//    }
//
//    private BankImportData getBankImportData(){
//        BankImportData bankImportData = new BankImportData();
//        bankImportData.setBankRequestDesc(getBankRequestDesc());
//        bankImportData.setBankRequestData(getBankRequestData());
//        return  bankImportData;
//    }
//
//    private BankRequestData getBankRequestData(){
//        BankRequestData bankRequestData= new BankRequestData();
//        bankRequestData.setBankTallyMessages(getBankTallyMessages());
//
//        return  bankRequestData;
//    }

    public List<BankTallyMessage> getBankTallyMessages(){

        List<BankTallyMessage> bankTallyMessageList= new ArrayList<>();

        List<PartyBankMapDTO> bankDTOList =  partyBankMapService.getForCompany(1l);

        bankDTOList.forEach(bank ->
                bankTallyMessageList.add(getBankTallyMessage(bank))
        );
        return bankTallyMessageList;
    }
    private BankTallyMessage getBankTallyMessage(PartyBankMapDTO bank){
        BankTallyMessage bankTallyMessage = new BankTallyMessage();
        bankTallyMessage.setBankLeadger(getBankLeadger(bank));

        return  bankTallyMessage;
    }
    private BankLeadger getBankLeadger(PartyBankMapDTO bank){

        BankLeadger bankLeadger = new BankLeadger();
        bankLeadger.setName(bank.getBankname());
        bankLeadger.setStartingFrom(ApplicationConstants.TALLY_FINANCIAL_START_DATE);
//        bankLeadger.setCurrencyName(ApplicationConstants.TALLY_CURRENCY_TYPE);
        bankLeadger.setCountryName(ApplicationConstants.TALLY_BANK_COUNTRY_NAME);
        bankLeadger.setParent(ApplicationConstants.TALLY_BANK_PARENT);
        bankLeadger.setIfsCode(bank.getIfsc());
        bankLeadger.setTaxType(ApplicationConstants.TALLY_TAX_TYPE);
        bankLeadger.setBankDetails(bank.getAccountNumber());
        bankLeadger.setBankBranchName(bank.getBankname());

//        bankLeadger.setServiceCategory(ApplicationConstants.TALLY_SERVICE_CATEGORY);
        bankLeadger.setBankingConfigBank("State bank of India (India)");
        bankLeadger.setBankingConfigBankId("1");

//       PartyDTO partyDTO = partyService.getPartyById(bank.getPartyId());
        bankLeadger.setCountryOfResidence(ApplicationConstants.TALLY_BANK_COUNTRY_NAME);
//        bankLeadger.setBankAccHolderName(partyDTO.getName());
//        bankLeadger.setBankStateName(partyDTO.getStateName());
        bankLeadger.setIsBillWiseon(ApplicationConstants.TALLY_NO_CONSTANT);
        bankLeadger.setIsCostcentereson(ApplicationConstants.TALLY_NO_CONSTANT);
        bankLeadger.setAsOriginal(ApplicationConstants.TALLY_YES_CONSTANT);
        bankLeadger.setIsAbcenabled(ApplicationConstants.TALLY_YES_CONSTANT);

        bankLeadger.setBankLanguageName(getBankLanguageName(bank));
        return  bankLeadger;
    }

    private  BankLanguageName getBankLanguageName(PartyBankMapDTO bank){
        BankLanguageName bankLanguageName = new BankLanguageName();
        bankLanguageName.setLanguageId(ApplicationConstants.TALLY_LANGUAGE_ID);
        bankLanguageName.setBankName(getBankName(bank));
        return bankLanguageName;
    }
    private BankName getBankName(PartyBankMapDTO bank){
        BankName bankName = new BankName();
        bankName.setName(bank.getBankname());
        return bankName;
    }

    private  BankRequestDesc getBankRequestDesc(){

        BankRequestDesc bankRequestDesc = new BankRequestDesc();
        bankRequestDesc.setReportName(ApplicationConstants.TALLY_REPORT_NAME);
        bankRequestDesc.setBankStaticVariable(getBankStaticVariable());

        return  bankRequestDesc;
    }
    private BankStaticVariable getBankStaticVariable(){
        BankStaticVariable bankStaticVariable = new BankStaticVariable();
        return bankStaticVariable;
    }

}
