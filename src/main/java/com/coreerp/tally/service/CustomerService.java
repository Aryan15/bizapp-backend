package com.coreerp.tally.service;

import com.coreerp.ApplicationConstants;
import com.coreerp.dto.PartyBankMapDTO;
import com.coreerp.model.Party;
import com.coreerp.model.PartyType;
import com.coreerp.reports.service.PartyBankMapService;
import com.coreerp.service.BankService;
import com.coreerp.service.PartyService;
import com.coreerp.service.PartyTypeService;
import com.coreerp.tally.model.master.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {

    final static Logger log= LogManager.getLogger(CustomerService.class);
    @Autowired
    PartyService partyService;

    @Autowired
    PartyTypeService partyTypeService;

    @Autowired
    BankService bankService;

    @Autowired
    PartyBankMapService partyBankMapService;


   /* public InputStreamResource getCustomerXml(Long partyTypeId){
        XmlMapper xmlMapper = new XmlMapper();
        CustomerEnvelope envelope = getCustomerEnvelope(partyTypeId);
        InputStream targetStream = null;
        String outValue = new String();
        try {
            outValue = xmlMapper.writeValueAsString(envelope);
            outValue = outValue.replaceAll("xmlns:wstxns\\d+=","xmlns:UDF=");
            outValue = outValue.replaceAll("wstxns\\d+:", "");

            targetStream = new ByteArrayInputStream(outValue.getBytes());
        }catch (Exception e){
            log.error("Error: "+e);
        }
        return new InputStreamResource(targetStream);
    }

    private CustomerEnvelope getCustomerEnvelope(Long partyTypeId){
        CustomerEnvelope customerEnvelope = new CustomerEnvelope();

        customerEnvelope.setCustomerHeader(getCustomerHeader());
        customerEnvelope.setCustomerBody(getCustomerBody(partyTypeId));

        return customerEnvelope;
    }

    private CustomerHeader getCustomerHeader(){
        CustomerHeader customerHeader = new CustomerHeader();
        customerHeader.setTallyRequest(ApplicationConstants.TALLY_REQUEST);
        return new CustomerHeader();
    }

    private CustomerBody getCustomerBody(Long partyTypeId){
        CustomerBody customerBody = new CustomerBody();
        customerBody.setCustomerImportData(getCustomerImportData(partyTypeId));
        return customerBody;
    }

    private CustomerImportData getCustomerImportData(Long partyTypeId){
        CustomerImportData customerImportData = new CustomerImportData();

        customerImportData.setCustomerRequestDesc(getCustomerRequestDesc());
        customerImportData.setCustomerRequestData(getCustomerRequestData(partyTypeId));
        return customerImportData;
    }

    private CustomerRequestDesc getCustomerRequestDesc(){
        CustomerRequestDesc customerRequestDesc = new CustomerRequestDesc();
        customerRequestDesc.setTallyRequest(ApplicationConstants.TALLY_REPORT_NAME);
        customerRequestDesc.setCustomerStaticVariable(getCustomerStaticVariable());
        return customerRequestDesc;
    }
    private CustomerStaticVariable getCustomerStaticVariable(){
        CustomerStaticVariable customerStaticVariable = new CustomerStaticVariable();
        return  customerStaticVariable;
    }

    private CustomerRequestData getCustomerRequestData(Long partyTypeId){
        CustomerRequestData customerRequestData = new CustomerRequestData();

        customerRequestData.setCustomerTallyMessages(getCustomerTallyMessages(partyTypeId));

        return customerRequestData;
    }*/

    public List<CustomerTallyMessage> getCustomerTallyMessages(Long partyTypeId){

        List<CustomerTallyMessage> customerTallyMessageList = new ArrayList<>();


        PartyType partyType = partyTypeService.getPartyTypeById(partyTypeId);
        List<Party> customers =  partyService.getPartyModelsByPartyType(partyType);

        customers.forEach(party ->
            customerTallyMessageList.add(getCustomerTallyMessage(party))
        );
        return customerTallyMessageList;
    }

    private CustomerTallyMessage getCustomerTallyMessage(Party party){
        CustomerTallyMessage customerTallyMessage = new CustomerTallyMessage();

        customerTallyMessage.setCustomerLedger(getCustomerLedger(party));
        return customerTallyMessage;
    }

    private CustomerLedger getCustomerLedger(Party party){
        CustomerLedger customerLedger = new CustomerLedger();
      
        customerLedger.setName(party.getName());

        customerLedger.setCustomerAddress(getCustomerAddress(party));
 customerLedger.setCustomerMailingName(getCustomerMailingName(party));
//        customerLeadger.setCustomerOldAuditEntryIds(getCustomerOldAuditEntryIds(party));

        if(party.getPartyType().getName().equals(ApplicationConstants.PARTY_TYPE_CUSTOMER)){
            customerLedger.setParent(ApplicationConstants.TALLLY_SUNDRY_DEBTORS);
        }
        else if(party.getPartyType().getName().equals(ApplicationConstants.PARTY_TYPE_SUPPLIER)){
            customerLedger.setParent(ApplicationConstants.TALLLY_SUNDRY_CREDITORS);
        }

//        customerLeadger.setCurrencyName(ApplicationConstants.TALLY_CURRENCY_TYPE);
        customerLedger.setCountryName(party.getCountry().getName());
//        customerLeadger.setGstRegistrationType(party.getGstRegistrationType().getName());
//        customerLeadger.setVatDealerType(party.getGstRegistrationType().getName());
//        customerLeadger.setTaxType(party.getGstRegistrationType().getName());
        customerLedger.setCountryOfResidence(party.getCountry().getName());
    customerLedger.setPartyGstin(party.getGstNumber());
//        customerLeadger.setServiceCategory(ApplicationConstants.TALLY_SERVICE_CATEGORY);
        customerLedger.setLedStateName(party.getState().getName());
        customerLedger.setIsBillWiseon(ApplicationConstants.TALLY_YES_CONSTANT);
        customerLedger.setIsUpdatingTargetId(ApplicationConstants.TALLY_NO_CONSTANT);
        customerLedger.setAsOriginal(ApplicationConstants.TALLY_YES_CONSTANT);
        customerLedger.setIsChequePrintingEnabled(ApplicationConstants.TALLY_YES_CONSTANT);
        customerLedger.setCustomerLanguageName(getCustomerLanguageName(party));
        customerLedger.setPanNum(party.getPanNumber());
        customerLedger.setPinCode(party.getPinCode());
        customerLedger.setBillCreaditPeriod(party.getDueDaysLimit()+"DAYS");
        customerLedger.setCustomerBankDetails(getCustomerBankDetails(party));
        return customerLedger;
    }



    /*private CustomerBankDetails getCustomerBankDetails(Party party){

        CustomerBankDetails customerBankDetails = new CustomerBankDetails();
        partyBankMapService.getBankDetailsByPartyId(party.getId());
        //customerBankDetails.setIfsCode(party.get);
        return customerBankDetails;
    }*/

    public CustomerBankDetails getCustomerBankDetails(Party party){
        CustomerBankDetails customerBankDetails = new CustomerBankDetails();
        PartyBankMapDTO partyBankDetails ;
       try {
           partyBankDetails = partyBankMapService.getForParty(party.getId()).get(0);


           customerBankDetails.setIfsCode(partyBankDetails.getIfsc());
           customerBankDetails.setAccountNumber(partyBankDetails.getAccountNumber());
           customerBankDetails.setBankName(partyBankDetails.getBankname());
           customerBankDetails.setPaymentFavouring(party.getName());
           customerBankDetails.setTransactionName("primary");
           customerBankDetails.setSetAsDefault(ApplicationConstants.TALLY_NO_CONSTANT);
           customerBankDetails.setDefaultTransactionType("Inter Bank Transfer");
      }
           catch (NullPointerException | IndexOutOfBoundsException e) {

               partyBankDetails=null;
          }


        return customerBankDetails;

    }





    private CustomerAddress getCustomerAddress(Party party){
        CustomerAddress customerAddress = new CustomerAddress();
        customerAddress.setAddress(party.getAddress());
        return customerAddress;
    }
    private CustomerMailingName getCustomerMailingName(Party party){

        CustomerMailingName customerMailingName = new CustomerMailingName();
        customerMailingName.setMailingName(party.getName());
        return  customerMailingName;
    }
    private CustomerOldAuditEntryIds getCustomerOldAuditEntryIds(Party party){
        CustomerOldAuditEntryIds customerOldAuditEntryIds = new CustomerOldAuditEntryIds();
        customerOldAuditEntryIds.setOldAuditEntryIds(1);
        return  customerOldAuditEntryIds;

    }
    private CustomerLanguageName getCustomerLanguageName(Party party){
        CustomerLanguageName customerLanguageName = new CustomerLanguageName();
        customerLanguageName.setLanguageId(ApplicationConstants.TALLY_LANGUAGE_ID);
        customerLanguageName.setCustomerName(getCustomerName(party));
        return customerLanguageName;
    }
    private CustomerName getCustomerName(Party party){
        CustomerName customerName = new CustomerName();
        customerName.setName(party.getName());
        return customerName;
    }
}
