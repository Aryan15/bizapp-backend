--liquibase formatted sql

--changeset ravi:30

UPDATE fw_menu
SET deleted = 'N'
WHERE id IN (16, 17, 21, 22)
AND deleted = 'Y';


UPDATE fw_activity
SET deleted = 'N'
WHERE id IN (
72,73,74,75,76,77,78,79,80,81,97,98,102)
AND deleted = 'Y';



UPDATE fw_activity_role_binding
SET deleted = 'N'
WHERE activity_id IN
(
SELECT id
FROM fw_activity
WHERE id IN (
72,73,74,75,76,77,78,79,80,81,97,98,102)
);

UPDATE `cd_transaction_type`
SET deleted = 'N'
WHERE deleted = 'Y'
AND id != 24;

INSERT  INTO `conf_number_range`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`auto_number`,`auto_number_reset`,`terms_and_condition_check`,`deleted`,`prefix`,`postfix`,`start_number`,`trans_type_id`,`delimiter`,`financial_year_check`,`print_template_id`,`print_template_top_height`,`print_header_text`) VALUES (NULL,NULL,NULL,NULL,0,0,NULL,'N','JWPO',NULL,NULL,20,NULL,0,NULL,180,'PURCHASE ORDER');
INSERT  INTO `conf_number_range`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`auto_number`,`auto_number_reset`,`terms_and_condition_check`,`deleted`,`prefix`,`postfix`,`start_number`,`trans_type_id`,`delimiter`,`financial_year_check`,`print_template_id`,`print_template_top_height`,`print_header_text`) VALUES (NULL,NULL,NULL,NULL,0,0,NULL,'N','JWIDC',NULL,NULL,13,NULL,0,NULL,180,'DELIVERY CHALLAN');
INSERT  INTO `conf_number_range`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`auto_number`,`auto_number_reset`,`terms_and_condition_check`,`deleted`,`prefix`,`postfix`,`start_number`,`trans_type_id`,`delimiter`,`financial_year_check`,`print_template_id`,`print_template_top_height`,`print_header_text`) VALUES (NULL,NULL,NULL,NULL,1,1,NULL,'N','JWODC',NULL,NULL,14,NULL,0,NULL,180,'DELIVERY CHALLAN');
INSERT  INTO `conf_number_range`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`auto_number`,`auto_number_reset`,`terms_and_condition_check`,`deleted`,`prefix`,`postfix`,`start_number`,`trans_type_id`,`delimiter`,`financial_year_check`,`print_template_id`,`print_template_top_height`,`print_header_text`) VALUES (NULL,NULL,NULL,NULL,1,1,NULL,'N','JWIN',NULL,NULL,15,NULL,0,NULL,180,'TAX INVOICE');
INSERT  INTO `conf_number_range`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`auto_number`,`auto_number_reset`,`terms_and_condition_check`,`deleted`,`prefix`,`postfix`,`start_number`,`trans_type_id`,`delimiter`,`financial_year_check`,`print_template_id`,`print_template_top_height`,`print_header_text`) VALUES (NULL,NULL,NULL,NULL,1,0,NULL,'N','JWCN',NULL,NULL,28,NULL,0,NULL,180,'JOBWORK NOTE');

INSERT  INTO `conf_number_range`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`auto_number`,`auto_number_reset`,`terms_and_condition_check`,`deleted`,`prefix`,`postfix`,`start_number`,`trans_type_id`,`delimiter`,`financial_year_check`,`print_template_id`,`print_template_top_height`,`print_header_text`) VALUES (NULL,NULL,NULL,NULL,1,1,NULL,'N','SCPO',NULL,NULL,21,NULL,0,NULL,180,'PURCHASE ORDER');
INSERT  INTO `conf_number_range`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`auto_number`,`auto_number_reset`,`terms_and_condition_check`,`deleted`,`prefix`,`postfix`,`start_number`,`trans_type_id`,`delimiter`,`financial_year_check`,`print_template_id`,`print_template_top_height`,`print_header_text`) VALUES (NULL,NULL,NULL,NULL,0,0,NULL,'N','SCIDC',NULL,NULL,17,NULL,0,NULL,180,'DELIVERY CHALLAN');
INSERT  INTO `conf_number_range`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`auto_number`,`auto_number_reset`,`terms_and_condition_check`,`deleted`,`prefix`,`postfix`,`start_number`,`trans_type_id`,`delimiter`,`financial_year_check`,`print_template_id`,`print_template_top_height`,`print_header_text`) VALUES (NULL,NULL,NULL,NULL,1,1,NULL,'N','SCODC',NULL,NULL,16,NULL,0,NULL,180,'DELIVERY CHALLAN');
INSERT  INTO `conf_number_range`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`auto_number`,`auto_number_reset`,`terms_and_condition_check`,`deleted`,`prefix`,`postfix`,`start_number`,`trans_type_id`,`delimiter`,`financial_year_check`,`print_template_id`,`print_template_top_height`,`print_header_text`) VALUES (NULL,NULL,NULL,NULL,0,0,NULL,'N','SCIN',NULL,NULL,18,NULL,0,NULL,180,'SUBCONTRACT INVOICE');
INSERT  INTO `conf_number_range`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`auto_number`,`auto_number_reset`,`terms_and_condition_check`,`deleted`,`prefix`,`postfix`,`start_number`,`trans_type_id`,`delimiter`,`financial_year_check`,`print_template_id`,`print_template_top_height`,`print_header_text`) VALUES (NULL,NULL,NULL,NULL,1,0,NULL,'N','SCDN',NULL,NULL,29,NULL,0,NULL,180,'SUBCONTRACT NOTE');


UPDATE cd_status
SET deleted = 'N'
WHERE transaction_type_id
IN(SELECT id FROM cd_transaction_type WHERE deleted = 'N')
AND deleted = 'Y';

UPDATE cd_transaction_type
SET NAME = 'Jobwork Invoice'
WHERE id = 15;


UPDATE cd_transaction_type
SET NAME = 'Subcontracting Invoice'
WHERE id = 18;

UPDATE fw_activity
SET NAME = "Jobwork Invoice", url_params = 15
WHERE id = 76;


UPDATE fw_activity
SET NAME = "Subcontracting Invoice", url_params = 18
WHERE id = 81;


UPDATE cd_status
SET name = 'Invoiced'
WHERE id = 102;


INSERT  INTO `fw_activity`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`activity_order`,`deleted`,`description`,`image_path`,`name`,`page_url`,`company_id`,`menu_id`,`sub_menu_id`,`url_params`) VALUES (103,NULL,NULL,NULL,NULL,1,'N',NULL,NULL,'Incoming DC Report','report/jr-container',1,21,NULL,'13,0,Jobwork-InDC');
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,103,NULL,1,NULL);
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,103,NULL,3,NULL);

INSERT  INTO `fw_activity`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`activity_order`,`deleted`,`description`,`image_path`,`name`,`page_url`,`company_id`,`menu_id`,`sub_menu_id`,`url_params`) VALUES (104,NULL,NULL,NULL,NULL,1,'N',NULL,NULL,'Incoming Outgoing DC Report','report/jr-container',1,21,NULL,'13,14,JW-IN-OUT-DC');
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,104,NULL,1,NULL);
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,104,NULL,3,NULL);


INSERT  INTO `fw_activity`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`activity_order`,`deleted`,`description`,`image_path`,`name`,`page_url`,`company_id`,`menu_id`,`sub_menu_id`,`url_params`) VALUES (105,NULL,NULL,NULL,NULL,1,'N',NULL,NULL,'Outgoing DC Report','report/jr-container',1,21,NULL,'14,0,Jobwork-InDC');
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,105,NULL,1,NULL);
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,105,NULL,3,NULL);


INSERT  INTO `fw_activity`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`activity_order`,`deleted`,`description`,`image_path`,`name`,`page_url`,`company_id`,`menu_id`,`sub_menu_id`,`url_params`) VALUES (106,NULL,NULL,NULL,NULL,1,'N',NULL,NULL,'Subcontract Out DC Report','report/jr-container',1,22,NULL,'16,0,Jobwork-InDC');
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,106,NULL,1,NULL);
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,106,NULL,3,NULL);

INSERT  INTO `fw_activity`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`activity_order`,`deleted`,`description`,`image_path`,`name`,`page_url`,`company_id`,`menu_id`,`sub_menu_id`,`url_params`) VALUES (107,NULL,NULL,NULL,NULL,1,'N',NULL,NULL,'Subcontract Outgoing Incoming DC Report','report/jr-container',1,22,NULL,'16,17,JW-IN-OUT-DC');
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,107,NULL,1,NULL);
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,107,NULL,3,NULL);


INSERT  INTO `fw_activity`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`activity_order`,`deleted`,`description`,`image_path`,`name`,`page_url`,`company_id`,`menu_id`,`sub_menu_id`,`url_params`) VALUES (108,NULL,NULL,NULL,NULL,1,'N',NULL,NULL,'Subcontract Incoming DC Report','report/jr-container',1,22,NULL,'17,0,Jobwork-InDC');
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,108,NULL,1,NULL);
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,108,NULL,3,NULL);

INSERT  INTO `fw_activity`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`activity_order`,`deleted`,`description`,`image_path`,`name`,`page_url`,`company_id`,`menu_id`,`sub_menu_id`,`url_params`) VALUES (109,NULL,NULL,NULL,NULL,1,'N',NULL,NULL,'Jobwork Invoice Report','report/jr-container',1,21,NULL,'15,0,Incoming_Jobwork_Invoice_Report');
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,109,NULL,1,NULL);
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,109,NULL,3,NULL);


INSERT  INTO `fw_activity`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`activity_order`,`deleted`,`description`,`image_path`,`name`,`page_url`,`company_id`,`menu_id`,`sub_menu_id`,`url_params`) VALUES (110,NULL,NULL,NULL,NULL,1,'N',NULL,NULL,'Subcontract Invoice Report','report/jr-container',1,22,NULL,'18,0,Incoming_Jobwork_Invoice_Report');
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,110,NULL,1,NULL);
INSERT  INTO `fw_activity_role_binding`(`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`permission_to_approve`,`permission_to_create`,`permission_to_delete`,`permission_to_cancel`,`permission_to_print`,`permission_to_update`,`activity_id`,`company_id`,`role_id`,`user_id`) VALUES (NULL,NULL,NULL,NULL,'N',NULL,1,NULL,NULL,NULL,NULL,110,NULL,3,NULL);