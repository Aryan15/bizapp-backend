--liquibase formatted sql

--changeset ravi:30

insert into user_tenant_relation(id, username, tenant) values(1, 'ravi', 'mybos_coreerp_tenant1');

insert  into `ma_country`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`name`) values (1,NULL,NULL,NULL,NULL,'N','India');
insert  into `ma_country`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`name`) values (2,NULL,NULL,NULL,NULL,'N','Australia');
insert  into `ma_country`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`name`) values (3,NULL,'2018-01-19 12:20:26',NULL,'2018-01-19 12:20:26','N','Sri lanka');
insert  into `ma_country`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`name`) values (4,NULL,'2018-02-01 16:43:54',NULL,'2018-02-01 16:43:54','N','USA');

INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (1,'Jammu and Kashmir',1,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (2,'Himachal Pradesh',2,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (3,'Punjab',3,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (4,'Chandigarh',4,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (5,'Uttarakhand',5,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (6,'Haryana',6,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (7,'Delhi',7,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (8,'Rajasthan',8,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (9,'Uttar Pradesh',9,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (10,'Bihar',10,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (11,'Sikkim',11,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (12,'Arunachal Pradesh',12,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (13,'Nagaland',13,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (14,'Manipur',14,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (15,'Mizoram',15,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (16,'Tripura',16,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (17,'Meghalaya',17,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (18,'Assam',18,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (19,'West Bengal',19,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (20,'Jharkhand',20,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (21,'Orissa',21,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (22,'Chhattisgarh',22,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (23,'Madhya Pradesh',23,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (24,'Gujarat',24,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (25,'Daman and Diu',25,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (26,'Dadra and Nagar Haveli',26,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (27,'Maharashtra',27,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (28,'Andhra Pradesh',28,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (29,'Karnataka',29,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (30,'Goa',30,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (31,'Lakshadweep',31,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (32,'Kerala',32,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (33,'Tamil Nadu',33,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (34,'Puducherry',34,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (35,'Andaman and Nicobar Islands',35,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (36,'TELANGANA',36,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (37,'OTHER TERRITORY',97,1,'N');
INSERT INTO ma_state(id, NAME, state_code, country_id, deleted) VALUES (38,'OTHER COUNTRY',99,1,'N');


insert  into `ma_city`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`name`,`state_id`) values (1,NULL,NULL,NULL,NULL,'N','Bangalore',29);

insert  into `ma_area`(`id`,`created_by`,`created_date_time`,`updated_by`,`updated_date_time`,`deleted`,`name`,`city_id`) values (1,'santhosh','2018-02-20 11:18:13','santhosh','2018-02-20 11:18:13','N','Bsk 3rd stage',1);

insert into `cd_about_company` (`id`, `product_name`, `product_version`, `company_name`, `company_email`, `company_website`, `company_contact_number`, `created_by`, `created_date_time`, `updated_by`, `updated_date_time`) values('1','Suktha Business Solution','Beta 1.0','Suktha Solution','support@suktha.com','www.suktha.com','',NULL,NULL,NULL,NULL);


