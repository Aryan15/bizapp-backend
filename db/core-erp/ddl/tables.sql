--liquibase formatted sql

--changeset ravi:1
create table user_tenant_relation(
  `id` int not null AUTO_INCREMENT,
  `username` varchar(100),
  `tenant` varchar(100),
  `otp_date` datetime DEFAULT NULL,
  `otp_number` bigint(20) DEFAULT 0,
  `email` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  reg_date datetime DEFAULT NULL,
  created_date_time datetime DEFAULT NULL,
  created_by varchar(100),
  updated_date_time datetime DEFAULT NULL,
  updated_by varchar(100),
  is_active int not null DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_UTR_USERNAME` (`username`)
);

CREATE TABLE `ma_country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `deleted` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `ma_state` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `deleted` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `state_code` varchar(255) NOT NULL,
  `country_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_STATE_COUNTRY` (`country_id`),
  CONSTRAINT `FK_STATE_COUNTRY` FOREIGN KEY (`country_id`) REFERENCES `ma_country` (`id`)
);




CREATE TABLE `ma_city` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `deleted` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `state_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CITY_STATE` (`state_id`),
  CONSTRAINT `FK_CITY_STATE` FOREIGN KEY (`state_id`) REFERENCES `ma_state` (`id`)
);


CREATE TABLE `ma_area` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `deleted` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_AREA_CITY` (`city_id`),
  CONSTRAINT `FK_AREA_CITY` FOREIGN KEY (`city_id`) REFERENCES `ma_city` (`id`)
);


CREATE TABLE `cf_email_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email_host` varchar(255) DEFAULT NULL,
  `email_port` int(20) DEFAULT NULL,
  `email_user_name` varchar(255) DEFAULT NULL,
  `email_password` varchar(255) DEFAULT NULL,
  `email_enabled` varchar(255) DEFAULT NULL,
  `email_smtp_start_tls_enable` varchar(255) DEFAULT NULL,
  `email_smtp_auth` varchar(255) DEFAULT NULL,
  `email_transport_protocol` varchar(255) DEFAULT NULL,
  `email_debug` varchar(255) DEFAULT NULL,
  `email_from`  varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `cd_about_company` (
    `id` BIGINT (20),
    `product_name` VARCHAR (765),
    `product_version` VARCHAR (765),
    `company_name` VARCHAR (765),
    `company_email` VARCHAR (765),
    `company_website` VARCHAR (765),
    `company_contact_number` VARCHAR (765),
    `created_by` VARCHAR (765),
    `created_date_time` DATETIME ,
    `updated_by` VARCHAR (765),
    `updated_date_time` DATETIME
);


CREATE TABLE `conf_help_videos`(
  `id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `created_by` VARCHAR(225) DEFAULT NULL,
  `created_date_time` DATETIME DEFAULT NULL,
  `updated_by` VARCHAR(225) DEFAULT NULL,
  `updated_date_time` DATETIME DEFAULT NULL,
  `header` VARCHAR(255),
 `description` VARCHAR(255),
  `activity_id` BIGINT(10),
  `transaction_type_id` BIGINT(10),
 `orders` INT(10),
 `video_link` VARCHAR(255),

 PRIMARY KEY (`id`)
-);
